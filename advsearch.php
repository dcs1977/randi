<?php
if (isset($_POST['a'])) ob_start();
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               advsearch.php                    #
# File purpose            Powerfull search profiles        #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
define('REQ_COUNTRIES',True);
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
//include_once 'classes/cachequery.class.php';

include_once 'templates/'.C_TEMP.'/config.php';
IsSecure(C_ADVSEARCH);
define('C_TITLE',template(C_SEARCH_TITLE,array('SEARCH'=>$w[125],'SNAME'=>C_SNAME)));

if((isset($a) && !(isset($sid) && isset($cache))) && C_SEARCH_TIMEOUT)
{
	$diff = time() - get_info('search');
	if($diff < C_SEARCH_TIMEOUT) printm(template($w[597],array(C_SEARCH_TIMEOUT,C_SEARCH_TIMEOUT-$diff)));
	set_info('search',time());
}
include_once C_PATH.'/header.php';

if(!isset($a)) $a='';

# Detect access
$access=explode(',',C_ADVSRCH);
$access[-1]='-1';

$sr=explode(',',C_SR_COLUMNS);

if ($a == "s")
{
	/*if(isset($sid) && isset($cache)) {
		if(!is_numeric($sid) || !is_md5($cache)) printm($w[1].'1');
		IsAccessToSeeThisPage($access[36]);

		$tmp=db_query("SELECT * FROM ".C_MYSQL_SEARCH." WHERE id = '".$sid."' AND cache = '".$cache."' AND (uid = '".MyID()."' OR uid = 0) AND utype <= '".MyAccess()."' limit 1");
		$cnt = db_num_rows($tmp);
		if($cnt == 0) printm($w[530]);
		while ($i = db_fetch_array($tmp))
		{
			$count = $i['result'];
			$import= unserialize($i['search_data']);
		}
		list($gender,$sgender,$agef,$aget,$country,$purpose,$orientation,$bodytype,$interests,$horo,$id,$usr,$fname,$lname,$keywords,$state,$city,$zip,$heightf,$heightt,$weightf,$weightt,$marstat,$child,$hcolor,$ecolor,$etnicity,$religion,$smoke,$drink,$education,$langlist,$photo,$url,$icq,$aim,$phone,$online,$regin,$usertype,$sortby,$step,$purpose_oper,$interests_oper,$religion_oper,$lang_oper,$distance) = $import;


	}*/
	//else{
	IsAccessToSeeThisPage($access[36]);

        if(!ReturnSearchValue('usertype','num',33,$wpu,1)) $usertype = -1;
        if(!ReturnSearchValue('id','num',-1,0,1)) $id = '';
        if(!ReturnSearchValue('distance','num',-1,0,1)) $distance = '';
        # Checking for bad symbols
        $keywords=ReturnSearchValue('keywords','text',12);$state=ReturnSearchValue('state','text',38);
        $usr=ReturnSearchValue('usr','text',9);$city=ReturnSearchValue('city','text',13);
        $fname=ReturnSearchValue('fname','text',10);$lname=ReturnSearchValue('lname','text',11);
        $zip=ReturnSearchValue('zip','text',14);

        $photo=ReturnSearchValue('photo','text',31);
        $phone=ReturnSearchValue('phone','text',30);
        $url=ReturnSearchValue('url','text',27);
        $icq=ReturnSearchValue('icq','text',28);
        $aim=ReturnSearchValue('aim','text',29);
        $online=ReturnSearchValue('online','text',32);

        if(!isset($agef) || !is_numeric($agef) || ($agef < 0) || !IsSearch($agef,$access[6])) $agef = 0;
        if(!isset($aget) || !is_numeric($aget) || ($agef > 255) || !IsSearch($aget,$access[6])) $aget = 255;
        if($agef > $aget) printm($w[109]);

        $horo=ReturnSearchValue('horo','multinum',7,$whr);
        $gender=ReturnSearchValue('gender','multinum',1,$wg);
        $sgender=ReturnSearchValue('sgender','num',0,$wg);
        $orientation=ReturnSearchValue('orientation','multinum',4,$wor);
        $bodytype=ReturnSearchValue('bodytype','multinum',5,$wbt);
        $interests=ReturnSearchValue('interests','multinum',37,$win);
        $interests_oper=ReturnSearchValue('interests_oper','num',37);
        $purpose=ReturnSearchValue('purpose','multinum',3,$wp);
        $purpose_oper=ReturnSearchValue('purpose_oper','num',3);
        $country=ReturnSearchValue('country','multinum',2,$wcr);
        $marstat=ReturnSearchValue('marstat','multinum',17,$wm);
        $child=ReturnSearchValue('child','multinum',18,$wc);
        $hcolor=ReturnSearchValue('hcolor','multinum',19,$whc);
        $ecolor=ReturnSearchValue('ecolor','multinum',20,$we);
        $heightf=ReturnSearchValue('heightf','num',15,$wh);
        $heightt=ReturnSearchValue('heightt','num',15,$wh);
        $heightt=($heightt == (sizeof($wh) - 1)) ? 0 : $heightt;
        $weightf=ReturnSearchValue('weightf','num',16,$ww);
        $weightt=ReturnSearchValue('weightt','num',16,$ww);
        $weightt=($weightt == (sizeof($ww) - 1)) ? 0 : $weightt;
        $etnicity=ReturnSearchValue('etnicity','multinum',21,$wet);
        $religion=ReturnSearchValue('religion','multinum',22,$wr);
        $religion_oper=ReturnSearchValue('religion_oper','num',3);
        $smoke=ReturnSearchValue('smoke','multinum',23,$ws);
        $drink=ReturnSearchValue('drink','multinum',24,$wd);
        $education=ReturnSearchValue('education','multinum',25,$wed);
        $regin=ReturnSearchValue('regin','num',34,$wrg);
        $sortby=ReturnSearchValue('sortby','num',35);

        $langlist='';
        if(isset($lang) && IsSearch($lang,$access[26]))
        {
                $p=0;while (isset($lang[$p]))		{
                        if(($lang[$p] >= sizeof($wlng)) || ($lang[$p] < 0)) printm($w[1].'2', 1);
                        $langlist.=$lang[$p].',';$p++;}
                        $langlist=substr($langlist, 0, -1);
        }
        $lang_oper=ReturnSearchValue('lang_oper','num',26);

        $intlist='';
        if(isset($interests) && IsSearch($interests,$access[37]))
        {
                $p=0;while (isset($interests[$p]))		{
                        if(($interests[$p] >= sizeof($win)) || ($interests[$p] < 0)) printm($w[1].'2', 1);
                        $intlist.=$interests[$p].',';$p++;}
                        $intlist=substr($intlist, 0, -1);
        }
        $export=stripslashes(serialize(array($gender,$sgender,$agef,$aget,$country,$purpose,$orientation,$bodytype,$interests,$horo,$id,$usr,$fname,$lname,$keywords,$state,$city,$zip,$heightf,$heightt,$weightf,$weightt,$marstat,$child,$hcolor,$ecolor,$etnicity,$religion,$smoke,$drink,$education,$langlist,$photo,$url,$icq,$aim,$phone,$online,$regin,$usertype,$sortby,$step,$purpose_oper,$interests_oper,$religion_oper,$lang_oper,$distance)));
	//}
        
	///////// Checking and creating for search
	$sh_ph_f = (C_SHOW_PHOTOS_FIRST) ? 't1.files DESC,' : '';
	if(!IsSearch($sortby,$access[35])) $sortby = 0;
	$privsf = (C_PRIV_SHOW_FIRST) ? 't1.req DESC,':'';
	switch ($sortby) {
		case "1":$msortby = " order by ".$privsf.$sh_ph_f." t1.editdate ASC";break;
		case "2":$msortby = " order by ".$privsf.$sh_ph_f." t1.username DESC";break;
		case "3":$msortby = " order by ".$privsf.$sh_ph_f." t1.username ASC";break;
		case "4":$msortby = " order by ".$privsf.$sh_ph_f." t1.birthday ASC";break;
		case "5":$msortby = " order by ".$privsf.$sh_ph_f." t1.birthday DESC";break;
		case "6":$msortby = " order by ".$privsf.$sh_ph_f." t1.height DESC";break;
		case "7":$msortby = " order by ".$privsf.$sh_ph_f." t1.height ASC";break;
		case "8":$msortby = " order by ".$privsf.$sh_ph_f." t1.weight DESC";break;
		case "9":$msortby = " order by ".$privsf.$sh_ph_f." t1.weight ASC";break;
		default:$msortby = " order by ".$privsf.$sh_ph_f." t1.editdate DESC";break;
	}

	$musertype = ($usertype >= '0' && IsSearch($usertype,$access[33])) ? " AND t1.req = '".$usertype."'" : $musertype = "";

	$mlanglist=$mintlist=$mpurpose=$mreligion='';
	if($langlist != '') {
		$lang=explode(',',$langlist);$p=0;
		if($lang_oper)
		{
			while (isset($lang[$p])){$mlanglist.= ($lang[$p] != 0) ? " AND FIND_IN_SET(".cb(C_MYSQL_CHARSET)."'".$lang[$p]."',t2.languages)": "";$p++;}
		} else {
			$mlanglist.=" AND (";
			while (isset($lang[$p])){$mlanglist.= ($lang[$p] != 0) ? "FIND_IN_SET(".cb(C_MYSQL_CHARSET)."'".$lang[$p]."',t2.languages) OR ": "";$p++;}
			$mlanglist=(strlen($mlanglist) > 10) ? substr($mlanglist,0,-4).")" : "";
		}
	}
	if($interests != '') {
		$interests=explode(',',$interests);$p=0;
		if($interests_oper)
		{
			while (isset($interests[$p])){$mintlist.= ($interests[$p] != 0) ? " AND FIND_IN_SET(".cb(C_MYSQL_CHARSET)."'".$interests[$p]."',t2.interests)": "";$p++;}
		} else {
			$mintlist.=" AND (";
			while (isset($interests[$p])){$mintlist.= ($interests[$p] != 0) ? "FIND_IN_SET(".cb(C_MYSQL_CHARSET)."'".$interests[$p]."',t2.interests) OR ": "";$p++;}
			$mintlist=(strlen($mintlist) > 10) ? substr($mintlist,0,-4).")" : "";
		}
	}
	if($purpose != '') {
		$purpose=explode(',',$purpose);$p=0;
		if($purpose_oper)
		{
			while (isset($purpose[$p])){$mpurpose.= ($purpose[$p] != 0) ? " AND FIND_IN_SET(".cb(C_MYSQL_CHARSET)."'".$purpose[$p]."',t1.purposes)": "";$p++;}
		} else {
			$mpurpose.=" AND (";
			while (isset($purpose[$p])){$mpurpose.= ($purpose[$p] != 0) ? "FIND_IN_SET(".cb(C_MYSQL_CHARSET)."'".$purpose[$p]."',t1.purposes) OR ": "";$p++;}
			$mpurpose=(strlen($mpurpose) > 10) ? substr($mpurpose,0,-4).")" : "";
		}
	}

    if($religion != '') {
        $religion=explode(',',$religion);$p=0;
        if($religion_oper)
        {
            while (isset($religion[$p])){$mreligion.= ($religion[$p] != 0) ? " AND FIND_IN_SET(".cb(C_MYSQL_CHARSET)."'".$religion[$p]."',t2.religion)": "";$p++;}
        } else {
            $mreligion.=" AND (";
            while (isset($religion[$p])){$mreligion.= ($religion[$p] != 0) ? "FIND_IN_SET(".cb(C_MYSQL_CHARSET)."'".$religion[$p]."',t2.religion) OR ": "";$p++;}
            $mreligion=(strlen($mreligion) > 10) ? substr($mreligion,0,-4).")" : "";
        }
    }

	if(isset($online) && ($online == "on") && IsSearch($online,$access[32]))
	{
		$onlinesql = ' AND t1.id = t3.user';
		$onlinetable = ', '.C_MYSQL_ONLINE_USERS.' as t3';
	}
	else
	{
		$onlinesql = '';
		$onlinetable = '';
	}

	// Important /////////////
	$mid = ($id != "" && IsSearch($id,$access[8])) ? " t1.id = '".$id."' AND t1.status >= '7' AND t1.hide='0' AND t1.id = t2.id".$onlinesql : " t1.status >= '7' AND t1.hide='0' AND t1.id = t2.id".$onlinesql;
	//////////////////////////
	$mfname = (!empty($fname) && IsSearch($fname,$access[10])) ? " AND t1.fname LIKE '%".$fname."%'" : "";
	$mlname = (!empty($lname) && IsSearch($lname,$access[11])) ? $mlname=" AND t1.lname LIKE '%".$lname."%'" : "";
	$mhoro = (!empty($horo) && IsSearch($horo,$access[7])) ? " AND t1.horo IN (".$horo.")" : "";
	$mzip = (!empty($zip) && IsSearch($zip,$access[14])) ? " AND t1.zip = '".$zip."'" : "";
	$musr = (!empty($usr) && IsSearch($usr,$access[9])) ? " AND t1.username LIKE '".str_replace("*", "%", $usr)."'" : "";
	$mkeywords = (!empty($keywords) && IsSearch($keywords,$access[12])) ? " AND (t2.descr LIKE '%".$keywords."%' OR t2.hobby LIKE '%".$keywords."%' OR t2.job LIKE '%".$keywords."%')" : "";
	$mcity = (!empty($city) && IsSearch($city,$access[13])) ? " AND t1.city LIKE '%".$city."%'" : "";
	$mstate = (!empty($state) && IsSearch($state,$access[38])) ? " AND t2.state LIKE '%".$state."%'" : "";
	$mdistance = '';
	$magef = (!empty($agef) && IsSearch($agef,$access[6]) && is_numeric($agef) && ($agef >= 0)) ? " AND t1.birthday <= DATE_SUB(NOW(), INTERVAL ".$agef." YEAR)" : "";
	$maget = (!empty($aget) && IsSearch($aget,$access[6]) && is_numeric($aget) && ($aget <= 255) && ($aget != 255)) ? " AND t1.birthday >= DATE_SUB(NOW(), INTERVAL ".($aget+1)." YEAR)" : "";

	$mheightf=(!empty($heightf) && IsSearch($heightf,$access[15])) ? " AND t1.height >= '".$heightf."'" : "";
	$mheightt=(!empty($heightt) && IsSearch($heightt,$access[15])) ? " AND t1.height <= '".$heightt."'" : "";
	$mweightf=(!empty($weightf) && IsSearch($weightf,$access[16])) ? " AND t1.weight >= '".$weightf."'" : "";
	$mweightt=(!empty($weightt) && IsSearch($weightt,$access[16])) ? " AND t1.weight <= '".$weightt."'" : "";
	$mgender = (!empty($gender) && IsSearch($gender,$access[1])) ? " AND t1.gender IN (".$gender.")" : "";
	$msgender = (!empty($sgender) && IsSearch($sgender,$access[0])) ? " AND (FIND_IN_SET(".cb(C_MYSQL_CHARSET)."'".$sgender."',t1.sgender) OR t1.sgender = '')" : "";
	#$mpurpose = (!empty($purpose) && IsSearch($purpose,$access[3])) ? " AND t1.purposes = '".$purpose."'" : "";
	$mcountry = (!empty($country) && IsSearch($country,$access[2])) ? " AND t1.country IN (".$country.")" : "";
	$mbodytype = (!empty($bodytype) && IsSearch($bodytype,$access[5])) ? " AND t1.bodytype IN (".$bodytype.")" : "";
	$morientation = (!empty($orientation) && IsSearch($orientation,$access[4])) ? " AND t1.orientation IN (".$orientation.")" : "";
	$mmarstat = (!empty($marstat) && IsSearch($marstat,$access[17])) ? " AND t2.marstat IN (".$marstat.")" : "";
	$mchild = (!empty($child) && IsSearch($child,$access[18])) ? " AND t2.child IN (".$child.")" : "";
	$mhcolor = (!empty($hcolor) && IsSearch($hcolor,$access[19])) ? " AND t2.hcolor IN (".$hcolor.")" : "";
	$mecolor = (!empty($ecolor) && IsSearch($ecolor,$access[20])) ? " AND t2.ecolor IN (".$ecolor.")" : "";
	$metnicity = (!empty($etnicity) && IsSearch($etnicity,$access[21])) ? " AND t2.etnicity IN (".$etnicity.")" : "";
	#$mreligion = (!empty($religion) && IsSearch($religion,$access[22])) ? " AND t2.religion IN (".$religion.")" : "";
	$msmoke = (!empty($smoke) && IsSearch($smoke,$access[23])) ? " AND t2.smoke IN (".$smoke.")" : "";
	$mdrink = (!empty($drink) && IsSearch($drink,$access[24])) ? " AND t2.drink IN (".$drink.")" : "";
	$meducation = (!empty($education) && IsSearch($education,$access[25])) ? " AND t2.education IN (".$education.")" : "";
	$mregin = (!empty($regin) && IsSearch($regin,$access[34])) ? " AND t1.regdate > DATE_SUB(NOW(), INTERVAL ".$wrgv[$regin]." DAY)" : "";

	$mphoto = (isset($photo) && ($photo == "on") && IsSearch($photo,$access[31])) ? " AND t1.files != ''" : "";
	$murl = (isset($url) && ($url == "on") && IsSearch($url,$access[27])) ? " AND t2.url != ''" : "";
	$micq = (isset($icq) && ($icq == "on") && IsSearch($icq,$access[28])) ? " AND t2.icq != ''" : "";
	$maim = (isset($aim) && ($aim == "on") && IsSearch($aim,$access[29])) ? " AND t2.aim != ''" : "";
	$mphone = (isset($phone) && ($phone == "on") && IsSearch($phone,$access[30])) ? " AND t2.phone != ''" : "";
                
        if (isset($_SESSION['search_date']) && $_SESSION['search_date'] < date("Y-m-d H:i:s")){
            unset($_SESSION['search_type']);    
            unset($_SESSION['search_date']);
            unset($_SESSION['search_sortby']);
            unset($_SESSION['search_data']);
        }
        
        if (!isset($_SESSION['search_data']) || (isset($_SESSION['search_type']) && $_SESSION['search_type'] != 'advsearch')){
            $_SESSION['search_type'] = 'advsearch';
            $_SESSION['search_data'] = array();
            $_SESSION['search_sortby'] = $msortby;
            $tmp0=db_query("SELECT t1.id FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2".$onlinetable." WHERE ".$mid.$mfname.$mlname.$mhoro.$magef.$maget.$mgender.$msgender.$mpurpose.$mbodytype.$morientation.$mcountry.$mmarstat.$mchild.$mhcolor.$mecolor.$metnicity.$mreligion.$msmoke.$mdrink.$meducation.$mlanglist.$mintlist.$mphoto.$mcity.$mstate.$mzip.$mdistance.$musr.$mkeywords.$murl.$micq.$maim.$mphone.$mheightf.$mheightt.$mweightf.$mweightt.$musertype.$mregin.$msortby) or die(db_error());
            while($i=db_fetch_array($tmp0)) {
                $_SESSION['search_data'][] = $i['id'];
            }
            $_SESSION['search_date'] = date("Y-m-d H:i:s", ( time() + 300 ) );
        }
        $search = $_SESSION['search_data'];

        //$tmp0=db_query("SELECT count(*) as total FROM ".C_MYSQL_ONLINE_USERS." as t1, ".C_MYSQL_MEMBERS_MAIN." as t2 WHERE t1.user = t2.id AND t2.status > '5' AND t2.hide='0'".$sql_dump) or die(db_error());
        //$cnt=db_fetch_assoc($tmp0);
        //$count=$cnt['total'];#die($count);
        $count = count($search);
        $in = array_slice($search, $from, $step);
        if (count($in)){
            $where_in = ' AND t1.id IN ('.implode(', ', $in).') ';
        } else {
            $where_in = ' AND t1.id IS NULL ';
        }
        //var_dump($where_in);
        //$tmp=db_query("SELECT DISTINCT t1.user, t1.upage, t2.*, t3.* FROM ".C_MYSQL_ONLINE_USERS." as t1, ".C_MYSQL_MEMBERS_MAIN." as t2, ".C_MYSQL_MEMBERS_EXT." as t3 WHERE t1.user = t2.id AND t2.id = t3.id AND t2.status > '5' AND t2.hide='0'".$sql_dump.$where_in." order by t1.utime desc ") or die(db_error());

	/*if(!isset($sid)) {
		$tmp = New CacheQuery("SELECT count(*) as total FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2".$onlinetable." WHERE ".$mid.$mfname.$mlname.$mhoro.$magef.$maget.$mgender.$msgender.$mpurpose.$mbodytype.$morientation.$mcountry.$mmarstat.$mchild.$mhcolor.$mecolor.$metnicity.$mreligion.$msmoke.$mdrink.$meducation.$mlanglist.$mintlist.$mphoto.$mcity.$mstate.$mzip.$mdistance.$musr.$mkeywords.$murl.$micq.$maim.$mphone.$mheightf.$mheightt.$mweightf.$mweightt.$musertype.$mregin,IsCache(1,1024));
		$count = $tmp->data[0]['total'];
	}*/
	if($count == "0") printm($w[110]);

	### What to show
	$qage = (IsAccess($sr[4])) ? ',t1.birthday' : '';
	$qgender = (IsAccess($sr[5])) ? ',t1.gender' : '';
	$qbodytype = (IsAccess($sr[6])) ? ',t1.bodytype' : '';
	$qorientation = (IsAccess($sr[7])) ? ',t1.orientation' : '';
	$qpurpose = (IsAccess($sr[8])) ? ',t1.purposes' : '';
	$qcountry = (IsAccess($sr[9])) ? ',t1.country' : '';
	$qcity = (IsAccess($sr[10])) ? ',t1.city' : '';
	$qheight = (IsAccess($sr[11])) ? ',t1.height' : '';
	$qweight = (IsAccess($sr[12])) ? ',t1.weight' : '';
	$qsgender = (IsAccess($sr[13])) ? ',t1.sgender' : '';
	$qhoro = (IsAccess($sr[15])) ? ',t1.horo' : '';
	$qregdate = (IsAccess($sr[16])) ? ',t1.regdate' : '';
	$qeditdate = (IsAccess($sr[17])) ? ',t1.editdate' : '';
	$qusertype = (IsAccess($sr[19])) ? ',t1.req' : '';
	$qzip = (IsAccess($sr[21])) ? ',t1.zip' : '';
	if(IsAccess($sr[18])) {
		if(C_POPULARITY == 2) {
			$qrate =',ROUND(hits/hits_count,'.C_RATE_DIGITS.') as hits';
		} else {
			$qrate = ',ROUND(hits/(TO_DAYS(Now()) - TO_DAYS(IF(regdate > \''.C_LAST_CLEAR_DATE.'\', regdate, \''.C_LAST_CLEAR_DATE.'\')) + 1),'.C_RATE_DIGITS.') as hits';
		}
	}
	else $qrate='';
	$qchild = ',t2.child';
        $msortby = $_SESSION['search_sortby'];

	//$tmp = New CacheQuery("SELECT t1.id,t1.username,t1.fname,t1.lname,t1.files".$qage.$qgender.$qchild.$qbodytype.$qorientation.$qpurpose.$qcountry.$qcity.$qzip.$qheight.$qweight.$qsgender.$qhoro.$qregdate.$qeditdate.$qusertype.$qrate." FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2".$onlinetable." WHERE ".$mid.$mfname.$mlname.$mhoro.$magef.$maget.$mgender.$msgender.$mpurpose.$mbodytype.$morientation.$mcountry.$mmarstat.$mchild.$mhcolor.$mecolor.$metnicity.$mreligion.$msmoke.$mdrink.$meducation.$mlanglist.$mintlist.$mphoto.$mcity.$mstate.$mzip.$mdistance.$musr.$mkeywords.$murl.$micq.$maim.$mphone.$mheightf.$mheightt.$mweightf.$mweightt.$musertype.$mregin.$where_in,IsCache(1,1024));
        
        $result = db_query("SELECT t1.id,t1.username,t1.fname,t1.lname,t1.files".$qage.$qgender.$qchild.$qbodytype.$qorientation.$qpurpose.$qcountry.$qcity.$qzip.$qheight.$qweight.$qsgender.$qhoro.$qregdate.$qeditdate.$qusertype.$qrate." FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2".$onlinetable." WHERE ".$mid.$mfname.$mlname.$mhoro.$magef.$maget.$mgender.$msgender.$mpurpose.$mbodytype.$morientation.$mcountry.$mmarstat.$mchild.$mhcolor.$mecolor.$metnicity.$mreligion.$msmoke.$mdrink.$meducation.$mlanglist.$mintlist.$mphoto.$mcity.$mstate.$mzip.$mdistance.$musr.$mkeywords.$murl.$micq.$maim.$mphone.$mheightf.$mheightt.$mweightf.$mweightt.$musertype.$mregin.$where_in.$msortby);
        $tmp = array();
        while ($i = db_fetch_array($result)){
            $tmp[] = $i;
        }

	if(!isset($sid)) {
		$cache=code_gen();
		db_query("INSERT INTO ".C_MYSQL_SEARCH." VALUES (Null,'".MyID()."','".MyAccess()."','".$cache."',NOW(),'".$count."','','".$export."','')");
		$sid = db_fetch_array(db_query("SELECT LAST_INSERT_ID() AS last_id"));
		$sid = $sid['last_id'];
	}

$str='';$color='';
	$ids=$names=$photoinfo=$genders=$childs=$sgenders=$fnames=$lnames=$bodytypes=$orientations=$purposes=$ages=$countries=$cities=$heights=$weights=$horoscopes=$rates=$usertypes=$editdates=$regdates=$pictures=$colors=array();
	### What to show
	function ShowConst($var,$acs)
	{
		Global $sr;
		if(IsAccess($sr[$acs])) define($var,'1');
		else define($var,'0');
	}

	/*ShowConst('SHOW_FNAME',2);
	ShowConst('SHOW_LNAME',3);*/
	define('SHOW_FNAME',false);
	define('SHOW_LNAME', false);
	ShowConst('SHOW_AGE',4);
	ShowConst('SHOW_GENDER',5);
	define('CHILD', $w[135]);
	ShowConst('SHOW_BODYTYPE',6);
	ShowConst('SHOW_ORIENTATION',7);
	ShowConst('SHOW_PURPOSE',8);
	ShowConst('SHOW_COUNTRY',9);
	ShowConst('SHOW_CITY',10);
	ShowConst('SHOW_HEIGHT',11);
	ShowConst('SHOW_WEIGHT',12);
	/*ShowConst('SHOW_SEEKING_GENDER',13);*/
	define('SHOW_SEEKING_GENDER', false);
	ShowConst('SHOW_PHOTO',14);
	ShowConst('SHOW_HORO',15);
	ShowConst('SHOW_REG_DATE',16);
	/*ShowConst('SHOW_LAST_ACCESS',17);*/
	define('SHOW_LAST_ACCESS',true);
	ShowConst('SHOW_RATING',18);
	ShowConst('SHOW_USERTYPE',19);
	ShowConst('SHOW_ONLINE_STATUS',20);
	/*ShowConst('SHOW_ZIP',21);*/
	define('SHOW_ZIP', false);
	define('ONLINE_STATUS',$w[352]);
	define('ONLINE', $w[350]);
	define('OFFLINE', $w[351]);
	define('TOPS', false);

	while(list($k,$i)=@each($tmp)) {
		$pictures[] = SearchPhotoResult(IsAccess($sr[14]),$i['files'],$i['id'],$i['gender']);
		$color = ($color == COLOR4) ? COLOR3 : COLOR4;
		$colors[] = $color;
		$ids[]=$i['id'];
		$photoinfo[]=($i['files'] == '') ? $w[532] : template($w[533],array(PhotosNum($i['files'])));
		$names[] = username($i['id'],$i['username'],$i['fname'],$i['lname']);
		$fnames[] = (IsAccess($sr[2])) ? $i['fname'] : '';
		$lnames[] = (IsAccess($sr[3])) ? $i['lname'] : '';
		$genders[]=(IsAccess($sr[5])) ? $wg[$i['gender']] : '';
		$sgenders[]=(IsAccess($sr[13])) ? arr2val($wg, $i['sgender']) : '';
		$purposes[]=(IsAccess($sr[8])) ? arr2val($wp, $i['purposes']) : '';
		$orientations[]=(IsAccess($sr[7])) ? $wor[$i['orientation']] : '';
		$heights[]=(IsAccess($sr[11])) ? $wh[$i['height']] : '';
		$weights[]=(IsAccess($sr[12])) ? $ww[$i['weight']] : '';
		$horoscopes[]=(IsAccess($sr[15])) ? $whr[$i['horo']] : '';
		$bodytypes[]=(IsAccess($sr[6])) ? $wbt[$i['bodytype']] : '';
		$childs[]= $wc[$i['child']];
		$ages[]=(IsAccess($sr[4])) ? abs(mysql2data($i['birthday'],1)) : '';
		$countries[]=(IsAccess($sr[9])) ? $wcr[$i['country']] : '';
		$cities[]=(IsAccess($sr[10])) ? $i['city'] : '';
		$usertypes[]=(IsAccess($sr[19])) ? $wpu[$i['req']] : '';
		$editdates[]=(IsAccess($sr[17])) ? mysql2data($i['editdate']) : '';
		$regdates[]=(IsAccess($sr[16])) ? mysql2data($i['regdate']) : '';
		$rates[]=(IsAccess($sr[18])) ? $i['hits'] : '';
		$zips[]=(IsAccess($sr[21])) ? $i['zip'] : '';
	}

	$param='l='.$l.'&a=s&sid='.$sid.'&cache='.$cache;
	$colspan=1;
	if (isset($_POST['a'])) {
		ob_end_clean();
		sredirect($step,$count,$param,$colspan);
	}
	define('PAGES',pages($from,$step,$count,$param,$colspan));
	define('SEARCH_RESULTS',search_results($from,$step,$count,IsAccess(C_SHOW_SEARCH_COUNT)));
	define('USERNAME',$w[118]);
	define('PURPOSES',$w[119]);
	define('AGE',$w[120]);
	define('COUNTRY',$w[121]);
	define('CITY',$w[122]);
	define('LAST_ACCESS',$w[123]);
	define('REGISTER_DATE',$w[124]);
	define('PHOTO',$w[87]);
	define('RATING',$w[216]);
	define('FIRSTNAME',$w[127]);
	define('LASTNAME',$w[128]);
	define('GENDER',$w[57]);
	define('BODYTYPE',$w[525]);
	define('ORIENTATION',$w[524]);
	define('HEIGHT',$w[130]);
	define('WEIGHT',$w[131]);
	define('SEEKING_GENDER',$w[80]);
	define('HOROSCOPE',$w[129]);
	define('USERTYPE',$w[270]);
	define('ZIP',$w[285]);
	define('ADDITIONAL_INFO',$w[534]);
	//define('WITH_SELECTED',$w[556]);
	define('WHENSEEN', false);
	$p=0;

	include_once C_PATH.'/templates/'.C_TEMP.'/search_results.php';
} else {

	define('ADVANCED_SEARCH',$w[125]);
        unset($_SESSION['search_type']);    
        unset($_SESSION['search_date']);
        unset($_SESSION['search_sortby']);
        unset($_SESSION['search_data']);
        
	$Value='';$p=0;while(isset($wg[$p])) {$Value .= '<option value="'.$p.'"'.sv($p).'>'.$wg[$p];$p++;}
	IsShowSearchFields('I_AM_A',0,334,$Value);

	$Value='';$p=0;while(isset($wg[$p])) {$Value .= '<option value="'.$p.'"'.sv($p).'>'.$wg[$p];$p++;}
	IsShowSearchFields('SEEKING_FOR',1,335,$Value);
	$cntr = 0;
	
	$cntr0 = null;
	$Value='';asort($wcr);reset($wcr);while (list ($p, $val) = each ($wcr)) {$Value .=  '<option value="'.$p.'"'.sv($p,$cntr0).'>'.$val;}
	IsShowSearchFields('COUNTRY',2,121,$Value);

	$Value='';$p=0;while(isset($wp[$p])) {$Value .= '<option value="'.$p.'"'.sv($p).'>'.$wp[$p];$p++;}
	IsShowSearchFields('TYPE_OF_RELATION',3,133,$Value);

	$Value='';$p=0;while(isset($wor[$p])) {$Value .= '<option value="'.$p.'"'.sv($p).'>'.$wor[$p];$p++;}
	IsShowSearchFields('ORIENTATION',4,524,$Value);

	$Value='';$p=0;while(isset($wbt[$p])) {$Value .= '<option value="'.$p.'"'.sv($p).'>'.$wbt[$p];$p++;}
	IsShowSearchFields('BODYTYPE',5,525,$Value);

	$Value='';while (list ($p, $val) = each ($win)) {$Value .=  '<option value="'.$p.'"'.sv($p).'>'.$val;}
	IsShowSearchFields('INTERESTS',37,526,$Value);

	IsShowSearchFields('AGE',6,120,0,'from_to',0);

	$Value=''; $p=0; while(isset($whr[$p])) {$Value .= '<option value="'.$p.'"'.sv($p).'>'.$whr[$p];$p++;}
	IsShowSearchFields('HOROSCOPE',7,129,$Value);

	IsShowSearchFields('USERID',8,126,'','none');

	if(!(C_ID))
	{
		define('USERNAME_ALLOW',True);
	}
	else
	{
		define('USERNAME_ALLOW',False);
	}
	IsShowSearchFields('USERNAME',9,118,'','none');
	IsShowSearchFields('FIRSTNAME',10,127,'','none');
	IsShowSearchFields('LASTNAME',11,128,'','none');
	IsShowSearchFields('KEYWORDS',12,289,'','none');
	IsShowSearchFields('CITY',13,122,'','none');
	IsShowSearchFields('STATE',38,527,'','none');
	IsShowSearchFields('ZIP',14,285,'','none');
	IsShowSearchFields('DISTANCE',39,721,'','none');

	$Value='';$p=0;while(isset($wh[$p])){if($p == "0") $Value .= '<option value="'.$p.'" selected>'.$wh[$p];else $Value .= '<option value="'.$p.'">'.$wh[$p];$p++;}
	$Value2='';$p=1;while(isset($wh[$p])) {if($p == (sizeof($wh)-1)) $Value2 .= '<option value="'.$p.'" selected>'.$wh[$p];else $Value2 .= '<option value="'.$p.'">'.$wh[$p];$p++;}
	IsShowSearchFields('HEIGHT',15,130,$Value,'from_to',$Value2);

	$Value='';$p=0;while(isset($ww[$p])) {if($p == "0") $Value .= '<option value="'.$p.'" selected>'.$ww[$p];else $Value .= '<option value="'.$p.'">'.$ww[$p];$p++;}
	$Value2='';$p=0;while(isset($ww[$p])) {if($p == (sizeof($ww)-1)) $Value2 .= '<option value="'.$p.'" selected value=0>'.$ww[$p];else $Value2 .= '<option value="'.$p.'">'.$ww[$p];$p++;}
	IsShowSearchFields('WEIGHT',16,131,$Value,'from_to',$Value2);

	$Value='';$p=0;while(isset($wm[$p])) {$Value .= '<option value="'.$p.'"'.sv($p).'>'.$wm[$p];$p++;}
	IsShowSearchFields('MARITAL_STATUS',17,134,$Value);

	$Value='';$p=0;while(isset($wc[$p])) {$Value .= '<option value="'.$p.'"'.sv($p).'>'.$wc[$p];$p++;}
	IsShowSearchFields('CHILD',18,135,$Value);

	$Value='';$p=0;while(isset($whc[$p])) {$Value .= '<option value="'.$p.'"'.sv($p).'>'.$whc[$p];$p++;}
	IsShowSearchFields('HAIR',19,136,$Value);

	$Value='';$p=0;while(isset($we[$p])) {$Value .= '<option value="'.$p.'"'.sv($p).'>'.$we[$p];$p++;}
	IsShowSearchFields('EYE',20,137,$Value);

	$Value='';$p=0;while(isset($wet[$p])) {$Value .= '<option value="'.$p.'"'.sv($p).'>'.$wet[$p];$p++;}
	IsShowSearchFields('ETHNICITY',21,138,$Value);

	$Value='';$p=0;while(isset($wr[$p])) {$Value .= '<option value="'.$p.'"'.sv($p).'>'.$wr[$p];$p++;}
	IsShowSearchFields('RELIGION',22,139,$Value);

	$Value='';$p=0;while(isset($ws[$p])) {$Value .= '<option value="'.$p.'"'.sv($p).'>'.$ws[$p];$p++;}
	IsShowSearchFields('SMOKE',23,140,$Value);

	$Value='';$p=0;while(isset($wd[$p])) {$Value .= '<option value="'.$p.'"'.sv($p).'>'.$wd[$p];$p++;}
	IsShowSearchFields('DRINK',24,141,$Value);

	$Value='';$p=0;while(isset($wed[$p])) {$Value .= '<option value="'.$p.'"'.sv($p).'>'.$wed[$p];$p++;}
	IsShowSearchFields('EDUCATION',25,142,$Value);

	$checklang=array();$p = 0;while (isset($wlng[$p]))
	{$checklang[] = '<input type="checkbox" name="lang[]" value="'.$p.'">';$p++;}
	IsShowSearchFields('LANG',26,382,'','none');


	$Value='<option value="-1">'.$w[402];$p=0;while(isset($wpu[$p])) {$Value .= '<option value="'.$p.'">'.$wpu[$p];$p++;}
	IsShowSearchFields('USERTYPE',33,270,$Value);

	define('SEARCH_USERS_WITH',$w[143]);
	define('WOR',$w[552]);
	define('WAND',$w[553]);
	IsShowSearchFields('PHOTO',31,87,'','none');
	IsShowSearchFields('URL',27,144,'','none');
	IsShowSearchFields('ICQ',28,145,'','none');
	IsShowSearchFields('AIM',29,146,'','none');
	IsShowSearchFields('PHONE',30,147,'','none');
	IsShowSearchFields('ONLINE_USERS_ONLY',32,509,'','none');

	$Value='';$p=0;while(isset($wrg[$p])) {$Value .= '<option value="'.$p.'">'.$wrg[$p];$p++;}
	IsShowSearchFields('REGISTERED_IN',34,148,$Value);

	$Value='';$p=0;while(isset($wsb[$p])) {$Value .= '<option value="'.$p.'">'.$wsb[$p];$p++;}
	IsShowSearchFields('SORT_BY',35,149,$Value);

	$steps=explode(',',C_SEARCH_STEPS);
	$Value='';for($p=0;$p<count($steps);$p++){$Value .= '<option value="'.$steps[$p].'">'.$steps[$p];}
	IsShowSearchFields('RESULTS_ON_PAGE',36,150,$Value);


	define('SIMPLE_SEARCH',$w[151]);

	include_once C_PATH.'/templates/'.C_TEMP.'/advsearch.php';
}
include_once C_PATH.'/footer.php';

if (isset($_POST['a'])) ob_end_flush();
?>