<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               online.php                       #
# File purpose            Show online users                #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';
IsSecure(C_ONLINE);
if(isset($set_cookies))
{
    unset($_SESSION['search_type']);    
    unset($_SESSION['search_date']);
    unset($_SESSION['search_data']);    
    
	function SetOnlineCookies($Const,$arr,$vname,$checkbox=null)
	{
		Global $$vname;
		if(isset($$vname))
		{
			$var = $$vname;
			if($checkbox == null)
			{
				if(IsAccess($Const) && is_numeric($var) && ($var >= 0) && ($var < sizeof($arr)))
				{
					setcookie(C_LOGIN_COOKIE.'on'.$vname, $var, time() + 86400 * C_COOKIE_LIFE);
				}
			}
			elseif($checkbox == 'num')
			{
				if(IsAccess($Const) && is_numeric($var))
				{
					setcookie(C_LOGIN_COOKIE.'on'.$vname, $var, time() + 86400 * C_COOKIE_LIFE);
				}
			}
			elseif($checkbox == 'text')
			{
				if(IsAccess($Const))
				{
					setcookie(C_LOGIN_COOKIE.'on'.$vname, $var, time() + 86400 * C_COOKIE_LIFE);
				}
			}
			else
			{
				if(IsAccess($Const) && ($var == 'on'))
				{
					setcookie(C_LOGIN_COOKIE.'on'.$vname, '1', time() + 86400 * C_COOKIE_LIFE);
				}
				else 
				{
					setcookie(C_LOGIN_COOKIE.'on'.$vname, '-1', time() + 86400 * C_COOKIE_LIFE);
				}
			}
		} else setcookie(C_LOGIN_COOKIE.'on'.$vname, '-1', time() + 86400 * C_COOKIE_LIFE);
	}
	SetOnlineCookies(C_ONLINE_GENDER,$wg,'gender');
	SetOnlineCookies(C_ONLINE_COUNTRY,$wcr,'country');
	SetOnlineCookies(C_ONLINE_CITY,null,'city','text');
	SetOnlineCookies(C_ONLINE_AGE,null,'agef','num');
	SetOnlineCookies(C_ONLINE_AGE,null,'aget','num');
	SetOnlineCookies(C_ONLINE_USERTYPE,$wpu,'usertype');
	SetOnlineCookies(C_ONLINE_PICTURE,null,'picture',True);
	include_once C_PATH.'/header.php';
	GoToURL(C_URL.'/online.php?l='.$l);
	printm($w[523]);
}
include_once C_PATH.'/header.php';

# Detect access
$access=explode(',',C_ADVSRCH);
$access[-1]='-1';

$sr=explode(',',C_SR_COLUMNS);

function GetOnlineCookies($Const,$arr,$vname,$checkbox=null)
{
	if(isset($_COOKIE[C_LOGIN_COOKIE.'on'.$vname]))
	{
		$var = $_COOKIE[C_LOGIN_COOKIE.'on'.$vname];
		if($checkbox == null)
		{
			if(IsAccess($Const) && isset($_COOKIE[C_LOGIN_COOKIE.'on'.$vname]) && is_numeric($_COOKIE[C_LOGIN_COOKIE.'on'.$vname]) && ($_COOKIE[C_LOGIN_COOKIE.'on'.$vname] >=0) && ($_COOKIE[C_LOGIN_COOKIE.'on'.$vname] < sizeof($arr)))
			{
				Return $_COOKIE[C_LOGIN_COOKIE.'on'.$vname];
			} else Return '-1';
		}
		elseif($checkbox == 'num')
		{
			if(IsAccess($Const) && isset($_COOKIE[C_LOGIN_COOKIE.'on'.$vname]) && is_numeric($_COOKIE[C_LOGIN_COOKIE.'on'.$vname]))
			{
				Return $_COOKIE[C_LOGIN_COOKIE.'on'.$vname];
			} else Return '-1';
		}
		elseif($checkbox == 'text')
		{
			if(IsAccess($Const) && isset($_COOKIE[C_LOGIN_COOKIE.'on'.$vname]))
			{
				Return cb($_COOKIE[C_LOGIN_COOKIE.'on'.$vname]);
			} else Return '-1';
		}
		else
		{
			if(IsAccess($Const) && isset($var) && ($var == '1'))
			{
				Return $_COOKIE[C_LOGIN_COOKIE.'on'.$vname];
			} else Return '-1';
		}
	}
}
$gender=GetOnlineCookies(C_ONLINE_GENDER,$wg,'gender');
$country=GetOnlineCookies(C_ONLINE_COUNTRY,$wcr,'country');
$city=GetOnlineCookies(C_ONLINE_CITY,null,'city','text');
$agef=GetOnlineCookies(C_ONLINE_AGE,null,'agef','num');
$aget=GetOnlineCookies(C_ONLINE_AGE,null,'aget','num');
$usertype=GetOnlineCookies(C_ONLINE_USERTYPE,$wpu,'usertype');
$picture=GetOnlineCookies(C_ONLINE_PICTURE,null,'picture',True);

if(file_exists(C_PATH.'/languages/'.$l.'/'.$l.'o.php')) include_once(C_PATH.'/languages/'.$l.'/'.$l.'o.php');
elseif(defined('C_USERLANG')) include_once(C_PATH.'/languages/'.C_USERLANG.'/'.C_USERLANG.'o.php');
else include_once(C_PATH.'/languages/default/defaulto.php');

define('USERS_ONLINE_VALUE',ONLINE_USERS);
define('USERNAME',$w[118]);
define('PURPOSES',$w[119]);
define('AGE',$w[120]);
define('COUNTRY',$w[121]);
define('CITY',$w[122]);
define('PAGE',$w[443]);
define('PHOTO',$w[87]);

### What to show
function ShowConst($var,$acs)
{
	Global $sr;
	if(IsAccess($sr[$acs])) define($var,'1');
	else define($var,'0');
}

if(IsAccess(C_ONLINE_GENDER) || IsAccess(C_ONLINE_USERTYPE) || IsAccess(C_ONLINE_PICTURE)) define('SHOW_FORM','1');
else  define('SHOW_FORM','0');

if(IsAccess(C_ONLINE_GENDER)) ShowConst('SHOW_GENDER',5);
else  define('SHOW_GENDER','0');

if(IsAccess(C_ONLINE_COUNTRY)) ShowConst('SHOW_COUNTRY',9);
else  define('SHOW_COUNTRY','0');

if(IsAccess(C_ONLINE_CITY)) ShowConst('SHOW_CITY',10);
else  define('SHOW_CITY','0');

if(IsAccess(C_ONLINE_AGE)) ShowConst('SHOW_AGE',4);
else  define('SHOW_AGE','0');

if(IsAccess(C_ONLINE_USERTYPE)) ShowConst('SHOW_USERTYPE',19);
else  define('SHOW_USERTYPE','0');

if(IsAccess(C_ONLINE_PICTURE)) define('SHOW_PICTURE','1');
else  define('SHOW_PICTURE','0');

/*ShowConst('SHOW_FNAME',2);
ShowConst('SHOW_LNAME',3);*/
define('SHOW_FNAME',false);
define('SHOW_LNAME', false);
//ShowConst('SHOW_AGE',4);
//ShowConst('SHOW_GENDER',5);
define('CHILD', $w[135]);
ShowConst('SHOW_BODYTYPE',6);
ShowConst('SHOW_ORIENTATION',7);
ShowConst('SHOW_PURPOSE',8);
//ShowConst('SHOW_COUNTRY',9);
//ShowConst('SHOW_CITY',10);
ShowConst('SHOW_HEIGHT',11);
ShowConst('SHOW_WEIGHT',12);
/*ShowConst('SHOW_SEEKING_GENDER',13);*/
define('SHOW_SEEKING_GENDER', false);
ShowConst('SHOW_PHOTO',14);
ShowConst('SHOW_HORO',15);
ShowConst('SHOW_REG_DATE',16);
/*ShowConst('SHOW_LAST_ACCESS',17);*/
define('SHOW_LAST_ACCESS',true);
ShowConst('SHOW_RATING',18);
//ShowConst('SHOW_USERTYPE',19);
ShowConst('SHOW_ONLINE_STATUS',20);
/*ShowConst('SHOW_ZIP',21);*/
define('SHOW_ZIP', false);
define('ONLINE_STATUS',$w[352]);
define('ONLINE', $w[350]);
define('OFFLINE', $w[351]);
define('TOPS', false);
define('WHENSEEN', false);

$str=$color='';
$ids=$names=$genders=$purposes=$ages=$countries=$cities=$pictures=$colors=$pages=array();
$step = C_ONLINE_USERS_NUM;
if (!isset($from) || !is_numeric($from)) $from = 0;

$mgender=($gender < 1) ? '' : " AND t2.gender = '".$gender."'";
$mcountry=($country < 1) ? '' : " AND t2.country = '".$country."'";
$mcity=(trim($city) == '') ? '' : " AND t2.city = '".cb($city)."'";
$magef=($agef < 1) ? '' : " AND t2.birthday <= DATE_SUB(NOW(), INTERVAL ".$agef." YEAR)";
$maget=($aget < 1) ? '' : " AND t2.birthday >= DATE_SUB(NOW(), INTERVAL ".($aget+1)." YEAR)";
$musertype=($usertype < 1) ? '' : " AND t2.req >= '".$usertype."'";
$mpicture=($picture < 1) ? '' : " AND t2.files != ''";

$sql_dump = $mgender.$mcountry.$mcity.$magef.$maget.$musertype.$mpicture;

if (isset($_SESSION['search_date']) && $_SESSION['search_date'] < date("Y-m-d H:i:s")){
    unset($_SESSION['search_type']);    
    unset($_SESSION['search_date']);
    unset($_SESSION['search_data']);
}

if (!isset($_SESSION['search_data']) || (isset($_SESSION['search_type']) && $_SESSION['search_type'] != 'online')){
    $_SESSION['search_type'] = 'online';
    $_SESSION['search_data'] = array();
    $tmp0=db_query("SELECT t1.user as id FROM ".C_MYSQL_ONLINE_USERS." as t1, ".C_MYSQL_MEMBERS_MAIN." as t2 WHERE t1.user = t2.id AND t2.status > '5' AND t2.hide='0'".$sql_dump." order by t1.utime desc ") or die(db_error());
    while($i=db_fetch_array($tmp0)) {
        $_SESSION['search_data'][] = $i['id'];
    }
    $_SESSION['search_date'] = date("Y-m-d H:i:s", ( time() + 300 ) );
}
$search = $_SESSION['search_data'];

//$tmp0=db_query("SELECT count(*) as total FROM ".C_MYSQL_ONLINE_USERS." as t1, ".C_MYSQL_MEMBERS_MAIN." as t2 WHERE t1.user = t2.id AND t2.status > '5' AND t2.hide='0'".$sql_dump) or die(db_error());
//$cnt=db_fetch_assoc($tmp0);
//$count=$cnt['total'];#die($count);
$count = count($search);
$in = array_slice($search, $from, $step);
if (count($in)){
    $where_in = ' AND t1.user IN ('.implode(', ', $in).') ';
} else {
    $where_in = ' AND t1.user IS NULL ';
}
$tmp=db_query("SELECT DISTINCT t1.user, t1.upage, t2.*, t3.* FROM ".C_MYSQL_ONLINE_USERS." as t1, ".C_MYSQL_MEMBERS_MAIN." as t2, ".C_MYSQL_MEMBERS_EXT." as t3 WHERE t1.user = t2.id AND t2.id = t3.id AND t2.status > '5' AND t2.hide='0'".$sql_dump.$where_in." order by t1.utime desc ") or die(db_error());
$color='';
while($i=db_fetch_array($tmp)) {
	$pictures[] = SearchPhotoResult(IsAccess($sr[14]),$i['files'],$i['id'],$i['gender']);
	$color = ($color == COLOR4) ? COLOR3 : COLOR4;
	$colors[] = $color;
	$ids[]=$i['id'];
	$photoinfo[]=($i['files'] == '') ? $w[532] : template($w[533],array(PhotosNum($i['files'])));
	$names[] = username($i['id'],$i['username'],$i['fname'],$i['lname']);
	$fnames[] = (IsAccess($sr[2])) ? $i['fname'] : '';
	$lnames[] = (IsAccess($sr[3])) ? $i['lname'] : '';
	$genders[]=(IsAccess($sr[5])) ? $wg[$i['gender']] : '';
	$sgenders[]=(IsAccess($sr[13])) ? arr2val($wg, $i['sgender']) : '';
	$purposes[]=(IsAccess($sr[8])) ? arr2val($wp, $i['purposes']) : '';
	$orientations[]=(IsAccess($sr[7])) ? $wor[$i['orientation']] : '';
	$heights[]=(IsAccess($sr[11])) ? $wh[$i['height']] : '';
	$weights[]=(IsAccess($sr[12])) ? $ww[$i['weight']] : '';
	$childs[]= $wc[$i['child']];
	$horoscopes[]=(IsAccess($sr[15])) ? $whr[$i['horo']] : '';
	$bodytypes[]=(IsAccess($sr[6])) ? $wbt[$i['bodytype']] : '';
	$ages[]=(IsAccess($sr[4])) ? abs(mysql2data($i['birthday'],1)) : '';
	$countries[]=(IsAccess($sr[9])) ? $wcr[$i['country']] : '';
	$cities[]=(IsAccess($sr[10])) ? $i['city'] : '';
	$usertypes[]=(IsAccess($sr[19])) ? $wpu[$i['req']] : '';
	/*$editdates[]=(IsAccess($sr[17])) ? mysql2data($i['editdate']) : '';*/
	$editdates[]= mysql2data($i['editdate']);
	$regdates[]=(IsAccess($sr[16])) ? mysql2data($i['regdate']) : '';
	$rates[]=(IsAccess($sr[18])) ? $i['hits'] : '';
	$zips[]=(IsAccess($sr[21])) ? $i['zip'] : '';
}
$param='l='.$l;
$colspan=8;
	define('GENDER',$w[57]);
	$Value='';$p=0;while(isset($wg[$p])) {$str=(($gender!='Nothing') && ($gender == $p)) ? ' selected' : '';$Value .= '<option value="'.$p.'"'.$str.'>'.$wg[$p];$p++;}
	define('GENDER_VALUE',$Value);
	$Value='';
	$p = 0;asort($wcr);reset($wcr);while (list ($p, $val) = each ($wcr)) {$str=(($country!='Nothing') && ($country == $p)) ? ' selected' : '';$Value .= '<option value="'.$p.'"'.$str.'>'.$wcr[$p];}
	define('COUNTRY_VALUE',$Value);
	define('CITY_VALUE',$city);
	define('AGEF_VALUE',$agef);
	define('AGET_VALUE',$aget);
	define('USERTYPE',$w[270]);
	$Value='';$p=0;while(isset($wpu[$p])) {$str=(($usertype!='Nothing') && ($usertype == $p)) ? ' selected' : '';$Value .= '<option value="'.$p.'"'.$str.'>'.$wpu[$p];$p++;}
	define('USERTYPE_VALUE',$Value);
	define('SEARCH_USERS_WITH',$w[143]);
	$str= ($picture > 0 ) ? ' checked' : '';
	define('PHOTO_CHECKED',$str);
	
$hidden_users = online_users() - $count;
if($hidden_users){
define('HAVE_HIDDEN_USERS',True);
if(strlen($sql_dump) == '0') define('HIDDEN_USERS',template($w[746],array($hidden_users)));
else define('HIDDEN_USERS',template($w[752],array($hidden_users)));
} 
else {define('HAVE_HIDDEN_USERS',False);define('HIDDEN_USERS','');}

define('PAGES',pages($from,$step,$count,$param,$colspan));
define('SEARCH_RESULTS',search_results($from,$step,$count,IsAccess(C_SHOW_SEARCH_COUNT),USERS_ONLINE));
//define('USERNAME',$w[118]);
//define('PURPOSES',$w[119]);
//define('AGE',$w[120]);
//define('COUNTRY',$w[121]);
//define('CITY',$w[122]);
define('LAST_ACCESS',$w[123]);
define('REGISTER_DATE',$w[124]);
//define('PHOTO',$w[87]);
define('RATING',$w[216]);
define('FIRSTNAME',$w[127]);
define('LASTNAME',$w[128]);
//define('GENDER',$w[57]);
define('BODYTYPE',$w[525]);
define('ORIENTATION',$w[524]);
define('HEIGHT',$w[130]);
define('WEIGHT',$w[131]);
define('SEEKING_GENDER',$w[80]);
define('HOROSCOPE',$w[129]);
//define('USERTYPE',$w[270]);
define('ZIP',$w[285]);
define('ADDITIONAL_INFO',$w[534]);
define('WITH_SELECTED',$w[556]);
$p=0;


include_once C_PATH.'/templates/'.C_TEMP.'/online.php';

include_once C_PATH.'/templates/'.C_TEMP.'/search_results.php';

include_once C_PATH.'/footer.php';

?>