<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               remind.php                       #
# File purpose            Forget password script           #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';

include_once 'include/functions.inc.php';
include_once 'include/security.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';
include_once C_PATH.'/header.php';

if(!isset($_POST['p'])) $_POST['p']=''; 

  if ($_POST['p'] == "s" || $_POST['p'] == "u"){
      if (c_email($id) == 0) 
    {
      include_once C_PATH.'/header.php';
      printm($w[11]);
    }

    switch(C_ID)
    {
    case '0':
      $tmpvar = 't1.username,';
    break;
    case '1':
      $tmpvar = 't1.id,';
    break;
    default:
      $tmpvar='';      
    break;
    }
    
    $tmp=db_query("SELECT ".$tmpvar." t2.email, t2.password, t2.id FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_SEC." as t2 WHERE t1.status > '5' AND t2.email='".cbmail($id)."' AND t1.id = t2.id LIMIT 1");
    if(db_num_rows($tmp) == '0') printm($w[196]);
    $i=db_fetch_array($tmp);

    switch(C_ID)
    {
    case '0':
      $id = $i['username'];
    break;
    case '1':
      $id = $i['id'];
    break;
    default:
      $id= $i['email'];      
    break;
    }
	
	db_query("UPDATE ".C_MYSQL_MEMBERS_SEC." SET ip = '".ip2int(ip())."', fingerprint = '".$fingerprint."' WHERE id='".$i['id']."'") or die(db_error());

    //$tm=array(C_SNAME);
    //$subject=template($w[195],$tm);
    //$tm=array($id,$i['password'],C_SNAME);
    //$message=template($w[197],$tm);
    //sendmail(C_FROMM,$i['email'],$subject,nl2br($message),$message);

    do{
      $codegen = random_str(10);
      $temp = db_query("SELECT count(*) as total FROM ".C_MYSQL_TEMP." WHERE code='".$codegen."'");
      $row = db_fetch_array($temp);
      $count = $row['total'];
    }while($count > 0);
    
    if ($_POST['p'] == "s") 
    {
      $tm = array(C_URL.'/check.php?l='.$l.'&code=', $codegen, $i['username'], $i['email']);
      $html = template($wl['newpass_activation']['html'], $tm);
      $text = template($wl['newpass_activation']['text'], $tm);
      db_query("INSERT INTO ".C_MYSQL_TEMP." VALUES ('".$i['id']."',NOW(),'".$codegen."', 11)");
      sendmail(C_FROMM, $i['email'], $wl['newpass_activation']['subject'], $html, $text);
      printm($w[198],2);
    }else{
      $tm = array($i['username'], $i['email']);
      $html = template($wl['forgotten_username']['html'], $tm);
      $text = template($wl['forgotten_username']['text'], $tm);
      sendmail(C_FROMM, $i['email'], $wl['forgotten_username']['subject'], $html, $text);
      printm($w[880],2);
    }
    
  } else {

define('FORGOT_PASSWORD',$w[173]);
define('EMAIL',$w[60]);
define('SEND_PASSWORD',$w[200]);
define('FORGOT_USERNAME',$w[879]);
define('SEND_EMAIL',$w[217]);
define('ACTIVATION_CODE',$w[867]);
define('CODE',$w[498]);

include_once C_PATH.'/templates/'.C_TEMP.'/remind.php';

}

include_once C_PATH.'/footer.php';
?>