<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               top.php                          #
# File purpose            search profiles                  #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
define('REQ_COUNTRIES',True);
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
//include_once 'classes/cachequery.class.php';
include_once 'templates/'.C_TEMP.'/config.php';
IsSecure(C_TOP_MORE);
include_once C_PATH.'/header.php';

IsAccessToSeeThisPage();
if (!isset($pic)) $pic = 0;
$sr=explode(',',C_SR_COLUMNS);

if(C_HACK1) {
if(!is_numeric($step)||!is_numeric($from)||!is_numeric($pic)||!is_numeric($gender)) printm($w[1].'1',1);}

if(C_HACK2) { 
if(($gender < 0)||($gender >= sizeof($wg))) printm($w[1].'2',1);}

$cntr = 0;

///////// Checking and creating for search
$mid = " t1.status > '5' AND rate='1' AND hide='0' AND hits > 0";
$midp = " t1.status > '5' AND rate='1' AND hide='0' AND hits_count > ".C_RATE_MIN_HITS;
!empty($gender) ? $mgender=" AND gender = '".$gender."'" : $mgender = "";
!empty($pic) ? $mpic = " AND files != ''" : $mpic = "";

$tmp = (C_POPULARITY == 2) ? New CacheQuery("SELECT count(id) as total FROM ".C_MYSQL_MEMBERS_MAIN." as t1 WHERE ".$midp.$mgender.$mpic,IsCache(1,32)) : New CacheQuery("SELECT count(id) as total FROM ".C_MYSQL_MEMBERS_MAIN." WHERE ".$mid.$mgender.$mpic,IsCache(1,32));
$count = $tmp->data[0]['total'];
if($count == "0") printm($w[110]);

	### What to show
	$qage = (IsAccess($sr[4])) ? ',t1.birthday' : '';
	$qgender = (IsAccess($sr[5])) ? ',t1.gender' : '';
	$qbodytype = (IsAccess($sr[6])) ? ',t1.bodytype' : '';
	$qorientation = (IsAccess($sr[7])) ? ',t1.orientation' : '';
	$qpurpose = (IsAccess($sr[8])) ? ',t1.purposes' : '';
	$qcountry = (IsAccess($sr[9])) ? ',t1.country' : '';
	$qzip = (IsAccess($sr[21])) ? ',t1.zip' : '';
	$qcity = (IsAccess($sr[10])) ? ',t1.city' : '';
	$qheight = (IsAccess($sr[11])) ? ',t1.height' : '';
	$qweight = (IsAccess($sr[12])) ? ',t1.weight' : '';
	$qsgender = (IsAccess($sr[13])) ? ',t1.sgender' : '';
	$qhoro = (IsAccess($sr[15])) ? ',t1.horo' : '';
	$qregdate = (IsAccess($sr[16])) ? ',t1.regdate' : '';
	$qeditdate = (IsAccess($sr[17])) ? ',t1.editdate' : '';
	$qusertype = (IsAccess($sr[19])) ? ',t1.req' : '';
	if(IsAccess($sr[18])) {
		if(C_POPULARITY == 2) {
			$qrate =',ROUND(hits/hits_count,'.C_RATE_DIGITS.') as hits';
		} else {
			$qrate = ',ROUND(hits/(TO_DAYS(Now()) - TO_DAYS(IF(regdate > \''.C_LAST_CLEAR_DATE.'\', regdate, \''.C_LAST_CLEAR_DATE.'\')) + 1),'.C_RATE_DIGITS.') as hits';
		}
	}
	else $qrate='';
	$qchild = ',t2.child';
$sql0 = "SELECT t1.id,t1.username,t1.fname,t1.lname,t1.files".$qage.$qgender.$qchild.$qbodytype.$qorientation.$qpurpose.$qcountry.$qcity.$qzip.$qheight.$qweight.$qsgender.$qhoro.$qregdate.$qeditdate.$qusertype.$qrate."{0} FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2 WHERE t1.id = t2.id AND ".$mid.$mgender.$mpic." order by {1} hits DESC, hits_count DESC limit ".$from.",".$step;

$arr = array("","");

$sql = template($sql0,$arr);
if($gender) $tmp = ($gender == '1') ? New CacheQuery($sql,IsCache(1,1)) : New CacheQuery($sql,IsCache(1,2));
else $tmp = New CacheQuery($sql,IsCache(1,32));

$str='';$color='';
	$ids=$names=$photoinfo=$genders=$childs=$sgenders=$fnames=$lnames=$bodytypes=$orientations=$purposes=$ages=$countries=$cities=$heights=$weights=$horoscopes=$rates=$usertypes=$editdates=$regdates=$pictures=$colors=array();
	### What to show
	function ShowConst($var,$acs)
	{
		Global $sr;
		if(IsAccess($sr[$acs])) define($var,'1');
		else define($var,'0');
	}

	/*ShowConst('SHOW_FNAME',2);
	ShowConst('SHOW_LNAME',3);*/
	define('SHOW_FNAME',false);
	define('SHOW_LNAME', false);
	ShowConst('SHOW_AGE',4);
	ShowConst('SHOW_GENDER',5);
	define('CHILD', $w[135]);
	ShowConst('SHOW_BODYTYPE',6);
	ShowConst('SHOW_ORIENTATION',7);
	ShowConst('SHOW_PURPOSE',8);
	ShowConst('SHOW_COUNTRY',9);
	ShowConst('SHOW_CITY',10);
	ShowConst('SHOW_HEIGHT',11);
	ShowConst('SHOW_WEIGHT',12);
	/*ShowConst('SHOW_SEEKING_GENDER',13);*/
	define('SHOW_SEEKING_GENDER', false);
	ShowConst('SHOW_PHOTO',14);
	ShowConst('SHOW_HORO',15);
	ShowConst('SHOW_REG_DATE',16);
	/*ShowConst('SHOW_LAST_ACCESS',17);*/
	define('SHOW_LAST_ACCESS',true);
	ShowConst('SHOW_RATING',18);
	ShowConst('SHOW_USERTYPE',19);
	ShowConst('SHOW_ONLINE_STATUS',20);
	/*ShowConst('SHOW_ZIP',21);*/
	define('SHOW_ZIP', false);
	define('ONLINE_STATUS',$w[352]);
	define('ONLINE', $w[350]);
	define('OFFLINE', $w[351]);
	define('TOPS', true);

	while(list($k,$i)=@each($tmp->data)) {
		$pictures[] = SearchPhotoResult(IsAccess($sr[14]),$i['files'],$i['id'],$i['gender']);
		$color = ($color == COLOR4) ? COLOR3 : COLOR4;
		$colors[] = $color;
		$ids[]=$i['id'];
		$photoinfo[]=($i['files'] == '') ? $w[532] : template($w[533],array(PhotosNum($i['files'])));
		$names[] = username($i['id'],$i['username'],$i['fname'],$i['lname']);
		$fnames[] = (IsAccess($sr[2])) ? $i['fname'] : '';
		$lnames[] = (IsAccess($sr[3])) ? $i['lname'] : '';
		$genders[]=(IsAccess($sr[5])) ? $wg[$i['gender']] : '';
		$sgenders[]=(IsAccess($sr[13])) ? arr2val($wg, $i['sgender']) : '';
		$purposes[]=(IsAccess($sr[8])) ? arr2val($wp, $i['purposes']) : '';
		$orientations[]=(IsAccess($sr[7])) ? $wor[$i['orientation']] : '';
		$heights[]=(IsAccess($sr[11])) ? $wh[$i['height']] : '';
		$weights[]=(IsAccess($sr[12])) ? $ww[$i['weight']] : '';
		$horoscopes[]=(IsAccess($sr[15])) ? $whr[$i['horo']] : '';
		$bodytypes[]=(IsAccess($sr[6])) ? $wbt[$i['bodytype']] : '';
		$childs[]= $wc[$i['child']];
		$ages[]=(IsAccess($sr[4])) ? abs(mysql2data($i['birthday'],1)) : '';
		$countries[]=(IsAccess($sr[9])) ? $wcr[$i['country']] : '';
		$cities[]=(IsAccess($sr[10])) ? $i['city'] : '';
		$usertypes[]=(IsAccess($sr[19])) ? $wpu[$i['req']] : '';
		$editdates[]=(IsAccess($sr[17])) ? mysql2data($i['editdate']) : '';
		$regdates[]=(IsAccess($sr[16])) ? mysql2data($i['regdate']) : '';
		$rates[]=(IsAccess($sr[18])) ? $i['hits'] : '';
		$zips[]=(IsAccess($sr[21])) ? $i['zip'] : '';
	}


$param='l='.$l.'&pic='.$pic.'&gender='.$gender;
$colspan=1;
switch($gender)
{
	case 0:default:
		define('TOP_USERS_DESCRIPTION',str_replace('{0}','',$w[341]));
	break;
	case 1:
		define('TOP_USERS_DESCRIPTION',$w[512]);
	break;
	case 2:
		define('TOP_USERS_DESCRIPTION',$w[513]);
	break;
}	

$pages = (C_SEF) ? pages($from,$step,$count,$param,$colspan,null,null,null,array($gender,$pic),'Top') : pages($from,$step,$count,$param,$colspan);

define('PAGES',$pages);
define('SEARCH_RESULTS',search_results($from,$step,$count,IsAccess(C_SHOW_SEARCH_COUNT),TOP_USERS_DESCRIPTION));
	//define('SEARCH_RESULTS',search_results($from,$step,$count,IsAccess(C_SHOW_SEARCH_COUNT)));
	define('USERNAME',$w[118]);
	define('PURPOSES',$w[119]);
	define('AGE',$w[120]);
	define('COUNTRY',$w[121]);
	define('CITY',$w[122]);
	define('LAST_ACCESS',$w[123]);
	define('REGISTER_DATE',$w[124]);
	define('PHOTO',$w[87]);
	define('RATING',$w[216]);
	define('FIRSTNAME',$w[127]);
	define('LASTNAME',$w[128]);
	define('GENDER',$w[57]);
	define('BODYTYPE',$w[525]);
	define('ORIENTATION',$w[524]);
	define('HEIGHT',$w[130]);
	define('WEIGHT',$w[131]);
	define('SEEKING_GENDER',$w[80]);
	define('HOROSCOPE',$w[129]);
	define('USERTYPE',$w[270]);
	define('ZIP',$w[285]);
	define('ADDITIONAL_INFO',$w[534]);
	//define('WITH_SELECTED',$w[556]);
	define('WHENSEEN', false);
	$p=0;


include_once C_PATH.'/templates/'.C_TEMP.'/search_results.php';
include_once C_PATH.'/footer.php'; 
?>