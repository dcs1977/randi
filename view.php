<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               view.php                         #
# File purpose            View profiles                    #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';
define('VIEWMYPROFILE',True);
include_once 'include/functions.inc.php';

if (isset($id) && is_numeric($id))
{
	define('BANNED', CheckForIgnor($id, true));
	
	$result = db_query("SELECT t1.* ,".CorrectDate("regdate").", ".CorrectDate("editdate").", t2.*, t3.* FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2, ".C_MYSQL_MEMBERS_SEC." as t3 WHERE t1.id = '".$id."' AND t1.id = t2.id AND t1.id = t3.id") or die(db_error());
	$nick='';

	$count = db_num_rows($result);
	if ($count == '0')	printm($w[205]);
	while ($i = db_fetch_array($result))
	{
		if($i['status'] < '7')
		{
			if(C_VP_STAT)
			{
				switch($i['status'])
				{
					case '0': printm($w[205].': '.$wst[1]); break;
					case '1':case '10': printm($w[205].': '.$wst[2]); break;
					case '2':case '9': printm($w[205].': '.$wst[3]); break;
					case '3':case '8': printm($w[205].': '.$wst[4]); break;
					case '4': printm($w[205].': '.$w[764]); break;
					case '5': printm($w[205].': '.$wst[5]); break;
				}
			} else 	printm($w[205]);
		}


		//if(IsUserAccess(C_HIDE_VIEWPROF,$i['req']) && $i['hide'] == '1') printm($w[753]);
		define('HIDDEN',(IsUserAccess(C_HIDE_VIEWPROF,$i['req']) && $i['hide'] == '1'));


		include_once 'templates/'.C_TEMP.'/config.php';
		IsSecure(C_VIEW);

		//if (isset($i)) {while(list($namep,$valuep) = each($i)) {if(!is_numeric($namep)) {define('PROFILE_'.strtoupper($namep), $valuep);};};};
		$pictures = $photos = array();
		$not_shown=0;
		if($id == "0") $id = $i['id'];
		if (IsAccess(C_PHOTOSH))
		{
			define('SHOW_PHOTOS', '1');
		}
		else
		{
			define('SHOW_PHOTOS', '0');
		}
		if(C_USE_GALLERY && C_DONT_SHOW_CHANGED_PHOTOS) {
			$rslt = db_query("SELECT * FROM ".C_MYSQL_NEW_PHOTOS." WHERE uid = '".$id."' limit 1 ") or die(db_error());
			$depr=array();
			while ($j = db_fetch_array($rslt))
			{
				if($j['photonum'] != "") $depr= explode(',',$j['photonum']);
			}
			$not_shown = count($depr);
		}
		if (BANNED || HIDDEN){
			$i['files'] = "";
		}
		
		if ($i['files'] != "")
		{
			$arr=explode(',',$i['files']);
			$p=0;while(isset($arr[$p])) {
				$pictures[] = ShowPic(PicPath($arr[$p],$i['id']));
				$photos[] = ShowPicView(PicPath($arr[$p],$i['id']),$p,$i['id']);
				$p++;}
		}

		define('PICTURES_NUMBER',count($pictures));
		define('TOTAL_PHOTOS',$w[506]);
		define('SEE_ANOTHER',$w[507]);
		if($not_shown)	define('IS_NOT_CHECKED',template($w[522],array($not_shown)));
		else define('IS_NOT_CHECKED','');


		$PrivUser = 0;

		if (IsAccess(C_PP_SEE))
		{
			$sqlp = db_query("SELECT count(*) as total FROM ".C_MYSQL_PRIVATE_PHOTOS." WHERE uid = '".$id."' AND status > '5'");
			$trows = db_fetch_array($sqlp);
			$countp = $trows['total'];
			define('PRIVATE_PHOTOS_NUM',$countp);
			define('PRIVATE_PHOTOS_TOTAL',template($w[681],array($countp)));
		}	else {
			define('PRIVATE_PHOTOS_NUM','0');
			define('PRIVATE_PHOTOS_TOTAL','0');
		}

		define('ID', $i['id']);
		define('PROFILE', $w[207]);
		define('USERNAME', $w[118]);
		define('FIRSTNAME', $w[208]);
		define('LASTNAME', $w[209]);
		define('BIRTHDAY', $w[210]);
		define('HOROSCOPE', $w[129]);
		define('GENDER', $w[132]);
		define('BODYTYPE', $w[525]);
		define('ORIENTATION', $w[524]);
		define('PURPOSES', $w[133]);
		define('COUNTRY', $w[121]);
		define('EMAIL', $w[211]);
		define('URL', $w[144]);
		define('ICQ', $w[145]);
		define('AIM', $w[146]);
		define('PHONE', $w[147]);
		define('ZIP', $w[285]);
		define('STATE', $w[527]);
		define('CITY', $w[122]);
		define('ADDRESS', $w[378]);
		define('MARSTAT', $w[134]);
		define('CHILD', $w[135]);
		define('HEIGHT', $w[130]);
		define('WEIGHT', $w[131]);
		define('HCOLOR', $w[136]);
		define('ECOLOR', $w[137]);
		define('ETHNICITY', $w[138]);
		define('RELIGION', $w[139]);
		define('SMOKE', $w[140]);
		define('DRINK', $w[141]);
		define('EDUCATION', $w[142]);
		define('KNOWN_LANGUAGES', $w[382]);
		define('INTERESTS', $w[526]);
		define('JOB', $w[213]);
		define('HOBBY', $w[214]);
		define('DESCR', $w[215]);
		define('SCOUNTRY', $w[444]);
		define('SMARSTAT', $w[445]);
		define('SCHILD', $w[446]);
		define('SHCOLOR', $w[447]);
		define('SECOLOR', $w[448]);
		define('SSMOKE', $w[449]);
		define('SDRINK', $w[450]);
		define('SEDUCATION', $w[451]);
		define('SGENDER', $w[80]);
		define('SETHNICITY', $w[81]);
		define('SRELIGION', $w[82]);
		define('SAGE', $w[83]);
		define('SHEIGHT', $w[84]);
		define('SWEIGHT', $w[85]);
		define('VIEWS', $w[365]);
		define('REGDATE', $w[124]);
		define('ACCDATE', $w[123]);
		define('ONLINESTAT', $w[352]);
		define('USERTYPE', $w[270]);
		define('POPULARITY', $w[216]);
		define('AUDIO', $w[296]);
		define('RATE', $w[424]);
		define('WHO_SEEN', $w[428]);
		define('USER_FAVORITES', $w[664]);
		define('LINKED_PROFILE', $w[806]);
		define('LINKED_PROFILE_TXT', $i['link_txt']);
		define('AGE', $w[120]);
		define('DATE', $w[229]);
		$cnt = count($pictures);
		if(IsAccess(C_SLIDESHOW) && PICTURES_NUMBER >= C_SLIDESHOW_PHOTOS_NUM)
		{
			define('SLIDESHOW_SHOW', 1);
			define('SLIDESHOW', $w[598]);
		}
		else
		{
			define('SLIDESHOW_SHOW', 0);
			define('SLIDESHOW', '');

		}

		/*
		$Mdata - MySQL profile data
		$AltCond - Alternate show access options
		$AccessConst - Base show access options
		$ShowConst - Constant name for field description
		$ValueConst - Constant name for field value
		$Rdata - Redesigned MySQL profile data
		$Points - points number require to see this field
		$arr - Array from lang_.php, if value should be an element of array
		*/
		define('MULTIPLY',1);

		function WhatIsShow($Mdata, $AltCond, $AccessConst, $ShowConst, $ValueConst, $Rdata, $Fnum=null, $arr=null)
		{
			Global $w, $wpu, $l;
			$Points = explode(',',C_POINT_FIELDS);
			if (!empty($Mdata) && (IsAccess($AccessConst)) || $AltCond)
			{
				if (!defined($ShowConst)) define($ShowConst, '1');
				if($arr == null) define($ValueConst, $Rdata);
				else
				{
					if(!strpos($Rdata,',')) define($ValueConst, $arr[$Rdata]);
					else define($ValueConst, arr2val($arr, $Rdata));
				}
			}
			elseif(IsAccess(C_WHO_CAN_SHOW) && isset($wpu[$AccessConst]) && !empty($Mdata))
			{
				if (!defined($ShowConst)) define($ShowConst, '1');
				$goto = (GetServerConstant("PHP_SELF") && GetServerConstant("QUERY_STRING")) ? '&goto='.GetServerConstant("PHP_SELF").'?'.htmlentities(urlencode(GetServerConstant("QUERY_STRING"))) :
				'';
				$url = ($AccessConst == '0') ? C_URL.'/login.php?l='.$l.$goto :	C_URL.'/members/index.php?l='.LANGUAGE.'&a=r';
				define($ValueConst, template($w[384], array('<a href="'.$url.'">'.$wpu[$AccessConst].'</a>')));

			}
			else
			{
				if (!defined($ShowConst)) define($ShowConst, '0');
				define($ValueConst, '');
			}

		}

		function ReturnValues($Mdata, $Priv, $Const)
		{
			Return (!empty($Mdata) && ($Priv) && ($Const)) ? True :	False;
		}

		$langlist = '';
		$p = 0;
		if (!empty($i['languages']))
		{
			$AllValues = explode(',', $i['languages']);
			while (isset($AllValues[$p]))
			{
				$langlist .= $wlng[$AllValues[$p]].', ';
				$p++;
			}
		}

		$intlist = '';
		$p = 0;
		if (!empty($i['interests']))
		{
			$AllValues = explode(',', $i['interests']);
			while (isset($AllValues[$p]))
			{
				$intlist .= $win[$AllValues[$p]].', ';
				$p++;
			}
		}

		if (IsAccess(C_ONLINESTAT))	$cnt = (IsOnline($id)) ? $w[350] :	$w[351];
		else $cnt = '';

		if (C_VIEWS != '256') db_query("UPDATE ".C_MYSQL_MEMBERS_SEC." SET prv = prv + 1 WHERE id = '".$id."' AND status > '5'");

		if (C_AUDIO_AUTOSTART) define('AUTOSTART', 'true');
		else define('AUTOSTART', 'false');

		define('NAME_VALUE',htmlentities(urlencode(username($i['id'],$i['username'],$i['fname'],$i['lname']))));
		WhatIsShow($i['username'], 0, C_USERNAMESH, 'USERNAME_SHOW', 'USERNAME_VALUE', $i['username'],0);
		WhatIsShow($i['fname'], 0, C_FIRSTSH, 'FIRSTNAME_SHOW', 'FIRSTNAME_VALUE', $i['fname'],1);
		WhatIsShow($i['lname'], 0, C_LASTSH, 'LASTNAME_SHOW', 'LASTNAME_VALUE', $i['lname'],2);
		WhatIsShow($i['birthday'], 0, C_BIRTHDAYSH, 'BIRTHDAY_SHOW', 'BIRTHDAY_VALUE', mysql2data($i['birthday'],null,null,!IsAccess(C_BIRTHDAY_WITH_DAY)),3);
		WhatIsShow($i['birthday'], 0, -1, 'AGE_SHOW', 'AGE_VALUE', abs(mysql2data($i['birthday'],1)),3);
		WhatIsShow($i['horo'], 0, C_HOROSH, 'HOROSCOPE_SHOW', 'HOROSCOPE_VALUE', $whr[$i['horo']],3);
		WhatIsShow($i['gender'], 0, C_GENDERSH, 'GENDER_SHOW', 'GENDER_VALUE', $wg[$i['gender']],4);
		WhatIsShow($i['bodytype'], 0, C_BODYTYPESH, 'BODYTYPE_SHOW', 'BODYTYPE_VALUE', $wbt[$i['bodytype']],51);
		WhatIsShow($i['orientation'], 0, C_ORIENTATIONSH, 'ORIENTATION_SHOW', 'ORIENTATION_VALUE', $wor[$i['orientation']],50);
		WhatIsShow($i['purposes'], 0, C_PURPOSESH, 'PURPOSES_SHOW', 'PURPOSES_VALUE', $i['purposes'],5,$wp);
		WhatIsShow($i['country'], 0, C_COUNTRYSH, 'COUNTRY_SHOW', 'COUNTRY_VALUE', $wcr[$i['country']],6);
		WhatIsShow($i['email'], 0, C_EMAILSH, 'EMAIL_SHOW', 'EMAIL_VALUE', crm($i['email'], $w[212]),7);
		WhatIsShow($i['url'], 0, C_URLSH, 'URL_SHOW', 'URL_VALUE', checkurl($i['url']),8);
		WhatIsShow($i['icq'], 0, C_ICQSH, 'ICQ_SHOW', 'ICQ_VALUE', $i['icq'],9);
		WhatIsShow($i['aim'], 0, C_AIMSH, 'AIM_SHOW', 'AIM_VALUE', $i['aim'],10);
		WhatIsShow($i['phone'], 0, C_PHONESH, 'PHONE_SHOW', 'PHONE_VALUE', $i['phone'],11);
		WhatIsShow($i['state'], 0, C_STATESH, 'STATE_SHOW', 'STATE_VALUE', $i['state'],53);
		WhatIsShow($i['zip'], 0, C_ZIPSH, 'ZIP_SHOW', 'ZIP_VALUE', $i['zip'],12);
		WhatIsShow($i['link'], 0, C_PROF_LNKSH, 'LINK_SHOW', 'LINK_VALUE', $i['link'],55);
		WhatIsShow($i['city'], 0, C_CITYSH, 'CITY_SHOW', 'CITY_VALUE', $i['city'],13);
		WhatIsShow($i['address'], 0, C_ADDRESSSH, 'ADDRESS_SHOW', 'ADDRESS_VALUE', $i['address'],14);
		WhatIsShow($i['marstat'], 0, C_MARSTATSH, 'MARSTAT_SHOW', 'MARSTAT_VALUE', $wm[$i['marstat']],15);
		WhatIsShow($i['child'], 0, C_CHILDSH, 'CHILD_SHOW', 'CHILD_VALUE', $wc[$i['child']],16);
		WhatIsShow($i['height'], 0, C_HEIGHTSH, 'HEIGHT_SHOW', 'HEIGHT_VALUE', $wh[$i['height']],17);
		WhatIsShow($i['weight'], 0, C_WEIGHTSH, 'WEIGHT_SHOW', 'WEIGHT_VALUE', $ww[$i['weight']],18);
		WhatIsShow($i['hcolor'], 0, C_HCOLORSH, 'HCOLOR_SHOW', 'HCOLOR_VALUE', $whc[$i['hcolor']],19);
		WhatIsShow($i['ecolor'], 0, C_ECOLORSH, 'ECOLOR_SHOW', 'ECOLOR_VALUE', $we[$i['ecolor']],20);
		WhatIsShow($i['etnicity'], 0, C_ETHNICITYSH, 'ETHNICITY_SHOW', 'ETHNICITY_VALUE', $wet[$i['etnicity']],21);
		WhatIsShow($i['religion'], 0, C_RELIGIONSH, 'RELIGION_SHOW', 'RELIGION_VALUE', $i['religion'],22, $wr);
		WhatIsShow($i['smoke'], 0, C_SMOKESH, 'SMOKE_SHOW', 'SMOKE_VALUE', $ws[$i['smoke']],23);
		WhatIsShow($i['drink'], 0, C_DRINKSH, 'DRINK_SHOW', 'DRINK_VALUE', $wd[$i['drink']],24);
		WhatIsShow($i['education'], 0, C_EDUCATIONSH, 'EDUCATION_SHOW', 'EDUCATION_VALUE', $wed[$i['education']],25);
		WhatIsShow($i['languages'], 0, C_LANGSH, 'LANG_SHOW', 'LANG_VALUE', substr($langlist, 0, -2),26);
		WhatIsShow($i['job'], 0, C_JOBSH, 'JOB_SHOW', 'JOB_VALUE', $i['job'],27);
		WhatIsShow($i['interests'], 0, C_INTERESTSSH, 'INTERESTS_SHOW', 'INTERESTS_VALUE', substr($intlist, 0, -2),52);
		WhatIsShow($i['hobby'], 0, C_HOBBYSH, 'HOBBY_SHOW', 'HOBBY_VALUE', $i['hobby'],28);

		include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';
		WhatIsShow($i['descr'], 0, C_DESCRSH, 'DESCR_SHOW', 'DESCR_VALUE', AzDGCode($i['descr']),29);
		WhatIsShow($i['sgender'], 0, C_SGENDERSH, 'SGENDER_SHOW', 'SGENDER_VALUE', $i['sgender'],30,$wg);
		WhatIsShow($i['scountry'], 0, C_SCOUNTRYSH, 'SCOUNTRY_SHOW', 'SCOUNTRY_VALUE', $i['scountry'],31,$wcr);
		WhatIsShow($i['smarstat'], 0, C_SMARSTATSH, 'SMARSTAT_SHOW', 'SMARSTAT_VALUE', $i['smarstat'],32,$wm);
		WhatIsShow($i['schild'], 0, C_SCHILDSH, 'SCHILD_SHOW', 'SCHILD_VALUE', $i['schild'],33,$wc);
		WhatIsShow($i['shcolor'], 0, C_SHCOLORSH, 'SHCOLOR_SHOW', 'SHCOLOR_VALUE', $i['shcolor'],34,$whc);
		WhatIsShow($i['secolor'], 0, C_SECOLORSH, 'SECOLOR_SHOW', 'SECOLOR_VALUE', $i['secolor'],35,$we);
		WhatIsShow($i['ssmoke'], 0, C_SSMOKESH, 'SSMOKE_SHOW', 'SSMOKE_VALUE', $i['ssmoke'],36,$ws);
		WhatIsShow($i['sdrink'], 0, C_SDRINKSH, 'SDRINK_SHOW', 'SDRINK_VALUE', $i['sdrink'],37,$wd);
		WhatIsShow($i['seducation'], 0, C_SEDUCATIONSH, 'SEDUCATION_SHOW', 'SEDUCATION_VALUE', $i['seducation'],38,$wed);
		WhatIsShow($i['setnicity'], 0, C_SETHNICITYSH, 'SETHNICITY_SHOW', 'SETHNICITY_VALUE', $i['setnicity'],39,$wet);
		WhatIsShow($i['sreligion'], 0, C_SRELIGIONSH, 'SRELIGION_SHOW', 'SRELIGION_VALUE', $i['sreligion'],40,$wr);
		WhatIsShow($i['agef'], 0, C_SAGESH, 'SAGE_SHOW', 'SAGEF_VALUE', $i['agef'],41);
		WhatIsShow($i['aget'], 0, C_SAGESH, 'SAGE_SHOW', 'SAGET_VALUE', $i['aget'],41);
		WhatIsShow($i['heightf'], 0, C_SHEIGHTSH, 'SHEIGHT_SHOW', 'SHEIGHTF_VALUE', $wh[$i['heightf']],42);
		WhatIsShow($i['heightt'], 0, C_SHEIGHTSH, 'SHEIGHT_SHOW', 'SHEIGHTT_VALUE', $wh[$i['heightt']],42);
		WhatIsShow($i['weightf'], 0, C_SWEIGHTSH, 'SWEIGHT_SHOW', 'SWEIGHTF_VALUE', $ww[$i['weightf']],43);
		WhatIsShow($i['weightt'], 0, C_SWEIGHTSH, 'SWEIGHT_SHOW', 'SWEIGHTT_VALUE', $ww[$i['weightt']],43);
		WhatIsShow($i['audio'], 0, C_AUDIOSH, 'AUDIO_SHOW', 'AUDIO_VALUE', id2pathAudio($i['id']).'.'.$i['audio'],44);
		WhatIsShow($i['rate'], 0, C_POPULARITYSH, 'POPULARITY_SHOW', 'POPULARITY_VALUE', pop_out($i['id']),45);
		WhatIsShow($i['id'], 0, C_VIEWS, 'VIEWS_SHOW', 'VIEWS_VALUE', $i['prv'],46);
		WhatIsShow($i['regdate'], 0, C_REGDATE, 'REGDATE_SHOW', 'REGDATE_VALUE', mysql2data($i['regdate'], 0, 1),47);
		WhatIsShow($i['editdate'], 0, C_ACCDATE, 'ACCDATE_SHOW', 'ACCDATE_VALUE', mysql2data($i['editdate'], 0, 1),48);
		WhatIsShow($i['id'], 0, C_USERTYPE, 'USERTYPE_SHOW', 'USERTYPE_VALUE', $wpu[$i['req']],49);
		WhatIsShow($i['id'], 0, C_ONLINESTAT, 'ONLINESTAT_SHOW', 'ONLINESTAT_VALUE', $cnt,54);
		WhatIsShow('M', 0, C_OTHER_FAVORITES, 'FAVORITES_SHOW', 'FAVORITES_VALUE', '<a href="'.C_URL.'/favorites.php?l='.LANGUAGE.'&id='.ID.'">'.$w[663].'</a>');
	
		//Kuponkód!
		if (MyID()=="5461")
		{
			if (isset($_POST['kupon']))
			{
				$i['coupon'] = $_POST['kupon'];
				db_query("UPDATE ".C_MYSQL_MEMBERS_MAIN." SET coupon = '".$i['coupon']."' WHERE id='".$id."'") or die(db_error());
			}
			
			define('COUPON', $i['coupon']);
		}
		else
		{
			define('COUPON', false);
		}
		
		//Banlistán van?
		define('ON_BANLIST', $w[320]);
		
		$sqlban = db_query("SELECT count(*) as total FROM ".C_MYSQL_BANLIST." WHERE user = '".$id."' AND myid = '".MyID()."'");
		$brows = db_fetch_array($sqlban);
		$countban = $brows['total'];
		define('ON_MYBANLIST', $countban);
		
		#    if (IsAccess(C_SENDSH))
		#    {
        if($id != MyID()){
            define('SEND_SHOW', '1');
            define('SEND_MESSAGE', $w[514]);
        } else {
            define('SEND_SHOW', '0');
            define('SEND_MESSAGE', '');
        }

		#   }
		#  else
		#  {
		#    define('SEND_SHOW', '0');
		#    define('SEND_MESSAGE', '');
		#  }
		if (IsAccess(C_RATE) && (C_POPULARITY == '2') &&  ($i['rate'] == '1') && $id != MyID())
		{
			define('RATE_SHOW', '1');
		}
		else
		{
			define('RATE_SHOW', '0');
		}

		if (IsAccess(C_MAILSH) && is_numeric(get_info('m')) && $id != MyID())
		{
			define('MAIL_SHOW', '1');
			define('MAIL_MESSAGE', $w[217]);
		}
		else
		{
			define('MAIL_SHOW', '0');
			define('MAIL_MESSAGE', '');
		}
		if (IsAccess(C_BEDSH) && is_numeric(get_info('m')) && $id != MyID())
		{
			define('BED_SHOW', '1');
			define('BED_MESSAGE', $w[219]);
		}
		else
		{
			define('BED_SHOW', '0');
			define('BED_MESSAGE', '');
		}
		if (IsAccess(C_BANSH) && is_numeric(get_info('m')) && $id != MyID())
		{
			define('BAN_SHOW', '1');
			define('BAN_MESSAGE', $w[319]);
			define('UNBAN_MESSAGE', $w[864]);
		}
		else
		{
			define('BAN_SHOW', '0');
			define('BAN_MESSAGE', '');
			define('UNBAN_MESSAGE', $w[864]);
		}
		if (IsAccess(C_BADSH) && $id != MyID())
		{
			define('BAD_SHOW', '1');
			define('BAD_MESSAGE', $w[218]);
		}
		else
		{
			define('BAD_SHOW', '0');
			define('BAD_MESSAGE', '');
		}
		if (IsAccess(C_SEND_KISS) && ($i['nokiss'] == '0' || (IsAccess(C_SUPER_KISS) && $i['nokiss'] == '1')) && $id != MyID())
		{
			define('SEND_KISS', '1');
			define('SEND_KISS_MESSAGE', $w[398]);
		}
		else
		{
			define('SEND_KISS', '0');
			define('SEND_KISS_MESSAGE', '');
		}
		if($i['rate'] == '1')	{if (C_POPULARITY == '1') pop_in($i['id'], C_HITS_VIEW);}
	}

	### Who seen my profile
	/*if ((MyID() == $id && IsAccess(C_WHO_SEEN_OWN)) || IsAccess(C_WHO_SEEN_OTHER))
	{
		define('SHOW_VIEWERS', '1');
		if ($id != 0)
		{
			$total = db_fetch_array(db_query("SELECT count(*) as total FROM ".C_MYSQL_PROFILEVIEW." WHERE myid = '".$id."'"));
			$count = $total['total'];

			if ($count == '0') define('NOBODY_SEEN_PROFILE', $w[429]);
			else
			{
				if (!isset($step) || !is_numeric($step)) $step = C_WHO_SEEN_STEP;
				if (!isset($from) || !is_numeric($from)) $from = 0;
				$result = db_query("SELECT t1.*,".CorrectDate("visdate").",t2.id,t2.username,t2.fname,t2.lname,t2.birthday,t2.gender FROM ".C_MYSQL_PROFILEVIEW." as t1, ".C_MYSQL_MEMBERS_MAIN." as t2  WHERE myid = '".$id."' AND t1.visid = t2.id AND t1.visdate > DATE_SUB(NOW(), INTERVAL ".C_WHOSEENTIME." DAY) order by t1.visdate DESC limit ".$from.",".$step) or die(db_error());
				$thiscnt = db_num_rows($result);
				if ($thiscnt == '0') define('NOBODY_SEEN_PROFILE', $w[429]);
				else
				{
					$color='';
					$visid = array();
					$visdate = array();
					$names = array();
					$ages = array();
					$genders = array();
					$colors=array();
					while ($i = db_fetch_array($result))
					{
						$color = ($color == COLOR4) ? COLOR3 : COLOR4;
						$colors[] = $color;
						$visid[] = $i['visid'];
						$visdate[] = mysql2data($i['visdate'], 0, 1);
						$names[] = username($i['id'], $i['username'], $i['fname'], $i['lname']);
						$ages[] = abs(mysql2data($i['birthday'], 1));
						$genders[] = $wg[$i['gender']];
					}
					$param = 'l='.$l.'&id='.$id.'&nick='.$nick;
					$colspan = 4;
					define('PAGES', pages($from, $step, $count, $param, $colspan));
				}
			}
		}
	}
	else */
	    define('SHOW_VIEWERS', '0');
	### Who seen my profile ###

	$p = 0;
	$k = 1;

	if ((MyID() == $id && IsAccess(C_SEE_OWN_COMMENTS)) || IsAccess(C_SEE_ANOTHER_COMMENTS))
	{
		define('SHOW_COMMENTS', '1');
		include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';
		$total = db_fetch_array(db_query("SELECT count(*) as total FROM ".C_MYSQL_COMMENTS." WHERE toid = '".$id."'"));
		$count = $total['total'];

		if ($count == '0') define('NO_COMMENTS', $w[491]);
		else
		{

			$cstep = C_COMMENT_STEP;
			if (!isset($cfrom) || !is_numeric($cfrom)) $cfrom = 0;
			$cfromid = $cid = $cdate = $rdate = $cnames = $cages = $cgenders = $cphotos = $csubject = $ccomment = $creply = array();

			$result = db_query("SELECT *,".CorrectDate("cdate").",".CorrectDate("rdate")." FROM ".C_MYSQL_COMMENTS." WHERE toid = '".$id."' order by cdate DESC limit ".$cfrom.",".$cstep) or die(db_error());
			while ($i = db_fetch_array($result))
			{
				$cfromid[] = $i['fromid'];
				$cid[] = $i['id'];
				$csubject[] = $i['subject'];
				$ccomment[] = AzDGCode($i['comment']);
				$creply[] = AzDGCode($i['reply']);
				$cdate[] = mysql2data($i['cdate'], 0, 1);
				$rdate[] = mysql2data($i['rdate'], 0, 1);
			}
			$result = db_query("SELECT * FROM ".C_MYSQL_MEMBERS_MAIN." WHERE id IN (".implode(',',$cfromid).")") or die(db_error());

			#$result = db_query("SELECT t1.*,".CorrectDate("cdate").",".CorrectDate("rdate").",t1.id as cid,t2.id,t2.username,t2.fname,t2.lname,t2.birthday,t2.gender,t2.files FROM ".C_MYSQL_COMMENTS." as t1, ".C_MYSQL_MEMBERS_MAIN." as t2  WHERE (t1.toid = '".$id."' AND t1.fromid = t2.id) OR (t1.toid = '".$id."' AND t1.fromid = 0 AND t2.id is NULL) order by t1.cdate DESC limit ".$cfrom.",".$cstep) or die(db_error());
			#	$result = db_query("SELECT t1.*,".CorrectDate("cdate").",".CorrectDate("rdate").",t1.id as cid,t2.id,t2.username,t2.fname,t2.lname,t2.birthday,t2.gender,t2.files FROM ".C_MYSQL_COMMENTS." as t1, ".C_MYSQL_MEMBERS_MAIN." as t2  WHERE t1.toid = '".$id."' AND t1.fromid = t2.id order by t1.cdate DESC limit ".$cfrom.",".$cstep) or die(db_error());
			while ($i = db_fetch_array($result))
			{
				$cphotos[intval($i['id'])] = SearchPhotoResult(IsAccess(C_COMMENT_PHOTO),$i['files'],$i['id'],$i['gender']);
				$cnames[intval($i['id'])] = ($i['id']) ? username($i['id'], $i['username'], $i['fname'], $i['lname']) : $w[354];
				$cages[intval($i['id'])] = abs(mysql2data($i['birthday'], 1));
				$cgenders[intval($i['id'])] = $wg[$i['gender']];
			}
			$cphotos[0] = "";
			$cnames[0] = $w[354];
			$cages[0] = '';
			$cgenders[0] = $wg[0];


			$param = 'l='.$l.'&id='.$id.'&nick='.$nick;
			$colspan = 4;
			if($count > $cstep)	define('CPAGES', pages($cfrom, $cstep, $count, $param, $colspan,'c',null,C_URL.'/view.php'));
			else 	define('CPAGES', 0);
		}

	}
	else define('SHOW_COMMENTS', '0');

	include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';
	define('COMMENTS',$w[492]);
	define('ADD_COMMENTS',$w[489]);
	define('SUBJECT',$w[168]);
	define('MESSAGE',$w[169]);
	define('SUBJECT_ERROR',$w[166]);
	define('MESSAGE_ERROR',$w[167]);
	define('REPLY',$w[234]);
	define('DELETE',$w[297]);
	define('WARNING',$w[494]);
	if(IsAccess(C_REPLY_COMMENTS) && MyID() == $id) define('SHOW_REPLY_LINK','1');
	else  define('SHOW_REPLY_LINK','0');
	if(IsAccess(C_REMOVE_COMMENTS) /*&& MyID() == $id*/) define('SHOW_REMOVE_LINK','1');
	else  define('SHOW_REMOVE_LINK','0');
	if(IsAccess(C_ADD_COMMENTS) && MyID() != $id) define('SHOW_COMMENTS_FORM','1');
	else define('SHOW_COMMENTS_FORM','0');

	$p = 0;
	$k = 1;

	define('C_TITLE',template(C_VIEW_TITLE, array('USERNAME'=>cb(USERNAME_VALUE), 'FIRSTNAME'=>cb(FIRSTNAME_VALUE), 'LASTNAME'=>cb(LASTNAME_VALUE),'AGE'=>cb(AGE_VALUE), 'HOROSCOPE'=>cb(HOROSCOPE_VALUE), 'COUNTRY'=>cb(COUNTRY_VALUE), 'CITY'=>cb(CITY_VALUE), 'MARSTAT'=>cb(MARSTAT_VALUE), 'RELIGION'=>cb(RELIGION_VALUE), 'JOB'=>cb(JOB_VALUE), 'HOBBY'=>cb(HOBBY_VALUE), 'DESCRIPTION'=>str_replace("<br>","",cb(strip_tags(DESCR_VALUE))))));
	define('C_KEYWORDS',template(C_VIEW_KEYWORDS, array('USERNAME'=>cb(USERNAME_VALUE), 'FIRSTNAME'=>cb(FIRSTNAME_VALUE), 'LASTNAME'=>cb(LASTNAME_VALUE),'AGE'=>cb(AGE_VALUE),  'HOROSCOPE'=>cb(HOROSCOPE_VALUE), 'COUNTRY'=>cb(COUNTRY_VALUE), 'CITY'=>cb(CITY_VALUE), 'MARSTAT'=>cb(MARSTAT_VALUE), 'RELIGION'=>cb(RELIGION_VALUE), 'JOB'=>cb(JOB_VALUE), 'HOBBY'=>cb(HOBBY_VALUE), 'DESCRIPTION'=>str_replace("<br>","",cb(strip_tags(DESCR_VALUE))))));
	define('C_DESCRIPTION',template(C_VIEW_DESCRIPTION, array('USERNAME'=>cb(USERNAME_VALUE), 'FIRSTNAME'=>cb(FIRSTNAME_VALUE), 'LASTNAME'=>cb(LASTNAME_VALUE),'AGE'=>cb(AGE_VALUE),  'HOROSCOPE'=>cb(HOROSCOPE_VALUE), 'COUNTRY'=>cb(COUNTRY_VALUE), 'CITY'=>cb(CITY_VALUE), 'MARSTAT'=>cb(MARSTAT_VALUE), 'RELIGION'=>cb(RELIGION_VALUE), 'JOB'=>cb(JOB_VALUE), 'HOBBY'=>cb(HOBBY_VALUE), 'DESCRIPTION'=>str_replace("<br>","",cb(strip_tags(DESCR_VALUE))))));

	if (IsAccess(C_VERIMG_COMMENT)) // NOT
	{
		define('VERIFY_ALLOW', '0');
		define('VERIFY_ERROR', '');
	}
	else
	{
		define('VERIFY_ALLOW', '1');
		define('VERIFY_ERROR', $w[497]);
	}
	define('VERIFICATION', $w[498]);
	
	include_once C_PATH.'/header.php';
	BBCheck(C_BB_PCOM_ACL);
	include_once C_PATH.'/templates/'.C_TEMP.'/view.php';
	AddToViewProfile($id, MyID());
	include_once C_PATH.'/footer.php';

}
else
{
	DefineTemplate();
	include_once 'templates/'.C_TEMP.'/config.php';
	IsSecure(C_VIEW);
	printm($w[1].'1',2);
}
?>