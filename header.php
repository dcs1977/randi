<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               header.php                       #
# File purpose            Header for love template         #
# File created by         AzDG <support@azdg.com>          #
############################################################


function show_banners($banner_name=null,$BanRep=null,$pcnt=null) {
	$ShowBan = True;
	if($BanRep !== null && $pcnt !== null)
	{
		if(($pcnt > 0) && ($pcnt % $BanRep == 0)) $ShowBan = True;
		else $ShowBan = False;
		
	}
	if(C_BANNER_ALLOW && $ShowBan) {
		global $l,$w;
		if($banner_name == null) include C_PATH.'/templates/'.C_TEMP.'/banner.php';
		else 
		{
			if(file_exists(C_PATH.'/templates/'.C_TEMP.'/banner_'.$banner_name.'.php')) include C_PATH.'/templates/'.C_TEMP.'/banner_'.$banner_name.'.php';
		}
	}
}

function show_languages() {
	if(C_SHOW_LANG > '1') {
		global $l,$w,$reallangname;
		include_once C_PATH.'/include/languages.inc.php';
	}
}

function CountUsersPerPage($page) {
	Global $_OnlineChatIDs,$_OnlineForumIDs,$_OnlineSearchIDs,$_OnlineMainIDs,$_OnlineFAQIDs,$_OnlineMembersIDs,$_OnlineLinksIDs,$_OnlineStatIDs,$_OnlineSubscrIDs;	
	if(IsAccess(C_WHO_IS_WHERE) && isset(${$page})  && count(${$page})) {
		define($page,count(${$page}));
		Return True;
	} else return False;	
}

function DescriptionAndLinks($var,$url,$phrase)
{
	define($var,$phrase);
	define($var.'_LINK',$url);
}

define('MY_DATA',$w[456]);
$regurl = (!C_REGISTER_TYPE) ? C_URL.'/add.php?l='.$l : C_URL.'/register.php?l='.$l;

DescriptionAndLinks('MAIN_PAGE',C_URL.'/index.php?l='.$l,$w[88]);
DescriptionAndLinks('REGISTER',$regurl,$w[89]);
DescriptionAndLinks('LOGIN',C_URL.'/login.php?l='.$l,$w[90]);
DescriptionAndLinks('SEARCH',C_URL.'/advsearch.php?l='.$l,$w[91]);
DescriptionAndLinks('FORGOT_PASS',C_URL.'/remind.php?l='.$l,$w[866]);
DescriptionAndLinks('FEEDBACK',C_URL.'/feedback.php?l='.$l,$w[92]);
DescriptionAndLinks('FAQ',C_URL.'/faq.php?l='.$l,$w[93]);
DescriptionAndLinks('LINKS',C_URL.'/links.php?l='.$l,$w[376]);
DescriptionAndLinks('STATISTICS',GenLink('Stat',array($l)),$w[94]);
DescriptionAndLinks('MATCHMAKER',C_URL.'/matchmaker.php?l='.$l,$w[453]);
DescriptionAndLinks('VISITED_PROFILES',C_URL.'/members/index.php?l='.$l.'&a=vp',$w[510]);
DescriptionAndLinks('WHO_SEEN_MY_PROFILE',C_URL.'/members/index.php?l='.$l.'&a=ws',$w[428]);
DescriptionAndLinks('ALL_USERS',C_URL.'/all_users.php?l='.$l,$w[402]);
DescriptionAndLinks('VIEW_INBOX',C_URL.'/members/index.php?l='.$l.'&a=v',$w[860]);
DescriptionAndLinks('VIEW_OUTBOX',C_URL.'/members/index.php?l='.$l.'&a=sm',$w[861]);
DescriptionAndLinks('ADDRESS_BOOK',C_URL.'/members/index.php?l='.$l.'&a=adr',$w[557]);
DescriptionAndLinks('MY_FAVORITES',C_URL.'/members/index.php?l='.$l.'&a=b',$w[97]);
DescriptionAndLinks('BANLIST',C_URL.'/members/index.php?l='.$l.'&a=n',$w[317]);
DescriptionAndLinks('OPTIONS',C_URL.'/options.php?l='.$l,$w[422]);
DescriptionAndLinks('MY_PROFILE',C_URL.'/view.php?l='.$l.'&id='.MyID().'&MyProfile',$w[98]);
DescriptionAndLinks('CHANGE_PROFILE',C_URL.'/members/index.php?l='.$l.'&a=c',$w[99]);
DescriptionAndLinks('REMOVE_PROFILE',C_URL.'/members/index.php?l='.$l.'&a=h',$w[101]);
DescriptionAndLinks('CHANGE_PASSWORD',C_URL.'/members/index.php?l='.$l.'&a=p',$w[100]);
DescriptionAndLinks('TESTIMONIALS',C_URL.'/testimonials.php?l='.$l,$w[633]);
DescriptionAndLinks('TERMS_OF_USE',C_URL.'/terms.php?l='.$l,$w[707]);
DescriptionAndLinks('PRIVACY_STATEMENT',C_URL.'/privacy.php?l='.$l,$w[708]);

define('MEMBERS_MENU',$w[95]);
define('VIEW_MESSAGES',$w[96]);
define('CHANGE_DESIGN',$w[419]);

define('ONLINE_LINK',C_URL.'/online.php?l='.$l);
define('NEWS', $w[344]);

define('EXIT_LINK',C_URL.'/logout.php?l='.$l);
define('EXIT_FROM',$w[102]);


if(MyAccess() >= 0) define('MEMBER_TYPE',@$wpu[MyAccess()]);
else define('MEMBER_TYPE','');
define('MEMBER_NAME',get_info('n'));
if(!defined("USERS_ONLINE")) define('USERS_ONLINE',$w[105]);
if(C_HAVE_MESSAGE && MyID() > 0) {
	$count = UnreadedMessages(MyID());
	if($count > 0 && $count != '') {
		$tm=array($count);
		define('YOU_HAVE_NEW_MESSAGE',template($w[331],$tm));
		define('NEWMSG',$count);
		define('NEW_ICON',NewMsgIcon());
	}
}
if(!DEFINED("YOU_HAVE_NEW_MESSAGE")) {define('NEWMSG','0');define('NEW_ICON','');define('YOU_HAVE_NEW_MESSAGE','');}

echo @$cont;
if(!defined('C_TITLE')) define('C_TITLE',C_DEFAULT_TITLE);
if(!defined('C_KEYWORDS')) define('C_KEYWORDS',C_DEFAULT_KEYWORDS);
if(!defined('C_DESCRIPTION')) define('C_DESCRIPTION',C_DEFAULT_DESCRIPTION);

# Detecting Current url for language.
function ShowLng($lng)
{
	$cur = cb($_SERVER["PHP_SELF"]);
	$env = (GetServerConstant("QUERY_STRING") !== false) ? GetServerConstant("QUERY_STRING") : '';
	$qs = (!empty($env)) ? str_replace("l=".LANGUAGE,"?l=".$lng,str_replace("&l=".LANGUAGE,"&l=".$lng,$env)) : '?l='.$lng;
	return $cur.$qs;
}


if(C_NOTICES_TYPES != 0)
{
	$notices = (!isset($_COOKIE[C_LOGIN_COOKIE.'Notices']) || !preg_match("/^[0-9]|,$/", $_COOKIE[C_LOGIN_COOKIE.'Notices'])) ? '100,0,0' : $_COOKIE[C_LOGIN_COOKIE.'Notices'];
	$notarr=explode(',',$notices);
	$not_show=array();
	if($notarr[0] < C_NOTICES_FIELDS_PROC) $not_show[]=template($w[627],array($notarr[0],'<a href="'.C_URL.'/members/index.php?l='.LANGUAGE.'&a=c">','</a>'));
	if($notarr[1]) $not_show[]=template($w[628],array('<a href="'.C_URL.'/members/index.php?l='.LANGUAGE.'&a=c">','</a>'));
	if($notarr[2]) $not_show[]=template($w[629],array('<a href="'.C_URL.'/members/index.php?l='.LANGUAGE.'&a=c">','</a>'));
	if(count($not_show))
	{
		$fe=rand(0,sizeof($not_show)-1);
		define('NOTICE',$not_show[$fe]);
	} else define('NOTICE',False);
} else define('NOTICE',False);

include_once C_PATH.'/templates/'.C_TEMP.'/header.php';


if(defined("C_MAINTENANCE_MODE") && C_MAINTENANCE_MODE)
printm(C_MAINTENANCE_MESSAGE,2);

if(CheckAccess(C_DENY_IPS)) printm($w[380],2);
?>