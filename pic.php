<?php
@ob_start();$k=@ob_get_contents();
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               pic.php                          #
# File purpose            Show pictures                    #
# File created by         AzDG <support@azdg.com>          #
############################################################
error_reporting(0);

if (empty($_SERVER['HTTP_REFERER']) || (strpos($_SERVER['HTTP_REFERER'],'hotbull.hu') === FALSE)) 
     {
        $redirect='index.php'; 
        header("Location: $redirect");
     }

define('DONT_COUNT_ONLINE',True);
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/functions.inc.php';
include_once 'classes/thumbnail.class.php';
include_once 'classes/watermark.class.php';
include_once 'classes/crypt.class.php';
@ob_end_clean();

if(isset($_GET['c']))
{
	if(C_CRYPT_URL)
	{
		$cr64 = new AzDGCrypt(C_CRYPT_KEY);
		$Path = $cr64->decrypt($_GET['c']);
	}
	else
	{
		$Path = $_GET['c'];
	}
	$Path = str_replace('..', '', $Path);
	if (!file_exists(C_PATH.'/members/uploads/'.$Path)) exit('Security error - #12<br>No such file:'.$Path);
}
else exit('No input parameters');

function ShowImage($Pic)
{
	Global $Path;

	if(C_USE_WATERMARK && isset($_GET['bigger']))
	{
		$watermark = new watermark($Pic);
		$watermark->setPosition(C_WATERMARK_POSITION);
		if(!C_WATERMARK_TYPE)
		{
			$watermark->setFixedColor(C_WATERMARK_TEXT_COLOR);
			$wordphrase=C_WATERMARK_TEXT;
			if(strpos(C_WATERMARK_TEXT,'USERNAME') || strpos(C_WATERMARK_TEXT,'FNAME') || strpos(C_WATERMARK_TEXT,'LNAME') || strpos(C_WATERMARK_TEXT,'ID')) {
				$arrid = explode('_',substr(strrchr($Pic, '/'),1));
				$id = intval($arrid[0]);
				$result = db_query("SELECT fname,lname,id,username FROM ".C_MYSQL_MEMBERS_MAIN." WHERE id = '".$id."'") or die(db_error());
				while ($i = db_fetch_array($result))
				{
					$wordphrase=template($wordphrase,array('ID'=>intval($i['id']),'FNAME'=>$i['fname'],'LNAME'=>$i['lname'],'USERNAME'=>$i['username'],'C_URL'=>C_URL));
				}

			}

			$watermark->addWatermark($wordphrase, "TEXT");
		}
		else
		{
			$watermark->addWatermark(C_PATH.'/templates/'.C_TEMP.'/images/'.C_WATERMARK_IMAGE, "IMAGE");
		}
		$img = $watermark->getMarkedImage();
		header("Content-type: image/png");
		imagepng($img);die;
	}
	else // Show thumbnail if watermark don`t work
	{#die(C_URL."/members/uploads/".$Path);
		$thumb=new Thumbnail(C_PATH."/members/uploads/".$Path);
		if(C_TH_WIDTH) $thumb->Width(C_TH_WIDTH);
		if(C_TH_HEIGHT) $thumb->Height(C_TH_HEIGHT);
		if(C_TH_AUTO) $thumb->Auto(C_TH_AUTO);
		$thumb->Quality(C_TH_QUALITY);
		$thumb->ShowAsIs($Pic);
		die;
	}
}

if(isset($_GET['bigger']))
{
	if(!IsAccess(C_BIG_PHOTOSH))
	{
		if (!IsAdmin(1)) {
			include_once 'include/security.inc.php';
			include_once C_PATH.'/templates/'.C_TEMP.'/config.php';
			IsSecure(C_BIG_PHOTOSH,True);
		}
	}
	ShowImage(C_PATH.'/members/uploads/'.$Path);
}

switch(C_THUMBNAIL_USE)
{
	case '1':
		$thumb=new Thumbnail(C_PATH."/members/uploads/".$Path);
		if(C_TH_WIDTH) $thumb->Width(C_TH_WIDTH);
		if(C_TH_HEIGHT) $thumb->Height(C_TH_HEIGHT);
		if(C_TH_AUTO) $thumb->Auto(C_TH_AUTO);
		$thumb->Quality(C_TH_QUALITY);
		$thumb->Show();
		break;
	case '2':
		$FileName = 't' . GetFileName($Path);
		$FullPath=C_PATH."/members/uploads/".GetDirName($Path)."/".$FileName;
		if(!file_exists($FullPath))
		{
			$thumb=new Thumbnail(C_PATH."/members/uploads/".$Path,null,null,True);
			if(C_TH_WIDTH) $thumb->Width(C_TH_WIDTH);
			if(C_TH_HEIGHT) $thumb->Height(C_TH_HEIGHT);
			if(C_TH_AUTO) $thumb->Auto(C_TH_AUTO);
			$thumb->Quality(C_TH_QUALITY);
			$thumb->Save($FullPath);
			if(!file_exists($FullPath))
			{
				ShowImage(C_PATH."/members/uploads/".$Path);
			}
			else
			ShowImage($FullPath);
		}
		else
		{
			ShowImage($FullPath);
		}
		break;
	default:
		ShowImage(C_PATH.'/members/uploads/'.$Path);
		break;
}
?>