<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               vote/vote.php                    #
# File purpose            Vote form                        #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once '../include/config.inc.php';
include_once '../include/options.inc.php';
include_once '../include/security.inc.php';
include_once '../include/functions.inc.php';
include_once '../templates/'.C_TEMP.'/config.php';
include_once C_PATH.'/sheader.php';
IsSecure(C_VOTE);
///// Delete old IPs //////////
db_query("DELETE FROM ".C_MYSQL_VOTE_IPS." WHERE time < DATE_SUB(NOW(), INTERVAL ".C_TTR." HOUR)") or die(db_error());
///////////////////////////////


if (isset($id)&&(is_numeric($id)&&($id > 0)))
{
	if(!isset($vote)) sprintm($w['432']);
  $resultip = db_query("SELECT count(*) as total FROM ".C_MYSQL_VOTE_IPS." WHERE vid=".$id." AND ip='".ip2int(ip())."'") or die(db_error());
  $trows = db_fetch_array($resultip);
  $count = $trows['total'];
  if ($count != "0")
  {
		#User already voted
		mes(template($w[433],array(C_TTR)));
	}
	else
	{
    db_query("INSERT INTO ".C_MYSQL_VOTE_IPS." VALUES ('".$id."', '".ip2int(ip())."',NOW())") or die(db_error()); 
		$result = db_query("SELECT * FROM ".C_MYSQL_VOTES." WHERE id='".$id."'");
		$count=db_num_rows($result);
		if($count == '0') sprintm($w[434]);
    while ($i = db_fetch_array($result)) 
		{
			for ($k=0;$k<=count($vote)-1;$k++)
			{
				if(($vote[$k] < 0) || ($vote[$k] > ($i['num']-1))) sprintm($w[1].'18');
			}
    	if ($i['stype'] == '1')
    	{
      	$sql = "UPDATE ".C_MYSQL_VOTES." SET q".($vote[0]+1)."=q".($vote[0]+1)."+1 WHERE id=".$id;
        $i['q'.($vote[0]+1)]++;
      }
      else
      {
        $sql = 'UPDATE '.C_MYSQL_VOTES.' SET ';
        $firstc="1";
        for ($k=0;$k<=count($vote)-1;$k++)
        {
           $i['q'.($vote[$k]+1)]++;
           if ($firstc == "1")
           {
             $sql.='q'.($vote[$k]+1).'=q'.($vote[$k]+1).'+1';
             $firstc='0';
           }
           else
           {
             $sql.=", q".($vote[$k]+1)."=q".($vote[$k]+1)."+1";
           }
         }
     		 $sql.=' WHERE id='.$id;   
       }
     }
     db_query($sql) or die(db_error());
		 mes($w[435]);
	}	 
}
include_once C_PATH.'/sfooter.php';
?>

