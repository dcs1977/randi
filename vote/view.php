<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               vote/view.php                    #
# File purpose            View vote results                #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once '../include/config.inc.php';
include_once '../include/options.inc.php';
include_once '../include/security.inc.php';
include_once '../include/functions.inc.php';
include_once '../templates/'.C_TEMP.'/config.php';
include_once C_PATH.'/sheader.php';
IsSecure(C_VOTE);
if (isset($id)&&(is_numeric($id)))
{
	define('VOTE',$w[430]);
	$tmp = db_query("SELECT * FROM ".C_MYSQL_VOTES." WHERE id = '".$id."'");
  while ($i = db_fetch_array($tmp)) 
	{
    $total=0;
    for ($c=1;$c<=$i['num'];$c++)          
    {
      $mas[$c]=$i['q'.$c];
      $total+=$i['q'.$c];
    }
    $result2 = db_query("SELECT * FROM ".C_MYSQL_VOTE_QUESTIONS." WHERE id = '".$i['id']."' AND lang = '".cb(LANGUAGE)."' limit 1") or die(db_error());
    while ($j = db_fetch_array($result2)) 
		{
      $max=max($mas);
      // If no votes
      if ($max == "0")
      {
				sprintm($w[436]);
      }
      define('QUESTION', $j['question']);
			$result=$graphic=$votes=$proc=$text=array();
      for ($k=1;$k<=$i['num'];$k++)          
      {
        $g[$k] = $i['q'.$k] * C_GRAFLEN / $max; 
        $p[$k] = $i['q'.$k] * 100 / $total; 
        $g[$k] = sprintf ("%01.0f", $g[$k]);
        $p[$k] = sprintf ("%01.2f", $p[$k]);
        $rslt = $j['qtext'.$k]." [".$i['q'.$k]." users, ".$p[$k]."%]";
				$text[]=$j['qtext'.$k];
				$votes[]=$i['q'.$k];
				$proc[]=$p[$k];
        $result[] = $rslt;
        $graphic[] = $g[$k];
      }
      define('TOTAL', template($w[437],array($total)));
			define('VOTES',$w[438]);
    }            
  }
	$p=0;
include_once(C_PATH.'/templates/'.C_TEMP.'/vote/view.php');
include_once C_PATH.'/sfooter.php';
}
?>

