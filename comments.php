<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               comments.php                     #
# File purpose            Comments for user                #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once C_PATH.'/include/options.inc.php';
include_once C_PATH.'/include/security.inc.php';
include_once C_PATH.'/include/functions.inc.php';
include_once C_PATH.'/templates/'.C_TEMP.'/config.php';

include_once C_PATH.'/sheader.php';

if(isset($id) && is_numeric($id))
{

	/* Checking for ban */
	CheckForIgnor($id);


	/* Checking for allowing to send message to user with the same gender */
	IsAllowContact($id);

	if(isset($a) && $a == 'a') {
		if(!isset($message)) printm($w[1].'Message is empty');
		if(!IsAccess(C_VERIMG_COMMENT)) {
			$verifyimage = cb($verifyimage);
			if(strtoupper($verifyimage) != strtoupper(@$_SESSION['vc']))
			{
				include_once C_PATH.'/header.php';
				printm($w[497]);
			}
		}
	
		include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';
		$message=RemoveAzDGCode($message,C_BB_PCOM_ACL);
		if(empty($message)) printm($w[360],1);
				
		//$subject=cb($subject);
		$subject = '';
		$message=cb($message);

		if (C_BADWORD_COMMENTS)
		{
			if (C_BADWORDS_ERR)
			{
				if (is_badwords($subject) || is_badwords($message))
				sprintm(C_BADWORDS_MES);
			}
			elseif (C_BADWORDS_REP)
			{
				$message = replace_badwords($message);
				$subject = replace_badwords($subject);
			}
		}


		if(MyAccess() >= 0)
		{
			$CreatedBy = MyName();
			$uid = get_info('m');
		}
		else
		{
			$CreatedBy = $w[361];
			$uid='0';
		}

		db_query("INSERT INTO ".C_MYSQL_COMMENTS." VALUES (Null,'".$id."','".$uid."','".$subject."','".$message."','',NOW(),'0','7')") or die(db_error());

		mes($w[490]);
		ChangeURL(C_URL."/view.php?l=".$l."&id=".$id);
	}
	$p=0;
}
elseif (isset($reply) && IsAccess(C_REPLY_COMMENTS) && isset($cid) && is_numeric($cid)) {
	if(isset($a)) {

		$message=cb($message);
		if (C_BADWORD_COMMENTS)
		{
			if (C_BADWORDS_ERR)
			{
				if (is_badwords($message))
				sprintm(C_BADWORDS_MES);
			}
			elseif (C_BADWORDS_REP)
			{
				$message = replace_badwords($message);
			}
		}

		@db_query("UPDATE ".C_MYSQL_COMMENTS." SET reply = '".$message."', rdate = NOW() WHERE toid = '".MyID()."' AND id = '".$cid."'");
		sprintm($w[496]);

	} else {

		$tmp = db_query("SELECT *,".CorrectDate("cdate")." FROM ".C_MYSQL_COMMENTS." WHERE toid = '".MyID()."' AND id = '".$cid."'") or die(db_error());
		$thiscnt = db_num_rows($tmp);
		if ($thiscnt == '0') sprintm($w[1]);
		$cfromid = $cid = $cdate = $cnames = $cages = $cgenders = $cphotos = $csubject = $ccomment = $creply = '';
		include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';
		while ($ic = db_fetch_array($tmp))
		{
			$cfromid = $ic['fromid'];
			$cid = $ic['id'];
			$csubject = $ic['subject'];
			$ccomment = AzDGCode($ic['comment']);
			$creply = tb($ic['reply']);
			$cdate = mysql2data($ic['cdate'], 0, 1);
			if($ic['fromid'] == '0')
			{
				$cphotos = SearchPhotoResult(IsAccess(C_COMMENT_PHOTO),'','0','0');
				$cnames = $w[354];
				$cages = '';
				$cgenders = '';
			}
			else
			{
				$result = db_query("SELECT * FROM ".C_MYSQL_MEMBERS_MAIN." WHERE id = '".$cfromid."'") or die(db_error());
				while ($i = db_fetch_array($result))
				{

					$cphotos = SearchPhotoResult(IsAccess(C_COMMENT_PHOTO),$i['files'],$i['id'],$i['gender']);
					$cnames = username($i['id'], $i['username'], $i['fname'], $i['lname']);
					$cages = abs(mysql2data($i['birthday'], 1));
					$cgenders = $wg[$i['gender']];
				}
			}
		}
		define('MESSAGE',$w[169]);
		define('REPLY',$w[234]);
		include_once C_PATH.'/templates/'.C_TEMP.'/reply_comment.php';

	}
}
elseif (isset($remove) && IsAccess(C_REMOVE_COMMENTS) && isset($cid) && is_numeric($cid)) {
	//@db_query("DELETE FROM ".C_MYSQL_COMMENTS." WHERE id = '".$cid."' AND toid = '".MyID()."'");
	@db_query("DELETE FROM ".C_MYSQL_COMMENTS." WHERE id = '".$cid."' AND (toid = '".MyID()."' OR fromid = '".MyID()."')");
	sprintm($w[495]);
}

include_once C_PATH.'/sfooter.php';

?>