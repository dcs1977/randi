<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               advsearch.php                    #
# File purpose            Powerfull search profiles        #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
//include_once 'classes/cachequery.class.php';

include_once 'templates/'.C_TEMP.'/config.php';
IsSecure(C_QSEARCH);
if((isset($a) && !(isset($sid) && isset($cache))) && C_SEARCH_TIMEOUT)
{
	$diff = time() - get_info('search');
	if($diff < C_SEARCH_TIMEOUT) printm(template($w[597],array(C_SEARCH_TIMEOUT,C_SEARCH_TIMEOUT-$diff)));
	set_info('search',time());
}
include_once C_PATH.'/header.php';

if(!isset($a)) $a='';

# Detect access
$access=explode(',',C_SRCH);
$access[-1]='-1';

$sr=explode(',',C_SR_COLUMNS);

if ($a == "s")
{
	if(isset($sid) && isset($cache)) {
		if(!is_numeric($sid) || !is_md5($cache)) printm($w[1].'1');
		IsAccessToSeeThisPage();
		$tmp=db_query("SELECT * FROM ".C_MYSQL_SEARCH." WHERE id = '".$sid."' AND cache = '".$cache."' AND (uid = '".MyID()."' OR uid = 0) AND utype <= '".MyAccess()."' limit 1");
		$cnt = db_num_rows($tmp);
		if($cnt == 0) printm($w[530]);
		while ($i = db_fetch_array($tmp))
		{
			$count = $i['result'];
			$import= unserialize($i['search_data']);
		}
		list($gender,$sgender,$agef,$aget,$country,$purpose,$orientation,$bodytype,$interests,$horo,$id,$usr,$fname,$lname,$keywords,$state,$city,$zip,$heightf,$heightt,$weightf,$weightt,$marstat,$child,$hcolor,$ecolor,$etnicity,$religion,$smoke,$drink,$education,$langlist,$photo,$url,$icq,$aim,$phone,$online,$regin,$usertype,$sortby,$step,$purpose_oper,$interests_oper,$lang_oper,$distance) = $import;


	}
	else
	{
		IsAccessToSeeThisPage();
		
		$usertype = -1;
		$id = '';

		# Checking for bad symbols
		$keywords=$state=$phone=$url=$purpose_oper=$icq=$aim=$interests=$marstat=$child=$hcolor=$ecolor=$etnicity=$religion=$smoke=$drink=$education=$usr=$fname=$city=$lname=$horo=$orientation=$bodytype=$purpose=$heightf=$heightt=$weightf=$weightt=$purpose_oper=$interests_oper=$lang_oper='';
		$photo=ReturnSearchValue('photo','text',31);
		$online=ReturnSearchValue('online','text',32);
		$zip=ReturnSearchValue('zip','text',14);
		if(!ReturnSearchValue('distance','num',-1,0,1)) $distance = '';
		if(!isset($agef) || !is_numeric($agef) || ($agef < 0) || !IsSearch($agef,$access[6])) $agef = 0;
		if(!isset($aget) || !is_numeric($aget) || ($agef > 255) || !IsSearch($aget,$access[6])) $aget = 255;
		if($agef > $aget) printm($w[109]);

		$gender=ReturnSearchValue('gender','multinum',1,$wg);
		$sgender=ReturnSearchValue('sgender','num',0,$wg);
		$country=ReturnSearchValue('country','multinum',2,$wcr);
		$regin=ReturnSearchValue('regin','num',34,$wrg);
		$sortby=ReturnSearchValue('sortby','num',35);

		$langlist='';
		$intlist='';
		$export=stripslashes(serialize(array($gender,$sgender,$agef,$aget,$country,$purpose,$orientation,$bodytype,$interests,$horo,$id,$usr,$fname,$lname,$keywords,$state,$city,$zip,$heightf,$heightt,$weightf,$weightt,$marstat,$child,$hcolor,$ecolor,$etnicity,$religion,$smoke,$drink,$education,$langlist,$photo,$url,$icq,$aim,$phone,$online,$regin,$usertype,$sortby,$step,$purpose_oper,$interests_oper,$lang_oper,$distance)));

	}


	///////// Checking and creating for search
	$sh_ph_f = (C_SHOW_PHOTOS_FIRST) ? 't1.files DESC,' : '';
	if(!IsSearch($sortby,$access[35])) $sortby = 0;
	$privsf = (C_PRIV_SHOW_FIRST) ? 't1.req DESC,':'';
	switch ($sortby) {
		case "1":$msortby = " order by ".$privsf.$sh_ph_f." t1.editdate ASC";break;
		case "2":$msortby = " order by ".$privsf.$sh_ph_f." t1.fname DESC";break;
		case "3":$msortby = " order by ".$privsf.$sh_ph_f." t1.fname ASC";break;
		case "4":$msortby = " order by ".$privsf.$sh_ph_f." t1.birthday DESC";break;
		case "5":$msortby = " order by ".$privsf.$sh_ph_f." t1.birthday ASC";break;
		case "6":$msortby = " order by ".$privsf.$sh_ph_f." t1.height DESC";break;
		case "7":$msortby = " order by ".$privsf.$sh_ph_f." t1.height ASC";break;
		case "8":$msortby = " order by ".$privsf.$sh_ph_f." t1.weight DESC";break;
		case "9":$msortby = " order by ".$privsf.$sh_ph_f." t1.weight ASC";break;
		default:$msortby = " order by ".$privsf.$sh_ph_f." t1.editdate DESC";break;
	}

	$musertype = ($usertype >= '0' && IsSearch($usertype,$access[33])) ? " AND t1.req = '".$usertype."'" : $musertype = "";

	$mlanglist=$mintlist=$mpurpose='';
	if($purpose != '') {
		$purpose=explode(',',$purpose);$p=0;
		if($purpose_oper)
		{
			while (isset($purpose[$p])){$mpurpose.= ($purpose[$p] != 0) ? " AND FIND_IN_SET(".cb(C_MYSQL_CHARSET)."'".$purpose[$p]."',t1.purposes)": "";$p++;}
		} else {
			$mpurpose.=" AND (";
			while (isset($purpose[$p])){$mpurpose.= ($purpose[$p] != 0) ? "FIND_IN_SET(".cb(C_MYSQL_CHARSET)."'".$purpose[$p]."',t1.purposes) OR ": "";$p++;}
			$mpurpose=(strlen($mpurpose) > 10) ? substr($mpurpose,0,-4).")" : "";
		}
	}

	if(isset($online) && ($online == "on") && IsSearch($online,$access[32]))
	{
		$onlinesql = ' AND t1.id = t3.user';
		$onlinetable = ', '.C_MYSQL_ONLINE_USERS.' as t3';
	}
	else
	{
		$onlinesql = '';
		$onlinetable = '';
	}
	// Important /////////////
	$mid = ($id != "" && IsSearch($id,$access[8])) ? " t1.id = '".$id."' AND t1.hide='0' AND t1.status > '5'".$onlinesql : " t1.status > '5' AND t1.hide='0'".$onlinesql;
	//////////////////////////
	$magef = (!empty($agef) && IsSearch($agef,$access[6]) && is_numeric($agef) && ($agef >= 0)) ? " AND t1.birthday <= DATE_SUB(NOW(), INTERVAL ".$agef." YEAR)" : "";
	$maget = (!empty($aget) && IsSearch($aget,$access[6]) && is_numeric($aget) && ($aget <= 255) && ($aget != 255)) ? " AND t1.birthday >= DATE_SUB(NOW(), INTERVAL ".($aget+1)." YEAR)" : "";

	$mgender = (!empty($gender) && IsSearch($gender,$access[1])) ? " AND t1.gender IN (".$gender.")" : "";
	$msgender = (!empty($sgender) && IsSearch($sgender,$access[0])) ? " AND (FIND_IN_SET(".cb(C_MYSQL_CHARSET)."'".$sgender."',t1.sgender) OR t1.sgender = '')" : "";
	$mcountry = (!empty($country) && IsSearch($country,$access[2])) ? " AND t1.country IN (".$country.")" : "";
	$mregin = (!empty($regin) && IsSearch($regin,$access[34])) ? " AND t1.regdate > DATE_SUB(NOW(), INTERVAL ".$wrgv[$regin]." DAY)" : "";

	$mphoto = (isset($photo) && ($photo == "on") && IsSearch($photo,$access[31])) ? " AND t1.files != ''" : "";
	$mzip = (!empty($zip) && IsSearch($zip,$access[14])) ? " AND t1.zip = '".$zip."'" : "";
	$mdistance = '';
        
        if (isset($_SESSION['search_date']) && $_SESSION['search_date'] < date("Y-m-d H:i:s")){
            unset($_SESSION['search_type']);    
            unset($_SESSION['search_date']);
            unset($_SESSION['search_sortby']);
            unset($_SESSION['search_data']);
        }
        
        if (!isset($_SESSION['search_data']) || (isset($_SESSION['search_type']) && $_SESSION['search_type'] != 'qsearch')){
            $_SESSION['search_type'] = 'qsearch';
            $_SESSION['search_data'] = array();
            $_SESSION['search_sortby'] = $msortby;
            $tmp0=db_query("SELECT t1.id FROM ".C_MYSQL_MEMBERS_MAIN." as t1".$onlinetable." WHERE ".$mid.$magef.$maget.$mgender.$msgender.$mpurpose.$mcountry.$mphoto.$mzip.$mdistance.$mregin.$msortby) or die(db_error());
            while($i=db_fetch_array($tmp0)) {
                $_SESSION['search_data'][] = $i['id'];
            }
            $_SESSION['search_date'] = date("Y-m-d H:i:s", ( time() + 300 ) );
        }
        $search = $_SESSION['search_data'];

        $count = count($search);
        $in = array_slice($search, $from, $step);
        if (count($in)){
            $where_in = ' AND t1.id IN ('.implode(', ', $in).') ';
        } else {
            $where_in = ' AND t1.id IS NULL ';
        }
	
	/*if(!isset($sid)) {
		$tmp = New CacheQuery("SELECT count(*) as total FROM ".C_MYSQL_MEMBERS_MAIN." as t1".$onlinetable." WHERE ".$mid.$magef.$maget.$mgender.$msgender.$mpurpose.$mcountry.$mphoto.$mzip.$mdistance.$mregin,IsCache(1,1024));
		$count = $tmp->data[0]['total'];
	}*/
	if($count == "0") printm($w[110]);

	### What to show
	$qage = (IsAccess($sr[4])) ? ',t1.birthday' : '';
	$qgender = (IsAccess($sr[5])) ? ',t1.gender' : '';
	$qbodytype = (IsAccess($sr[6])) ? ',t1.bodytype' : '';
	$qorientation = (IsAccess($sr[7])) ? ',t1.orientation' : '';
	$qpurpose = (IsAccess($sr[8])) ? ',t1.purposes' : '';
	$qcountry = (IsAccess($sr[9])) ? ',t1.country' : '';
	$qcity = (IsAccess($sr[10])) ? ',t1.city' : '';
	$qheight = (IsAccess($sr[11])) ? ',t1.height' : '';
	$qweight = (IsAccess($sr[12])) ? ',t1.weight' : '';
	$qsgender = (IsAccess($sr[13])) ? ',t1.sgender' : '';
	$qhoro = (IsAccess($sr[15])) ? ',t1.horo' : '';
	$qregdate = (IsAccess($sr[16])) ? ',t1.regdate' : '';
	$qeditdate = (IsAccess($sr[17])) ? ',t1.editdate' : '';
	$qusertype = (IsAccess($sr[19])) ? ',t1.req' : '';
	$qzip = (IsAccess($sr[21])) ? ',t1.zip' : '';
	if(IsAccess($sr[18])) {
		if(C_POPULARITY == 2) {
			$qrate =',ROUND(hits/hits_count,'.C_RATE_DIGITS.') as hits';
		} else {
			$qrate = ',ROUND(hits/(TO_DAYS(Now()) - TO_DAYS(IF(regdate > \''.C_LAST_CLEAR_DATE.'\', regdate, \''.C_LAST_CLEAR_DATE.'\')) + 1),'.C_RATE_DIGITS.') as hits';
		}
	}
	else $qrate='';
	$qchild = ',t2.child';
        $msortby = $_SESSION['search_sortby'];

	//$tmp = New CacheQuery("SELECT t1.id,t1.username,t1.fname,t1.lname,t1.files".$qage.$qgender.$qchild.$qbodytype.$qorientation.$qpurpose.$qcountry.$qcity.$qzip.$qheight.$qweight.$qsgender.$qhoro.$qregdate.$qeditdate.$qusertype.$qrate." FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2".$onlinetable." WHERE t1.id = t2.id AND ".$mid.$magef.$maget.$mgender.$msgender.$mpurpose.$mcountry.$mphoto.$mzip.$mdistance.$mregin.$where_in,IsCache(1,1024));
        $query = "SELECT t1.id,t1.username,t1.fname,t1.lname,t1.files".$qage.$qgender.$qchild.$qbodytype.$qorientation.$qpurpose.$qcountry.$qcity.$qzip.$qheight.$qweight.$qsgender.$qhoro.$qregdate.$qeditdate.$qusertype.$qrate." FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2".$onlinetable." WHERE t1.id = t2.id AND ".$mid.$magef.$maget.$mgender.$msgender.$mpurpose.$mcountry.$mphoto.$mzip.$mdistance.$mregin.$where_in.$msortby;
        $result = db_query($query);
        $tmp = array();
        while ($i = db_fetch_array($result)){
            $tmp[] = $i;
        }

	$str='';$color='';

	if(!isset($sid)) {
		$cache=code_gen();
		db_query("INSERT INTO ".C_MYSQL_SEARCH." VALUES (Null,'".MyID()."','".MyAccess()."','".$cache."',NOW(),'".$count."','','".$export."','')");
		$sid = db_fetch_array(db_query("SELECT LAST_INSERT_ID() AS last_id"));
		$sid = $sid['last_id'];
	}


	$ids=$names=$photoinfo=$genders=$childs=$sgenders=$fnames=$lnames=$bodytypes=$orientations=$purposes=$ages=$countries=$cities=$heights=$weights=$horoscopes=$rates=$usertypes=$editdates=$regdates=$pictures=$colors=$zips=array();
	### What to show
	function ShowConst($var,$acs)
	{
		Global $sr;
		if(IsAccess($sr[$acs])) define($var,'1');
		else define($var,'0');
	}

	ShowConst('SHOW_FNAME',2);
	ShowConst('SHOW_LNAME',3);
	ShowConst('SHOW_AGE',4);
	ShowConst('SHOW_GENDER',5);
	define('CHILD', $w[135]);
	ShowConst('SHOW_BODYTYPE',6);
	ShowConst('SHOW_ORIENTATION',7);
	ShowConst('SHOW_PURPOSE',8);
	ShowConst('SHOW_COUNTRY',9);
	ShowConst('SHOW_CITY',10);
	ShowConst('SHOW_HEIGHT',11);
	ShowConst('SHOW_WEIGHT',12);
	ShowConst('SHOW_SEEKING_GENDER',13);
	ShowConst('SHOW_PHOTO',14);
	ShowConst('SHOW_HORO',15);
	ShowConst('SHOW_REG_DATE',16);
	ShowConst('SHOW_LAST_ACCESS',17);
	ShowConst('SHOW_RATING',18);
	ShowConst('SHOW_USERTYPE',19);
	ShowConst('SHOW_ONLINE_STATUS',20);
	ShowConst('SHOW_ZIP',21);
	define('ONLINE_STATUS',$w[352]);
	define('ONLINE', $w[350]);
	define('OFFLINE', $w[351]);
	define('TOPS', false);
	define('WHENSEEN', false);

	while(list($k,$i)=@each($tmp)) {
		$pictures[] = SearchPhotoResult(IsAccess($sr[14]),$i['files'],$i['id'],$i['gender']);
		$color = ($color == COLOR4) ? COLOR3 : COLOR4;
		$colors[] = $color;
		$ids[]=$i['id'];
		$photoinfo[]=($i['files'] == '') ? $w[532] : template($w[533],array(PhotosNum($i['files'])));
		$names[] = username($i['id'],$i['username'],$i['fname'],$i['lname']);
		$fnames[] = (IsAccess($sr[2])) ? $i['fname'] : '';
		$lnames[] = (IsAccess($sr[3])) ? $i['lname'] : '';
		$genders[]=(IsAccess($sr[5])) ? $wg[$i['gender']] : '';
		$sgenders[]=(IsAccess($sr[13])) ? arr2val($wg, $i['sgender']) : '';
		$purposes[]=(IsAccess($sr[8])) ? arr2val($wp, $i['purposes']) : '';
		$orientations[]=(IsAccess($sr[7])) ? $wor[$i['orientation']] : '';
		$heights[]=(IsAccess($sr[11])) ? $wh[$i['height']] : '';
		$weights[]=(IsAccess($sr[12])) ? $ww[$i['weight']] : '';
		$horoscopes[]=(IsAccess($sr[15])) ? $whr[$i['horo']] : '';
		$bodytypes[]=(IsAccess($sr[6])) ? $wbt[$i['bodytype']] : '';
		$childs[]= $wc[$i['child']];
		$ages[]=(IsAccess($sr[4])) ? abs(mysql2data($i['birthday'],1)) : '';
		$countries[]=(IsAccess($sr[9])) ? $wcr[$i['country']] : '';
		$cities[]=(IsAccess($sr[10])) ? $i['city'] : '';
		$usertypes[]=(IsAccess($sr[19])) ? $wpu[$i['req']] : '';
		$editdates[]=(IsAccess($sr[17])) ? mysql2data($i['editdate']) : '';
		$regdates[]=(IsAccess($sr[16])) ? mysql2data($i['regdate']) : '';
		$rates[]=(IsAccess($sr[18])) ? $i['hits'] : '';
		$zips[]=(IsAccess($sr[21])) ? $i['zip'] : '';
	}

	$param='l='.$l.'&a=s&sid='.$sid.'&cache='.$cache;
	$colspan=1;
	define('PAGES',pages($from,$step,$count,$param,$colspan));
	define('SEARCH_RESULTS',search_results($from,$step,$count,IsAccess(C_SHOW_SEARCH_COUNT)));
	define('USERNAME',$w[118]);
	define('PURPOSES',$w[119]);
	define('AGE',$w[120]);
	define('COUNTRY',$w[121]);
	define('CITY',$w[122]);
	define('LAST_ACCESS',$w[123]);
	define('REGISTER_DATE',$w[124]);
	define('PHOTO',$w[87]);
	define('RATING',$w[216]);
	define('FIRSTNAME',$w[127]);
	define('LASTNAME',$w[128]);
	define('GENDER',$w[57]);
	define('BODYTYPE',$w[525]);
	define('ORIENTATION',$w[524]);
	define('HEIGHT',$w[130]);
	define('WEIGHT',$w[131]);
	define('SEEKING_GENDER',$w[80]);
	define('HOROSCOPE',$w[129]);
	define('USERTYPE',$w[270]);
	define('ZIP',$w[285]);
	define('ADDITIONAL_INFO',$w[534]);
	//define('WITH_SELECTED',$w[556]);
	$p=0;
	include_once C_PATH.'/templates/'.C_TEMP.'/search_results.php';
}
include_once C_PATH.'/footer.php';
?>