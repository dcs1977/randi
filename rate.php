<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               rate.php                         #
# File purpose            Rate profiles                    #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';
IsSecure(C_RATE);
include_once C_PATH.'/sheader.php';
if (isset($id)&&is_numeric($id) && C_POPULARITY == '2') 
{

	CheckForIgnor($id,True);
	
	if(!isset($rate) || !is_numeric($rate) || $rate < 1 || $rate > 10) sprintm($w[1].'1', 1);
	if($id == MyID()) sprintm($w[425]);
	if(pop_in($id, intval($rate))) sprintm($w[427]);
	else sprintm($w[426]);
}
include_once C_PATH.'/sfooter.php';
?>