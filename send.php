<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               send.php                         #
# File purpose            Send message to user             #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
include_once 'templates/' . C_TEMP . '/config.php';
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

} else {
    include_once C_PATH . '/header.php';
}
IsSecure(C_SENDSH);

if (isset($id) && is_numeric($id) && GetStatus() > 6) {

	/* Checking for allowing to send message */
	CheckAllowMessaging(get_info('m'),get_info('d'),True);

	/* Checking for allowing to send message to user with the same gender */
	IsAllowContact($id);

	/* Checking for ban */
	CheckForIgnor($id);
	
	if(!isset($a)) $a='';
	if ($a == "s") {
	    if (isset($_SESSION['recaptcha'])){
            $recaptcha_alert = false;
        } else {
            if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                $secret_key = RECAPTCHA_SECRET_KEY;
                if (!isset($token)){
                    send_response(500, $w[355].'<br/>Nem töltött be a recaptcha');
                } else if (empty($token)){
                    send_response(500, $w[355].'<br/>Nem töltött be a recaptcha' );
                }
            } else {
                $secret_key = OLD_RECAPTCHA_SECRET_KEY;
                if (!isset($_POST['g-recaptcha-response'])){
                    printm($w[355].'<br/>Nem töltött be a recaptcha', 2);
                } else if (empty($_POST['g-recaptcha-response'])){
                    printm($w[355].'<br/>Nem töltött be a recaptcha', 2);
                }
                $token = $_POST['g-recaptcha-response'];
            }

            // call curl to POST request
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('secret' => $secret_key, 'response' => $token)));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);
            if (!empty($response)){
                $arrResponse = json_decode($response, true);
            }

            if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                if($arrResponse["success"] == '1' && $arrResponse["action"] == $action && $arrResponse["score"] >= 0.5) {
                    $recaptcha_alert = false;
                } else {
                    $recaptcha_alert = true;
                }
            } else {
                if ($arrResponse["success"]){
                    $recaptcha_alert = false;
                } else {
                    $recaptcha_alert = true;
                }
            }
        }

        if($recaptcha_alert === true){
            send_response(500, 'A recaptcha szerint SPAM küldő lehetsz.');
        } else {
            $_SESSION['recaptcha'] = true;
        }

        if(!isset($_POST['subject-honeypot'])){
            send_response(500, 'Nem támogatott böngészőt használhatsz!');
        } else if($_POST['subject-honeypot'] !== ''){
            send_response(500, 'Nem támogatott böngészőt használhatsz!');
        }

		$subject=$w[862];$message=cb(trim($message));

		include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';
		$message=RemoveAzDGCode($message,C_BB_SM_ACL);
		if(!IsAccess(C_VERIMG_SM)) {
			$verifyimage = cb($verifyimage);
			if(strtoupper($verifyimage) != strtoupper(@$_SESSION['vc']))
			{
                send_response(500);
			}
		}
		if(trim($subject) == '' || trim($message) == '') printm($w[164]);
		if (!isset($_SESSION['confirm'])){
                    $_SESSION['confirm'] = ((isset($confirm))?true:false);
                }

		if (!isset($confirm)) $confirm = "off";
		$confirm = ($confirm == "on") ? "2" : "1";

		// Checking for bad words
		if (C_BADWORDS_ENABLE) {
			if (C_BADWORDS_ERR) {
				if(is_badwords($subject) || is_badwords($message))
                    send_response(500, 'Tiltott szavak használata.');
			}
			elseif (C_BADWORDS_REP) {
				$subject = replace_badwords($subject);
				$message = replace_badwords($message);
			}
		}

		CheckAllowMessaging(get_info('m'),get_info('d'),False);

                /* Check for spamming or flooding */
                $is_spam = CheckNotSpamMessage(get_info('m'), $id, $message);
                
                if ($is_spam === false){
                    // Send Confirm if it first unreaded email
                    IsFirstMessage($id);
                    //send_response(500);                    
                    IsNewMemberLimit();
                    
                    db_query("INSERT INTO ".C_MYSQL_MESSAGES." VALUES (Null,'".get_info('m')."','".$id."','".$subject."',NOW(),'".ip2int(ip())."','".$confirm."', '0')");

                    $lastid = db_insert_id();		
                    save_message($lastid, $message);

                    AddUserToAddressBook($id);

                    //Image attachment

                    include_once C_PATH.'/classes/upload.class.php';
                    include_once C_PATH.'/classes/thumbnail.class.php';
                    
                    if ( !empty( $_FILES['image']['tmp_name'] ) ){
			
						
			$dir = ((integer)floor(($lastid)/10000));
			
			if (!file_exists(C_PATH.'/members/uploads/messages/'.$dir)) {
			    mkdir(C_PATH.'/members/uploads/messages/'.$dir,0777);
                        }

			$upl = new Upload('image', C_MAXSZ, C_MAXWD, C_MAXHG, C_PATH.'/members/uploads/messages/'.$dir.'/'.$lastid);
			
			if (!$upl->do_upload())
			{
				unlink (C_PATH.'/members/uploads/messages/'.$dir.'/'.$lastid);
				printm($upl->getErrors());
			}
			else
			{
				$thumb=new Thumbnail(C_PATH.'/members/uploads/messages/'.$dir.'/'.$lastid,null,True) or die('error');
				if(C_PICTURE_WIDTH) $thumb->Width(C_PICTURE_WIDTH);
				if(C_PICTURE_HEIGHT) $thumb->Height(C_PICTURE_HEIGHT);
				if(C_PICTURE_AUTO) $thumb->Auto(C_PICTURE_AUTO);
				$thumb->Quality(C_PICTURE_QUALITY);
				$thumb->Save(C_PATH.'/members/uploads/messages/'.$dir.'/'.$lastid,True);
				$subject = $w[863];
				db_query("UPDATE ".C_MYSQL_MESSAGES." SET subject='".$subject."' WHERE id='".$lastid."'");
			}
			
                    }

                    //Push Notification
                    sendpushnotification($id, $_SESSION['username'], $subject, C_URL.'/members/index.php?l=hu&a=v');

                    // Popularity
                    if((C_POPULARITY == '1')&&(C_HITS_SEND_FROM)) pop_in(get_info('m'),C_HITS_MAIL_FROM);
                    if((C_POPULARITY == '1')&&(C_HITS_SEND_TO)) pop_in($id,C_HITS_MAIL_TO);
                    //////////////
                    
                    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                        alert_mes($w[165]);
                        send_response(200, C_URL.'/members/index.php?l=hu&a=sm');
                    } else {
                        printm($w[165],2);
                    }
                } else {
                    send_response(500, 'Gyakori üzenetküldés történt');
                }

	} else {

		//Check to have inbound message
                $tmp = db_query("SELECT id, toid, fromid, subject, ".CorrectDate('sendtime').", readed, deleted FROM ".C_MYSQL_MESSAGES." WHERE (toid='".get_info('m')."' AND fromid='".$id."' AND deleted<>'1' AND deleted<>'3') OR (toid='".$id."' AND fromid='".get_info('m')."' AND deleted<>'2' AND deleted<>'3')");
                $num_rows = db_num_rows($tmp);
		
		if ($num_rows > 0){
			$i = db_fetch_array($tmp);
			echo '<script> location.replace("'.C_URL.'/members/index.php?l=hu&a=m&mid='.$i['id'].'"); </script>';
		}

                if (isset($_SESSION['confirm'])){
                    define('CONFIRM', (($_SESSION['confirm'])?' checked':''));
                } else {
                    define('CONFIRM', '');
                }

		define('ENTER_YOUR_SUBJECT',$w[166]);
		define('ENTER_YOUR_MESSAGE',$w[167]);
		define('ID_VALUE',$id);
		define('SEND_MESSAGE_TO_USER',$w[514]);
		define('NOTIFY_ME',$w[203]);
		define('SEND_MESSAGE',$w[170]);
		define('SUBJECT',$w[168].$w[0]);
		define('MESSAGE',$w[169].$w[0]);
		if (IsAccess(C_VERIMG_SM)) // NOT
		{
			define('VERIFY_ALLOW', '0');
			define('VERIFY_ERROR', '');
		}
		else
		{
			define('VERIFY_ALLOW', '1');
			define('VERIFY_ERROR', $w[497]);
		}
		define('VERIFICATION', $w[498]);

		if(isset($uname)) define('USER_NAME',$uname);
		else define('USER_NAME','');

		include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';
		BBCheck(C_BB_SM_ACL);

		include_once C_PATH.'/templates/'.C_TEMP.'/send.php';

	}} include_once C_PATH.'/footer.php';
?>