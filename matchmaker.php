<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               matchmaker.php                   #
# File purpose            Find perfect match               #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';
IsSecure(C_MATCHMAKER);
include_once C_PATH.'/header.php';

IsAccessToSeeThisPage();
$sr=explode(',',C_SR_COLUMNS);
if(isset($_GET['find_my_match']))
{
	$tmp=db_query("SELECT t1.*, t2.*, t3.* FROM ".C_MYSQL_MEMBERS_MAIN." as t1,".C_MYSQL_MEMBERS_EXT." as t2,".C_MYSQL_MEMBERS_SEC." as t3 WHERE t1.id = '".MyID()."' AND t1.id = t2.id AND t1.id = t3.id limit 1") or die(db_error());

	function MatchReturn($formvar,$sqlvar,$sqlfld,$type=null)
	{
		if($type == null)
		{
			$str='';
			if(isset($formvar) && ($formvar == "on"))
			{
				$sqlvar2=explode(',',$sqlvar);$p=0;$and=' OR ';
				if(count($sqlvar2))
				{
					$str = " AND (".$sqlfld." = '' OR ".$sqlfld." = '0' OR (";
					while (isset($sqlvar2[$p])){$str .= ($p == 0) ? "FIND_IN_SET(".cb(C_MYSQL_CHARSET)."'".$sqlvar2[$p]."',".$sqlfld.")" : $and."FIND_IN_SET(".cb(C_MYSQL_CHARSET)."'".$sqlvar2[$p]."',".$sqlfld.")";$p++;}
					$str.='))';
				}
				else {
					$str = " AND ".$sqlfld." = ''";
				}
			}
			else $str='';
			Return $str;
		}
		else
		{
			$str='';
			if(!empty($sqlvar) && (isset($formvar) && ($formvar == "on")))
			{
				$sqlvar2=explode(',',$sqlvar);$p=0;$or=' OR ';
				if(count($sqlvar2))
				{
					$str = " AND (";
					while (isset($sqlvar2[$p])){$str .= ($p == 0) ? $sqlfld." = '".$sqlvar2[$p]."'" : $or.$sqlfld." = '".$sqlvar2[$p]."'";$p++;}
					$str.=')';
				}
				else {
					$str = "";
				}
			}
		}
	}
	while($i=db_fetch_array($tmp))
	{

		### Who is looking for me

		$mage=(isset($my_age) && ($my_age == "on")) ? " AND t2.agef <= '".abs(mysql2data($i['birthday'],1))."' AND t2.aget >= '".abs(mysql2data($i['birthday'],1))."'" : "";
		$mgender=MatchReturn(@$my_gender,$i['gender'],'t1.sgender');
		$mpurpose=MatchReturn(@$common_purpose,$i['purposes'],'t1.purposes');
		$mcountry=MatchReturn(@$my_country,$i['country'],'t2.scountry');
		$mmarstat=MatchReturn(@$my_marstat,$i['marstat'],'t2.smarstat');
		$mchild=MatchReturn(@$my_child,$i['child'],'t2.schild');
		$mhcolor=MatchReturn(@$my_hcolor,$i['hcolor'],'t2.shcolor');
		$mecolor=MatchReturn(@$my_ecolor,$i['ecolor'],'t2.secolor');
		$msmoke=MatchReturn(@$my_smoke,$i['smoke'],'t2.ssmoke');
		$mdrink=MatchReturn(@$my_drink,$i['drink'],'t2.sdrink');
		$meducation=MatchReturn(@$my_education,$i['education'],'t2.seducation');
		$metnicity=MatchReturn(@$my_ethnicity,$i['etnicity'],'t2.setnicity');
		$mreligion=MatchReturn(@$my_religion,$i['religion'],'t2.sreligion');

		$mheight= ((!empty($i['height'])) && (isset($my_height) && ($my_height == "on"))) ? " AND t2.heightf <= '".$i['height']."' AND t2.heightt >= '".$i['height']."'" : '';
		$mweight= ((!empty($i['weight'])) && (isset($my_weight) && ($my_weight == "on"))) ? " AND t2.weightf <= '".$i['weight']."' AND t2.weightt >= '".$i['weight']."'" : '';

		### What I`m looking for?
		$meage=(isset($match_age) && ($match_age == "on")) ? " AND t1.birthday >= DATE_SUB(NOW(), INTERVAL ".$i['aget']." YEAR) AND t1.birthday <= DATE_SUB(NOW(), INTERVAL ".($i['agef']+1)." YEAR)" : "";

		
		$megender=MatchReturn(@$match_gender,$i['sgender'],'t1.gender',1);
		$mecountry=MatchReturn(@$match_country,$i['scountry'],'t1.country',1);
		$memarstat=MatchReturn(@$match_marstat,$i['smarstat'],'t2.marstat',1);
		$mechild=MatchReturn(@$match_child,$i['schild'],'t2.child',1);
		$mehcolor=MatchReturn(@$match_hcolor,$i['shcolor'],'t2.hcolor',1);
		$meecolor=MatchReturn(@$match_ecolor,$i['secolor'],'t2.ecolor',1);
		$mesmoke=MatchReturn(@$match_smoke,$i['ssmoke'],'t2.smoke',1);
		$medrink=MatchReturn(@$match_drink,$i['sdrink'],'t2.drink',1);
		$meeducation=MatchReturn(@$match_education,$i['seducation'],'t2.education',1);
		$meetnicity=MatchReturn(@$match_ethnicity,$i['setnicity'],'t2.etnicity',1);
		$mereligion=MatchReturn(@$match_religion,$i['sreligion'],'t2.religion',1);

		$meheight=(isset($match_height) && ($match_height == "on")) ? " AND ((t1.height >= '".$i['heightf']."' AND t1.height <= '".$i['heightt']."') OR t1.height = '0')" : "";
		$meweight=(isset($match_weight) && ($match_weight == "on")) ? " AND ((t1.weight >= '".$i['weightf']."' AND t1.weight <= '".$i['weightt']."') OR t1.weight = '0')" : "";


		///////// Checking and creating for search
		$sh_ph_f = (C_SHOW_PHOTOS_FIRST) ? 't1.files DESC,' : '';
		$privsf = (C_PRIV_SHOW_FIRST) ? 't1.req DESC,':'';
		$msortby = " order by ".$privsf.$sh_ph_f." t1.editdate DESC";
		$mid = " t1.status > '5' AND t1.id != '".MyID()."' AND t1.id = t2.id AND t1.id = t3.id AND t1.hide='0'";

		$opt=$mid.$mage.$mgender.$mpurpose.$mcountry.$mmarstat.$mchild.$mhcolor.$mecolor.$metnicity.$mreligion.$msmoke.$mdrink.$meducation.$mheight.$mweight.$meage.$megender.$mecountry.$memarstat.$mechild.$mehcolor.$meecolor.$meetnicity.$mereligion.$mesmoke.$medrink.$meeducation.$meheight.$meweight;

		$sql="SELECT count(*) as total FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2, ".C_MYSQL_MEMBERS_SEC." as t3 WHERE ".$opt;
		#echo $sql;
		$tmp = db_query($sql) or die(db_error());
		$trows = db_fetch_array($tmp);
		$count = $trows['total'];
		if($count == "0") printm($w[110]);

		### What to show
		$qage = (IsAccess($sr[4])) ? ',t1.birthday' : '';
		$qgender = (IsAccess($sr[5])) ? ',t1.gender' : '';
		$qbodytype = (IsAccess($sr[6])) ? ',t1.bodytype' : '';
		$qorientation = (IsAccess($sr[7])) ? ',t1.orientation' : '';
		$qpurpose = (IsAccess($sr[8])) ? ',t1.purposes' : '';
		$qcountry = (IsAccess($sr[9])) ? ',t1.country' : '';
		$qcity = (IsAccess($sr[10])) ? ',t1.city' : '';
		$qheight = (IsAccess($sr[11])) ? ',t1.height' : '';
		$qweight = (IsAccess($sr[12])) ? ',t1.weight' : '';
		$qsgender = (IsAccess($sr[13])) ? ',t1.sgender' : '';
		$qhoro = (IsAccess($sr[15])) ? ',t1.horo' : '';
		$qregdate = (IsAccess($sr[16])) ? ',t1.regdate' : '';
		$qeditdate = (IsAccess($sr[17])) ? ',t1.editdate' : '';
		$qusertype = (IsAccess($sr[19])) ? ',t1.req' : '';
		if(IsAccess($sr[18])) {
			if(C_POPULARITY == 2) {
				$qrate =',ROUND(hits/hits_count,'.C_RATE_DIGITS.') as hits';
			} else {
				$qrate = ',ROUND(hits/(TO_DAYS(Now()) - TO_DAYS(IF(regdate > \''.C_LAST_CLEAR_DATE.'\', regdate, \''.C_LAST_CLEAR_DATE.'\')) + 1),'.C_RATE_DIGITS.') as hits';
			}
		}
		else $qrate='';
		
		$sql = "SELECT t1.id,t1.username,t1.fname,t1.lname,t1.files".$qage.$qgender.$qbodytype.$qorientation.$qpurpose.$qcountry.$qcity.$qheight.$qweight.$qsgender.$qhoro.$qregdate.$qeditdate.$qusertype.$qrate." FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2, ".C_MYSQL_MEMBERS_SEC." as t3 WHERE ".$opt.$msortby." limit ".$from.",".$step;
		#echo $sql;
		$result = db_query($sql) or die(db_error());

		$str='';$color='';
		$ids=array();$names=array();$genders=array();$purposes=array();$ages=array();$countries=array();$cities=array();$editdates=array();$regdates=array();$pictures=array();$colors=array();
		### What to show
		function ShowConst($var,$acs)
		{
			Global $sr;
			if(IsAccess($sr[$acs])) define($var,'1');
			else define($var,'0');
		}

		ShowConst('SHOW_FNAME',2);
		ShowConst('SHOW_LNAME',3);
		ShowConst('SHOW_AGE',4);
		ShowConst('SHOW_GENDER',5);
		ShowConst('SHOW_BODYTYPE',6);
		ShowConst('SHOW_ORIENTATION',7);
		ShowConst('SHOW_PURPOSE',8);
		ShowConst('SHOW_COUNTRY',9);
		ShowConst('SHOW_CITY',10);
		ShowConst('SHOW_HEIGHT',11);
		ShowConst('SHOW_WEIGHT',12);
		ShowConst('SHOW_SEEKING_GENDER',13);
		ShowConst('SHOW_PHOTO',14);
		ShowConst('SHOW_HORO',15);
		ShowConst('SHOW_REG_DATE',16);
		ShowConst('SHOW_LAST_ACCESS',17);
		ShowConst('SHOW_RATING',18);
		ShowConst('SHOW_USERTYPE',19);
		ShowConst('SHOW_ONLINE_STATUS',20);
		define('ONLINE_STATUS',$w[352]);
		define('ONLINE', $w[350]);
		define('OFFLINE', $w[351]);
		define('TOPS', false);
		while ($i = db_fetch_array($result)) {
			$pictures[] = SearchPhotoResult(IsAccess($sr[14]),$i['files'],$i['id'],$i['gender']);
			$color = ($color == COLOR4) ? COLOR3 : COLOR4;
			$colors[] = $color;
			$ids[]=$i['id'];
			$photoinfo[]=($i['files'] == '') ? $w[532] : template($w[533],array(PhotosNum($i['files'])));
			$names[] = username($i['id'],$i['username'],$i['fname'],$i['lname']);
			$fnames[] = (IsAccess($sr[2])) ? $i['fname'] : '';
			$lnames[] = (IsAccess($sr[3])) ? $i['lname'] : '';
			$genders[]=(IsAccess($sr[5])) ? $wg[$i['gender']] : '';
			$sgenders[]=(IsAccess($sr[13])) ? arr2val($wg, $i['sgender']) : '';
			$purposes[]=(IsAccess($sr[8])) ? arr2val($wp, $i['purposes']) : '';
			$orientations[]=(IsAccess($sr[7])) ? $wor[$i['orientation']] : '';
			$heights[]=(IsAccess($sr[11])) ? $wh[$i['height']] : '';
			$weights[]=(IsAccess($sr[12])) ? $ww[$i['weight']] : '';
			$horoscopes[]=(IsAccess($sr[15])) ? $whr[$i['horo']] : '';
			$bodytypes[]=(IsAccess($sr[6])) ? $wbt[$i['bodytype']] : '';
			$ages[]=(IsAccess($sr[4])) ? abs(mysql2data($i['birthday'],1)) : '';
			$countries[]=(IsAccess($sr[9])) ? $wcr[$i['country']] : '';
			$cities[]=(IsAccess($sr[10])) ? $i['city'] : '';
			$usertypes[]=(IsAccess($sr[19])) ? $wpu[$i['req']] : '';
			$editdates[]=(IsAccess($sr[17])) ? mysql2data($i['editdate']) : '';
			$regdates[]=(IsAccess($sr[16])) ? mysql2data($i['regdate']) : '';
			$rates[]=(IsAccess($sr[18])) ? $i['hits'] : '';
		}
		$colspan=8;
		@$param='find_my_match&my_age='.$my_age.'&match_age='.$match_age.'&my_gender='.$my_gender.'&match_gender='.$match_gender.'&my_country='.$my_country.'&match_country='.$match_country.'&my_marstat='.$my_marstat.'&match_marstat='.$match_marstat.'&my_child='.$my_child.'&match_child='.$match_child.'&my_hcolor='.$my_hcolor.'&match_hcolor='.$match_hcolor.'&my_ecolor='.$my_ecolor.'&match_ecolor='.$match_ecolor.'&my_smoke='.$my_smoke.'&match_smoke='.$match_smoke.'&my_drink='.$my_drink.'&match_drink='.$match_drink.'&my_education='.$my_education.'&match_education='.$match_education.'&my_ethnicity='.$my_ethnicity.'&match_ethnicity='.$match_ethnicity.'&my_religion='.$my_religion.'&match_religion='.$match_religion.'&my_height='.$my_height.'&match_height='.$match_height.'&my_weight='.$my_weight.'&match_weight='.$match_weight.'&common_purpose='.$common_purpose;
		define('PAGES',pages($from,$step,$count,$param,$colspan));
		define('SEARCH_RESULTS',search_results($from,$step,$count,IsAccess(C_SHOW_SEARCH_COUNT),MATCHMAKER));
		define('USERNAME',$w[118]);
		define('PURPOSES',$w[119]);
		define('AGE',$w[120]);
		define('COUNTRY',$w[121]);
		define('CITY',$w[122]);
		define('LAST_ACCESS',$w[123]);
		define('REGISTER_DATE',$w[124]);
		define('PHOTO',$w[87]);
		define('RATING',$w[216]);
		define('FIRSTNAME',$w[127]);
		define('LASTNAME',$w[128]);
		define('GENDER',$w[57]);
		define('BODYTYPE',$w[525]);
		define('ORIENTATION',$w[524]);
		define('HEIGHT',$w[130]);
		define('WEIGHT',$w[131]);
		define('SEEKING_GENDER',$w[80]);
		define('HOROSCOPE',$w[129]);
		define('USERTYPE',$w[270]);
		define('ADDITIONAL_INFO',$w[534]);
		$p=0;
		include_once C_PATH.'/templates/'.C_TEMP.'/matchmaker_results.php';
	}
}
else
{
	define('MATCHMAKER_DESCRIPTION',$w[454]);
	define('MATCHMAKER_SEARCH',$w[455]);

	#define('MY_DATA',$w[456]);
	define('MATCH_DATA',$w[457]);
	define('COMMON_PURPOSE',$w[458]);

	define('MY_AGE',$w[120]);
	define('MY_GENDER',$w[132]);
	define('MY_COUNTRY',$w[121]);
	define('MY_MARSTAT',$w[134]);
	define('MY_CHILD',$w[135]);
	define('MY_HCOLOR',$w[136]);
	define('MY_ECOLOR',$w[137]);
	define('MY_SMOKE',$w[140]);
	define('MY_DRINK',$w[141]);
	define('MY_EDUCATION',$w[76]);
	define('MY_ETHNICITY',$w[138]);
	define('MY_RELIGION',$w[139]);
	define('MY_HEIGHT',$w[130]);
	define('MY_WEIGHT',$w[131]);

	define('MATCH_AGE',$w[83]);
	define('MATCH_GENDER',$w[80]);
	define('MATCH_COUNTRY',$w[444]);
	define('MATCH_MARSTAT',$w[445]);
	define('MATCH_CHILD',$w[446]);
	define('MATCH_HCOLOR',$w[447]);
	define('MATCH_ECOLOR',$w[448]);
	define('MATCH_SMOKE',$w[449]);
	define('MATCH_DRINK',$w[450]);
	define('MATCH_EDUCATION',$w[451]);
	define('MATCH_ETHNICITY',$w[81]);
	define('MATCH_RELIGION',$w[82]);
	define('MATCH_HEIGHT',$w[84]);
	define('MATCH_WEIGHT',$w[85]);
	include_once C_PATH.'/templates/'.C_TEMP.'/matchmaker.php';
}
include_once C_PATH.'/footer.php';
?>
