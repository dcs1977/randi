<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               slideshow.php                    #
# File purpose            View big photos                  #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';
IsSecure(C_SLIDESHOW); 

if(!IsAccess(C_BIG_PHOTOSH)) 
{
	if (!IsAdmin(1)) IsSecure(C_BIG_PHOTOSH,True);
}	
if (isset($id) && is_numeric($id))
	{
		$andstat = (!IsAdmin(1)) ? " AND status > '5'" : "";
		$result = db_query("SELECT * ,".CorrectDate("regdate").", ".CorrectDate("editdate")." FROM ".C_MYSQL_MEMBERS_MAIN." WHERE id = '".$id."'".$andstat) or die(db_error());
}
	 
	$count = db_num_rows($result);
	if ($count == '0') printm($w[205]);
	while ($i = db_fetch_array($result))
	{
		$pictures = $PicNum = array();
		if (IsAccess(C_PHOTOSH))
			{
			define('SHOW_PHOTOS', '1');
			if ($i['files'] != "")
			{
				$arr=explode(',',$i['files']);
				$big = (IsAccess(C_SLIDESHOW_BIG_PHOTOS)) ? '&bigger' : '';
				$p=0;while(isset($arr[$p])) {
					$pictures[] = ShowPic(PicPath($arr[$p],$i['id'])) . $big;
					$PicNum[] = $arr[$p];
				$p++;}
			}
		}
		else
			{
			define('SHOW_PHOTOS', '0');
		}
		
		$count = count($pictures);
		if($count < C_SLIDESHOW_PHOTOS_NUM) sprintm($w[1]);
		define('PLEASE_WAIT',$w[505]);
		define('UID', $i['id']);
    if (!empty($i['username']) && (IsAccess(C_USERNAMESH)))
    {
      define('USERNAME_VALUE', $i['username']);
    }
    else
    {
      define('USERNAME_VALUE', '');
    }
    if (!empty($i['birthday']) && (IsAccess(C_BIRTHDAYSH)))
    {
      define('AGE_VALUE', mysql2data($i['birthday'],2));
    }
    else
    {
      define('AGE_VALUE', '');
    }
    if (!empty($i['country']) && (IsAccess(C_COUNTRYSH)))
    {
      define('COUNTRY_VALUE', $wcr[$i['country']]);
    }
    else
    {
      define('COUNTRY_VALUE', '');
    }
    if (!empty($i['city']) && (IsAccess(C_CITYSH)))
    {
      define('CITY_VALUE', $i['city']);
    }
    else
    {
      define('CITY_VALUE', '');
    }
	
	$p = 0;
	if(IsAccess(C_SLIDESHOW_BIG_PHOTOS)) {
		define('SLIDESHOW_BIG_PHOTOS', '1');
	} else {
		define('SLIDESHOW_BIG_PHOTOS', '0');
	}
	$k=1;
	include_once C_PATH.'/templates/'.C_TEMP.'/slideshow.php';
	 
}
?>