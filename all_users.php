<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               all_users.php                    #
# File purpose            Show all users                   #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
define('REQ_COUNTRIES',True);
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
//include_once 'classes/cachequery.class.php';
include_once 'templates/'.C_TEMP.'/config.php';
#IsSecure(C_TOP_MORE);
include_once C_PATH.'/header.php';

$sr=explode(',',C_SR_COLUMNS);
//IsAccessToSeeThisPage();
$gender = 0;
$step = C_ONLINE_USERS_NUM;
if (!isset($from) || !is_numeric($from)) $from = 0;
if(C_HACK1) {
if(!is_numeric($step)||!is_numeric($from)||!is_numeric($gender)) printm($w[1].'1',1);}

if(C_HACK2) { 
if(($gender < 0)||($gender >= sizeof($wg))) printm($w[1].'2',1);}

$cntr = 0;
if(C_IP_MOD) $cntr = Country2NumByIP(array(2));

///////// Checking and creating for search
$mid = " t1.status >= '7' AND t1.hide='0'";
!empty($gender) ? $mgender=" AND gender = '".$gender."'" : $mgender = "";
$only_with_photo = '';


if (isset($_SESSION['search_date']) && $_SESSION['search_date'] < date("Y-m-d H:i:s")){
    unset($_SESSION['search_type']);    
    unset($_SESSION['search_date']);
    unset($_SESSION['search_data']);
}

if (!isset($_SESSION['search_data']) || (isset($_SESSION['search_type']) && $_SESSION['search_type'] != 'all_users')){
    $_SESSION['search_type'] = 'all_users';
    $_SESSION['search_data'] = array();
    $tmp0 = db_query("SELECT t1.id FROM ".C_MYSQL_MEMBERS_MAIN." as t1 WHERE".$mid.$only_with_photo.$mgender." order by t1.id DESC") or die(db_error());
    while($i=db_fetch_array($tmp0)) {
        $_SESSION['search_data'][] = $i['id'];
    }
    $_SESSION['search_date'] = date("Y-m-d H:i:s", ( time() + 300 ) );
}
$search = $_SESSION['search_data'];

//$tmp0=db_query("SELECT count(*) as total FROM ".C_MYSQL_ONLINE_USERS." as t1, ".C_MYSQL_MEMBERS_MAIN." as t2 WHERE t1.user = t2.id AND t2.status > '5' AND t2.hide='0'".$sql_dump) or die(db_error());
//$cnt=db_fetch_assoc($tmp0);
//$count=$cnt['total'];#die($count);
$count = count($search);
$in = array_slice($search, $from, $step);
if (count($in)){
    $where_in = ' AND t1.id IN ('.implode(', ', $in).') ';
} else {
    $where_in = ' AND t1.id IS NULL ';
}

//$tmp = New CacheQuery("SELECT count(*) as total FROM ".C_MYSQL_MEMBERS_MAIN." as t1 WHERE".$mid.$only_with_photo.$mgender,IsCache(1,16));
//$count = $tmp->data[0]['total'];
if($count == "0") printm($w[110]);

	### What to show
	$qage = (IsAccess($sr[4])) ? ',t1.birthday' : '';
	$qgender = (IsAccess($sr[5])) ? ',t1.gender' : '';
	$qbodytype = (IsAccess($sr[6])) ? ',t1.bodytype' : '';
	$qorientation = (IsAccess($sr[7])) ? ',t1.orientation' : '';
	$qpurpose = (IsAccess($sr[8])) ? ',t1.purposes' : '';
	$qcountry = (IsAccess($sr[9])) ? ',t1.country' : '';
    $qzip = (IsAccess($sr[21])) ? ',t1.zip' : '';
	$qcity = (IsAccess($sr[10])) ? ',t1.city' : '';
	$qheight = (IsAccess($sr[11])) ? ',t1.height' : '';
	$qweight = (IsAccess($sr[12])) ? ',t1.weight' : '';
	$qsgender = (IsAccess($sr[13])) ? ',t1.sgender' : '';
	$qhoro = (IsAccess($sr[15])) ? ',t1.horo' : '';
	$qregdate = (IsAccess($sr[16])) ? ','.CorrectDate('regdate') : '';
	$qeditdate = (IsAccess($sr[17])) ? ','.CorrectDate('editdate') : '';
	$qusertype = (IsAccess($sr[19])) ? ',t1.req' : '';
	if(IsAccess($sr[18])) {
		if(C_POPULARITY == 2) {
			$qrate =',ROUND(hits/hits_count,'.C_RATE_DIGITS.') as hits';
		} else {
			$qrate = ',ROUND(hits/(TO_DAYS(Now()) - TO_DAYS(IF(regdate > \''.C_LAST_CLEAR_DATE.'\', regdate, \''.C_LAST_CLEAR_DATE.'\')) + 1),'.C_RATE_DIGITS.') as hits';
		}
	}
	else $qrate='';
    $qchild = ',t4.child';

		if($cntr && (C_IP_ACTS & 2)){
                    //$tmp = New CacheQuery("SELECT t1.id,t1.username,t1.fname,t1.lname,t1.files".$qage.$qgender.$qbodytype.$qorientation.$qpurpose.$qchild.$qcountry.$qcity.$qzip.$qheight.$qweight.$qsgender.$qhoro.$qregdate.$qeditdate.$qusertype.$qrate.", IF(t1.country = ".$cntr.", 65536*id, id) as cntr FROM ".C_MYSQL_MEMBERS_MAIN." as t1,".C_MYSQL_MEMBERS_EXT." as t4 WHERE t1.id = t4.id AND ".$mid.$only_with_photo.$mgender.$where_in,IsCache(1,16));
                    $query = "SELECT t1.id,t1.username,t1.fname,t1.lname,t1.files".$qage.$qgender.$qbodytype.$qorientation.$qpurpose.$qchild.$qcountry.$qcity.$qzip.$qheight.$qweight.$qsgender.$qhoro.$qregdate.$qeditdate.$qusertype.$qrate.", IF(t1.country = ".$cntr.", 65536*id, id) as cntr FROM ".C_MYSQL_MEMBERS_MAIN." as t1,".C_MYSQL_MEMBERS_EXT." as t4 WHERE t1.id = t4.id AND ".$mid.$only_with_photo.$mgender.$where_in." order by t1.id DESC";
                } else {
                    $query = "SELECT t1.id,t1.username,t1.fname,t1.lname,t1.files".$qage.$qgender.$qbodytype.$qorientation.$qpurpose.$qchild.$qcountry.$qcity.$qzip.$qheight.$qweight.$qsgender.$qhoro.$qregdate.$qeditdate.$qusertype.$qrate." FROM ".C_MYSQL_MEMBERS_MAIN." as t1,".C_MYSQL_MEMBERS_EXT." as t4 WHERE t1.id = t4.id AND ".$mid.$only_with_photo.$mgender.$where_in." order by t1.id DESC";
                }
                
    $result = db_query($query);
    $tmp = array();
    while ($i = db_fetch_array($result)){
        $tmp[] = $i;
    }
		
    $str='';$color='';
	$ids=$names=$photoinfo=$genders=$sgenders=$fnames=$lnames=$bodytypes=$orientations=$child=$purposes=$ages=$countries=$cities=$heights=$weights=$horoscopes=$rates=$usertypes=$editdates=$regdates=$pictures=$colors=array();
	### What to show
	function ShowConst($var,$acs)
	{
		Global $sr;
		if(IsAccess($sr[$acs])) define($var,'1');
		else define($var,'0');
	}

	/*ShowConst('SHOW_FNAME',2);
	ShowConst('SHOW_LNAME',3);*/
	define('SHOW_FNAME',false);
	define('SHOW_LNAME',false);
	ShowConst('SHOW_AGE',4);
	ShowConst('SHOW_GENDER',5);
	ShowConst('SHOW_BODYTYPE',6);
	ShowConst('SHOW_ORIENTATION',7);
    define('CHILD', $w[135]);
	ShowConst('SHOW_PURPOSE',8);
	ShowConst('SHOW_COUNTRY',9);
	ShowConst('SHOW_CITY',10);
	ShowConst('SHOW_HEIGHT',11);
	ShowConst('SHOW_WEIGHT',12);
	/*ShowConst('SHOW_SEEKING_GENDER',13);*/
	define('SHOW_SEEKING_GENDER', false);
	ShowConst('SHOW_PHOTO',14);
	ShowConst('SHOW_HORO',15);
	ShowConst('SHOW_REG_DATE',16);
	ShowConst('SHOW_LAST_ACCESS',17);
	ShowConst('SHOW_RATING',18);
	ShowConst('SHOW_USERTYPE',19);
	ShowConst('SHOW_ONLINE_STATUS',20);
	define('ONLINE_STATUS',$w[352]);
	define('ONLINE', $w[350]);
	define('OFFLINE', $w[351]);
	define('TOPS', false);

	while(list($k,$i)=@each($tmp)) {
		$pictures[] = SearchPhotoResult(IsAccess($sr[14]),$i['files'],$i['id'],$i['gender']);

		$color = ($color == COLOR4) ? COLOR3 : COLOR4;
		$colors[] = $color;
		$ids[]=$i['id'];
		$photoinfo[]=($i['files'] == '') ? $w[532] : template($w[533],array(PhotosNum($i['files'])));
		$names[] = username($i['id'],$i['username'],$i['fname'],$i['lname']);
		$fnames[] = (IsAccess($sr[2])) ? $i['fname'] : '';
		$lnames[] = (IsAccess($sr[3])) ? $i['lname'] : '';
		$genders[]=(IsAccess($sr[5])) ? $wg[$i['gender']] : '';
		$sgenders[]=(IsAccess($sr[13])) ? arr2val($wg, $i['sgender']) : '';
		$purposes[]=(IsAccess($sr[8])) ? arr2val($wp, $i['purposes']) : '';
		$orientations[]=(IsAccess($sr[7])) ? $wor[$i['orientation']] : '';
		$heights[]=(IsAccess($sr[11])) ? $wh[$i['height']] : '';
		$weights[]=(IsAccess($sr[12])) ? $ww[$i['weight']] : '';
		$horoscopes[]=(IsAccess($sr[15])) ? $whr[$i['horo']] : '';
		$bodytypes[]=(IsAccess($sr[6])) ? $wbt[$i['bodytype']] : '';
        $childs[]= $wc[$i['child']];
		$ages[]=(IsAccess($sr[4])) ? abs(mysql2data($i['birthday'],1)) : '';
		$countries[]=(IsAccess($sr[9])) ? $wcr[$i['country']] : '';
		$cities[]=(IsAccess($sr[10])) ? $i['city'] : '';
		$usertypes[]=(IsAccess($sr[19])) ? $wpu[$i['req']] : '';
		$editdates[]=(IsAccess($sr[17])) ? mysql2data($i['editdate']) : '';
		$regdates[]=(IsAccess($sr[16])) ? mysql2data($i['regdate']) : '';
		$rates[]=(IsAccess($sr[18])) ? $i['hits'] : '';
		$zips[]=(IsAccess($sr[21])) ? $i['zip'] : '';
	}

$param='l='.$l;
$colspan=1;

define('PAGES',pages($from,$step,$count,$param,$colspan));
define('SEARCH_RESULTS',search_results($from,$step,$count,IsAccess(C_SHOW_SEARCH_COUNT),ALL_USERS));
	define('USERNAME',$w[118]);
	define('PURPOSES',$w[119]);
	define('AGE',$w[120]);
	define('COUNTRY',$w[121]);
	define('CITY',$w[122]);
	define('LAST_ACCESS',$w[123]);
	define('REGISTER_DATE',$w[124]);
	define('PHOTO',$w[87]);
	define('RATING',$w[216]);
	define('FIRSTNAME',$w[127]);
	define('LASTNAME',$w[128]);
	define('GENDER',$w[57]);
	define('BODYTYPE',$w[525]);
	define('ORIENTATION',$w[524]);
	define('HEIGHT',$w[130]);
	define('WEIGHT',$w[131]);
	define('SEEKING_GENDER',$w[80]);
	define('HOROSCOPE',$w[129]);
	define('USERTYPE',$w[270]);
	define('ADDITIONAL_INFO',$w[534]);
    define('WHENSEEN',false);
    define('WITH_SELECTED',$w[556]);
	$p=0;


include_once C_PATH.'/templates/'.C_TEMP.'/search_results.php';
include_once C_PATH.'/footer.php'; 
?>