<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               mail.php                         #
# File purpose            Send email to users              #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';

include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';
IsSecure(C_MAIL);
include_once C_PATH.'/header.php';
if(!IsAccess(C_MAILSH)) printm($w[187],2);

if (isset($id)&&is_numeric($id)) {
if (is_numeric(get_info('m'))) {
/* Checking for allowing to send message */
CheckAllowMessaging(get_info('m'),get_info('d'),True);

	
	/* Checking for allowing to send message to user with the same gender */
	IsAllowContact($id);

/* Checking for ban */
CheckForIgnor($id);

}

if(!isset($a)) $a='';if(!isset($sendto)) $sendto=0;
if ($a == "s") {
if(empty($subject)||empty($message)) printm('All field must be filled');
if (c_email($remail) == 0) printm($w[11]);
$subject=cb($subject);$message=cbmail($message);$remail=cb($remail);
$tmp=db_query("SELECT email FROM ".C_MYSQL_MEMBERS_SEC." WHERE id='".$id."' AND status > '5'");
$row=db_fetch_array($tmp);
$toemail=$row['email'];
unset($deactive);unset($m); // Special for hackers:)
/* Checking for allowing to send message */
CheckAllowMessaging(get_info('m'),get_info('d'),False);
if(is_numeric(get_info('m'))) { 

// If user is registered - Popularity
if((C_POPULARITY == '1')&&(C_HITS_MAIL_FROM)) pop_in(get_info('m'),C_HITS_MAIL_FROM);
if((C_POPULARITY == '1')&&(C_HITS_MAIL_TO)) pop_in($id,C_HITS_MAIL_TO);
//////////////

$tmp=db_query("SELECT email, nomail FROM ".C_MYSQL_MEMBERS_SEC." WHERE id='".get_info('m')."' AND status > '5'");
$row=db_fetch_array($tmp);
$deactive = IsAccess(C_ANOTHER_MAIL) ? $remail : $row['email']; 
$nomail =  $row['nomail']; 
}
else {
// If user not registered - Popularity
if((C_POPULARITY == '1')&&(C_HITS_MAIL_TO)) pop_in($id,C_HITS_MAIL_TO);
//////////////
$deactive=$remail;
}
$mestxt = template($w[667],array(C_URL));
if(IsAccess(C_MAIL_THROUGH_ADMIN) && $sendto) {
$addmes=$w[515].": ".$toemail."\r\n\r\n";
sendmail($deactive,C_ADMINM,$subject,nl2br($addmes.$message.$mestxt),$addmes.$message.$mestxt);
} else sendmail($deactive,$toemail,$subject,nl2br($message.$mestxt),$message.$mestxt,$nomail);
printm($w[165],2);
} else {

define('ENTER_YOUR_SUBJECT',$w[166]);
define('ENTER_YOUR_MESSAGE',$w[167]);
define('ENTER_YOUR_EMAIL',$w[11]);
define('ID_VALUE',$id);
define('SEND_MESSAGE_TO_EMAIL',$w[515]);
define('SUBJECT',$w[168].$w[0]);
define('MESSAGE',$w[169].$w[0]);
unset($deactive);unset($m); // Special for hackers:)
if(is_numeric(get_info('m'))) { // If user registered and login
$tmp=db_query("SELECT email FROM ".C_MYSQL_MEMBERS_SEC." WHERE id='".get_info('m')."' AND status > '5'");
$row=db_fetch_array($tmp);
$deactive = IsAccess(C_ANOTHER_MAIL) ? 'value="'.$row['email'].'"' : 'value="'.$row['email'].'" readonly="true"'; 
}
else $deactive='';
define('EMAIL_VALUE_DISABLED',$deactive);
define('EMAIL',$w[60].$w[0]);
define('SEND_MESSAGE',$w[170]);
if(isset($uname)) define('USER_NAME',$uname);
else define('USER_NAME','');

include_once C_PATH.'/templates/'.C_TEMP.'/mail.php';

}} include_once C_PATH.'/footer.php';?>