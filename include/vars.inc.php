<?php
############################################################
# AzDGDatingMedium        Version 1.8.0                    #
# Written by              AzDG (support@azdg.com)          #
# Created 29/08/03        Last Modified 29/08/03           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               vars.inc.php                     #
# File purpose            File with variables and arrays   #
# File created by         AzDG <support@azdg.com>          #
############################################################
# This file has been generated by admin script, be careful #
# on file change.                                          #
############################################################
if(!defined("C_URL")) die('Can`t access file directly!');

# How many messages per day can sent each user type
$wps=array('50');

# Messages number which can send new member for free by gender:
$wmg=array('0','20','20');

# How many photos may add each type of user
$wpn=array('10');

# Real language names in english
$reallangname=array('ar'=>'Arabic','az'=>'Azerbaijani','bg'=>'Bulgarian','br'=>'Brazilian','bs'=>'Bosnian','zh'=>'Chinese','cs'=>'Czech','de'=>'German','default'=>'Default','da'=>'Danish','es'=>'Spanish','fr'=>'French','fi'=>'Finnish','ka'=>'Georgian','el'=>'Greek','hr'=>'Croatian','hu'=>'Hungarian','it'=>'Italian','lt'=>'Lithuanian','lv'=>'Latvian','nl'=>'Dutch','no'=>'Norwegian','pl'=>'Polish','pt'=>'Portuguese','ro'=>'Romanian','ru'=>'Russian','sl'=>'Slovenian','sv'=>'Swedish','sk'=>'Slovak','sr'=>'Serbian','tr'=>'Turkish','uk'=>'Ukrainian');

# How many search pages show to user by access level - 0 means unlimited
$wsp=array('3','0');
?>