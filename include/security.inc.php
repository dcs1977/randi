<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 03/01/03        Last Modified 05/01/03           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               include/security.inc.php         #
# File purpose            For some secure reasons          #
# File created by         AzDG <support@azdg.com>          #
############################################################
if(!defined("C_URL")) die('Can`t access file directly!');

# Fixing possible security hole in register_globals = on
if (isset($_SESSION)){while (list($name, $value) = each($_SESSION)){unset($$name);};};
if (isset($_COOKIES)){while (list($name, $value) = each($_COOKIES)){unset($$name);};};
if (isset($_ENV)){while (list($name, $value) = each($_ENV)){unset($$name);};};
if (isset($_FILES)){while (list($name, $value) = each($_FILES)){unset($$name);};};
if (isset($_SERVER)){while (list($name, $value) = each($_SERVER)){unset($$name);};};

@ob_start();
if(C_SHOW_LANG == '0') {
	// Showing default language only
	if(defined("C_USERLANG") && file_exists(C_PATH.'/languages/'.C_USERLANG.'/'.C_USERLANG.'.php'))
	{
		include_once C_PATH.'/languages/'.C_USERLANG.'/'.C_USERLANG.'.php';
		include_once C_PATH.'/languages/'.C_USERLANG.'/'.C_USERLANG.'_.php';
		include_once C_PATH.'/languages/'.C_USERLANG.'/'.C_USERLANG.'l.php';
		$l=C_USERLANG;
	}
	else
	{
		include_once C_PATH.'/languages/default/default.php';
		include_once C_PATH.'/languages/default/default_.php';
		include_once C_PATH.'/languages/default/defaultl.php';
		$l='default';
	}

}
elseif(C_SHOW_LANG == '1') {
	// Showing browser language
	$my_langs = explode(',', @$_SERVER['HTTP_ACCEPT_LANGUAGE']);
	$avail_langs = explode(',',C_AVAILABLE_LANG);
	$l = '';
	while(list($key, $mylang) = each($my_langs)) {
		$lang = substr(trim($mylang),0,2);
		if(in_array($lang,$avail_langs) && file_exists(C_PATH.'/languages/'.$lang.'/'.$lang.'.php')) {
			// Showing browser language if available
			$l = $lang;
			break;
		}
		elseif($lang == C_DEFAULT_LANG) {
			// Showing default language if it the same as default
			$l = C_USERLANG;
			break;
		}
	}
	// Showing default language
		if(!file_exists(C_PATH.'/languages/'.$l.'/'.$l.'.php') || (empty($l))) $l=C_USERLANG;
		include_once C_PATH.'/languages/'.$l.'/'.$l.'.php';
		include_once C_PATH.'/languages/'.$l.'/'.$l.'l.php';
		include_once C_PATH.'/languages/'.$l.'/'.$l.'_.php';
}
else {
	if(isset($_GET['l']) || isset($_POST['l'])) $l = isset($_GET['l']) ? $_GET['l'] : $_POST['l'];
	else $l='';
	if ($l == '') {
		if(C_MULTLANG_DEF) {
			// Showing browser language
			$my_langs = explode(',', @$_SERVER['HTTP_ACCEPT_LANGUAGE']);
			$avail_langs = explode(',',C_AVAILABLE_LANG);
			$l = '';
			while(list($key, $mylang) = each($my_langs)) {
				$lang = substr(trim($mylang),0,2);
				if(in_array($lang,$avail_langs) && file_exists(C_PATH.'/languages/'.$lang.'/'.$lang.'.php')) {
					// Showing browser language if available
					$l = $lang;
					break;
				}
				elseif($lang == C_DEFAULT_LANG) {
					// Showing default language if it the same as default
					$l = C_USERLANG;
					break;
				}
			}
		}
		if(!file_exists(C_PATH.'/languages/'.$l.'/'.$l.'.php') || (empty($l))) $l=C_USERLANG;
		include_once C_PATH.'/languages/'.$l.'/'.$l.'.php';
		include_once C_PATH.'/languages/'.$l.'/'.$l.'l.php';
		include_once C_PATH.'/languages/'.$l.'/'.$l.'_.php';
	}
	else
	{
		if ((isset($l) && file_exists(C_PATH.'/languages/'.$l.'/'.$l.'.php') && $l != '') && strstr(C_AVAILABLE_LANG,$l))
		{
			include_once C_PATH.'/languages/'.$l.'/'.$l.'.php';
			include_once C_PATH.'/languages/'.$l.'/'.$l.'l.php';
			include_once C_PATH.'/languages/'.$l.'/'.$l.'_.php';
		}
		else
		{
			if(defined("C_USERLANG") && file_exists(C_PATH.'/languages/'.C_USERLANG.'/'.C_USERLANG.'.php'))
			{
				include_once C_PATH.'/languages/'.C_USERLANG.'/'.C_USERLANG.'.php';
				include_once C_PATH.'/languages/'.C_USERLANG.'/'.C_USERLANG.'l.php';
				include_once C_PATH.'/languages/'.C_USERLANG.'/'.C_USERLANG.'_.php';
				$l=C_USERLANG;
			}
			else
			{
				include_once C_PATH.'/languages/default/default.php';
				include_once C_PATH.'/languages/default/defaultl.php';
				include_once C_PATH.'/languages/default/default_.php';
				$l='default';
			}
		}
	}
}
$_GET['l']=$_POST['l']=$_REQUEST['l']=$l;
$cont=@ob_get_contents();@ob_end_clean();

?>