<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 03/01/03        Last Modified 15/05/07           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               config.inc.php                   #
# File purpose            Main configuration file          #
# File created by         AzDG <support@azdg.com>          #
############################################################

$m1 = explode(" ", microtime());$stime = $m1[1] + $m1[0];

### Url were AzDGDatingMedium has been installed, not '/' in end!
define('C_URL','https://local.4i.hu/hotbull.hu/randi');

### Domain name
define('C_DOMAIN','local.4i.hu');

### Internal path to AzDGDatingMedium directory
define('C_PATH','e:/xampp/htdocs/hotbull.hu/randi');

### Site Name
define('C_SNAME','Randi HotBull.hu');

### Site Term and Data Protection verzion
define('C_TERM_RULES','2018-05-25');

### Authorization type
define('C_AUTH','1'); // Work on sessions '1' or cookie '0'
if(C_AUTH){
    session_set_cookie_params(0, '/', C_DOMAIN, true, true);
    session_start();
    unset($s,$m,$g,$o);
}

### Admin Data
$c_adminl = array('hotbill','pultos');// Admin login
$c_adminp = array('953ba03d9ed4fd0858ce445d5511292867131dd7107a377d946b5434634b1759','6e4e11072758b634bd96b267cc003116e4e9b3596e19ce653271bc8e9b6178be');// Admin password
// 433626, 936939
define('C_ADMINM','info@hotbull.hu');
define('C_ADMINLANG','hu');
define('C_USERLANG','hu');

### Onesignal data
define('C_ONESIGNAL_APP_ID', 'c9ac95bb-f76c-45fe-ac25-836dd237f2ed');
define('C_ONESIGNAL_REST_API_KEY', 'NGRkYjQ2ZDgtNmUxZC00NGYwLWIxMTctMzAxYzE0MjE5YWEz');

### Recaptcha v3
define("RECAPTCHA_PUBLIC_KEY", '6LcUz7IZAAAAAA5P4hQvNGySf3KMFP4CJxK5_i3X');
define("RECAPTCHA_SECRET_KEY", '6LcUz7IZAAAAAKok_ytFe-Ei3pujVSemJXJN0jkl');

### Recaptcha v2
define("OLD_RECAPTCHA_PUBLIC_KEY", '6Lcaz7IZAAAAAK_1yiXExbWnFg1Fz8QEUHJmwKzL');
define("OLD_RECAPTCHA_SECRET_KEY", '6Lcaz7IZAAAAAA7igPdQjkqhWlVuAuGWAa0tSs1J');

### MySQL data
define('C_HOST','localhost');// MySQL host name (usually:localhost)
define('C_USER','zsebkftsql');// MySQL username
define('C_PASS','Ghgh5KIbdx');// MySQL password
define('C_BASE','zsebkft_hotbullrandi');// MySQL database

define('C_MYSQL_MEMBERS','19_membersu');// Table for members info
define('C_MYSQL_MEMBERS_MAIN','19_membersu_main');// Table for main members info
define('C_MYSQL_MEMBERS_EXT','19_membersu_ext');// Table for extended members info
define('C_MYSQL_MEMBERS_SEC','19_membersu_sec');// Table for security members info
define('C_MYSQL_MEMBERS_RESTRICTS','19_membersu_restricts');// Table for restricts
define('C_MYSQL_ONLINE_USERS','19_onlineu');// Table for online users info
define('C_MYSQL_TEMP','19_temp');// Table for temporary info
define('C_MYSQL_PMEMBERS','19_pmembersu');// Table for privilegied members info
define('C_MYSQL_MESSAGES','19_messages');// Table for messages
define('C_MYSQL_MESSAGES_ARCHIVE','19_messages_archive');// Table for messages archive
define('C_MYSQL_CARDS','19_cards');// Table for cards
define('C_MYSQL_HITS','19_hits');// Table for hits
define('C_MYSQL_FAQCAT','19_faq_cat');// Table for faq categories
define('C_MYSQL_FAQ','19_faq');// Table for faq
define('C_MYSQL_FORUMCAT','19_forum_cat');// Table for forum categories
define('C_MYSQL_FORUM','19_forum');// Table for forum
define('C_MYSQL_FORUM_MESSAGES','19_forum_messages');// Table for forum messages
define('C_MYSQL_FORUM_THREADS','19_forum_threads');// Table for forum threads
define('C_MYSQL_BAD_PROFILES','19_bad');// Table for bad profiles
define('C_MYSQL_BEDROOM','19_room');// Table for bedroom
define('C_MYSQL_MAILLIST','19_mail');// Table for maillist
define('C_MYSQL_RECOMMEND','19_recommend');// Table for recommend statistic
define('C_MYSQL_AFFILIATES','19_affiliates');// Table for affiliates
define('C_MYSQL_BANLIST','19_banlist');// Table for banlist
define('C_MYSQL_NEWS','19_news');// Table for news
define('C_MYSQL_LINKSCAT','19_links_cat');// Table for links categories
define('C_MYSQL_LINKS','19_links');// Table for links
define('C_MYSQL_OPTIONS','19_options');// Table for options
define('C_MYSQL_SUBSCRIBE','19_subscribe');// Table for profile views info
define('C_MYSQL_PROFILEVIEW','19_profileview');// Table for profile views info
define('C_MYSQL_VOTES','19_votes');// Table for votes
define('C_MYSQL_VOTE_IPS','19_vote_ips');// Table for vote IPs
define('C_MYSQL_VOTE_QUESTIONS','19_vote_questions');// Table for Vote questions
define('C_MYSQL_BLACKLIST','19_blacklist');// Table for blacklist
define('C_MYSQL_COMMENTS','19_comments');// Table for comments
define('C_MYSQL_NEW_PHOTOS','19_new_photos');// Table for new photos
define('C_MYSQL_SEARCH','19_search');// Table for search
define('C_MYSQL_COUPONS','19_coupons');// Table for coupons
define('C_MYSQL_ADDRESSBOOK','19_addressbook');// Address book
define('C_MYSQL_MEMBERS_FORUM','19_membersu_forum');// Members forum info
define('C_MYSQL_FORUM_SEARCH','19_forum_search');// Forum search
define('C_MYSQL_TESTIMONIALS','19_testimonials');// Testimonials
define('C_MYSQL_ARTCAT','19_art_cat');// Table for articles categories
define('C_MYSQL_ART','19_art');// Table for articles
define('C_MYSQL_BLOGS','19_blogs');// Table for blogs
define('C_MYSQL_PRIVATE_PHOTOS','19_private_photos');// Table for private photos
define('C_MYSQL_ZIP_DATA','19_zip_data');// Table for zip data
define('C_MYSQL_NOTES','19_notes');// Table for notes
define('C_MYSQL_IP2COUNTRY','19_ip2country');// Table for ip2country
define('C_MYSQL_HOTORNOT','19_hotornot');// Table for hot_or_not
define('C_MYSQL_BLOGS_CAT','19_blogs_cat');// Table for blogs categories
define('C_MYSQL_BLOGS_COM','19_blogs_com');// Table for blogs comments
define('C_MYSQL_BLOGS_VOTE','19_blogs_vote');// Table for blogs rate
define('C_MYSQL_ALLHITS','19_allhits');// Table for all hits
define('C_MYSQL_ADDRESSBOOK_ARCHIVE','19_addressbook_archive');// Address book archive
define('C_MYSQL_DEVICES','19_devices');// Devices of users - used for Onesignal
?>