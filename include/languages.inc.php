<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               languages.inc.php                #
# File purpose            File for Language select         #
# File created by         AzDG <support@azdg.com>          #
############################################################
if(!defined("C_URL")) die('Can`t access file directly!');
if ((isset($l) && ($l == '') && (C_SHOW_LANG == '2')) || (C_SHOW_LANG == '3')) 
{
  $langfile = explode(',',C_AVAILABLE_LANG);
  $fnm=count($langfile);
}
if ($fnm > 1) 
{
	define('LANGUAGES',$w[262]);
	include_once C_PATH.'/templates/'.C_TEMP.'/languages.php';
}
?>