<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               faq.php                          #
# File purpose            Show help                        #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';
//include_once 'classes/cachequery.class.php';
IsSecure(C_FAQ_FOR);
if(!isset($id)) {
include_once C_PATH.'/header.php';
$f=0;$p=-1;
$categories=array();$colors=array();$ids=array();$questions=array();
$mlang = (C_FAQ_MLTLNG) ? "WHERE t1.lang = '".cb($l)."' AND t1.lang != '' AND" : "WHERE";
$tmp = New CacheQuery("SELECT t1.*,t2.* FROM ".C_MYSQL_FAQCAT." as t1, ".C_MYSQL_FAQ." as t2 ".$mlang." t1.id = t2.cid order by t1.catname ASC",IsCache(0,2048));
if(empty($tmp->data)) printm($w[162],1);
$prev_cat='';$color='';
while(list($k,$j)=@each($tmp->data)) {
if($prev_cat != $j['catname']) {$categories[]=$prev_cat=$j['catname'];$p++;}
$color = ($color == COLOR4) ? COLOR3 : COLOR4;
$colors[$p][]=$color;
$ids[$p][]=$j['id'];
$questions[$p][]=$j['question'];
}
$p=0;
include_once C_PATH.'/templates/'.C_TEMP.'/faq.php';
include_once C_PATH.'/footer.php';
}
else {
include_once C_PATH.'/sheader.php';
if(isset($id)&&is_numeric($id)) {
$tmp=db_query("SELECT question, answer FROM ".C_MYSQL_FAQ." WHERE id='".$id."'");
while($i=db_fetch_array($tmp)) {

define('QUESTION',$i['question']);
define('ANSWER',tb($i['answer']));
define('FAQ_ANSWER',$w[163]);
define('FAQ_ID',$id);

}}
include_once C_PATH.'/templates/'.C_TEMP.'/faq_message.php';
include_once C_PATH.'/sfooter.php';
}
?>