<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 04/01/07           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               testimonials.php                 #
# File purpose            Add testimonials to DB           #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';

if (!isset($a)) $a = '';
if ($a == 'a')
{
	IsSecure(C_TST_REPLY);

	switch(C_TST_STATUS)
	{
		default:case '1':
			$cst = 3;
			break;
		case '0':
			$cst = 7;
			break;
		case '2':
			$cst = 8;
			break;
	}
	$message=cb(trim($message));
	include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';
	$message=RemoveAzDGCode($message, C_BB_TM_ACL);

	if(trim($message) == '') printm($w[164]);
	if (C_BADWORDS_ENABLE) {
		if (C_BADWORDS_ERR) {
			if(is_badwords($message))
			printm(C_BADWORDS_MES,2);
		}
		elseif (C_BADWORDS_REP) {
			$message = replace_badwords($message);
		}
	}

	db_query("INSERT INTO ".C_MYSQL_TESTIMONIALS." VALUES (Null, '".MyID()."','".$message."','',NOW(),NOW(),'".$cst."')") or die(db_error());
	printm($w[636]);
}

include_once C_PATH.'/header.php';
# Detect access
$sr=explode(',',C_TST_COLUMNS);
if(!isset($from) || !is_numeric($from)) $from = 0;
$tmp=db_fetch_array(db_query("SELECT count(*) as total FROM ".C_MYSQL_TESTIMONIALS." WHERE status > 6"));
$count = $tmp['total'];
if($count > 0) define('NO_TESTIMONIALS',False);
else define('NO_TESTIMONIALS',$w[637]);
$tmp=db_query("SELECT t1.*,t2.*,t1.id as id FROM ".C_MYSQL_TESTIMONIALS." as t2,".C_MYSQL_MEMBERS_MAIN." as t1 WHERE t2.status > 6 AND t1.id = t2.uid order by t2.id desc limit ".$from.",".C_TST_CNT);
$ids=$message=$mdate=$reply=$rdate=$names=$photoinfo=$genders=$sgenders=$fnames=$lnames=$bodytypes=$orientations=$purposes=$ages=$countries=$cities=$heights=$weights=$horoscopes=$rates=$usertypes=$editdates=$regdates=$pictures=$colors=array();$str='';$color='';
include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';
while($i=db_fetch_array($tmp))
{
	$pictures[] = SearchPhotoResult(IsAccess($sr[14]),$i['files'],$i['id'],$i['gender']);
	$color = ($color == COLOR4) ? COLOR3 : COLOR4;
	$colors[] = $color;
	$ids[]=$i['id'];
	$photoinfo[]=($i['files'] == '') ? $w[532] : template($w[533],array(PhotosNum($i['files'])));
	$names[] = username($i['id'],$i['username'],$i['fname'],$i['lname']);
	$fnames[] = (IsAccess($sr[2])) ? $i['fname'] : '';
	$lnames[] = (IsAccess($sr[3])) ? $i['lname'] : '';
	$genders[]=(IsAccess($sr[5])) ? $wg[$i['gender']] : '';
	$sgenders[]=(IsAccess($sr[13])) ? arr2val($wg, $i['sgender']) : '';
	$purposes[]=(IsAccess($sr[8])) ? arr2val($wp, $i['purposes']) : '';
	$orientations[]=(IsAccess($sr[7])) ? $wor[$i['orientation']] : '';
	$heights[]=(IsAccess($sr[11])) ? $wh[$i['height']] : '';
	$weights[]=(IsAccess($sr[12])) ? $ww[$i['weight']] : '';
	$horoscopes[]=(IsAccess($sr[15])) ? $whr[$i['horo']] : '';
	$bodytypes[]=(IsAccess($sr[6])) ? $wbt[$i['bodytype']] : '';
	$ages[]=(IsAccess($sr[4])) ? abs(mysql2data($i['birthday'],1)) : '';
	$countries[]=(IsAccess($sr[9])) ? $wcr[$i['country']] : '';
	$cities[]=(IsAccess($sr[10])) ? $i['city'] : '';
	$usertypes[]=(IsAccess($sr[19])) ? $wpu[$i['req']] : '';
	$editdates[]=(IsAccess($sr[17])) ? mysql2data($i['editdate']) : '';
	$regdates[]=(IsAccess($sr[16])) ? mysql2data($i['regdate']) : '';
	$rates[]=(IsAccess($sr[18])) ? $i['hits'] : '';
	$message[]=AzDGCode($i['message']);
	$mdate[]=mysql2data($i['mdate']);
	$reply[]=AzDGCode($i['reply']);
	$rdate[]=mysql2data($i['rdate']);
}
### What to show
function ShowConst($var,$acs)
{
	Global $sr;
	if(IsAccess($sr[$acs])) define($var,'1');
	else define($var,'0');
}

ShowConst('SHOW_FNAME',2);
ShowConst('SHOW_LNAME',3);
ShowConst('SHOW_AGE',4);
ShowConst('SHOW_GENDER',5);
ShowConst('SHOW_BODYTYPE',6);
ShowConst('SHOW_ORIENTATION',7);
ShowConst('SHOW_PURPOSE',8);
ShowConst('SHOW_COUNTRY',9);
ShowConst('SHOW_CITY',10);
ShowConst('SHOW_HEIGHT',11);
ShowConst('SHOW_WEIGHT',12);
ShowConst('SHOW_SEEKING_GENDER',13);
ShowConst('SHOW_PHOTO',14);
ShowConst('SHOW_HORO',15);
ShowConst('SHOW_REG_DATE',16);
ShowConst('SHOW_LAST_ACCESS',17);
ShowConst('SHOW_RATING',18);
ShowConst('SHOW_USERTYPE',19);
ShowConst('SHOW_ONLINE_STATUS',20);
define('ONLINE_STATUS',$w[352]);
define('ONLINE', $w[350]);
define('OFFLINE', $w[351]);
define('TOPS', false);

$param='l='.$l.'&a=s';
$colspan=1;
define('PAGES',pages($from,C_TST_CNT,$count,$param,$colspan));
define('SEARCH_RESULTS',search_results($from,C_TST_CNT,$count,IsAccess(C_TST_SHOW_COUNT),$w[633]));
define('USERNAME',$w[118]);
define('PURPOSES',$w[119]);
define('AGE',$w[120]);
define('COUNTRY',$w[121]);
define('CITY',$w[122]);
define('LAST_ACCESS',$w[123]);
define('REGISTER_DATE',$w[124]);
define('PHOTO',$w[87]);
define('RATING',$w[216]);
define('FIRSTNAME',$w[127]);
define('LASTNAME',$w[128]);
define('GENDER',$w[57]);
define('BODYTYPE',$w[525]);
define('ORIENTATION',$w[524]);
define('HEIGHT',$w[130]);
define('WEIGHT',$w[131]);
define('SEEKING_GENDER',$w[80]);
define('HOROSCOPE',$w[129]);
define('USERTYPE',$w[270]);
define('ADDITIONAL_INFO',$w[534]);
define('ADMINISTRATOR',$w[519]);
$p=0;


function GenConst($const,$bool,$phrase=null)
{
	if($phrase != null && !defined($const)) define($const,$phrase);
	if ($bool)
	{
		define($const.'_SHOW', '1');
	}
	else
	{
		define($const.'_SHOW', '0');
	}
}

GenConst('TESTIMONIALS_REPLY',C_TST_REPLY,$w[634]);
GenConst('TESTIMONIALS',C_TST_VIEW,$w[633]);
define('MESSAGE',$w[635]);
define('ENTER',$w[263]);

BBCheck(C_BB_TM_ACL);
include_once C_PATH.'/templates/'.C_TEMP.'/testimonials.php';


include_once C_PATH.'/footer.php';
?>