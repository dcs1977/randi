<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# Template Name           Default                          #
# Author                  AzDG <support@azdg.com>          #
############################################################
# File name               footer.php                       #
# File purpose            Footer for Default template      #
# File created by         AzDG <support@azdg.com>          #
############################################################
global $stime,$w,$l;$m2 = explode(" ", microtime());$etime = $m2[1] + $m2[0];$ttime = ($etime - $stime);$ttime = number_format($ttime, 7);

define('PROCESSING_TIME',$w[103]);
define('SEC',$w[104]);
define('COUNTED_VALUE',$ttime);
define('GUESTS_ONLINE',$w[106]);
define('COPYRIGHTS',$w[107]);

include_once C_PATH.'/templates/'.C_TEMP.'/footer.php';
?>