<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               links.php                        #
# File purpose            Show links                       #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';

include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';
//include_once 'classes/cachequery.class.php';
IsSecure(C_LINKS);


if(isset($sid) && is_numeric($sid))
{
	define('DETAIL_INFO',$w[788]);

	$tmp=db_query("SELECT t1.*,t2.* FROM ".C_MYSQL_LINKSCAT." as t1, ".C_MYSQL_LINKS." as t2 WHERE t1.id = t2.cid AND t2.status > 6 AND t2.id = '".$sid."'");
	while ($i = db_fetch_array($tmp)) {
		define('ID_VALUE',$i['id']);
		define('CID_VALUE',$i['cid']);
		define('CATNAME_VALUE',$i['catname']);
		define('TITLE_VALUE',$i['title']);
		if(TITLE_VALUE == '') define('URL_VALUE',quot_add($i['url']));
		else define('URL_VALUE',$i['url']);
		define('DESCRIPTION_VALUE',$i['descr']);
		define('KEYWORDS_VALUE',$i['mkeywords']);
		define('USERNAME_VALUE',$i['uname']);
		define('REGDATE_VALUE',$i['regdate']);
		#		define('CID',$i['cid']);
	}
	define('CATNAME',$w[773]);
	define('URL',$w[770]);
	define('TITLE',$w[357]);
	define('DESCRIPTION',$w[677]);
	define('KEYWORDS',$w[289]);
	define('USERNAME',$w[789]);
	define('REGDATE',$w[124]);


	define('C_TITLE',TITLE_VALUE);
	define('C_KEYWORDS',KEYWORDS_VALUE);
	define('C_DESCRIPTION',DESCRIPTION_VALUE);
	include_once C_PATH.'/header.php';

	$p=0;
	include_once C_PATH.'/templates/'.C_TEMP.'/link_detail.php';
	include_once C_PATH.'/footer.php';
	die();
}

include_once C_PATH.'/header.php';

if(isset($confirm) && $confirm == "yes")
{
	if(IsAccess(C_LINK_WHO))
	{

		if (!isset($id) || !isset($code) || !is_numeric($id) || !is_md5($code))
		{
			// Code must content only chars and numbers :)
			include_once C_PATH.'/header.php';
			printm($w[1].'3');
		}

		$cst = (C_LINK_ADD == '3') ? 2 : 7;
		
		# Check if URL already exist
		$tsql = db_fetch_array(db_query("SELECT count(id) as total FROM ".C_MYSQL_LINKS." WHERE id = '".$id."' AND code = '".$code."' AND status = '0'"));
		$count = $tsql['total'];
		if($count)
		{
			@db_query("UPDATE ".C_MYSQL_LINKS." SET status = '".$cst."' WHERE id = '".$id."' AND code = '".$code."' AND status = '0'");
			if(C_LINKS_DAYS) @db_query("DELETE FROM ".C_MYSQL_LINKS." WHERE regdate < DATE_SUB(NOW(), INTERVAL ".C_LINKS_DAYS." DAY) AND status = '0'");
			if($cst == 7) printm($w[783],2);
			else printm($w[785],2);
		} else printm($w[786],2);
	}
}

if(isset($a) && $a == "add")
{
	if(IsAccess(C_LINK_WHO))
	{
		$allow_cats=array();
		$tmp = New CacheQuery("SELECT * FROM ".C_MYSQL_LINKSCAT." order by catname ASC",IsCache(1,4096));
		if(empty($tmp->data)) printm($w[375],1);
		$Variable='';while(list($k,$j)=@each($tmp->data)) {
			$allow_cats[]=$j['id'];
			if (isset($cat) && ($cat == $j['id'])) $Variable .= '<option value="'.$j['id'].'" selected>'.$j['catname'];
			else $Variable .= '<option value="'.$j['id'].'">'.$j['catname'];
		}
		define('REC_TXT_VALUE',template(C_REC_VAL,array('C_URL'=>C_URL,'C_SNAME'=>C_SNAME)));

		if(isset($a2) && $a2 == 'true')
		{

			$errors='';
			if(
			!isset($cat) || !is_numeric($cat) || !in_array ($cat, $allow_cats) ||
			!isset($title) || empty($title) ||
			!isset($link) || empty($link) ||
			!isset($keywords) || empty($keywords) ||
			!isset($uname) || empty($uname) ||
			!isset($descr) || empty($descr) ||
			!isset($email) || empty($email)
			) $errors .= $w[164].'<br>';

			if(isset($link))
			{
				$link = checkurl($link);
				if (strlen($link) < 3)
				{
					$errors .= $w[12].'<br>';
				}
			}

			if(C_REC_LINK && empty($rec)) $errors .= $w[777].'<br>';
			else
			{
				if(!empty($rec))
				{

					$rec2 = checkurl($rec);
					if (strlen($rec2) < 3)
					{
						$errors .= $w[777].'<br>';
					}
					else {
						$rec = $rec2;
						if(GetURL($link) != GetURL($rec)) $errors .= $w[781].'<br>';
						else
						{
							$handle = @fopen ($rec, 'r');
							@ini_set('default_socket_timeout',2);
							@ini_set('max_execution_time ',2);
							if(!$handle)
							{
								$errors .= template($w[780],array($rec)).'<br>';
							}
							else
							{
								$link_exist = false;
								$buffer=array();
								$buffer[] = fread($handle, 4096);
								do {
									$c = fread($handle, 4096);
									if($c === false) break;
									else $buffer[] = $c;
								} while (!feof($handle));
								fclose($handle);

								$check = (C_REC_EXACT) ? REC_TXT_VALUE : C_URL;
								$p=0;while(isset($buffer[$p]))
								{
									if (stristr($buffer[$p],$check))
									{
										$link_exist = true;
										break;
									}
									$p++;
								}
								if($link_exist === false) $errors .= $w[782].'<br>';
							}

						}
					}
				}
			}

			if(C_VERIMG_LINKS) {
				$verifyimage = cb($verifyimage);
				if(strtoupper($verifyimage) != strtoupper($_SESSION['vc']))	$errors .= $w[497].'<br>';
			}

			# Check if URL already exist
			$tsql = db_fetch_array(db_query("SELECT count(id) as total FROM ".C_MYSQL_LINKS." WHERE url LIKE \"%".$link."%\""));
			$count = $tsql['total'];
			if($count) $errors .= $w[776].'<br>';

			if ($errors == '')
			{
				if(C_LINK_ADD == '0') $cst = 7;
				elseif(C_LINK_ADD == '2') $cst = 2;
				else $cst = 0;

				$codegen = code_gen();
				db_query("INSERT INTO ".C_MYSQL_LINKS." VALUES (Null,'".$cat."','".$link."','".cb($title)."','".cb(substr($descr,0,C_LINK_DESCR_LIMIT))."','".cb(substr($keywords,0,500))."','".cb(substr($uname,0,50))."','".cb($email)."','".$rec."',NOW(),'".$codegen."','".$cst."','0')") or die(db_error());
				$last_id = db_fetch_array(db_query("SELECT LAST_INSERT_ID() AS last_id"));
				$last_id = $last_id['last_id'];

				if($cst == 0)	sendmail(C_ADMINM, $email, $w[778], nl2br($w[779].C_URL.'/links.php?l='.$l.'&confirm=yes&id='.$last_id.'&code='.$codegen), $w[779].C_URL.'/links.php?l='.$l.'&confirm=yes&id='.$last_id.'&code='.$codegen);

				if($cst == 0) printm($w[784]);
				elseif($cst == 2) printm($w[785]);
				else printm($w[783]);

			}
		}


		define('SUBMIT_SITE',$w[769]);
		define('LIMIT',$w[771]);
		define('LIMIT_ERROR',template($w[39],array(C_LINK_DESCR_LIMIT)));
		define('URL',$w[770]);
		define('URL_VALUE',@$link);
		define('KEYWORDS',$w[289]);
		define('KEYWORDS_VALUE',@$keywords);
		define('NAME',$w[772]);
		define('NAME_VALUE',@$uname);
		define('EMAIL',$w[60]);
		define('EMAIL_VALUE',@$email);
		define('EMAIL_ERROR',$w[11]);
		define('TITLE',$w[357]);
		define('TITLE_VALUE',@$title);
		define('REC',$w[774]);
		define('REC_VALUE',@$rec);
		define('REC_TXT',$w[775]);
		define('DESCRIPTION',$w[677]);
		define('DESCRIPTION_VALUE',substr(@$descr,0,C_LINK_DESCR_LIMIT));
		define('VERIFICATION', $w[498]);
		if(C_VERIMG_LINKS) define('VERIFY_ALLOW', True);

		define('CATEGORY',$w[773]);
		define('CATEGORY_VALUE',$Variable);


		$p=0;

		if(isset($errors) && $errors != '') mes($errors);
		include_once C_PATH.'/templates/'.C_TEMP.'/links_submit.php';
		include_once C_PATH.'/footer.php';
	}
}
else
{


	if(isset($cid) && is_numeric($cid))
	{

		if(!isset($from) || !is_numeric($from)) $from = 0;
		if(!isset($step) || !is_numeric($step)) $step = C_SEF_LINKS_CATN;
		$f=0;$p=-1;
		$colors=$ids=$url=$title=$descr=$keywords=$uname=$regdate=array();

		$tmp = New CacheQuery("SELECT * FROM ".C_MYSQL_LINKSCAT." WHERE id = '".$cid."'",IsCache(1,4096));

		if(empty($tmp->data)) printm($w[818],1);
		while(list($k,$j)=@each($tmp->data)) {
			define('CATEGORY_VALUE',$j['catname']);
			define('CID',$j['id']);
		}

		$tmp = New CacheQuery("SELECT count(*) as total FROM ".C_MYSQL_LINKS." WHERE status > '6' AND cid = '".$cid."'",IsCache(1,4096));
		$count = $tmp->data[0]['total'];
		if($count == "0") printm($w[818],1);
		
		$tmp = New CacheQuery("SELECT * FROM ".C_MYSQL_LINKS." WHERE status > '6' AND cid = '".$cid."' order by req DESC, title ASC limit ".$from.",".$step,IsCache(1,4096));
		$color='';
		while(list($k,$j)=@each($tmp->data)) {
			$color = ($color == COLOR4) ? COLOR3 : COLOR4;
			$colors[]=$color;
			$ids[]=$j['id'];
			$title[]= $j['title'];
			$url[]= ($j['title'] == '') ? quot_add($j['url']) : $j['url'];
			$descr[]=quot_add($j['descr']);
			$keywords[]=$j['mkeywords'];
			$uname[]=$j['uname'];
			$regdate[]=$j['regdate'];
		}
		
	$param='l='.$l.'&cid='.$cid;
	$colspan=1;
	$pages = (C_SEF) ? pages($from,$step,$count,$param,$colspan,null,null,null,array($cid,CATEGORY_VALUE),'LinksCat') : pages($from,$step,$count,$param,$colspan);
	define('PAGES',$pages);
	define('SEARCH_RESULTS',search_results($from,$step,$count,True,CATEGORY_VALUE));
		
		$p=0;
		define('DETAIL',$w[787]);
		include_once C_PATH.'/templates/'.C_TEMP.'/links_cat.php';
		include_once C_PATH.'/footer.php';

	}
	else
	{

		$f=0;$p=-1;
		$categories=$cat_id=$cat_links_number=array();
		define('DETAIL',$w[787]);
		define('SUBMIT_SITE',$w[769]);
		$tmp = New CacheQuery("SELECT * FROM ".C_MYSQL_LINKSCAT." order by catname ASC",IsCache(1,4096));
				$stmp = (IsAccess(C_LINK_WHO)) ? '<br><a href="links.php?l='.LANGUAGE.'&a=add">'.SUBMIT_SITE.'</a>' : '';
		if(empty($tmp->data)) printm($w[375].$stmp,1);
		while(list($k,$j)=@each($tmp->data)) {
			$categories[]=$prev_cat=$j['catname'];
			$cat_id[]=$j['id'];
			$cat_links_number[$j['id']] = 0;
		}
		define('CAT_NUM',count($categories));
		$tmp2 = New CacheQuery("SELECT cid, count(*) as c FROM ".C_MYSQL_LINKS." WHERE status > '6' group by cid",IsCache(1,4096));
		while(list($k,$j)=@each($tmp2->data)) {
			$cat_links_number[$j['cid']] = $j['c'];
		}
		if(empty($tmp->data)) printm($w[375].$stmp,1);

		$p=0;

		include_once C_PATH.'/templates/'.C_TEMP.'/links.php';
		include_once C_PATH.'/footer.php';
	}
}
?>