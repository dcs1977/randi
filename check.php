<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               check.php                        #
# File purpose            Check for add user to DB         #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';

include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';

if (!isset($code))
{
	// Code must content only chars and numbers :)
	include_once C_PATH.'/header.php';
	printm($w[1].'3');
}

/// Check for users with code expired and remove these users
$to_remove=array();
$temp = db_query("SELECT id FROM ".C_MYSQL_TEMP." WHERE date < DATE_SUB(NOW(), INTERVAL ".C_REG_DAYS." DAY) AND type = 0");
while ($i = db_fetch_array($temp)){$to_remove[]=$i['id'];}
if(count($to_remove)) RemoveUser($to_remove);
/////////////

$temp = db_query("SELECT count(*) as total FROM ".C_MYSQL_TEMP." WHERE code='".$code."' limit 1");
$row = db_fetch_array($temp);
$count = $row['total'];
//var_dump($count);exit();
if ($count != '0')
{
	$temp = db_query("SELECT id, type FROM ".C_MYSQL_TEMP." WHERE code='".$code."' limit 1");
	$row = db_fetch_array($temp);
	$id = $row['id'];
	$check_type = $row['type'];
	db_query("DELETE FROM ".C_MYSQL_TEMP." WHERE code='".$code."'");
	$result = db_query("SELECT t1.*,t2.* FROM ".C_MYSQL_MEMBERS_MAIN." as t1,".C_MYSQL_MEMBERS_SEC." as t2 WHERE t1.id = '".$id."' AND t1.id = t2.id");
	if ($check_type == 0){ //Regisztráció utáni ellenőrzés
		while ($i = db_fetch_array($result))
		{
			if (C_CHECK_REGISTER == '3')
			{
				$status = (trim($i['files']) == '' && C_CHECK_WPHOTOS) ? '7' : '1';
				$str = $w[159];

				if (C_NOTIFY_ADMIN)
				{
					$tm = array(C_SNAME, C_URL.'/view.php?l='.C_ADMINLANG.'&id='.$i['id']);
					$html = template($wl['admin_notify_new_member']['html'], $tm);
					$text = template($wl['admin_notify_new_member']['text'], $tm);
					sendmail(C_FROMM, C_ADMINM, $wl['admin_notify_new_member']['subject'], $html, $text);
				}
			}
			else
			{
				$status = (C_SHOW_REGISTERED) ? 10 : 7;
				/*if ($status == 10){
					//ip check
					$ipsql = "SELECT count(t3.id) as total FROM ".C_MYSQL_MEMBERS_SEC." as t3 WHERE ip = ".$i['ip']." AND status IN (1,2,5,6)";
								
					$ipquery = db_query($ipsql) or die(db_error());
				
					$iprows = db_fetch_array($ipquery);
					if($iprows['total'] > 0) {
						$status = 1;
					} else {
                        //password check
                        $passsql = "SELECT count(t3.id) as total FROM ".C_MYSQL_MEMBERS_SEC." as t3 WHERE password = '".$i['password']."' AND status IN (1,2,5,6)";

                        $passquery = db_query($passsql) or die(db_error());

                        $passrows = db_fetch_array($passquery);
                        if($passrows['total'] > 0) {
                            $status = 1;
                        } elseif ($i['fingerprint']){
                            //fingerprint check
                            $fpsql = "SELECT count(t3.id) as total FROM ".C_MYSQL_MEMBERS_SEC." as t3 WHERE fingerprint = '".$i['fingerprint']."' AND status IN (1,2,5,6)";

                            $fpquery = db_query($fpsql) or die(db_error());

                            $fprows = db_fetch_array($fpquery);
                            if($fprows['total'] > 0) {
                                $status = 1;
                            }
                        }
					}
				}*/
				$str = $w[46];
							
			}
			ChangeStatus($id, $status);

			$tm = array(C_SNAME);
			$subject = template($w[195], $tm);
			switch (C_ID)
			{
				case '0':
					$sendid = $i['username'];
					break;
				case '2':
					$sendid = $i['email'];
					break;
				default:
					$sendid = $id;
					break;
			}
			$tm = array($sendid, C_SNAME);

			// Sending email with password to user, if user status is active and admin allow to send congratulation emails
			if (($status > '5') && C_REG_EMAIL) {
				$html = template($wl['registration_success']['html'], $tm);
				$text = template($wl['registration_success']['text'], $tm);
				sendmail(C_FROMM, $i['email'], template($wl['registration_success']['subject'], $tm), $html, $text);
			} else {
				$html = template($wl['registration_problem']['html'], $tm);
				$text = template($wl['registration_problem']['text'], $tm);
				sendmail(C_FROMM, $i['email'], template($wl['registration_problem']['subject'], $tm), $html, $text);
			}

			if (C_NOTIFY_ADMIN)
			{
				$tm = array(C_SNAME, C_URL.'/view.php?l='.C_ADMINLANG.'&id='.$i['id']);
				$html = template($wl['admin_notify_new_member']['html'], $tm);
				$text = template($wl['admin_notify_new_member']['text'], $tm);
				sendmail(C_FROMM, C_ADMINM, $wl['admin_notify_new_member']['subject'], $html, $text);
			}

			if (($status > '5') && (C_AUTO_LOGIN))
			{
				if(C_VERIFY_IMAGE_LOGIN) 
				{
					@$_SESSION['vc'] = "REGISTER";
					$tmps="&verifyimage=REGISTER";
				} else $tmps="";
			?>
			<script language="JavaScript">
			<!--
			window.location.replace( '<?=C_URL."/login.php?l=".$l."&".s()."&p=s&id=".$sendid."&password=".$i['password'].$tmps?>');
			// -->
			</script>
			<?php
			die();
			}
		}
	} elseif (in_array($check_type, array(1,2,3,4,5,6,7,8,9,10))){
		//Státusz beállítása
        while ($i = db_fetch_array($result)) {
            $status = $check_type;
            ChangeStatus($i['id'], $status);
            alert_mes($w[160]);
            ?>
            <script language="JavaScript">
                window.location.replace( '<?=C_URL."/login.php?l=".$l?>');
            </script>
            <?php
            die();
        }
    } elseif ($check_type == 11){
        //Jelszóváltoztatás
        while ($i = db_fetch_array($result)) {
            if (in_array($i['status'], array(3,4,7,8,9,10))){
                AutoLogin($i['id']);
                ?>
                <script language="JavaScript">
                    window.location.replace( '<?=C_URL."/members/index.php?l=".$l."&a=p&fpw=1"?>');
                </script>
                <?php
                die();
            }
        }
    } else {
	    $str = $w[508];
    }
	include_once C_PATH.'/header.php';
	printm($str,2);
} else {
	unset($id);
	include_once C_PATH.'/header.php';
	printm($w[161]);
}
include_once C_PATH.'/footer.php';
?>