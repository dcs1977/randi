<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 06.06.2004         #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               ADM_DIR/index.php                #
# File purpose            Admin area                       #
# File created by         AzDG <support@azdg.com>          #
############################################################

error_reporting(0);
$coupon_code = array('diszkret','fiatalokat','nemkamu','nincshely','szexmost','weekend');

## Menu and images 
if(isset($_GET['img']))
{
	$ImagesArray=array(
	'folder'=>
	'R0lGODlhEwAQALMAAAAAAP///5ycAM7OY///nP//zv/OnPf39////wAAAAAAAAAAAAAAAAAAAAAA'.
	'AAAAACH5BAEAAAgALAAAAAATABAAAARQEMlJqxUYA3tPKQQhbJwkDGg6ZFklfHD8EQNpgmGuGzX1'.
	'6jkDQdib/IAhIc+GOAaH0OINqRxKmzho1bD0FbZa7vWkKqeYCIB6zWaX3vA4JQIAOw==',
	'base'=>'R0lGODlhEAAQAOb/ADG1/zm1/0JCnEJCpUq1/0q9/1JSnFKt71LG/1pSe1pShFpSjFpanFqU51ql'.
	'52NahGNajGNjjGNjnGNjpWOExmOl52PO/2tjjGtjnGtrpWtrvWt7vWuM1mvO/3NrlHNzpXO993PW'.
	'/3tznHtztXt7nHt7pXvG93vW/4R7pYSEtYTe/4yMrYyMtYyMvYyUzoyctYyl3oy93oze/5SUrZSU'.
	'tZSUzpS9zpTn/5ycvZycxpy1zpzG3pzO75zn/5zv/6WlvaWlxqWlzqWl1qWt3qXO562txq2tzq21'.
	'1q3v/7W1zrW11rW13rW93rXG57X3/721zr29zr291r29573G1r3G3r3G5733/8bGzsbG1sbG58bO'.
	'zsbO3sbO58b//87O1s7O3s7//9bW1tbW3t7e3t7e7+fn5+fn7+/v7/f398DAwAAAAAAAAAAAAAAA'.
	'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5'.
	'BAEAAGkALAAAAAAQABAAQAe8gGmCg1xbXFRKOUyDjEsOAAEBIDAjCxeMaVNEJzI3PTcyJxsrmFRn'.
	'aGdjWC0MKSksmF4vNkRWYF1INxhAmGlGCBYdwhYIDR69WS5LQjUCAhI4vWlBM0VJRTglChG8jFph'.
	'V1hFPzo7MR9HmFg8Mj1ITkg+Mhk0pU0dISr6Jx0UJL1KDhRAQLAAAQ4PekmpAMDEEA0DEogw0uuM'.
	'GTRllEww8IoKpipeyowREwVHhhYQUEgj8wXKkydQsJAZFAgAOw==',
	'empty'=>'R0lGODlhEwAQAJEAAAAAAP///4CAgP///yH5BAEAAAMALAAAAAATABAAAAIPnI+py+0Po5y02ouz'.
	'3hwVADs=',
	'line'=>'R0lGODlhEwAQAJEAAAAAAP///4CAgP///yH5BAEAAAMALAAAAAATABAAAAIXnG+Cy+2tnpws0ist'.
	'3kvz731bKF5keRQAOw==',
	'folderopen'=>'R0lGODlhEwAQAKIAAAAAAP///5ycAM7OY///nP/OnP///wAAACH5BAEAAAYALAAAAAATABAAAANQ'.
	'aLrcbiLG50S4V9BlifeBJDXd92HBADCl6aqcKEcDASvtVxCFOtyQgGnHswF+K45wyCsIAE/Wckct'.
	'9KBJJbVpHTyzuJkMAFaQz2j0Zs1mJwAAOw==',
	'page'=>'R0lGODlhEwAQALMAAAAAAP///+fn1v//9/f39+/v787OzpycnISEhHNzc1JSUjExMf///wAAAAAA'.
	'AAAAACH5BAEAAAwALAAAAAATABAAAARWkMlJK0U4Z3uD/8GhcBISDGh6BCNnpqmyzC6cGkfSVohN'.
	'DAXc7gL7EQoCw3DSG/yAAqSylipAk8tSFSrNMppWJBLrChTGY7IFYWi7316EYk6ve0l4SQQAOw==',
	'joinbottom'=>'R0lGODlhEwAQAJEAAAAAAP///4CAgP///yH5BAEAAAMALAAAAAATABAAAAIanG+Cy+2tnpws0ist'.
	'3kv58WkcNJaJWYooUgAAOw==',
	'join'=>'R0lGODlhEwAQAJEAAAAAAP///4CAgP///yH5BAEAAAMALAAAAAATABAAAAIXnG+Cy+2tnpws0ist'.
	'3kv58WncSJbmSRYAOw==',
	'plusbottom'=>'R0lGODlhEwAQAJEAAAAAAP///4CAgP///yH5BAEAAAMALAAAAAATABAAAAIrnG+Cy+2tnpxJ2Gtl'.
	'XBsecHTMBibaAKTpoLQsQoanUb6PaN4YRvUO7nsUAAA7',
	'plus'=>'R0lGODlhEwAQAJEAAAAAAP///4CAgP///yH5BAEAAAMALAAAAAATABAAAAIqnG+Cy+2tnpxJ2Gtl'.
	'XBsecHTMBibaAKTpoLQsQoanUb6PaN4YRvX+/ygAADs=',
	'minusbottom'=>'R0lGODlhEwAQAJEAAAAAAP///4CAgP///yH5BAEAAAMALAAAAAATABAAAAImnG+Cy+2tnpxJ2Gtl'.
	'XBv61x0hNwCmOShqiows+DmuK2IYhcv5XgAAOw==',
	'minus'=>'R0lGODlhEwAQAJEAAAAAAP///4CAgP///yH5BAEAAAMALAAAAAATABAAAAImnG+Cy+2tnpxJ2Gtl'.
	'XBv61x0hNwCmOShqiows+DmuK2IYhef6UwAAOw==',
	'expand'=>'R0lGODlhEAAQAKL/AABjzimEIbXe/8DAwP///wAAAAAAAAAAACH5BAEAAAMALAAAAAAQABAAQAMw'.
	'OLqs0QPAqeR8zdIFCBHaEIwjA5zdF5qoB24wh64ii0KYWc2UNdOV3W3i4xFjyEYCADs=',
	'collapse'=>'R0lGODlhEAAQAKL/AABjzqUhALXe/+cpAMDAwP///wAAAAAAACH5BAEAAAQALAAAAAAQABAAQAMt'.
	'SLrcDC4qKB2tsxRxSfjfA4waJ43k1mEn2g0wPLnsNKPnPeaEuy4UH69GJCQAADs=',
	'user-edit'=>'R0lGODlhEAAQAOb/ABB7EBCEEBicGCGlISmcKTExMTGlMTGtMTmcOTmtOUohIUpae0pzGEqlUkq9'.
	'SlJSOVJjc1JrKVLGUmNCCGO9Y2PGY2POY2taMWtjUmvGa2vOa3POc3tjKXtrMXuEjHu9jIxzOZRz'.
	'OZyEY6VrIaVzIa17Ka2EOa2MQq2MSrWUSr17Ib2UUr2la8Z7GMaEIcaUSsala9alENata96lQt6t'.
	'WucQAOeMIeeMKeecSue9c+e9jO+cKe+lQu+1Su/WCO//9/dzKfecOfelQvetY/e1UsDAwP+cOf+l'.
	'Qv+tSv+1Sv+1Uv+9Uv+9Wv/3jP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'.
	'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'.
	'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5'.
	'BAEAAEUALAAAAAAQABAAQAeMgEWCg4SFhS02NzYRhE4QCoIrPExMRD1EIIaCAwcOEggfMT5AE5ok'.
	'Mjo5NB2agy5HsEdBF4U1gw4bDU5OGRuipK1FCxwYwUUsKSgixiIkLzJDOCPBMEJJSktKSCHBJUbf'.
	'RjcPhDtAhAwmKicUCIIzHgWDBA4aGT8/FRajpYQBAgYJEgBoAsxYkXiaAgEAOw==',
	'selected'=>'R0lGODlhEAAQAMT/AABKAABSAABjACGcCCGcECmlEDmtEDmtOTm1EEq1GFK9SlK9UlK9WlrGWmPG'.
	'Y2PGa2vGa3vWe4zWjMDAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEA'.
	'ABMALAAAAAAQABAAQAU/4CSOZGkKiaEWQmCKgiMJwEsKA22TgL6PggbkofC9BIVCrvZrOm2Cg3En'.
	'gEQi09OCoei2oAjlkkoY/3rM5y4EADs=',
	'settings'=>'R0lGODlhEAAQAOb/AABSvQBaxgBr1ghKrRA5cxBStRBaxhBjzhBz1hg5YxhKhBhjvSExUiE5YyFC'.
	'cyFKhClCWilSnClrximM5zFKWjGE1jljlDlzxkJ7xkJ7zkKMzkpKSkqExlqt/1rG/2N7jGOU1mtr'.
	'a2uEnGuMrWuUvXOUlHOt53O193uMlHut3oRaWoSEhIScpYS994TW/4yMjIyUnIy175SUlJScpZS9'.
	'55S995TO/5xKQpycrZylrZy13qWtraW9va21va3G57VSSrXO57Xe/72lOb3n/8bO78be/8bn/86c'.
	'AM6tSs7WztZaMdZrQtatOdbWrdbn/9bv/97Wvd7n3t7v9+fnzufv9+f3/+/v1u/39+/3//f3/8DA'.
	'wP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'.
	'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5'.
	'BAEAAFoALAAAAAAQABAAQAe5gFqCg4SFgiISTkxQTUcsMTYcFiEygiMZWVuaVlMtDCuFMAU0T0VB'.
	'LRwRDIY9EwGvAAYYFxSVhIhYWVVVSFI0CR8xOEo/WigLQE9JR0EeJw2ghDkRDw4NGz2GhT05MBoV'.
	'CAK20gcXIDpEVFs0ENGDIphbWVhNUzWfWj1LN1qIVFdVoggxYoKDDRskVIQwtqBIlCNHeJQAoWDF'.
	'OGMFfDgxNcRFBwIXtcwYkCIGhgjXshna8SKESm2CAgEAOw==',
	'exit'=>'R0lGODlhEAAQANX/AABatQBaxgBjzgBrzgBz1gB73gB75wB77wCEAACE9wCE/wCcAAh73giE9xBa'.
	'rRhSlBh75xiE3hilCBitEClz3jGtITG9ITlKjDm9KUJCQkrGKUrGMWNjjGvWQoSEhMbn/9bv/9b3'.
	'/973/8DAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'.
	'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAACMALAAAAAAQABAAQAaCwJFw'.
	'SCwKOYNBRETIIJ6LBWfIUSQO2Exou50eAQAHIJCRPBES75FDgSgynricSiAU7hmQXq/mBASAAhkb'.
	'FhYaHWpDGYsZRkYckI2OI1UKDQlvXF1UBQaeB1oioiJ9dQx1GR+qqn0CSQNNCBUYG4hUAbh/gxO8'.
	'UlSLFw8AGZDFiZNGQQA7'
	);
	header("Content-type: image/gif");
	header("Cache-control: public");
	header("Expires: ".date("r",mktime(0,0,0,1,1,2037)));
	header("Cache-control: max-age=".(60*60*24*7));
	header("Last-Modified: ".date("r",filemtime(__FILE__)));
	echo base64_decode($ImagesArray[$_GET['img']]);
	exit();
}   
##########
define('ADMIN_AREA', 1);
include_once '../include/config.inc.php';
include_once C_PATH.'/include/options.inc.php';
# Including not once, because it will be required in "Mailing to subscribed users" section

include(C_PATH.'/languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'.php');
include(C_PATH.'/languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'l.php');
include(C_PATH.'/languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'_.php');
include(C_PATH.'/languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'a.php');
include_once C_PATH.'/include/functions.inc.php';

# Compability with moder area
define('ADM_DIR',C_ADMINDIR);

# Version
define('AC_VERSION','Medium 1.9.4');
define('AC_RELEASE_DATE',@$wmm[2].' 22, 2009');

define('C_VERSION','1.9.4');
define('C_RELEASE_DATE',@$wmm[2].' 22, 2009');

# Setting version
$C_VERSION = AC_VERSION;
$C_RELEASE_DATE = AC_RELEASE_DATE;

$wpu=array($wpu[0]); // Medium version allow to use Free membership only

function admin_security($error)
{
	unset($s);
	unset($m);
	unset($adminlogin);
	unset($adminpass);
	unset($adminip);
	global $stime, $w, $l, $x, $c_adminl, $c_adminp;
        
        foreach ($c_adminl as $key => $login_name){
            if(get_info('adminlogin') == md5($login_name)) {
                $pass = $c_adminp[$key];
                $login = $login_name;
            }
        }
        
	if ((((C_SECURITY == '1') || (C_SECURITY == '3')) && get_info('adminip') != md5(ip())) || (get_info('adminlogin') != md5($login)) || (get_info('adminpass') != $pass))
	{
		if (C_AUTH)
		{
			// If working on Sessions
			session_destroy();
			unset($adminlogin);
			unset($adminpass);
			unset($adminip);
		}
		else
		{
			del_login('adminlogin');
			del_login('adminpass');
			del_login('adminip');
		}
		include_once C_PATH.'/header.php';
		printm($error);
	}
}

function cbm($ss) {
	$ss = str_replace("\r\n"," <br>",$ss);
	$ss = str_replace("\\","",$ss);
	$ss = str_replace("'","&rsquo;",$ss);
	$ss = str_replace('"',"&quot;",$ss);
	$ss = trim($ss);
	return $ss;
}
function tbm($ss) {
	$ss = str_replace("<br>","\r\n",$ss);
	$ss = str_replace("&rsquo;","'",$ss);
	$ss = str_replace('&quot;','"',$ss);
	return $ss;
}
function menu($do,$word)
{
	global $l;
	$mitem = '<a href="'.C_URL.'/'.C_ADMINDIR.'/index.php?do='.$do.'&'.s().'" class=desc>['.$word.']</a> ';
	echo $mitem;
}
function nmenu($do,$word)
{
	global $l;
	$mitem = "'".$word."','".$do."'";
	echo $mitem;
}
function ChangeConfig($file)
{
	$cnt = file($file);
	$fp = fopen($file,"w");
	flock($fp, LOCK_EX);
	for ($i=0;$i<count($cnt);$i++)
	{
		reset($_GET);reset($_POST);
		if (strpos($cnt[$i],"','"))
		{
			list($param,$value) = split("','",$cnt[$i]);

			while(list($names,$values) = each($_POST))
			{
				if((substr($names,0,2) == 'c_') && (substr($names,-2) != '__'))
				{

					$newname=$names.'__';
					if (isset($_POST[$newname]))
					{
						if (trim($param) == "define('".strtoupper($names))
						{
							$cnt[$i] = "define('".strtoupper($names)."','1');\n";
						}
					}
					else
					{
						#$values=str_replace('"','&quot;',$values);
						$values=str_replace("'","&rsquo;",$values);
						$values=str_replace("\r\n","",$values);
						$values=str_replace("\n","",$values);
						if (trim($param) == "define('".strtoupper($names))
						{
							$cnt[$i] = "define('".strtoupper($names)."','".$values."');\n";
							#continue 2;
						}
					}
				}
				elseif(substr($names,-2) == '__')
				{
					$newname=substr($names,0,-2);
					if (!isset($_POST[$newname]))
					{
						if (trim($param) == "define('".strtoupper($newname))
						{
							$cnt[$i] = "define('".strtoupper($newname)."','0');\n";
						}
					}
				}
			}
		}
	}

	fwrite($fp, implode("",$cnt));
	fflush($fp);
	flock($fp, LOCK_UN);
	fclose($fp);
}
function ChangeVars($file)
{
	$cnt = file($file);
	$fp = fopen($file,"w");
	flock($fp, LOCK_EX);
	for ($i=0;$i<count($cnt);$i++)
	{
		reset($_GET);reset($_POST);
		if (strpos($cnt[$i],'=array'))
		{
			list($param,$value) = split('=array',$cnt[$i]);
			while(list($names,$values) = each($_POST))
			{
				if(substr($names,0,5) == 'vars_')
				{
					$origname=trim(substr($names,5,4));
					if (trim($param) == '$'.$origname)
					{
						$j=0; $tmpvar=$names.$j; $str='';
						while(isset($_POST[$names][$j]))
						{
							$str.="'".$_POST[$names][$j]."',";
							$j++;
						}
						$cnt[$i] = $param."=array(".substr($str,0,-1).");\r\n";
					}
				}
			}
		}
	}
	fwrite($fp, implode("",$cnt));
	fflush($fp);
	flock($fp, LOCK_UN);
	fclose($fp);
}

function ShowForEdit($Const, $Bit, $data)
{
	Return ((($Const & $Bit) && !(C_EDITVIEWEMPTY && empty($data))) || (FULLMODE)) ? True : False;
}
$MySQLTables=array(C_MYSQL_MEMBERS,C_MYSQL_MEMBERS_MAIN,C_MYSQL_MEMBERS_EXT,C_MYSQL_MEMBERS_SEC,C_MYSQL_MEMBERS_RESTRICTS,C_MYSQL_ONLINE_USERS,C_MYSQL_TEMP,C_MYSQL_PMEMBERS,C_MYSQL_MESSAGES,C_MYSQL_MESSAGES_ARCHIVE,C_MYSQL_CARDS,C_MYSQL_HITS,C_MYSQL_FAQCAT,C_MYSQL_FAQ,C_MYSQL_FORUMCAT,C_MYSQL_FORUM,C_MYSQL_FORUM_MESSAGES,C_MYSQL_FORUM_THREADS,C_MYSQL_BAD_PROFILES,C_MYSQL_BEDROOM,C_MYSQL_MAILLIST,C_MYSQL_RECOMMEND,C_MYSQL_AFFILIATES,C_MYSQL_BANLIST,C_MYSQL_NEWS,C_MYSQL_LINKSCAT,C_MYSQL_LINKS,C_MYSQL_OPTIONS,C_MYSQL_SUBSCRIBE,C_MYSQL_PROFILEVIEW,C_MYSQL_VOTES,C_MYSQL_VOTE_IPS,C_MYSQL_VOTE_QUESTIONS,C_MYSQL_BLACKLIST,C_MYSQL_COMMENTS,C_MYSQL_NEW_PHOTOS,C_MYSQL_SEARCH,C_MYSQL_COUPONS,C_MYSQL_ADDRESSBOOK,C_MYSQL_MEMBERS_FORUM,C_MYSQL_FORUM_SEARCH,C_MYSQL_TESTIMONIALS,C_MYSQL_ARTCAT,C_MYSQL_ART,C_MYSQL_BLOGS,C_MYSQL_PRIVATE_PHOTOS,C_MYSQL_ZIP_DATA,C_MYSQL_NOTES,C_MYSQL_IP2COUNTRY,C_MYSQL_HOTORNOT,C_MYSQL_BLOGS_CAT,C_MYSQL_BLOGS_COM,C_MYSQL_BLOGS_VOTE,C_MYSQL_ALLHITS,C_MYSQL_ADDRESSBOOK_ARCHIVE);

function RepairAndOptimize($rep,$opt,$tables)
{
	$MySQLTables=$tables;
	$rept=array_intersect($MySQLTables,$rep);
	$optt=array_intersect($MySQLTables,$opt);
	reset($rept);reset($optt);
	if(count($rept)) REOP($rept,'REPAIR');
	if(count($optt)) REOP($optt,'OPTIMIZE');
}

function REOP($table,$oper)
{
	global $x;
	$query = $oper." TABLE ";
	while (list(, $v) = each ($table)) {
		$query .= " `$v`,";
	}
	$query = substr($query,0,-1);
	$result = @db_query($query);
	$res = "<BR><Table CellSpacing='".C_BORDER."' CellPadding='0' width='600' bgcolor='".C_TBCOLOR."'><Tr><Td><Table Border=0 CellSpacing='".C_IBORDER."' CellPadding='".C_CELLP."' width='600' class=mes><Tr align='center' bgcolor='".COLORH."'><td>".$x[641]."</td><td>".$x[644]."</td><td>".$x[645]."</td><td>".$x[464]."</td></tr>";
	while ($line = db_fetch_array($result,MYSQL_ASSOC)) {
		$res .= "<Tr align=left bgcolor=".colors().">";
		while (list(, $col_value) = each ($line)) {
			$res .= "<td>$col_value</td>";
		}
		$res .= "</tr>";
	}
	$res .= "</Table></Td></Tr></Table><BR><BR>";
	echo $res;
}

function ApproveUser($id)
{
	Global $w,$wl,$x;
	if(!is_array($id)) $arr=array($id);
	else $arr=$id;

	$p=0;while(isset($arr[$p])) {
		if(!is_numeric($arr[$p])) sprintm('Security Error - ID should be numeric');
		$p++;}

		$id=implode(',',$arr);
		if(C_REG_EMAIL)
		{
			$have_usr = (C_ID == '0') ? 't1.username,' : '';
			$sql=db_query("SELECT ".$have_usr." t1.status, t2.mylang, t3.email, t3.password FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2, ".C_MYSQL_MEMBERS_SEC." as t3  WHERE t1.id IN (".$id.") AND t1.id = t2.id AND t1.id = t3.id") or die(db_error());
			while($i=db_fetch_array($sql))
			{
				if($i['status'] == '1' || $i['status'] == '0')
				{
					$lang = (file_exists(C_PATH.'/languages/'.$i['mylang'].'/'.$i['mylang'].'.php')) ? $i['mylang'] : C_ADMINLANG;
					@include C_PATH.'/languages/'.$lang.'/'.$lang.'.php';
					@include C_PATH.'/languages/'.$lang.'/'.$lang.'l.php';
					$tm=array(C_SNAME);
					$subject=template($w[195],$tm);
					switch (C_ID)
					{
						case '0':
							$sendid=$i['username'];
							break;
						case '2':
							$sendid=$i['email'];
							break;
						default:
							$sendid=$id;
							break;
					}
					$tm = array($sendid, C_SNAME);
					$html = template($wl['registration_success']['html'], $tm);
					$text = template($wl['registration_success']['text'], $tm);
					sendmail(C_FROMM, $i['email'], template($wl['registration_success']['subject'], $tm), $html, $text);
				}
			}
		}
		ChangeStatus($arr,'7');
		@db_query("DELETE FROM ".C_MYSQL_TEMP." WHERE id IN (".$id.")");

}

function SuspendUser($id)
{
	ChangeStatus($id,'5');
}


function ChangeConstant($file,$constant,$oldvalue,$newvalue)
{
	$cnt = file($file);
	$fp = fopen($file,"w");
	flock($fp, LOCK_EX);
	for ($i=0;$i<count($cnt);$i++)
	{
		if (strpos($cnt[$i],"','"))
		{
			list($param,$value) = split("','",$cnt[$i]);
			if (trim($param) == "define('".strtoupper($constant))
			{
				$cnt[$i] = "define('".strtoupper($constant)."','".$newvalue."');\n";
			}
		}
	}
	fwrite($fp, implode("",$cnt));
	fflush($fp);
	flock($fp, LOCK_UN);
	fclose($fp);
}

function JSEditor()
{
	echo '<script language="javascript" type="text/javascript" src="'.C_URL.'/classes/editor/tiny_mce.js"></script><script language="javascript" type="text/javascript">tinyMCE.init({mode : "textareas",theme : "advanced",plugins : "table,advimage,advlink,preview,zoom,searchreplace,contextmenu,paste,directionality,fullscreen",theme_advanced_buttons1_add : "fontselect,fontsizeselect",theme_advanced_buttons2_add : "separator,ltr,rtl,separator,forecolor,backcolor",theme_advanced_buttons2_add_before: "cut,copy,paste,separator,search,separator",theme_advanced_buttons3_add_before : "tablecontrols,separator",theme_advanced_buttons3_add : "preview,zoom,fullscreen",theme_advanced_toolbar_location : "top",	theme_advanced_toolbar_align : "left",theme_advanced_statusbar_location : "bottom",content_css : "example_word.css",	paste_use_dialog : false,theme_advanced_resizing : true,theme_advanced_resize_horizontal : false,paste_auto_cleanup_on_paste : true,paste_convert_headers_to_strong : false,paste_strip_class_attributes : "all",paste_remove_spans : false,paste_remove_styles : false});</script>';
}

function options($var,$lang1,$lang0) {
	global $x,$w;
	$str='';
	$str .= ($var == '1') ? ' <option selected value="1">'.$lang1 :  '<option value="1">'.$lang1;
	$str .= ($var == '0') ? ' <option selected value="0">'.$lang0 :  '<option value="0">'.$lang0;
	return $str;
}
function select($var,$min,$max,$step) {
	$str='';
	for($i=$min;$i<=$max;$i+=$step) {
		$str .= ($var == $i) ? ' <option selected>'.$i :  '<option>'.$i;
	}
	return $str;
}
function checkbox($var,$name) {
	global $x,$w;
	$str='';
	$str .= ($var == '1') ? '<input type="checkbox" name="'.$name.'" checked>' :  '<input type="checkbox" name="'.$name.'">';
	$str .= '<input type="hidden" name="'.$name.'__" value="1">';
	return $str;
}
function select_access($descr,$name,$enable_guests,$var,$color=null)
{
	Global $w,$wpu;
	$color = ($color==null) ? colors() : colors(1);
	$str = '<tr class=desc bgcolor="'.$color.'"><td width=70%>'.$descr.'</td><td><select name="'.$name.'" class=input>';

	if($enable_guests == '1')
	{
		$str .= ($var == '-1') ? ' <option selected value="-1">'.$w[361] :  '<option value="-1">'.$w[361];
	}
	else
	{
		$str .= '';
	}

	$i = 0;
	while(isset($wpu[$i]))
	{
		if(($enable_guests != '2') || ($i != '0'))
		{
			$str .= ($var == $i) ? ' <option selected value="'.$i.'">'.$wpu[$i] :  '<option value="'.$i.'">'.$wpu[$i];
		}
		$i++;
	}

	if($enable_guests != '2')
	{
		$str .= ($var == '256') ? ' <option selected value="256">'.$w[370] :  '<option value="256">'.$w[370];
	}
	$str .= '</select></td></tr>';
	return $str;
}
function show_checkbox($descr,$var,$name,$colspan=null)
{
	global $x,$w,$color;
	$clsp = ($colspan==null) ? '' : ' colspan='.$colspan;
	$str='<tr class=desc bgcolor="'.colors().'"><td width=70%>'.$descr.'</td><td'.$clsp.'>';
	$str .= ($var == '1') ? '<input type="checkbox" name="'.$name.'" checked>' :  '<input type="checkbox" name="'.$name.'">';
	$str .= '<input type="hidden" name="'.$name.'__" value="1"></td></tr>';
	return $str;
}
function show_input($descr,$var,$name,$class)
{
	global $x,$w,$color;
	$str='<tr class=desc bgcolor="'.colors().'"><td width=70%>'.$descr.'</td><td><input type="text" name="'.$name.'" class="'.$class.'" value="'.$var.'"></td></tr>';
	return $str;
}
function ShowVars($descr,$arr,$var,$name,$acc=null)
{
	global $x,$w,$color,$wpu;

	$str='<Tr><Td bgcolor="'.C_TBCOLOR.'" colspan=2></Td></Tr><tr class=desc bgcolor="'.COLORH.'"><td width=100% colspan=2 align=center>'.$descr.'</td></tr><Tr><Td bgcolor="'.C_TBCOLOR.'" colspan=2></Td></Tr>
';
	$i=0;while(isset($arr[$i]))
	{
		if($acc != null)
		{
			$str.='<tr class=desc bgcolor="'.colors().'"><td width=70%>'.$arr[$i].'</td><td><select name="vars_'.$name.'[]" class=minput>';
			$k = 0;
			while(isset($wpu[$k]))
			{
				$str .= ($var[$i] == $k) ? ' <option selected value="'.$k.'">'.$wpu[$k] :  '<option value="'.$k.'">'.$wpu[$k];
				$k++;
			}
			$str.='</select></td></tr>';
		}
		else $str.='<tr class=desc bgcolor="'.colors().'"><td width=70%>'.$arr[$i].'</td><td><input type="text" name="vars_'.$name.'[]" class="minput" value="'.$var[$i].'"></td></tr>';
		$i++;
	}
	return $str;
}

function GenField($const,$req,$show,$var,$arr,$windex)
{
	Global $w,$i,$$var;
	$Require = ($req) ? $w[0] : '';$Variable = '';
	define(strtoupper($const), $w[$windex].$Require);$p = 1;
	$pur= (strpos($i[$var],',')) ? explode(',',$i[$var])  : array($i[$var]);
	while (isset($arr[$p])){
		$pur2 = @array_flip($pur);
		if (isset($pur2[$p])) $Variable .= '<label for="'.$var.$p.'" class="cb"><input name="'.$var.'[]" value="'.$p.'" type="checkbox" id="'.$var.$p.'" onclick="hld(this);" checked>'.$arr[$p].'</label>';
		else $Variable .= '<label for="'.$const.$p.'" class="lb"><input name="'.$var.'[]" value="'.$p.'" type="checkbox" id="'.$var.$p.'" onclick="hld(this);">'.$arr[$p].'</label>';$p++;}
		define(strtoupper($const.'_VALUE'), $Variable);
}

function GetThumbnails($df)
{
	Global $files;
	$df=str_replace("//","/",$df);
	if ($dir=opendir($df)) {
		while (($file=readdir($dir))!==false) {
			if ($file=="." || $file=="..") continue;
			if (is_dir("$df/$file"))  {
				GetThumbnails("$df/$file");
			}
			elseif(is_file("$df/$file") && substr($file,0,1) == 't') {
				$files[]="$df/$file";
			}
		}
	}
	closedir($dir);
}

function RemoveFiles($files_array)
{
	if(count($files_array))
	{
		$p=0;while(isset($files_array[$p]))
		{
			unlink($files_array[$p]);
			$p++;
		}
	}
}

function OptionsByACL($minAcs, $head, $fld1, $const, $not_inc=null)
{
		Global $w,$x,$wpu;
		$str='';for($i=0;$i<count($fld1);$i++) {$str.='document.forms[0].c_'.$i.'.value+","+';}
		$Bconst = strtoupper($const);
		if($not_inc == null) $not_inc = array();
?>
<script language="Javascript">function CountChecked(){document.forms[0].<?=$const?>.value = <?=substr($str,0,-5)?>;}</script>
    <?php table_header($head)?>
    <input type=hidden name=<?=$const?> value="<?=constant($Bconst)?>">
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
     <tr class=desc bgcolor="<?=colors()?>"><td width=70% align=center><?=$x[564]?></td>
      <td align=center><?=$w[116]?></td></tr>
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
      <?php 
      $arr=explode(',',constant($Bconst));
      for ($p = 0; $p < count($fld1); $p++) {
      	$k = 1;
      	$var = isset($arr[$p]) ? $arr[$p] : -1;
      	if(!in_array($p, $not_inc)) {
      		$str = '<tr class=desc bgcolor="'.colors().'"><td width=30%>'.$p.'. '.$fld1[$p].'</td><td><select name="c_'.$p.'" class=minput>';
      		if($minAcs == -1) $str .= ($var == '-1') ? ' <option selected value="-1">'.$w[361] :  '<option value="-1">'.$w[361];
      		$i = 0;
      		while(isset($wpu[$i]))
      		{
      			$str .= ($var == $i) ? ' <option selected value="'.$i.'">'.$wpu[$i] :  '<option value="'.$i.'">'.$wpu[$i];
      			$i++;
      		}
      		$str .= ($var == '256') ? ' <option selected value="256">'.$w[370] :  '<option value="256">'.$w[370];
      		$str .= '</select></td></tr>';
      	} else $str = '<tr class=desc bgcolor="'.colors().'"><td width=30%>'.$p.'. '.$fld1[$p].'</td><td><select name="c_'.$p.'" class=minput disabled><option selected value="-1">'.$w[361].'</select></td></tr>';
      	echo $str;
      }
			?> 
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
    <?php table_footer()?>
<?php
}

function SearchPhotoResultAdmin($acs,$files,$id,$gender)
{
	Global $l;
	$photo_wh=array();
	if($files == '')
	{
		if(C_NO_PHOTO)
		{
			Return '<a href="'.C_URL.'/'.C_ADMINDIR.'/index.php?do=vp&ds=1&id='.$id.'" target="_blank"><img src="'.C_URL.'/templates/'.C_TEMP.'/images/'.str_replace("{G}",$gender,C_NO_PHOTO_FILE).'" border="0"></a>';
		}
		else
		{
			Return '';
		}
	}
	else
	{
		if(C_SEARCH_PHOTO && $acs) {
			$photol=ShowPic(not_empty($files,$id));
			$link = C_URL.'/'.C_ADMINDIR.'/index.php?do=vp&ds=1&id='.$id;
			if(C_SEARCH_PHOTO_AUTORESIZE) {
				$photo_wh=PhotoSize(C_PATH.'/members/uploads/'.not_empty($files,$id));
				$width = ($photo_wh[0] >= $photo_wh[1]) ? ' width="'.C_SEARCH_PHOTO_WIDTH.'"' : ' height="'.C_SEARCH_PHOTO_WIDTH.'"';
			} else $width=' width="'.C_SEARCH_PHOTO_WIDTH.'"';
			$pictures='<a href="'.$link.'" target="_blank"><img src="'.$photol.'" border="'.C_IMG_BRDR.'"'.$width.' hspace="0" vspace="0" align="middle"></a>';
		} else $pictures = '';
		Return $pictures;
	}
}


include_once C_PATH.'/templates/'.C_TEMP.'/config.php';
admin_security($x[6]);
if(file_exists(C_PATH."/install.php")) {include_once C_PATH.'/sheader.php';sprintm($x[545],2);}

if(!isset($ds)) $ds='0';


if(isset($frame) && $frame == 'menu') {

	if(!C_REGISTER_TYPE) {
		define('REGISTER_LINK',C_URL.'/add.php?l='.C_ADMINLANG);
	} else {
		define('REGISTER_LINK',C_URL.'/register.php?l='.C_ADMINLANG);
	}

	$tmp2="SELECT count(id) as total FROM ".C_MYSQL_MESSAGES." WHERE toid='".get_info('m')."' AND readed != '0'";
	$tquery = db_query($tmp2) or die(db_error());
	$trows = db_fetch_array($tquery);
	$count = $trows['total'];
	$new_mes = '';
	if($count != "0") $new_mes = ' <B>['.$count.']</B>';

			?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 TRANSITIONAL//EN"><html dir=<?=C_HTML_DIR?>><head><title><?=C_SNAME?></title><link rel="stylesheet" type="text/css" href="<?=C_URL?>/templates/<?=C_TEMP?>/style.css"><META HTTP-EQUIV="Expires" Content="0">
<meta http-equiv=Content-Type content="text/html; charset=<?=C_CHARSET?>"><script language="JavaScript">
<!--
function tree(A,B){this.C=B;this.AX=A;this.D=this;this.E=[];this.F=[];this.G=null;this.H=-1;var I=new Image(),J=new Image();I.src=B['icon_e'];J.src=B['icon_l'];B['im_e']=I;B['im_l']=J;for(var i=0;i<128;i++)if(B['icon_'+i]){var K=new Image();B['im_'+i]=K;K.src=B['icon_'+i]}this.L=function(M){var N=this.E[M];N.open(N.n_state&8)};this.O=function(M){return this.E[M].O()};this.P=function(M){var N=this.E[M];N.Q();N.P(true)};this.R=function(M){var N=this.E[M];N.Q(true);N.R(true)};this.S=function(T){for(var i=0;i<this.E.length;i++)if(this.E[i].AX[0]==T){return(this.E[i]);break}};this.U=false;this.V=function(){if(!W&&this.U)window.location=window.location};this.X=function(){document.cookie='tree_'+this.D.H+'_state=';this.Y=[]};this.a_children=[];for(var i=0;i<A.length;i++)this.a_children[this.a_children.length]=new Z(this,i);W=Boolean(document.body&&document.body.innerHTML);this.M=a.length;a[this.M]=this;this.Y=[];this.Az=[];var b=/^\s*(\S+)\s*=\s*(\S*)\s*$/,c=document.cookie.split(';'),d,e,d='tree_'+this.D.M+'_state',Ax='tree_'+this.D.M+'_slate';for(var i=0;i<c.length;i++){if(b.exec(c[i])){if(RegExp.$1==d&&!this.Y.length){e=RegExp.$2;this.Y=e.split('_')}else if(RegExp.$1==Ax&&!this.Az.length){e=RegExp.$2;this.Az=e.split('_')}}}if(B['beforeInit']){eval('var f='+B['beforeInit']+'(return);');if(!f)return}for(var i=0;i<this.a_children.length;i++){this.a_children[i].g=h;document.write(this.a_children[i].g())}if(B['afterInit'])eval(B['afterInit']+'(return);')}function Z(o_parent,k){this.D=o_parent.D;this.H=o_parent.H+1;this.AX=o_parent.AX[k+(this.H?3:0)];while(!this.AX[this.AX.length-1])this.AX.length=this.AX.length-1;this.M=this.D.E.length;this.D.E[this.M]=this;if(this.AX.length<4)return;this.l=this.D.F.length;this.D.F[this.l]=this;for(var i=3;i<this.AX.length;i++)new Z(this,i-3)}function m(n,o){if(Boolean(this.n_state&8)!=Boolean(n))return;var p=(this.AX[2]?this.AX[2][n?'hc':'ho']:null);p=(p?p:this.D.C[n?'onItemClose':'onItemOpen']);if(p){eval('var f='+p+'(this);');if(!f)return}this.n_state^=8;this.U=true;this.q();this.Q();this.r();if(W){var s=t('c'+this.D.M+'_'+this.M);if(!s.innerHTML)s.innerHTML=this.u();s.style.display=(n?'none':'block')}else if(!o)window.location=window.location}function vv(w){var p=(this.AX[2]?this.AX[2][w?'hd':'hs']:null);p=(p?p:this.D.C[w?'onItemDeselect':'onItemSelect']);if(p){eval('var f='+p+'(this);');if(!f)return}if(w){this.n_state&=~4}else{var x=this.D.G;this.D.G=this;if(x)x.O(true);this.n_state|=4};this.Qa();this.q();this.Q();return Boolean(this.AX[1])}function y(){var p=(this.AX[2]?this.AX[2]['hv']:null);p=(p?p:this.D.C['onItemMover']);if(p){eval('var f='+p+'(this);');if(!f)return}this.n_state|=64;this.q()}function z(){var p=(this.AX[2]?this.AX[2]['hu']:null);p=(p?p:this.D.C['onItemMout']);if(p){eval('var f='+p+'(this);');if(!f)return}this.n_state&=~64;this.q()}function AA(AB){window.setTimeout("window.status='"+(AB?'':(this.AX[2]&&this.AX[2]['sb']?this.AX[2]['sb']:this.AX[0]+(this.AX[1]?' ('+this.AX[1]+')':'')))+"'",10)}function h(){var E=this.D.E,k=0,M=this.M,o_parent;while(true){M--;if(M<0)break;if(E[M].H<this.H){o_parent=E[M];break}if(E[M].H==this.H)k++}this.o_parent=o_parent?o_parent:this.D;this.k=k;this.q=AC;this.P=y;this.R=z;this.O=vv;this.Q=AA;this.Qa=Ay;this.AE=AF;if(this.AX.length>3){M=this.M;this.a_children=[];while(true){M++;if(M==E.length)break;if(E[M].H<=this.H)break;if(E[M].H==this.H+1){E[M].g=h;this.a_children[this.a_children.length]=E[M]}}this.open=m;this.r=AD;this.u=AG}else{this.open=function(){alert("Only nodes can be opened. id="+this.M)}}this.n_state=(this.H?0:32)+(this.a_children?16:0)+(this.k==this.o_parent.a_children.length-1?1:0);var AH=(this.D.C['style_icons']?' class="'+this.D.C['style_icons']+'"':''),AI=[],AJ=this.o_parent,AK=this.AX[2];for(var i=this.H;i>1;i--){AI[i]='<img src="'+this.D.C[AJ.n_state&1?'icon_e':'icon_l']+'"'+AH+' border="0">';AJ=AJ.o_parent}this.AE();var AL=this.q(true);return '<table cellpadding="0" cellspacing="0" border="0"><tr onmouseover="a['+this.D.M+'].P('+this.M+')" onmouseout="a['+this.D.M+'].R('+this.M+')"><td nowrap>'+AI.join('')+(AL[1]?(this.a_children?'<a href="javascript: a['+this.D.M+'].L('+this.M+')" onmouseover="a['+this.D.M+'].P('+this.M+')" onmouseout="a['+this.D.M+'].R('+this.M+')"><img src="'+AL[1]+'" border="0" name="j'+this.D.M+'_'+this.M+'"'+AH+'></a>':'<img src="'+AL[1]+'" border="0"'+AH+'>'):'')+(AL[0]?'<a href="'+this.AX[1]+'" target="'+(AK&&AK['tw']?AK['tw']:this.D.C['target'])+'" title="'+(AK&&AK['tt']?AK['tt']:'')+'" onclick="return a['+this.D.M+'].O('+this.M+')" ondblclick="a['+this.D.M+'].'+(this.a_children?'L(':'O(')+this.M+')"><img src="'+AL[0]+'" border="0" name="i'+this.D.M+'_'+this.M+'"'+AH+'></a>':'')+'</td><td nowrap'+(AL[2]?' class="'+AL[2]+'"':'')+' id="t'+this.D.M+'_'+this.M+'"><a href="'+this.AX[1]+'" target="'+(AK&&AK['tw']?AK['tw']:this.D.C['target'])+'" title="'+(AK&&AK['tt']?AK['tt']:'')+'" onclick="return a['+this.D.M+'].O('+this.M+')" ondblclick="a['+this.D.M+'].'+(this.a_children?'L(':'O(')+this.M+')">'+this.AX[0]+'</a></td></tr></table>'+(this.a_children?'<div id="c'+this.D.M+'_'+this.M+'" style="display:'+(this.n_state&8?'block">'+this.u():'none">')+'</div>':'')}function AG(){var AM=[];for(var i=0;i<this.a_children.length;i++)AM[i]=this.a_children[i].g();return AM.join('')}function AD(){var AN=Math.floor(this.l/31);this.D.Y[AN]=(this.n_state&8?this.D.Y[AN]|(1<<(this.l%31)):this.D.Y[AN]&~(1<<(this.l%31)));document.cookie='tree_'+this.D.M+'_state='+this.D.Y.join('_')}function Ay(){var AN=Math.floor(this.M/31);this.D.Az[AN]=(this.n_state&4?this.D.Az[AN]|(1<<(this.M%31)):this.D.Az[AN]&~(1<<(this.M%31)));document.cookie='tree_'+this.D.M+'_slate='+this.D.Az.join('_')}function AF(){var p=(p?p:this.D.C['onItemLoad']);if(p){eval('var f='+p+'(tree);');if(!f)return}var AN=Math.floor(this.M/31);if(Boolean(this.D.Az[AN]&(1<<(this.M%31)))){this.n_state|=4;this.D.G=this}else this.n_state&=~4;if(!this.a_children)return;if(!this.D.Y.length){var AO=this.D.a_children;for(var i=0;i<AO.length;i++){AO[i].n_state|=8;AO[i].r()}return}var AN=Math.floor(this.l/31);if(Boolean(this.D.Y[AN]&(1<<(this.l%31))))this.n_state|=8;else this.n_state&=~8}function AC(AP){var AQ=this.n_state&~3;var AR=this.n_state&~68|2;var AS=this.AX[2]?this.AX[2]['i'+(AQ&~48)]:0;if(!AS)AS=this.D.C['icon_'+AQ];if(!AS)AS=this.D.C['icon_'+(AQ&~64)];var AT=this.D.C['icon_'+AR];var AU=this.AX[2]?this.AX[2]['s'+(AQ&~48)]:0;if(!AU)AU=this.D.C['style_'+AQ];if(!AU)AU=this.D.C['style_'+(AQ&~64)];if(AP)return[AS,AT,AU];var AV=document.images['j'+this.D.M+'_'+this.M];if(AV)AV.src=AT;AV=document.images['i'+this.D.M+'_'+this.M];if(AV)AV.src=AS;AV=t('t'+this.D.M+'_'+this.M);if(AV)AV.className=AU}var a=[],W;t=document.all?function(AW){return document.all[AW]}:(document.getElementById?function(AW){return document.getElementById(AW)}:function(AW){return null});

var tree_tpl = {
'target'  : 'main',
'cookie_ext' : 'opened',
'icon_e'  : '<?=C_URL?>/<?=ADM_DIR?>/index.php?img=empty',
'icon_l'  : '<?=C_URL?>/<?=ADM_DIR?>/index.php?img=line',
'icon_32' : '<?=C_URL?>/<?=ADM_DIR?>/index.php?img=base',   // root leaf icon normal
'icon_36' : '<?=C_URL?>/<?=ADM_DIR?>/index.php?img=base',   // root leaf icon selected
'icon_48' : '<?=C_URL?>/<?=ADM_DIR?>/index.php?img=base',   // root icon normal
'icon_52' : '<?=C_URL?>/<?=ADM_DIR?>/index.php?img=base',   // root icon selected
'icon_56' : '<?=C_URL?>/<?=ADM_DIR?>/index.php?img=base',   // root icon opened
'icon_60' : '<?=C_URL?>/<?=ADM_DIR?>/index.php?img=base',   // root icon selected
'icon_16' : '<?=C_URL?>/<?=ADM_DIR?>/index.php?img=folder',
'icon_20' : '<?=C_URL?>/<?=ADM_DIR?>/index.php?img=folder', // node icon selected
'icon_24' : '<?=C_URL?>/<?=ADM_DIR?>/index.php?img=folderopen', // node icon opened
'icon_28' : '<?=C_URL?>/<?=ADM_DIR?>/index.php?img=folderopen', // node icon selected opened
'icon_0'  : '<?=C_URL?>/<?=ADM_DIR?>/index.php?img=page', // leaf icon normal
'icon_4'  : '<?=C_URL?>/<?=ADM_DIR?>/index.php?img=selected', // leaf icon selected
'icon_2'  : '<?=C_URL?>/<?=ADM_DIR?>/index.php?img=joinbottom', // junction for leaf
'icon_3'  : '<?=C_URL?>/<?=ADM_DIR?>/index.php?img=join',       // junction for last leaf
'icon_18' : '<?=C_URL?>/<?=ADM_DIR?>/index.php?img=plusbottom', // junction for closed node
'icon_19' : '<?=C_URL?>/<?=ADM_DIR?>/index.php?img=plus',       // junctioin for last closed node
'icon_26' : '<?=C_URL?>/<?=ADM_DIR?>/index.php?img=minusbottom',// junction for opened node
'icon_27' : '<?=C_URL?>/<?=ADM_DIR?>/index.php?img=minus'       // junction for last opended node
};
var TREE_ITEMS = [
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php',$x[2]);?>,{'i0':'<?=C_URL?>/<?=ADM_DIR?>/index.php?img=settings','i64':'<?=C_URL?>/<?=ADM_DIR?>/index.php?img=settings'},
['<?=$x[120]?>',0,0,
['<?=$x[121]?>',0,0,
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=config&'.s(),$x[121]);?>,{'i0':'<?=C_URL?>/<?=ADM_DIR?>/index.php?img=settings','i64':'<?=C_URL?>/<?=ADM_DIR?>/index.php?img=settings'}],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=sef&'.s(),$x[905]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=meta&'.s(),$x[746]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=adopt&'.s(),$x[655]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=delpro&'.s(),$x[652]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=mail&'.s(),$x[705]);?>],
],
['<?=$x[339]?>',0,0,
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=ip&'.s(),$x[345]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=access&'.s(),$x[188]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=options&'.s(),$x[430]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=req&'.s(),$x[183]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=change&'.s(),$w[99]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=mg&'.s(),$x[213]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=msctrl&'.s(),$x[350]);?>],
],
['<?=$x[340]?>',0,0,
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=main&'.s(),$x[196]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=reg&'.s(),$x[491]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=regshow&'.s(),$x[527]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=tst_res&'.s(),$w[633]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=links&'.s(),$w[376]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=onlusr&'.s(),$x[629]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=comments&'.s(),$w[492]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=vote&'.s(),$w[430]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=stat&'.s(),$x[250]);?>],
],
['<?=$x[342]?>',0,0,
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=img&'.s(),$x[342]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=img_thumb&'.s(),$x[372]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=img_water&'.s(),$x[415]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=img_slide&'.s(),$x[773]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=imgver&'.s(),$x[606]);?>],
],
['<?=$x[602]?>',0,0,
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=sop&'.s(),$x[602]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=searchopt&'.s(),$x[528]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=searchres&'.s(),$x[530]);?>],
],
['<?=$x[856]?>',0,0,
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=bb_conf&'.s(),$x[121]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=bb_desc&'.s(),$x[750].' ('.$w[677].')');?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=bb_prof&'.s(),$x[750].' ('.$w[492].')');?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=bb_sm&'.s(),$w[170]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=bb_test&'.s(),$w[633]);?>],
],
['<?=$x[172]?>',0,0,
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=misc&'.s(),$x[172]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=notices_fields&'.s(),$x[673]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=pop&'.s(),$w[216]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=badword&'.s(),$x[304]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=audio&'.s(),$x[153]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=banner&'.s(),$x[170]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=len&'.s(),$x[173]);?>],
],
],
['<?=$x[10]?>',0,0,
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=um&'.s(),$x[10]);?>],
[<?php nmenu(REGISTER_LINK.s(),$x[468]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=ncpg&'.s(),$x[520]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=blacklist&'.s(),$x[512]);?>],
],
['<?=$x[349]?>',0,0,
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=ch&'.s(),$x[8]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=ro&'.s(),$x[9]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=backup&'.s(),$x[426]);?>],
],
['<?=$x[718]?>',0,0,
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=tedit&'.s(),$x[718]);?>],
],
['<?=$x[533].$new_mes?>',0,0,
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=vmess&'.s(),$w[96].$new_mes);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=mess&'.s(),$x[533]);?>],
],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=fm&'.s(),$x[11]);?>,0,
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=fm&'.s(),$x[50]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?ds=1&do=fc&'.s(),$x[47]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?ds=1&do=fe&'.s(),$x[48]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?ds=1&do=fd&'.s(),$x[49]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?ds=1&do=fqe&'.s(),$x[96]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?ds=1&do=fqd&'.s(),$x[97]);?>],
],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=lqe&'.s(),$x[326]);?>,0,
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?ds=1&do=lqe&'.s(),$x[326]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=lm&'.s(),$x[334]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?ds=1&do=lc&'.s(),$x[327]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?ds=1&do=le&'.s(),$x[328]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?ds=1&do=ld&'.s(),$x[329]);?>],
],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=nm&'.s(),$x[100]);?>,0,
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=nm&'.s(),$x[104]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=ne&'.s(),$x[105]);?>],
],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=comm&'.s(),$x[844]);?>,0,
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=comm&'.s(),$x[844]);?>],
],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=tm&'.s(),$x[688]);?>,0,
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=tm&'.s(),$x[688]);?>],
],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=vm&'.s(),$x[438]);?>,0,
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=vm&'.s(),$x[438]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?ds=1&do=vc&'.s(),$x[439]);?>],
],
['<?=$x[21]?>',0,0,
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=mm&'.s(),$x[12]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=mum&'.s(),$x[358]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=mbc&'.s(),$x[840]);?>],
],
['<?=$x[474]?>',0,0,
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=mui&'.s(),$x[474]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=muiedit&'.s(),$x[475]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=muieditmails&'.s(),$x[713]);?>],
],
['<?=$x[390]?>',0,0,
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=mtcu&'.s(),$x[392]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=mtrt&'.s(),$x[828]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=mtcp&'.s(),$x[829]);?>],
],
['<?=$w[94]?>',0,0,
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=re&'.s(),$x[83]);?>],
[<?php nmenu(C_URL.'/'.ADM_DIR.'/index.php?do=st&'.s(),$w[94]);?>],
],
[<?php nmenu(C_URL.'/logout.php?admin','&nbsp;'.$x[13]);?>,{'i0':'<?=C_URL?>/<?=ADM_DIR?>/index.php?img=exit','i64':'<?=C_URL?>/<?=ADM_DIR?>/index.php?img=exit','tw':'_top'}],
]
];

//-->
</script>
<style>
a:link{text-decoration:none;font-size:11px;color:black;text-indent : 4px;font-size:12px;}
a:visited{text-decoration:none;font-size:11px;color:black;text-indent : 4px;font-size:12px;}
a:hover{text-decoration:none;font-size:11px;color:#B00B76;text-indent : 4px;font-size:12px;}
</style>
</head>
<body LINK="#0057AE" VLINK="#0057AE" BGCOLOR="#E6E6E6" leftmargin=0 topmargin=0>
<br>
<script language="JavaScript">
<!--
new tree (TREE_ITEMS, tree_tpl);
//-->
</script>
</body></html>
<?php
}
elseif(isset($frame)  && $frame == 'index')
{
?>
<frameset  cols="20%,*" border="0" bordercolor="#000000">
    <frame name="menu" src="index.php?frame=menu" marginwidth="10" marginheight="10" scrolling="auto" frameborder="1">
    <frame name="main" src="index.php?frame=main" marginwidth="10" marginheight="10" scrolling="auto" frameborder="1">
</frameset>
<?php
}
else
{
	include_once C_PATH.'/sheader.php';
	echo '<center>';
	if(!isset($do)) $do='';

	function show_title($var)
	{
?>
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=2></Td></Tr>
    <tr class=desc bgcolor="<?=COLORH?>">
    <td colspan=2 align=center><?=$var?></td></tr>
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=2></Td></Tr>
<?php
	}

	function table_header($title,$size=null)
	{
		$width = ($size == null) ? C_WIDTH : C_BWIDTH;
?>
  <br><Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=$width?>" bgcolor="<?=C_TBCOLOR?>">
  <Tr>
    <Td width="<?=$width?>" bgcolor="<?=COLOR1?>" valign="top">
    <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=$width?>" class=mes>
    <Tr align="center" bgcolor="<?=COLORH?>"><Td><strong><?=$title?></strong></Td></Tr>
    <Tr><Td bgcolor="<?=C_TBCOLOR?>"></Td></Tr>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <table width="100%" class=tr>
<?php  
	}
	function table_footer()
	{
?>
</table></td></tr></table></td></tr></table>
<?php
	}
	switch($do) {
		############################################
		# Options - Begin
		############################################
		case 'opt':
			$mod_phrase = array($x[262],$x[263],$x[264],$x[265],$x[266],$x[267],$x[268],$x[269],$x[270],$x[271],$x[272],$x[273],$x[274],$x[275],$x[276],$x[325],$x[387],$x[388],$x[389],$x[688],$x[520],$x[731],$x[706],$x[972],$x[844],$x[840],$x[987]);
			if(isset($a) && ($a == 's')) {
				#			die(print_r($_POST));
				if(isset($_POST['c_available_lang'])) $_POST['c_available_lang'] = implode(",",$_POST['c_available_lang']);
				if(!(isset($define_mod) && ($define_mod == '1'))) {
					ChangeVars(C_PATH.'/include/vars.inc.php');
					ChangeConfig(C_PATH."/include/options.inc.php");
					ChangeConfig(C_PATH."/include/config.inc.php");
				}
				sprintm($x[253],1);
			} else {

?>
<form action="?<?=s()?>" method="post">
<input type="hidden" name="a" value="s">
<input type="hidden" name="do" value="opt">
<?php 
if(!isset($type)) $type = 0;
switch($type){
	case 'config':
?>
<?php table_header($x[121])?>
<?=show_checkbox($x[305],C_MAINTENANCE_MODE,"c_maintenance_mode")?>
<?=show_input($x[306],C_MAINTENANCE_MESSAGE,"c_maintenance_message","input")?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[122]?></td>
      <td><select name="c_temp_default" class=input>
      <?php 
      $handle=opendir(C_PATH.'/templates/');
      $fnm = 0;
      while (false!==($file = readdir($handle))) {
      	if ($file != '.' && $file != '..') {
      		echo (C_TEMP_DEFAULT == $file) ? ' <option selected>'.$file :  ' <option>'.$file;
      	}
      }
      closedir($handle);
      ?>      
      </select></td>
    </tr>
    
<?=show_input($x[123],C_SNAME,"c_sname","input")?>    
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[603]?></td>
      <td><select name="c_adminlang" class=input>
      <?php 
      $existlang = explode(",",C_AVAILABLE_LANG);
      $p=0;while(isset($existlang[$p])) {
      	if (C_ADMINLANG == $existlang[$p]) {
      		echo '<option selected value="'.$existlang[$p].'">'.$reallangname[$existlang[$p]];
      	} else {
      		echo '<option value="'.$existlang[$p].'">'.$reallangname[$existlang[$p]];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
<tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[867]?></td>
      <td><select name="c_userlang" class=input>
      <?php 
      $p=0;while(isset($existlang[$p])) {
      	if (C_USERLANG == $existlang[$p]) {
      		echo '<option selected value="'.$existlang[$p].'">'.$reallangname[$existlang[$p]];
      	} else {
      		echo '<option value="'.$existlang[$p].'">'.$reallangname[$existlang[$p]];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
    
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[127]?></td>
      <td><input type="text" name="c_adminm" class=input value="<?=C_ADMINM?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[128]?></td>
      <td><input type="text" name="c_fromm" class=input value="<?=C_FROMM?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[129]?></td>
      <td><input type="text" name="c_mails" class=sinput value="<?=C_MAILS?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[130]?></td>
      <td><input type="text" name="c_lastreg" class=sinput value="<?=C_LASTREG?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[131]?></td>
      <td><input type="text" name="c_mostpop" class=sinput value="<?=C_MOSTPOP?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[493]?></td>
      <td><input type="text" name="c_birthday_step" class=sinput value="<?=C_BIRTHDAY_STEP?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[497]?></td>
      <td><input type="text" name="c_who_seen_step" class=sinput value="<?=C_WHO_SEEN_STEP?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[132]?></td>
      <td><input type="text" name="c_apage" class=sinput value="<?=C_APAGE?>"></td>
    </tr>
    <?=show_input($x[483],ADM_DIR,"ADM_DIR","input")?>
    <?=select_access($x[621],"c_template_random",1,C_TEMPLATE_RANDOM);?>  
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[134]?></td>
      <td><select name="c_check_register" class=input>
      <?php 
      $p=0;while(isset($xr[$p])) {
      	if (C_CHECK_REGISTER == $p) {
      		echo '<option selected value="'.$p.'">'.$xr[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xr[$p];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
		<?=show_checkbox($x[399],C_SHOW_REGISTERED,"c_show_registered")?>
		<?=show_checkbox($x[413],C_NOTIFY_ADMIN,"c_notify_admin")?>
		<?=show_checkbox($x[414],C_CHECK_WPHOTOS,"c_check_wphotos")?>
		<?=show_checkbox($x[625],C_USE_GALLERY,"c_use_gallery")?>
		<?=show_checkbox($x[626],C_DONT_SHOW_CHANGED_PHOTOS,"c_dont_show_changed_photos")?>
    <?=show_input($x[627],C_GALLERY_COLUMN,"c_gallery_column","sinput")?>
    <?=show_input($x[628],C_GALLERY_ROW,"c_gallery_row","sinput")?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[135]?></td>
      <td><select name="c_reg_email" class=sinput>
      <?=options(C_REG_EMAIL,$x[136],$x[137]);?>      
      </select>
      </td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[138]?></td>
      <td><input type="text" name="c_reg_days" class=sinput value="<?=C_REG_DAYS?>"></td>
    </tr>
    <?=show_input($x[546],C_WHOSEENTIME,"c_whoseentime","sinput")?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[139]?></td>
      <td><select name="c_update_allow" class=input>
      <?php 
      $p=0;while(isset($xu[$p])) {
      	if (C_UPDATE_ALLOW == $p) {
      		echo '<option selected value="'.$p.'">'.$xu[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xu[$p];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
    <tr class=desc bgcolor="<?=colors(True)?>">
      <td width=70%> > <?=$x[338]?></td>
      <td><select name="c_update_params" class=input>
      <?php 
      $p=0;while(isset($xup[$p])) {
      	if (C_UPDATE_PARAMS == $p) {
      		echo '<option selected value="'.$p.'">'.$xup[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xup[$p];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[140]?></td>
      <td><select name="c_remove_allow" class=input>
      <?php 
      $p=0;while(isset($xd[$p])) {
      	if (C_REMOVE_ALLOW == $p) {
      		echo '<option selected value="'.$p.'">'.$xd[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xd[$p];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
<?=show_input($x[780],C_REMOVE_USERS_IN_DAYS,"c_remove_users_in_days","sinput")?>
<?php table_footer()?>
<?php 
break;
	case 'meta':
		$vkeys = '{USERNAME} {FIRSTNAME} {LASTNAME} {AGE} {HOROSCOPE} {COUNTRY} {CITY} {MARSTAT} {RELIGION} {JOB} {HOBBY} {DESCRIPTION}';
?>
<?php table_header($x[746])?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=100% colspan=2 align=center><?=$x[752]?></td>
    </tr>
<?=show_input('<b>'.$x[750].' - '.$x[747].'</b><br>'.$x[751].'<br> '.$vkeys,C_VIEW_TITLE,"c_view_title","input")?>
<?=show_input('<b>'.$x[750].' - '.$x[748].'</b><br>'.$x[751].'<br> '.$vkeys,C_VIEW_KEYWORDS,"c_view_keywords","input")?>
<?=show_input('<b>'.$x[750].' - '.$x[749].'</b><br>'.$x[751].'<br> '.$vkeys,C_VIEW_DESCRIPTION,"c_view_description","input")?>
<tr class=desc bgcolor="<?=colors()?>"><td width=100% colspan=2 align=center><hr></td></tr>
<?=show_input('<b>'.$w[91].' - '.$x[747].'</b><br>'.$x[751].'<br>{SEARCH} {SNAME}',C_SEARCH_TITLE,"c_search_title","input")?>    
<?=show_input('<b>'.$w[89].' - '.$x[747].'</b><br>'.$x[751].'<br>{REGISTER} {SNAME}',C_REGISTER_TITLE,"c_register_title","input")?>    
<?php table_footer()?>
<?php 
break;
	case 'sef':

?>
<?php table_header($x[905])?>
<tr class=desc bgcolor="<?=colors()?>"><td width=100% colspan=2 align=center><?=$x[906]?></td></tr>
<?=show_checkbox($x[907],C_SEF,"c_sef")?>
<?=show_checkbox($x[908],C_SEF_ENCODE,"c_sef_encode")?>

<tr class=desc bgcolor="<?=colors()?>"><td width=100% colspan=2 align=center><hr></td></tr>
<?=show_input('<b>'.$x[909].'</b><br>'.$x[910].' users/{LANGUAGE}/{ID}.htm',C_SEF_VP,"c_sef_vp","input")?>
<?=show_input('<b>'.$w[94].'</b><br>'.$x[910].' stat/{LANGUAGE}',C_SEF_ST,"c_sef_st","input")?>
<?=show_input('<b>'.template($w[180],array(C_LASTREG)).'</b><br>'.$x[910].' lastest/{LANGUAGE}/{FROM}/{GENDER}',C_SEF_LAST,"c_sef_last","input")?>
<?=show_input('<b>'.$x[911].'</b><br>'.$x[910].' best/{LANGUAGE}/{FROM}/{GENDER}/{PIC}',C_SEF_TOP,"c_sef_top","input")?>
<?=show_input('<b>'.$w[376].'</b><br>'.$x[910].' site/{LANGUAGE}/{ID}/{TITLE}',C_SEF_LINKS,"c_sef_links","input")?>
<?=show_input('<b>'.$x[970].'</b><br>'.$x[910].' sites/{LANGUAGE}/{FROM}/{ID}/{TITLE}',C_SEF_LINKS_CAT,"c_sef_links_cat","input")?>
<?php table_footer()?>
<?php 
break;
	case 'pop':
?>

<?php table_header($w[216])?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=100% colspan=2 align=center><?=$x[434]?></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[436]?></td>
      <td><select name="c_popularity" class=input>
      <?php 
      $p=0;while(isset($xpop[$p])) {
      	if (C_POPULARITY == $p) {
      		echo '<option selected value="'.$p.'">'.$xpop[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xpop[$p];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[146]?></td>
      <td><input type="text" name="c_hits_view" class=sinput value="<?=C_HITS_VIEW?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[147]?></td>
      <td><input type="text" name="c_hits_mail_from" class=sinput value="<?=C_HITS_MAIL_FROM?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[148]?></td>
      <td><input type="text" name="c_hits_mail_to" class=sinput value="<?=C_HITS_MAIL_TO?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[149]?></td>
      <td><input type="text" name="c_hits_send_from" class=sinput value="<?=C_HITS_SEND_FROM?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[150]?></td>
      <td><input type="text" name="c_hits_send_to" class=sinput value="<?=C_HITS_SEND_TO?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[487]?></td>
      <td><input type="text" name="c_hits_kiss" class=sinput value="<?=C_HITS_KISS?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[151]?></td>
      <td><input type="text" name="c_hits_bedroom" class=sinput value="<?=C_HITS_BEDROOM?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[152]?></td>
      <td><input type="text" name="c_hits_time" class=sinput value="<?=C_HITS_TIME?>"></td>
    </tr>
		<?=select_access($x[433],"c_rate",1,C_RATE);?>
		<?=select_access($x[518],"c_can_no_rate",0,C_CAN_NO_RATE);?>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[519]?></td>
      <td><?=checkbox(C_DEF_RATE,"c_def_rate");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[435]?></td>
      <td><input type="text" name="c_rate_digits" class=sinput value="<?=C_RATE_DIGITS?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[437]?></td>
      <td><input type="text" name="c_rate_min_hits" class=sinput value="<?=C_RATE_MIN_HITS?>"></td>
    </tr>

<?php table_footer()?>
<?php 
break;
	case 'badword':
?>
<?php table_header($x[304])?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[547]?></td>
      <td>
      <?=@checkbox(C_BADWORDS_ENABLE,"c_badwords_enable")?>      
      </td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[548]?></td>
      <td><input type="text" name="c_badwords" class=input value="<?=@C_BADWORDS?>"></td>
    </tr>
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=2></Td></Tr>
    <tr class=desc bgcolor="<?=COLORH?>"><td colspan=2 align=center><?=$x[549]?></td></tr>
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=2></Td></Tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[550]?></td>
      <td>
      <?=@checkbox(C_BADWORDS_ERR,"c_badwords_err")?>      
      </td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[551]?></td>
      <td><input type="text" name="c_badwords_mes" class=input value="<?=@C_BADWORDS_MES?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[552]?></td>
      <td>
      <?=@checkbox(C_BADWORDS_REP,"c_badwords_rep")?>      
      </td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[553]?></td>
      <td><input type="text" name="c_badwords_repw" class=input value="<?=@C_BADWORDS_REPW?>"></td>
    </tr>
<?php table_footer()?>
<?php 
break;
	case 'comments':
?>
<?php table_header($w[492])?>
<?=select_access($x[596],"c_see_own_comments",0,C_SEE_OWN_COMMENTS);?>
<?=select_access($x[597],"c_see_another_comments",1,C_SEE_ANOTHER_COMMENTS);?>
<?=select_access($x[605],"c_comment_photo",1,C_COMMENT_PHOTO);?>
<?=select_access($x[600],"c_add_comments",1,C_ADD_COMMENTS);?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[598]?></td>
      <td>
      <?=@checkbox(C_BADWORD_COMMENTS,"c_badword_comments")?>      
      </td>
    </tr>
<?=select_access($x[599],"c_reply_comments",0,C_REPLY_COMMENTS);?>
<?=select_access($x[601],"c_remove_comments",0,C_REMOVE_COMMENTS);?>
<?php table_footer()?>
<?php 
break;
	case 'sop':
?>
<?php table_header($x[602])?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[133]?></td>
      <td><input type="text" name="c_cpage" class=sinput value="<?=C_CPAGE?>"></td>
    </tr>
    <?=select_access($x[216],"c_advsearch",1,C_ADVSEARCH);?>  
    <?=select_access($x[217],"c_search",1,C_SEARCH);?> 
    <?=select_access($x[319],"c_qsearch",1,C_QSEARCH);?> 
    <?=select_access($x[604],"c_show_search_count",1,C_SHOW_SEARCH_COUNT);?>  
    <?=show_input($x[618],C_SRC_PAGE,"c_src_page","sinput")?>
    <?=show_input($x[633],C_SEARCH_STEPS,"c_search_steps","input")?>
    <?=show_input($x[770],C_SEARCH_TIMEOUT,"c_search_timeout","sinput")?>
    <?=show_input($x[771],C_SEARCH_LIMIT,"c_search_limit","sinput")?>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[207]?></td>
      <td><?=checkbox(C_SHOW_PHOTOS_FIRST,"c_show_photos_first");?></td></tr>
		<?=show_title($x[386])?>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[232]?></td>
      <td><?=checkbox(C_SEARCH_PHOTO,"c_search_photo");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[635]?></td>
      <td><?=checkbox(C_SEARCH_PHOTO_AUTORESIZE,"c_search_photo_autoresize");?></td></tr>
    <?=show_input($x[384],C_SEARCH_PHOTO_WIDTH,"c_search_photo_width","input")?>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[638]?></td>
      <td><?=checkbox(C_NO_PHOTO,"c_no_photo");?></td></tr>
    <?=show_input($x[639],C_NO_PHOTO_FILE,"c_no_photo_file","input")?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[385]?></td>
      <td><select name="c_search_photo_link" class=input>
      <?php 
      $p=0;while(isset($xlt[$p])) {
      	if (C_SEARCH_PHOTO_LINK == $p) {
      		echo '<option selected value="'.$p.'">'.$xlt[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xlt[$p];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>

<?php table_footer()?>
    <?php table_header($x[529])?>
    <?php array_unshift($wpu,$w[354]);?>
    <?=ShowVars($x[529],$wpu,$wsp,'wsp')?> 
    <?php table_footer()?>
    <br>

<?php 
break;
	
	case 'links':
?>
<?php table_header($w[376])?>
    <?=select_access($x[922],"c_link_who",1,C_LINK_WHO);?>  
    <?=show_input($x[924],C_LINK_DESCR_LIMIT,"c_link_descr_limit","sinput")?>
	<?=show_checkbox($x[532],C_VERIMG_LINKS,"c_verimg_links")?>
	<?=show_checkbox($x[925],C_REC_LINK,"c_rec_link")?>
	<?=show_checkbox($x[926],C_REC_SHOW,"c_rec_show")?>
	<?=show_checkbox($x[928],C_REC_EXACT,"c_rec_exact")?>
    <?=show_input($x[927],C_REC_VAL,"c_rec_val","input")?>
    <?=show_input($x[971],C_SEF_LINKS_CATN,"c_sef_links_catn","sinput")?>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[929]?></td>
      <td><select name="c_link_add" class=input>
      <?php 
      $p=0;while(isset($xhal[$p])) {
      	if (C_LINK_ADD == $p) {
      		echo '<option selected value="'.$p.'">'.$xhal[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xhal[$p];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
		</tr>
    <?=show_input($x[979],C_LINKS_DAYS,"c_links_days","sinput")?>

    <?php table_footer()?>
<?php 
break;
	case 'imgver':
?>
<?php table_header($x[606])?>
<tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[894]?></td>
      <td><select name="c_verify_alg" class=input>
      <?php  $p=0;while(isset($xiva[$p])) {
      	echo (C_VERIFY_ALG == $p) ?  '<option value="'.$p.'" selected>'.$xiva[$p] : '<option value="'.$p.'">'.$xiva[$p];
      	$p++;}?> </select></td>   </tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[607]?></td>
      <td><?=checkbox(C_VERIFY_IMAGE_REGISTER,"c_verify_image_register");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[608]?></td>
      <td><?=checkbox(C_VERIFY_IMAGE_LOGIN,"c_verify_image_login");?></td></tr>
<tr class=desc bgcolor="<?=colors()?>"><td width=95% colspan=2><hr></td></tr>      
    <?=select_access($x[895]. ': <br>'.$x[896],"c_verimg_sm",0,C_VERIMG_SM);?>  
    <?=select_access($x[895]. ': <br>'.$x[897],"c_verimg_forum",1,C_VERIMG_FORUM);?>  
    <?=select_access($x[895]. ': <br>'.$w[492],"c_verimg_comment",1,C_VERIMG_COMMENT);?>  
    <?=select_access($x[895]. ': <br>'.$w[92],"c_verimg_fdbk",1,C_VERIMG_FDBK);?>  
    <?=select_access($x[895]. ': <br>'.$x[898],"c_verimg_bcom",1,C_VERIMG_BCOM);?>  

<?php table_footer()?>
<?php 
break;
	case 'onlusr':
?>
<?php table_header($x[629])?>
    <?=select_access($x[218],"c_online",1,C_ONLINE);?>  
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[341]?></td>
      <td><input type="text" name="c_online_member_time" class=sinput value="<?=C_ONLINE_MEMBER_TIME?>"></td>
    </tr>
<?=select_access($x[783],"c_who_is_where",1,C_WHO_IS_WHERE);?>
<?=select_access($x[630],"c_online_gender",1,C_ONLINE_GENDER);?>
<?=select_access($x[632],"c_online_picture",1,C_ONLINE_PICTURE);?>
<?=select_access($x[871],"c_online_country",1,C_ONLINE_COUNTRY);?>
<?=select_access($x[872],"c_online_city",1,C_ONLINE_CITY);?>
<?=select_access($x[873],"c_online_age",1,C_ONLINE_AGE);?>
<?=show_input($x[874],C_ONLINE_USERS_NUM,"c_online_users_num","sinput")?>
<?=show_checkbox($x[875],C_LOGOUT,"c_logout")?>
<?php table_footer()?>
<?php 
break;
	case 'img':
?>
<?php table_header($x[342])?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[208]?></td>
      <td><input type="text" name="c_maxsz" class=sinput value="<?=C_MAXSZ?>"></td>
    </tr>
		<?=show_checkbox($x[609],C_DECREASE_IMAGES,"c_decrease_images")?>
		<?=show_checkbox($x[899],C_GD2_QUAL,"c_gd2_qual")?>
    <?=show_input($x[900],C_THUMB_SIDE,"c_thumb_side","sinput")?>
    <?=show_input($x[610],C_PICTURE_WIDTH,"c_picture_width","input")?>
    <?=show_input($x[611],C_PICTURE_HEIGHT,"c_picture_height","input")?>
    <?=show_input($x[612],C_PICTURE_AUTO,"c_picture_auto","input")?>
    <?=show_input($x[613],C_PICTURE_QUALITY,"c_picture_quality","input")?>
    <?=show_input($x[614],C_VIEW_PHOTOS_SHOW,"c_view_photos_show","input")?>
    <?=show_input($x[615],C_VIEW_PHOTOS_STEP,"c_view_photos_step","input")?>
    <?=show_checkbox($x[616],C_ALLOW_RIGHT_CLICK,"c_allow_right_click")?>
    <?=show_checkbox($x[617],C_ALLOW_RIGHT_CLICK_MESSAGE,"c_allow_right_click_message")?>
		<?=show_title($x[371])?>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[209]?></td>
      <td><?=checkbox(C_HACK3,"c_hack3");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[210]?></td>
      <td><input type="text" name="c_maxwd" class=sinput value="<?=C_MAXWD?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[211]?></td>
      <td><input type="text" name="c_maxhg" class=sinput value="<?=C_MAXHG?>"></td>
    </tr>
		<?=show_title($x[386])?>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[232]?></td>
      <td><?=checkbox(C_SEARCH_PHOTO,"c_search_photo");?></td></tr>
    <?=show_input($x[384],C_SEARCH_PHOTO_WIDTH,"c_search_photo_width","input")?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[385]?></td>
      <td><select name="c_search_photo_link" class=input>
      <?php 
      $p=0;while(isset($xlt[$p])) {
      	if (C_SEARCH_PHOTO_LINK == $p) {
      		echo '<option selected value="'.$p.'">'.$xlt[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xlt[$p];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
		
		<?=ShowVars($x[480],$wpu,$wpn,'wpn')?>

<?php table_footer()?>
<?php 
break;
	case 'img_thumb':
?>
<?php table_header($x[372])?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[372]?></td>
      <td><select name="c_thumbnail_use" class=input>
      <?php 
      $p=0;while(isset($xth[$p])) {
      	if (C_THUMBNAIL_USE == $p) {
      		echo '<option selected value="'.$p.'">'.$xth[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xth[$p];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
    <?=show_input($x[373],C_TH_WIDTH,"c_th_width","input")?>
    <?=show_input($x[374],C_TH_HEIGHT,"c_th_height","input")?>
    <?=show_input($x[375],C_TH_AUTO,"c_th_auto","input")?>
    <?=show_input($x[376],C_TH_QUALITY,"c_th_quality","input")?>
		<?=show_checkbox($x[382],C_CRYPT_URL,"c_crypt_url")?>
    <?=show_input($x[383],C_CRYPT_KEY,"c_crypt_key","input")?>
<?php table_footer()?>
<?php 
break;
	case 'img_water':
?>
<?php table_header($x[415])?>
    <tr class=desc bgcolor="<?=colors()?>"><td width=100% colspan=2><?=$x[423]?></td></tr>
		<?=show_checkbox($x[416],C_USE_WATERMARK,"c_use_watermark")?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[417]?></td>
      <td><select name="c_watermark_type" class=input>
      <?php 
      $p=0;while(isset($xwmtype[$p])) {
      	if (C_WATERMARK_TYPE == $p) {
      		echo '<option selected value="'.$p.'">'.$xwmtype[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xwmtype[$p];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
    <?=show_input($x[418],C_WATERMARK_TEXT,"c_watermark_text","input")?>
    <?=show_input($x[421],C_WATERMARK_TEXT_COLOR,"c_watermark_text_color","input")?>
    <?=show_input($x[620],C_WATERMARK_TEXT_TRANSPARENCY,"c_watermark_text_transparency","input")?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[422]?></td>
      <td><select name="c_watermark_text_or" class=input>
      <?php 
      $p=0;while(isset($xwmor[$p])) {
      	if (C_WATERMARK_TEXT_OR == $p) {
      		echo '<option selected value="'.$p.'">'.$xwmor[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xwmor[$p];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
    <?=show_input($x[419],C_WATERMARK_IMAGE,"c_watermark_image","input")?>
		
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[420]?></td>
      <td><select name="c_watermark_position" class=input>
      <?php 
      $p=0;while(isset($xwmpos[$p])) {
      	if (C_WATERMARK_POSITION == $p) {
      		echo '<option selected value="'.$p.'">'.$xwmpos[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xwmpos[$p];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
<?php table_footer()?>
<?php 
break;
	case 'img_slide':
?>
<?php table_header($x[773])?>
		<?=select_access($x[774],"c_slideshow",1,C_SLIDESHOW);?>
    <?=show_input($x[779],C_SLIDESHOW_PHOTOS_NUM,"c_slideshow_photos_num","sinput")?>
    <?=show_input($x[775],C_SLIDESHOW_SPEED,"c_slideshow_speed","sinput")?>
		<?=show_checkbox($x[776],C_SLIDESHOW_FILTER,"c_slideshow_filter")?>
    <?=show_input($x[777],C_SLIDESHOW_FILTER_SPEED,"c_slideshow_filter_speed","sinput")?>
		<?=select_access($x[778],"c_slideshow_big_photos",1,C_SLIDESHOW_BIG_PHOTOS);?>
<?php table_footer()?>
<?php 
break;

	case 'mail':
?>
<?php table_header($x[705])?>
    <?=show_checkbox($x[800],C_MAIL_SENDFROM,"c_mail_sendfrom")?>
<?php table_footer()?>
<?php 
break;
	case 'audio':
?>
<?php table_header($x[153])?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[154]?></td>
      <td><select name="c_audio_allow" class=input>
      <?php 
      $p=0;while(isset($xav[$p])) {
      	if (C_AUDIO_ALLOW == $p) {
      		echo '<option selected value="'.$p.'">'.$xav[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xav[$p];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=100% colspan=2 align=center><b><?=$x[727]?></b></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[155]?></td>
      <td><input type="text" name="c_audio_size" class=sinput value="<?=C_AUDIO_SIZE?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[156]?></td>
      <td><?=checkbox(C_AUDIO_AUTOSTART,"c_audio_autostart")?></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[157]?></td>
      <td><input type="text" name="c_audio_loop" class=sinput value="<?=C_AUDIO_LOOP?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[158]?></td>
      <td><select name="c_audio_player" class=input>
      <?php 
      $p=0;while(isset($xa[$p])) {
      	if (C_AUDIO_PLAYER == $p) {
      		echo '<option selected value="'.$p.'">'.$xa[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xa[$p];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
<?php table_footer()?>
<?php 
break;
	
	case 'vote':
?>
<?php table_header($w[430])?>
<?=select_access($x[508],"c_vote",1,C_VOTE);?>
<?=show_input($x[498],C_GRAFLEN,"c_graflen","input")?>
<?=show_input($x[499],C_TTR,"c_ttr","input")?>
<?=show_checkbox($x[500],C_VIV,"c_viv")?>
<?=show_checkbox($x[501],C_VRL,"c_vrl")?>
<?=select_access($x[502],"c_results_after_succ_vote",1,C_RESULTS_AFTER_SUCC_VOTE);?>
<?=select_access($x[503],"c_results_after_unsucc_vote",1,C_RESULTS_AFTER_UNSUCC_VOTE);?>
<?php table_footer()?>
<?php 
break;
	case 'banner':
?>
<?php table_header($x[170])?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[171]?></td>
      <td>
      <?=checkbox(C_BANNER_ALLOW,"c_banner_allow");?>
	<?=show_input($x[966],C_SR_BANNER,"c_sr_banner","sinput")?>
      </td>
    </tr>
<?php table_footer()?>
<?php 
break;
	case 'len':
?>
<?php table_header($x[173])?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[174]?></td>
      <td>
      <select name="c_firstns" class=sinput><?=select(C_FIRSTNS,1,16,1);?></select>
      -
      <select name="c_firstnb" class=sinput><?=select(C_FIRSTNB,1,16,1);?></select>
      </td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[175]?></td>
      <td>
      <select name="c_lastns" class=sinput><?=select(C_LASTNS,1,30,1);?></select>
      -
      <select name="c_lastnb" class=sinput><?=select(C_LASTNB,1,30,1);?></select>
      </td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[176]?></td>
      <td>
      <select name="c_passs" class=sinput><?=select(C_PASSS,1,16,1);?></select>
      -
      <select name="c_passb" class=sinput><?=select(C_PASSB,1,16,1);?></select>
      </td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[177]?></td>
      <td>
      <select name="c_ages" class=sinput><?=select(C_AGES,1,255,1);?></select>
      -
      <select name="c_ageb" class=sinput><?=select(C_AGEB,1,255,1);?></select>
      </td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[178]?></td>
      <td>
      <select name="c_hobbw" class=sinput><?=select(C_HOBBW,1,100,1);?></select>
      </td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[179]?></td>
      <td>
      <select name="c_hobbb" class=sinput><?=select(C_HOBBB,0,255,5);?></select>
      </td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[180]?></td>
      <td>
      <select name="c_descw" class=sinput><?=select(C_DESCW,1,100,1);?></select>
      </td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[181]?></td>
      <td>
      <select name="c_descb" class=sinput><?=select(C_DESCB,0,10000,100);?></select>
      </td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[182]?></td>
      <td>
      <select name="c_cityb" class=sinput><?=select(C_CITYB,1,32,1);?></select>
      </td>
    </tr>
<?php table_footer()?>
<?php 
break;
	case 'req':
?>
<?php table_header($x[183])?>

    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[127]?> <?=$x[184]?></td>
      <td><?=checkbox(C_FIRSTNR,"c_firstnr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[128]?> <?=$x[184]?></td>
      <td><?=checkbox(C_LASTNR,"c_lastnr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[56]?> <?=$x[184]?></td>
      <td><?=checkbox(C_BIRTHR,"c_birthr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[57]?> <?=$x[184]?></td>
      <td><?=checkbox(C_GENDR,"c_gendr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[59]?> <?=$x[184]?></td>
      <td><?=checkbox(C_CNTRR,"c_cntrr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[525]?> </td>
      <td><?=checkbox(C_BODYTYPER,"c_bodytyper");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[524]?> </td>
      <td><?=checkbox(C_ORIENTATIONR,"c_orientationr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[65]?> </td>
      <td><?=checkbox(C_CITYR,"c_cityr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[527]?> </td>
      <td><?=checkbox(C_STATER,"c_stater");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[378]?> </td>
      <td><?=checkbox(C_ADDRESSR,"c_addressr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[382]?> </td>
      <td><?=checkbox(C_LANGR,"c_langr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[526]?> </td>
      <td><?=checkbox(C_INTERESTSR,"c_interestsr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[119]?> <?=$x[184]?></td>
      <td><?=checkbox(C_PURPR,"c_purpr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[134]?> </td>
      <td><?=checkbox(C_MARSR,"c_marsr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[78]?> </td>
      <td><?=checkbox(C_HOBBR,"c_hobbr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[130]?> </td>
      <td><?=checkbox(C_HGHTR,"c_hghtr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[131]?></td><td><?=checkbox(C_WGHTR,"c_wghtr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[135]?></td><td><?=checkbox(C_CHILDR,"c_childr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[136]?></td><td><?=checkbox(C_HAIRR,"c_hairr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[137]?></td><td><?=checkbox(C_EYER,"c_eyer");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[138]?></td><td><?=checkbox(C_ETNR,"c_etnr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[139]?></td><td><?=checkbox(C_RELR,"c_relr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[140]?></td><td><?=checkbox(C_SMOKER,"c_smoker");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[141]?></td><td><?=checkbox(C_DRINKR,"c_drinkr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[142]?></td><td><?=checkbox(C_EDUCR,"c_educr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[77]?></td><td><?=checkbox(C_JOBR,"c_jobr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[87]?></td><td><?=checkbox(C_PHOTOR,"c_photor");?></td></tr>
		<?=show_input($x[636],C_REQ_PHOTO_NUM,"c_req_photo_num","sinput")?>
     <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[869]?></td>
      <td><select name="c_ph_req_gen[]" class=mselect multiple><?php 
      $p=0;while(isset($wg[$p])) {
      	$arr = explode(',',C_PH_REQ_GEN);
      	$txt = ($p == 0) ? $x[236] : $wg[$p];
      	if (in_array($p,$arr)) {
      		echo '<option selected value="'.$p.'">'.$txt;
      	} else {
      		echo '<option value="'.$p.'">'.$txt;
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[147]?></td><td><?=checkbox(C_PHONER,"c_phoner");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[285]?></td><td><?=checkbox(C_ZIPR,"c_zipr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[86]?></td><td><?=checkbox(C_HDYFUR,"c_hdyfur");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[81]?></td><td><?=checkbox(C_SETNR,"c_setnr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[80]?></td><td><?=checkbox(C_SGENDR,"c_sgendr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[82]?></td><td><?=checkbox(C_SRELR,"c_srelr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[83]?></td><td><?=checkbox(C_SAGER,"c_sager");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[85]?></td><td><?=checkbox(C_SWGHTR,"c_swghtr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[84]?></td><td><?=checkbox(C_SHGHTR,"c_shghtr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[444]?></td>
      <td><?=checkbox(C_SCNTRR,"c_scntrr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[445]?> </td>
      <td><?=checkbox(C_SMARSR,"c_smarsr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[446]?></td><td><?=checkbox(C_SCHILDR,"c_schildr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[447]?></td><td><?=checkbox(C_SHAIRR,"c_shairr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[448]?></td><td><?=checkbox(C_SEYER,"c_seyer");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[449]?></td><td><?=checkbox(C_SSMOKER,"c_ssmoker");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[450]?></td><td><?=checkbox(C_SDRINKR,"c_sdrinkr");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$w[451]?></td><td><?=checkbox(C_SEDUCR,"c_seducr");?></td></tr>

    <tr class=desc bgcolor="<?=colors()?>">
    <td width=70%><?=$x[185]?></td><td><?=checkbox(C_AGR,"c_agr");?></td></tr>
<?php table_footer()?>
<?php 
break;
	case 'change':
?>
<?php table_header($w[99])?>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[186]?> </td>
      <td><?=checkbox(C_CHANGE_GENDER,"c_change_gender");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[187]?> </td>
      <td><?=checkbox(C_CHANGE_EMAIL,"c_change_email");?></td></tr>
<?=show_input($x[870],C_UPDPROF_PER,"c_updprof_per","sinput")?>      
<?php table_footer()?>
<?php 
break;
	
	case 'ip':
?>
<?php table_header($x[345])?>
<?=show_input($x[346],C_DENY_IPS,"c_deny_ips","input")?>
<?php table_footer()?>
<?php 
break;
	case 'access':
?>
<?php table_header($x[188])?>
<?=select_access($x[380],"c_hide_fields",1,C_HIDE_FIELDS);?>
<?=select_access($x[381],"c_who_can_show",1,C_WHO_CAN_SHOW);?>

<?=show_checkbox($x[904],C_VP_STAT,"c_vp_stat")?>

<?=select_access($w[118],"c_usernamesh",1,C_USERNAMESH);?>
<?=select_access($w[127],"c_firstsh",1,C_FIRSTSH);?>
<?=select_access($w[128],"c_lastsh",1,C_LASTSH);?>
<?=select_access($w[56],"c_birthdaysh",1,C_BIRTHDAYSH);?>
<?=select_access($w[57],"c_gendersh",1,C_GENDERSH);?>
<?=select_access($w[59],"c_countrysh",1,C_COUNTRYSH);?>
<?=select_access($w[525],"c_bodytypesh",1,C_BODYTYPESH);?>
<?=select_access($w[524],"c_orientationsh",1,C_ORIENTATIONSH);?>
<?=select_access($w[65],"c_citysh",1,C_CITYSH);?>
<?=select_access($w[527],"c_statesh",1,C_STATESH);?>
<?=select_access($w[378],"c_addresssh",1,C_ADDRESSSH);?>
<?=select_access($w[382],"c_langsh",1,C_LANGSH);?>
<?=select_access($w[526],"c_interestssh",1,C_INTERESTSSH);?>
<?=select_access($w[119],"c_purposesh",1,C_PURPOSESH);?>
<?=select_access($w[134],"c_marstatsh",1,C_MARSTATSH);?>
<?=select_access($w[130],"c_heightsh",1,C_HEIGHTSH);?>
<?=select_access($w[131],"c_weightsh",1,C_WEIGHTSH);?>
<?=select_access($w[135],"c_childsh",1,C_CHILDSH);?>
<?=select_access($w[136],"c_hcolorsh",1,C_HCOLORSH);?>
<?=select_access($w[137],"c_ecolorsh",1,C_ECOLORSH);?>
<?=select_access($w[138],"c_ethnicitysh",1,C_ETHNICITYSH);?>
<?=select_access($w[139],"c_religionsh",1,C_RELIGIONSH);?>
<?=select_access($w[140],"c_smokesh",1,C_SMOKESH);?>
<?=select_access($w[141],"c_drinksh",1,C_DRINKSH);?>
<?=select_access($w[142],"c_educationsh",1,C_EDUCATIONSH);?>
<?=select_access($w[77],"c_jobsh",1,C_JOBSH);?>
<?=select_access($w[78],"c_hobbysh",1,C_HOBBYSH);?>
<?=select_access($w[215],"c_descrsh",1,C_DESCRSH);?>
<?=select_access($w[87],"c_photosh",1,C_PHOTOSH);?>
<?=select_access($w[147],"c_phonesh",1,C_PHONESH);?>
<?=select_access($w[285],"c_zipsh",1,C_ZIPSH);?>
<?=select_access($w[81],"c_sethnicitysh",1,C_SETHNICITYSH);?>
<?=select_access($w[80],"c_sgendersh",1,C_SGENDERSH);?>
<?=select_access($w[82],"c_sreligionsh",1,C_SRELIGIONSH);?>
<?=select_access($w[83],"c_sagesh",1,C_SAGESH);?>
<?=select_access($w[85],"c_sweightsh",1,C_SWEIGHTSH);?>
<?=select_access($w[84],"c_sheightsh",1,C_SHEIGHTSH);?>
<?=select_access($w[444],"c_scountrysh",1,C_SCOUNTRYSH);?>
<?=select_access($w[445],"c_smarstatsh",1,C_SMARSTATSH);?>
<?=select_access($w[446],"c_schildsh",1,C_SCHILDSH);?>
<?=select_access($w[447],"c_shcolorsh",1,C_SHCOLORSH);?>
<?=select_access($w[448],"c_secolorsh",1,C_SECOLORSH);?>
<?=select_access($w[449],"c_ssmokesh",1,C_SSMOKESH);?>
<?=select_access($w[450],"c_sdrinksh",1,C_SDRINKSH);?>
<?=select_access($w[451],"c_seducationsh",1,C_SEDUCATIONSH);?>
<?=select_access($w[129],"c_horosh",1,C_HOROSH);?>
<?=select_access($w[60],"c_emailsh",1,C_EMAILSH);?>
<?=select_access($w[61],"c_urlsh",1,C_URLSH);?>
<?=select_access($w[62],"c_icqsh",1,C_ICQSH);?>
<?=select_access($w[63],"c_aimsh",1,C_AIMSH);?>
<?=select_access($w[124],"c_regdate",1,C_REGDATE);?>
<?=select_access($w[123],"c_accdate",1,C_ACCDATE);?>
<?=select_access($x[962],"c_prof_lnksh",1,C_PROF_LNKSH);?>
<?=select_access($x[311],"c_views",1,C_VIEWS);?>
<?=select_access($x[312],"c_onlinestat",1,C_ONLINESTAT);?>
<?=select_access($x[317],"c_popularitysh",1,C_POPULARITYSH);?>
<?=select_access($x[489],"c_big_photosh",1,C_BIG_PHOTOSH);?>
<?=select_access($x[318],"c_audiosh",1,C_AUDIOSH);?>
<?=select_access($x[495],"c_who_seen_own",0,C_WHO_SEEN_OWN);?>
<?=select_access($x[496],"c_who_seen_other",1,C_WHO_SEEN_OTHER);?>
<?=select_access($x[809],"c_other_favorites",1,C_OTHER_FAVORITES);?>
<?=show_checkbox($x[810],C_OTHER_FAVORITES_FAVONLY,"c_other_favorites_favonly")?>
<?php show_title($x[189])?>
<?=select_access($x[190],"c_sendsh",0,C_SENDSH);?>
<?=select_access($x[486],"c_send_kiss",0,C_SEND_KISS);?>
<?=select_access($x[192],"c_mailsh",1,C_MAILSH);?>
<?=select_access($x[193],"c_bedsh",0,C_BEDSH);?>
<?=select_access($x[194],"c_badsh",1,C_BADSH);?>
<?=select_access($x[195],"c_bansh",0,C_BANSH);?>
<?php table_footer()?>
<?php 
break;
	case 'options':
?>
<?php table_header($x[430])?>
<?=select_access($x[431],"c_options",0,C_OPTIONS);?>
<?=select_access($x[432],"c_options_template",1,C_OPTIONS_TEMPLATE);?>
<?=select_access($x[623],"c_can_no_rate",0,C_CAN_NO_RATE);?>
<?=select_access($x[826],"c_hide_profile",0,C_HIDE_PROFILE);?>
<?=select_access($x[884],"c_hide_viewprof",1,C_HIDE_VIEWPROF);?>
<?=select_access($x[885],"c_hide_whoseen",1,C_HIDE_WHOSEEN);?>
<?=select_access($x[886],"c_my_temp",1,C_MY_TEMP);?>
<?=select_access($x[887],"c_prof_lnk",0,C_PROF_LNK);?>
<?=select_access($x[888],"c_opt_nomail",0,C_OPT_NOMAIL);?>
<?=select_access($x[917],"c_no_kiss",0,C_NO_KISS);?>
<?=select_access($x[918],"c_super_kiss",0,C_SUPER_KISS);?>
<?php table_footer()?>
<?php 
break;
	case 'main':
		JSEditor();
?>
<?php table_header($x[196])?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[554]?></td>
      <td><input type="text" name="c_welcome_title" class=input value="<?=@C_WELCOME_TITLE?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[555]?></td>
      <td><textarea name="c_welcome_message" rows="10" cols="80" class=input style="width:100%"><?=@C_WELCOME_MESSAGE?></textarea></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[797]?></td>
      <td><?=checkbox(C_SMALL_INDEX,"c_small_index");?></td></tr>
		<?=select_access($x[368],"c_back_welcome",0,C_BACK_WELCOME);?>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[197]?></td>
      <td><?=checkbox(C_RECOMMENDED,"c_recommended");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[198]?></td>
      <td><input type="text" name="c_recom_num" class=sinput value="<?=C_RECOM_NUM?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[199]?></td>
      <td><?=checkbox(C_QSEARCH_BIRTHDAY,"c_qsearch_birthday");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[878]?></td>
      <td><?=checkbox(C_LASTREG_BG,"c_lastreg_bg");?></td></tr>
          <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[200]?></td>
      <td><?=checkbox(C_LAST_WITH_PHOTOS,"c_last_with_photos");?></td></tr>
      <?=select_access($x[781],"c_last_show_photos",1,C_LAST_SHOW_PHOTOS);?>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[201]?></td>
      <td><?=checkbox(C_MOST_POPULAR,"c_most_popular");?></td></tr>
		<?=select_access($x[370],"c_top_more",1,C_TOP_MORE);?>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[202]?></td>
      <td><?=checkbox(C_NEWS_ALLOW,"c_news_allow");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[203]?></td>
      <td><input type="text" name="c_news_num" class=sinput value="<?=C_NEWS_NUM?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[204]?></td>
      <td><?=checkbox(C_TOP_MANS,"c_top_mans");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[205]?></td>
      <td><?=checkbox(C_TOP_WOMANS,"c_top_womans");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[206]?></td>
      <td><input type="text" name="c_top_num" class=sinput value="<?=C_TOP_NUM?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[224]?></td>
      <td><?=checkbox(C_BIRTHDAY,"c_birthday");?></td></tr>
<?php table_footer()?>
<?php 
break;
	case 'misc':
?>
<?php table_header($x[172])?>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[876]?></td>
      <td><?=checkbox(C_LOGIN_SECURE,"c_login_secure");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[343]?></td>
      <td><?=checkbox(C_CLEAR_HITS,"c_clear_hits");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[212]?></td>
      <td><select name="c_security" class=input>
      <?php 
      $p=0;while(isset($xs[$p])) {
      	if (C_SECURITY == $p) {
      		echo '<option selected value="'.$p.'">'.$xs[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xs[$p];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[229]?></td>
      <td><?=checkbox(C_NO_NAME,"c_no_name");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$w[557]?></td>
      <td><select name="c_adrbook_sort" class=input>
      <?php 
      $p=0;while(isset($xadr[$p])) {
      	if (C_ADRBOOK_SORT == $p) {
      		echo '<option selected value="'.$p.'">'.$xadr[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xadr[$p];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[230]?></td>
      <td><?=checkbox(C_REPLYS,"c_replys");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[231]?></td>
      <td><?=checkbox(C_REPLYM,"c_replym");?></td></tr>
    <?=show_input($x[912],C_MYSQL_CHARSET,"c_mysql_charset","input")?>
    <?=show_input($x[307],C_DATE_FORMAT,"c_date_format","input")?>
    <?=show_input($x[308],C_LONG_DATE_FORMAT,"c_long_date_format","input")?>
    <?=show_input($x[309],C_TIME_DIFF,"c_time_diff","input")?>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[880]?></td>
      <td><?=checkbox(C_DATE2PHRASE,"c_date2phrase");?></td></tr>    

<?php table_footer()?>
<?php 
break;
	
	case 'imop':
?>
<?php table_header($x[786])?>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[753]?></td>
      <td><?=checkbox(C_MESS_TARGET,"c_mess_target");?></td></tr>
    <?=show_input($x[772],C_WIN_REFRESH,"c_win_refresh","sinput")?>
    <?=select_access($x[784],"c_imsound",0,C_IMSOUND);?>
    <?=select_access($x[891],"c_ajax_msginf",0,C_AJAX_MSGINF);?>
    <?=show_input($x[892],C_MSGINF_SEC,"c_msginf_sec","sinput")?>
		<?=show_checkbox($x[785],C_IMSOUND_DEFAULT,"c_imsound_default")?>
    <?=show_input($x[814],C_SMILES_NUMBER,"c_smiles_number","sinput")?>
		<?php table_footer()?>
<?php 
break;
	case 'reg':
?>
<?php table_header($x[491])?>
		<?=select_access($x[490],"c_register_access",1,C_REGISTER_ACCESS);?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[507]?></td>
      <td><select name="c_register_type" class=input>
      <?php 
      $p=0;while(isset($xreg[$p])) {
      	if (C_REGISTER_TYPE == $p) {
      		echo '<option selected value="'.$p.'">'.$xreg[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xreg[$p];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
		<?=show_input($x[402],C_LOGIN_CHARS,"c_login_chars","input")?>
    <?=show_checkbox($x[492],C_REGISTER_NOTE,"c_register_note")?>
    <?=show_checkbox($x[782],C_REG_TERMS,"c_reg_terms")?>
    <?=show_checkbox($x[939],C_STATES_FULL,"c_states_full")?>
    <?=show_checkbox($x[313],C_AUTO_LOGIN,"c_auto_login")?>    
      <?=show_input($x[879],C_LOGIN_URL,"c_login_url","input")?>
    <?=show_checkbox($x[877],C_CHECK_ALL,"c_check_all")?>
<?php table_footer()?>
<?php 
break;
	case 'mg':
?>
<?php table_header($x[213])?>
    <?=select_access($x[214],"c_feedback",1,C_FEEDBACK);?>
    <?=select_access($x[215],"c_feedback_mail",0,C_FEEDBACK_MAIL);?>
    <?=select_access($x[219],"c_bad",1,C_BAD);?> 
    <?=select_access($x[220],"c_view",1,C_VIEW);?> 
    <?=select_access($x[221],"c_mail",1,C_MAIL);?>  
    <?=select_access($x[222],"c_another_mail",0,C_ANOTHER_MAIL);?> 
    <?=select_access('Who can send email through administrator',"c_mail_through_admin",1,C_MAIL_THROUGH_ADMIN);?> 
    <?=select_access($x[223],"c_faq_for",1,C_FAQ_FOR);?> 
    <?=select_access($x[344],"c_links",1,C_LINKS);?> 
    <?=select_access($x[506],"c_matchmaker",0,C_MATCHMAKER);?> 
    <?=select_access($x[619],"c_visited_profiles",0,C_VISITED_PROFILES);?> 
    <?=select_access($x[624],"c_birthday_with_day",1,C_BIRTHDAY_WITH_DAY);?>  
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[225]?></td>
      <td><?=checkbox(C_RECOM,"c_recom");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[226]?></td>
      <td><?=checkbox(C_HAVE_MESSAGE,"c_have_message");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[227]?></td>
      <td><?=checkbox(C_ADD_CONFIRM,"c_add_confirm");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[488]?></td>
      <td><?=checkbox(C_KISS_NOTIFY,"c_kiss_notify");?></td></tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[228]?></td>
      <td><select name="c_show_user" class=input>
      <?php 
      $p=0;while(isset($xn[$p])) {
      	if (C_SHOW_USER == $p) {
      		echo '<option selected value="'.$p.'">'.$xn[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xn[$p];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[254]?><br><?=$x[255]?></td>
      <td><input type="text" name="c_username" class=minput value="<?=C_USERNAME?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[246]?></td>
      <td><input type="text" name="c_login_cookie" class=minput value="<?=C_LOGIN_COOKIE?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[252]?></td>
      <td><input type="text" name="c_mpages" class=sinput value="<?=C_MPAGES?>"></td>
    </tr>
<?php table_footer()?>
<?php 
break;
	case 'mui':
?>
<?php table_header($x[556])?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[141]?></td>
      <td><select name="c_show_lang" class=input>
      <?php 
      $p=0;while(isset($xl[$p])) {
      	if (C_SHOW_LANG == $p) {
      		echo '<option selected value="'.$p.'">'.$xl[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xl[$p];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[142]?></td>
      <td><select name="c_multlang_def" class=input>
      <?=options(C_MULTLANG_DEF,$x[144],$x[143]);?>      
      </select>
      </td>
    </tr>
<tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[867]?></td>
      <td><select name="c_userlang" class=input>
      <?php 
      $existlang = explode(",",C_AVAILABLE_LANG);
      $p=0;while(isset($existlang[$p])) {
      	if (C_USERLANG == $existlang[$p]) {
      		echo '<option selected value="'.$existlang[$p].'">'.$reallangname[$existlang[$p]];
      	} else {
      		echo '<option value="'.$existlang[$p].'">'.$reallangname[$existlang[$p]];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>    
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[557]?></td>
      <td><input type="text" name="c_default_lang" class=sinput value="<?=C_DEFAULT_LANG?>"></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[558]?></td>
      <td><select name="c_edit_lang" class=input>
      <?php 
      $existlang = explode(",",C_AVAILABLE_LANG);
      $p=0;while(isset($existlang[$p])) {
      	if (C_EDIT_LANG == $existlang[$p]) {
      		echo '<option selected value="'.$existlang[$p].'">'.$reallangname[$existlang[$p]];
      	} else {
      		echo '<option value="'.$existlang[$p].'">'.$reallangname[$existlang[$p]];
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
		<?=show_checkbox($x[559],C_NEWS_MLTLNG,"c_news_mltlng")?>
		<?=show_checkbox($x[560],C_FAQ_MLTLNG,"c_faq_mltlng")?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=40%><?=$x[454]?></td>
      <td>
      <input type="hidden" name="c_available_lang[]__" value="default">
      <?php 
      $existlang = explode(",",C_AVAILABLE_LANG);
      $langname=array();
      $handle=opendir(C_PATH.'/languages/');
      while (false!==($file = readdir($handle)))
      {
      	if ($file != "." && $file != "..")
      	{
      		if(is_dir(C_PATH.'/languages/'.$file))
      		{
      			$langname[]=$file;
      			$checked = (in_array($file, $existlang)) ? 'checked' : '';

				?>
				<input type="checkbox" name="c_available_lang[]" value="<?=$file?>" <?=$checked?>> - <?=$reallangname[$file]?>: languages/<?=$file?><BR>

				<?PHP
      		}
      	}
      }
      closedir($handle);
?>
      </td>
    </tr>
<?php table_footer()?>
<?php 
break;
	case 'muieditmails':
		//array('x',-1,18),array('x',19,95),
		$mail_phrases=array(array('w',43,44),array('w',405,407),array('w',195,291),array('w',405,406),array('x',-1,18),array('x',19,95),array('w',390,'mail2subscribers.php'),array('w',559,560),array('w',287,286),array('w',301,302),array('w',0,406,405),array('w',538,537),array('w',540,539),array('w',-1,515),array('w',191,192),array('w',0,197,195),array('w',442,441),array('w',391,392),array('w',0,389,390),array('w',0,397,390),array('w',704,703),array('w',706,705));
		if(!isset($from)) $from=0;
		$file=C_PATH.'/languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'.php';
		$filew='languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'.php';
		$file2=C_PATH.'/languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'a.php';
		$filex='languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'a.php';
		if(!(file_exists(C_PATH.'/languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'.php'))) sprintm(template($x[476],array($filew)));
		if(!(is_writable(C_PATH.'/languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'.php'))) sprintm(template($x[477],array($filew)));
		if(!(file_exists(C_PATH.'/languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'a.php'))) sprintm(template($x[476],array($filex)));
		if(!(is_writable(C_PATH.'/languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'a.php'))) sprintm(template($x[477],array($filex)));

		@include(C_PATH.'/languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'.php');
		$edit_w=$w;
		@include(C_PATH.'/languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'a.php');
		$edit_x=$x;
		@include(C_PATH.'/languages/default/default.php');
		@include(C_PATH.'/languages/default/defaulta.php');

		$tot=count($w);
		function MailMes($arr)
		{
			Global $edit_w,$edit_x;
			$p=0;while(isset($arr[$p]))
			{
				$tmp = $arr[$p];
				$tmpw = ($tmp[0] == 'w') ? $edit_w : $edit_x;
				$thisvar = ($tmp[0] == 'w') ? 'c_edit_phrase' : 'c_xedit_phrase';
				if($tmp[1] == 0)
				{
					echo '<tr class=desc bgcolor="'.colors().'"><td width=50%>'.$edit_x[26].' - '.$edit_x[714].@$tmp[3].' *</td><td width=50%>'.template($edit_x[716],array(@$tmp[3])).'</td></tr>';
				} elseif($tmp[1] == -1) {
					echo '<tr class=desc bgcolor="'.colors().'"><td width=100% colspan=2>'.$edit_x[717].'</td></tr>';
				} else {
					echo '<tr class=desc bgcolor="'.colors().'"><td width=50%>'.$edit_x[26].' - '.$edit_x[714].@$tmp[1].'</td><td width=50%><textarea name="'.$thisvar.'['.@$tmp[1].']" rows="3" cols="80" class=binput>'.@$tmpw[@$tmp[1]].'</textarea></td></tr>';
				}
				if(isset($tmp[2]))
				{
					if(is_numeric($tmp[2])) echo '<tr class=desc bgcolor="'.colors().'"><td width=50%>'.$edit_x[27].' - '.$edit_x[714].@$tmp[2].'</td><td width=50%><textarea name="'.$thisvar.'['.@$tmp[2].']" rows="3" cols="80" class=binput>'.@$tmpw[@$tmp[2]].'</textarea></td></tr>';
					else echo '<tr class=desc bgcolor="'.colors().'"><td width=50%>'.$edit_x[27].' - '.@$tmp[2].'</td><td width=50%>'.$edit_x[715].' templates/*/'.@$tmp[2].'</td></tr>';
					/* $k=3;while(isset($tmp[$k]))
					{
					echo '<tr class=desc bgcolor="'.colors().'"><td width=50%>'.@$tmp[$k].'</td><td width=50%><textarea name="c_edit_phrase['.@$tmp[$k].']" rows="3" cols="1" class=binput>'.@$tmpw[$k].'</textarea></td></tr>';
					$k++;
					}
					*/
				}
				echo '<tr class=desc bgcolor="'.COLORH.'"><td width=100% colspan=2></td></tr>';
				$p++;
			}
		}
?>
<input type="hidden" name="do" value="muieditmails">
<?php table_header($x[713],1)?>
    <tr class=desc bgcolor="<?=colors()?>" align=center>
      <td width=50%><strong><?=$reallangname[C_ADMINLANG]?></strong></td>
      <td><strong><?=$reallangname[C_EDIT_LANG]?></strong></td>
    </tr>
<?php
MailMes($mail_phrases);
?>
<?php table_footer()?>
<?php 
break;


	case 'muiedit':

		if(!isset($from)) $from=0;
		$file=C_PATH.'/languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'.php';
		$filew='languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'.php';
		if(!(file_exists(C_PATH.'/languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'.php'))) sprintm(template($x[476],array($filew))); if(!(is_writable(C_PATH.'/languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'.php'))) sprintm(template($x[477],array($filew)));

		@include(C_PATH.'/languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'.php');
		$edit_w=$w;
		@include(C_PATH.'/languages/default/defaulta.php');

		$tot=count($w);
?>

<?=$x[478]?>
<?php
for($k=0;$k<$tot;$k+=50) {
?>
<a href="<?=C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=muiedit&from='.$k?>" target="main"><?=$k?>-<?=$k+49?></a> 
<?php }?>
<input type="hidden" name="do" value="muiedit">
<?php table_header($x[561],1)?>
    <tr class=desc bgcolor="<?=colors()?>" align=center>
      <td width=50% colspan="2"><strong><?=$reallangname[C_ADMINLANG]?></strong></td>
      <td><strong><?=$reallangname[C_EDIT_LANG]?></strong></td>
    </tr>
<?php
for($p=$from;$p<$from+50;$p++) {
?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=1%><?=$p?></td>
      <td width=49%><?=@$w[$p]?></td>
      <td width=50%><textarea name="c_edit_phrase[<?=$p?>]" rows="3" cols="80" class=binput><?=@$edit_w[$p]?></textarea></td>
    </tr>
<?php
}
?>
<?php table_footer()?>
<?php 
break;


	case 'msctrl':
?>
<?php table_header($x[350])?>
 <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[356]?></td>
 <td><?=checkbox(C_ENABLE_MSCTRL,"c_enable_msctrl");?></td></tr>
 <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[369]?></td>
 <td><?=checkbox(C_SEND_ONFIRST,"c_send_onfirst");?></td></tr>
 <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[989]?></td>
 <td><input class="minput" type="text" name="c_remove_message_in_days" value="<?=C_REMOVE_MESSAGE_IN_DAYS?>"></td></tr>
<?=ShowVars($x[354],$wpu,$wps,'wps')?>
<?=ShowVars($x[355],$wg,$wmg,'wmg')?>
<?php table_footer()?>
<?php 
break;
	
	case 'stat':
?>
<?php table_header($x[250])?>
      <?=select_access($x[509],"c_statistics",1,C_STATISTICS);?>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[121]?></td>
      <td><select name="c_stat_country" class=input><?php 
      $p=0;while(isset($xst[$p])) {
      	if (C_STAT_COUNTRY == $p) {
      		echo '<option selected value="'.$p.'">'.$xst[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xst[$p];
      }$p++;}?></select></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[132]?></td>
      <td><select name="c_stat_gender" class=input><?php 
      $p=0;while(isset($xst[$p])) {
      	if (C_STAT_GENDER == $p) {
      		echo '<option selected value="'.$p.'">'.$xst[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xst[$p];
      }$p++;}?></select></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[134]?></td>
      <td><select name="c_stat_marital" class=input><?php 
      $p=0;while(isset($xst[$p])) {
      	if (C_STAT_MARITAL == $p) {
      		echo '<option selected value="'.$p.'">'.$xst[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xst[$p];
      }$p++;}?></select></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[133]?></td>
      <td><select name="c_stat_relation" class=input><?php 
      $p=0;while(isset($xst[$p])) {
      	if (C_STAT_RELATION == $p) {
      		echo '<option selected value="'.$p.'">'.$xst[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xst[$p];
      }$p++;}?></select></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[135]?></td>
      <td><select name="c_stat_children" class=input><?php 
      $p=0;while(isset($xst[$p])) {
      	if (C_STAT_CHILDREN == $p) {
      		echo '<option selected value="'.$p.'">'.$xst[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xst[$p];
      }$p++;}?></select></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[130]?></td>
      <td><select name="c_stat_height" class=input><?php 
      $p=0;while(isset($xst[$p])) {
      	if (C_STAT_HEIGHT == $p) {
      		echo '<option selected value="'.$p.'">'.$xst[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xst[$p];
      }$p++;}?></select></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[131]?></td>
      <td><select name="c_stat_weight" class=input><?php 
      $p=0;while(isset($xst[$p])) {
      	if (C_STAT_WEIGHT == $p) {
      		echo '<option selected value="'.$p.'">'.$xst[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xst[$p];
      }$p++;}?></select></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[136]?></td>
      <td><select name="c_stat_hair" class=input><?php 
      $p=0;while(isset($xst[$p])) {
      	if (C_STAT_HAIR == $p) {
      		echo '<option selected value="'.$p.'">'.$xst[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xst[$p];
      }$p++;}?></select></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[137]?></td>
      <td><select name="c_stat_eyes" class=input><?php 
      $p=0;while(isset($xst[$p])) {
      	if (C_STAT_EYES == $p) {
      		echo '<option selected value="'.$p.'">'.$xst[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xst[$p];
      }$p++;}?></select></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[138]?></td>
      <td><select name="c_stat_etnicity" class=input><?php 
      $p=0;while(isset($xst[$p])) {
      	if (C_STAT_ETNICITY == $p) {
      		echo '<option selected value="'.$p.'">'.$xst[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xst[$p];
      }$p++;}?></select></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[139]?></td>
      <td><select name="c_stat_religion" class=input><?php 
      $p=0;while(isset($xst[$p])) {
      	if (C_STAT_RELIGION == $p) {
      		echo '<option selected value="'.$p.'">'.$xst[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xst[$p];
      }$p++;}?></select></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[140]?></td>
      <td><select name="c_stat_smoke" class=input><?php 
      $p=0;while(isset($xst[$p])) {
      	if (C_STAT_SMOKE == $p) {
      		echo '<option selected value="'.$p.'">'.$xst[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xst[$p];
      }$p++;}?></select></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[141]?></td>
      <td><select name="c_stat_drink" class=input><?php 
      $p=0;while(isset($xst[$p])) {
      	if (C_STAT_DRINK == $p) {
      		echo '<option selected value="'.$p.'">'.$xst[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xst[$p];
      }$p++;}?></select></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[142]?></td>
      <td><select name="c_stat_education" class=input><?php 
      $p=0;while(isset($xst[$p])) {
      	if (C_STAT_EDUCATION == $p) {
      		echo '<option selected value="'.$p.'">'.$xst[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xst[$p];
      }$p++;}?></select></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[86]?></td>
      <td><select name="c_stat_hdyfu" class=input><?php 
      $p=0;while(isset($xst[$p])) {
      	if (C_STAT_HDYFU == $p) {
      		echo '<option selected value="'.$p.'">'.$xst[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xst[$p];
      }$p++;}?></select></td></tr>
    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$w[129]?></td>
      <td><select name="c_stat_horo" class=input><?php 
      $p=0;while(isset($xst[$p])) {
      	if (C_STAT_HORO == $p) {
      		echo '<option selected value="'.$p.'">'.$xst[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xst[$p];
      }$p++;}?></select></td></tr>
      
<?php table_footer()?>
<?php 
break;
	case 'delpro':
?>
    <?php table_header($x[652])?>
    <?=show_checkbox($x[661],C_ADMIN_REMOVE_CONFIRM,"c_admin_remove_confirm")?>
    <?=show_checkbox($x[663],C_ADMIN_REMOVE_CHECKBOX_MAIL,"c_admin_remove_checkbox_mail")?>
    <tr class=desc bgcolor="<?=colors()?>"><td width=100% colspan=2><?=$x[654]?></td></tr>
    <?=show_checkbox($x[653],C_ADMIN_REMOVE_PERM,"c_admin_remove_perm")?>
    <?=show_checkbox($x[656],C_ADMIN_REMOVE_PERM_WO_CONFIRM,"c_admin_remove_perm_wo_confirm")?>
    <?=show_checkbox($x[657],C_ADMIN_REMOVE_MAIL,"c_admin_remove_mail")?>
    <?=show_checkbox($x[658],C_ADMIN_REMOVE_MAIL_SUBJ,"c_admin_remove_mail_subj")?>
    <?=show_checkbox($x[659],C_ADMIN_REMOVE_MAIL_MESS,"c_admin_remove_mail_mess")?>
    <?=show_checkbox($x[660],C_ADMIN_REMOVE_MAIL_LANG_ADMIN,"c_admin_remove_mail_lang_admin")?>
    <?php table_footer()?><br>
<?php
break;
	
	case 'adopt':
		$fld1=array($w[126],$w[118],$w[208],$w[209],$w[210].' ('.$w[129].')',$w[132],$w[133],$w[121],$w[211],$w[144],$w[145],$w[146],$w[147],$w[285],$w[122],$w[378],$w[134],$w[135],$w[130],$w[131],$w[136],$w[137],$w[138],$w[139],$w[140],$w[141],$w[142],$w[382],$w[459],$w[213],$w[214]);
		$fld2=array($w[215],$w[80],$w[444],$w[445],$w[446],$w[447],$w[448],$w[449],$w[450],$w[451],$w[81],$w[82],$w[83],$w[84],$w[85],$w[87],$w[292],$w[54],$x[72],$x[73],$x[74],$x[99],$x[481],$w[365],$w[124],$w[123],$w[86],$x[510],$x[511],$w[524],$w[525]);
		$fld3=array($w[526],$w[527],$x[738],$w[745],$w[806]);

?>
<script language="Javascript">
function CountChecked()
{
	count = document.forms[0].elements.length;
	var MyCount1 = 0;
	var MyCount2 = 0;
	var MyCount3 = 0;
	var MyCount4 = 0;
	var MyCount5 = 0;
	var MyCount6 = 0;
	for (i=0; i < count; i++)
	{
		if(document.forms[0].elements[i].checked == 1 && document.forms[0].elements[i].name == "edit1")
		{
			MyCount1 += parseInt(document.forms[0].elements[i].value);
		}
		if(document.forms[0].elements[i].checked == 1 && document.forms[0].elements[i].name == "view1")
		{
			MyCount2 += parseInt(document.forms[0].elements[i].value);
		}
		if(document.forms[0].elements[i].checked == 1 && document.forms[0].elements[i].name == "edit2")
		{
			MyCount3 += parseInt(document.forms[0].elements[i].value);
		}
		if(document.forms[0].elements[i].checked == 1 && document.forms[0].elements[i].name == "view2")
		{
			MyCount4 += parseInt(document.forms[0].elements[i].value);
		}
		if(document.forms[0].elements[i].checked == 1 && document.forms[0].elements[i].name == "edit3")
		{
			MyCount5 += parseInt(document.forms[0].elements[i].value);
		}
		if(document.forms[0].elements[i].checked == 1 && document.forms[0].elements[i].name == "view3")
		{
			MyCount6 += parseInt(document.forms[0].elements[i].value);
		}
	}
	document.forms[0].c_adminedit1.value = MyCount1;
	document.forms[0].c_adminview1.value = MyCount2;
	document.forms[0].c_adminedit2.value = MyCount3;
	document.forms[0].c_adminview2.value = MyCount4;
	document.forms[0].c_adminedit3.value = MyCount5;
	document.forms[0].c_adminview3.value = MyCount6;
}

</script>
    <?php table_header($x[562])?>
    <?=show_checkbox($x[563],C_EDITVIEWEMPTY,"c_editviewempty",2)?>
    <input type=hidden name=c_adminedit1 value="<?=C_ADMINEDIT1?>">
    <input type=hidden name=c_adminview1 value="<?=C_ADMINVIEW1?>">
    <input type=hidden name=c_adminedit2 value="<?=C_ADMINEDIT2?>">
    <input type=hidden name=c_adminview2 value="<?=C_ADMINVIEW2?>">
    <input type=hidden name=c_adminedit3 value="<?=C_ADMINEDIT3?>">
    <input type=hidden name=c_adminview3 value="<?=C_ADMINVIEW3?>">
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
     <tr class=desc bgcolor="<?=colors()?>"><td width=70% align=center><?=$x[564]?></td>
      <td align=center>
			<?=$x[565]?>
			</td>
			      <td align=center>
			<?=$x[566]?>
			</td></tr>
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
      <?php for ($p = 0; $p < count($fld1); $p++) {
      	$adm = pow(2,$p);
      	$k = 1;
        ?>
     <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$fld1[$p]?></td>
      <td align=center>
			<?php
			$checked = (C_ADMINEDIT1 & $adm) ? ' checked' : '';
			?>
			<input type="checkbox" name="edit<?=$k?>" <?=$checked?> value=<?=$adm?>>
			</td>
			      <td align=center>
			<?php
			$checked = (C_ADMINVIEW1 & $adm) ? ' checked' : '';
			?>
			<input type="checkbox" name="view<?=$k?>" <?=$checked?> value=<?=$adm?>>
			</td></tr>
    <?php
      }
    ?>
      <?php for ($p = 0; $p < count($fld2); $p++) {
      	$adm = pow(2,$p);
      	$k = 2;
        ?>
     <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$fld2[$p]?></td>
      <td align=center>
			<?php
			$checked = (C_ADMINEDIT2 & $adm) ? ' checked' : '';
			?>
			<input type="checkbox" name="edit<?=$k?>" <?=$checked?> value=<?=$adm?>>
			</td>
			      <td align=center>
			<?php
			$checked = (C_ADMINVIEW2 & $adm) ? ' checked' : '';
			?>
			<input type="checkbox" name="view<?=$k?>" <?=$checked?> value=<?=$adm?>>
			</td></tr>
    <?php
      }
    ?>
      <?php for ($p = 0; $p < count($fld3); $p++) {
      	$adm = pow(2,$p);
      	$k = 3;
        ?>
     <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$fld3[$p]?></td>
      <td align=center>
			<?php
			$checked = (C_ADMINEDIT3 & $adm) ? ' checked' : '';
			?>
			<input type="checkbox" name="edit<?=$k?>" <?=$checked?> value=<?=$adm?>>
			</td>
			      <td align=center>
			<?php
			$checked = (C_ADMINVIEW3 & $adm) ? ' checked' : '';
			?>
			<input type="checkbox" name="view<?=$k?>" <?=$checked?> value=<?=$adm?>>
			</td></tr>
    <?php
      }
    ?>
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
	
    <?php table_footer()?><br>
<?php
break;
	case 'regshow':
		$fld1=array($w[208],$w[209],$w[210].' ('.$w[129].')',$w[133],$w[524],$w[525],$w[121],$w[144],$w[145],$w[146],$w[147],$w[527],$w[285],$w[122],$w[378],$w[134],$w[135],$w[130],$w[131],$w[136],$w[137],$w[138],$w[139],$w[140],$w[141],$w[142],$w[382],$w[459],$w[213],$w[214],$w[526]);
		$fld2=array($w[215],$w[80],$w[444],$w[445],$w[446],$w[447],$w[448],$w[449],$w[450],$w[451],$w[81],$w[82],$w[83],$w[84],$w[85],$w[87],$w[292],$w[86],$w[548]);


?>
<script language="Javascript">
function CountChecked()
{
	count = document.forms[0].elements.length;
	var MyCount1 = 0;
	var MyCount2 = 0;
	var MyCount3 = 0;
	var MyCount4 = 0;
	for (i=0; i < count; i++)
	{
		if(document.forms[0].elements[i].checked == 1 && document.forms[0].elements[i].name == "regf1")
		{
			MyCount1 += parseInt(document.forms[0].elements[i].value);
		}
		if(document.forms[0].elements[i].checked == 1 && document.forms[0].elements[i].name == "regf2")
		{
			MyCount2 += parseInt(document.forms[0].elements[i].value);
		}
		if(document.forms[0].elements[i].checked == 1 && document.forms[0].elements[i].name == "regu1")
		{
			MyCount3 += parseInt(document.forms[0].elements[i].value);
		}
		if(document.forms[0].elements[i].checked == 1 && document.forms[0].elements[i].name == "regu2")
		{
			MyCount4 += parseInt(document.forms[0].elements[i].value);
		}
	}
	document.forms[0].c_register_fields1.value = MyCount1;
	document.forms[0].c_register_fields2.value = MyCount2;
	document.forms[0].c_update_fields1.value = MyCount3;
	document.forms[0].c_update_fields2.value = MyCount4;
}

</script>
    <?php table_header($x[567])?>
    <input type=hidden name=c_register_fields1 value="<?=C_REGISTER_FIELDS1?>">
    <input type=hidden name=c_register_fields2 value="<?=C_REGISTER_FIELDS2?>">
    <input type=hidden name=c_update_fields1 value="<?=C_UPDATE_FIELDS1?>">
    <input type=hidden name=c_update_fields2 value="<?=C_UPDATE_FIELDS2?>">
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
     <tr class=desc bgcolor="<?=colors()?>"><td width=70% align=center><?=$x[564]?></td>
      <td align=center>
			<?=$w[89]?>
			</td><td align=center>
			<?=$w[99]?>
			</td></tr>
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
      <?php for ($p = 0; $p < count($fld1); $p++) {
      	$adm = pow(2,$p);
      	$k = 1;
        ?>
     <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$fld1[$p]?></td>
      <td align=center>
			<?php
			$checked = (C_REGISTER_FIELDS1 & $adm) ? ' checked' : '';
			?>
			<input type="checkbox" name="regf<?=$k?>" <?=$checked?> value=<?=$adm?>>
			</td>
			<td align=center>
			<?php
			$checked = (C_UPDATE_FIELDS1 & $adm) ? ' checked' : '';
			?>
			<input type="checkbox" name="regu<?=$k?>" <?=$checked?> value=<?=$adm?>>
			</td></tr>
    <?php
      }
    ?>
      <?php for ($p = 0; $p < count($fld2); $p++) {
      	$adm = pow(2,$p);
      	$k = 2;
        ?>
     <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$fld2[$p]?></td>
      <td align=center>
			<?php
			$checked = (C_REGISTER_FIELDS2 & $adm) ? ' checked' : '';
			?>
			<input type="checkbox" name="regf<?=$k?>" <?=$checked?> value=<?=$adm?>>
			</td><td align=center>
			<?php
			$checked = (C_UPDATE_FIELDS2 & $adm) ? ' checked' : '';
			?>
			<input type="checkbox" name="regu<?=$k?>" <?=$checked?> value=<?=$adm?>>
			</td></tr>
    <?php
      }
    ?>
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
	
    <?php table_footer()?><br>
<?php
break;
	case 'notices_fields':
		$fld1=array($w[208],$w[209],$w[210].' ('.$w[129].')',$w[133],$w[524],$w[525],$w[121],$w[144],$w[145],$w[146],$w[147],$w[527],$w[285],$w[122],$w[378],$w[134],$w[135],$w[130],$w[131],$w[136],$w[137],$w[138],$w[139],$w[140],$w[141],$w[142],$w[382],$w[459],$w[213],$w[214],$w[526]);
		$fld2=array($w[215],$w[80],$w[444],$w[445],$w[446],$w[447],$w[448],$w[449],$w[450],$w[451],$w[81],$w[82],$w[87],$w[292],$w[86]);
		$fld3=array($x[675],$x[677],$x[678]);

?>
<script language="Javascript">
function CountChecked()
{
	count = document.forms[0].elements.length;
	var MyCount1 = 0;
	var MyCount2 = 0;
	var MyCount3 = 0;
	for (i=0; i < count; i++)
	{
		if(document.forms[0].elements[i].checked == 1 && document.forms[0].elements[i].name == "regf1")
		{
			MyCount1 += parseInt(document.forms[0].elements[i].value);
		}
		if(document.forms[0].elements[i].checked == 1 && document.forms[0].elements[i].name == "regf2")
		{
			MyCount2 += parseInt(document.forms[0].elements[i].value);
		}
		if(document.forms[0].elements[i].checked == 1 && document.forms[0].elements[i].name == "types")
		{
			MyCount3 += parseInt(document.forms[0].elements[i].value);
		}
	}
	document.forms[0].c_notices_fields1.value = MyCount1;
	document.forms[0].c_notices_fields2.value = MyCount2;
	document.forms[0].c_notices_types.value = MyCount3;
}

</script>
<?php table_header($x[673])?>
    <input type=hidden name=c_notices_fields1 value="<?=C_NOTICES_FIELDS1?>">
    <input type=hidden name=c_notices_fields2 value="<?=C_NOTICES_FIELDS2?>">
    <input type=hidden name=c_notices_types value="<?=C_NOTICES_TYPES?>">
        <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
      <?php for ($p = 0; $p < count($fld3); $p++) {
      	$adm = pow(2,$p);
      	$k = 1;
        ?>
     <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$fld3[$p]?></td>
      <td align=center>
			<?php
			$checked = (C_NOTICES_TYPES & $adm) ? ' checked' : '';
			?>
			<input type="checkbox" name="types" <?=$checked?> value=<?=$adm?>>
			</td>
			</tr>
    <?php
      }
    ?>

    <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[679]?></td>
      <td><input type="text" name="c_notices_about_len" class=minput value="<?=C_NOTICES_ABOUT_LEN?>"></td></tr>
     <?=show_checkbox($x[893],C_NOT_PRIV,"c_not_priv")?>    
		<?=select_access($x[849],"c_not_priv_a",3,C_NOT_PRIV_A);?>
		    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[850]?></td>
      <td><select name="c_not_priv_g" class=input><?php 
      $p=0;while(isset($wg[$p])) {
      	$txt = ($p == 0) ? $x[236] : $wg[$p];
      	if (C_NOT_PRIV_G == $p) {
      		echo '<option selected value="'.$p.'">'.$txt;
      	} else {
      		echo '<option value="'.$p.'">'.$txt;
      	}
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
    <?php table_footer()?><br>
    <?php table_header($x[674])?>
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
     <tr class=desc bgcolor="<?=colors()?>"><td width=70% align=center><?=$x[564]?></td>
      <td align=center>
			
			</td></tr>

			
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
      <?php for ($p = 0; $p < count($fld1); $p++) {
      	$adm = pow(2,$p);
      	$k = 1;
        ?>
     <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$fld1[$p]?></td>
      <td align=center>
			<?php
			$checked = (C_NOTICES_FIELDS1 & $adm) ? ' checked' : '';
			?>
			<input type="checkbox" name="regf<?=$k?>" <?=$checked?> value=<?=$adm?>>
			</td>
			</tr>
    <?php
      }
    ?>
      <?php for ($p = 0; $p < count($fld2); $p++) {
      	$adm = pow(2,$p);
      	$k = 2;
        ?>
     <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$fld2[$p]?></td>
      <td align=center>
			<?php
			$checked = (C_NOTICES_FIELDS2 & $adm) ? ' checked' : '';
			?>
			<input type="checkbox" name="regf<?=$k?>" <?=$checked?> value=<?=$adm?>>
			</td></tr>
    <?php
      }
    ?>
        <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$x[676]?></td>
      <td><input type="text" name="c_notices_fields_proc" class=minput value="<?=C_NOTICES_FIELDS_PROC?>"></td></tr>

    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
	
    <?php table_footer()?><br>
<?php
break;
	
	case 'searchopt':
		$fld1=array($w[334],$w[335],$w[121],$w[133],$w[524],$w[525],$w[120],$w[129],$w[126],$w[118],$w[127],$w[128],$w[289],$w[122],$w[285],$w[130],$w[131],$w[134],$w[135],$w[136],$w[137],$w[138],$w[139],$w[140],$w[141],$w[142],$w[382],$w[143].' '.$w[144],$w[143].' '.$w[145],$w[143].' '.$w[146],$w[143].' '.$w[147],$w[143].' '.$w[87],$w[143].' '.$w[509],$w[270],$w[148],$w[149],$w[150],$w[526],$w[527],$x[745]);
		$not_in_advsearch=array(12,17,18,19,20,21,22,23,24,25,26,27,28,29,30,37,38);

		$str='';$str2='';for($i=0;$i<count($fld1);$i++) {$str.='document.forms[0].srch_'.$i.'.value+","+';$str2.='document.forms[0].advsrch_'.$i.'.value+","+';}
?>
<script language="Javascript">
function CountChecked()
{
	document.forms[0].c_srch.value = <?=substr($str,0,-5)?>;
	document.forms[0].c_advsrch.value = <?=substr($str2,0,-5)?>;
}

</script>
    <?php table_header($x[568])?>
    <input type=hidden name=c_srch value="<?=C_SRCH?>">
    <input type=hidden name=c_advsrch value="<?=C_ADVSRCH?>">
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
     <tr class=desc bgcolor="<?=colors()?>"><td width=70% align=center><?=$x[564]?></td>
      <td align=center>
			<?=$w[91]?>
			</td>
			      <td align=center>
			<?=$w[125]?>
			</td></tr>
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
      <?php 
      $arr=explode(',',C_SRCH);$arr2=explode(',',C_ADVSRCH);
      for ($p = 0; $p < count($fld1); $p++) {
      	$k = 1;
      	$var = isset($arr[$p]) ? $arr[$p] : -1;
      	if(!in_array($p, $not_in_advsearch)) {
      		$str = '<tr class=desc bgcolor="'.colors().'"><td width=30%>'.$p.'. '.$fld1[$p].'</td><td><select name="srch_'.$p.'" class=minput>';
      		$str .= ($var == '-1') ? ' <option selected value="-1">'.$w[361] :  '<option value="-1">'.$w[361];
      		$i = 0;
      		while(isset($wpu[$i]))
      		{
      			$str .= ($var == $i) ? ' <option selected value="'.$i.'">'.$wpu[$i] :  '<option value="'.$i.'">'.$wpu[$i];
      			$i++;
      		}
      		$str .= ($var == '256') ? ' <option selected value="256">'.$w[370] :  '<option value="256">'.$w[370];
      		$str .= '</select></td>';
      	} else $str = '<tr class=desc bgcolor="'.colors().'"><td width=30%>'.$p.'. '.$fld1[$p].'</td><td><select name="srch_'.$p.'" class=minput disabled><option selected value="256">'.$w[370].'</select></td>';

      	$var2 = isset($arr2[$p]) ? $arr2[$p] : -1;
      	$str .= '<td><select name="advsrch_'.$p.'" class=minput>';
      	$str .= ($var2 == '-1') ? ' <option selected value="-1">'.$w[361] :  '<option value="-1">'.$w[361];
      	$i = 0;
      	while(isset($wpu[$i]))
      	{
      		$str .= ($var2 == $i) ? ' <option selected value="'.$i.'">'.$wpu[$i] :  '<option value="'.$i.'">'.$wpu[$i];
      		$i++;
      	}
      	$str .= ($var2 == '256') ? ' <option selected value="256">'.$w[370] :  '<option value="256">'.$w[370];
      	$str .= '</select></td></tr>';
      	echo $str;
      }
			?> 
			
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
	
    
    <?php table_footer()?>
<?php
break;
	case 'searchres':
		$fld1=array($w[126],$w[118],$w[127],$w[128],$w[120],$w[57],$w[525],$w[524],$w[133],$w[121],$w[122],$w[130],$w[131],$w[80],$w[87],$w[129],$w[124],$w[123],$w[216],$w[270],$w[352],$w[285]);
		$not_in_advsearch=array(0,1);
		$str='';for($i=0;$i<count($fld1);$i++) {$str.='document.forms[0].srchres_'.$i.'.value+","+';}
?>
<script language="Javascript">
function CountChecked()
{
	document.forms[0].c_sr_columns.value = <?=substr($str,0,-5)?>;
}

</script>
    <?php table_header($x[569])?>
    <input type=hidden name=c_sr_columns value="<?=C_SR_COLUMNS?>">
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
     <tr class=desc bgcolor="<?=colors()?>"><td width=70% align=center><?=$x[564]?></td>
      <td align=center>
			<?=$w[116]?>
			</td>
			</tr>
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
      <?php 
      $arr=explode(',',C_SR_COLUMNS);
      for ($p = 0; $p < count($fld1); $p++) {
      	$k = 1;
      	$var = isset($arr[$p]) ? $arr[$p] : -1;
      	if(!in_array($p, $not_in_advsearch)) {
      		$str = '<tr class=desc bgcolor="'.colors().'"><td width=30%>'.$p.'. '.$fld1[$p].'</td><td><select name="srchres_'.$p.'" class=minput>';
      		$str .= ($var == '-1') ? ' <option selected value="-1">'.$w[361] :  '<option value="-1">'.$w[361];
      		$i = 0;
      		while(isset($wpu[$i]))
      		{
      			$str .= ($var == $i) ? ' <option selected value="'.$i.'">'.$wpu[$i] :  '<option value="'.$i.'">'.$wpu[$i];
      			$i++;
      		}
      		$str .= ($var == '256') ? ' <option selected value="256">'.$w[370] :  '<option value="256">'.$w[370];
      		$str .= '</select></td></tr>';
      	} else $str = '<tr class=desc bgcolor="'.colors().'"><td width=30%>'.$p.'. '.$fld1[$p].'</td><td><select name="srchres_'.$p.'" class=minput disabled><option selected value="-1">'.$w[361].'</select></td></tr>';
      	echo $str;
      }
			?> 
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
    <?php table_footer()?>
<?php
break;
	case 'bb_conf':
?>	
    <?php table_header($x[856].': '.$x[121])?>
    <?=show_input($x[955],C_BB_IMG_WIDTHMAX,"c_bb_img_widthmax","sinput")?>
    <?=show_checkbox($x[956],C_BB_IMG_NEWWIN,"c_bb_img_newwin")?>
    <?=show_input($x[957],C_BB_IMG_MAX,"c_bb_img_max","sinput")?>
    <?=show_input($x[958],C_BB_IMG_EXT,"c_bb_img_ext","input")?>
    <?=show_input($x[959],C_BB_VIDEO_MAX,"c_bb_video_max","sinput")?>
    <?=show_input($x[960],C_BB_VIDEO_WIDTH,"c_bb_video_width","sinput")?>
    <?=show_input($x[961],C_BB_VIDEO_HEIGHT,"c_bb_video_height","sinput")?>
    <?php table_footer()?>
<?php
break;
	case 'bb_desc':
		OptionsByACL(-1, $x[856].': '.$x[750].' ('.$w[677].')', array($x[857],$x[858],$x[859],$x[860],$x[861],$x[862],$x[863],$x[864],$x[865]), 'c_bb_desc_acl');
		break;
	case 'bb_prof':
		OptionsByACL(-1, $x[856].': '.$x[750].' ('.$w[492].')', array($x[857],$x[858],$x[859],$x[860],$x[861],$x[862],$x[863],$x[864],$x[865]), 'c_bb_pcom_acl');
		break;
		case 'bb_sm':
		OptionsByACL(0, $x[856].': '.$w[170], array($x[857],$x[858],$x[859],$x[860],$x[861],$x[862],$x[863],$x[864],$x[865]), 'c_bb_sm_acl');
		break;
	case 'bb_test':
		OptionsByACL(0, $x[856].': '.$w[633], array($x[857],$x[858],$x[859],$x[860],$x[861],$x[862],$x[863],$x[864],$x[865]), 'c_bb_tm_acl');
		break;
	case 'tst_res':
		$fld1=array($w[126],$w[118],$w[127],$w[128],$w[120],$w[57],$w[525],$w[524],$w[133],$w[121],$w[122],$w[130],$w[131],$w[80],$w[87],$w[129],$w[124],$w[123],$w[216],$w[270],$w[352]);
		$not_in_advsearch=array(0,1);
		$str='';for($i=0;$i<count($fld1);$i++) {$str.='document.forms[0].srchres_'.$i.'.value+","+';}
?>
<script language="Javascript">
function CountChecked()
{
	document.forms[0].c_tst_columns.value = <?=substr($str,0,-5)?>;
}

</script>

    <?php table_header($w[633])?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[685]?></td>
      <td><select name="c_tst_status" class=input>
      <?php 
      $p=0;while(isset($xtst[$p])) {
      	if (C_TST_STATUS == $p) {
      		echo '<option selected value="'.$p.'">'.$xtst[$p];
      	} else {
      		echo '<option value="'.$p.'">'.$xtst[$p];
      	}
      	$p++;
      }$p=0;
      ?>      
      </select>
      </td>
    </tr>
    <?=select_access($x[682],"c_tst_view",1,C_TST_VIEW);?>
    <?=select_access($x[683],"c_tst_reply",0,C_TST_REPLY);?>
    <?=select_access($x[687],"c_tst_show_count",1,C_TST_SHOW_COUNT);?>
    <?=show_input($x[686],C_TST_CNT,"c_tst_cnt","input")?>
    <?php table_footer()?>


    <?php table_header($x[681])?>
    <input type=hidden name=c_tst_columns value="<?=C_TST_COLUMNS?>">
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
     <tr class=desc bgcolor="<?=colors()?>"><td width=70% align=center><?=$x[564]?></td>
      <td align=center>
			<?=$w[116]?>
			</td>
			</tr>
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
      <?php 
      $arr=explode(',',C_TST_COLUMNS);
      for ($p = 0; $p < count($fld1); $p++) {
      	$k = 1;
      	$var = isset($arr[$p]) ? $arr[$p] : -1;
      	if(!in_array($p, $not_in_advsearch)) {
      		$str = '<tr class=desc bgcolor="'.colors().'"><td width=30%>'.$p.'. '.$fld1[$p].'</td><td><select name="srchres_'.$p.'" class=minput>';
      		$str .= ($var == '-1') ? ' <option selected value="-1">'.$w[361] :  '<option value="-1">'.$w[361];
      		$i = 0;
      		while(isset($wpu[$i]))
      		{
      			$str .= ($var == $i) ? ' <option selected value="'.$i.'">'.$wpu[$i] :  '<option value="'.$i.'">'.$wpu[$i];
      			$i++;
      		}
      		$str .= ($var == '256') ? ' <option selected value="256">'.$w[370] :  '<option value="256">'.$w[370];
      		$str .= '</select></td></tr>';
      	} else $str = '<tr class=desc bgcolor="'.colors().'"><td width=30%>'.$p.'. '.$fld1[$p].'</td><td><select name="srchres_'.$p.'" class=minput disabled><option selected value="-1">'.$w[361].'</select></td></tr>';
      	echo $str;
      }
			?> 
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
    <?php table_footer()?>
<?php
break;
}
?>
    
    <br>
    <input type="submit" value="<?=$x[251]?>" class=button OnClick="CountChecked();">
    </form>
<?php 
			}
			break;
			############################################
			# Options - End
			############################################
			############################################
			# Admin View Profile - Begin
			############################################
		case 'vp':
		    if (!isset($id) && isset($status)){
		        $result = db_query("select id from ".C_MYSQL_MEMBERS_MAIN." WHERE status = '".$status."' ORDER BY editdate DESC LIMIT 1");
		        $count=db_num_rows($result);
		        if($count == '0') sprintm($w[110]);
                while ($i = db_fetch_array($result)) {
                    $id = $i['id'];
                }
		    }

			if (isset($id) && $id != "" && is_numeric($id)) {
				if(isset($fullmode))
				{
					define('FULLMODE','1');
					$modeword='Quick mode';
					$modelink='';
				}
				else
				{
					define('FULLMODE','0');
					$modeword='Full mode';
					$modelink='&fullmode';
				}
				
				if(isset($coupon)){
					db_query("UPDATE ".C_MYSQL_MEMBERS_MAIN." SET coupon = '".$coupon."' WHERE id = '".$id."'");
				}

				$result = db_query("select t1.* ,".correctdate("regdate").", ".correctdate("editdate").", t2.*, t3.* from ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2, ".C_MYSQL_MEMBERS_SEC." as t3 where t1.id = '".$id."' and t1.id = t2.id and t1.id = t3.id");
				$count=db_num_rows($result);
				if($count == '0') sprintm($w[205]);
				while ($i = db_fetch_array($result)) {
					$save_log = empty_log_array();
                                        $save_log['user'] = get_info('adminID');
					$save_log['time'] = date('Y-m-d H:i:s');
					$save_log['id'] = $i['id'];
					$save_log['email'] = $i['email'];
					$save_log['action'] = 'Profil megtekintése';
					log_csv(array($save_log));

					$ipsql = "SELECT count(t3.id) as total FROM ".C_MYSQL_MEMBERS_SEC." as t3 WHERE ip = ".$i['ip']; 
							
					$ipquery = db_query($ipsql) or die(db_error());
				
					$iprows = db_fetch_array($ipquery);
					$i['ipcount'] = $iprows['total'];

					$fpsql = "SELECT count(t3.id) as total FROM ".C_MYSQL_MEMBERS_SEC." as t3 WHERE fingerprint = '".$i['fingerprint']."'"; 
							
					$fpquery = db_query($fpsql) or die(db_error());
				
					$fprows = db_fetch_array($fpquery);
					$i['fpcount'] = $fprows['total'];
					
					$passsql = "SELECT count(t3.id) as total FROM ".C_MYSQL_MEMBERS_SEC." as t3 WHERE password = '".$i['password']."'"; 
					
					$passquery = db_query($passsql) or die(db_error());
				
					$passrows = db_fetch_array($passquery);
					$i['passcount'] = $passrows['total'];
					
					$name = username($i['id'],$i['username'],$i['fname'],$i['lname']);
						?>
<script>
    <!--
    function press_f9(){
        open_win('<?=filename()?>?id=<?=$i['id']?>&do=al&ds=1<?=s()?>','allow');
    }

    function press_f10(){
        open_win('<?=filename()?>?id=<?=$i['id']?>&ds=1&do=sp&<?=s()?>','suspend');
    }

    function press_f5(){
        location.href = '<?=filename()?>?status=<?=$i['status']?>&do=vp';
    }
    -->
</script>
<br>
<center><span class=dat><?=$w[207]?>[<?=$id?>]</span></center>
<br><br>
<table CellSpacing="0" CellPadding="0" width="600" border=0>
<Tr>
<td width="100%" valign="top" align="center">
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="650"><Tr>
<Td width="160" valign="top">
<p style="text-align: center;line-height: 40px;">
    <?="<a href=\"javascript:open_win('".filename()."?id=".$i['id']."&do=al&ds=1".s()."','allow');\">[".$x[75]." (F9)]</a> <br/>  
    <a href=\"javascript:open_win('".filename()."?id=".$i['id']."&ds=1&do=sp&".s()."','suspend');\">[".$x[853]." (F10)]</a> <br/>
    <a href=\"javascript:open_win('".filename()."?id=".$i['id']."&ds=1&do=dp&".s()."','delete');\">[".$x[66]."]</a><br/> 
    <a href=\"".filename()."?id=".$i['id']."&do=ue&".s()."\">[".$x[60]."]</a><br/>
    <a href=\"".filename()."?l=default&a=s&do=mess&fromid=".$i['id'].s()."\" target=\"_blank\">[Üzenetek]</a><br/>
    <a href=\"".filename()."?id=".$i['id']."&username=".$name."&do=smes&".s()."\">[".$w[170]."]</a><br/>
    <a href=\"".filename()."?id=".$i['id']."&do=vp".$modelink."&".s()."\">[".$modeword."]</a><br/>
    <a href=\"".filename()."?status=".$i['status']."&do=vp"."\">[Következő (F5)]</a>";?>
</p>

<p>
<?php if(ShowForEdit(C_ADMINVIEW2,32768,str_replace('0','1',$i['files']))) {?>
<?php
$photos = '';
if(C_USE_GALLERY)
{
	$tmpp=db_query("SELECT photonum FROM ".C_MYSQL_NEW_PHOTOS." WHERE uid='".$id."'");
	while ($kk = db_fetch_array($tmpp))
	{
		$photos = $kk['photonum'];
	}
}

if ($i['files'] != "" || $photos != "" )
{

	$arr=explode(',',AddPicElement($i['files'],$photos));
	$width = (C_TH_WIDTH != 0) ? C_TH_WIDTH : '120';
$p=0;while(isset($arr[$p])) {?>
<a href="<?=ShowPic(PicPath($arr[$p],$i['id'])).ShowPictureBigger()?>" target="_blank"><img src="<?=ShowPic(PicPath($arr[$p],$i['id']))?>" border="<?=C_IMG_BRDR?>" width=<?=$width?>></a>
<?php $p++;}
} else echo ' &nbsp; ';?>
<?php } else echo ' &nbsp; ';?>
</p>

</Td>
<Td valign="top">
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="100%" class=mes>
<?php if(ShowForEdit(C_ADMINVIEW1,1,$i['id'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width="120"><?=$w[126]?>
</td><td><?=$i['id'];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,2,$i['username'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[118]?>
</td><td><?=$i['username'];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,4,$i['fname'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[208]?>
</td><td><?=$i['fname'];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,8,$i['lname'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[209]?>
</td><td><?=$i['lname'];?></td></tr>
<?php }?>

<?php if(ShowForEdit(C_ADMINVIEW2,1,$i['descr'])) {
include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';		
?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[215]?>
</td><td><?=AzDGCode($i['descr']);?></td></tr>
<?php }?>

<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>Kuponkód:
</td><td>
<form method="POST">
<select name="coupon">
<option value="" <?=((empty($i['coupon']))?('selected="selected"'):(''))?>></option>
<?php foreach($coupon_code as $coupon):?>
<option value="<?=$coupon?>" <?=(($coupon == $i['coupon'])?('selected="selected"'):(''))?>><?=$coupon?></option>
<?php endforeach;?>
</select>
<button type="submit">OK</button>
</form>
</td></tr>

<?php if(ShowForEdit(C_ADMINVIEW1,256,$i['email'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[211]?>
</td><td><?=crm($i['email'],$w[212]);?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,131072,str_replace('0','1',$i['password']))) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[54]?>
</td><td><a href="<?=filename();?>?a=s&step=<?=C_APAGE;?>&id=&horo=0&agef=0&aget=255&gender=0&purpose=0&country=0&regin=0&sortby=0&do=um&userstat=0&password=<?=$i['password'];?>&" target="_blank">PASS [<?=$i['passcount'];?>]</a></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW2,262144,str_replace('0','1',$i['ip']))) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[72]?>
</td><td><a href="<?=filename();?>?a=s&step=<?=C_APAGE;?>&id=&horo=0&agef=0&aget=255&gender=0&purpose=0&country=0&regin=0&sortby=0&do=um&userstat=0&ip=<?=int2ip($i['ip']);?>" target="_blank"><?=int2ip($i['ip']);?> [<?=$i['ipcount'];?>]</a></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>Fingerprint
</td><td><a href="<?=filename();?>?a=s&step=<?=C_APAGE;?>&id=&horo=0&agef=0&aget=255&gender=0&purpose=0&country=0&regin=0&sortby=0&do=um&userstat=0&fingerprint=<?=$i['fingerprint']?>" target="_blank">FPID [<?=$i['fpcount'];?>]</a></td></tr>
<?php }?>

<?php if(ShowForEdit(C_ADMINVIEW1,16,$i['birthday'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[210]?>
</td><td><?=mysql2data($i['birthday']);?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,16,$i['birthday'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[120]?>
</td><td><?=abs(mysql2data($i['birthday'],1));?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,16,$i['birthday'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[129]?>
</td><td><?=$whr[$i['horo']];?></td></tr>
<?php }?>

<?php if(ShowForEdit(C_ADMINVIEW2,4096,$i['agef'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[83]?>
</td><td><?=$i['agef'];?> - <?=$i['aget'];?></td></tr>
<?php }?>

<?php if(ShowForEdit(C_ADMINVIEW1,32,$i['gender'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[132]?>
</td><td><?=$wg[$i['gender']]?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW2,1073741824,$i['bodytype'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[525]?>
</td><td><?=$wbt[$i['bodytype']]?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW2,536870912,$i['orientation'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[524]?>
</td><td><?=$wor[$i['orientation']]?></td></tr>
<?php }?>

<?php if(ShowForEdit(C_ADMINVIEW1,64,$i['purposes'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[133]?>
</td><td><?=arr2val($wp, $i['purposes'])?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,128,$i['country'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[121]?>
</td><td><?=$wcr[$i['country']];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW3,2,$i['state'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[527]?>
</td><td><?=$i['state'];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,512,$i['url'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[144]?>
</td><td><a href="<?=checkurl($i['url']);?>" target="_blank"><?=checkurl($i['url']);?></a></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,1024,$i['icq'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[145]?>
</td><td><?=$i['icq'];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,2048,$i['aim'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[146]?>
</td><td><?=$i['aim'];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,4096,$i['phone'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[147]?>
</td><td><?=$i['phone'];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,8192,$i['zip'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[285]?>
</td><td><?=$i['zip'];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,16384,$i['city'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[122]?>
</td><td><?=$i['city'];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,32768,$i['address'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[378]?>
</td><td><?=$i['address'];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,65536,$i['marstat'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[134]?>
</td><td><?=$wm[$i['marstat']];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,131072,$i['child'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[135]?>
</td><td><?=$wc[$i['child']];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,262144,$i['height'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[130]?>
</td><td><?=$wh[$i['height']];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,524288,$i['weight'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[131]?>
</td><td><?=$ww[$i['weight']];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,1048576,$i['hcolor'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[136]?>
</td><td><?=$whc[$i['hcolor']];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,2097152,$i['ecolor'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[137]?>
</td><td><?=$we[$i['ecolor']];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,4194304,$i['etnicity'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[138]?>
</td><td><?=$wet[$i['etnicity']];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,8388608,$i['religion'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[139]?>
</td><td><?=arr2val($wr,$i['religion']);?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,16777216,$i['smoke'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[140]?>
</td><td><?=$ws[$i['smoke']];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,33554432,$i['drink'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[141]?>
</td><td><?=$wd[$i['drink']];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,67108864,$i['education'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[142]?>
</td><td><?=$wed[$i['education']];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,134217728,$i['languages'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[142]?>
</td><td><?=arr2val($wlng,$i['languages'])?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW3,1,$i['interests'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[526]?>
</td><td><?=arr2val($win,$i['interests'])?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,268435456,$i['mylang'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[459]?>
</td><td><?=(isset($reallangname[$i['mylang']])) ? $reallangname[$i['mylang']] : 'Default';?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,536870912,$i['job'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[213]?>
</td><td><?=$i['job'];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW1,1073741824,$i['hobby'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[214]?>
</td><td><?=$i['hobby'];?></td></tr>
<?php }?>

<?php if(ShowForEdit(C_ADMINVIEW2,2,$i['sgender'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[80]?>
</td><td><?=arr2val($wg,$i['sgender']);?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW2,4,$i['scountry'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[444]?>
</td><td><?=arr2val($wcr,$i['scountry']);?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW2,8,$i['smarstat'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[445]?>
</td><td><?=arr2val($wm,$i['smarstat']);?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW2,16,$i['schild'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[446]?>
</td><td><?=arr2val($wc,$i['schild']);?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW2,32,$i['shcolor'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[447]?>
</td><td><?=arr2val($whc,$i['shcolor']);?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW2,64,$i['secolor'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[448]?>
</td><td><?=arr2val($we,$i['secolor']);?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW2,128,$i['ssmoke'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[449]?>
</td><td><?=arr2val($ws,$i['ssmoke']);?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW2,256,$i['sdrink'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[450]?>
</td><td><?=arr2val($wd,$i['sdrink']);?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW2,512,$i['seducation'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[451]?>
</td><td><?=arr2val($wed,$i['seducation']);?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW2,1024,$i['setnicity'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[81]?>
</td><td><?=arr2val($wet,$i['setnicity']);?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW2,2048,$i['sreligion'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[82]?>
</td><td><?=arr2val($wr,$i['sreligion']);?></td></tr>
<?php }?>

<?php if(ShowForEdit(C_ADMINVIEW2,8192,$i['heightf'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[84]?>
</td><td><?=$wh[$i['heightf']];?> - <?=$wh[$i['heightt']];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW2,16384,$i['weightf'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[85]?>
</td><td><?=$ww[$i['weightf']];?> - <?=$ww[$i['weightt']];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW2,65536,$i['audio'])) {?>
<?php
if(!empty($i['audio'])) {
	$autostart = (C_AUDIO_AUTOSTART) ? 'true' : 'false';
	define('AUDIO_VALUE',id2pathAudio($i['id']).'.'.$i['audio']);
	define('AUTOSTART',$autostart);
?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[296]?>
</td><Td>
      <?php if(IsAV(AUDIO_VALUE)) {?>
      <script language="JavaScript">
      <!--
      var soundFile = "<?=C_URL?>/members/uploads/<?=AUDIO_VALUE?>";
      if(navigator.plugins.length>0){
      	document.write('<embed src="'+soundFile+'" autostart="<?=AUTOSTART?>" loop="<?=C_AUDIO_LOOP?>" <?=audio_player(C_AUDIO_PLAYER,'0');?>></embed>');}
      	else {
      		document.write('<embed src="'+soundFile+'" autostart="<?=AUTOSTART?>" loop="<?=C_AUDIO_LOOP?>" <?=audio_player(C_AUDIO_PLAYER,'1');?>></embed>');}
      		//-->
      </script>
      <noscript><embed src="<?=C_URL?>/members/uploads/<?=AUDIO_VALUE?>" autostart="<?=AUTOSTART?>" loop="<?=C_AUDIO_LOOP?>"></embed></noscript>
      <noembed><bgsound src="<?=C_URL?>/members/uploads/<?=AUDIO_VALUE?>" loop="<?=C_AUDIO_LOOP?>"></noembed>
      <?php  } else { ?>
      <OBJECT id='mediaPlayer' width="320" height="285" 
      classid='CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95' 
      codebase='http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701'
      standby='Loading Microsoft Windows Media Player components...' type='application/x-oleobject'>
      <param name='fileName' value="<?=C_URL?>/members/uploads/<?=AUDIO_VALUE?>">
      <param name='animationatStart' value='true'>
      <param name='transparentatStart' value='true'>
      <param name='autoStart' value="<?=AUTOSTART?>">
      <param name='showControls' value="true">
      <param name='loop' value="<?=C_AUDIO_LOOP?>">
      <EMBED type='application/x-mplayer2'
        pluginspage='http://microsoft.com/windows/mediaplayer/en/download/'
        id='mediaPlayer' name='mediaPlayer' displaysize='4' autosize='-1' 
        bgcolor='darkblue' showcontrols="true" showtracker='-1' 
        showdisplay='0' showstatusbar='-1' videoborder3d='-1' width="320" height="285"
        src="<?=C_URL?>/members/uploads/<?=AUDIO_VALUE?>" autostart="<?=AUTOSTART?>" designtimesp='5311' loop="<?=C_AUDIO_LOOP?>">
      </EMBED>
      </OBJECT>
			<?php }?>
</td></tr>
<?php }?>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW2,524288,str_replace('0','1',$i['req']))) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[91]?>
</td><td><?=$wpu[$i['req']];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW2,1048576,str_replace('0','1',$i['status']))) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><a href="<?="javascript:open_win('".filename()."?ds=1&do=sh&".s()."','help');"?>" class=desc><?=$x[90]?></a>
</td><td><?=$i['status'];?>

<?php
if($i['status'] <= '5' || ($i['status'] >= '6' && $i['status'] <= '10'))
{
	echo '(';
	switch($i['status'])
	{
		case '0': 
			$tmpl=db_query("SELECT * FROM ".C_MYSQL_TEMP." WHERE id='".$id."'");
			while ($kl = db_fetch_array($tmpl))
			{
				$tempid = $kl['id'];
				$tempcode = $kl['code'];
			}
			echo '<a href="'.C_URL.'/check.php?l=hu&id='.$tempid.'&code='.$tempcode.'">'.$wst[1].'</a>'; 
			break;
		case '1':case '10': echo $wst[2]; break;
		case '2':case '9': echo $wst[3]; break;
		case '3':case '8': echo $wst[4]; break;
		case '4': echo $w[764]; break;
		case '5': echo $wst[5]; break;
		default: echo $wst[6];  break;
	}
	echo ')';
}
?>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW2,2097152,str_replace('0','1',$i['hits']))) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[216]?>
</td><td><?=pop_out($i['id']);?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW3,8,str_replace('0','1',$i['hide']))) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[745]?>
</td><td><?=($i['hide']) ? $x[136] : $x[137];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW2,8388608,str_replace('0','1',$i['prv']))) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[365]?>
</td><td><?=$i['prv'];?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW3,16,$i['link_txt'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[806]?>
</td><td><a href="<?=C_URL?>/<?=ADM_DIR?>/index.php?l=<?=LANGUAGE?>&do=vp&id=<?=$i['link'];?>" target="_blank"><?=$i['link_txt'];?></a></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW2,16777216,$i['regdate'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[124]?>
</td><td><?=mysql2data($i['regdate'],0,1);?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW2,33554432,$i['editdate'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[123]?>
</td><td><?=mysql2data($i['editdate'],0,1);?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINVIEW2,67108864,$i['hdyfu'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[86]?>
</td><td><?=$wu[$i['hdyfu']];?></td></tr>
<?php }?>
</td></tr></table></td></tr></table></td></tr></table>
<?php  }} 
break;
############################################
# Admin View Profile - End
############################################
############################################
# Profile update
############################################
		case 'uu':
			$tmp = db_query("SELECT t1.*, t2.*, t3.* FROM ".C_MYSQL_MEMBERS_MAIN." as t1,".C_MYSQL_MEMBERS_EXT." as t2,".C_MYSQL_MEMBERS_SEC." as t3 WHERE t1.id='".$id."' AND t1.id = t2.id AND t1.id = t3.id");
			while ($i = db_fetch_array($tmp))
			{

				function ReturnRegisterValues($val,$global,$type=null,$fld_acs=null,$arr=null,$interval=null,$req=null,$err=null,$return_max=null)
				{
					Global $$val,$errors,$w;
					$bool =  1;
					if($type == 'num')
					{
						if($arr == null)
						{
							if(isset($$val) && is_numeric($$val) && $bool)
							{
								Return $$val;
							}
							elseif($bool && $req)
							{
								sprintm($err);
							}
							else Return ($return_max == null) ? 0 : sizeof($arr) - 1;
						}
						else
						{
							$count = (is_array($arr)) ? sizeof($arr) - 1 : $arr;
							$int_from = ($interval == null) ? 0 : $interval[0];
							$int_to = ($interval == null) ? $count : $interval[1];
							if(isset($$val) && is_numeric($$val) && $bool && ($$val >= $int_from) && ($$val <= $int_to))
							{
								Return $$val;
							}
							elseif($bool && $req)
							{
								sprintm($err);
							}
							else Return ($return_max == null) ? $int_from : $int_to;
						}
					}
					else if($type == 'text')// Text value
					{
						if(isset($$val) && $bool)
						{
							Return cb($$val);
						}
						elseif($bool && $req)
						{
							sprintm($err);
						}
						else Return '';
					}
					else if($type == 'check')// checking only
					{
						Return (isset($$val) && $bool) ? True : False;
					}
					else if($type == 'multinum')
					{
						if(isset($$val) && $bool)
						{
							$tmp = implode(',',$$val);
							if(!preg_match("/^[0-9]|,$/", $tmp)) $tmp = '';
							Return $tmp;
						}
						elseif($bool && $req)
						{
							sprintm($err);
						}
						else Return '';
					}
					else if($type == 'list')// Text value
					{
						if(isset(${$val}) && $bool)
						{
							$p=0;$lst='';
							while (isset(${$val}[$p]))
							{
								if((${$val}[$p] < sizeof($arr)) && (${$val}[$p] >= 0))
								{
									$lst .= ${$val}[$p].',';
								}
								$p++;
							}
							Return substr($lst, 0, -1);
						}
						elseif($bool && $req)
						{
							sprintm($err);
						}
						else Return '';
					}
				}

				if(isset($fullmode))	define('FULLMODE','1');
				else 	define('FULLMODE','0');

				
				# Using also for update status of adressbook!
				$mstatus = (isset($status)) ? "status = '".$status."'" : "status = '".$i['status']."'";
				
				if(isset($ip)) $ip=cb($ip);
				if(isset($username)) $username=cb($username);
				if(isset($fname)) $fname=cb($fname);
				if(isset($lname)) $lname=cb($lname);
				if(isset($email)) $email=cb($email);
				if(isset($url)) $url=cb($url);
				if(isset($icq)) $icq=cb($icq);
				if(isset($aim)) $aim=cb($aim);
				if(isset($phone)) $phone=cb($phone);
				if(isset($city)) $city=cb($city);
				if(isset($job)) $job=cb($job);
				if(isset($hobby)) $hobby=cb($hobby);
				if(isset($descr)) $descr=cb($descr);
				if(isset($pass) && !empty($pass)) $pass=cb($pass);
				if(isset($zip)) $zip=cb($zip);
				if(isset($link_txt)) $link_txt=cb($link_txt);
				if(isset($address)) $address=cb($address);
				$langlist="";
				$mylang=(isset($mylang)) ? cb($mylang) : '';
				if(strtolower($mylang) == 'default') $mylang='';


				if(isset($lang))
				{
					$p=0;
					while (isset($lang[$p]))
					{
						if(($lang[$p] >= sizeof($wlng)) || ($lang[$p] < 0)) sprintm($w[1].'2', 1);
						$langlist.=$lang[$p].',';
						$p++;
					}
					$langlist=substr($langlist, 0, -1);
					$mlanguages="languages='".$langlist."',";
				} else $mlanguages="languages='',";

				if(isset($username)) {
					$username=ReturnRegisterValues('username',1,'text',0);
					$musername="username='".$username."',";
				} else $musername='';

				if(isset($fname)) {
					$fname=ReturnRegisterValues('fname',1,'text',0);
					if (((C_FIRSTNR)||(!empty($fname)))&&((strlen($fname) > C_FIRSTNB)||(strlen($fname) < C_FIRSTNS))) {
						$tm=array(C_FIRSTNS,C_FIRSTNB);
						sprintm(template($w[3],$tm));
					} else $mfname="fname='".$fname."',";
				} else $mfname='';

				if(isset($lname)) {
					$lname=ReturnRegisterValues('lname',1,'text',1);
					if (((C_LASTNR)||(!empty($lname)))&&((strlen($lname) > C_LASTNB)||(strlen($lname) < C_LASTNS))) {
						$tm=array(C_LASTNS,C_LASTNB);
						sprintm(template($w[4],$tm));
					} else $mlname="lname='".$lname."',";
				} else $mlname='';
				// Final checks for most real values for profiles
				if(isset($month) || isset($day) || isset($year)) {
					if (((C_BIRTHR)||(!empty($month))||(!empty($day))||(!empty($year)))&&(($month == "0")||($day == "0")||($year == "0"))) sprintm($w[5]);
					else {$mbirthday="birthday='".$year."-".$month."-".$day."',";$mhoro="horo='".horo($month, $day)."',";}
				} else {$mbirthday="";$mhoro="";}
				if(isset($pass) && !empty($pass)) {
					if ((strlen($pass) > C_PASSB)||(strlen($pass) < C_PASSS)) {
						$tm=array(C_PASSS,C_PASSB);
						sprintm(template($w[6],$tm));
					} else $mpass="password=SHA2('".$pass."', 256),";
				} else $mpass='';



				if(isset($gender)) {
					$gender=ReturnRegisterValues('gender',1,'num',0,$wg,0,C_GENDR,$w[7]);
					$mgender="gender='".$gender."',";
				} else $mgender='';
				if(isset($bodytype)) {
					$bodytype=ReturnRegisterValues('bodytype',1,'num',5,$wbt,0,C_BODYTYPER,$w[531].$w[525]);
					$mbodytype="bodytype='".$bodytype."',";
				} else $mbodytype='';
				if(isset($orientation)) {
					$orientation=ReturnRegisterValues('orientation',1,'num',4,$wor,0,C_ORIENTATIONR,$w[531].$w[524]);
					$morientation="orientation='".$orientation."',";
				} else $morientation='';
				if(isset($sgender) || ShowForEdit(C_ADMINEDIT2,2,1)) {
					$sgender=ReturnRegisterValues('sgender',2,'list',1,$wg,0,C_SGENDR,$w[8]);
					$msgender="sgender='".$sgender."',";
				} else $msgender='';
				if(isset($purposes) || ShowForEdit(C_ADMINEDIT1,64,1)) {
					$purposes=ReturnRegisterValues('purposes',1,'list',3,$wp,0,C_PURPR,$w[9]);
					$mpurposes="purposes='".$purposes."',";
				} else $mpurposes='';
				if(isset($country)) {
					$country=ReturnRegisterValues('country',1,'num',6,$wcr,0,C_CNTRR,$w[10]);
					$mcountry="country='".$country."',";
				} else $mcountry='';
				if(isset($scountry) || ShowForEdit(C_ADMINEDIT2,4,1)) {
					$scountry=ReturnRegisterValues('scountry',2,'list',2,$wcr,0,C_SCNTRR,$w[452].' '.$w[444]);
					$mscountry="scountry='".$scountry."',";
				} else $mscountry='';
				if(isset($email)) {
					if (c_email($email) == 0) sprintm($w[11]);
					else $memail="email='".$email."',";
				} else $memail='';
				if(isset($url)) {
					if (!empty($url))
					{
						$urll = checkurl($url);
						if (strlen($urll) < 3)
						{
							sprintm($w[12]);
						} else $murl="url='".$url."',";
					} else $murl='';
				} else $murl='';
				if(isset($icq)) {
					if ((!empty($icq)) && (!is_numeric($icq))) sprintm($w[13]);
					else $micq="icq='".$icq."',";
				} else $micq='';
				if(isset($aim)) {
					if ((!empty($aim)) && ((strlen($aim) < 3) || (strlen($aim) > 16))) sprintm($w[14]);
					else $maim="aim='".$aim."',";
				} else $maim='';
				if(isset($phone)) {
					if ((C_PHONER) && (empty($phone))) sprintm($w[15]);
					$mphone="phone='".$phone."',";
				} else $mphone='';
				if(isset($zip)) {
					$zip=ReturnRegisterValues('zip',1,'text',12,0,0,C_ZIPR,$w[288]);
					$mzip="zip='".$zip."',";
				} else $mzip='';
				if(isset($city)) {
					$city=ReturnRegisterValues('city',1,'text',13,0,0,C_CITYR,$w[16]);
					$mcity="city='".$city."',";
				} else $mcity='';
				if(isset($state)) {
					$state=ReturnRegisterValues('state',1,'text',11,0,0,C_STATER,$w[531].$w[527]);
					$mstate="state='".$state."',";
				} else $mstate='';
				if(isset($address)) {
					$address=ReturnRegisterValues('address',1,'text',14,0,0,C_ADDRESSR,$w[377]);
					$maddress="address='".$address."',";
				} else $maddress='';
				if(isset($marstat)) {
					$marstat=ReturnRegisterValues('marstat',1,'num',15,$wm,0,C_MARSR,$w[17]);
					$mmarstat="marstat='".$marstat."',";
				} else $mmarstat='';
				if(isset($child)) {
					$child=ReturnRegisterValues('child',1,'num',16,$wc,0,C_CHILDR,$w[18]);
					$mchild="child='".$child."',";
				} else $mchild='';
				if(isset($smarstat) || ShowForEdit(C_ADMINEDIT2,8,1)) {
					$smarstat=ReturnRegisterValues('smarstat',2,'list',3,$wm,0,C_SMARSR,$w[452].' '.$w[445]);
					$msmarstat="smarstat='".$smarstat."',";
				} else $msmarstat='';
				if(isset($schild) || ShowForEdit(C_ADMINEDIT2,16,1)) {
					$schild=ReturnRegisterValues('schild',2,'list',4,$wc,0,C_SCHILDR,$w[452].' '.$w[446]);
					$mschild="schild='".$schild."',";
				} else $mschild='';
				if(isset($height)) {
					$height=ReturnRegisterValues('height',1,'num',17,$wh,0,C_HGHTR,$w[19]);
					$mheight="height='".$height."',";
				} else $mheight='';
				if(isset($weight)) {
					$weight=ReturnRegisterValues('weight',1,'num',18,$ww,0,C_WGHTR,$w[20]);
					$mweight="weight='".$weight."',";
				} else $mweight='';
				if(isset($heightf)) {
					$heightf=ReturnRegisterValues('heightf',2,'num',13,$wh);
					$mheightf="heightf='".$heightf."',";
				} else $mheightf='';
				if(isset($heightt)) {
					$heightt=ReturnRegisterValues('heightt',2,'num',13,$wh,0,0,0,1);
					$mheightt="heightt='".$heightt."',";
				} else $mheightt='';
				if(isset($weightf)) {
					$weightf=ReturnRegisterValues('weightf',2,'num',14,$ww);
					$mweightf="weightf='".$weightf."',";
				} else $mweightf='';
				if(isset($weightt)) {
					$weightt=ReturnRegisterValues('weightt',2,'num',14,$ww,0,0,0,1);
					$mweightt="weightt='".$weightt."',";
				} else $mweightt='';
				if(isset($hcolor)) {
					$hcolor=ReturnRegisterValues('hcolor',1,'num',19,$whc,0,C_HAIRR,$w[23]);
					$mhcolor="hcolor='".$hcolor."',";
				} else $mhcolor='';
				if(isset($ecolor)) {
					$ecolor=ReturnRegisterValues('ecolor',1,'num',20,$we,0,C_EYER,$w[24]);
					$mecolor="ecolor='".$ecolor."',";
				} else $mecolor='';
				if(isset($shcolor) || ShowForEdit(C_ADMINEDIT2,32,1)) {
					$shcolor=ReturnRegisterValues('shcolor',2,'list',5,$whc,0,C_SHAIRR,$w[452].' '.$w[447]);
					$mshcolor="shcolor='".$shcolor."',";
				} else $mshcolor='';
				if(isset($secolor) || ShowForEdit(C_ADMINEDIT2,64,1)) {
					$secolor=ReturnRegisterValues('secolor',2,'list',6,$we,0,C_SEYER,$w[452].' '.$w[448]);
					$msecolor="secolor='".$secolor."',";
				} else $msecolor='';
				if(isset($etnicity)) {
					$etnicity=ReturnRegisterValues('etnicity',1,'num',21,$wet,0,C_ETNR,$w[25]);
					$metnicity="etnicity='".$etnicity."',";
				} else $metnicity='';
				if(isset($religion)) {
					$religion=ReturnRegisterValues('religion',1,'list',22,$wr,0,C_RELR,$w[26]);
					$mreligion="religion='".$religion."',";
				} else $mreligion='';
				if(isset($setnicity) || ShowForEdit(C_ADMINEDIT2,1024,1)) {
					$setnicity=ReturnRegisterValues('setnicity',2,'list',10,$wet,0,C_SETNR,$w[27]);
					$msetnicity="setnicity='".$setnicity."',";
				} else $msetnicity='';
				if(isset($sreligion) || ShowForEdit(C_ADMINEDIT2,2048,1)) {
					$sreligion=ReturnRegisterValues('sreligion',2,'list',11,$wr,0,C_SRELR,$w[28]);
					$msreligion="sreligion='".$sreligion."',";
				} else $msreligion='';
				if(isset($interests) || ShowForEdit(C_ADMINEDIT3,1,1)) {
					$interests=ReturnRegisterValues('interests',1,'list',30,$win,0,C_INTERESTSR,$w[531].$w[526]);
					$minterests="interests='".$interests."',";
				} else $minterests='';
				if(isset($smoke)) {
					$smoke=ReturnRegisterValues('smoke',1,'num',23,$ws,0,C_SMOKER,$w[29]);
					$msmoke="smoke='".$smoke."',";
				} else $msmoke='';
				if(isset($drink)) {
					$drink=ReturnRegisterValues('drink',1,'num',24,$wd,0,C_DRINKR,$w[30]);
					$mdrink="drink='".$drink."',";
				} else $mdrink='';
				if(isset($education)) {
					$education=ReturnRegisterValues('education',1,'num',25,$wed,0,C_EDUCR,$w[31]);
					$meducation="education='".$education."',";
				} else $meducation='';
				if(isset($ssmoke) || ShowForEdit(C_ADMINEDIT2,128,1)) {
					$ssmoke=ReturnRegisterValues('ssmoke',2,'list',7,$ws,0,C_SSMOKER,$w[452].' '.$w[449]);
					$mssmoke="ssmoke='".$ssmoke."',";
				} else $mssmoke='';
				if(isset($sdrink) || ShowForEdit(C_ADMINEDIT2,256,1)) {
					$sdrink=ReturnRegisterValues('sdrink',2,'list',8,$wd,0,C_SDRINKR,$w[452].' '.$w[450]);
					$msdrink="sdrink='".$sdrink."',";
				} else $msdrink='';
				if(isset($seducation) || ShowForEdit(C_ADMINEDIT2,512,1)) {
					$seducation=ReturnRegisterValues('seducation',2,'list',9,$wed,0,C_SEDUCR,$w[452].' '.$w[451]);
					$mseducation="seducation='".$seducation."',";
				} else $mseducation='';
				if(isset($job)) {
					$job=ReturnRegisterValues('job',1,'text',28,1,0,C_JOBR,$w[32]);
					$mjob="job='".$job."',";
				} else $mjob='';
				if(isset($agef)) {
					if ((C_SAGER) && ((empty($agef) || empty($aget)))) sprintm($w[33]);
					else {$magef="agef='".$agef."',";$maget="aget='".$aget."',";}
				} else {$magef='';$maget='';}
				if(isset($hdyfu)) {
					if ((C_HDYFUR) && ($hdyfu == "0")) sprintm($w[34]);
					else $mhdyfu="hdyfu='".$hdyfu."',";
				} else $mhdyfu='';


				if(isset($hobby)) {
					if (C_HOBBR) {
						if (empty($hobby) || trim($hobby) == "") sprintm($w[35]);
						if (strlen($hobby) > C_HOBBB) {
							$tm=array(C_HOBBB);
							sprintm(template($w[36],$tm));
						}
						$es = explode(" ",$hobby);
						for ($as = 0; $as < sizeof($es); $as++){
							$os = strlen($es[$as]);
							if ($os > C_HOBBW) {
								$tm=array(C_HOBBW);
								sprintm(template($w[37],$tm));
							}
						}
					}
					$mhobby="hobby='".$hobby."',";
				} else $mhobby='';
				if(isset($descr)) {
					if (empty($descr) || trim($descr) == "") sprintm($w[38]);
					if (strlen($descr) > C_DESCB) {
						$tm=array(C_DESCB);
						sprintm(template($w[39],$tm));
					}
					$es = explode(" ",$descr);
					for ($as = 0; $as < sizeof($es); $as++){$os = strlen($es[$as]);
					if ($os > C_DESCW) {
						$tm=array(C_DESCW);
						sprintm(template($w[40],$tm));
					}
					}
					$mdescr="descr='".$descr."',";
				} else $mdescr='';
				///////////


				$raudio = 0;
				$cnt = $rem_photos = '0';
				$PicUpl = False;
				$AudioUpl = False;
				$photonum=$photonum_orig='';

				if(C_USE_GALLERY) {
					$rslt = db_query("SELECT * FROM ".C_MYSQL_NEW_PHOTOS." WHERE uid = '".$i['id']."' limit 1 ") or die(db_error());
					while ($j = db_fetch_array($rslt))
					{
						$photonum=$photonum_orig=$j['photonum'];
					}
				}

				////////////////////////////
				include_once C_PATH.'/classes/upload.class.php';
				$time = time();
				for($p = 0; $p < $wpn[$i['req']]; $p++)
				{
					$file = 'file'.$p;
					if (!empty($_FILES[$file]['name']))
					{
						if(C_DECREASE_IMAGES)
						{
							$upl = new Upload($file, C_MAXSZ, C_MAXWD, C_MAXHG, C_PATH.'/members/uploads/'.$i['id'].'-'.$p);
						}
						else
						{
							$upl = new Upload($file, C_MAXSZ, C_MAXWD, C_MAXHG, C_PATH.'/members/uploads/'.PicPath($p,$i['id']));
						}
						if (!$upl->do_upload())
						{
							if(C_DECREASE_IMAGES)
							unlink (C_PATH.'/members/uploads/'.$i['id'].'-'.$p);
							else
							{
								unlink (C_PATH.'/members/uploads/'.PicPath($p,$i['id']));
								unlink (C_PATH.'/members/uploads/'.TPicPath($p,$i['id']));
							}
							printm($upl->getErrors());
						}
						else
						{
							if(C_DECREASE_IMAGES)
							{
								include_once '../classes/thumbnail.class.php';
								$thumb=new Thumbnail(C_PATH.'/members/uploads/'.$i['id'].'-'.$p,True,True);
								if(C_PICTURE_WIDTH) $thumb->Width(C_PICTURE_WIDTH);
								if(C_PICTURE_HEIGHT) $thumb->Height(C_PICTURE_HEIGHT);
								if(C_PICTURE_AUTO) $thumb->Auto(C_PICTURE_AUTO);
								$thumb->Quality(C_PICTURE_QUALITY);
								$thumb->Save(C_PATH.'/members/uploads/'.PicPath($p,$i['id']),True);
								unlink (C_PATH.'/members/uploads/'.$i['id'].'-'.$p);
							}
							unlink (C_PATH.'/members/uploads/'.TPicPath($p,$i['id']));
							$i['files'] = AddPicElement($i['files'],$p);
							$PicUpl=True;
							$rem_photos++;
						}
					}
				}
				$arr=explode(',',$i['files']);
				reset($arr);rsort($arr);
				$max = (($i['files'] != "") && ($arr[0]+1) > $wpn[$i['req']]) ? ($arr[0]+1) : $wpn[$i['req']];
				for($k = 0; $k <= $max; $k++)
				{
					$tmp = 'movepp'.$k;
					if (isset($$tmp) && ($$tmp == 'on'))
					{
						$i['files'] = DelPicElement($i['files'],$k);
						if(C_USE_GALLERY && $photonum != '') $photonum = DelPicElement($photonum,$k);
						unlink (C_PATH.'/members/uploads/'.TPicPath($k,$i['id']));

						@db_query("INSERT INTO ".C_MYSQL_PRIVATE_PHOTOS." VALUES (Null, '".$i['id']."','".intval($type)."','".intval($access)."','','',NOW(),'7')") or die(db_error());
						$last_id = db_fetch_array(db_query("SELECT LAST_INSERT_ID() AS last_id"));
						$last_id = $last_id['last_id'];
						$newpath = id2path($i['id'],'p',$last_id);
						rename (C_PATH.'/members/uploads/'.PicPath($k,$i['id']), C_PATH.'/members/uploads/'.$newpath.'0');
					}
					else
					{
						$tmp = 'delpic'.$k;
						if (isset($$tmp) && ($$tmp == 'on'))
						{
							$i['files'] = DelPicElement($i['files'],$k);
							if(C_USE_GALLERY && $photonum != '') $photonum = DelPicElement($photonum,$k);
							unlink (C_PATH.'/members/uploads/'.PicPath($k,$i['id']));
							unlink (C_PATH.'/members/uploads/'.TPicPath($k,$i['id']));
						}
					}
				}
				if(isset($delaudio) && $delaudio == 'on')
				{
					unlink (C_PATH.'/members/uploads/'.id2pathAudio($i['id']).'.'.$i['audio']);
					$i['audio'] = '';
				}



				if (C_AUDIO_ALLOW)
				{
					$file = 'filea';
					if (!empty($_FILES[$file]['name']))
					{
						$upl = new Upload($file, C_AUDIO_SIZE, C_MAXWD, C_MAXHG, C_PATH.'/members/uploads/'.id2pathAudio($i['id']));
						if (!$upl->do_upload(C_AUDIO_ALLOW))
						{
							unlink (C_PATH.'/members/uploads/'.id2pathAudio($i['id']).'.'.$upl->getType());
							printm($upl->getErrors());
						}
						else
						{
							$i['audio'] = $upl->getType();
							$AudioUpl = True;
						}
					}
				}
				else $i['audio'] = '';
				/////////////
				$last_id = $id;
				$mhdyfu = (isset($hdyfu)) ? "hdyfu = '".$hdyfu."'," : "";
				$mlink_txt = (isset($link_txt)) ? "link_txt = '".$link_txt."', link = '".intval($link)."'," : "";
				$mreq = (isset($req)) ? "req = '".$req."'," : "";
				$mhits = (isset($hits)) ? "hits = '".$hits."'," : "";
				$mip = (isset($ip)) ? "ip = '".ip2int($ip)."'," : "";
				$mprv = (isset($prv)) ? "prv = '".$prv."'," : "";
				$mhits_count = (isset($hits_count)) ? "hits_count = '".$hits_count."'," : "";
				$mfiles = "files='".$i['files']."',";
				$maudio = "audio='".$i['audio']."',";
				$mmylang = (isset($mylang)) ? "mylang = '".$mylang."'," : "";
				if(ShowForEdit(C_ADMINEDIT2,134217728,1)) $ir_photo=(isset($ir_photo) && $ir_photo == 'on') ? 1 : 0;
				else $ir_photo = -1;
				if(ShowForEdit(C_ADMINEDIT2,268435456,1)) $ir_audio=(isset($ir_audio) && $ir_audio == 'on') ? 1 : 0;
				else $ir_audio = -1;
				if(ShowForEdit(C_ADMINEDIT3,8,1)) $mhide=(isset($hide) && $hide == 'on') ? "hide = '1'," : "hide = '0',";
				else $mhide = "";

				if($ir_photo && $ir_audio)
				{
					@db_query("DELETE FROM ".C_MYSQL_MEMBERS_RESTRICTS." WHERE id='".$id."'");
				}
				else if($ir_photo >= 0 && $ir_audio >= 0)
				{
					db_query("REPLACE INTO ".C_MYSQL_MEMBERS_RESTRICTS." SET id = '".$id."',photo = '".$ir_photo."', audio = '".$ir_audio."'") or die(db_error());
				}
				else if($ir_photo >= 0 || $ir_audio >= 0)
				{
					$tmpr=db_query("SELECT * FROM ".C_MYSQL_MEMBERS_RESTRICTS." WHERE id='".$id."'");
					$countr = db_num_rows($tmpr);
					if($countr) {
						if($ir_photo >= 0) db_query("UPDATE ".C_MYSQL_MEMBERS_RESTRICTS." SET photo = '".$ir_photo."' WHERE id = '".$id."'") or die(db_error());
						else db_query("UPDATE ".C_MYSQL_MEMBERS_RESTRICTS." SET audio = '".$ir_audio."' WHERE id = '".$id."'") or die(db_error());
					}
					else
					{
						if($ir_photo >= 0) db_query("INSERT INTO ".C_MYSQL_MEMBERS_RESTRICTS." VALUES ('".$id."','".$ir_photo."','1')") or die(db_error());
						else db_query("INSERT INTO ".C_MYSQL_MEMBERS_RESTRICTS." VALUES ('".$id."','1','".$ir_audio."')") or die(db_error());
					}
				}

				$str1=$musername.$mfname.$mlname.$mbirthday.$mgender.$morientation.$mbodytype.$mpurposes.$mcountry.$mcity.$mzip.$mheight.$mweight.$msgender.$mhoro.$mreq.$mhits.$mhits_count.$mfiles.$mhide.$mstatus;
				$str2=$murl.$micq.$maim.$mphone.$mstate.$maddress.$mmarstat.$mchild.$mhcolor.$mecolor.$metnicity.$mreligion.$msmoke.$mdrink.$meducation.$mscountry.$msmarstat.$mschild.$mshcolor.$msecolor.$mssmoke.$msdrink.$mseducation.$mlanguages.$minterests.$mmylang.$mjob.$mhobby.$mdescr.$msetnicity.$msreligion.$magef.$maget.$mheightf.$mheightt.$mweightf.$mweightt.$mhdyfu.$maudio.$mstatus;
				$str3=$memail.$mpass.$mprv.$mlink_txt.$mip.$mstatus;
				db_query("UPDATE ".C_MYSQL_MEMBERS_MAIN." SET ".$str1." WHERE id='".$i['id']."'") or die(db_error());
				#				echo "UPDATE ".C_MYSQL_MEMBERS_MAIN." SET ".$str1." WHERE id='".$i['id']."'"."<br>";
				db_query("UPDATE ".C_MYSQL_MEMBERS_EXT." SET ".$str2." WHERE id='".$i['id']."'") or die(db_error());
				#				echo "UPDATE ".C_MYSQL_MEMBERS_EXT." SET ".$str2." WHERE id='".$i['id']."'"."<br>";
				db_query("UPDATE ".C_MYSQL_MEMBERS_SEC." SET ".$str3." WHERE id='".$i['id']."'") or die(db_error());
				#				echo "UPDATE ".C_MYSQL_MEMBERS_SEC." SET ".$str3." WHERE id='".$i['id']."'";
				@db_query("UPDATE ".C_MYSQL_ADDRESSBOOK." SET ".$mstatus." WHERE myid ='".$i['id']."'");

				if(C_USE_GALLERY) { // If we`re using the Gallery for admin, we add new photos for checking
					if(($photonum !== $photonum_orig) && (trim($photonum) != '')) {
						db_query("REPLACE INTO ".C_MYSQL_NEW_PHOTOS." VALUES ('".$i['id']."', '".$photonum."')") or die(db_error());
					}
					else if(trim($photonum) === '')
					{
						db_query("DELETE FROM ".C_MYSQL_NEW_PHOTOS." WHERE uid = ".$i['id']) or die(db_error());
					}
				}

				sprintm($x[41]);

			}
			break;
			############################################
			# Profile update - End
			############################################
			############################################
			# View messages from users - Begin
			############################################
		case 'vmess':
			if(isset($sendmess) && isset($id) && is_numeric($id))
			{
				if (empty($subject) || empty($message)) sprintm($w[164]);
				if (!isset($id) || !is_numeric($id)) sprintm($w[1].'1', 1);

				$confirm = ($confirm == "on") ? "2" : "1";
				$subject = cb($subject);
				$message = cb($message);

				$subject=cb(trim($subject));$message=cb(trim($message));

				db_query("INSERT INTO ".C_MYSQL_MESSAGES." VALUES (Null,'0','".$id."','".$subject."',NOW(),'".ip2int(ip())."','".$confirm."', '0')");
				
				$lastid = db_insert_id();
				save_message($lastid, $message);

				AddUserToAddressBook($id,1);
				sprintm($w[165], 2);
			}
			if(isset($mid) && is_numeric($mid))
			{
				$id = 0;

				if(isset($next)&&($mid==0) ) {
					$tmp = db_query("SELECT id, toid, fromid, subject, ".CorrectDate('sendtime').", readed, deleted FROM ".C_MYSQL_MESSAGES." WHERE toid='".$id."' AND readed != '0' limit 1");
					$count = db_num_rows($tmp);
					if ($count == "0") sprintm($w[409], 1);

				}
				else {
					$tmp = db_query("SELECT id, toid, fromid, subject, ".CorrectDate('sendtime').", readed, deleted FROM ".C_MYSQL_MESSAGES." WHERE id='".$mid."' AND toid='".$id."'");
				}
				while ($i = db_fetch_array($tmp))
				{
					define('FROMID', $i['fromid']);
					$name = '';
					$mid=$i['id'];
					define('SENDTIME', $i['sendtime']);
					if(!empty($i['subject']))
					define('SUBJECT_VALUE', $i['subject']);
					else
					define('SUBJECT_VALUE', $w[349]);
					define('MESSAGE_ID', $i['id']);
					include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';
					$message = read_message($i['id']);
					if($message) $i['message'] = $message;
					else $i['message'] = '';
					define('MESSAGE_VALUE', AzDGCode($i['message']));
					define('ADMINISTRATOR', $w[519]);

					if($i['fromid']) {
						if (!C_NO_NAME)
						{
							$tmp2 = db_query("SELECT * FROM ".C_MYSQL_MEMBERS_MAIN." WHERE id='".$i['fromid']."' AND status >= '7'");
							while ($j = db_fetch_array($tmp2))
							{
								$name = username($j['id'], $j['username'], $j['fname'], $j['lname']);
								define('NAME', $name);
							}
						} else define('NAME', '');
					}
					else define('NAME', '');

					define('ENTER_SUBJECT', $w[166]);
					define('ENTER_MESSAGE', $w[167]);
					define('MESSAGE_FROM', $w[233]);
					define('DATE', $w[229]);
					define('SUBJECT', $w[168]);
					define('MESSAGE', $w[169]);
					define('DELETE_MESSAGE', $w[366]);
					define('READ_NEXT_UNREAD', $w[408]);
					define('SUBJECTR', $w[168].$w[0]);
					define('MESSAGER', $w[169].$w[0]);
					define('REPLY', $w[234]);

					if (C_REPLYS)
					{
						define('SUBJECT_ANSWER', $w[349].$i['subject']);
					}
					else define('SUBJECT_ANSWER', '');
					if (C_REPLYM)
					{
						$tm = array($i['sendtime'], tb($i['message']));
						define('MESSAGE_ANSWER', strip_tags(template($w[235], $tm)));
					}
					else define('MESSAGE_ANSWER', '');

					define('NOTIFY_ME', $w[203]);
					define('SEND_MESSAGE', $w[170]);

?>

   <TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="6">
    <TR> 
     <TD ALIGN="CENTER" VALIGN="TOP">
<br><br><br>



<script language="JavaScript">
<!--
function ins(prop,type)
{
	var input = document.forms[0].message;
	var mysel;var text;
	if (window.getSelection) mysel = window.getSelection();
	else if (document.selection) mysel = document.selection.createRange().text;
	else if (document.getSelection)	mysel = document.getSelection();
	b = (type == 0) ? '['+prop+']' : prop;
	e = (type == 0) ? '[/'+prop+']' : '';
	if (input.createTextRange)
	{
		var text;
		input.focus(input.caretPos);
		input.caretPos = document.selection.createRange().duplicate();
		input.caretPos.text = (input.caretPos.text.length>0) ? b + input.caretPos.text + e : b + mysel + e;
	}
	else input.value += b + mysel + e;
}
function formCheck(form) {
	if (form.subject.value == "") {
		alert("<?=ENTER_SUBJECT?>");
		return false;
	}
	if (form.message.value == "") {
		alert("<?=ENTER_MESSAGE?>");
		return false;
	}
	if (document.form.submit.action != "") {
		document.form.submit.disabled=1;
	}
}
// -->
</script>
<form action="index.php" method="post" name=form OnSubmit="return formCheck(this)">
<input class=input type=hidden name="l" value="<?=LANGUAGE?>">
<input class=input type=hidden name="id" value="<?=FROMID?>">
<input class=input type=hidden name="do" value="vmess">
<input class=input type=hidden name="sendmess" value="1">
<span class=head><?=MESSAGE_FROM?>
   <?php if(!FROMID) { /* If message from administrator */?>
    <B><?=ADMINISTRATOR?></B>
		<?php } else { /* Message from user*/ ?>
<a href="<?=C_URL?>/<?=ADM_DIR?>/index.php?l=<?=LANGUAGE?>&do=vp&id=<?=FROMID?>" target="_blank"><?=NAME?> [ID#<?=FROMID?>]</a></span>
		<?php }?>
<br><br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>">
<Tr>
  <Td>
  <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
  <Tr bgcolor="<?=COLOR1?>">
    <Td>
    <?=DATE?>
    </Td>
    <td>
    <?=SENDTIME?>
    </td>
  </Tr>
  <Tr bgcolor="<?=COLOR1?>">
    <Td>
    <?=SUBJECT?>
    </Td>
    <td>
    <?=SUBJECT_VALUE?>
    </td>
  </Tr>
  <Tr bgcolor="<?=COLOR1?>">
    <Td>
    <?=MESSAGE?>
    </Td>
    <td>
    <?=MESSAGE_VALUE?>
    </td>
  </Tr>
  <Tr bgcolor="<?=COLOR1?>" align="left">
    <Td colspan=2>
    <input class=input1 type=button value="<?=DELETE_MESSAGE?>" OnClick="location.href='<?=C_URL?>/<?=ADM_DIR?>/index.php?l=<?=LANGUAGE?>&do=dmess&del[]=<?=MESSAGE_ID?>'"> <input class=input1 type=button value="<?=READ_NEXT_UNREAD?>" OnClick="location.href='<?=C_URL?>/<?=ADM_DIR?>/index.php?l=<?=LANGUAGE?>&do=vmess&next=1&mid=0'">
    </Td>
  </Tr>
  <Tr bgcolor="<?=COLORH?>" align="center">
    <Td colspan=2>
    <?=REPLY?>
    </Td>
  </Tr>
  <Tr bgcolor="<?=COLOR1?>">
    <Td>
    <?=SUBJECTR?>
    </td>
    <td>
    <input class=input type=text name=subject maxlength="40" value="<?=SUBJECT_ANSWER?>">
    </td>
  </tr>
  <Tr bgcolor="<?=COLOR1?>">
    <Td>
    <?=MESSAGER?>
    <BR>
 <Table Border=0 width="160"><Tr align="left" bgcolor="<?=COLORH?>"><Td>
<?php $p=0;foreach ($smiles as $k=>$v){$p++;?>
<a href="javascript:ins(' <?=$k?> ',1)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/forum/smiles/<?=$v?>" border=0 /></a>
<?php if(C_SMILES_NUMBER <= $p){?>
<br><a href="javascript:open_win('<?=C_URL?>/forum/smiles_show.php','_blank')"><?=$w[720]?></a><br>
<?php break;}?>
<?php }?><br>
<a href="javascript:ins('B',0)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Bold.gif" border=0 /></a>
<a href="javascript:ins('I',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Italic.gif" border=0 /></a>
<a href="javascript:ins('U',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Underline.gif" border=0 /></a>
<a href="javascript:ins('S',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Strike.gif" border=0 /></a>
<a href="javascript:ins('QUOTE',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Quote.gif" border=0 /></a>  </td></tr></table>
    </td>
    <td>
    <textarea class=textarea name=message cols=40 rows=15>
    <?=MESSAGE_ANSWER?>
    </textarea>
    </td>
  </tr>
  <Tr bgcolor="<?=COLOR1?>">
    <Td colspan=2>
    <input type="checkbox" name="confirm" checked> <?=NOTIFY_ME?>
    </td>
  </tr>
  <Tr bgcolor="<?=COLOR1?>">
    <Td colspan="2">
    <input class=input1 type=submit value="<?=SEND_MESSAGE?>" name="submit">
    </td>
  </tr>
  </Table>
  </td>
</Tr>
</Table>
</form>


<br><br><br><br>

     </TD>
    </TR>
   </TABLE>

<?php

// Send confirm for message and set readed to 0 if it necessary
if ($i['readed'] != '0')
{
	// If message is not new
	if (($i['readed'] == '2') || ($i['readed'] == '4'))
	{
		// Confirm needed
		$subject = $w[236];
		$date = date("d/m/Y H:i:s", time());
		$name2 = $w[519];
		$tm = array($i['message'], $name2, '', $date);
		$message = template($w[237], $tm);

		db_query("INSERT INTO ".C_MYSQL_MESSAGES." VALUES (Null,'".$id."','".$i['fromid']."','".$subject."',NOW(),'".ip2int(ip())."','0','0')") or die(db_error());
		$lastid = db_insert_id();		
		save_message($lastid, $message);
		AddUserToAddressBook($i['fromid'],1,1);
	}
	db_query("UPDATE ".C_MYSQL_MESSAGES." SET readed='0' WHERE id='".$mid."' AND toid='".$id."'");
}

				}

			}
			else
			{
				$id = 0;
				if (!isset($from) || !is_numeric($from)) $from = 0;
				$cikl = $from;
				$tmp = db_query("SELECT id, fromid, subject,".CorrectDate("sendtime").", readed, deleted FROM ".C_MYSQL_MESSAGES." WHERE toid='".$id."' order by id DESC limit ".$from.",".C_MPAGES);
				$tmp2 = "SELECT count(id) as total FROM ".C_MYSQL_MESSAGES." WHERE toid='".$id."'";
				$tquery = db_query($tmp2) or die(db_error());
				$trows = db_fetch_array($tquery);
				$count = $trows['total'];
				if ($count == "0") sprintm($w[225], 2);

				/////////// Define variables for template
				define('TOTAL_MESSAGES', $w[226]);
				define('TOTAL_MESSAGES_VALUE', $count);
				define('NUM', $w[227]);
				define('FROM', $w[228]);
				define('SUBJECT', $w[168]);
				define('DATE', $w[229]);
				define('DEL', $w[230]);

				$vmreaded = array();
				$vmcolors = array();
				$vmnums = array();
				$vmfromids = array();
				$vmnames = array();
				$vmids = array();
				$vmsubjects = array();
				$vmsendtimes = array();
				$color = '';

				while ($i = db_fetch_array($tmp))
				{
					$cikl++;
					$color = ($color == COLOR4) ? COLOR3 :
					COLOR4;
					$vmcolors[] = $color;
					$vmnums[] = $cikl;
					$vmfromids[] = $i['fromid'];
					$vmids[] = $i['id'];
					$vmsubjects[] = (empty($i['subject'])) ? $w[349] : $i['subject'];
					$vmsendtimes[] = mysql2data($i['sendtime'], 0, 1);
					$name = "";
					if (!C_NO_NAME)
					{
						$tmp2 = db_query("SELECT * FROM ".C_MYSQL_MEMBERS_MAIN." WHERE id='".$i['fromid']."' AND status >= '7'");
						while ($j = db_fetch_array($tmp2))
						{
							$name = username($j['id'], $j['username'], $j['fname'], $j['lname']);
						}
					}
					$vmnames[] = $name;
					$vmreaded[] = ($i['readed'] == '0') ? '' :
					$w[231];
				}
				$param = "l=".$l."&do=vmess";
				define('VMPAGES', pages($from, C_MPAGES, $count, $param, '5'));
				define('DELETE_SELECTED_MESSAGES', $w[232]);
				define('ADMINISTRATOR', $w[519]);
				$p = 0;

				#		include_once C_PATH.'/templates/'.C_TEMP.'/members_view_messages.php';

?>
<script language="JavaScript">
function sel() {
	with(document.form) {
		for(i=0;i<elements.length;i++) {
			thiselm = elements[i];
			if(thiselm.name.substring(0,3) == 'del')
			thiselm.checked = !thiselm.checked
		}
	}
}
</script>


   <TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="6">
    <TR> 
     <TD ALIGN="CENTER" VALIGN="TOP">
<br><br><br>

<form action="?" method="post" name=form>
<input class=input type=hidden name="l" value="<?=LANGUAGE?>">
<input class=input type=hidden name="do" value="dmess">
<span class=head><?=TOTAL_MESSAGES?><?=TOTAL_MESSAGES_VALUE?></span>
<br><br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_BWIDTH?>" bgcolor="<?=C_TBCOLOR?>">
<Tr>
  <Td>
  <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_BWIDTH?>" class=mes>
  <Tr align="center" bgcolor="<?=COLORH?>">
    <Td>
    <?=NUM?>
    </Td>
    <td>
    <?=FROM?>
    </td>
    <td>
    <?=SUBJECT?>
    </td>
    <td>
    <?=DATE?>
    </td>
    <td>
    <?=DEL?> <input type="checkbox" onClick="sel()">
    </td>
  </Tr>
<?php while(isset($vmids[$p])) { /* Show messages - Begin */ ?>
  <Tr align="center" bgcolor="<?=$vmcolors[$p]?>">
    <Td>
    <?=$vmnums[$p]?>
    </Td>
    <td>
    <?php if(!$vmfromids[$p]) { /* If message from administrator */?>
    <B><?=ADMINISTRATOR?></B>
		<?php } else { /* Message from user*/ ?>
    <a class=link2 href="<?=C_URL?>/<?=ADM_DIR?>/index.php?do=vp&id=<?=$vmfromids[$p]?>" target="_blank"><?=$vmnames[$p]?> [ID#<?=$vmfromids[$p]?>]</a>
		<?php }?>
    </td>
    <td>
    <a class=link2 href="?l=<?=LANGUAGE?>&do=vmess&mid=<?=$vmids[$p]?>"><?=$vmsubjects[$p]?></a> <?=$vmreaded[$p]?>
    </td>
    <td>
    <?=$vmsendtimes[$p]?>
    </td>
    <td>
    <input type="checkbox" name="del[]" value="<?=$vmids[$p]?>">
    </td>
  </Tr>      
<?php $p++;}$p=0; /* Show messages - End */ ?>
  <?=VMPAGES?>  
  <Tr align="center" bgcolor="<?=COLORH?>">
    <Td colspan=5>
    <input type="submit" value="<?=DELETE_SELECTED_MESSAGES?>" class=input1>
    </td>
  </Tr>
  </Table>
  </td>
</Tr>
</Table>
</form><br><br><br><br></TD></TR></TABLE>

<?php
			}
			break;
			############################################
			# View messages from users - End
			############################################
		case "dmess":
			######################
			# Delete messages
			######################
			$arr=array();
			//if(!is_array($del)) $del = array($del);
			for($p = 0; $p <= (sizeof($del)-1); $p++)
			{
				$k=$del[$p];
				if(is_numeric($k)) $arr[]=intval($k);
			}
			$cnt = count($arr);
			RemoveMessages($arr,0);
			$tm = array($cnt);
			mes(template($w[238], $tm));
			echo "<script language=\"JavaScript\">window.location.replace('".C_URL."/".ADM_DIR."/index.php?l=".$l."&do=vmess');</script>";
			break;
			############################################
			# Maillist engine - Begin
			############################################
		case 'ms':
			$onemail='0';
			if ($firsttime) {
				$firsttime=0;$from=0;
				db_query("INSERT INTO ".C_MYSQL_MAILLIST." VALUES(null,'".$total."','".$horo."','".$agef."','".$aget."','".$gender."','".$purpose."','".$country."','".$regin."','".$number."','".$userstat."','".$usertype."','".$send_op."','".$mtype."','".$frommail."','".$subject."','".$message."','".$confirm."','0','0')") or die(db_error());
				$last_id = db_fetch_array(db_query("SELECT LAST_INSERT_ID() AS last_id"));
				$listid = $last_id['last_id'];
				if(!empty($id))  {
					$mid = " t1.id = '".$id."' AND t1.id = t2.id AND t1.id = t3.id";
					$onemail='1';
				} else {
					$mid = " t1.id > '0' AND t1.id = t2.id AND t1.id = t3.id";
				};
			}
			else {
				$from+=C_MAILS;
				$mid = " t1.id > '".$id."' AND t1.id = t2.id AND t1.id = t3.id";
			}

			$tmp=db_query("SELECT * FROM ".C_MYSQL_MAILLIST." WHERE id='".$listid."'");
			while ($i = db_fetch_array($tmp)) {
				$total=$i['uid'];
				$horo=$i['horo'];
				$agef=$i['agef'];
				$aget=$i['aget'];
				$gender=$i['gender'];
				$purpose=$i['purposes'];
				$country=$i['country'];
				$regin=$i['regin'];
				$number=$i['unact'];
				$userstat=$i['status'];
				$usertype=$i['usertype'];
				$send_op=$i['send_op'];
				$mtype=$i['mtype'];
				$frommail=$i['frommail'];
				$subject=$i['subject'];
				$message = ($mtype) ? $i['message'] : tbm($i['message']);
				$confirm=$i['confirm'];
			}
			!empty($horo) ? $mhoro=" AND t1.horo = '".$horo."'" : $mhoro = "";
			$magef=" AND t1.birthday <= DATE_SUB(NOW(), INTERVAL ".$agef." YEAR)";
			$maget=" AND t1.birthday >= DATE_SUB(NOW(), INTERVAL ".($aget+1)." YEAR)";
			$mnumber = !empty($number) ? " AND t1.editdate < DATE_SUB(NOW(), INTERVAL ".$number." DAY)" : "";
			!empty($gender) ? $mgender=" AND t1.gender = '".$gender."'" : $mgender = "";
			!empty($purpose) ? $mpurpose=" AND t1.purposes = '".$purpose."'" : $mpurpose = "";
			!empty($country) ? $mcountry=" AND t1.country = '".$country."'" : $mcountry = "";
			!empty($usertype) ? $musertype=" AND t1.req IN(".$usertype.")" : $musertype = "";
			switch ($userstat) {
				case "1":$muserstat = " AND t1.status = '0'";break;
				case "2":$muserstat = " AND t1.status = '1'";break;
				case "3":$muserstat = " AND (t1.status = '2' OR t1.status = '9')";break;
				case "4":$muserstat = " AND (t1.status = '3' OR t1.status = '8')";break;
				case "5":$muserstat = " AND (t1.status = '5' OR t1.status = '6' OR t1.status = '4')";break;
				case "6":$muserstat = " AND t1.status >= '7'";break;
				default:$muserstat = "";break;
			}
			!empty($regin) ? $mregin=" AND t1.regdate > DATE_SUB(NOW(), INTERVAL ".$wrgv[$regin]." DAY)" : $mregin = "";
			//$mes_type = $mtype ? 'html' : 'text';
			$tmp = db_query("SELECT t1.* ,".CorrectDate("regdate").", ".CorrectDate("editdate").", t2.*, t3.* FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2, ".C_MYSQL_MEMBERS_SEC." as t3 WHERE ".$mid.$mhoro.$magef.$maget.$mgender.$mpurpose.$mcountry.$muserstat.$musertype.$mregin.$mnumber." order by t1.id ASC limit ".C_MAILS) or die(db_error());
			$is_zero=db_num_rows($tmp);
			while ($i = db_fetch_array($tmp)) {
				if ($mtype){
					$html = $message;
					$plain = cb($message);
				} else {
					$html = nl2br($message);
					$plain = $message;
				}
				$messages = $mtype ? $message : tb($message);
				$repl = array('USERNAME'=>$i['username'],'FNAME'=>$i['fname'],'LNAME'=>$i['lname'],'PASSWORD'=>$i['password'],'EMAIL'=>$i['email'],'ID'=>$i['id'],'USERTYPE'=>$wpu[$i['req']],'REGDATE'=>mysql2data($i['regdate'], 0, 1),'EDITDATE'=>mysql2data($i['editdate'], 0, 1));
				//$msg=template($messages,$repl);
				$html=template($html, $repl);
				$plain=template($plain, $repl);
				$subj=template($subject,$repl);
				switch($send_op)
				{
					case '0':default:
						sendmail($frommail,$i['email'],$subj,$html,$plain,$i['nomail']);
						break;
					case '1':
						db_query("INSERT INTO ".C_MYSQL_MESSAGES." VALUES (Null,'0','".$i['id']."','".$subj."',NOW(),'0','1','0')");
						$lastid = db_insert_id();		
						save_message($lastid, nl2br($msg));
						AddUserToAddressBook($i['id'],0);
						break;
					case '2':
						db_query("INSERT INTO ".C_MYSQL_MESSAGES." VALUES (Null,'0','".$i['id']."','".$subj."',NOW(),'0','1','0')");
						$lastid = db_insert_id();		
						save_message($lastid, nl2br($msg));
						AddUserToAddressBook($i['id'],0);

						if (C_ID == 0) $login = $i['username'];
						elseif(C_ID == 1) $login = $i['id'];
						elseif(C_ID == 2) $login = $i['email'];

						if(!empty($i['mylang']) && file_exists(C_PATH.'/languages/'.$i['mylang'].'/'.$i['mylang'].'a.php'))
						{
							@include(C_PATH.'/languages/'.$i['mylang'].'/'.$i['mylang'].'.php');
							@include(C_PATH.'/languages/'.$i['mylang'].'/'.$i['mylang'].'a.php');
							@include(C_PATH.'/languages/'.$i['mylang'].'/'.$i['mylang'].'l.php');
							@include(C_PATH.'/languages/'.$i['mylang'].'/'.$i['mylang'].'_.php');
							$USERLANG=$i['mylang'];
						}
						else
						{
							@include(C_PATH.'/languages/default/default.php');
							@include(C_PATH.'/languages/default/defaulta.php');
							@include(C_PATH.'/languages/default/defaultl.php');
							@include(C_PATH.'/languages/default/default_.php');
							$USERLANG='default';
						}

						$subject = template($x[19], array(C_SNAME));
						$message = template($x[95], array($i['fname'], $i['lname'], $x[730], C_SNAME, C_URL.'/login.php', $login));
						sendmail(C_FROMM,$i['email'],$subject,nl2br($message),$message,$i['nomail']);
						break;
				}
				$id=$i['id'];
			}

			if($send_op == '2')
			{
				@include(C_PATH.'/languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'.php');
				@include(C_PATH.'/languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'_.php');
				@include(C_PATH.'/languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'a.php');
				@include(C_PATH.'/languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'l.php');
			}
			if($onemail) $id='99999999999'; // Big number
			db_query("UPDATE ".C_MYSQL_MAILLIST." SET lastid='".$id."' WHERE id='".$listid."'");
			if ($is_zero != '0') {
				smes('<center>'.$x[15].$total);
				smes('<br>'.$x[16].$from);
				smes('<br>'.$x[17].$is_zero);
?>
<script language="JavaScript">
<!--
window.location.replace('<?=filename()?>?<?=s()?>&from=<?=$from?>&l=<?=$l?>&a=s&listid=<?=$listid?>&firsttime=0&ds=1&do=ms&id=<?=$id?>');
// -->
</script>
<?php 
			} else {
				smes('<center>'.$x[15].$total);
				smes('<br>'.$x[15].$total);
				$tm=array($total);
				$submes=template($x[18],$tm);
				db_query("UPDATE ".C_MYSQL_MAILLIST." SET maildate=NOW() WHERE id='".$listid."'") or die(db_error());
				if($confirm == 'on') sendmail(C_ADMINM,C_ADMINM,$submes,nl2br($submes),$submes);
				sprintm($submes);
			}
			break;
			############################################
			# Maillist engine - End
			############################################
			############################################
			# Maillist engine 2 (Send mail to users with unreaded messages) - Begin
			############################################
		case 'ms2':
			if ($firsttime)
			{
				$firsttime = 0;$from = 0;
				$tm = array(C_SNAME);
				$subject = template($x[19], $tm);
				$message = '';

				$tmp = db_query("SELECT DISTINCT toid FROM ".C_MYSQL_MESSAGES." WHERE readed='1' OR readed='2'") or die(db_error());
				$total = db_num_rows($tmp);
			}
			else $from += C_MAILS;
			$tmp_ = db_query("SELECT id, toid, count(*) as c FROM ".C_MYSQL_MESSAGES." WHERE (readed = '1' or readed = '2') group by toid order by toid ASC limit ".C_MAILS);
			$is_zero = db_num_rows($tmp_);
			if ($is_zero != '0')
			{
				while ($j = db_fetch_array($tmp_))
				{
					$tmp = db_query("SELECT t1.* ,".CorrectDate("regdate").", ".CorrectDate("editdate").", t2.*, t3.* FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2, ".C_MYSQL_MEMBERS_SEC." as t3 WHERE t1.id=".$j['toid']." AND t1.id = t2.id AND t1.id = t3.id") or die(db_error());
					$is_zero2 = db_num_rows($tmp);
					if ($is_zero2 == '0')
					{
						# Fixing possible error... If no user in database, remove that message
						db_query("DELETE FROM ".C_MYSQL_MESSAGES." WHERE id = '".$j['id']."'");
					}
					else
					{
						while ($i = db_fetch_array($tmp))
						{
							if (C_ID == 0) $login = $i['username'];
							elseif(C_ID == 1) $login = $i['id'];
							elseif(C_ID == 2) $login = $i['email'];
							if(!empty($i['mylang']) && file_exists(C_PATH.'/languages/'.$i['mylang'].'/'.$i['mylang'].'a.php'))
							{
								@include(C_PATH.'/languages/'.$i['mylang'].'/'.$i['mylang'].'.php');
								@include(C_PATH.'/languages/'.$i['mylang'].'/'.$i['mylang'].'a.php');
								@include(C_PATH.'/languages/'.$i['mylang'].'/'.$i['mylang'].'l.php');
								@include(C_PATH.'/languages/'.$i['mylang'].'/'.$i['mylang'].'_.php');
								$USERLANG=$i['mylang'];
							}
							else
							{
								@include(C_PATH.'/languages/default/default.php');
								@include(C_PATH.'/languages/default/defaulta.php');
								@include(C_PATH.'/languages/default/defaultl.php');
								@include(C_PATH.'/languages/default/default_.php');
								$USERLANG='default';
							}
							$tm = array(C_SNAME);
							$subject = template($x[19], $tm);
							$tm = array($i['fname'], $i['lname'], $j['c'], C_SNAME, C_URL.'/login.php', $login);
							sendmail(C_FROMM, $i['email'], $subject, nl2br(template($x[95], $tm)), template($x[95], $tm),$i['nomail']);
							db_query("UPDATE ".C_MYSQL_MESSAGES." SET readed=readed+2 WHERE toid = '".$i['id']."' AND (readed = '1' or readed = '2')");
						}
					}
				}
				@include(C_PATH.'/languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'.php');
				@include(C_PATH.'/languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'_.php');
				@include(C_PATH.'/languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'a.php');
				@include(C_PATH.'/languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'l.php');
				smes('<center>'.$x[15].': '.$total);
				smes('<br>'.$x[16].': '.$from);
				smes('<br>'.$x[17].': '.$is_zero);
				#sleep(1);
?>
<script language="JavaScript">
<!--
window.location.replace('<?=filename()?>?<?=s()?>&total=<?=$total?>&from=<?=$from?>&l=<?=$l?>&a=s&firsttime=0&ds=1&do=ms2');
// -->
</script>
<?php
			}
			else
			{
				smes('<center>'.$x[15].': '.$total);
				smes('<br>'.$x[16].': '.$total);
				$tm = array($total);
				$submes = template($x[18], $tm);
				sendmail(C_ADMINM, C_ADMINM, $submes, nl2br($submes), $submes);
				sprintm($submes);
			}
			break;
			############################################
			# Maillist engine - End
			############################################
			############################################
			# Maillist form - Begin
			############################################
		case 'mm':
			if(!isset($a)) $a='';
			if ($a == "s") {
				if (!isset($step)) $step = 10;
				if (!isset($from)) $from = 0;
				$id=cb($id);
				if(!isset($agef) || !is_numeric($agef)) $agef = 0;
				if(!isset($aget) || !is_numeric($aget)) $aget = 255;


				if(isset($usertype))
				{
					$tmp = implode(',',$usertype);
					if(!preg_match("/^[0-9]|,$/", $tmp)) $usertype = '';
					else $usertype = $tmp;
				} else $usertype = '';

				if($agef > $aget) sprintm($w[109]);
				if((isset($number) && !is_numeric($number)) || !isset($number)) $number = 0;
				if(C_HACK1) {
					if((!empty($id)&&!is_numeric($id))||!is_numeric($horo)||!is_numeric($step)||!is_numeric($from)||!is_numeric($gender)||!is_numeric($purpose)||!is_numeric($country)||!is_numeric($agef)||!is_numeric($aget)||!is_numeric($regin)||!is_numeric($userstat)||!is_numeric($send_op)) sprintm($w[1].'1',1);}
					if(C_HACK2) {
						if(($regin < 0)||($regin >= sizeof($wrg))||($regin >= sizeof($wrgv))||($horo < 0)||($horo >= sizeof($whr))||($gender < 0)||($gender >= sizeof($wg))||($purpose < 0)||($purpose >= sizeof($wp))||($country < 0)||($country >= sizeof($wcr))||($userstat < 0)||($userstat >= sizeof($wst))||($send_op < 0)||($send_op >= sizeof($xmm))) sprintm($w[1].'2',1);}
						// Important /////////////
						$id != "" ? $mid = " t1.id = '".$id."' AND t1.id = t2.id AND t1.id = t3.id" : $mid = " t1.id != '0' AND t1.id = t2.id AND t1.id = t3.id";
						//////////////////////////
						!empty($horo) ? $mhoro=" AND t1.horo = '".$horo."'" : $mhoro = "";
						$magef=" AND t1.birthday <= DATE_SUB(NOW(), INTERVAL ".$agef." YEAR)";
						$mnumber = !empty($number) ? " AND t1.editdate < DATE_SUB(NOW(), INTERVAL ".$number." DAY)" : "";
						$maget=" AND t1.birthday >= DATE_SUB(NOW(), INTERVAL ".($aget+1)." YEAR)";
						!empty($gender) ? $mgender=" AND t1.gender = '".$gender."'" : $mgender = "";
						!empty($purpose) ? $mpurpose=" AND t1.purposes = '".$purpose."'" : $mpurpose = "";
						$musertype= ($usertype !== '') ? " AND t1.req IN(".$usertype.")" : "";
						!empty($country) ? $mcountry=" AND t1.country = '".$country."'" : $mcountry = "";
						///////// Checking and creating for search
						switch ($userstat) {
							case "1":$muserstat = " AND t1.status = '0'";break;
							case "2":$muserstat = " AND t1.status = '1'";break;
							case "3":$muserstat = " AND (t1.status = '2' OR t1.status = '9')";break;
							case "4":$muserstat = " AND (t1.status = '3' OR t1.status = '8')";break;
							case "5":$muserstat = " AND (t1.status = '5' OR t1.status = '6' OR t1.status = '4')";break;
							case "6":$muserstat = " AND t1.status >= '7'";break;
							default:$muserstat = "";break;
						}
						switch ($otherfilter){
							case "0": $motherfilter = " AND t1.coupon = ''";break;
							case "1": $motherfilter = " AND t1.regdate >= DATE_SUB(t1.editdate, INTERVAL 0 DAY)";break;
							default: $motherfilter = "";break;
						}
						!empty($regin) ? $mregin=" AND t1.regdate > DATE_SUB(NOW(), INTERVAL ".$wrgv[$regin]." DAY)" : $mregin = "";
						$tsql = "SELECT count(t1.id) as total FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2, ".C_MYSQL_MEMBERS_SEC." as t3 WHERE ".$mid.$mhoro.$magef.$maget.$mnumber.$mgender.$mpurpose.$musertype.$mcountry.$muserstat.$motherfilter.$mregin;
						$tquery = db_query($tsql) or die(db_error());
						$trows = db_fetch_array($tquery);
						$count = $trows['total'];
						if($count == "0") sprintm($w[110]);
						$tm=array($count);
?>
<span class=head><?=template($x[20],$tm)?></span>
<center><span class=head><?=$x[21]?></span>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="600" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="600" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[126]?></td>
<td><?=$id?></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[129]?></td>
<td><?=$whr[$horo]?></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[120]?></td>
<td><?=$agef?> - <?=$aget?>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[132]?></td>
<td><?=$wg[$gender]?>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[133]?></td>
<td><?=arr2val($wp,$purpose)?>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[121]?></td>
<td><?=$wcr[$country]?>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[148]?> </td>
<td><?=$wrg[$regin]?>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[425]?></Td><Td><?php if($number) {?><?=$number?> <?=$x[394]?><?php }?></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[22]?> </td>
<td><?=$wst[$userstat]?>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
Egyéb szűrő </td>
<td><?=$wos[$otherfilter]?>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[270]?> </td>
<td><?=arr2val($wpu, $usertype)?>
</td></tr>
<Tr align="center" bgcolor="<?=COLOR1?>"><Td colspan=2>
<?=$x[23]?></td>
</tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <td width=70%><?=$x[729]?></td>
      <td><?=$xmm[$send_op]?> </td>
    </tr>

<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[24]?></td>
<td><?= $mtype ? 'HTML' : 'Text';?>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[25]?></td>
<td><?=$frommail?></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[26]?></td>
<td><?=cb($subject)?></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[27]?></td>
<td><?= $mtype ? $message : cb($message);?></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[28]?></td>
<td><input type="checkbox" name="confirm" <?php  echo ((isset($confirm))&&($confirm == 'on')) ? 'checked' : '';?> disabled></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2>
<input type=button value="<?=$x[29]?>" class=button OnClick="javascript:history.back(-1);"> 
<form method="Post" action="?" OnSubmit="javascript:open_win('','maillist');" target='maillist' name='maillist'>
<input type="hidden" name="total" value="<?=$count?>">
<input type="hidden" name="a" value="s">
<input type="hidden" name="firsttime" value="1">
<input type="hidden" name="ds" value="1">
<input type="hidden" name="do" value="ms">
<input type="hidden" name="id" value="<?=$id?>">
<input type="hidden" name="horo" value="<?=$horo?>">
<input type="hidden" name="agef" value="<?=$agef?>">
<input type="hidden" name="aget" value="<?=$aget?>">
<input type="hidden" name="gender" value="<?=$gender?>">
<input type="hidden" name="purpose" value="<?=$purpose?>">
<input type="hidden" name="number" value="<?=$number?>">
<input type="hidden" name="country" value="<?=$country?>">
<input type="hidden" name="userstat" value="<?=$userstat?>">
<input type="hidden" name="usertype" value="<?=$usertype?>">
<input type="hidden" name="regin" value="<?=$regin?>">
<input type="hidden" name="send_op" value="<?=$send_op?>">
<input type="hidden" name="mtype" value="<?=$mtype?>">
<input type="hidden" name="frommail" value="<?=cbm($frommail)?>">
<input type="hidden" name="subject" value="<?=cbm($subject)?>">
<?php
if($mtype) {
	$message=str_replace("\r\n","",$message);
?>
<input type="hidden" name="message" value="<?=cbm($message)?>">
<?php } else {?>
<input type="hidden" name="message" value="<?=cbm($message)?>">
<?php }?>
<input type="hidden" name="confirm" value="<?=$confirm?>">
<input type="submit" value="<?=$x[30]?>" class=button>
</form>
</td></tr>
</table></td></tr></table>
<?php
			} else {
?>
<script language="JavaScript">
<!--
function formCheck(form) {
	if (form.subject.value == "")
	{alert("<?=$w[166]?>");return false;}
	if (form.message.value == "")
	{alert("<?=$w[167]?>");return false;}
	if (document.form.submit.action != "") {
		document.form.submit.disabled=1;}
}
// -->
</script>
<form action="?" method="post" name=form OnSubmit="return formCheck(this)">
<input type="hidden" name="l" value="<?=$l?>">
<input type="hidden" name="a" value="s">
<input type="hidden" name="do" value="mm">
<center><span class=head><?=$x[21]?></span><br>
<?php 
// Check for unfinished dispatch
$tmp=db_query("SELECT * FROM ".C_MYSQL_MAILLIST." WHERE maildate='0'");
$count=db_num_rows($tmp);
if ($count != '0') { // Have unfinished dispatches
	$tm=array($count);
?>
<br><span class=head><?=template($x[31],$tm);?></span><br>
<?php 
while($i = db_fetch_array($tmp)) {
	$tm=array(cb($i['subject']),$i['uid']);
	echo '<span class=mes>'.template($x[32],$tm);
?> 
<a href="javascript:open_win('<?=filename()?>?<?=s()?>&total=<?=$i['uid']?>&from=<?=$i['lastid']?>&l=<?=$l?>&a=s&listid=<?=$i['id']?>&firsttime=0&ds=1&do=ms&id=<?=$i['lastid']?>','sendmail');">[<?=$x[33]?>]</a> <a href="javascript:open_win('<?=filename()?>?<?=s()?>&l=<?=$l?>&a=s&ds=1&do=dm&id=<?=$i['id']?>','delmail');">[<?=$x[66]?>]</a></span><br>
<?php 
}
}
?>
<br><br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="600" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="600" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[126]?></td>
<td><input type="text" name="id" class="minput"></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[129]?></td>
<td><select name="horo" class="input">
<?php  $p=0; while(isset($whr[$p])) {echo '<option value="'.$p.'">'.$whr[$p];$p++;}?>
</select></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[120]?></td>
<td><input name="agef" class="sinput"> -  <input name="aget" class="sinput">
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[132]?></td>
<td>
<select name="gender" class="input">
<?php  $p=0;while(isset($wg[$p])) {echo '<option value="'.$p.'">'.$wg[$p];$p++;}
?>
</select>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[133]?></td>
<td>
<select name="purpose" class="input">
<?php  $p=0;while(isset($wp[$p])) {echo '<option value="'.$p.'">'.$wp[$p];$p++;}
?>
</select>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[121]?></td>
<td>
<select name="country" class="input">
<?php  $p=0;asort($wcr);reset($wcr);
while (list ($p, $val) = each ($wcr)) {
	echo '<option value="'.$p.'">'.$val;
}
?>
</select>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[148]?> </td>
<td>
<select name="regin" class="input">
<?php  $p=0;while(isset($wrg[$p])) {echo '<option value="'.$p.'">'.$wrg[$p];$p++;}
?>
</select>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[425]?>
</Td><Td>
<input type=text name="number" class=sinput> <?=$x[394]?>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[22]?></td>
<td>
<select name="userstat" class="input">
<?php  $p=0;while(isset($wst[$p])) {echo '<option value="'.$p.'">'.$wst[$p];$p++;}
?>
</select>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
Egyéb szűrő</td>
<td>
<select name="otherfilter" class="input">
<option value="-1"><?=$x[236]?>
<?php  $p=0;while(isset($wos[$p])) {echo '<option value="'.$p.'">'.$wos[$p];$p++;}
?>
</select>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[270]?><br>
<?=$x[448]?></Td><Td>
<select name="usertype[]" class=input multiple style='height:60px'>
<?php $p=0;while(isset($wpu[$p])) {echo '<option value='.$p.' selected>'.$wpu[$p];$p++;}?></select></Td></Tr>
<Tr align="center" bgcolor="<?=COLOR1?>"><Td colspan=2>
<?=$x[23]?></td>
</tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <td width=70%><?=$x[729]?></td>
      <td><select name="send_op" class=input>
      <?php 
      $p=0;while(isset($xmm[$p])) {
      	echo '<option value="'.$p.'">'.$xmm[$p];
      	$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[24]?></td>
<td>
<select name="mtype" class="input">
<option value=0><?=$x[34]?>
<option value=1><?=$x[35]?>
</select>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[25]?></td>
<td><input type="text" name="frommail" class="input" value="<?=C_ADMINM?>"></td></tr>
<Tr align="left" bgcolor="<?=COLOR1?>"><Td colspan=2><?=$x[424]?></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[26]?></td>
<td><input type="text" name="subject" class="input"></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[27]?></td>
<td><textarea name="message" rows="8" class=input></textarea></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[28]?></td>
<td><input type="checkbox" name="confirm" checked></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2>
<input type="submit" value="<?=$x[36]?>" class=button></td></tr>
</table></td></tr></table></form><br>
<?php
			}
			break;
			############################################
			# Maillist manager - End
			############################################
			############################################
			# Maillist form - Begin
			############################################
		case 'mum':
			$tmp=db_query("SELECT DISTINCT toid FROM ".C_MYSQL_MESSAGES." WHERE (readed='1' OR readed='2')") or die(db_error());
			$count = db_num_rows($tmp);
			$tmp=db_query("SELECT count(toid) as total FROM ".C_MYSQL_MESSAGES." WHERE readed='1' OR readed='2'") or die(db_error());
			$rows=db_fetch_array($tmp);
			$count2=$rows['total'];
			$tmp=db_query("SELECT id FROM ".C_MYSQL_MESSAGES." WHERE readed='1' OR readed='2' order by id DESC limit 1") or die(db_error());
			$rows=db_fetch_array($tmp);
			$total=$rows['id'];
?>
<?php table_header($x[359])?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[37]?></td>
      <td><?=$count?></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[38]?></td>
      <td><?=$count2?></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[39]?></td>
      <td><?=$total?></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td colspan=2><span class=mes><a href="javascript:open_win('<?=filename()?>?<?=s()?>&total=<?=$total?>&firsttime=1&ds=1&do=ms2','sendmail');"><?=$x[40]?></a></span></td>
    </tr>
<?php table_footer()?>
<?php
break;
############################################
# Maillist manager - End
############################################
############################################
# Maillist form - Begin
############################################
		case 'mbc':
			$month=date("m", time());
			$day=date("d", time());
			$md = $month.$day;

			if(isset($run))
			{
				@set_time_limit(0);
				@ini_set("memory_limit", "64M");

				if($md != LAST_DATE) {
					#echo $md;
					$file=C_PATH.'/include/options.inc.php';
					function WriteConstant($file,$data,$real)
					{
						$cnt = file($file);
						$fp = fopen($file,"w");
						flock($fp, LOCK_EX);
						for ($i=0;$i<count($cnt);$i++)
						{
							if (strpos($cnt[$i],"','"))
							{
								list($param,$value) = split("','",$cnt[$i]);
								if (trim($param) == "define('".$data)
								{
									$cnt[$i] = "define('".$data."','".$real."');\n";
									break;
								}
							}
						}
						fwrite($fp, implode("",$cnt));fflush($fp);flock($fp, LOCK_UN);
						fclose($fp);
					}


					$result = db_query("SELECT t1.*, t2.* FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_SEC." as t2  WHERE MONTH(t1.birthday)=".$month." AND DAYOFMONTH(t1.birthday)=".$day." AND t1.status > '6' AND t1.id > '".LAST_ID."' AND t1.id = t2.id order by t1.id ASC limit ".C_MAILS) or die(db_error());
					$count=db_num_rows($result);#echo $count;
					if($count == '0') {
						WriteConstant($file,'LAST_DATE',$md);
						WriteConstant($file,'LAST_ID','0');
						sprintm($x[843]);

					}
					elseif($count < C_MAILS) {
						while ($i = db_fetch_array($result)) {

							###### Selecting language for each user
							if(C_SHOW_LANG) {
								$avail_langs = explode(',',C_AVAILABLE_LANG);
								$l = '';
								$lang = $i['mylang'];
								if(in_array($lang,$avail_langs) && file_exists(C_PATH.'/languages/'.$lang.'/'.$lang.'.php')) {
									$l = $lang;
								}
								elseif($lang == C_DEFAULT_LANG) {
									$l = C_USERLANG;
								}
								if(!file_exists(C_PATH.'/languages/'.$l.'/'.$l.'.php') || (empty($l))) $l=C_USERLANG;
								@include C_PATH.'/languages/'.$l.'/'.$l.'.php';
								@include C_PATH.'/languages/'.$l.'/'.$l.'l.php';
								@include C_PATH.'/languages/'.$l.'/'.$l.'_.php';
							}
							else {
								@include_once C_PATH.'/languages/'.C_USERLANG.'/'.C_USERLANG.'.php';
								@include_once C_PATH.'/languages/'.C_USERLANG.'/'.C_USERLANG.'l.php';
								@include_once C_PATH.'/languages/'.C_USERLANG.'/'.C_USERLANG.'_.php';
							}
							######
							$arraysub=array($i['fname'],$i['lname']);
							$arraymes=array($i['fname'],$i['lname'],C_SNAME,C_URL);
							sendmail(C_ADMINM,$i['email'],template($w[420],$arraysub),nl2br(template($w[421],$arraymes)),template($w[421],$arraymes),$i['nomail']);
						}
						WriteConstant($file,'LAST_DATE',$md);
						WriteConstant($file,'LAST_ID','0');
						sprintm($x[843]);
					}
					else {
						while ($i = db_fetch_array($result)) {
							###### Selecting language for each user
							if(C_SHOW_LANG) {
								$avail_langs = explode(',',C_AVAILABLE_LANG);
								$l = '';
								$lang = $i['mylang'];
								if(in_array($lang,$avail_langs) && file_exists(C_PATH.'/languages/'.$lang.'/'.$lang.'.php')) {
									$l = $lang;
								}
								elseif($lang == C_DEFAULT_LANG) {
									$l = C_USERLANG;
								}
								if(!file_exists(C_PATH.'/languages/'.$l.'/'.$l.'.php') || (empty($l))) $l=C_USERLANG;
								@include C_PATH.'/languages/'.$l.'/'.$l.'.php';
								@include C_PATH.'/languages/'.$l.'/'.$l.'l.php';
								@include C_PATH.'/languages/'.$l.'/'.$l.'_.php';
							}
							else {
								@include_once C_PATH.'/languages/'.C_USERLANG.'/'.C_USERLANG.'.php';
								@include_once C_PATH.'/languages/'.C_USERLANG.'/'.C_USERLANG.'l.php';
								@include_once C_PATH.'/languages/'.C_USERLANG.'/'.C_USERLANG.'_.php';
							}
							######
							$id=$i['id'];
							$arraysub=array($i['fname'],$i['lname']);
							$arraymes=array($i['fname'],$i['lname'],C_SNAME,C_URL);
							sendmail(C_ADMINM,$i['email'],template($w[420],$arraysub),nl2br(template($w[421],$arraymes)),template($w[421],$arraymes),$i['nomail']);
						}
						WriteConstant($file,'LAST_ID',$id);
						if(!isset($from)) $from = C_MAILS;
						else $from=$from+C_MAILS;
						$ccount = (isset($total)) ? $total : $count;
						smes('<center>'.$x[15].': '.$ccount);
						smes('<br>'.$x[16].': '.$from);
						smes('<br>'.$x[17].': '.C_MAILS);
					?>
					<script language="JavaScript">
					<!--
					window.location.replace('index.php?ds=1&do=mbc&run=1&from=<?=$from?>&total=<?=$ccount?>');
					// -->
					</script>
<?php 
die();
					}
				}

			} else {

				$result = db_query("SELECT t1.*, t2.* FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_SEC." as t2  WHERE MONTH(t1.birthday)=".$month." AND DAYOFMONTH(t1.birthday)=".$day." AND t1.status > '6' AND t1.id > '".LAST_ID."' AND t1.id = t2.id order by t1.id ASC") or die(db_error());
				$count=db_num_rows($result);

?>
<?php table_header($x[840])?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[361]?></td>
      <td>[<?=substr(LAST_DATE,0,2).'/'.substr(LAST_DATE,2,2)?>]</td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$w[176]?> [<?=$month?>/<?=$day?>]</td>
      <td><?=$count?></td>
    </tr>
    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[90]?></td>
      <td><?php  if($md != LAST_DATE) { ?> <?=$x[841]?> <?php } else {?> <?=$x[842]?> <?php }?></td>
    </tr>
    <?php  if($md != LAST_DATE) { ?>
    <tr class=desc bgcolor="<?=colors()?>">
      <td colspan=2><span class=mes><a href="javascript:open_win('index.php?ds=1&do=mbc&run=1&total=<?=$count?>','mbc');"><?=$x[840]?></a></span></td>
    </tr>
    <?php }?>
<?php table_footer()?>
<?php
			}
			break;
			############################################
			# Maillist manager - End
			############################################

############################################
# Vote Language Remove - Begin
############################################
		case 'vlr':
			if(isset($id) && is_numeric($id)) {
				db_query("DELETE FROM ".C_MYSQL_VOTE_QUESTIONS." where id = '".$id."' and lang = '".cb($lang)."'");
				sprintm($x[459]);
			}
			break;
			############################################
			# Vote Language Remove - End
			############################################
			############################################
			# Vote Add Language - Begin
			############################################
		case 'val':
			if(isset($id) && is_numeric($id)) {
				if(isset($a) && ($a == 'create')) {
					if(isset($lang) && count($lang) > 0) {
						$tmp=db_query("SELECT * FROM ".C_MYSQL_VOTE_QUESTIONS." where id = '".$id."' limit 1");
						$exarr=array();
						while($i=db_fetch_array($tmp)) {
							$p=0;while(isset($lang[$p])) {
								db_query("REPLACE INTO ".C_MYSQL_VOTE_QUESTIONS." VALUES ('".$id."','".$i['question']."','".cb($lang[$p])."','".$i['qtext1']."','".$i['qtext2']."','".$i['qtext3']."','".$i['qtext4']."','".$i['qtext5']."','".$i['qtext6']."','".$i['qtext7']."','".$i['qtext8']."','".$i['qtext9']."','".$i['qtext10']."','".$i['qtext11']."','".$i['qtext12']."','".$i['qtext13']."','".$i['qtext14']."','".$i['qtext15']."','".$i['qtext16']."','".$i['qtext17']."','".$i['qtext18']."','".$i['qtext19']."','".$i['qtext20']."')");

								$p++;
							}
						}
						sprintm($x[461]);
					}
				} else {
					$tmp=db_query("SELECT * FROM ".C_MYSQL_VOTE_QUESTIONS." where id = '".$id."'");
					$exarr=array();
					while($i=db_fetch_array($tmp)) {
						$exarr[]=$i['lang'];
					}
?>
<span class=head><?=$x[460]?></span><br>
<form action="<?=filename()?>?l=<?=$l?>&id=<?=$id?>&do=val&<?=s()?>&a=create" method="post" name=form>
        <Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="500" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="500" class=mes>
    <tr align="<?=C_ALIGN?>" class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[450]?><br><br><?=$x[448]?></td>
      <td><select multiple name="lang[]" class=input size="12">
      <?php 
      $existlang = explode(",",C_AVAILABLE_LANG);
      $p=0;while(isset($existlang[$p])) {
      	if(!in_array($existlang[$p],$exarr)) {
			?>
			<option value="<?=$existlang[$p]?>"><?=$reallangname[$existlang[$p]]?>
			<?php }$p++;}?>      
      </select>
      </td>
    </tr>
				<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
        <Td colspan=2><input type=submit name=submit class=input value="<?=$x[460]?>"></Td></Tr>
</Table></Td></Tr></Table></form>

<?php
				}
			}
			break;
			############################################
			# Vote Add Language - End
			############################################
			############################################
			# Vote Make Default - Begin
			############################################
		case 'vmd':
			if(isset($id) && is_numeric($id)) {
				db_query("UPDATE ".C_MYSQL_VOTES." set actual = '0'");
				db_query("UPDATE ".C_MYSQL_VOTES." set actual = '1' where id = '".$id."'");
				sprintm($x[458]);
			}
			break;
			############################################
			# Vote Manager - End
			############################################
			############################################
			# Vote Manager - Begin
			############################################
		case 'vm':
?>
<span class=head><?=$x[438]?></span><br><br>
</center>
<?php
$tmp=db_query("SELECT * FROM ".C_MYSQL_VOTES." order by actual DESC, id DESC");
while($i = db_fetch_array($tmp)) {
	$tmp2=db_query("SELECT * FROM ".C_MYSQL_VOTE_QUESTIONS." where id = '".$i['id']."' limit 1");
	while($j = db_fetch_array($tmp2)) {
?>        
<center><br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="600" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="600" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[451]?> [<?=$reallangname[$j['lang']]?>]</Td>
        <Td><strong><?=$j['question']?></strong></Td>
        </Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[449]?> <strong><?=$i['id']?></strong>
</Td><Td>
<?=$x[452]?> <strong>&lt;?=ShowVote(<?=$i['id']?>)&gt;</strong> 
<?php if($i['actual'] > 0){?> <?=$x[457]?> <strong>&lt;?=ShowVote()&gt;</strong> <?php }?><br>
<?=$x[453]?> <strong>&lt;?=ShowResults(<?=$i['id']?>)&gt;</strong>
<?php if($i['actual'] > 0){?> <?=$x[457]?> <strong>&lt;?=ShowResults()&gt;</strong> <?php }?>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[454]?>
</Td><Td align=left>
<table border=0 width=100% cellspacing=1 cellpadding=2>
<?php
$tmp3=db_query("SELECT * FROM ".C_MYSQL_VOTE_QUESTIONS." where id = '".$i['id']."'");
while($jj = db_fetch_array($tmp3)) {
?>
<tr><td bgcolor=<?=COLOR4?>>
<?=$reallangname[$jj['lang']]?><br>
</td>
<td>
<a href="<?="javascript:open_win('".filename()."?l=".$l."&id=".$i['id']."&lang=".$jj['lang']."&ds=1&do=vle&".s()."','vle');";?>" class=desc>[<?=$x[60]?>]</a>
</td>

<td>
<a href="<?="javascript:open_win('".filename()."?l=".$l."&id=".$i['id']."&lang=".$jj['lang']."&ds=1&do=vlr&".s()."','vlr');";?>" class=desc>[<?=$x[66]?>]</a>
</td>
</tr>

<?php
}
?>
</table>
<a href="<?="javascript:open_win('".filename()."?l=".$l."&id=".$i['id']."&ds=1&do=val&".s()."','val');";?>" class=desc>[<?=$x[460]?>]</a>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[90]?>
</Td><Td>
<?php if($i['actual'] > 0){?> 
<strong><?=$x[456]?></strong>
<?php } else {?>
<a href="<?="javascript:open_win('".filename()."?l=".$l."&id=".$i['id']."&ds=1&do=vmd&".s()."','vmd');";?>" class=desc>[<?=$x[455]?>]</a>
<?php }?>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2>
<a href="<?="javascript:open_win('".filename()."?l=".$l."&id=".$i['id']."&ds=1&do=ve&".s()."','ve');";?>" class=desc>[<?=$x[440]?>]</a> <a href="<?="javascript:open_win('".filename()."?l=".$l."&id=".$i['id']."&ds=1&do=vd&".s()."','vd');";?>" class=desc>[<?=$x[441]?>]</a>  
</Td></Tr>
</Table></Td></Tr></Table>
<br>
<?php 
	}}
	break;
	############################################
	# Vote Manager - End
	############################################
	############################################
	# Create Vote - Begin
	############################################
		case 'vc':
			if(isset($a) && ($a == 'create')) {
				if(!isset($question) || $question == "") sprintm($x[570]);
				if(!isset($num) || $num < 2 || !is_numeric($num)) sprintm($x[571]);
				if(!isset($lang) || count($lang) == 0) sprintm($x[572]);

				#db_query("UPDATE ".C_MYSQL_VOTES." set actual = '0'");
				$sql = "INSERT INTO ".C_MYSQL_VOTES." VALUES (Null, '".$num."', ";
				for ($i=1;$i<=20;$i++)
				{
					$sql .= "'0', ";
				}
				$sql .= "'".$stype."', '0')";
				db_query($sql);
				$last_id = db_fetch_array(db_query("SELECT LAST_INSERT_ID() AS last_id"));
				$last_id = $last_id['last_id'];
				$p=0;
				while(isset($lang[$p])) {
					$sql = "INSERT INTO ".C_MYSQL_VOTE_QUESTIONS." (id, question, lang";
					for ($i=1;$i<=$num;$i++)
					{   
						$sql .= ", qtext".$i;
					}
					$sql .= ") VALUES ('".$last_id."', '".cb($question)."', '".cb($lang[$p])."'";
					for ($i=1;$i<=$num;$i++)
					{
						$sql .= ", '".$ans[$i]."'";
					}
					$sql .= ")";
					db_query($sql) or die(db_error());
					$p++;}

			}
			else {
?>
<span class=head><?=$x[439]?></span><br><br>

        
<form action="<?=filename()?>?l=<?=$l?>&do=vc&<?=s()?>&a=create" method="post" name=form>
        <Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="600" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="600" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[442]?></Td>
        <Td><input type=text name=question class=input></Td>
        </Tr>
    <tr align="<?=C_ALIGN?>" class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[450]?><br><br><?=$x[448]?></td>
      <td><select multiple name="lang[]" class=input size="12">
      <?php 
      $existlang = explode(",",C_AVAILABLE_LANG);
      $p=0;while(isset($existlang[$p])) {
			?>
			<option value="<?=$existlang[$p]?>"><?=$reallangname[$existlang[$p]]?>
			<?php 
			$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
        <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
        <Td><?=$x[443]?></Td>
        <Td>
            <select name="num" class="input">
                <?php
                for ($i=1;$i<=20;$i++)
                {
                ?> 
                <option value="<?=$i?>"><?=$i?></option>
                <?php 
                }
                ?>
            </select>
        </Td>
        </Tr>
        <?php
        for ($i=1;$i<=20;$i++)
        {
        ?> 
        <Tr  align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
        <Td><?=$x[444]?> <?=$i?></Td>
        <Td><input type=text name=ans[<?=$i?>] class=input></Td>
        </Tr>
        <?php 
        }
        ?>
        <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
        <Td><?=$x[447]?></Td>
        <Td><select name="stype" class=select>
            <option selected value=1><?=$x[445]?>
            <option value=2><?=$x[446]?>
            </select>
        </Td>
        </Tr>
				<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
        <Td colspan=2><input type=submit name=submit class=input value="<?=$x[439]?>"></Td></Tr>
</Table></Td></Tr></Table></form>
<br>
<?php 
			}
			break;
			############################################
			# Create Vote - End
			############################################
			############################################
			# Edit Vote - Begin
			############################################
		case 've':
			if(isset($a) && ($a == 'edit')) {
				if(!isset($num) || $num < 2 || !is_numeric($num)) sprintm($x[571]);
				if(isset($id) && is_numeric($id))		{
					$sql = "UPDATE ".C_MYSQL_VOTES." SET num = '".$num."', ";
					for ($i=1;$i<=20;$i++)
					{
						if(is_numeric($q[$i])) { $sql .= "q".$i." = '".$q[$i]."', ";}
					}
					$sql .= "stype = '".$stype."' where id = '".$id."'";
					db_query($sql);
					sprintm($x[465]);
				}
			}
			else {
				$tmp=db_query("SELECT * FROM ".C_MYSQL_VOTES." where id = '".$id."'");
				while($i = db_fetch_array($tmp)) {
?>
<span class=head><?=$x[440]?></span><br><br>

        
<form action="<?=filename()?>?l=<?=$l?>&do=ve&<?=s()?>&a=edit&id=<?=$id?>" method="post" name=form>
        <Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="500" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="500" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[449]?></Td>
        <Td><?=$id?></Td>
        </Tr>
        <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
        <Td><?=$x[443]?></Td>
        <Td>
            <select name="num" class="input">
                <?php
                for ($j=1;$j<=20;$j++)
                {
                ?> 
                <option value="<?=$j?>"<?=(($j == $i['num'])?' selected="selected"':'')?>><?=$j?></option>
                <?php 
                }
                ?>
            </select>
        </Td>
        </Tr>
        <?php
        for ($k=1;$k<=20;$k++)
        {
        	$var='q'.$k;
        ?> 
        <Tr  align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
        <Td><?=$x[464]?> <?=$k?></Td>
        <Td><input type=text name=q[<?=$k?>] class=input value="<?=$i[$var]?>"></Td>
        </Tr>
        <?php 
        }
        ?>
        <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
        <Td><?=$x[447]?></Td>
        <Td><select name="stype" class=select>
						<?php $sel = ($i['stype'] == '1') ? 'selected' : '';?>
						<option <?=$sel?> value=1><?=$x[445]?>
						<?php $sel = ($i['stype'] == '2') ? 'selected' : '';?>
            <option <?=$sel?> value=2><?=$x[446]?>
            </select>
        </Td>
        </Tr>
				<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
        <Td colspan=2><input type=submit name=submit class=input value="<?=$x[440]?>"></Td></Tr>
</Table></Td></Tr></Table></form>
<br>
<?php 
				}
			}
			break;
			############################################
			# Edit Vote - End
			############################################
			############################################
			# Delete Vote - Begin
			############################################
		case 'vd':
			if(isset($a) && ($a == 'delete')) {
				if(isset($id) && is_numeric($id))		{
					db_query("DELETE FROM ".C_MYSQL_VOTES." where id = '".$id."'");
					db_query("DELETE FROM ".C_MYSQL_VOTE_QUESTIONS." where id = '".$id."'");
					db_query("DELETE FROM ".C_MYSQL_VOTE_IPS." where vid = '".$id."'");
					sprintm($x[467]);
				}
			}
			else {
				$tmp=db_query("SELECT * FROM ".C_MYSQL_VOTES." where id = '".$id."'");
				while($i = db_fetch_array($tmp)) {
?>
<span class=head><?=$x[441]?></span><br><br>

        
<form action="<?=filename()?>?l=<?=$l?>&do=vd&<?=s()?>&a=delete&id=<?=$id?>" method="post" name=form>
        <Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="500" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="500" class=mes>
<Tr align="center" bgcolor="<?=COLOR1?>"><Td><?=$x[466]?></Td></Tr>
<Tr align="center" bgcolor="<?=COLOR1?>">
        <Td colspan=2><input type=submit name=submit class=input value="<?=$x[441]?>"></Td></Tr>
</Table></Td></Tr></Table></form>
<br>
<?php 
				}
			}
			break;
			############################################
			# Delete Vote - End
			############################################
			############################################
			# Vote Language Edit - Begin
			############################################
		case 'vle':
			if(isset($a) && ($a == 'edit')) {
				if(isset($id) && is_numeric($id) && isset($lang)) {
					$sql = "UPDATE ".C_MYSQL_VOTE_QUESTIONS." set question = '".$question."'";
					for ($i=1;$i<=20;$i++)
					{
						$sql .= ", qtext".$i."='".$ans[$i]."'";
					}
					$sql .= " where id = '".$id."' and lang = '".cb($lang)."'";
					db_query($sql) or die(db_error());
					sprintm($x[463]);
				}}
				else {
?>
<span class=head><?=$x[462]?>: <?=$reallangname[$lang]?></span><br><br>
<?php
$tmp=db_query("SELECT * FROM ".C_MYSQL_VOTE_QUESTIONS." where id = '".$id."' and lang = '".cb($lang)."' limit 1");
while($i = db_fetch_array($tmp)) {

?>
        
<form action="<?=filename()?>?l=<?=$l?>&do=vle&<?=s()?>&a=edit&id=<?=$id?>&lang=<?=$lang?>" method="post" name=form>
        <Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="500" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="500" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[442]?></Td>
        <Td><input type=text name=question class=input value="<?=$i['question']?>"></Td>
        </Tr>
        <?php
        for ($k=1;$k<=20;$k++)
        {
        	$var='qtext'.$k;
        ?> 
        <Tr  align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
        <Td><?=$x[444]?> <?=$k?></Td>
        <Td><input type=text name=ans[<?=$k?>] class=input value="<?=$i[$var]?>"></Td>
        </Tr>
        <?php 
        }
        ?>
				<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
        <Td colspan=2><input type=submit name=submit class=input value="<?=$x[440]?>"></Td></Tr>
</Table></Td></Tr></Table></form>
<br>
<?php }
				}
				break;
				############################################
				# Vote Language Edit - End
				############################################
############################################
# Links Manager - Begin
############################################
		case 'lm':
?>
<script language="JavaScript">
<!--
function formCheck(form) {
	tinyMCE.triggerSave();
	if (form.question.value == "")
	{alert("<?=$x[330]?>");return false;}
	if (form.answer.value == "")
	{alert("<?=$x[331]?>");return false;}
	if (document.form.submit.action != "") {
		document.form.submit.disabled=1;}
}
// -->
</script>
<span class=head><?=$x[326]?></span><br><br>
<center>
<a href="<?="javascript:open_win('".filename()."?l=".$l."&ds=1&do=lc&".s()."','lc');";?>" class=desc>[<?=$x[327]?>]</a> <a href="<?="javascript:open_win('".filename()."?l=".$l."&ds=1&do=le&".s()."','le');";?>" class=desc>[<?=$x[328]?>]</a> 
<a href="<?="javascript:open_win('".filename()."?l=".$l."&ds=1&do=ld&".s()."','ld');";?>" class=desc>[<?=$x[329]?>]</a><br>
<a href="<?=filename()."?l=".$l."&do=lqe&".s()?>" class=desc>[<?=$x[332]?>]</a> 
<br>
</center>
<?php 
$tmp=db_query("SELECT * FROM ".C_MYSQL_LINKSCAT." order by id DESC");
$count=db_num_rows($tmp);
if($count != '0') {
	JSEditor();
?>
<form action="<?=filename()?>?l=<?=$l?>&do=lq&<?=s()?>" method="post" name=form OnSubmit="return formCheck(this)">
<center><span class=head><?=$x[334]?></span><br>
<center><br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="650" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="650" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[51]?>
</Td><Td>
<select name="cat" class=input>
<?php 
while($i=db_fetch_array($tmp)) {
	echo '<option value='.$i['id'].'>'.$i['catname'];
}
?>
</select>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[357]?></Td><Td><input type=text name="title" class=input></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[770]?></Td><Td><input type=text name="question" class=input></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[331]?>
</Td><Td>
<textarea name="answer" rows=12 style="width:100%"></textarea>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[289]?></Td><Td><input type=text name="keywords" class=input></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[772]?></Td><Td><input type=text name="uname" class=input></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[60]?></Td><Td><input type=text name="email" class=input></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[774]?></Td><Td><input type=text name="rec" class=input></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[90]?></Td><Td>
<select name=status class=minput>
<option value=2><?=$x[115]?>
<option value=7 selected><?=$x[114]?>

</select></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[336]?></Td><Td>
<select name=req class=sinput><?php
for($i=0;$i<=255;$i+=1) {	echo '<option>'.$i;}?></select></Td></Tr>

<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<input type="submit" value="<?=$x[334]?>" class=button name="submit">
</Td></Tr>
</Table></Td></Tr></Table>
<br>
<?php 
}
else {
	sprintm($x[335],2);
}
break;
############################################
# Links Manager - End
############################################
############################################
# Links add - Begin
############################################
		case 'lq':
			if(isset($cat) && is_numeric($cat) && !empty($question) && !empty($answer)) {
				$question=quot_del($question);
				$answer=quot_del($answer);
				$title=quot_del($title);
				$req=intval($req);
				$codegen = code_gen();
				$status = ($status == '2') ? '2' : '7';
				$question = checkurl($question);
				$rec = checkurl($rec);
				db_query("INSERT INTO ".C_MYSQL_LINKS." VALUES(null,'".$cat."','".$question."','".$title."','".$answer."','".cb($keywords)."','".cb($uname)."','".cb($email)."','".cb($rec)."',NOW(),'".$codegen."','".$status."','".intval($req)."')") or die(db_error());
				sprintm($x[337].'<br><br><a href="'.filename().'?l='.$l.'&do=lm&'.s().'">'.$x[43].'</a>');
			}
			else sprintm($w[164]);
			break;
			############################################
			# Links add - End
			############################################
			############################################
			# Links categories creating - Begin
			############################################
		case 'lc':
?>
<script language="JavaScript">
<!--
function formCheck(form) {
	if (form.cat.value == "")
	{alert("<?=$x[55]?>");return false;}
	if (document.form.submit.action != "") {
		document.form.submit.disabled=1;}
}
// -->
</script>
<form action="<?=filename()?>?l=<?=$l?>&ds=1&&do=lc&<?=s()?>" method="post" name=form OnSubmit="return formCheck(this)">
<center>
<span class=head><?=$x[327]?></span>
<br>
<?php 
if(isset($cat) && !empty($cat)) {
	$cat=quot_del($cat);
	db_query("INSERT INTO ".C_MYSQL_LINKSCAT." VALUES (Null,'".$cat."')");
	smes($x[57].'<br>'.$cat);
}
?>
<br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_SWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_SWIDTH?>" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[58]?>
</Td><Td>
<input type="text" name="cat" class=minput>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<input type="submit" value="<?=$x[43]?>" class=button name="submit">
</Td></Tr>
</Table></Td></Tr></Table>
</center>
<?php 
break;
############################################
# Links categories creating - End
############################################
############################################
# Links categories editing - Begin
############################################
		case 'le':
?>
<form action="<?=filename()?>?l=<?=$l?>&ds=1&do=lx&<?=s()?>" method="post" name=form>
<center>
<span class=head><?=$x[328]?></span>
<br>
<br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_SWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_SWIDTH?>" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[58]?>
</Td><Td>
<select name="cat" class=input>
<?php 
$tmp=db_query("SELECT * FROM ".C_MYSQL_LINKSCAT);while($i=db_fetch_array($tmp)) {
	echo '<option value='.$i['id'].'>'.$i['catname'];
}
?>
</select>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<input type="submit" value="<?=$x[60]?>" class=button>
</Td></Tr>
</Table></Td></Tr></Table>
</center>
<?php 
break;
############################################
# Links categories editing - End
############################################
############################################
# Links editing - Begin
############################################
		case 'lqe':


?>

<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLORH?>">
<Td colspan=2><form action="<?=filename()?>?l=<?=$l?>&ds=1&do=lqe&find=s&<?=s()?>" method="post" name=form>
<center>
<span class=head><?=$x[967]?></span></Td>
</Tr>
</Table></Td></Tr></Table>
<br>

<?php
$tmp_=db_query("SELECT * FROM ".C_MYSQL_LINKSCAT);$cat=$cat_id=$cat_by_id=array();
while($j=db_fetch_array($tmp_)) {$cat[]=$j['catname'];$cat_id[]=$j['id'];$cat_by_id[$j['id']]=$j['catname'];}
if(!isset($find))
{
?>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>ID#</Td><Td><input type=text name="id" class=input></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[58]?></Td>
<Td><?php
?>
<select name=category class=input>
<option value="0">--------
<?php $p=0;while(isset($cat[$p])) {?>
<option value="<?=$cat_id[$p]?>"><?=$cat[$p]?>
<?php $p++;}?>
</select></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[357]?></Td><Td><input type=text name="title" class=input></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[770]?>
</Td><Td>
<input type=text name="url" class="input">
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[677]?>
</Td><Td>
<input type=text name="descr" class="input">
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[289]?></Td><Td><input type=text name="keywords" class=input></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[772]?></Td><Td><input type=text name="uname" class=input></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[60]?></Td><Td><input type=text name="email" class=input></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[774]?></Td><Td><input type=text name="rec" class=input></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[90]?></Td><Td>
<select name=status class=minput>
<option value=0>------
<option value=1><?=$x[978]?>
<option value=2><?=$x[115]?>
<option value=7><?=$x[114]?>

</select></Td></Tr>

<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[336]?></Td><Td>
<select name=req[] class=inputm multiple style="width:50px"><?php
for($k=0;$k<=255;$k+=1) {	echo '<option>'.$k;}?>
</select></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<input type="submit" value="<?=$x[967]?>" class=button name="submit">
</Td></Tr>
</Table></Td></Tr></Table>
</form>
<br><center>
<?php
} else {
	if(!isset($from) || !is_numeric($from)) $from = 0;
	if(!isset($step) || !is_numeric($step)) $step = C_APAGE;
	$mtitle = (trim($title) == '') ? '' : " title LIKE '%".quot_del(utf8_decode($title))."%' AND";
	$mkeywords = (trim($keywords) == '') ? '' : " mkeywords LIKE '%".quot_del(utf8_decode($keywords))."%' AND";
	$mdescr = (trim($descr) == '') ? '' : " descr LIKE '%".quot_del(utf8_decode($descr))."%' AND";
	$muname = (trim($uname) == '') ? '' : " uname LIKE '%".quot_del(utf8_decode($uname))."%' AND";
	$memail = (trim($email) == '') ? '' : " email LIKE '%".quot_del(utf8_decode($email))."%' AND";
	$murl = (trim($url) == '') ? '' : " url LIKE '%".quot_del(utf8_decode($url))."%' AND";
	$mrec = (trim($rec) == '') ? '' : " rec LIKE '%".quot_del(utf8_decode($rec))."%' AND";
	$mcat = (!is_numeric($category) || $category < '1') ? '' : " cid = '".$category."' AND";
	$mid = (!is_numeric($id) || $id < '1') ? " id > '0'" : " id = '".$id."'";
	$mstatus = (!is_numeric($status) || $status < '1') ? '' : " status = '". (($status == '1') ? '0' : $status) . "' AND";

	if(isset($req) && !is_array($req) && $req != '') $req = explode(',',$req);
	if(!isset($req[0])) $mreq = "";
	else
	{ $p=0;$mreq=' (';while(isset($req[$p]))
	{
		$mreq .= "req = '".intval($req[$p])."' OR ";
		$p++;}
		$mreq = substr($mreq,0,-4).') AND';
	}
	#echo "SELECT count(*) as total FROM ".C_MYSQL_LINKS." WHERE ".$mtitle.$mkeywords.$mdescr.$muname.$memail.$murl.$mrec.$mcat.$mstatus.$mreq.$mid;
	$tmp=db_query("SELECT count(*) as total FROM ".C_MYSQL_LINKS." WHERE ".$mtitle.$mkeywords.$mdescr.$muname.$memail.$murl.$mrec.$mcat.$mstatus.$mreq.$mid) or die(db_error());



	$rows=db_fetch_array($tmp);
	$total=$rows['total'];
	if($total == 0) sprintm($w[110]);
	$tmp=db_query("SELECT * FROM ".C_MYSQL_LINKS." WHERE ".$mtitle.$mkeywords.$mdescr.$muname.$memail.$murl.$mrec.$mcat.$mstatus.$mreq.$mid." limit ".$from.",".$step) or die(db_error());

	$par_req = (isset($req) && $req != '') ? implode(',',$req) : '';

	$param='l='.$l.'&ds=1&do=lqe&find=s&title='.quot_del(utf8_encode($title)).'&keywords='.quot_del(utf8_encode($keywords)).'&descr='.quot_del(utf8_encode($descr)).'&uname='.quot_del(utf8_encode($uname)).'&email='.quot_del(utf8_encode($email)).'&rec='.quot_del(utf8_encode($rec)).'&url='.quot_del(utf8_encode($url)).'&category='.$category.'&id='.$id.'&status='.$status.'&req='.$par_req;
	$colspan=2;
	define('PAGES',pages($from,$step,$total,$param,$colspan));
	define('SEARCH_RESULTS',search_results($from,$step,$total,True));
?>
<span class="head"><?=SEARCH_RESULTS?></span><br>
<?php	
while($i=db_fetch_array($tmp)) {
	$urltmp = checkurl($i['url']);
	$show_url = ($urltmp == '-1') ? $i['url'] . '<br>(' . $x[968] . ')' : '<a href="'.C_URL.'/go.php?url='.$urltmp.'" target="_blank">'.$urltmp.'</a>';
	$urltmp = checkurl($i['reclink']);
	$show_rec = ($urltmp == '-1') ? $w[12] : '<a href="'.C_URL.'/go.php?url='.$urltmp.'" target="_blank">'.$urltmp.'</a>';
?>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>ID#</Td><Td><?=$i['id']?></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[58]?></Td><Td><?=$cat_by_id[$i['cid']]?></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[357]?></Td><Td><?=$i['title']?></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[770]?></Td><Td><?=$show_url?></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[677]?></Td><Td><?=$i['descr']?></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[289]?></Td><Td><?=$i['mkeywords']?></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[772]?></Td><Td><?=$i['uname']?></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[60]?></Td><Td><?=$i['email']?></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[774]?></Td><Td><?=$show_rec?></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[90]?></Td><Td><?php if($i['status'] == '7') echo '<span style="font-weight:bold;color:green">'.$x[114].'</span>'; elseif($i['status'] == '0') echo '<span style="font-weight:bold;color:red">'.$x[978].'</span>';else echo '<span style="font-weight:bold;color:red">'.$x[115].'</span>';?></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[336]?></Td><Td><?=$i['req']?></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<?php if($i['status'] == '7') {?>
<a href="<?="javascript:open_win('".filename()."?l=".$l."&ds=1&do=lqmu&".s()."&id=".$i['id']?>','link_status');" class=desc>[<?=$x[118]?>]</a> &nbsp; &nbsp;
<?php  } else {?>
<a href="<?="javascript:open_win('".filename()."?l=".$l."&ds=1&do=lqma&".s()."&id=".$i['id']?>','link_status');" class=desc>[<?=$x[112]?>]</a> &nbsp; &nbsp;
<?php }?>
<a href="<?=filename()?>?l=<?=$l?>&do=lqx&<?=s()?>&id=<?=$i['id']?>" class=desc>[<?=$x[60]?>]</a> &nbsp; &nbsp; <a href="<?="javascript:open_win('".filename()."?l=".$l."&ds=1&do=lqd&".s()."&id=".$i['id']?>','ld');" class=desc>[<?=$x[66]?>]</a>
</Td></Tr>
</Table></Td></Tr></Table><br>

<?php 
}
?>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
<?=PAGES?>
</Table></Td></Tr></Table><br>
<?php 
}
break;
############################################
# Links editing - End
############################################
############################################
# Links categories editing 2 - Begin
############################################
		case 'lx':
			if(isset($id) && !empty($cat) && is_numeric($id)) {
				$cat=quot_del($cat);
				db_query("UPDATE ".C_MYSQL_LINKSCAT." SET catname='".$cat."' WHERE id='".$id."'") or die(db_error());
				sprintm($x[61]);
			}
?>
<script language="JavaScript">
<!--
function formCheck(form) {
	if (form.cat.value == "")
	{alert("<?=$x[55]?>");return false;}
	if (document.form.submit.action != "") {
		document.form.submit.disabled=1;}
}
// -->
</script>
<form action="<?=filename()?>?l=<?=$l?>&ds=1&do=lx&<?=s()?>" method="post" name=form OnSubmit="return formCheck(this)">
<center>
<span class=head><?=$x[328]?></span>
<br>
<br>
<?php 
if(isset($cat) && !empty($cat) && is_numeric($cat)) {
?>
<br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_SWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_SWIDTH?>" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[58]?>
</Td><Td>
<?php 
$tmp=db_query("SELECT * FROM ".C_MYSQL_LINKSCAT." WHERE id='".$cat."'");
while($i=db_fetch_array($tmp)) {
	echo "<input name=\"id\" class=input type=hidden value=\"".$i['id']."\">
<input type=\"text\" name=\"cat\" class=\"input\" value=\"".$i['catname']."\">";
}
?>
</select>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<input type="submit" value="<?=$x[60]?>" class=button name="submit">
</Td></Tr>
</Table></Td></Tr></Table>
</center>
<?php 
}
break;
############################################
# Links categories editing 2 - End
############################################
############################################
# Links MakeActive - Begin
############################################
		case 'lqma':
			if(isset($id) && is_numeric($id)) {
				db_query("UPDATE ".C_MYSQL_LINKS." SET status = '7' WHERE id='".$id."'") or die(db_error());
				sprintm(template($x[969],array($id,$x[114])));
			}
			break;
			###########################################
			############################################
			# Links MakeActive - Begin
			############################################
		case 'lqmu':
			if(isset($id) && is_numeric($id)) {
				db_query("UPDATE ".C_MYSQL_LINKS." SET status = '2' WHERE id='".$id."'") or die(db_error());
				sprintm(template($x[969],array($id,$x[115])));
			}
			break;
			###########################################
			############################################
			# Links editing 2 - Begin
			############################################
		case 'lqx':
			if(isset($id) && !empty($cat) && is_numeric($id)) {
				$url=quot_del($url);$descr=quot_del($descr);
				$url = checkurl($url);
				$rec = checkurl($rec);
				$status = ($status == '2') ? '2' : '7';

				db_query("UPDATE ".C_MYSQL_LINKS." SET url='".$url."', descr='".$descr."', req = '".$req."', cid='".$cat."', title = '".$title."', mkeywords = '".cb($keywords)."', uname = '".cb($uname)."', email = '".cb($email)."', reclink = '".$rec."', status = '".$status."' WHERE id='".$id."'") or die(db_error());
				sprintm($x[337]);
			}
?>
<script language="JavaScript">
<!--
function formCheck(form) {
	tinyMCE.triggerSave();
	if (form.cat.value == "")
	{alert("<?=$x[55]?>");return false;}
	if (document.form.submit.action != "") {
		document.form.submit.disabled=1;}
}
// -->
</script>
<form action="<?=filename()?>?l=<?=$l?>&do=lqx&<?=s()?>" method="post" name=form OnSubmit="return formCheck(this)">
<center>
<span class=head><?=$x[332]?></span>
<br>
<br>
<?php 
if(isset($id) && !empty($id) && is_numeric($id)) {
	JSEditor();
?>
<br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="650" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="650" class=mes>
<?php 
$tmp=db_query("SELECT * FROM ".C_MYSQL_LINKS." WHERE id='".$id."'");
while($i=db_fetch_array($tmp)) {
?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>ID#</Td><Td><?=$i['id']?></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[58]?>
</Td><Td>
<select name=cat class=input>
<?php
$tmp2=db_query("SELECT * FROM ".C_MYSQL_LINKSCAT);
while($j=db_fetch_array($tmp2)) {
	echo ($i['cid'] == $j['id']) ? '<option selected value="'.$j['id'].'">'.$j['catname'] : '<option value="'.$j['id'].'">'.$j['catname'];
}
?>
</select>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[357]?></Td><Td><input type=text name="title" class=input value="<?=$i['title']?>"></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[770]?>
</Td><Td>
<input name="id" class=input type=hidden value="<?=$i['id']?>">
<input type=text name="url" class="input" value="<?=$i['url']?>">
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[331]?>
</Td><Td>
<textarea name="descr" class="input" rows=12 style="width:100%"><?=$i['descr']?></textarea>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[289]?></Td><Td><input type=text name="keywords" class=input value="<?=$i['mkeywords']?>"></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[772]?></Td><Td><input type=text name="uname" class=input value="<?=$i['uname']?>"></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[60]?></Td><Td><input type=text name="email" class=input value="<?=$i['email']?>"></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[774]?></Td><Td><input type=text name="rec" class=input value="<?=$i['reclink']?>"></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[90]?></Td><Td>
<select name=status class=minput>
<option value=2 <?php if($i['status'] == '2') echo 'selected';?>><?=$x[115]?>
<option value=7 <?php if($i['status'] == '7') echo 'selected';?>><?=$x[114]?>

</select></Td></Tr>

<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[336]?>
</Td><Td>
<select name=req class=sinput>
<?php
for($k=0;$k<=255;$k+=1) {
	echo ($k == $i['req']) ? '<option selected>'.$k : '<option>'.$k;
}
?>
</select>
<?php
}
?>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<input type="submit" value="<?=$x[60]?>" class=button name="submit">
</Td></Tr>
</Table></Td></Tr></Table>
</center>
<?php 
}
break;
############################################
# Links editing 2 - End
############################################
############################################
# Links delete - Begin
############################################
		case 'ld':
			if(isset($cat) && is_numeric($cat)) {
				db_query("DELETE FROM ".C_MYSQL_LINKS." WHERE cid='".$cat."'") or die(db_error());
				db_query("DELETE FROM ".C_MYSQL_LINKSCAT." WHERE id='".$cat."'") or die(db_error());
				sprintm($x[63]);
			}
?>
<form action="<?=filename()?>?l=<?=$l?>&ds=1&do=ld&<?=s()?>" method="post" name=form>
<center>
<span class=head><?=$x[329]?></span>
<br>
<span class=desc><?=$x[65]?></span>
<br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_SWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_SWIDTH?>" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[58]?>
</Td><Td>
<select name="cat" class=input>
<?php 
$tmp=db_query("SELECT * FROM ".C_MYSQL_LINKSCAT);while($i=db_fetch_array($tmp)) {
	echo '<option value='.$i['id'].'>'.$i['catname'];
}
?>
</select>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<input type="submit" value="<?=$x[66]?>" class=button>
</Td></Tr>
</Table></Td></Tr></Table>
</center>
<?php 
break;
############################################
# Links delete - End
############################################
############################################
# Links delete - Begin
############################################
		case 'lqd':
			if(isset($id) && is_numeric($id)) {
				db_query("DELETE FROM ".C_MYSQL_LINKS." WHERE id='".$id."'") or die(db_error());
				sprintm($x[63]);
			}
			break;
			############################################
			# Links delete - End
			############################################

			############################################
			# Maintenance - Remove thumbnails - Begin
			############################################
		case 'mtrt':
			if(!isset($act)) $act = 0;
			switch($act) {
				case '1':
					$files=array();
					GetThumbnails(C_PATH."/members/uploads/");
					RemoveFiles($files);

					sprintm(template($x[831],array(count($files))));
					break;
				default:
?>
<span class=head><?=$x[828]?></span><br><br>
<form action="<?=filename()?>?l=<?=$l?>&do=mtrt&act=1&<?=s()?>" method="post" name=form>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="600" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="600" class=mes>
<Tr align="center" bgcolor="<?=COLOR1?>"><Td colspan=2 align=center>
<input type="submit" value="<?=$x[828]?>" class=button name="submit">
</Td></Tr>
</Table></Td></Tr></Table>
<br>
<?php 
break;
			}
			break;
			############################################
			# Maintenance - Remove thumbnails - End
			############################################
			############################################
			# Maintenance - Cleaning error photos - Begin
			############################################
		case 'mtcp':
			if(!isset($act)) $act = 0;
			switch($act) {
				case '1':
					$tmp=db_query("SELECT id,files FROM ".C_MYSQL_MEMBERS_MAIN." WHERE files != ''");
					$users=0;$pictures=0;
					while($i=db_fetch_array($tmp))
					{
						$pics=explode(',',$i['files']);
						$p=0;$fails=array();
						while(isset($pics[$p]))
						{
							if(!file_exists(C_PATH.'/members/uploads/'.id2path($i['id']).$pics[$p]))
							{
								$fails[]=$pics[$p];
							}
							$p++;
						}
						if(isset($fails[0]))
						{
							$users++;
							$p=0;$str=$i['files'];
							while(isset($fails[$p]))
							{
								$str=DelPicElement($str,$fails[$p]);
								$pictures++;
								$p++;
							}
							@db_query("UPDATE ".C_MYSQL_MEMBERS_MAIN." SET files = '".$str."' WHERE id = '".$i['id']."'");

						}
					}

					sprintm(template($x[830],array($users,$pictures)));
					break;
				default:
?>
<span class=head><?=$x[829]?></span><br><br>
<form action="<?=filename()?>?l=<?=$l?>&do=mtcp&act=1&<?=s()?>" method="post" name=form>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="600" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="600" class=mes>
<Tr align="center" bgcolor="<?=COLOR1?>"><Td colspan=2 align=center>
<input type="submit" value="<?=$x[829]?>" class=button name="submit">
</Td></Tr>
</Table></Td></Tr></Table>
<br>
<?php 
break;
			}
			break;
			############################################
			# Maintenance - Cleaning error photos - End
			############################################
			############################################
			# Maintenance - Cleaning cache - Begin
			############################################
		case 'mtcc':
			if(!isset($act)) $act = 0;
			switch($act) {
				case '1':
					@CleanTemporaryFiles(C_PATH.'/'.C_TMPDIR);
					sprintm($x[837]);
					break;
				default:
?>
<span class=head><?=$x[836]?></span><br><br>
<form action="<?=filename()?>?l=<?=$l?>&do=mtcc&act=1&<?=s()?>" method="post" name=form>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="600" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="600" class=mes>
<Tr align="center" bgcolor="<?=COLOR1?>"><Td colspan=2 align=center>
<input type="submit" value="<?=$x[836]?>" class=button name="submit">
</Td></Tr>
</Table></Td></Tr></Table>
<br>
<?php 
break;
			}
			break;
			############################################
			# Maintenance - Cleaning cache - End
			############################################
			############################################
			# Maintenance - Cleaning inactive users - Begin
			############################################
		case 'mtcu':
			if(!isset($act)) $act = 0;
			switch($act) {
				case '1':
					if(!is_numeric($number)) sprintm($w[1] . '1');
					$mpriv = (isset($priv) && $priv == 'on') ? " and req = '0'" : '';
					$remstat = (C_REMOVE_ALLOW == '1') ? '3' : '8';
					$tmp=db_query("SELECT t1.id, t2.email  FROM ".C_MYSQL_MEMBERS_MAIN." as t1 LEFT JOIN ".C_MYSQL_MEMBERS_SEC." as t2 ON t1.id = t2.id WHERE editdate < DATE_SUB(NOW(), INTERVAL ".$number." DAY)".$mpriv) or die(db_error());
					$str='id IN(';$cnt=0;
					while($i=db_fetch_array($tmp))
					{
						$str.=$i['id'].",";$cnt++;

						$save_log = empty_log_array();
                                                $save_log['user'] = get_info('adminID');
                        $save_log['time'] = date('Y-m-d H:i:s');
                        $save_log['id'] = $i['id'];
                        $save_log['email'] = $i['email'];
                        $save_log['action'] = 'Státuszmódosítás: '.$remstat;
                        log_csv(array($save_log));
					}
					$str = substr($str,0,-1).')';
					if($cnt != 0)
					{
						db_query("UPDATE ".C_MYSQL_MEMBERS_MAIN." set status = '".$remstat."' WHERE ".$str) or die(db_error());
						db_query("UPDATE ".C_MYSQL_MEMBERS_EXT." set status = '".$remstat."' WHERE ".$str) or die(db_error());
						db_query("UPDATE ".C_MYSQL_MEMBERS_SEC." set status = '".$remstat."' WHERE ".$str) or die(db_error());
						# :) Concat, Where MYid IN (...
						@db_query("UPDATE ".C_MYSQL_ADDRESSBOOK." set status='".$remstat."' WHERE my".$str);
					}
					sprintm(template($x[412],array($cnt)).'<br><br>'.$x[398]);
					break;
				default:
?>
<span class=head><?=$x[408]?></span><br><br>
<form action="<?=filename()?>?l=<?=$l?>&do=mtcu&act=1&<?=s()?>" method="post" name=form>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="600" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="600" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=60%>
<?=$x[77]?>
</td>
<td>
<?php 
$tmp=db_query("SELECT count(*) as total FROM ".C_MYSQL_MEMBERS_MAIN." WHERE status >= '7'");
$rows=db_fetch_array($tmp);
$total=$rows['total'];
echo $total;
?>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[409]?>
</Td><Td>
<input type=text name="number" class=sinput> <?=$x[394]?>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[410]?>
</Td><Td>
<input type=checkbox name=priv checked>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<input type="submit" value="<?=$x[408]?>" class=button name="submit" onclick="return confirm('<?=$x[411]?>')">
</Td></Tr>
</Table></Td></Tr></Table>
<br>
<?php 
break;
			}
			break;
			############################################
			# MUIEdit
			############################################
		case 'muiedit':
			$file=C_PATH.'/languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'.php';
			$filew='languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'.php';
			if(!(file_exists(C_PATH.'/languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'.php'))) sprintm(template($x[476],array($filew))); if(!(is_writable(C_PATH.'/languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'.php'))) sprintm(template($x[477],array($filew)));

			function qdel($ss)
			{
				$ss = str_replace("'", "", $ss);
				return $ss;
			}
			function RemoveFromArray($arr,$elem)
			{
				Return array_merge(array_slice($arr, 0, $elem),array_slice ($arr, $elem+1));
			}
			function ChangeLang($file,$arr)
			{
				$cnt = file($file);
				$fp = fopen($file,"w");
				flock($fp, LOCK_EX);$pcnt=0;
				for ($i=0;$i<count($cnt);$i++)
				{
					if ((substr($cnt[$i],0,1) == "'") && (substr($cnt[$i],0,2) != "',") && (isset($arr[$pcnt])))
					{
						$cnt[$i] = "'".qdel($arr[$pcnt])."', //".$pcnt."\r\n";
						$pcnt++;
					}
					elseif ( ( substr($cnt[$i],0,2) != ');' ) && ($pcnt != 0) && isset($arr[$pcnt]))
					{
						$cnt=RemoveFromArray($cnt,$i);
						$i--;
					}
					elseif(substr($cnt[$i],0,2) == ');') $pcnt = 0;
					elseif(((substr($cnt[$i],0,1) == "'") && (substr($cnt[$i],0,2) != "',") && (!isset($arr[$pcnt]))))
					$pcnt++;

				}
				fwrite($fp, implode("",$cnt));
				fflush($fp);
				flock($fp, LOCK_UN);
				fclose($fp);
			}


			ChangeLang($file,$c_edit_phrase);
?>
<?=$x[478]?>
<?php
$tot=count($w);
for($k=0;$k<$tot;$k+=50) {
?>
<a href="<?=C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=muiedit&from='.$k?>" target="main"><?=$k?>-<?=$k+49?></a> 
<?php }
sprintm($x[479]);

break;



		case 'muieditmails':
			$file=C_PATH.'/languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'.php';
			$filew='languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'.php';
			$file2=C_PATH.'/languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'a.php';
			$file2w='languages/'.C_EDIT_LANG.'/'.C_EDIT_LANG.'a.php';
			if(!(file_exists($file))) sprintm(template($x[476],array($filew)));
			if(!(is_writable($file))) sprintm(template($x[477],array($filew)));
			if(!(file_exists($file2))) sprintm(template($x[476],array($file2w)));
			if(!(is_writable($file2))) sprintm(template($x[477],array($file2w)));

			ksort($c_edit_phrase);reset($c_edit_phrase);
			ksort($c_xedit_phrase);reset($c_xedit_phrase);
			//print_r($c_edit_phrase);die();
			function qdel($ss)
			{
				$ss = str_replace("'", "", $ss);
				return $ss;
			}
			function RemoveFromArray($arr,$elem)
			{
				Return array_merge(array_slice($arr, 0, $elem),array_slice ($arr, $elem+1));
			}
			function ChangeLang($file,$arr)
			{
				$ContRemove = false;
				$cnt = file($file);
				$fp = fopen($file,"w");
				flock($fp, LOCK_EX);$pcnt=0;
				for ($i=0;$i<count($cnt);$i++)
				{
					if(substr($cnt[$i],0,1) == "'" && substr($cnt[$i],0,2) != "'," && $ContRemove === True) $ContRemove = False;

					if ((substr($cnt[$i],0,1) == "'") && (substr($cnt[$i],0,2) != "',") && (isset($arr[$pcnt])))
					{
						$cnt[$i] = "'".qdel($arr[$pcnt])."', //".$pcnt."\r\n";
						$pcnt++;
						if(substr($cnt[$i+1],0,1) != "'" || substr($cnt[$i+1],0,2) == "',") $ContRemove = True;
						else  $ContRemove = False;
					}
					elseif ((substr($cnt[$i],0,1) != "'" || substr($cnt[$i],0,2) == "',") && $ContRemove === True)
					{
						$cnt=RemoveFromArray($cnt,$i);
						$i--;
					}
					elseif ( ( substr($cnt[$i],0,2) != ');' ) && ($pcnt != 0) && isset($arr[$pcnt]))
					{
						$cnt=RemoveFromArray($cnt,$i);
						$i--;
					}
					elseif(substr($cnt[$i],0,2) == ');') $pcnt = 0;
					elseif(((substr($cnt[$i],0,1) == "'") && (substr($cnt[$i],0,2) != "',") && (!isset($arr[$pcnt]))))
					$pcnt++;

				}
				fwrite($fp, implode("",$cnt));
				fflush($fp);
				flock($fp, LOCK_UN);
				fclose($fp);
			}




			ChangeLang($file,$c_edit_phrase);
			ChangeLang($file2,$c_xedit_phrase);
?>
<?=$x[478]?>
<?php
$tot=count($w);
for($k=0;$k<$tot;$k+=50) {
?>
<a href="<?=C_URL.'/'.ADM_DIR.'/index.php?do=opt&type=muiedit&from='.$k?>" target="main"><?=$k?>-<?=$k+49?></a> 
<?php }
sprintm($x[479]);
break;
	case 'phpinfo':
			if(@GetC_URL() == 'azdg.com' || @GetC_URL() == 'test.com') sprintm($x[574]);
			
			phpinfo(61);
	break;		
  		############################################
			# Send message to user - Begin
			############################################
		case 'smes':
			if(isset($id) && is_numeric($id)) {
				if(isset($a) && ($a == "s")){
					if (empty($subject) || empty($message)) sprintm($w[164]);
					if (!isset($id) || !is_numeric($id)) sprintm($w[1].'1', 1);

					$confirm = ((isset($confirm)) && ($confirm == "on")) ? "2" : "1";
					$subject = cb($subject);

					$message = cb($message);
					$mess = preg_replace('/\<br(\s*)?\/?\>/i', "%0D%0A", $message);

					db_query("INSERT INTO ".C_MYSQL_MESSAGES." VALUES (Null,'0','".$id."','".$subject."',NOW(),'".ip2int(ip())."','".$confirm."', '0')") or die(db_error());
					$lastid = db_insert_id();		
					save_message($lastid, $message);
					AddUserToAddressBook($id, 1);

					sprintm($w[165].$javascript, 1);
					break;

				}
				else
				{
					$name = isset($username) ? cb($username) : $id;
					$url = "<a href='".C_URL."/".ADM_DIR."/index.php?do=vp&ds=1&id=".$id."' target=_blank>".$name."</a>";
?>
<form method="post" name=form OnSubmit="return formCheck(this)" action="<?=filename()?>">
<input type="hidden" name="do" value="smes">
<input type="hidden" name="a" value="s">
<input type="hidden" name="id" value="<?=$id?>">
<center><BR>
<BR>
<span class=head><?=$w[514]?> <?=$url?></span>
<br><br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_SWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_SWIDTH?>" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[168]?>
</Td><Td>
<input type="hidden" name="subject" value="<?=$w['862']?>"> <b><?=$w['862']?></b>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[169]?>
</Td><Td>
<textarea name="message" class=textarea rows=12></textarea>
</Td></Tr>
 <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td colspan=2>
    <input type="checkbox" name="confirm"> <?=$w[203]?>
    </td>
  </tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<input type="submit" value="<?=$w[170]?>" class=button name="submit">
</form>
</Td></Tr>
</Table></Td></Tr></Table>
</center>

<script language="JavaScript">
<!--

function formCheck(form) {
	if ((form.subject.value == "") || (form.message.value == ""))
	{alert("<?=$w[164]?>");return false;}
	if (document.form.submit.action != "") {
		document.form.submit.disabled=1;}
}
// -->
</script>
<?php 
				}}
				break;
				############################################
				# Send message to user - End
				############################################
		case 'tedit':
			$path=C_PATH.'/templates/';

			if(isset($c_temp_edit) && is_dir($path.$c_temp_edit)) define('C_TEMP_EDIT',$c_temp_edit);
			else define('C_TEMP_EDIT','');
			?>
			<span class=head><?=$x[718]?>: <?=C_TEMP_EDIT?></span><br><br>
<form action="<?=filename()?>?l=<?=$l?>&do=tedit&action=do&<?=s()?>" method="post">
<center><br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="600" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="600" class=mes>

    <tr class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[122]?></td>
      <td><select name="c_temp_edit" class=input>
      <?php 
      $handle=opendir(C_PATH.'/templates/');
      $fnm = 0;
      while (false!==($file = readdir($handle))) {
      	if ($file != '.' & $file != '..') {
      		echo (C_TEMP_EDIT == $file) ? ' <option selected>'.$file :  ' <option>'.$file;
      	}
      }
      closedir($handle);
      ?>      
      </select></td>
    </tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<input type="submit" value="<?=$w[263]?>" class=button name="submit">
</Td></Tr>
</Table></Td></Tr></Table><br></form>

<?php  if(C_TEMP_EDIT == '') { ?>
<br>
<br>
<form action="?<?=s()?>" method="post">
<input type="hidden" name="a" value="s">
<input type="hidden" name="do" value="opt">
<?php table_header($x[120])?>
		<?=show_checkbox($x[801],C_TEMPLATE_BACKUP,"c_template_backup")?>
<?php table_footer()?>
    <br>
    <input type="submit" value="<?=$x[251]?>" class=button>
    </form>

<?php }?>
<?php  if(C_TEMP_EDIT != '') { 
	function clr()
	{
		global $color;
		$color = ($color == COLOR4) ? COLOR3 : COLOR4;
		return $color;
	}
	$color=0;
	function dir_tree($df,$level=0)
	{
		global $color,$self,$x;

		$df=str_replace("//","/",$df);
		$dirs=$files=$backup=array();
		if ($dir=opendir($df)) {
			while (($file=readdir($dir))!==false) {
				if ($file=="." || $file=="..") continue;
				if (is_dir("$df/$file"))  {
					$dirs[]=$file;
				}
				elseif(substr($file,-4) == '.php' || substr($file,-4) == '.txt') {
					$files[]=$file;
					$backup[] = (is_file($df.'/'.$file.'.bak')) ?	True : False;
				}
			}
		}
		closedir($dir);

		sort($dirs);
		sort($files);
		if(count($files))
		{
			echo "<tr class=mes bgcolor=".COLORH."><td colspan=3>".
			"<a href=?do=tedit&c_temp_edit=".C_TEMP_EDIT."&dir=".urlencode($df).">".$df."</a></td></tr>";

			if (count($dirs) || count($files)) {
				for ($i=0; $i<count($files); $i++) {
					if($backup[$i]) echo "<tr class=mes bgcolor=".clr()."><td width=15 align=center> * </td>";
					else echo "<tr class=mes bgcolor=".clr()."><td width=15>&nbsp;</td>";
					echo "<td class=all width=80%>";
					echo "<a href=?do=tedit&c_temp_edit=".C_TEMP_EDIT."&dir=".urlencode($df)."&f=".urlencode($files[$i]).">".$files[$i]."</a>";
					if($backup[$i]) echo "</td><td width=120><a href='?do=tedit&c_temp_edit=".C_TEMP_EDIT."&dir=".urlencode($df)."&f=".urlencode($files[$i])."&restore=1'>".$x[724]."</a></td></tr>";
					else echo "</td><td width=120>&nbsp;</td></tr>";
				}
				for ($i=0; $i<count($dirs); $i++) {
					dir_tree($df."/".$dirs[$i],$level+1);
				}
			}
		}
	}
?>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="600" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="600" class=mes>

<?php

if(isset($dir) && is_dir($dir) && isset($f)) {
	if (!@realpath($dir.'/'.$f) || !file_exists($dir.'/'.$f)) sprintm($x[721]);
	if (!is_file($dir.'/'.$f) || !$fi=@fopen($dir.'/'.$f,"r")) sprintm($x[719]);
	if (!is_file($dir.'/'.$f) || !$fi=@fopen($dir.'/'.$f,"r+")) sprintm($x[720]);

	if(isset($restore))
	{
		if(is_file($dir.'/'.$f.'.bak') && is_file($dir.'/'.$f))
		{
			echo "<Tr bgcolor=".COLORH."><Td><span class=head><a href=?do=tedit&c_temp_edit=".C_TEMP_EDIT."&dir=".urlencode($dir).">".$dir.'/</a>'.$f."<br></td></tr>";
			echo "</Table></Td></Tr></Table>";
			if(!copy($dir.'/'.$f.'.bak',$dir.'/'.$f)) sprintm($x[725]);
			else
			{
				unlink ($dir.'/'.$f.'.bak');
				sprintm(template($x[726],array($f)));
			}
			die();
		}
	}
	echo "<Tr bgcolor=".COLORH."><Td><span class=head><a href=?do=tedit&c_temp_edit=".C_TEMP_EDIT."&dir=".urlencode($dir).">".$dir.'/</a>'.$f."<br>";
	if(isset($do2) && $do2 == 'submit' && isset($text))
	{
		if(!is_file($dir.'/'.$f.'.bak') && C_TEMPLATE_BACKUP) if(!copy($dir.'/'.$f,$dir.'/'.$f.'.bak')) sprintm($x[723]);
		if (!$fi=@fopen($dir.'/'.$f,"w+")) sprintm($x[720]);
		fwrite($fi,$text);
		fclose($fi);
		sprintm($x[722]);
	} else {
		echo "<form action=? method=post>
   <input type=hidden name=do value=tedit>
   <input type=hidden name=c_temp_edit value=\"".C_TEMP_EDIT."\">
   <input type=hidden name=do2 value=submit>
   <input type=hidden name=dir value=\"".htmlspecialchars($dir)."\">
   <input type=hidden name=f value=\"".htmlspecialchars($f)."\">
   <textarea name=text cols=70 rows=20 style='width: 100%;'>".
		htmlspecialchars(fread($fi,filesize($dir.'/'.$f)))."</textarea><p>
   <input type=submit value='".$w[263]."'>
   </form>";
	}
	echo "</td></tr>";
} elseif(isset($dir) && is_dir($dir)) dir_tree($dir);
else dir_tree(C_PATH.'/templates/'.C_TEMP_EDIT);
      ?>      
</Table></Td></Tr></Table>
<?php }

break;
############################################
# News Management - Add News - Begin
############################################
		case 'nm':
?>

<?php JSEditor(); ?>
<script language="JavaScript">
<!--
function formCheck(form) {
	tinyMCE.triggerSave();
	if ((form.subject.value == "") || (form.news.value == "") || !((form.priority.value >= 0) && (form.priority.value <= 255)))
	{alert("<?=$x[108]?>");return false;}
	if (document.form.submit.action != "") {
		document.form.submit.disabled=1;}
}
// -->
</script>
<?php menu('ne',$x[105]);?>
<form method="post" name=form OnSubmit="return formCheck(this)" action="<?=filename()?>">
<input type="hidden" name="do" value="nc">
<center>
<span class=head><?=$x[104]?></span>
<br><br>
<b><i><sub><?=$x[103]?></sub></i></b>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_BWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_BWIDTH?>" class=mes>
    <tr align="<?=C_ALIGN?>" class=desc bgcolor="<?=colors()?>">
      <td width=30%><?=$x[450]?><br><br><?=$x[448]?></td>
      <td><select multiple name="lang[]" class=input size="12">
      <?php 
      $existlang = explode(",",C_AVAILABLE_LANG);
      $p=0;while(isset($existlang[$p])) {
			?>
			<option value="<?=$existlang[$p]?>"><?=$reallangname[$existlang[$p]]?>
			<?php 
			$p++;
      }
      ?>      
      </select>
      </td>
    </tr>

<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[101]?>
</Td><Td>
<input type="text" name="subject" class=input>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[102]?>
</Td><Td width=70%>
<textarea name="news" style="width: 100%"></textarea>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[107]?>
</Td><Td>
<input type="text" name="priority" class=input value="0">
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<input type="submit" value="<?=$x[104]?>" class=button name="submit">
</form>
</Td></Tr>
</Table></Td></Tr></Table>
</center>
<?php 
break;
############################################
# News Management - Add News - End
############################################
############################################
# News Management - News Edit - Begin
############################################
		case 'ne':
			if((isset($do2))&&($do2 == 'make') && (isset($id) && (is_numeric($id)))) {
				$subject=quot_del($subject);$news=quot_del($news);
				if(($subject == "") || ($news == "") || (!is_numeric($priority))) mes($x[108]);
				else {
					db_query("UPDATE ".C_MYSQL_NEWS." SET subject='".$subject."', news='".$news."', date=NOW(), priority='".$priority."' WHERE id='".$id."'");
					mes($x[119]);
				}
			}
			if(isset($id) && (is_numeric($id))) {
				$tmp=db_query("SELECT * FROM ".C_MYSQL_NEWS." WHERE id='".$id."'");
				JSEditor();
				while($i=db_fetch_array($tmp)) {
?>
<script language="JavaScript">
<!--
function formCheck(form) {
	tinyMCE.triggerSave();
	if ((form.subject.value == "") || (form.news.value == "") || !((form.priority.value >= 0) && (form.priority.value <= 255)))
	{alert("<?=$x[108]?>");return false;}
	if (document.form.submit.action != "") {
		document.form.submit.disabled=1;}
}
// -->
</script>
<form method="post" name=form OnSubmit="return formCheck(this)" action="<?=filename()?>">
<input type="hidden" name="do" value="ne">
<input type="hidden" name="do2" value="make">
<input type="hidden" name="id" value="<?=$id?>">
<center>
<span class=head><?=$x[105]?></span>
<br><br>
<b><i><sub><?=$x[103]?></sub></i></b>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_BWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_BWIDTH?>" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<?php
echo template($x[473],array($reallangname[$i['lang']])).' ';
if($i['type'] == '0') {
	$type = '<span style="color:red;font-weight:bold;">'.$x[115].'</span>';
	$make_link='<a href="javascript:open_win(\''.C_URL.'/'.ADM_DIR.'/index.php?do=na&ds=1&id='.$id.'\',\'ma\');" class=desc>['.$x[112].']</a>';
} else {
	$type = '<span style="color:#45C824;font-weight:bold;">'.$x[114].'</span>';
	$make_link='<a href="javascript:open_win(\''.C_URL.'/'.ADM_DIR.'/index.php?do=ni&ds=1&id='.$id.'\',\'mi\');" class=desc>['.$x[118].']</a>';
}
echo $x[116].' '.$type.' &nbsp; &nbsp; &nbsp; '.$make_link.' &nbsp; <a href="'.C_URL.'/index.php?l='.$i['lang'].'" class=desc target="_blank">['.$x[111].']</a> &nbsp;';
?>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[101]?>
</Td><Td>
<input type="text" name="subject" class=input value="<?=quot_add($i['subject'])?>">
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[102]?>
</Td><Td>
<textarea name="news" class=textarea rows=12 style="width:100%""><?=quot_add($i['news'])?></textarea>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[107]?>
</Td><Td>
<input type="text" name="priority" class=input value="<?=quot_add($i['priority'])?>">
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<input type="submit" value="<?=$x[105]?>" class=button name="submit">
</form>
</Td></Tr>
</Table></Td></Tr></Table>
</center>
<?php 
				}
			} else {
				if(!isset($from) || !is_numeric($from)) $from =  0;
				$step = C_NEWS_NUM;

				$tmp=db_query("SELECT * FROM ".C_MYSQL_NEWS." order by priority DESC, date DESC limit ".$from.",".$step);
				$tmp2=db_query("SELECT count(*) as total FROM ".C_MYSQL_NEWS);
				$trows = db_fetch_array($tmp2);
				$count = $trows['total'];
				$param='l='.$l.'&do=ne';
				$colspan=1;
				$str=pages($from,$step,$count,$param,$colspan);
?><center>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTHC?>" class=mes>
<Tr><Td bgcolor="<?=C_TBCOLOR?>"></Td></Tr>
<Tr align="center" bgcolor="<?=COLORH?>"><Td>
<?=$w[344]?>
</Td></Tr>
<Tr><Td bgcolor="<?=C_TBCOLOR?>"></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td align=center>
<?php
if(db_num_rows($tmp) == '0') smes($w[345]);
else {
	while($i=db_fetch_array($tmp)) {
?>
<table width="100%" class=tr>
<tr class=mes bgcolor="<?=COLORH?>" align=center>
<td width="75%"><?=quot_add($i['subject'])?></td><td><?=mysql2data($i['date'])?></td></tr>
<tr><td colspan="2" class=mes><?=quot_add($i['news'])?></td></tr>
<tr class=mes bgcolor="<?=COLORH?>" align=center><td colspan="2">
<?php
echo template($x[473],array($reallangname[$i['lang']])).' ';
if($i['type'] == '0') {
	$type = '<span style="color:red;font-weight:bold;">'.$x[115].'</span>';
	$make_link='<br><a href="javascript:open_win(\''.C_URL.'/'.ADM_DIR.'/index.php?do=na&ds=1&id='.$i['id'].'\',\'ma\');" class=desc>['.$x[112].']</a>';
} else {
	$type = '<span style="color:#45C824;font-weight:bold;">'.$x[114].'</span>';
	$make_link='<br><a href="javascript:open_win(\''.C_URL.'/'.ADM_DIR.'/index.php?do=ni&ds=1&id='.$i['id'].'\',\'mi\');" class=desc>['.$x[118].']</a>';
}
echo $x[116].' '.$type.' &nbsp; '.$make_link.' &nbsp;';
menu('ne&id='.$i['id'],$x[105]);
echo '<a href="javascript:open_win(\''.C_URL.'/'.ADM_DIR.'/index.php?do=nd&ds=1&id='.$i['id'].'\',\'nd\');" class=desc>['.$x[106].']</a>';
?> 
</td></tr>
</table><br>
<?php }?>
</Td></Tr>
<?php }?>
<?=$str?>
<Tr><Td bgcolor="<?=C_TBCOLOR?>"></Td></Tr>
</Table>
<?php
			}
			break;
			############################################
			# News Management - News Edit - End
			############################################
			############################################
			# News Management - Begin
			############################################
		case 'nc':
			if(($subject == "") || ($news == "") || (!is_numeric($priority))) sprintm($x[108],2);
			$subject=quot_del($subject);
			$news=quot_del($news);
			$tmp=db_query("SELECT count(id) as total FROM ".C_MYSQL_NEWS." WHERE news='".$news."'");
			$trows = db_fetch_array($tmp);
			$count = $trows['total'];
			if($count != '0') sprintm($x[109],2);

			if(!isset($lang) || count($lang) == 0) sprintm($x[572]);
			$p=0;
			while(isset($lang[$p])) {
				db_query("INSERT INTO ".C_MYSQL_NEWS." VALUES (Null, '".cb($lang[$p])."', '".$subject."', '".$news."', NOW(), '".$priority."', '0')");
				$p++;
			}

			$last_id = db_fetch_array(db_query("SELECT LAST_INSERT_ID() AS last_id"));
			$last_id = $last_id['last_id'];
?>
<?php menu('ne&id='.$last_id,$x[105]);?> &nbsp; 
<a href="<?=C_URL?>/index.php?l=default" class=desc target="_blank">[<?=$x[111]?>]</a> &nbsp; 
<a href="javascript:open_win('<?=C_URL?>/<?=ADM_DIR?>/index.php?do=na&ds=1&id=<?=$last_id?>','na');" class=desc>[<?=$x[112]?>]</a>
<?php
sprintm($x[110]);
break;
############################################
# News Management - End
############################################
############################################
# News Management - Make Active - Begin
############################################
		case 'na':
			if(isset($id) && (is_numeric($id))) {
				db_query("UPDATE ".C_MYSQL_NEWS." SET type='7' WHERE id='".$id."'") or die(db_error());
				sprintm($x[113]);
			} else sprintm($w[1]);
			break;
			############################################
			# News Management - Make Active - End
			############################################
			############################################
			# News Management - Make Inactive - Begin
			############################################
		case 'ni':
			if(isset($id) && (is_numeric($id))) {
				db_query("UPDATE ".C_MYSQL_NEWS." SET type='0' WHERE id='".$id."'") or die(db_error());
				sprintm($x[117]);
			} else sprintm($w[1]);
			break;
			############################################
			# News Management - Make Inactive - End
			############################################
			############################################
			# News Management - Remove - Begin
			############################################
		case 'nd':
			if(isset($id) && (is_numeric($id))) {
				db_query("DELETE FROM ".C_MYSQL_NEWS." WHERE id='".$id."'") or die(db_error());
				sprintm($x[472]);
			} else sprintm($w[1]);
			break;
			############################################
			# News Management - Remove - End
			############################################
			############################################
			# Testimonials Management - Begin
			############################################
		case 'tm':
			if((isset($do2)) && (isset($id) && (is_numeric($id)))) {
				switch($do2)
				{
					case 'ma':
						db_query("UPDATE ".C_MYSQL_TESTIMONIALS." SET status='7' WHERE id='".$id."'");
						sprintm($x[689]);
						break;
					case 'mi':
						db_query("UPDATE ".C_MYSQL_TESTIMONIALS." SET status='3' WHERE id='".$id."'");
						sprintm($x[690]);
						break;
					case 'td':
						db_query("DELETE FROM ".C_MYSQL_TESTIMONIALS." WHERE id='".$id."'");
						sprintm($x[694]);
						break;
					case 'make':
						if(!isset($id) || !is_numeric($uid)) sprintm($w[185]);
						$message=cb(trim($message));
						$reply=cb(trim($reply));
						$ins_rep = ($reply == '') ? "" : ",reply='".$reply."'";
						db_query("UPDATE ".C_MYSQL_TESTIMONIALS." SET message='".$message."'".$ins_rep.",rdate=NOW(),uid='".$uid."' WHERE id='".$id."'");
						sprintm($x[696]);
						break;
					case 'te':
						$tmp=db_query("SELECT * FROM ".C_MYSQL_TESTIMONIALS." WHERE id='".$id."'");
						while($i=db_fetch_array($tmp)) {
?>
<script language="JavaScript">
<!--
function formCheck(form) {
	if ((form.subject.value == "") || (form.news.value == "") || !((form.priority.value >= 0) && (form.priority.value <= 255)))
	{alert("<?=$x[108]?>");return false;}
	if (document.form.submit.action != "") {
		document.form.submit.disabled=1;}
}
// -->
</script>
<script language="JavaScript">
<!--
function ins(prop,type)
{
	var input = document.forms[0].message;
	var mysel;var text;
	if (window.getSelection) mysel = window.getSelection();
	else if (document.selection) mysel = document.selection.createRange().text;
	else if (document.getSelection)	mysel = document.getSelection();
	b = (type == 0) ? '['+prop+']' : prop;
	e = (type == 0) ? '[/'+prop+']' : '';
	if (input.createTextRange)
	{
		var text;
		input.focus(input.caretPos);
		input.caretPos = document.selection.createRange().duplicate();
		input.caretPos.text = (input.caretPos.text.length>0) ? b + input.caretPos.text + e : b + mysel + e;
	}
	else input.value += b + mysel + e;
}
function ins2(prop,type)
{
	var input = document.forms[0].reply;
	var mysel;var text;
	if (window.getSelection) mysel = window.getSelection();
	else if (document.selection) mysel = document.selection.createRange().text;
	else if (document.getSelection)	mysel = document.getSelection();
	b = (type == 0) ? '['+prop+']' : prop;
	e = (type == 0) ? '[/'+prop+']' : '';
	if (input.createTextRange)
	{
		var text;
		input.focus(input.caretPos);
		input.caretPos = document.selection.createRange().duplicate();
		input.caretPos.text = (input.caretPos.text.length>0) ? b + input.caretPos.text + e : b + mysel + e;
	}
	else input.value += b + mysel + e;
}
// -->
</script>
<?php
include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';
?>
<form method="post" name=form OnSubmit="return formCheck(this)" action="<?=filename()?>">
<input type="hidden" name="do" value="tm">
<input type="hidden" name="do2" value="make">
<input type="hidden" name="id" value="<?=$id?>">
<center>
<span class=head><?=$x[692]?></span>
<br><br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[126]?>
</Td><Td>
<input name="uid" class=input value="<?=$i['uid']?>">
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[692]?><BR>
 <Table Border=0 width="160"><Tr align="left" bgcolor="<?=COLORH?>"><Td>
<?php $p=0;foreach ($smiles as $k=>$v){$p++;?>
<a href="javascript:ins(' <?=$k?> ',1)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/forum/smiles/<?=$v?>" border=0 /></a>
<?php if(C_SMILES_NUMBER <= $p){?>
<br><a href="javascript:open_win('<?=C_URL?>/forum/smiles_show.php','_blank')"><?=$w[720]?></a><br>
<?php break;}?>
<?php }?><br>
<a href="javascript:ins('B',0)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Bold.gif" border=0 /></a>
<a href="javascript:ins('I',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Italic.gif" border=0 /></a>
<a href="javascript:ins('U',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Underline.gif" border=0 /></a>
<a href="javascript:ins('S',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Strike.gif" border=0 /></a>
<a href="javascript:ins('QUOTE',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Quote.gif" border=0 /></a>  </td></tr></table>
</Td><Td>
<textarea name="message" class=textarea rows=12><?=tb($i['message'])?></textarea>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[695]?><BR>
 <Table Border=0 width="160"><Tr align="left" bgcolor="<?=COLORH?>"><Td>
<?php $p=0;foreach ($smiles as $k=>$v){$p++;?>
<a href="javascript:ins2(' <?=$k?> ',1)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/forum/smiles/<?=$v?>" border=0 /></a>
<?php if(C_SMILES_NUMBER <= $p){?>
<br><a href="javascript:open_win('<?=C_URL?>/forum/smiles_show.php','_blank')"><?=$w[720]?></a><br>
<?php break;}?>
<?php }?><br>
<a href="javascript:ins2('B',0)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Bold.gif" border=0 /></a>
<a href="javascript:ins2('I',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Italic.gif" border=0 /></a>
<a href="javascript:ins2('U',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Underline.gif" border=0 /></a>
<a href="javascript:ins2('S',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Strike.gif" border=0 /></a>
<a href="javascript:ins2('QUOTE',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Quote.gif" border=0 /></a>  </td></tr></table>
</Td><Td>
<textarea name="reply" class=textarea rows=12><?=tb($i['reply'])?></textarea>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<input type="submit" value="<?=$x[692]?>" class=button name="submit">
</form>
</Td></Tr>
</Table></Td></Tr></Table>
</center>
<?php 
						}
						break;
				}
			} else {
				if(!isset($from) || !is_numeric($from)) $from =  0;
				$step = C_TST_CNT;

				#				$tmp=db_query("SELECT * FROM ".C_MYSQL_TESTIMONIALS." order by id DESC limit ".$from.",".$step);
				$tmp2=db_query("SELECT count(*) as total FROM ".C_MYSQL_TESTIMONIALS);
				$tmp=db_query("SELECT t1.*,t2.*,t1.id as id, t2.id as tid FROM ".C_MYSQL_TESTIMONIALS." as t2,".C_MYSQL_MEMBERS_MAIN." as t1 WHERE t1.id = t2.uid order by t2.id desc limit ".$from.",".C_TST_CNT);
				$ids=$message=$mdate=$reply=$rdate=$names=$photoinfo=$genders=$sgenders=$fnames=$lnames=$bodytypes=$orientations=$purposes=$ages=$countries=$cities=$heights=$weights=$horoscopes=$rates=$usertypes=$editdates=$regdates=$pictures=$colors=array();$str='';$color='';
				include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';
				$trows = db_fetch_array($tmp2);
				$count = $trows['total'];
				$param='l='.$l.'&do=tm';
				$colspan=1;
				$str=pages($from,$step,$count,$param,$colspan);
?><center>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
<Tr><Td bgcolor="<?=C_TBCOLOR?>"></Td></Tr>
<Tr align="center" bgcolor="<?=COLORH?>"><Td>
<?=$w[633]?>
</Td></Tr>
<Tr><Td bgcolor="<?=C_TBCOLOR?>"></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td align=center>
<?php
if(db_num_rows($tmp) == '0') smes($w[637]);
else {
	while($i=db_fetch_array($tmp))
	{
		$pictures[] = SearchPhotoResultAdmin(True,$i['files'],$i['id'],$i['gender']);
		$color = ($color == COLOR4) ? COLOR3 : COLOR4;
		$colors[] = $color;
		$ids[]=$i['id'];
		$photoinfo[]=($i['files'] == '') ? $w[532] : template($w[533],array(PhotosNum($i['files'])));
		$names[] = username($i['id'],$i['username'],$i['fname'],$i['lname']);
?>
<table width="100%" class=tr>
<tr class=mes bgcolor="<?=$color?>" align=center>
<td width="75%" valign=top><?=mysql2data($i['mdate'])?><br><br>
<?=AzDGCode($i['message']);?>
</td><td valign=top><?=SearchPhotoResult(True,$i['files'],$i['id'],$i['gender'])?><br><a class=link2 href="<?=C_URL?>/<?=ADM_DIR?>/index.php?do=vp&ds=1&id=<?=$i['id']?>" target=_blank><?=username($i['id'],$i['username'],$i['fname'],$i['lname'])?></a></td></tr>
<?php if($i['reply'] != '') {?>
<tr><td colspan="2" class=mes  bgcolor="<?=COLORH?>"></td></tr>
<tr class=mes bgcolor="<?=$color?>" align=center>
<td width="75%" valign=top><?=mysql2data($i['rdate'])?><br><br>
<?=AzDGCode($i['reply']);?>
</td><td valign=top><?=$w[519]?></a></td></tr>
<?php }?>
<tr class=mes bgcolor="<?=COLORH?>" align=center><td colspan="2">
<?php
if($i['status'] != '7') {
	$type = '<span style="color:red;font-weight:bold;">'.$x[115].'</span>';
	$make_link='<br><a href="javascript:open_win(\''.C_URL.'/'.ADM_DIR.'/index.php?do=tm&do2=ma&ds=1&id='.$i['tid'].'\',\'tma\');" class=desc>['.$x[112].']</a>';
} else {
	$type = '<span style="color:#45C824;font-weight:bold;">'.$x[114].'</span>';
	$make_link='<br><a href="javascript:open_win(\''.C_URL.'/'.ADM_DIR.'/index.php?do=tm&do2=mi&ds=1&id='.$i['tid'].'\',\'tmi\');" class=desc>['.$x[118].']</a>';
}
echo $x[691].' '.$type.' &nbsp; '.$make_link.' &nbsp;';
echo '<a href="javascript:open_win(\''.C_URL.'/'.ADM_DIR.'/index.php?do=tm&do2=te&ds=1&id='.$i['tid'].'\',\'te\');" class=desc>['.$x[692].']</a>';
echo '<a href="javascript:open_win(\''.C_URL.'/'.ADM_DIR.'/index.php?do=tm&do2=td&ds=1&id='.$i['tid'].'\',\'td\');" class=desc>['.$x[693].']</a>';
?> 
</td></tr>
<tr><td colspan="2" class=mes  bgcolor="<?=COLORH?>"></td></tr>
<tr><td colspan="2" class=mes  bgcolor="<?=COLORH?>"></td></tr>
</table><br>
<?php }?>
</Td></Tr>
<?php }?>
<?=$str?>
<Tr><Td bgcolor="<?=C_TBCOLOR?>"></Td></Tr>
</Table>
<?php
			}
			break;
			############################################
			# Testimonials Management - End
			############################################

			############################################
			# Comments Management - Begin
			############################################
		case 'comm':
			if(isset($act) && isset($cid) && is_numeric($cid))
			{
				switch($act)
				{
					case 'delete':
						@db_query("DELETE FROM ".C_MYSQL_COMMENTS." WHERE id = '".$cid."'");
						sprintm($w[495]);
						break;
					case 'reply_do':
						$message=cb($message);
						@db_query("UPDATE ".C_MYSQL_COMMENTS." SET reply = '".$message."', rdate = NOW() WHERE id = '".$cid."'");
						sprintm($w[496]);
						break;
					case 'edit_do':
						$message=cb($message);$subject=cb($subject);
						@db_query("UPDATE ".C_MYSQL_COMMENTS." SET comment = '".$message."', subject = '".$subject."' WHERE id = '".$cid."'");
						sprintm($w[757]);
						break;
					case 'reply':
						$tmp = db_query("SELECT *,".CorrectDate("cdate")." FROM ".C_MYSQL_COMMENTS." WHERE id = '".$cid."'") or die(db_error());
						$thiscnt = db_num_rows($tmp);
						if ($thiscnt == '0') sprintm($w[1]);
						$cfromid = $cid = $cdate = $cnames = $cages = $cgenders = $cphotos = $csubject = $ccomment = $creply = '';
						include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';
						while ($ic = db_fetch_array($tmp))
						{
							$cfromid = $ic['fromid'];
							$cid = $ic['id'];
							$csubject = $ic['subject'];
							$ccomment = AzDGCode($ic['comment']);
							$creply = tb($ic['reply']);
							$cdate = mysql2data($ic['cdate'], 0, 1);
							if($ic['fromid'] == '0')
							{
								$cphotos = SearchPhotoResult(IsAccess(C_COMMENT_PHOTO),'','0','0');
								$cnames = $w[354];
								$cages = '';
								$cgenders = '';
							}
							else
							{
								$result = db_query("SELECT * FROM ".C_MYSQL_MEMBERS_MAIN." WHERE id = '".$cfromid."'") or die(db_error());
								while ($i = db_fetch_array($result))
								{

									$cphotos = SearchPhotoResult(IsAccess(C_COMMENT_PHOTO),$i['files'],$i['id'],$i['gender']);
									$cnames = username($i['id'], $i['username'], $i['fname'], $i['lname']);
									$cages = abs(mysql2data($i['birthday'], 1));
									$cgenders = $wg[$i['gender']];
								}
							}
						}

						define('MESSAGE',$w[169]);
						define('REPLY',$w[234]);
			?>
<script language="JavaScript">
<!--
function formCheck(form) {
	if (document.form.submit.action != "") {
		document.form.submit.disabled=1;}
}
// -->
</script>
<script language="JavaScript">
<!--
function IS(smile)
{
	if (document.form.message.createTextRange && document.form.message.caretPos)
	{
		var caretPos = document.form.message.caretPos;
		caretPos.smile = caretPos.smile.charAt(caretPos.smile.length - 1)==' '? smile+' ':smile;
		document.form.message.focus();
	}
	else
	{
		document.form.message.value += smile;
		document.form.message.focus();
	}
}
var agt = navigator.userAgent.toLowerCase();
var is_major = parseInt(navigator.appVersion);
var is_minor = parseFloat(navigator.appVersion);
var is_ie = ((agt.indexOf('msie') != -1) && (agt.indexOf('opera') == -1));
var is_nav = ((agt.indexOf('mozilla')!=-1) && (agt.indexOf('spoofer')==-1) && (agt.indexOf('compatible') == -1) && (agt.indexOf('opera')==-1) && (agt.indexOf('webtv')==-1) && (agt.indexOf('hotjava')==-1));
var is_moz = 0;
var is_win = ((agt.indexOf('win')!=-1) || (agt.indexOf('16bit') != -1));
var is_mac = (agt.indexOf('mac')!=-1);
var SelectedText = '';
function AddCode(open, close)
{
	var txtarea = document.form.message;
	if ((is_major >= 4) && is_ie && is_win)
	{
		SelectedText = document.selection.createRange().text;
		if (SelectedText=='')
		{
			txtarea.value += open + close;
			txtarea.focus();
			return;
		}
		document.selection.createRange().text = open + SelectedText + close;
		txtarea.focus();
		return;
	}else if (txtarea.selectionEnd && (txtarea.selectionEnd - txtarea.selectionStart > 0)){
		mozWrap(txtarea, open, close);
		return;
	}else{
		txtarea.value += open + close;
		txtarea.focus();
	}
	storeCaret(txtarea);
}

// -->
</script>

<form action="index.php" method="post" name=form OnSubmit="return formCheck(this)">
<input class=input type=hidden name="l" value="<?=LANGUAGE?>">
<input class=input type=hidden name="do" value="comm">
<input class=input type=hidden name="cid" value="<?=$cid?>">
<input class=input type=hidden name="act" value="reply_do">
<center><span class=head><?=$w[234]?></span>
<br>
	  <Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>">
  <Tr>
    <Td>
    <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
            <TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3">
  <Tr align="center" bgcolor="<?=COLOR3?>" class=mes>
              <TD width=70%><?=$csubject?></TD>
              <TD><?=$cdate?></TD>
             </TR>
  <Tr align="center" bgcolor="<?=COLOR3?>" class=mes>
              <TD><?=$ccomment?></TD>
              <TD>
							<?=$cphotos?><br>
							<?=$cnames?>
							</TD>
             </TR>
</table></td></tr></table></TD></TR></TABLE><br><br>		
	  <Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>">
  <Tr>
    <Td>
    <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
            <TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3">
  <Tr align="center" bgcolor="<?=COLOR3?>" class=mes>		
    <Td>
    <div align="center" width=200><?php $p=0;foreach ($smiles as $k=>$v){$p++;?>
<a href="javascript:IS(' <?=$k?> ',1)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/forum/smiles/<?=$v?>" border=0 /></a>
<?php if(C_SMILES_NUMBER <= $p){?>
<br><a href="javascript:open_win('<?=C_URL?>/forum/smiles_show.php','_blank')"><?=$w[720]?></a><br>
<?php break;}?>
<?php }?><br>
<a href="javascript:AddCode('[B]','[/B]')"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Bold.gif" border=0 /></a>
<a href="javascript:AddCode('[I]','[/I]')"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Italic.gif" border=0 /></a>
<a href="javascript:AddCode('[U]','[/U]')"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Underline.gif" border=0 /></a>
<a href="javascript:AddCode('[QUOTE]','[/QUOTE]')"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Quote.gif" border=0 /></a></div>
    </td><td align=left><?=MESSAGE?><br>
		
    <textarea class=textarea name=message cols=40 rows=15>
		<?=$creply?>
		</textarea>
    </td>
  </tr>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td colspan="2">
    <input class=input1 type=submit value="<?=REPLY?>" name="submit">
    </td>
  </tr>
  </table>
  </td>
</tr>
</table>
     </TD>
    </TR></form>
   </TABLE>			
<?php
break;
					case 'edit':
						$tmp = db_query("SELECT *,".CorrectDate("cdate")." FROM ".C_MYSQL_COMMENTS." WHERE id = '".$cid."'") or die(db_error());
						$thiscnt = db_num_rows($tmp);
						if ($thiscnt == '0') sprintm($w[1]);
						$cfromid = $cid = $cdate = $cnames = $cages = $cgenders = $cphotos = $csubject = $ccomment = $creply = '';
						include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';
						while ($ic = db_fetch_array($tmp))
						{
							$cfromid = $ic['fromid'];
							$cid = $ic['id'];
							$csubject = $ic['subject'];
							$ccomment = tb($ic['comment']);
							$creply = tb($ic['reply']);
							$cdate = mysql2data($ic['cdate'], 0, 1);
							if($ic['fromid'] == '0')
							{
								$cphotos = SearchPhotoResult(IsAccess(C_COMMENT_PHOTO),'','0','0');
								$cnames = $w[354];
								$cages = '';
								$cgenders = '';
							}
							else
							{
								$result = db_query("SELECT * FROM ".C_MYSQL_MEMBERS_MAIN." WHERE id = '".$cfromid."'") or die(db_error());
								while ($i = db_fetch_array($result))
								{

									$cphotos = SearchPhotoResult(IsAccess(C_COMMENT_PHOTO),$i['files'],$i['id'],$i['gender']);
									$cnames = username($i['id'], $i['username'], $i['fname'], $i['lname']);
									$cages = abs(mysql2data($i['birthday'], 1));
									$cgenders = $wg[$i['gender']];
								}
							}
						}

						define('SUBJECT',$w[168]);
						define('MESSAGE',$w[169]);
						define('REPLY',$w[234]);
			?>
<script language="JavaScript">
<!--
function formCheck(form) {
	if (document.form.submit.action != "") {
		document.form.submit.disabled=1;}
}
// -->
</script>
<script language="JavaScript">
<!--
function ins(prop,type)
{
	var input = document.form.message;
	var mysel;var text;
	if (window.getSelection) mysel = window.getSelection();
	else if (document.selection) mysel = document.selection.createRange().text;
	else if (document.getSelection)	mysel = document.getSelection();
	b = (type == 0) ? '['+prop+']' : prop;
	e = (type == 0) ? '[/'+prop+']' : '';
	if (input.createTextRange)
	{
		var text;
		input.focus(input.caretPos);
		input.caretPos = document.selection.createRange().duplicate();
		input.caretPos.text = (input.caretPos.text.length>0) ? b + input.caretPos.text + e : b + mysel + e;
	}
	else input.value += b + mysel + e;
}

// -->
</script>

<form action="index.php" method="post" name=form OnSubmit="return formCheck(this)">
<input class=input type=hidden name="l" value="<?=LANGUAGE?>">
<input class=input type=hidden name="do" value="comm">
<input class=input type=hidden name="cid" value="<?=$cid?>">
<input class=input type=hidden name="act" value="edit_do">
<center><span class=head><?=$x[60]?></span>
<br>

	  <Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>">
  <Tr>
    <Td>
    <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
            <TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3">
  <Tr align="center" bgcolor="<?=COLOR3?>" class=mes>		
    <Td width="40%">
        <?=SUBJECT?>
        </td>
    <Td>
        <input type=text name=subject class=input value="<?=$csubject?>">
        </td>
  </tr>    
  
  <Tr align="center" bgcolor="<?=COLOR3?>" class=mes>		
    <Td><?=MESSAGE?>
    </td>
    <Td>
<Table Border=0 width="240"><Tr align="left" bgcolor="<?=COLORH?>"><Td>
 <?php $p=0;foreach ($smiles as $k=>$v){$p++;?>
<a href="javascript:ins(' <?=$k?> ',1)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/forum/smiles/<?=$v?>" border=0 /></a>
<?php if(C_SMILES_NUMBER <= $p){?>
<br><a href="javascript:open_win('<?=C_URL?>/forum/smiles_show.php','_blank')"><?=$w[720]?></a><br>
<?php break;}?>
<?php }?><br>
<a href="javascript:ins('B',0)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Bold.gif" border=0 /></a>
<a href="javascript:ins('I',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Italic.gif" border=0 /></a>
<a href="javascript:ins('U',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Underline.gif" border=0 /></a>
<a href="javascript:ins('S',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Strike.gif" border=0 /></a>
<a href="javascript:ins('QUOTE',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Quote.gif" border=0 /></a>  </td></tr></table>
		
    <textarea class=textarea name=message cols=40 rows=15>
		<?=$ccomment?>
		</textarea>
    </td>
  </tr>
  <Tr align="right" bgcolor="<?=COLOR1?>">
    <Td colspan="2">
    <input class=input1 type=submit value="<?=$x[60]?>" name="submit">
    </td>
  </tr>
  </table>
  </td>
</tr>
</table>
     </TD>
    </TR></form>
   </TABLE>			
<?php
break;
				}

				die;
			}
			$tmp2=db_query("SELECT count(*) as total FROM ".C_MYSQL_COMMENTS);
			$trows = db_fetch_array($tmp2);
			$count = $trows['total'];
			if($count == 0) sprintm($w[491]);

			$tuid = $cid = $toid = $fromid = $subject = $comment = $reply = $cdate = $rdate = $status = $pictures = $colors = $photoinfo = $names = array();
			if (!isset($step) || !is_numeric($step)) $step = 10;
			if (!isset($from) || !is_numeric($from)) $from = 0;
			$tmp=db_query("SELECT * FROM ".C_MYSQL_COMMENTS." order by id desc limit ".$from.",".$step);
			include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';
			while($i=db_fetch_array($tmp))
			{
				$cid[] = $i['id'];
				$toid[] = intval($i['toid']);
				$fromid[] = intval($i['fromid']);
				$subject[] = $i['subject'];
				$comment[] = AzDGCode($i['comment']);
				$reply[] = AzDGCode($i['reply']);
				$cdate[] = $i['cdate'];
				$rdate[] = $i['rdate'];
				$status[] = $i['status'];
				$color = ($color == COLOR4) ? COLOR3 : COLOR4;
				$colors[] = $color;

				if($i['toid'] > 0) $tuid[] = intval($i['toid']);
				if($i['fromid'] > 0) $tuid[] = intval($i['fromid']);
			}
			$uid = array_unique ($tuid);
			$utmp=db_query("SELECT * FROM ".C_MYSQL_MEMBERS_MAIN." WHERE id IN (".implode(',',$uid).")");

			$pictures['0'] = $photoinfo['0'] = $names['0'] = '';
			while($us=db_fetch_array($utmp))
			{
				$pictures[intval($us['id'])] = SearchPhotoResultAdmin(True,$us['files'],$us['id'],$us['gender']);
				$photoinfo[intval($us['id'])]=($us['files'] == '') ? $w[532] : template($w[533],array(PhotosNum($us['files'])));
				$names[intval($us['id'])] = username($us['id'],$us['username'],$us['fname'],$us['lname']);
			}

			$param='l='.$l.'&do=comm';
			$colspan=1;
			$str=pages($from,$step,$count,$param,$colspan);
?>
<center>
              <table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="30" colspan="2" class=head align=center><?=$x[844]?></td>
                  </tr>
              <?php $p=0;while(isset($cid[$p])) {?>
                              <tr bgcolor="<?=$colors[$p]?>">
                  <td height="20"  class=mes colspan=2><b><?=template($x[845],array('<a href="index.php?do=vp&id='.$toid[$p].'" target="_blank">'.$names[$toid[$p]].'</a>'))?></b></td>
                </tr>
                <tr bgcolor="<?=$colors[$p]?>">
                  <td height="20"  class=mes><?=$subject[$p]?></td>
                  <td align="center" class=mes><?=$cdate[$p]?></td>
                </tr>
                <tr bgcolor="<?=$colors[$p]?>">
                  <td width="309" valign="top"  class=mes><?=$comment[$p]?></td>
                  <td align="right" class=mes><?=$pictures[$fromid[$p]]?>
                      <?php if($fromid[$p]){?><a  href="index.php?l=<?=LANGUAGE?>&do=vp&id=<?=$fromid[$p]?>" target="_blank"><?=$names[$fromid[$p]]?></a><?php } else {?><?=$w[354]?><?php }?>
                </td></tr>
                  <?php if(!empty($reply[$p])){?>
                <tr bgcolor="<?=$colors[$p]?>">
                  <td height="30"  class=mes colspan=2><b><?=template($x[846],array('<a href="index.php?do=vp&id='.$toid[$p].'" target="_blank">'.$names[$toid[$p]].'</a>'))?></b></td>
                </tr>                  
                <tr bgcolor="<?=$colors[$p]?>">
                  <td height="30"  class=mes><?=$reply[$p]?></td>
                  <td align="center" class=mes><?=$rdate[$p]?></td>
                </tr>
                <?php }?>
                <tr align="center">
                  <td height="20" colspan="2">
                  <a class=link2 href="javascript:open_win('index.php?l=<?=LANGUAGE?>&do=comm&act=edit&cid=<?=$cid[$p]?>','cedit');">[<?=$x[60]?>]</a>
&nbsp;                  <a class=link2 href="javascript:open_win('index.php?l=<?=LANGUAGE?>&do=comm&act=reply&cid=<?=$cid[$p]?>','creply');">[<?=$w[234]?>]</a>
&nbsp;
      						<a class=link2 href="javascript:open_win('index.php?l=<?=LANGUAGE?>&do=comm&act=delete&cid=<?=$cid[$p]?>','cremove');">[<?=$w[297]?>]</a>
      						</td></tr>
                <tr bgcolor="<?=COLORH?>"><td height="10" colspan=2> </td></tr>
                <?php $p++;}$p=0; ?>              
              </table>

              <?php if($str){?>
              <br>
              <Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>">
                <Tr>
                  <Td>
                    <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
                      <?=$str?>
                  </table></td>
                </tr>
              </table>
              <?php 

              }
              ?>
              
<?php              
break;
############################################
# Comments Management - End
############################################

############################################
# Gallery - Begin
############################################
		case 'ncpg':
			if(isset($a) && $a == 'a') {

				reset($_GET);reset($_POST);
				$ids=$tmparr=$narr=array();
				while(list($names,$values) = each($_POST))
				{
					if(substr($names,0,6) == 'hidden')
					{
						$tmparr[]=$names;
						$narr=explode('_',$names);
						$ids[] = $narr[1];
					}
				}
				$ids=array_unique($ids);sort($ids);

				$rslt = db_query("SELECT t1.*,t2.files FROM ".C_MYSQL_NEW_PHOTOS." as t1,".C_MYSQL_MEMBERS_MAIN." as t2 WHERE t1.uid IN (".cb(implode(',',$ids)).") and t1.uid = t2.id and t1.photonum != ''") or die(db_error());

				while ($j = db_fetch_array($rslt))
				{
					$p=0;reset($tmparr);$narr=array();$orig_files=$j['files'];
					while(isset($tmparr[$p]))
					{
						$narr=explode('_',$tmparr[$p]);$remarr=array();
						if($j['uid'] == $narr[1])
						{
							$j['photonum'] = DelPicElement($j['photonum'],$narr[2]);
							$tmp='image_'.intval($narr[1]).'_'.intval($narr[2]);
							if(isset($$tmp)) { // We need to remove photo and thumbnail
								unlink (C_PATH.'/members/uploads/'.PicPath(intval($narr[2]),$j['uid']));
								unlink (C_PATH.'/members/uploads/'.TPicPath(intval($narr[2]),$j['uid']));
							} else $j['files'] = AddPicElement($j['files'],$narr[2]);
						}
						$p++;
					}
					if(trim($j['photonum']) == '') db_query("DELETE FROM ".C_MYSQL_NEW_PHOTOS." WHERE uid = '".$j['uid']."'");
					else db_query("UPDATE ".C_MYSQL_NEW_PHOTOS." SET photonum = '".$j['photonum']."' WHERE uid = '".$j['uid']."'");
					if($orig_files != $j['files'])
					{
						db_query("UPDATE ".C_MYSQL_MEMBERS_MAIN." SET files = '".$j['files']."' WHERE id = '".$j['uid']."'");
					}
				}

				GoToURL(C_URL.'/'.ADM_DIR.'/index.php?do=ncpg&'.s());
				die;

			}

			$rsltt = db_query("SELECT count(*) as cp FROM ".C_MYSQL_NEW_PHOTOS." WHERE photonum != ''") or die(db_error());
			$tmp=db_fetch_array($rsltt);
			if($tmp['cp'] == '0') sprintm($x[525]);
?>
<form action="<?=filename()?>?l=<?=$l?>&ds=1&do=ncpg&<?=s()?>" method="post" name=form>
<input type="hidden" name=a value=a>
<center>
<BR>
<span class=head><?=$x[520]?></span>
<br><BR>

<?=template($x[522],array($tmp['cp']))?><BR>

<br>
<?=$x[523]?><BR>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
<Tr bgcolor="<?=COLOR1?>">
<?php
$limit = C_GALLERY_COLUMN * C_GALLERY_ROW;
$rslt = db_query("SELECT t1.fname,t1.lname,t1.username,t2.* FROM ".C_MYSQL_NEW_PHOTOS." as t2,".C_MYSQL_MEMBERS_MAIN." as t1 WHERE t1.id = t2.uid AND t2.photonum != '' order by t1.editdate limit ".$limit) or die(db_error());
$depr=$ufile=$ufile=$uid=$fid=$usrname=array();
$fnm = 0;
while ($j = db_fetch_array($rslt))
{
	if($j['photonum'] != "") $depr= explode(',',$j['photonum']);
	$p=0;$path = id2path($j['uid']);
	while (isset($depr[$p]))
	{
		$ufile[$fnm] = ShowPic(PicPath($depr[$p],$j['uid']));
		$bfile[$fnm] = ShowPic(PicPath($depr[$p],$j['uid']),True);
		$uid[$fnm] = $j['uid'];
		$fid[$fnm] = $depr[$p];
		$usrname[$fnm]=username($j['uid'],$j['username'],$j['fname'],$j['lname']);
		$fnm++;$p++;
		if($fnm == $limit) continue 2;
	}
}

$str = '';
$p = 0;
$rp = 0;
$color = '';
while (list ($p, $val) = each ($ufile))
{
	$rp++;
	$tr = $rp / C_GALLERY_COLUMN;
	if (is_int($tr) && $rp != '1')
	{
		$bcode = '<Td valign=top>';
		if ($rp != $fnm)
		{
			$color = ($color == COLOR4) ? COLOR3 :  COLOR4;
			$ecode = '</Td></Tr><Tr bgcolor="'.$color.'" class=mes>';
		}
		else $ecode = '</Td></Tr>';
	}
	else
	{
		$bcode = "<Td valign=top>";
		$ecode = "</Td>";
	}
	$str .= $bcode.'<a href="'.C_URL.'/'.ADM_DIR.'/index.php?do=vp&ds=1&id='.$uid[$p].'" target=Blank>'.$usrname[$p].'</a><BR><a href="'.$bfile[$p].'" target=_blank><img src="'.$ufile[$p].'" border="0"></a><BR><input type="hidden" name="hidden_'.$uid[$p].'_'.$fid[$p].'" value="1"><input type="checkbox" name="image_'.$uid[$p].'_'.$fid[$p].'" value="'.$ufile[$p].'">'.$x[524].$ecode;
}
if (!is_int($tr))
{
	while (!is_int($tr))
	{
		$rp++;
		$tr = $rp / C_GALLERY_COLUMN;

		if (is_int($tr))
		{
			$bcode = '<Td> &nbsp;';
			$ecode = '</Td></Tr>';
		}
		else
		{
			$bcode = "<Td> &nbsp;";
			$ecode = "</Td>";
		}
		$str .= $bcode.$ecode;
	}
}
?>
<?=$str?>
<Tr align="center" bgcolor="<?=COLOR1?>"><Td  colspan="<?=C_GALLERY_COLUMN?>">
<input type="submit" value="<?=$x[521]?>" class=button>
</Td></Tr>
</Table></Td></Tr></Table>
</center>
<?php 
break;
############################################
# Gallery - End
############################################


############################################
# FAQ question add - Begin
############################################
		case 'fq':
			if(isset($cat) && is_numeric($cat) && !empty($question) && !empty($answer)) {
				$question=quot_del($question);
				$answer=quot_del($answer);
				db_query("INSERT INTO ".C_MYSQL_FAQ." VALUES(null,'".$cat."','".$question."','".$answer."')") or die(db_error());
				sprintm($x[42].'<br><br><a href="'.filename().'?l='.$l.'&do=fm&'.s().'">'.$x[43].'</a>');
			}
			else sprintm($w[164]);
			break;
			############################################
			# FAQ question add - End
			############################################
			############################################
			# FAQ Manager - Begin
			############################################
		case 'fm':
?>
<script language="JavaScript">
<!--
function formCheck(form) {
	tinyMCE.triggerSave();
	if (form.question.value == "")
	{alert("<?=$x[44]?>");return false;}
	if (form.answer.value == "")
	{alert("<?=$x[45]?>");return false;}
	if (document.form.submit.action != "") {
		document.form.submit.disabled=1;}
}
// -->
</script>
<span class=head><?=$x[46]?></span><br><br>
<center>
<a href="<?="javascript:open_win('".filename()."?l=".$l."&ds=1&do=fc&".s()."','fc');";?>" class=desc>[<?=$x[47]?>]</a> <a href="<?="javascript:open_win('".filename()."?l=".$l."&ds=1&do=fe&".s()."','fe');";?>" class=desc>[<?=$x[48]?>]</a> 
<a href="<?="javascript:open_win('".filename()."?l=".$l."&ds=1&do=fd&".s()."','fd');";?>" class=desc>[<?=$x[49]?>]</a><br>
<a href="<?="javascript:open_win('".filename()."?l=".$l."&ds=1&do=fqe&".s()."','fc');";?>" class=desc>[<?=$x[96]?>]</a> 
<a href="<?="javascript:open_win('".filename()."?l=".$l."&ds=1&do=fqd&".s()."','fd');";?>" class=desc>[<?=$x[97]?>]</a><br>
</center>
<?php 
$tmp=db_query("SELECT * FROM ".C_MYSQL_FAQCAT." order by id DESC");
$count=db_num_rows($tmp);
if($count != '0') {
	JSEditor();
?>
<form action="<?=filename()?>?l=<?=$l?>&do=fq&<?=s()?>" method="post" name=form OnSubmit="return formCheck(this)">
<center><span class=head><?=$x[50]?></span><br>
<center><br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="600" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="600" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[51]?>
</Td><Td>
<select name="cat" class=input>
<?php 
while($i=db_fetch_array($tmp)) {
	echo '<option value='.$i['id'].'>'.$i['catname'].' ('.$reallangname[$i['lang']].')';
}
?>
</select>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[52]?>
</Td><Td>
<input type=text name="question" class=input>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[53]?>
</Td><Td>
<textarea name="answer" class=input rows=12 style="width:100%"></textarea>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<input type="submit" value="<?=$x[50]?>" class=button name="submit">
</Td></Tr>
</Table></Td></Tr></Table>
<br>
<?php 
}
else {
	sprintm($x[54],2);
}
break;
############################################
# FAQ Manager - End
############################################
############################################
# FAQ categories creating - Begin
############################################
		case 'fc':
?>
<script language="JavaScript">
<!--
function formCheck(form) {
	if (form.cat.value == "")
	{alert("<?=$x[55]?>");return false;}
	if (document.form.submit.action != "") {
		document.form.submit.disabled=1;}
}
// -->
</script>
<form action="<?=filename()?>?l=<?=$l?>&ds=1&&do=fc&<?=s()?>" method="post" name=form OnSubmit="return formCheck(this)">
<center>
<span class=head><?=$x[56]?></span>
<br>
<?php 
if(isset($cat) && !empty($cat)) {
	if(!isset($lang) || count($lang) == 0) sprintm($x[572]);
	$p=0;
	while(isset($lang[$p])) {
		$cat=quot_del($cat);
		db_query("INSERT INTO ".C_MYSQL_FAQCAT." VALUES (Null, '".cb($lang[$p])."', '".$cat."')");
		smes($x[57].'<br>'.$cat.' <b>('.$reallangname[$lang[$p]].')</b><br>');
		$p++;
	}
}
?>
<br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[58]?>
</Td><Td>
<input type="text" name="cat" class=input>
</Td></Tr>
    <tr align="<?=C_ALIGN?>" class=desc bgcolor="<?=colors()?>">
      <td width=70%><?=$x[450]?><br><br><?=$x[448]?></td>
      <td><select multiple name="lang[]" class=input size="12">
      <?php 
      $existlang = explode(",",C_AVAILABLE_LANG);
      $p=0;while(isset($existlang[$p])) {
			?>
			<option value="<?=$existlang[$p]?>"><?=$reallangname[$existlang[$p]]?>
			<?php 
			$p++;
      }
      ?>      
      </select>
      </td>
    </tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<input type="submit" value="<?=$x[43]?>" class=button name="submit">
</Td></Tr>
</Table></Td></Tr></Table>
</center>
<?php 
break;
############################################
# FAQ categories creating - End
############################################

############################################
# FAQ categories editing - Begin
############################################
		case 'fe':
?>
<form action="<?=filename()?>?l=<?=$l?>&ds=1&&do=fx&<?=s()?>" method="post" name=form>
<center>
<span class=head><?=$x[59]?></span>
<br>
<br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_SWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_SWIDTH?>" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[58]?>
</Td><Td>
<select name="cat" class=input>
<?php 
$tmp=db_query("SELECT * FROM ".C_MYSQL_FAQCAT);while($i=db_fetch_array($tmp)) {
	echo '<option value='.$i['id'].'>'.$i['catname'].' <b>('.$reallangname[$i['lang']].')</b><br>';
}
?>
</select>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<input type="submit" value="<?=$x[60]?>" class=button>
</Td></Tr>
</Table></Td></Tr></Table>
</center>
<?php 
break;
############################################
# FAQ categories editing - End
############################################
############################################
# FAQ questions editing - Begin
############################################
		case 'fqe':
?>
<form action="<?=filename()?>?l=<?=$l?>&ds=1&do=fqx&<?=s()?>" method="post" name=form>
<center>
<span class=head><?=$x[96]?></span>
<br>
<br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_SWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_SWIDTH?>" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[52]?>
</Td><Td>
<select name="cat" class=input>
<?php 
$tmp=db_query("SELECT t1.*, t2.lang FROM ".C_MYSQL_FAQ." as t1, ".C_MYSQL_FAQCAT." as t2 where t1.cid = t2.id");while($i=db_fetch_array($tmp)) {
	echo '<option value='.$i['id'].'>'.$i['question'].' ('.$reallangname[$i['lang']].')';
}

?>
</select>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<input type="submit" value="<?=$x[60]?>" class=button>
</Td></Tr>
</Table></Td></Tr></Table>
</center>
<?php 
break;
############################################
# FAQ questions editing - End
############################################
############################################
# FAQ categories editing 2 - Begin
############################################
		case 'fx':
			if(isset($id) && !empty($cat) && is_numeric($id)) {
				#$cat=quot_del($cat);
				db_query("UPDATE ".C_MYSQL_FAQCAT." SET catname='".$cat."' WHERE id='".$id."'") or die(db_error());
				sprintm($x[61]);
			}
?>
<script language="JavaScript">
<!--
function formCheck(form) {
	if (form.cat.value == "")
	{alert("<?=$x[55]?>");return false;}
	if (document.form.submit.action != "") {
		document.form.submit.disabled=1;}
}
// -->
</script>
<form action="<?=filename()?>?l=<?=$l?>&ds=1&&do=fx&<?=s()?>" method="post" name=form OnSubmit="return formCheck(this)">
<center>
<span class=head><?=$x[62]?></span>
<br>
<br>
<?php 
if(isset($cat) && !empty($cat) && is_numeric($cat)) {
	$tmp=db_query("SELECT * FROM ".C_MYSQL_FAQCAT." WHERE id='".$cat."' limit 1");
	while($i=db_fetch_array($tmp)) {
?>
<br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_SWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_SWIDTH?>" class=mes>
<Tr align="center" bgcolor="<?=COLOR1?>"><Td colspan=2>
<?=template($x[473],array($reallangname[$i['lang']]))?>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[58]?>
</Td><Td>
<?php 
echo "<input name=\"id\" class=input type=hidden value=\"".$i['id']."\">
<input type=\"text\" name=\"cat\" class=\"input\" value=\"".$i['catname']."\">";

?>
</select>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<input type="submit" value="<?=$x[60]?>" class=button name="submit">
</Td></Tr>
</Table></Td></Tr></Table>
</center>
<?php 
	}
}
break;
############################################
# FAQ categories editing 2 - End
############################################
############################################
# FAQ questions editing 2 - Begin
############################################
		case 'fqx':
			if(isset($id) && !empty($cat) && is_numeric($id)) {
				$cat=quot_del($cat);$ans=quot_del($ans);
				db_query("UPDATE ".C_MYSQL_FAQ." SET question='".$cat."', answer='".$ans."' WHERE id='".$id."'") or die(db_error());
				sprintm($x[61]);
			}
?>
<script language="JavaScript">
<!--
function formCheck(form) {
	tinyMCE.triggerSave();
	if (form.cat.value == "")
	{alert("<?=$x[55]?>");return false;}
	if (document.form.submit.action != "") {
		document.form.submit.disabled=1;}
}
// -->
</script>
<form action="<?=filename()?>?l=<?=$l?>&ds=1&&do=fqx&<?=s()?>" method="post" name=form OnSubmit="return formCheck(this)">
<center>
<span class=head><?=$x[96]?></span>
<br>
<br>
<?php 
if(isset($cat) && !empty($cat) && is_numeric($cat)) {
?>
<br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_SWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_SWIDTH?>" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[52]?>
</Td><Td>
<?php 
$tmp=db_query("SELECT * FROM ".C_MYSQL_FAQ." WHERE id='".$cat."'");
while($i=db_fetch_array($tmp)) {
	JSEditor();
?>
<input name="id" class=input type=hidden value="<?=$i['id']?>">
<input type="text" name="cat" class="input" value="<?=$i['question']?>">
<textarea name="ans" class="input" rows=12 style="width:100%"><?=$i['answer']?></textarea>
<?php
}
?>
</select>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<input type="submit" value="<?=$x[60]?>" class=button name="submit">
</Td></Tr>
</Table></Td></Tr></Table>
</center>
<?php 
}
break;
############################################
# FAQ questions editing 2 - End
############################################
############################################
# FAQ delete - Begin
############################################
		case 'fd':
			if(isset($cat) && is_numeric($cat)) {
				db_query("DELETE FROM ".C_MYSQL_FAQ." WHERE cid='".$cat."'") or die(db_error());
				db_query("DELETE FROM ".C_MYSQL_FAQCAT." WHERE id='".$cat."'") or die(db_error());
				sprintm($x[63]);
			}
?>
<form action="<?=filename()?>?l=<?=$l?>&ds=1&&do=fd&<?=s()?>" method="post" name=form>
<center>
<span class=head><?=$x[64]?></span>
<br>
<span class=desc><?=$x[65]?></span>
<br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_SWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_SWIDTH?>" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[58]?>
</Td><Td>
<select name="cat" class=input>
<?php 
$tmp=db_query("SELECT * FROM ".C_MYSQL_FAQCAT);while($i=db_fetch_array($tmp)) {
	echo '<option value='.$i['id'].'>'.$i['catname'].' ('.$reallangname[$i['lang']].')';
}
?>
</select>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<input type="submit" value="<?=$x[66]?>" class=button>
</Td></Tr>
</Table></Td></Tr></Table>
</center>
<?php 
break;
############################################
# FAQ delete - End
############################################
############################################
# FAQ delete - Begin
############################################
		case 'fqd':
			if(isset($cat) && is_numeric($cat)) {
				db_query("DELETE FROM ".C_MYSQL_FAQ." WHERE id='".$cat."'") or die(db_error());
				sprintm($x[63]);
			}
?>
<form action="<?=filename()?>?l=<?=$l?>&ds=1&&do=fqd&<?=s()?>" method="post" name=form>
<center>
<span class=head><?=$x[97]?></span>
<br>
<span class=desc><?=$x[98]?></span>
<br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_SWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_SWIDTH?>" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[52]?>
</Td><Td>
<select name="cat" class=input>
<?php 
$tmp=db_query("SELECT t1.*, t2.lang FROM ".C_MYSQL_FAQ." as t1, ".C_MYSQL_FAQCAT." as t2 where t1.cid = t2.id");while($i=db_fetch_array($tmp)) {
	echo '<option value='.$i['id'].'>'.$i['question'].' ('.$reallangname[$i['lang']].')';
}
?>
</select>
</Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2 align=right>
<input type="submit" value="<?=$x[66]?>" class=button>
</Td></Tr>
</Table></Td></Tr></Table>
</center>
<?php 
break;
############################################
# FAQ delete - End
############################################
############################################
# Status help - Begin
############################################
		case 'sh':
			sprintm('<p align=left>'.$x[67]);
			break;
			############################################
			# Status help - End
			############################################
			############################################
			# Recommend Statistic - Begin
			############################################
		case 're':
?>
<span class=head><?=$x[83]?></span> <br><a href="javascript:open_win('<?=C_URL?>/<?=ADM_DIR?>/index.php?ds=1&do=rr','rr');" class=desc>[<?=$x[87]?>]</a>
<br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="600" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="600" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
<Td><?=$x[84]?></Td>
<Td><?=$x[85]?></Td>
<Td><?=$x[86]?></Td>
<Td width=150><?=$w[229]?></Td>
</Tr>
<?php
$tmp=db_query("SELECT *, ".CorrectDate("maildate")." FROM ".C_MYSQL_RECOMMEND);
while($i=db_fetch_array($tmp)) {
?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
<Td><?=$i['email']?></Td>
<Td><?=int2ip($i['ip'])?></Td>
<Td><?=$i['email2']?></Td>
<Td><?=mysql2data($i['maildate'],0,1)?></Td>
</Tr>
<?php }?>
</Table></Td></Tr></Table>
</center>
<?php
break;
############################################
# Recommend Statistic - End
############################################
############################################
# Repair&Optimize - Begin
############################################
		case 'rodo':
			if(GetC_URL() == 'azdg.com' || GetC_URL() == 'test.com') sprintm($x[574]);
			if(!isset($rep) || !is_array($rep)) $rep=array();
			if(!isset($opt) || !is_array($opt)) $opt=array();
			RepairAndOptimize($rep,$opt,$MySQLTables);
			break;
			############################################
			# Repair&Optimize - End
			############################################
			############################################
			# Repair&Optimize - Begin
			############################################
		case 'ro':

			if(!C_MAINTENANCE_MODE) mes($x[646]);
?>
<script language="JavaScript">
function sel(val) {
	with(document.form) {
		for(i=0;i<elements.length;i++) {
			thiselm = elements[i];
			if(thiselm.name.substring(0,3) == val)
			thiselm.checked = !thiselm.checked
		}
	}
}
</script>
		<form name=form action="?do=rodo" method=post>
    <?php table_header($x[9])?>
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
     <tr class=desc bgcolor="<?=colors()?>"><td width=70% align=center><?=$x[641]?></td>
      <td align=center><?=$x[642]?><input type="checkbox" onClick="sel('rep')" checked></td>
			<td align=center><?=$x[643]?><input type="checkbox" onClick="sel('opt')" checked></td></tr>
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
      <?php for ($p = 0; $p < count($MySQLTables); $p++) {
      	$adm = pow(2,$p);
      	$k = 1;
        ?>
     <tr class=desc bgcolor="<?=colors()?>"><td width=70%><?=$MySQLTables[$p]?></td>
      <td align=center>
			<?php
			$checked = (True) ? ' checked' : '';
			?>
			<input type="checkbox" name="rep[]" <?=$checked?> value="<?=$MySQLTables[$p]?>">
			</td>
			<td align=center>
			<?php
			$checked = (True) ? ' checked' : '';
			?>
			<input type="checkbox" name="opt[]" <?=$checked?> value="<?=$MySQLTables[$p]?>"">
			</td></tr>
    <?php
      }
    ?>
    <Tr><Td bgcolor="<?=C_TBCOLOR?>" colspan=3></Td></Tr>
	
    <?php table_footer()?><br>
    <input class=input type=submit value="<?=$x[9]?>" name="submit"></form>
<?php

break;
############################################
# Repair&Optimize - End
############################################
############################################
# Delete profile - Begin
############################################
		case 'dp':
			if(isset($id) && is_numeric($id))
			{
				if(C_ADMIN_REMOVE_PERM_WO_CONFIRM) {RemoveUser($id);$tm=array($id);	sprintm(template($x[69],$tm));}

				if(isset($make)) {
					if(isset($sendmail) && $sendmail == 'on') {
						if(!isset($subject) || empty($subject) || !isset($message) || empty($message)) sprintm($w[164]);
						$tmp=db_query("SELECT email FROM ".C_MYSQL_MEMBERS_SEC." WHERE id='".$id."'");
						while ($i = db_fetch_array($tmp))
						{
							sendmail(C_ADMINM,$i['email'],$subject,nl2br($message),$message);
						}
					}
					if(isset($perman) && $perman == 'on') RemoveUser($id);
					else ChangeStatus($id,'3');
					$tm=array($id);
					sprintm(template($x[69],$tm));
				}
				else
				{
					//$per_check = (C_ADMIN_REMOVE_PERM) ? ' checked' : '';
	?>

<form action="<?=filename()?>?do=dp&make=1" method="post" name=form>
<input class=input type=hidden name=id value="<?=$id?>">
	<script language=Javascript>
	function hide( control, division ) {
  controlChecked = true;
 	if ( control.checked == false )
      controlChecked = false
 
  if ( controlChecked == false ) {
      document.all[division].style.display = "none";
 } else {
     document.all[division].style.display = "block";
  }
 }
	</script>
	<span class=head><?=$x[66]?> ID#[<?=$id?>]</span><BR><BR>


	<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="400" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="400" class=mes>
<Tr align="center" bgcolor="<?=COLORH?>"><Td><?=$x[650]?></td>
<Td><input type=checkbox name=perman></td></Tr>
<?php if(C_ADMIN_REMOVE_MAIL){?>
<Tr align="center" bgcolor="<?=COLORH?>"><Td><?=$x[30]?></td>
<Td><input type=checkbox name=sendmail onclick="hide(this,'mydiv')"></td></Tr>
</table></td></Tr></table><BR>

<div id=mydiv style="display:none"">
<?php
$subject=$message='';
if(C_ADMIN_REMOVE_MAIL_LANG_ADMIN){
	$subject = (C_ADMIN_REMOVE_MAIL_SUBJ) ? template($w[559],array(C_SNAME)) : "";
	$message = (C_ADMIN_REMOVE_MAIL_MESS) ? template($w[560],array(C_SNAME,C_URL)) : "";
}
elseif(C_ADMIN_REMOVE_MAIL_SUBJ || C_ADMIN_REMOVE_MAIL_MESS)
{
	$tmp=db_query("SELECT mylang FROM ".C_MYSQL_MEMBERS_EXT." WHERE id='".$id."'");
	while ($i = db_fetch_array($tmp))
	{
		if(!empty($i['mylang']) && file_exists(C_PATH.'/languages/'.$i['mylang'].'/'.$i['mylang'].'a.php'))
		{
			@include(C_PATH.'/languages/'.$i['mylang'].'/'.$i['mylang'].'.php');
			@include(C_PATH.'/languages/'.$i['mylang'].'/'.$i['mylang'].'a.php');
			@include(C_PATH.'/languages/'.$i['mylang'].'/'.$i['mylang'].'l.php');
			@include(C_PATH.'/languages/'.$i['mylang'].'/'.$i['mylang'].'_.php');
		}
		else
		{
			@include(C_PATH.'/languages/default/default.php');
			@include(C_PATH.'/languages/default/defaulta.php');
			@include(C_PATH.'/languages/default/defaultl.php');
			@include(C_PATH.'/languages/default/default_.php');
		}
		$subject = (C_ADMIN_REMOVE_MAIL_SUBJ) ? template($w[559],array(C_SNAME)) : "";
		$message = (C_ADMIN_REMOVE_MAIL_MESS) ? template($w[560],array(C_SNAME,C_URL)) : "";
	}

	@include(C_PATH.'/languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'.php');
	@include(C_PATH.'/languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'_.php');
	@include(C_PATH.'/languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'a.php');
	@include(C_PATH.'/languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'l.php');
}
?>
	<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="400" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="400" class=mes>
<Tr align="left" bgcolor="<?=COLOR1?>"><Td><?=$x[26]?></td>
<td><input type=text name=subject class=input value="<?=$subject?>"></td></tr>
<Tr align="left" bgcolor="<?=COLOR1?>"><Td><?=$x[27]?></td>
<td><textarea name=message class=input rows=10 style="width:100%"><?=$message?></textarea></td></tr>

<?php }?>
</table></td></Tr></table><BR>

</div>

<input type=submit value="<?=$x[5]?>"><BR>

<?php	
				}
			}
			break;
			############################################
			# Delete profile - End
			############################################
			############################################
			# Delete maillist - Begin
			############################################
		case 'dm':
			if(isset($id) && is_numeric($id)) {
				db_query("DELETE FROM ".C_MYSQL_MAILLIST." WHERE id='".$id."'");
				sprintm($x[94]);
			}
			break;
			############################################
			# Delete maillist - End
			############################################
			############################################
			# Clear hits - Begin
			############################################
		case 'ch':
			if(GetC_URL() == 'azdg.com' || GetC_URL() == 'test.com') sprintm($x[574]);
			$hits = (C_CLEAR_HITS) ? '1' : '0';
			(C_POPULARITY != 2) ? db_query("UPDATE ".C_MYSQL_MEMBERS_MAIN." SET hits='".$hits."'") : db_query("UPDATE ".C_MYSQL_MEMBERS_MAIN." SET hits='0', hits_count='0'");
			db_query("DELETE FROM ".C_MYSQL_HITS) or die(db_error());
			$tmp=db_query('SELECT DATE_ADD(NOW(), INTERVAL '.C_TIME_DIFF.' HOUR) as mydate') or die(db_error());
			$rows=db_fetch_array($tmp);
			$mydate=$rows['mydate'];
			ChangeConstant(C_PATH.'/include/options.inc.php','C_LAST_CLEAR_DATE',0,$mydate);			sprintm($x[70]);
			break;
			############################################
			# Clear hits - End
			############################################
			############################################
			# Clear recommend statistic - Begin
			############################################
		case 'rr':
			db_query("DELETE FROM ".C_MYSQL_RECOMMEND) or die(db_error());
			sprintm($x[88],2);
			break;
			############################################
			# Clear recommend statistic - End
			############################################
			############################################
			# Statistic - Begin
			############################################
		case 'st':
			// Data
			$stat_need=array(C_STAT_COUNTRY,C_STAT_GENDER,C_STAT_MARITAL,C_STAT_CHILDREN,C_STAT_HEIGHT,C_STAT_WEIGHT,C_STAT_HAIR,C_STAT_EYES,C_STAT_ETNICITY,C_STAT_RELIGION,C_STAT_SMOKE,C_STAT_DRINK,C_STAT_EDUCATION,C_STAT_HDYFU,C_STAT_HORO);
			$w_num=array('224','57','66','67','68','69','70','71','72','73','74','75','76','86','129');
			$mysql_name=array('t1.country','t1.gender','t2.marstat','t2.child','t1.height','t1.weight','t2.hcolor','t2.ecolor','t2.etnicity','t2.religion','t2.smoke','t2.drink','t2.education','t2.hdyfu','t1.horo');
			$w_name=array('wcr','wg','wm','wc','wh','ww','whc','we','wet','wr','ws','wd','wed','wu','whr');
			/////////////////////
			if(!isset($a)) $a='';
			if ($a == "s") {
				if (!isset($step)) $step = 10;
				if (!isset($from)) $from = 0;
				if(!isset($src)) $src='0';
				if(!isset($val)) $val='0';
				if(C_HACK1) {
					if(!is_numeric($src)||!is_numeric($step)||!is_numeric($from)||!is_numeric($val)) sprintm($w[1].'1',1);}
					if(C_HACK2) {
						if(($src < 0)||($src >= sizeof($mysql_name))||($val < 0)||($val >= sizeof($$w_name[$src]))) sprintm($w[1].'2',1);}
						///////// Checking and creating for search
						$msortby = " order by t1.req DESC, t1.editdate DESC";

						// Important /////////////
						$mid = " t1.status >= '7' AND t1.id = t2.id";
						$msrc=" AND ".$mysql_name[$src]." = '".$val."'";
						$sql="SELECT t1.id,t1.username,t1.fname,t1.lname,t1.gender,t1.purposes,t1.birthday,t1.country,t1.city,t1.editdate,t1.regdate,t1.files FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2 WHERE ".$mid.$msrc.$msortby." limit ".$from.",".$step;
						$tsql = "SELECT count(*) as total FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2 WHERE ".$mid.$msrc;
						$result = db_query($sql) or die(db_error());
						$tquery = db_query($tsql) or die(db_error());
						$trows = db_fetch_array($tquery);
						$count = $trows['total'];
						if($count == "0") sprintm($w[110]);
						$str='';$color='';
						while ($i = db_fetch_array($result)) {
							if($i['files'] == '') {$picav = $w[111];$sk=0;} else {$picav = $w[112];$sk=1;}
							if((C_SEARCH_PHOTO) && ($sk)) {
								$photo=ShowPic(not_empty($i['files'],$i['id']));
								$picav='<a href="'.$photo.'" target="_blank"><img src="'.$photo.'" border="'.C_IMG_BRDR.'" width=50></a>';
							}
							$color = ($color == COLOR4) ? COLOR3 : COLOR4;
							$age=mysql2data($i['birthday'],1);
							//$name = (trim($i['fname']) == '') ? $i['id'] : $i['fname'];
							$name = username($i['id'],$i['username'],$i['fname'],$i['lname']);
							$str.="<Tr bgcolor=".$color." align=center><td><a href=index.php?do=vp&ds=1&id=".$i['id']." target='_blank'>".$name."</a></td><td>".$wg[$i['gender']]." ".arr2val($wp,$i['purposes'])."</td><td>".abs($age)."</td><td>".$wcr[$i['country']]."</td><td>".$i['city']."</td><td>".mysql2data($i['editdate'])."</td><td>".mysql2data($i['regdate'])."</td><td>".$picav."</td></tr>";
						}
						$param='do=st&a=s&src='.$src.'&val='.$val;
						$colspan=8;
						$str.=pages($from,$step,$count,$param,$colspan);
						$str.="</table></td></tr></table>";
?>
<br>
<span class=head><?=search_results($from,$step,$count);?></span>
<br><br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="600" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="600" class=mes>
<Tr align="center" bgcolor="<?=COLORH?>"><Td><?=$w[118]?></td>
<Td><?=$w[119]?></td><Td><?=$w[120]?></td><Td><?=$w[121]?></td><Td><?=$w[122]?></td><Td><?=$w[123]?></td><Td><?=$w[124]?></td><Td><?=$w[87]?></td></Tr>
<?=$str;?>
<?php 
			} else {
				$sql = "SELECT count(*) as total FROM ".C_MYSQL_MEMBERS_MAIN." WHERE status >= '7'";
				$result = db_query($sql) or die(db_error());
				$trows = db_fetch_array($result);
				$num = $trows['total'];
				if ($num == 0) sprintm($w[204]);
				function azdg_stat($stat_need=array(),$w_num=array(),$mysql_name=array(),$w_name=array()) {
					global $w;
					for($i=0;$i<sizeof($stat_need);$i++) {
						$DB_NAME = (substr($mysql_name[$i],1,1) == '1') ? C_MYSQL_MEMBERS_MAIN.' as t1' : C_MYSQL_MEMBERS_EXT.' as t2';
?>
<br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_SWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_SWIDTH?>" class=mes>
<Tr align="center" bgcolor="<?=COLORH?>"><Td colspan=2>
<?=$w[$w_num[$i]]?></Td></Tr>
<?php $color='';
$tmp=db_query("SELECT ".$mysql_name[$i].", count(*) as c FROM ".$DB_NAME." WHERE status >= '7' group by ".$mysql_name[$i]." order by c desc");
while($j=db_fetch_array($tmp)) {
	$color = ($color == COLOR4) ? COLOR3 : COLOR4;
        $wname = $w_name[$i];
        global ${$wname};
        $mas = ${$wname};
?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=$color?>"><Td>
<?php
global $l;
$param='l='.$l.'&do=st&a=s&src='.$i.'&val='.$j[substr(stristr($mysql_name[$i], '.'),1)];
echo '<a href="'.C_URL.'/'.ADM_DIR.'/index.php?'.$param.'">'.$mas[$j[substr(stristr($mysql_name[$i], '.'),1)]].'</a>';
?></Td><Td><?=$j['c']?></Td></Tr>
<?php }?></Table></Td></Tr></Table>
<?php
					}
				}
				azdg_stat($stat_need,$w_num,$mysql_name,$w_name);
			}
			break;
			############################################
			# Statistic - End
			############################################
			############################################
			# Users allow - small window - Begin
			############################################
		case 'al':
			ApproveUser($id);
			sprintm(template($x[71],array($id)));
			break;
			############################################
			# Users allow - small window - End
			############################################
			############################################
			# Users suspend - small window - Begin
			############################################
		case 'sp':
		    if(isset($id) && is_numeric($id)){
                if(isset($make)) {
                    // 1. user suspend
			        SuspendUser($id);

			        // 2. send private message
			        $subject = cb($w['862']);
			        $message = cb($xur[$reason]['chat']);
			        $mess = preg_replace('/\<br(\s*)?\/?\>/i', "%0D%0A", $message);

			        db_query("INSERT INTO ".C_MYSQL_MESSAGES." VALUES (Null,'0','".$id."','".$subject."',NOW(),'".ip2int(ip())."','1', '0')") or die(db_error());
					$lastid = db_insert_id();
					save_message($lastid, $message);
					AddUserToAddressBook($id, 1);

			        //3. send email
			        $tmp_email=db_query("SELECT email FROM ".C_MYSQL_MEMBERS_SEC." WHERE id IN (".$id.")") or die(db_error());
                    while ($i = db_fetch_array($tmp_email))
                    {
                        $email = $i['email'];
                    }
			        $html = $xur[$reason]['html'];
                    $text = $xur[$reason]['text'];
                    sendmail(C_ADMINM, $email, $xur[$reason]['subject'], $html, $text);

                    //4. log
                    $save_log = empty_log_array();
                    $save_log['user'] = get_info('adminID');
					$save_log['time'] = date('Y-m-d H:i:s');
					$save_log['id'] = $id;
					$save_log['email'] = $email;
					$save_log['action'] = 'Profil kitiltása. Indok: ' . $xur[$reason]['title'];
					log_csv(array($save_log));

			        sprintm(template($x[854],array($id)));

			    } else {
?>
<script language="JavaScript">
<!--
function formCheck(form) {

	if (document.form.submit.action != "") {
		document.form.submit.disabled=1;
	}

	if (document.form.reason.value == ''){
	    document.form.submit.disabled=0;
	    alert(<?=$w['432']?>);
	    return false;
	}
}
// -->
</script>
<form action="<?=filename()?>?do=sp&make=1" method="post" name=form OnSubmit="return formCheck(this)">
    <input class=input type=hidden name=id value="<?=$id?>">
    <span class=head><?=$x[853]?> ID#[<?=$id?>]</span><BR><BR>

    <Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="400" bgcolor="<?=C_TBCOLOR?>">
        <Tr><Td>
                <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="400" class=mes>
                    <Tr align="center" bgcolor="<?=COLORH?>">
                        <Td><?=$x[401]?>: </td>
                        <Td>
                            <select name="reason">
<?php
echo '<option value="">'.$w['432'].'</option>';
foreach($xur as $key => $item){
    echo '<option value="'.$key.'">'.$item['title'].'</option>';
}
 ?>
                            </select>
                        </Td>
                    </Tr>
                    <Tr bgcolor="<?=COLORH?>">
                        <Td colspan="2" style="text-align: center"><input type=submit value="<?=$w[170]?>"></Td>
                    </Tr>
                </table>
        </td></Tr>
    </table><BR>
</form>
<?php
			    }
			} else {
		        sprintm($x['277']);
			}

			break;
			############################################
			# Users suspend - small window - End
			############################################
			############################################
			# Add profile to banlist - small window - Begin
			############################################
		case 'apb':
			@db_query("INSERT INTO ".C_MYSQL_BANLIST." VALUES (Null, '".$myid."','".date('Y-m-d')."','".$user."','')") or die(db_error());
			sprintm(template($x[990],array($user, $myid)));
			break;
			############################################
			# Add profile to banlist - small window - End
			############################################
			############################################
			# User edit form - Begin
			############################################
		case 'ue':
			if(isset($id) && is_numeric($id)) {

				if(isset($fullmode))
				{
					define('FULLMODE','1');
					$modeword='Quick mode';
					$modelink='';
				}
				else
				{
					define('FULLMODE','0');
					$modeword='Full mode';
					$modelink='&fullmode';
				}

				######################
				# Change profile - form
				######################
				$tmp=db_query("SELECT t1.* ,".CorrectDate("regdate").", ".CorrectDate("editdate").", t2.*, t3.* FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2, ".C_MYSQL_MEMBERS_SEC." as t3 WHERE t1.id = '".$id."' AND t1.id = t2.id AND t1.id = t3.id");
				while($i=db_fetch_array($tmp)){
					$name = username($i['id'],$i['username'],$i['fname'],$i['lname']);

					$tmpr=db_query("SELECT * FROM ".C_MYSQL_MEMBERS_RESTRICTS." WHERE id='".$id."'");
					$countr = db_num_rows($tmpr);
					if($countr) {
						$ir = db_fetch_array($tmpr);
					}
					else
					{
						$ir['photo'] = '1';
						$ir['audio'] = '1';
					}


					GenField('purpose',C_PURPR,C_PURPOSESH,'purposes',$wp,'58');
					GenField('interests',C_INTERESTSR,C_INTERESTSSH,'interests',$win,'526');
					GenField('religion',C_RELR,C_RELIGIONSH,'religion',$wr,'82');

					GenField('seeking_gender',C_SGENDR,C_SGENDERSH,'sgender',$wg,'80');
					GenField('seeking_country',C_SCNTRR,C_SCOUNTRYSH,'scountry',$wcr,'444');
					GenField('seeking_marital_status',C_SMARSR,C_SMARSTATSH,'smarstat',$wm,'445');
					GenField('seeking_child',C_SCHILDR,C_SCHILDSH,'schild',$wc,'446');
					GenField('seeking_hair',C_SHAIRR,C_SHCOLORSH,'shcolor',$whc,'447');
					GenField('seeking_eye',C_SEYER,C_SECOLORSH,'secolor',$we,'448');
					GenField('seeking_smoke',C_SSMOKER,C_SSMOKESH,'ssmoke',$ws,'449');
					GenField('seeking_drink',C_SDRINKR,C_SDRINKSH,'sdrink',$wd,'450');
					GenField('seeking_education',C_SEDUCR,C_SEDUCATIONSH,'seducation',$wed,'451');
					GenField('seeking_ethnicity',C_SETNR,C_SETHNICITYSH,'setnicity',$wet,'81');
					GenField('seeking_religion',C_SRELR,C_SRELIGIONSH,'sreligion',$wr,'82');


					$ppquery = db_query("SELECT count(id) as total FROM ".C_MYSQL_PRIVATE_PHOTOS." WHERE uid='".$i['id']."'") or die(db_error());
					$pprows = db_fetch_array($ppquery);
					$ppcount = $pprows['total'];

					?>
<script language="JavaScript">
<!--
// Select Multiple js-function: hld()
function hld(nid){ln=nid.parentNode;if(nid.checked){ln.style.backgroundColor='#0a246a';ln.style.color='#fff';}else{ln.style.backgroundColor='#fff';ln.style.color='#000';}}
// -->
</script>
<form action="<?=filename()?>?do=uu&<?=s()?>" method="post" enctype="multipart/form-data" name=form>
<input class=input type=hidden name=id value="<?=$i['id']?>">
<center><span class=head><?=$w[99]?></span><br>
<center><br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="600" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="600" class=mes>
<?php if(ShowForEdit(C_ADMINEDIT1,1,$i['id'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[126]?></td><td><?=$i['id']?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,2,$i['username'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[264]?><?=$w[0];?></td>
<td><input class=input type=text name=username value="<?=$i['username']?>"></td>
</tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,4,$i['fname'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[208]?><?php if(C_FIRSTNR) echo $w[0];?> [<?=C_FIRSTNS?>-<?=C_FIRSTNB?> <?=$w[52]?>]</td>
<td><input class=input type=text name=fname maxlength="<?=C_FIRSTNB?>" value="<?=$i['fname']?>"></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,8,$i['lname'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[209]?><?php if(C_LASTNR) echo $w[0];?> [<?=C_LASTNS?>-<?=C_LASTNB?> <?=$w[52]?>]</td>
<td><input class=input type=text name=lname maxlength="<?=C_LASTNB?>" value="<?=$i['lname']?>"></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,16,$i['birthday'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[210]?><?php if(C_BIRTHR) echo $w[0];?></td>
<td>
<select name="month" class="minput">
<?php  
$sqldata=str_replace(" ","-",$i['birthday']);
$val = explode('-', $sqldata);
$year=$val[0];$month=$val[1];$day=$val[2];
$p=1; while(isset($wmm[$p])) {
($month == $p) ? $sel=" selected" : $sel="";
echo '<option value="'.$p.'"'.$sel.'>'.$wmm[$p];
$p++;}
?>
</select>
<select name="day" class="sinput">
<?php  for($p=1;$p<32;$p++) {
($day == $p) ? $sel=" selected" : $sel="";
echo '<option'.$sel.'>'.$p;}
?>
</select>
<?php  $y=date("Y", time());$yfrom=$y-C_AGES;$yto=$y-C_AGEB; ?>
<select name="year" class="sinput">
<?php  for($p=$yto;$p<=$yfrom;$p++) {
($year == $p) ? $sel=" selected" : $sel="";
echo '<option'.$sel.'>'.$p;}
?>
</select>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,32,$i['gender'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[132]?><?php if(C_GENDR) echo $w[0];?></td>
<td>
<select name="gender" class="input">
<?php  $p=0;while(isset($wg[$p])) {
($i['gender'] == $p) ? $sel=" selected" : $sel="";
echo '<option value="'.$p.'"'.$sel.'>'.$wg[$p];$p++;}
?>
</select>
</td></tr>
<?php }?>

<?php if(ShowForEdit(C_ADMINEDIT2,1073741824,$i['bodytype'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[525]?><?php if(C_BODYTYPER) echo $w[0];?></td>
<td>
<select name="bodytype" class="input">
<?php  $p=0;while(isset($wbt[$p])) {
($i['bodytype'] == $p) ? $sel=" selected" : $sel="";
echo '<option value="'.$p.'"'.$sel.'>'.$wbt[$p];$p++;}
?>
</select>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,536870912,$i['orientation'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[524]?><?php if(C_ORIENTATIONR) echo $w[0];?></td>
<td>
<select name="orientation" class="input">
<?php  $p=0;while(isset($wor[$p])) {
($i['orientation'] == $p) ? $sel=" selected" : $sel="";
echo '<option value="'.$p.'"'.$sel.'>'.$wor[$p];$p++;}
?>
</select>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,64,$i['purposes'])) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=PURPOSE?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=PURPOSE_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,128,$i['country'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[121]?><?php if(C_CNTRR) echo $w[0];?></td>
<td>
<select name="country" class="input">
<?php  $p=0;asort($wcr);reset($wcr);
while (list ($p, $val) = each ($wcr)) {
($i['country'] == $p) ? $sel=" selected" : $sel="";
echo '<option value="'.$p.'"'.$sel.'>'.$val;
}
?>
</select>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT3,2,$i['state'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[527]?><?php if(C_STATER) echo $w[0];?></td>
<td><input class=input type=text name=state value="<?=$i['state']?>"></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,256,$i['email'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[211]?><?=$w[0];?></td>
<td><input class=input type=text name=email value="<?=$i['email']?>"></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,512,$i['url'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[144]?></td>
<td><input class=input type=text name=url value="<?=$i['url']?>"></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,1024,$i['icq'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[145]?></td>
<td><input class=input type=text name=icq value="<?=$i['icq']?>"></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,2048,$i['aim'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[146]?></td>
<td><input class=input type=text name=aim value="<?=$i['aim']?>"></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,4096,$i['phone'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[147]?><?php if(C_PHONER) echo $w[0];?></td>
<td><input class=input type=text name=phone value="<?=$i['phone']?>"></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,8192,$i['zip'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[285]?><?php if(C_ZIPR) echo $w[0];?></td>
<td><input class=input type=text name=zip value="<?=$i['zip']?>"></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,16384,$i['city'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[122]?><?php if(C_CITYR) echo $w[0];?></td>
<td><input class=input type=text name=city value="<?=$i['city']?>"></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,32768,$i['address'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[378]?><?php if(C_ADDRESSR) echo $w[0];?></td>
<td><input class=input type=text name=address value="<?=$i['address']?>"></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,65536,$i['marstat'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[134]?><?php if(C_MARSR) echo $w[0];?></td>
<td>
<select name="marstat" class="input">
<?php  $p=0;while(isset($wm[$p])) {
($i['marstat'] == $p) ? $sel=" selected" : $sel="";
echo '<option value="'.$p.'"'.$sel.'>'.$wm[$p];$p++;}
?>
</select>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,131072,$i['child'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[135]?><?php if(C_CHILDR) echo $w[0];?></td>
<td>
<select name="child" class="input">
<?php  $p=0;while(isset($wc[$p])) {
($i['child'] == $p) ? $sel=" selected" : $sel="";
echo '<option value="'.$p.'"'.$sel.'>'.$wc[$p];$p++;}
?>
</select>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,262144,$i['height'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[130]?><?php if(C_HGHTR) echo $w[0];?></td>
<td>
<select name="height" class="input">
<?php  $p=0;while(isset($wh[$p])) {
($i['height'] == $p) ? $sel=" selected" : $sel="";
echo '<option value="'.$p.'"'.$sel.'>'.$wh[$p];$p++;}
?>
</select>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,524288,$i['weight'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[131]?><?php if(C_WGHTR) echo $w[0];?></td>
<td>
<select name="weight" class="input">
<?php  $p=0;while(isset($ww[$p])) {
($i['weight'] == $p) ? $sel=" selected" : $sel="";
echo '<option value="'.$p.'"'.$sel.'>'.$ww[$p];$p++;}
?>
</select>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,1048576,$i['hcolor'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[136]?><?php if(C_HAIRR) echo $w[0];?></td>
<td>
<select name="hcolor" class="input">
<?php  $p=0;while(isset($whc[$p])) {
($i['hcolor'] == $p) ? $sel=" selected" : $sel="";
echo '<option value="'.$p.'"'.$sel.'>'.$whc[$p];$p++;}
?>
</select>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,2097152,$i['ecolor'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[137]?><?php if(C_EYER) echo $w[0];?></td>
<td>
<select name="ecolor" class="input">
<?php  $p=0;while(isset($we[$p])) {
($i['ecolor'] == $p) ? $sel=" selected" : $sel="";
echo '<option value="'.$p.'"'.$sel.'>'.$we[$p];$p++;}
?>
</select>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,4194304,$i['etnicity'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[138]?><?php if(C_ETNR) echo $w[0];?></td>
<td>
<select name="etnicity" class="input">
<?php  $p=0;while(isset($wet[$p])) {
($i['etnicity'] == $p) ? $sel=" selected" : $sel="";
echo '<option value="'.$p.'"'.$sel.'>'.$wet[$p];$p++;}
?>
</select>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,8388608,$i['religion'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[139]?><?php if(C_RELR) echo $w[0];?></td>
<td>
<div class=multi1>
    <div class=multis>
      <?=RELIGION_VALUE?>
    </div>
		</div>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,16777216,$i['smoke'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[140]?><?php if(C_SMOKER) echo $w[0];?></td>
<td>
<select name="smoke" class="input">
<?php  $p=0;while(isset($ws[$p])) {
($i['smoke'] == $p) ? $sel=" selected" : $sel="";
echo '<option value="'.$p.'"'.$sel.'>'.$ws[$p];$p++;}
?>
</select>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,33554432,$i['drink'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[141]?><?php if(C_DRINKR) echo $w[0];?></td>
<td>
<select name="drink" class="input">
<?php  $p=0;while(isset($wd[$p])) {
($i['drink'] == $p) ? $sel=" selected" : $sel="";
echo '<option value="'.$p.'"'.$sel.'>'.$wd[$p];$p++;}
?>
</select>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,67108864,$i['education'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[142]?><?php if(C_EDUCR) echo $w[0];?></td>
<td>
<select name="education" class="input">
<?php  $p=0;while(isset($wed[$p])) {
($i['education'] == $p) ? $sel=" selected" : $sel="";
echo '<option value="'.$p.'"'.$sel.'>'.$wed[$p];$p++;}
?>
</select>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT3,1,$i['interests'])) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=INTERESTS?>
    </Td>
    <Td align=left>    
    <div class=multi1>
    <div class=multi2>
      <?=INTERESTS_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,134217728,$i['languages'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[382]?><?php if(C_LANGR) echo $w[0];?></td>
<td>
<?php
$p = 1;
$lang=explode(',',$i['languages']);
while (isset($wlng[$p]))
{
	$lang2 = @array_flip($lang);
	if (isset($lang2[$p])) echo $wlng[$p].' <input type="checkbox" name="lang[]" value="'.$p.'" checked><br>';
	else  echo $wlng[$p].' <input type="checkbox" name="lang[]" value="'.$p.'"><br>';
	$p++;
}
?>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,268435456,$i['mylang'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[459]?></td>
<td>
<select name="mylang" class="input">
<?php 
$existlang = explode(",",C_AVAILABLE_LANG);
if(!in_array($i['mylang'],$existlang)) $i['mylang'] = 'default';
$rlng=array_keys($reallangname);
$p=0;while(isset($rlng[$p])) {
(strtolower($i['mylang']) == strtolower($rlng[$p])) ? $sel=" selected" : $sel="";
echo '<option value="'.$rlng[$p].'"'.$sel.'>'.$reallangname[$rlng[$p]];$p++;}
?>
</select>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,536870912,$i['job'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[213]?><?php if(C_JOBR) echo $w[0];?></td>
<td><input class=input type=text name=job value="<?=$i['job']?>"></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT1,1073741824,$i['hobby'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[214]?><?php if(C_HOBBR) echo $w[0];?></td>
<td><input class=input type=text name=hobby value="<?=$i['hobby']?>"></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,1,$i['descr'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><td><?=$w[215]?><?=$w[0];?></td><td><textarea class=textarea cols=20 rows=8 name=descr><?=tb($i['descr'])?></textarea></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,2,$i['sgender'])) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_GENDER?>
    </Td>
    <Td>     
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_GENDER_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,4,$i['scountry'])) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_COUNTRY?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multi2>
      <?=SEEKING_COUNTRY_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,8,$i['smarstat'])) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_MARITAL_STATUS?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_MARITAL_STATUS_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,16,$i['schild'])) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_CHILD?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_CHILD_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,32,$i['shcolor'])) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_HAIR?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_HAIR_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,64,$i['secolor'])) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_EYE?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_EYE_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,128,$i['ssmoke'])) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_SMOKE?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_SMOKE_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,256,$i['sdrink'])) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_DRINK?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_DRINK_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,512,$i['seducation'])) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_EDUCATION?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_EDUCATION_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,1024,$i['setnicity'])) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_ETHNICITY?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_ETHNICITY_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,2048,$i['sreligion'])) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_RELIGION?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_RELIGION_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,4096,$i['agef'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[83]?><?php if(C_SAGER) echo $w[0];?></td>
<td><select name="agef" class="minput"> 
<?php  for($p=C_AGES;$p<=C_AGEB;$p++){
	if($p == $i['agef']) echo '<option selected>'.$p;
	else echo '<option>'.$p;
}?>
</select>-<select name="aget" class="minput">
<?php  for($p=C_AGES;$p<=C_AGEB;$p++){
	if($p == $i['aget']) echo '<option selected>'.$p;
	else echo '<option>'.$p;
}?>
</select>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,8192,$i['heightf'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[84]?><?php if(C_SHGHTR) echo $w[0];?></td>
<td><select name="heightf" class="minput"> 
<?php  $p=0;while(isset($wh[$p])) {
	if($p == $i['heightf']) echo '<option value="'.$p.'" selected>'.$wh[$p];
	else echo '<option value="'.$p.'">'.$wh[$p];
$p++;}?>
</select>-<select name="heightt" class="minput">
<?php  $p=0;while(isset($wh[$p])) {
	if($p == $i['heightt']) echo '<option value="'.$p.'" selected>'.$wh[$p];
	else echo '<option value="'.$p.'">'.$wh[$p];
$p++;}?>
</select>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,16384,$i['weightf'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[85]?><?php if(C_SWGHTR) echo $w[0];?></td>
<td><select name="weightf" class="minput">
<?php  $p=0;while(isset($ww[$p])) {
	if($p == $i['weightf']) echo '<option value="'.$p.'" selected>'.$ww[$p];
	else echo '<option value="'.$p.'">'.$ww[$p];
$p++;}?>
</select>-<select name="weightt" class="minput">
<?php  $p=0;while(isset($ww[$p])) {
	if($p == $i['weightt']) echo '<option value="'.$p.'" selected value=0>'.$ww[$p];
	else echo '<option value="'.$p.'">'.$ww[$p];
$p++;}?>
</select>
</td></tr>
<?php }?>

<?php if(ShowForEdit(C_ADMINEDIT2,32768,1)) {?>

<?php 


define('DELETE_PHOTO', $w[259]);
if ($i['files'] != "")
{
	$arr=explode(',',$i['files']);
}
else $arr=array();
$k=0;$tm = array(C_MAXSZ);$PHOTO_NUM=$REMOVE_PHOTO=$PHOTO_VALUE=array();
reset($arr);rsort($arr);
$max = (($i['files'] != "") && ($arr[0]+1) > $wpn[$i['req']]) ? ($arr[0]+1) : $wpn[$i['req']];

for($ii = 0; $ii < $max; $ii++)
{
	$k = $ii+1;
	$Require = ((C_PHOTOR) && ($ii == "0")) ? $w[0] :	'';
	$PHOTO_NUM[] = $w[87].' '.$k.' '.$Require.' '.template($w[223], $tm);
	if (in_array($ii,$arr))
	{
		$REMOVE_PHOTO[$ii]=1;
		$REMOVE_VALUE[$ii]= ShowPic(PicPath($ii,$i['id']));
	}
	elseif($ii >= $wpn[$i['req']]) $REMOVE_PHOTO[$ii]=-1;
	else $REMOVE_PHOTO[$ii]=0;
}
$p=0;
while(isset($PHOTO_NUM[$p])) {?>
<?php if($REMOVE_PHOTO[$p] > 0) { ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td><?=$PHOTO_NUM[$p]?></Td>
    <Td><a href="<?=$REMOVE_VALUE[$p]?>&bigger=1" target="_blank"><img src="<?=$REMOVE_VALUE[$p]?>" border="<?=C_IMG_BRDR?>" width=100></a><br><div id=delform<?=$p?> style='display:block'><input type="checkbox" name="delpic<?=$p?>"><?=DELETE_PHOTO?></div>
</Td>
  </Tr>
<?php } elseif($REMOVE_PHOTO[$p] < 0) { ?>
<?php } else { ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td><?=$PHOTO_NUM[$p]?></Td>
    <Td><input class=input type=file name="file<?=$p?>"></Td>
  </Tr>
<?php }?>
<?php $p++;}$p=0;?>
<?php }?>  
<?php if(ShowForEdit(C_ADMINEDIT2,65536,1)) {?>
<?php if(!empty($i['audio']) && (C_AUDIO_ALLOW)) {
	$autostart = (C_AUDIO_AUTOSTART) ? 'true' : 'false';
?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
      <?php if(IsAV($i['audio'])) {?>
      <script language="JavaScript">
      <!--
      var soundFile = "<?=C_URL?>/members/uploads/<?=id2pathAudio($i['id']).'.'.$i['audio']?>";
      if(navigator.plugins.length>0){
      	document.write('<embed src="'+soundFile+'" autostart="<?=$autostart?>" loop="<?=C_AUDIO_LOOP?>" <?=audio_player(C_AUDIO_PLAYER,'0');?>></embed>');}
      	else {
      		document.write('<embed src="'+soundFile+'" autostart="<?=$autostart?>" loop="<?=C_AUDIO_LOOP?>" <?=audio_player(C_AUDIO_PLAYER,'1');?>></embed>');}
      		//-->
      </script>
      <noscript><embed src="<?=C_URL?>/members/uploads/<?=id2pathAudio($i['id']).'.'.$i['audio']?>" autostart="<?=$autostart?>" loop="<?=C_AUDIO_LOOP?>"></embed></noscript>
      <noembed><bgsound src="<?=C_URL?>/members/uploads/<?=id2pathAudio($i['id']).'.'.$i['audio']?>" loop="<?=C_AUDIO_LOOP?>"></noembed>
      <?php  } else { ?>
      <OBJECT id='mediaPlayer' width="320" height="285" 
      classid='CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95' 
      codebase='http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701'
      standby='Loading Microsoft Windows Media Player components...' type='application/x-oleobject'>
      <param name='fileName' value="<?=C_URL?>/members/uploads/<?=id2pathAudio($i['id']).'.'.$i['audio']?>">
      <param name='animationatStart' value='true'>
      <param name='transparentatStart' value='true'>
      <param name='autoStart' value="<?=$autostart?>">
      <param name='showControls' value="true">
      <param name='loop' value="<?=C_AUDIO_LOOP?>">
      <EMBED type='application/x-mplayer2'
        pluginspage='http://microsoft.com/windows/mediaplayer/en/download/'
        id='mediaPlayer' name='mediaPlayer' displaysize='4' autosize='-1' 
        bgcolor='darkblue' showcontrols="true" showtracker='-1' 
        showdisplay='0' showstatusbar='-1' videoborder3d='-1' width="320" height="285"
        src="<?=C_URL?>/members/uploads/<?=id2pathAudio($i['id']).'.'.$i['audio']?>" autostart="<?=$autostart?>" designtimesp='5311' loop="<?=C_AUDIO_LOOP?>">
      </EMBED>
      </OBJECT>
			<?php }?>
</td>
<td><input type="checkbox" name="delaudio"><?=$w[297]?></td></tr>
<?php } else {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?php
$tm = array(C_AUDIO_SIZE);
switch(C_AUDIO_ALLOW)
{
	case '1': define('AUDIO', $w[292].' '.template($w[223], $tm));break;
	case '2': define('AUDIO', $w[670].' '.template($w[223], $tm));break;
	case '3': define('AUDIO', $w[671].' '.template($w[223], $tm));break;
}

?>
<?=AUDIO?> </td>
<td><input class=input type=file name="filea"></td></tr>
<?php }?>
<?php }?>
<?php if(FULLMODE) {?>
<Tr align="center" bgcolor="<?=COLORH?>"><Td colspan=2>
<?=$x[482]?>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,131072,1)) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[54]?></td>
<td><input class=input type=text name=pass maxlength="<?=C_PASSB?>" value=""></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,262144,1)) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[72]?></td>
<td><input class=input type=text name=ip value="<?=int2ip($i['ip'])?>"></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,1048576,1)) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<a href="<?="javascript:open_win('".filename()."?ds=1&do=sh&".s()."','help');"?>" class=desc><?=$x[74]?></a></td>
<td><input class=input type=text name=status value="<?=$i['status']?>"></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,2097152,1)) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[99]?></a></td>
<td><input class=input type=text name=hits value="<?=$i['hits']?>"></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,4194304,1)) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[481]?></a></td>
<td><input class=input type=text name=hits_count value="<?=$i['hits_count']?>"></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT3,8,1)) {$checked = ($i['hide']) ? ' checked' : '';?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[745]?></td>
<td><input type=checkbox name="hide"  <?=$checked?>></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,8388608,1)) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[365]?></a></td>
<td><input class=input type=text name=prv value="<?=$i['prv']?>"></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT3,16,$i['link_txt'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[806]?>
</td><td>
<input class=input type=text name=link_txt value="<?=$i['link_txt'];?>"> <br>

ID: <input class=sinput type=text name=link value="<?=$i['link'];?>"></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,16777216,$i['regdate'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[124]?>
</td><td><?=mysql2data($i['regdate'],0,1);?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,33554432,$i['editdate'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[123]?>
</td><td><?=mysql2data($i['editdate'],0,1);?></td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,67108864,$i['hdyfu'])) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[86]?></td>
<td>
<select name="hdyfu" class="input">
<?php  $p=0;while(isset($wu[$p])) {
($i['hdyfu'] == $p) ? $sel=" selected" : $sel="";
echo '<option value="'.$p.'"'.$sel.'>'.$wu[$p];$p++;}
?>
</select>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,134217728,1)) { $checked = ($ir['photo']) ? ' checked' : '';?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[510]?></td>
<td>
<input type=checkbox name="ir_photo"  <?=$checked?>>
</td></tr>
<?php }?>
<?php if(ShowForEdit(C_ADMINEDIT2,268435456,1)) { $checked = ($ir['audio']) ? ' checked' : '';?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[511]?></td>
<td>
<input type=checkbox name="ir_audio"  <?=$checked?>>
</td></tr>
<?php }?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><td colspan=2 align=right><input class=input type=submit value="<?=$w[99]?>" name="submit"> <input class=input type=submit OnClick="javascript:document.forms[0].status.value=7" value="<?=$x[526]?>" name="submit">
</td></tr>
<Tr align="center" bgcolor="<?=COLOR1?>"><Td colspan="2"><BR>
 <?php if(isset($fullmode)) {?><input type=hidden name=fullmode value="1"><?php }	?>
<?="<a href=\"javascript:open_win('".filename()."?id=".$i['id']."&do=al&ds=1".s()."','allow');\">[".$x[75]."]</a> &nbsp; <a href=\"javascript:open_win('".filename()."?id=".$i['id']."&ds=1&do=sp&".s()."','suspend');\">[".$x[853]."]</a> &nbsp; <a href=\"javascript:open_win('".filename()."?id=".$i['id']."&ds=1&do=dp&".s()."','delete');\">[".$x[66]."]</a> &nbsp; <a href=\"".filename()."?id=".$i['id']."&username=".$name."&do=smes&".s()."\">[".$w[170]."]</a> &nbsp; <a href=\"".filename()."?id=".$i['id']."&do=ue".$modelink."&".s()."\">[".$modeword."]</a>";?>
</td></tr>
</table></Td></Tr></table>
<br>
<?php 
				}
			}
			break;
			############################################
			# User edit form - End
			############################################
			############################################
			# User management - Begin
			############################################
		case 'um':
			if (!isset($a)) $a='';
			if ($a == "s") {
				if (!isset($step)) $step = 10;
				if (!isset($from)) $from = 0;
				if(!isset($id)) $id='';if(!isset($email)) $email='';
				if(!isset($username)) $username='';
				if(!isset($ip)) $ip='';
				$id=cb($id);$email=cb($email);$username=cb($username);$ip=ip2int($ip);
				if(!isset($agef) || !is_numeric($agef)) $agef = 0;
				if(!isset($aget) || !is_numeric($aget)) $aget = 255;

				if($agef > $aget) sprintm($w[109]);
				if(!isset($usertype)) $usertype = '-1';
				if(!isset($photo)) $photo='';if(!isset($phone)) $phone='';if(!isset($url)) $url='';if(!isset($icq)) $icq='';if(!isset($aim)) $aim='';
				if(C_HACK1) {
					if((!empty($id)&&!is_numeric($id))||!is_numeric($horo)||!is_numeric($step)||!is_numeric($from)||!is_numeric($gender)||!is_numeric($purpose)||!is_numeric($country)||!is_numeric($agef)||!is_numeric($aget)||!is_numeric($regin)||!is_numeric($sortby)||!is_numeric($userstat)||!is_numeric($usertype)) sprintm($w[1].'1',1);}
					if(C_HACK2) {
						if(($regin < 0)||($regin >= sizeof($wrg))||($regin >= sizeof($wrgv))||($horo < 0)||($horo >= sizeof($whr))||($gender < 0)||($gender >= sizeof($wg))||($purpose < 0)||($purpose >= sizeof($wp))||($country < 0)||($country >= sizeof($wcr))||($userstat < 0)||($userstat >= sizeof($wst))) sprintm($w[1].'2',1);}
						///////// Checking and creating for search
						switch ($sortby) {
							case "1":$msortby = " order by t1.editdate ASC";break;
							case "2":$msortby = " order by t1.fname DESC";break;
							case "3":$msortby = " order by t1.fname ASC";break;
							case "4":$msortby = " order by t1.birthday DESC";break;
							case "5":$msortby = " order by t1.birthday ASC";break;
							case "6":$msortby = " order by t1.height DESC";break;
							case "7":$msortby = " order by t1.height ASC";break;
							case "8":$msortby = " order by t1.weight DESC";break;
							case "9":$msortby = " order by t1.weight ASC";break;
							default:$msortby = " order by t1.status DESC, t1.editdate DESC";break;
						}
						// Important /////////////
						$id != "" ? $mid = " t1.id = '".$id."' AND t1.status >= '7' AND t1.id = t2.id AND t1.id = t3.id" : $mid = " t1.id = t2.id AND t1.id = t3.id";
						//////////////////////////
						!empty($horo) ? $mhoro=" AND t1.horo = '".$horo."'" : $mhoro = "";
						!($agef == 0) ? $magef=" AND t1.birthday <= DATE_SUB(NOW(), INTERVAL ".$agef." YEAR)" : $magef="";
						!($aget == 255) ? $maget=" AND t1.birthday >= DATE_SUB(NOW(), INTERVAL ".($aget+1)." YEAR)" : $maget="";
						!empty($gender) ? $mgender=" AND t1.gender = '".$gender."'" : $mgender = "";
						!empty($purpose) ? $mpurpose=" AND FIND_IN_SET(".cb(C_MYSQL_CHARSET)."'".$purpose."',t1.purposes)" : $mpurpose = "";
						!empty($country) ? $mcountry=" AND t1.country = '".$country."'" : $mcountry = "";
						!empty($email) ? $memail=" AND t3.email LIKE '%".SearchByLike($email)."%'" : $memail = "";
						!empty($username) ? $musername=" AND t1.username LIKE '%".SearchByLike($username)."%'" : $musername = "";
						!empty($ip) ? $mip = " AND t3.ip = ".$ip : $mip = "";
						!empty($password) ? $mpassword = " AND (t3.password = SHA2('".$password."', 256) OR t3.password = '".$password."')" : $mpassword = "";
						!empty($fingerprint) ? $mfingerprint = " AND t3.fingerprint = '".$fingerprint."'" : $mfingerprint = "";
						///////// Checking and creating for search
						switch ($userstat) {
							case "1":$muserstat = " AND t1.status = '0'";break;
							case "2":$muserstat = " AND (t1.status = '1' OR t1.status = '10')";break;
							case "3":$muserstat = " AND (t1.status = '2' OR t1.status = '9')";break;
							case "4":$muserstat = " AND (t1.status = '3' OR t1.status = '8')";break;
							case "5":$muserstat = " AND (t1.status = '5' OR t1.status = '6' OR t1.status = '4')";break;
							case "6":$muserstat = " AND t1.status >= '7'";break;
							default:$muserstat = "";break;
						}
						switch ($otherfilter){
							case "0": $motherfilter = " AND t1.coupon = ''";break;
							case "1": $motherfilter = " AND t1.regdate >= DATE_SUB(t1.editdate, INTERVAL 0 DAY)";break;
							default: $motherfilter = "";break;
						}
						!empty($regin) ? $mregin=" AND t1.regdate > DATE_SUB(NOW(), INTERVAL ".$wrgv[$regin]." DAY)" : $mregin = "";
						if(!isset($onlypriv)) $onlypriv = '';
						if($onlypriv == 'on') $musertype = " AND t1.req > '0'";
						else $musertype = ($usertype == '-1') ? '' : " AND t1.req = '".$usertype."'";

						(isset($photo) && ($photo == "on")) ? $mphoto = " AND t1.files != ''" : $mphoto = "";
						(isset($url) && ($url == "on")) ? $murl = " AND t2.url != ''" : $murl = "";
						(isset($icq) && ($icq == "on")) ? $micq = " AND t2.icq != ''" : $micq = "";
						(isset($aim) && ($aim == "on")) ? $maim = " AND t2.aim != ''" : $maim = "";
						(isset($phone) && ($phone == "on")) ? $mphone = " AND t2.phone != ''" : $mphone = "";

						$sql="SELECT t1.*, t3.ip, t3.fingerprint, t3.status, t3.password, ".CorrectDate("regdate").", ".CorrectDate("editdate")." FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2, ".C_MYSQL_MEMBERS_SEC." as t3 WHERE ".$mid.$mhoro.$magef.$maget.$mgender.$mpurpose.$mcountry.$memail.$musername.$mip.$mpassword.$mfingerprint.$mphoto.$murl.$micq.$maim.$mphone.$muserstat.$musertype.$motherfilter.$mregin.$msortby." limit ".$from.",".$step;
						$tsql = "SELECT count(t1.id) as total FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2, ".C_MYSQL_MEMBERS_SEC." as t3 WHERE ".$mid.$mhoro.$magef.$maget.$mgender.$mpurpose.$mcountry.$memail.$musername.$mip.$mpassword.$mfingerprint.$mphoto.$murl.$micq.$maim.$mphone.$muserstat.$musertype.$motherfilter.$mregin;
						
						$result = db_query($sql) or die(db_error());
						$tquery = db_query($tsql) or die(db_error());
						
						$trows = db_fetch_array($tquery);
						$count = $trows['total'];
						
						if($count == "0") sprintm($w[110]);
						$str=$color='';$usnum=$from+1;
						while ($i = db_fetch_array($result)) {
							$ipsql = "SELECT count(t3.id) as total FROM ".C_MYSQL_MEMBERS_SEC." as t3 WHERE ip = ".$i['ip']; 
							
							$ipquery = db_query($ipsql) or die(db_error());
						
							$iprows = db_fetch_array($ipquery);
							$i['ipcount'] = $iprows['total'];
							
							if ($i['fingerprint']){
								$fpsql = "SELECT count(t3.fingerprint) as total FROM ".C_MYSQL_MEMBERS_SEC." as t3 WHERE fingerprint = '".$i['fingerprint']."'"; 
							
								$fpquery = db_query($fpsql) or die(db_error());
							
								$fprows = db_fetch_array($fpquery);
								$i['fpcount'] = $fprows['total'];
							}
							
							$passsql = "SELECT count(t3.id) as total FROM ".C_MYSQL_MEMBERS_SEC." as t3 WHERE password = '".$i['password']."'"; 
							
							$passquery = db_query($passsql) or die(db_error());
						
							$passrows = db_fetch_array($passquery);
							$i['passcount'] = $passrows['total'];
							
							$picav = ($i['files'] == '') ? $w[111] : $w[112];
							$color = ($color == COLOR4) ? COLOR3 : COLOR4;
							$age=mysql2data($i['birthday'],1);
							//$name = (trim($i['fname']) == '') ? $i['id'] : $i['fname'];
							$name=username($i['id'],$i['username'],$i['fname'],$i['lname']);
							if($i['status'] != '7') $allow_link = "<a href=\"javascript:open_win('".filename()."?id=".$i['id']."&ds=1&do=al&".s()."','allow');\" class=desc>".$x[75]."</a>";
							else $allow_link = '<font color="green" face="Verdana">'.$x[114].'</font>';
							$str.="<Tr bgcolor=".$color." align=center class=desc><td>".$usnum."</td><td><a href=index.php?do=vp&ds=1&id=".$i['id']." class=desc target='_blank'>".$name."</a></td><td>".abs($age)."</td><td>".$wcr[$i['country']]."</td><td>".mysql2data($i['regdate'])."</td><td>".mysql2data($i['editdate'])."</td><td>".$picav."</td>";
							$str.="<td><a href='".filename()."?a=s&step=".C_APAGE."&id=&horo=0&agef=0&aget=255&gender=0&purpose=0&country=0&regin=0&sortby=0&do=um&userstat=0&ip=".int2ip($i['ip'])."&' target='_blank'>".int2ip($i['ip'])." [".$i['ipcount']."]</a></td>";
							if ($i['fingerprint']){
								$str.="<td><a href='".filename()."?a=s&step=".C_APAGE."&id=&horo=0&agef=0&aget=255&gender=0&purpose=0&country=0&regin=0&sortby=0&do=um&userstat=0&fingerprint=".$i['fingerprint']."&' target='_blank'>FPID [".$i['fpcount']."]</a></td>";
							} else {
								$str.="<td></td>";
							}							
							$str.="<td><a href='".filename()."?a=s&step=".C_APAGE."&id=&horo=0&agef=0&aget=255&gender=0&purpose=0&country=0&regin=0&sortby=0&do=um&userstat=0&password=".$i['password']."&' target='_blank'>PASS [".$i['passcount']."]</a></td>";
							$str.="<td>".$i['status']."</td><td>".$allow_link."</td><td><a href=\"".filename()."?id=".$i['id']."&do=ue&".s()."\" class=desc>".$x[60]."</a></td><td><a href=\"javascript:open_win('".filename()."?id=".$i['id']."&ds=1&do=dp&".s()."','delete');\" class=desc>".$x[66]."</a></td><td><input type='checkbox' name='act[]' value=".$i['id']."></td></tr>";
							$usnum++;
						}
						$param='l='.$l.'&a=s&do=um&id='.$id.'&horo='.$horo.'&agef='.$agef.'&aget='.$aget.'&gender='.$gender.'&purpose='.$purpose.'&country='.$country.'&email='.$email.'&username='.$username.'&userstat='.$userstat.'&photo='.$photo.'&url='.$url.'&icq='.$icq.'&aim='.$aim.'&phone='.$phone.'&regin='.$regin.'&sortby='.$sortby.'&usertype='.$usertype.'&onlypriv='.$onlypriv;
						$colspan=14;
						$str.=pages($from,$step,$count,$param,$colspan);
?>
<script language="JavaScript">
function sel() {
	with(document.form) {
		for(i=0;i<elements.length;i++) {
			thiselm = elements[i];
			if(thiselm.name.substring(0,3) == 'act')
			thiselm.checked = !thiselm.checked
		}
	}
}

function CheckAct() {
	with(document.form) {
		if(myactions.value == "" || myactions.value == "0") {
			alert("<?=$w[432]?>");
		} else {
			if(myactions.value >= 3 && myactions.value <= 4) {
				if(myactions.value == "3") {
					document.forms[0].post_action.value='approve';
					document.forms[0].submit();
				} else if(myactions.value == "4") {
					<?php if(C_ADMIN_REMOVE_CONFIRM) {?>
					if(confirm('<?=$x[662]?>')) {
						<?php }?>
						document.forms[0].post_action.value='remove';
						document.forms[0].submit();
						<?php if(C_ADMIN_REMOVE_CONFIRM) {?>
					}
					<?php }?>
				}
			}
			cnt=0;
			for(i=0;i<elements.length;i++) {thiselm = elements[i];
			if(thiselm.name.substring(0,3) == 'act') if(thiselm.checked) {
				cnt=cnt+1;
				v = thiselm.value;
				if(myactions.value >= 1 && myactions.value <= 2) {
					if(myactions.value == "1") {
						window.open("<?=C_URL?>/<?=ADM_DIR?>/index.php?do=vp&ds=1&id="+v, "_blank");
					} else if(myactions.value == "2") {
						window.open("<?=C_URL?>/<?=ADM_DIR?>/index.php?do=ue&id="+v, "_blank");
					}
				}
			}
			}
			if(cnt == 0) alert("<?=$w[555]?>");
		}
	}
}

</script>
<br>
<span class=head><?=search_results($from,$step,$count);?>
<?php
switch ($userstat) {
	case "2":$str0 = ': '.$x[78].'<br>';break;
	case "3":$str0 = ': '.$x[79].'<br>';break;
	case "4":$str0 = ': '.$x[80].'<br>';break;
	case "5":$str0 = ': '.$x[852].'<br>';break;
	default:$str0 = "";break;
}
echo $str0;
?>
</span>
<br>
<form action="?do=umar" method="post" name=form target='umar' name='umar' OnSubmit="javascript:open_win('','umar');">
<input type="hidden" name="a" value="s">
<input type="hidden" name="post_action" value="">
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="650" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="650" class=mes>
<Tr align="center" bgcolor="<?=COLORH?>"><Td>N</td><Td><?=$w[118]?></td>
<Td><?=$w[120]?></td><Td><?=$w[121]?></td><Td><?=$w[124]?></td><Td><?=$w[123]?></td><Td><?=$w[87]?></td><Td><?=$x[72]?></td><Td>Fingerprint</td><Td>Jelszó</td><Td><a href="<?="javascript:open_win('".filename()."?ds=1&do=sh&".s()."','help');"?>" class=desc>ST</a></td><Td><?=$x[75]?></td><Td><?=$x[60]?></td><Td><?=$x[66]?></td><Td><input type="checkbox" onClick="sel()"></td></Tr>
<?=$str;?>
<tr><td bgcolor="<?=COLORH?>" align=right colspan=14>
<?=$w[556]?> 
<select name=myactions class="select">
<option value=0> --------
<option value=1> <?=$w[554]?>
<option value=2> <?=$w[99]?>
<option value=3> <?=$x[592]?>
<option value=4> <?=$x[593]?>
</select> 
<input type="button" value="<?=$w[263]?>" class=button OnClick="javascript:CheckAct()"> 
</td></tr>
</table></td></tr></table>
</form>
<?php 
			} else {
?>
<form action="?<?=s()?>" method="post">
<input type="hidden" name="l" value="<?=$l?>">
<input type="hidden" name="a" value="s">
<input type="hidden" name="do" value="um">
<center><span class=head><?=$x[76]?></span>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="600" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="600" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[126]?></td>
<td><input type="text" name="id" class="minput"></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[60]?></td>
<td><input type="text" name="email" class="minput"></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[118]?></td>
<td><input type="text" name="username" class="minput"></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[54]?></td>
<td><input type="text" name="password" class="minput"></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[72]?></td>
<td><input type="text" name="ip" class="minput"></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$w[129]?></td>
<td><select name="horo" class="input">
<?php  $p=0; while(isset($whr[$p])) {echo '<option value="'.$p.'">'.$whr[$p];$p++;}?>
</select></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[120]?></td>
<td><input name="agef" class="sinput"> -  <input name="aget" class="sinput">
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[132]?></td>
<td>
<select name="gender" class="input">
<?php  $p=0;while(isset($wg[$p])) {echo '<option value="'.$p.'">'.$wg[$p];$p++;}
?>
</select>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[133]?></td>
<td>
<select name="purpose" class="input">
<?php  $p=0;while(isset($wp[$p])) {echo '<option value="'.$p.'">'.$wp[$p];$p++;}
?>
</select>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[121]?></td>
<td>
<select name="country" class="input">
<?php  $p=0;asort($wcr);reset($wcr);
while (list ($p, $val) = each ($wcr)) {
	echo '<option value="'.$p.'">'.$val;
}
?>
</select>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[22]?></td>
<td>
<select name="userstat" class="input">
<?php  $p=0;while(isset($wst[$p])) {echo '<option value="'.$p.'">'.$wst[$p];$p++;}
?>
</select>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[91]?></td>
<td>
<select name="usertype" class="input">
<option value="-1"><?=$x[236]?>
<?php  $p=0;while(isset($wpu[$p])) {echo '<option value="'.$p.'">'.$wpu[$p];$p++;}
?>
</select>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
Egyéb szűrő</td>
<td>
<select name="otherfilter" class="input">
<option value="-1"><?=$x[236]?>
<?php  $p=0;while(isset($wos[$p])) {echo '<option value="'.$p.'">'.$wos[$p];$p++;}
?>
</select>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[143]?></td>
<td>
<?=$w[87]?> <input type="checkbox" name="photo"><br>
<?=$w[144]?> <input type="checkbox" name="url"><br>
<?=$w[145]?> <input type="checkbox" name="icq"><br>
<?=$w[146]?> <input type="checkbox" name="aim"><br>
<?=$w[147]?> <input type="checkbox" name="phone">
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[148]?> </td>
<td>
<select name="regin" class="input">
<?php  $p=0;while(isset($wrg[$p])) {echo '<option value="'.$p.'">'.$wrg[$p];$p++;}
?>
</select>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[149]?> </td>
<td>
<select name="sortby" class="input">
<?php  $p=0;while(isset($wsb[$p])) {echo '<option value="'.$p.'">'.$wsb[$p];$p++;}
?>
</select>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$w[150]?> </td>
<td>
<select name="step" class="sinput">
<?php  for($p=10;$p<=50;$p+=10) {
	echo '<option>'.$p;}
?>
</select>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2>
<input type="submit" value="<?=$w[91]?>" class=button></td></tr>
</table></td></tr></table></form>
<?php
			}
			break;
			############################################
			# User management - End
			############################################
			############################################
			# Messaging management - Begin
			############################################
		case 'mess':
			if (!isset($a)) $a='';
			if ($a == "s") {
				if (!isset($step) || !is_numeric($step)) $step = 50;
				if (!isset($from) || !is_numeric($from)) $from = 0;
				if(!isset($fromid) || !is_numeric($fromid)) $id='0';
				if(!isset($toid) || !is_numeric($toid)) $toid='0';
				if(!isset($lastdays) || !is_numeric($lastdays)) $lastdays='0';
				$keywords=cb($keywords);
				$msortby = " order by id desc";

				// Important /////////////
				$mid = " id != '0'";
				$mfromid = (!empty($fromid)) ? " AND (fromid = '".$fromid."' OR toid = '".$fromid."')" : "";
				$mtoid = (!empty($toid)) ? " AND (fromid = '".$toid."' OR toid = '".$toid."')" : "";
				$mlastdays = (!empty($lastdays)) ? " AND sendtime > DATE_SUB(NOW(), INTERVAL ".$lastdays." DAY)" : "";
				$mkeywords = (!empty($keywords)) ? " AND (message LIKE '%".SearchByLike($keywords)."%' OR subject LIKE '".SearchByLike($keywords)."')" : "";
				$exeption = " AND subject <> '".$w['236']."'";

				$sql="SELECT * ,".CorrectDate("sendtime")." FROM ".C_MYSQL_MESSAGES." WHERE ".$mid.$mfromid.$mtoid.$mlastdays.$mkeywords.$exeption.$msortby." limit ".$from.",".$step;
				$tsql = "SELECT count(*) as total FROM ".C_MYSQL_MESSAGES." WHERE ".$mid.$mfromid.$mtoid.$mlastdays.$mkeywords.$exeption;
				$result = db_query($sql) or die(db_error());
				$tquery = db_query($tsql) or die(db_error());
				$trows = db_fetch_array($tquery);
				$count = $trows['total'];
				if($count == "0") sprintm($w[110]);
				$str=$color='';
				include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';

				while ($i = db_fetch_array($result)) {
					$color = ($color == COLOR4) ? COLOR3 : COLOR4;
					$messcolor = ($i['fromid'] == $fromid) ? 'color: #000000;' : 'color: #ffff00; font-weight: bold;';
					$senddate=mysql2data($i['sendtime'], 0, 1);
					
					$dir = ((integer)floor(($i['id'])/10000));
					$file = C_PATH.'/members/uploads/messages/'.$dir.'/'.$i['id'];
					
					include_once C_PATH.'/classes/crypt.class.php';
					$cr64 = new AzDGCrypt(C_CRYPT_KEY);
				
					if (file_exists($file))
						$i['attachment'] = C_URL . '/pic.php?c=' . $cr64->crypt('messages/'.$dir.'/'.$i['id']).'&bigger';
					else
						$i['attachment'] = '';
					
					$message = read_message($i['id']);
					if ($message) $i['message'] = $message;
					else $i['message'] = '';
					
					$str.="<Tr bgcolor=".$color." align=left class=desc style=\"".$messcolor."\"><td>".$i['id']."</td><td><a href=index.php?do=vp&ds=1&id=".$i['fromid']." class=desc target='_blank'>".$i['fromid']."</a></td><td><a href=index.php?do=vp&ds=1&id=".$i['toid']." class=desc target='_blank'>".$i['toid']."</a></td><td><B>".$w[168]."</B>: ".$i['subject']."<BR><B>".$w[169]."</B>: ".AzDGCode($i['message'])."<BR>";
					
					if ($i['attachment'] != ''){
						$str.='<img src="'.$i['attachment'].'" style="max-width: 100%; margin: 0 auto;"/><BR>';
					}
					
					$str.="<B>".$w[229]."</B>: ".$senddate."</td><td><input type='checkbox' name='act[]' value=".$i['id']."></td></tr>";
				}
				$param='l='.$l.'&a=s&do=mess&fromid='.$fromid.'&toid='.$toid.'&lastdays='.$lastdays.'&keywords='.urlencode($keywords);
				$colspan=4;
				$str.=pages($from,$step,$count,$param,$colspan);
				$str.="<tr><td colspan=4 align=right> <input type='submit' value='".$x[594]."' name='remove' class=button> </td></tr></table></td></tr></table>";
?>
<script language="JavaScript">
function sel() {
	with(document.form) {
		for(i=0;i<elements.length;i++) {
			thiselm = elements[i];
			if(thiselm.name.substring(0,3) == 'act')
			thiselm.checked = !thiselm.checked
		}
	}
}
</script>
<br>
<span class=head><?=search_results($from,$step,$count);?></span>
<br>
<form action="?do=messr" method="post" name=form target='messr' name='messr' OnSubmit="javascript:open_win('','messr');">
<input type="hidden" name="a" value="s">
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="650" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="650" class=mes>
<Tr align="center" bgcolor="<?=COLORH?>"><Td><?=$x[589]?></td>
<Td>#</td><Td><?=$x[588]?></td><Td><?=$w[169]?></td><Td><input type="checkbox" onClick="sel()"></td></Tr>
<?=$str;?>
</form>
<?php 
			} else {
?><BR>
<form action="?<?=s()?>" method="post">
<input type="hidden" name="l" value="<?=$l?>">
<input type="hidden" name="a" value="s">
<input type="hidden" name="do" value="mess">
<center><span class=head><?=$x[533]?></span><BR><BR>

<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="500" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="500" class=mes>
<Tr align="left" bgcolor="<?=COLOR1?>"><Td><?=$x[589]?></td>
<td><input type="text" name="fromid" class="sinput"></td></tr>
<Tr align="left" bgcolor="<?=COLOR1?>"><Td><?=$x[588]?></td>
<td><input type="text" name="toid" class="sinput"></td></tr>
<Tr align="left" bgcolor="<?=COLOR1?>"><Td><?=$w[289]?><BR>
<?=$x[591]?></td>
<td><input type="text" name="keywords" class="input"></td></tr>
<Tr align="left" bgcolor="<?=COLOR1?>"><Td><?=$x[590]?></td>
<td><input type="text" name="lastdays" class="sinput"></td></tr>
<Tr align="left" bgcolor="<?=COLOR1?>"><Td colspan=2>
<input type="submit" value="<?=$w[91]?>" class=button></td></tr>
</table></td></tr></table></form>
<?php
			}
			break;
			############################################
			# Messaging management - End
			############################################
			############################################
			# Blacklist manager - Begin
			############################################
		case 'blacklist':
			if (!isset($a)) $a='';
			if ($a == "s") {
				if(isset($bword) && empty($bword)) sprintm($w[164]);
				if(isset($bl_type) && !is_numeric($bl_type)) $bl_type=0;
				$bword=cb($bword);
				$barr=explode(',',$bword);
				$p=0;$str='';while(isset($barr[$p]))
				{
					if(!empty($barr[$p])) $str.="('','".$bl_type."','".$barr[$p]."'),";
					$p++;}

					db_query("REPLACE INTO ".C_MYSQL_BLACKLIST." VALUES ".substr($str,0,-1));
					sprintm($x[516]);

			} elseif($a == 'del') {
				if(isset($del)) {
					db_query("DELETE FROM ".C_MYSQL_BLACKLIST." WHERE id IN (".cb(implode(',',$del)).")");
					sprintm($x[472]);
				}
			} else {
?>
<form action="?<?=s()?>" method="post">
<input type="hidden" name="l" value="<?=$l?>">
<input type="hidden" name="a" value="s">
<input type="hidden" name="do" value="blacklist">
<BR>
<center><span class=head><?=$x[512]?></span><BR><BR>


<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="400" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="400" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<?=$x[513]?></td>
<td>
<select name="bl_type" class="minput">
<?php  $p=0;while(isset($xbl[$p])) {echo '<option value="'.$p.'">'.$xbl[$p];$p++;}
?>
</select>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td><?=$x[514]?></td>
<td><input type="text" name="bword" class="input"></td></tr>
<Tr align="left" bgcolor="<?=COLOR1?>"><Td colspan=2><?=$x[515]?></td></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td colspan=2>
<input type="submit" value="<?=$x[5]?>" class=button></td></tr>
</table></td></tr></table></form>
<BR>
<BR>
<BR>
<script language="JavaScript">
function sel() {
	with(document.form) {
		for(i=0;i<elements.length;i++) {
			thiselm = elements[i];
			if(thiselm.name.substring(0,3) == 'del')
			thiselm.checked = !thiselm.checked
		}
	}
}
</script>
<?php
$count=db_fetch_row(db_query("SELECT count(*) FROM ".C_MYSQL_BLACKLIST));
if(!$count[0]) die();
else
{
	if(!isset($from) || !is_numeric($from)) $from=0;
	if(!isset($step) || !is_numeric($step)) $step=C_APAGE;
	$str='';$color='';
	$result=db_query("SELECT * FROM ".C_MYSQL_BLACKLIST." limit ".$from.",".$step);
	while ($i = db_fetch_array($result)) {
		$color = ($color == COLOR4) ? COLOR3 : COLOR4;
		$btype=$xbl[$i['bid']];
		$bword = $i['word'];
		$str.="<Tr bgcolor=".$color." align=center><td>".$btype."</td><td>".$bword."</td><td><input type='checkbox' name='del[]' value='".$i['id']."'></td></tr>";
	}
	$param='do=blacklist';
	$colspan=4;
	$str.=pages($from,$step,$count[0],$param,$colspan);
	$str.="<Tr bgcolor=".$color." align=center><td colspan=3><BR><input type='submit' value='".$x[664]."' name='remove' class=button></td></tr></table></td></tr></table>";
?>
<br>
<span class=head><?=search_results($from,$step,$count[0],1,$x[517]);?></span>
<br><br>
<form action="?<?=s()?>"  method="post" name=form target='blacklist' OnSubmit="javascript:open_win('','blacklist');">
<input type="hidden" name="l" value="<?=$l?>">
<input type="hidden" name="ds" value="1">
<input type="hidden" name="a" value="del">
<input type="hidden" name="do" value="blacklist">
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="400" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="400" class=mes>
<Tr align="center" bgcolor="<?=COLORH?>">
<Td><?=$x[513]?></td><Td><?=$x[514]?></td><Td><?=$x[66]?><input type="checkbox" onClick="sel()"></td></Tr>
<?=$str;?>
</form>
<?php
}
			}
			break;
			############################################
			# Blacklist manager - End
			############################################
			############################################
			# User management - Begin
			############################################
		case 'umar':
			if (!isset($a)) $a='';
			if ($a == "s") {
				#	if(isset($_POST['approve'])) {
				if(isset($_POST['post_action']) && $_POST['post_action'] == 'approve') {
					# Approving profiles
					$p=0;$str='';
					ApproveUser($act);
					while(isset($act[$p])) {
						$str.=template($x[71],array($act[$p])).'<br>';
						$p++;
					}
					if($str != '') sprintm($str);

				}
				#	elseif(isset($_POST['remove'])) {
				elseif(isset($_POST['post_action']) && $_POST['post_action'] == 'remove') {
					# Removing profiles
					$p=0;$str='';
					RemoveUser($act,C_ADMIN_REMOVE_CHECKBOX_MAIL);
					while(isset($act[$p])) {
						$str.=template($x[69],array($act[$p])).'<br>';
						$p++;
					}
					if($str != '') sprintm($str);

				}
			}
			break;
			############################################
			# User management - End
			############################################
			############################################
			# Message management - Begin
			############################################
		case 'messr':
			if (!isset($a)) $a='';
			if ($a == "s") {
				if(isset($_POST['remove'])) {
					# Removing profiles
					$p=0;$str='';
					RemoveMessages($act);
					while(isset($act[$p])) {
						$str.=template($x[595],array($act[$p])).'<br>';
						$p++;
					}
					if($str != '') sprintm($str);

				}
			}
			break;
			############################################
			# Message management - End
			############################################
			############################################
			# Clear Bad Profiles table - Begin
			############################################
		case 'br':
		    /*$tmp = db_query("SELECT * FROM ".C_MYSQL_BAD_PROFILES);
			$upload_folder = C_PATH.'/bad/';
			while($i=db_fetch_array($tmp)) {
				unlink($upload_folder.$i['id'].".zip");
			}*/
			//db_query("DELETE FROM ".C_MYSQL_BAD_PROFILES) or die(db_error());
                        db_query("UPDATE ".C_MYSQL_BAD_PROFILES." SET status = '1' WHERE status = '0'");
			sprintm($x[472],2);
			break;
			############################################
			# Clear Bad Profiles table - End
			############################################
			############################################
			# Online Users - Begin
			############################################
		case 'ou':
			#online_users();online_quests();
			include_once(C_PATH.'/languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'o.php');
			if(online_users() == '0') sprintm($w[189],1);
?>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td width="<?=C_WIDTH?>" bgcolor="<?=COLOR1?>" valign="top">
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
<Tr><Td bgcolor="<?=C_TBCOLOR?>"></Td></Tr>
<Tr align="center" bgcolor="<?=COLORH?>"><Td>
<?=$w[105]?><?=online_users()?>
</Td></Tr>
<Tr><Td bgcolor="<?=C_TBCOLOR?>"></Td></Tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td>
<table width="100%" class=tr>
<tr class=mes bgcolor="<?=COLORH?>" align=center><td><?=$w[118]?></td><td><?=$w[119]?></td><td><?=$w[120]?></td><td><?=$w[121]?></td><td><?=$w[122]?></td><td><?=$w[443]?></td><td><?=$w[87]?></td></tr>
<?php 
$tmp=db_query("SELECT DISTINCT t1.user, t1.upage, t2.* FROM ".C_MYSQL_ONLINE_USERS." as t1, ".C_MYSQL_MEMBERS_MAIN." as t2 WHERE t1.user = t2.id order by t2.regdate") or die(db_error());
$color='';
while($i=db_fetch_array($tmp)) {
	$color = ($color == COLOR4) ? COLOR3 : COLOR4;
	$name=username($i['id'],$i['username'],$i['fname'],$i['lname']);
	if($i['files'] == '') $ph = $w[111];
	else {
		if(C_SEARCH_PHOTO) {
			$photo=ShowPic(not_empty($i['files'],$i['id']));
			$link = (C_SEARCH_PHOTO_LINK) ? 'index.php?do=vp&ds=1&id='.$i['id'] : $photo.ShowPictureBigger();
			$ph='<a href="'.$link.'" target="_blank"><img src="'.$photo.'" border="'.C_IMG_BRDR.'" width="'.C_SEARCH_PHOTO_WIDTH.'"></a>';
		} else $ph = $w[112];
	}
	$page='';reset($wo);
	while (list ($key, $val) = each ($wo)) {
		if(strpos($i['upage'], $key))
		{
			$page=$val;
			break;
		}
	}
	$pages = ($page == '') ? '' : $page;
	$age=abs(mysql2data($i['birthday'],1));
	echo "<tr class=desc bgcolor=\"".$color."\" align=center><td><a href=\"index.php?do=vp&ds=1&id=".$i['id']."\" class=desc target=\"_blank\">".$name."</a></td><td>".$wg[$i['gender']]." ".$wp[$i['purposes']]."</td><td>".$age."</td><td>".$wcr[$i['country']]."</td><td>".$i['city']."</td><td>".$pages."</td><td>".$ph."</td></tr>";
}
?>
</table></Td></Tr></Table></Td></Tr></Table>
<?php
break;
############################################
# Online Users - End
############################################
############################################
# Bad Profiles - Begin
############################################
		case 'bp':
			if (!isset($step) || !(is_numeric($step))) $step = 10;
			if (!isset($from) || !(is_numeric($from))) $from = 0;
			$msortby = " order by date DESC";
			$count = db_fetch_row(db_query("SELECT count(*) as total FROM ".C_MYSQL_BAD_PROFILES." WHERE status = '0'"));
			$sql = "SELECT * FROM ".C_MYSQL_MEMBERS_MAIN.",".C_MYSQL_BAD_PROFILES." WHERE ".C_MYSQL_MEMBERS_MAIN.".id=".C_MYSQL_BAD_PROFILES.".user AND ".C_MYSQL_BAD_PROFILES.".status = '0'".$msortby." limit ".$from.",".$step;
                        $result2 = db_query($sql) or die(db_error());
			$str = $color = '';
			#$count = db_num_rows($result2);
			while ($i = db_fetch_array($result2))
			{
				$color = ($color == COLOR4) ? COLOR3 : COLOR4;
				$name = username($i['id'], $i['username'], $i['fname'], $i['lname']);
				if ($i['fromid'] != '0' )
				{
					$sql2 = db_query("SELECT * FROM ".C_MYSQL_MEMBERS_MAIN." WHERE id=".$i['fromid']);
					while ($ii = db_fetch_array($sql2))
					{
						$sname = username($ii['id'], $ii['username'], $ii['fname'], $ii['lname']);
						$sname = "<a href=index.php?do=vp&ds=1&id=".$i['fromid']." class=desc target='_blank'>".$sname."</a>";
					}
					
					$banlist = db_fetch_row(db_query("SELECT * FROM ".C_MYSQL_BANLIST." WHERE myid=".$i['fromid']." AND user=".$i['user']));
					
					if ($banlist > 0){
						$banned = "Letiltva";
					} else {
						$banned = "<a href=\"javascript:open_win('".filename()."?myid=".$i['fromid']."&user=".$i['user']."&ds=1&do=apb&".s()."','delete');\" class=desc>Letiltás</a>";
					}
					
				}
				else $sname = $w[295];
				$file = C_PATH.'/bad/'.$i['id'].'.zip';
				if (file_exists($file)){
					$download = "<a href=\"".C_URL.'/bad/'.$i['id'].".zip\" target=\"_blank\">Letöltés</a>";
				} else {
					$download = "Nincs file";
				}
				
				$str .= "<Tr bgcolor=".$color." align=center class=desc><td><a href=index.php?do=vp&ds=1&id=".$i['user']." class=desc target='_blank'>".$name."</a></td><td align=left>".$i['reason']."</td><td>".$sname."</td><td>".mysql2data($i['date'])."</td><td>".$download."</td><td><a href=\"".filename()."?l=default&a=s&do=mess&fromid=".$i['user']."&toid=".$i['fromid']."&".s()."\" class=desc target='_blank'>".$w[96]."</a></td><td>".$banned."</td><td><a href=\"".filename()."?id=".$i['user']."&do=ue&".s()."\" class=desc>".$x[60]."</a></td><td><a href=\"javascript:open_win('".filename()."?id=".$i['user']."&ds=1&do=sp&".s()."','delete');\" class=desc>".$x[853]."</a></td><td><a href=\"javascript:open_win('".filename()."?id=".$i['user']."&ds=1&do=dp&".s()."','delete');\" class=desc>".$x[66]."</a></td></tr>";
			}
			$param = 'l='.$l.'&do=bp';
			$colspan = 7;
			$str .= pages($from, $step, $count[0], $param, $colspan);
			$str .= "</table></td></tr></table>";
?>
<span class=head><?=search_results($from,$step,$count[0]);?></span>
<br><br>
<center><a href="javascript:open_win('<?=C_URL?>/<?=ADM_DIR?>/index.php?ds=1&do=br','br');" class=desc>[Clear Bad Profiles Table]</a></center>
<br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="600" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="600" class=mes>
<Tr align="center" bgcolor="<?=COLORH?>"><Td><?=$w[118]?></td>
<Td><?=$x[401]?></td><Td><?=$w[294]?></td><Td><?=$w[229]?></td><Td>Bizonyíték</td><Td>Beszélgetés</td><Td>Letiltás</td><Td><?=$x[60]?></td><Td><?=$x[853]?></td><Td><?=$x[66]?></td></Tr>
<?=$str;?>
<?php
break;
############################################
# Bad Profiles - End
############################################
			############################################
			# Main page - Begin
			############################################
		default:
?>

<Table CellSpacing="0" CellPadding="8" width="100%"><Tr><Td width=50% align=center valign=top>
<Table CellSpacing="1" CellPadding="0" width="100%" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="1" CellPadding="<?=C_CELLP?>" width="100%" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLORH?>"><Td width=100% colspan=2 align=center>
<strong><?=$x[575]?></strong>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=70%>
<a href="<?=filename()?>?a=s&step=<?=C_APAGE?>&id=&horo=0&agef=0&aget=255&gender=0&purpose=0&country=0&regin=0&sortby=0&do=um&userstat=2&"><?=$x[78]?></a>:
</td>
<td>
<?php 
$tmp=db_query("SELECT count(id) as total FROM ".C_MYSQL_MEMBERS_MAIN." WHERE status = '1' OR status = '10'");
$rows=db_fetch_array($tmp);
$total=$rows['total'];
echo $total;
?>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=70%>
<a href="<?=filename()?>?a=s&step=<?=C_APAGE?>&id=&horo=0&agef=0&aget=255&gender=0&purpose=0&country=0&regin=0&sortby=0&do=um&userstat=3&"><?=$x[79]?></a> :
</td>
<td>
<?php 
$tmp=db_query("SELECT count(id) as total FROM ".C_MYSQL_MEMBERS_MAIN." WHERE status = '2' OR status = '9'");
$rows=db_fetch_array($tmp);
$total=$rows['total'];
echo $total;
?>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=70%>
<a href="<?=filename()?>?a=s&step=<?=C_APAGE?>&id=&horo=0&agef=0&aget=255&gender=0&purpose=0&country=0&regin=0&sortby=0&do=um&userstat=5&"><?=$x[852]?></a> :
</td>
<td>
<?php 
$tmp=db_query("SELECT count(id) as total FROM ".C_MYSQL_MEMBERS_MAIN." WHERE status = '5'");
$rows=db_fetch_array($tmp);
$total=$rows['total'];
echo $total;
?>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=70%>
<a href="<?=filename()?>?a=s&step=<?=C_APAGE?>&id=&horo=0&agef=0&aget=255&gender=0&purpose=0&country=0&regin=0&sortby=0&do=um&userstat=4&"><?=$x[80]?></a> :
</td>
<td>
<?php 
$tmp=db_query("SELECT count(id) as total FROM ".C_MYSQL_MEMBERS_MAIN." WHERE status = '3' or status = '8'");
$rows=db_fetch_array($tmp);
$total=$rows['total'];
echo $total;
?>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=70%>
<a href="<?=filename()?>?do=ou&"><?=$w[105]?></a> :
</td>
<td>
<?=online_users()?>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=70%>
<a href="<?=filename()?>?do=bp&"><?=$w[218]?></a> :
</td>
<td>
<?php 
$total=db_fetch_row(db_query("SELECT COUNT(*) as total FROM ".C_MYSQL_BAD_PROFILES." WHERE status = '0'"));
echo $total[0];
?>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=70%>
<a href="<?=filename()?>?ds=1&do=lqe&find=s&title=&keywords=&descr=&uname=&email=&rec=&url=&category=0&id=&status=2&req=&"><?=$x[980]?></a> :
</td>
<td>
<?php 
$total=db_fetch_row(db_query("SELECT COUNT(*) as total FROM ".C_MYSQL_LINKS." where status = '2'"));
echo $total[0];
?>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=70%>
<a href="<?=filename()?>?do=tm&"><?=$w[633]?></a> :
</td>
<td>
<?php 
$total=db_fetch_row(db_query("SELECT COUNT(*) as total FROM ".C_MYSQL_TESTIMONIALS." where status = '7'"));
echo $total[0];
$total=db_fetch_row(db_query("SELECT COUNT(*) as total FROM ".C_MYSQL_TESTIMONIALS." where status != '7'"));
if($total[0]) echo ' <b>('.$total[0].')</b>';
?>
</td></tr></table></Td></Tr></table>
</td>
<td width=50% align=center valign=top></Td></Tr>

<Tr><Td width=50% align=center valign=top>
<Table CellSpacing="1" CellPadding="0" width="100%" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="1" CellPadding="<?=C_CELLP?>" width="100%" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLORH?>"><Td width=100% colspan=2 align=center>
<strong><?=$x[577]?></strong>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=70%><?=$x[578]?></td>
<td><?=$C_VERSION?></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=70%><?=$x[648]?></td>
<td><?=$C_RELEASE_DATE?></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=70%>
<?=$x[77]?>
</td>
<td>
<?php 
$tmp=db_query("SELECT count(id) as total FROM ".C_MYSQL_MEMBERS_MAIN." WHERE status >= '7'");
$rows=db_fetch_array($tmp);
$total=$rows['total'];
$tmp=db_query("SELECT max(id) as maxid FROM ".C_MYSQL_MEMBERS_MAIN);
$rows=db_fetch_array($tmp);
$maxid=$rows['maxid'];
echo $total;
?>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=70%>
<?=$x[579]?>
</td>
<td>
<?php 
$tmp=db_query("SELECT count(*) as total FROM ".C_MYSQL_MESSAGES);
$rows=db_fetch_array($tmp);
$total=$rows['total'];
echo $total;
?>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=70%>
<?=$x[983]?>
</td>
<td>
<?php 
$tmp=db_query("SELECT count(*) as total FROM ".C_MYSQL_MESSAGES_ARCHIVE);
$rows=db_fetch_array($tmp);
$total=$rows['total'];
echo $total;
?>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=70%><?=$x[982]?></td><td>
<?php  $tmp=db_fetch_array(db_query("SELECT count(*) as total FROM ".C_MYSQL_LINKS." WHERE status >= '7'"));echo $tmp['total'];?></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=70%>
<?=$x[647]?>
</td>
<td>
<a href="http://www.azdg.com/help/?script=AzDGDatingMedium&version=<?=C_VERSION?>" target="_blank"><?=$x[649]?></a>, <a href="http://www.azdg.com/faq/?script=AzDGDatingMedium&version=<?=C_VERSION?>" target="_blank"><?=$w[93]?></a>
</td></tr>
</table></Td></Tr></table>
</td>
<td width=50% align=center valign=top><Table CellSpacing="1" CellPadding="0" width="100%" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="1" CellPadding="<?=C_CELLP?>" width="100%" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLORH?>"><Td width=100% colspan=2 align=center>
<strong><?=$x[587]?></strong>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=40%><?=$x[985]?></td>
<td><?=php_uname();?></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=40%><?=$x[986]?></td>
<td><?=php_sapi_name();?></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=40%><?=$x[582]?></td>
<td><a href="<?=filename()?>?ds=1&do=phpinfo&" target="_blank"><?=phpversion();?></a></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=40%>
<?=$x[583]?>
</td>
<td>
<?=db_get_server_info();?>
</td></tr>

<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=40%>
<?=$x[585]?>
</td>
<td>
<?php echo (ini_get('safe_mode')) ? $x[668] : $x[667]; 
?>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=40%>
<?=$x[586]?>
</td>
<td>
<?php  echo (extension_loaded('gd')) ? $x[668] : $x[667]; ?>
</td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>"><Td width=40%><?=$x[984]?></td><td><?php  echo @ini_get("session.gc_maxlifetime"); ?></td></tr>
</table></Td></Tr></table></Td></Tr>
</table>
<?php 
CheckUploadDirectories($maxid,True);
break;
############################################
# Main page - End
############################################
	}
	?><br><?php 
	include_once C_PATH.'/sfooter.php';
}?>