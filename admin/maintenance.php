<?php
//error_reporting(0);

include_once '../include/config.inc.php';
include_once C_PATH.'/include/options.inc.php';

# Including not once, because it will be required in "Mailing to subscribed users" section

include(C_PATH.'/languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'.php');
include(C_PATH.'/languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'l.php');
include(C_PATH.'/languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'_.php');
include(C_PATH.'/languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'a.php');
include_once C_PATH.'/include/functions.inc.php';

# Compability with moder area
define('ADM_DIR',C_ADMINDIR);

# Version
define('AC_VERSION','Medium 1.9.4');
define('AC_RELEASE_DATE',@$wmm[2].' 22, 2009');

define('C_VERSION','1.9.4');
define('C_RELEASE_DATE',@$wmm[2].' 22, 2009');

# Setting version
$C_VERSION = AC_VERSION;
$C_RELEASE_DATE = AC_RELEASE_DATE;

$wpu=array($wpu[0]); // Medium version allow to use Free membership only

### Maintenance code - run every time when admin logins into admin area

# Remove Profile views older than C_WHOSEENTIME days
@db_query("DELETE FROM ".C_MYSQL_PROFILEVIEW." WHERE visdate < DATE_SUB(NOW(), INTERVAL ".C_WHOSEENTIME." DAY)");
@db_query("REPAIR TABLE ".C_MYSQL_PROFILEVIEW);@db_query("OPTIMIZE TABLE ".C_MYSQL_PROFILEVIEW);

# Remove profiles waiting to remove
if(C_REMOVE_USERS_IN_DAYS)
{
    $before = date("Y-m-d H:i:s", time() - C_REMOVE_USERS_IN_DAYS * 86400);
	$tmp=db_query("SELECT id FROM ".C_MYSQL_MEMBERS_MAIN." WHERE editdate <= '".$before."' AND status IN (3, 4, 8)") or die(db_error());
    $cnt = db_num_rows($tmp);
    if($cnt)
    {
        $arrids=array();
        while ($i = db_fetch_array($tmp)) {$arrids[]=$i['id'];}
        RemoveUser($arrids,C_ADMIN_REMOVE_CHECKBOX_MAIL);
    }

}

/*
SELECT * FROM 19_membersu_main WHERE id = X;
SELECT * FROM 19_membersu_ext WHERE id = X;
SELECT * FROM 19_membersu_sec WHERE id = X;
SELECT * FROM 19_membersu_restricts WHERE id = X;
SELECT * FROM 19_temp WHERE id = X;
SELECT * FROM 19_hits WHERE id = X;
SELECT * FROM 19_options WHERE id = X;
SELECT * FROM 19_onlineu WHERE user = X;
SELECT * FROM 19_new_photos WHERE uid = X;
SELECT * FROM 19_search WHERE uid = X;
SELECT * FROM 19_messages WHERE fromid = X or toid = X;
SELECT * FROM 19_comments WHERE fromid = X or toid = X;
SELECT * FROM 19_room WHERE user = X or myid = X;
SELECT * FROM 19_banlist WHERE user = X or myid = X;
SELECT * FROM 19_addressbook WHERE user = X or myid = X;
SELECT * FROM 19_bad WHERE user = X;
SELECT * FROM 19_profileview WHERE myid = X or visid = X;
SELECT * FROM 19_testimonials WHERE uid = X;
 */

#Remove profile who visit 3 months ago
$day = 91;
$before = date("Y-m-d H:i:s", time() - $day * 86400);
$tmp=db_query("SELECT t1.id, t1.status, t2.email FROM ".C_MYSQL_MEMBERS_MAIN." as t1 LEFT JOIN ".C_MYSQL_MEMBERS_SEC." as t2 ON t1.id = t2.id WHERE editdate <= '".$before."'") or die(db_error());
$cnt = db_num_rows($tmp);
if($cnt)
{
    $arrids=array();
    while ($i = db_fetch_array($tmp)) {
		if (!in_array($i['status'], array('5', '6'))){
			$arrids[]=$i['id'];

            $save_log = empty_log_array();
            $save_log['time'] = date('Y-m-d H:i:s');
            $save_log['id'] = $i['id'];
            $save_log['email'] = $i['email'];
            $save_log['action'] = 'Státuszmódosítás: 4';
            log_csv(array($save_log));
		}
	}
    //RemoveUser($arrids);
    //$remstat = (C_REMOVE_ALLOW == '1') ? '8' : '3';
	if (count($arrids) > 0){
		@db_query("UPDATE ".C_MYSQL_MEMBERS_MAIN." SET status='4', username = CONCAT_WS('', '#', username), editdate='".date("Y-m-d H:i:s")."' WHERE id IN (".implode(', ',$arrids).")");
		@db_query("UPDATE ".C_MYSQL_MEMBERS_EXT." SET status='4' WHERE id IN (".implode(', ',$arrids).")");
		@db_query("UPDATE ".C_MYSQL_MEMBERS_SEC." SET status='4', email = CONCAT_WS('', '#', email) WHERE id IN (".implode(', ',$arrids).")");
		@db_query("UPDATE ".C_MYSQL_ADDRESSBOOK." SET status='4' WHERE myid IN (".implode(', ',$arrids).")");
	}
    /*while ($i = db_fetch_array($tmp)) {
        ChangeStatus($i['id'],$remstat);
    }*/
}

#Remove profile who never visits after 7 days ago
$day = 7;
$before = date("Y-m-d H:i:s", time() - $day * 86400);
$day2 = $day + 0.5;
$before2 = date("Y-m-d H:i:s", time() - $day2 * 86400);

$tmp=db_query("SELECT t1.id, t1.status, t2.email FROM ".C_MYSQL_MEMBERS_MAIN." as t1 LEFT JOIN ".C_MYSQL_MEMBERS_SEC." as t2 ON t1.id = t2.id WHERE editdate <= '".$before."' AND regdate >= '".$before2."'") or die(db_error());
$cnt = db_num_rows($tmp);
if($cnt)
{
    $arrids=array();
    while ($i = db_fetch_array($tmp)) {
        if (!in_array($i['status'], array('5', '6'))){
            $arrids[]=$i['id'];

            $save_log = empty_log_array();
            $save_log['time'] = date('Y-m-d H:i:s');
            $save_log['id'] = $i['id'];
            $save_log['email'] = $i['email'];
            $save_log['action'] = 'Státuszmódosítás: 4';
            log_csv(array($save_log));
        }
    }
    //RemoveUser($arrids);
    //$remstat = (C_REMOVE_ALLOW == '1') ? '8' : '3';
    if (count($arrids) > 0){
        @db_query("UPDATE ".C_MYSQL_MEMBERS_MAIN." SET status='4', username = CONCAT_WS('', '#', username), editdate='".date("Y-m-d H:i:s")."' WHERE id IN (".implode(', ',$arrids).")");
        @db_query("UPDATE ".C_MYSQL_MEMBERS_EXT." SET status='4' WHERE id IN (".implode(', ',$arrids).")");
        @db_query("UPDATE ".C_MYSQL_MEMBERS_SEC." SET status='4', email = CONCAT_WS('', '#', email) WHERE id IN (".implode(', ',$arrids).")");
        @db_query("UPDATE ".C_MYSQL_ADDRESSBOOK." SET status='4' WHERE myid IN (".implode(', ',$arrids).")");
    }
    /*while ($i = db_fetch_array($tmp)) {
        ChangeStatus($i['id'],$remstat);
    }*/
}

# Remove old messages
if(C_REMOVE_MESSAGE_IN_DAYS)
{
    $before = date("Y-m-d H:i:s", time() - C_REMOVE_MESSAGE_IN_DAYS * 86400);
    $tmp=db_query("SELECT id FROM ".C_MYSQL_MESSAGES." WHERE sendtime <= '".$before."' ORDER BY id ASC") or die(db_error());

    $cnt = db_num_rows($tmp);
    if($cnt)
    {
        while ($i = db_fetch_array($tmp)) {
            //echo "<pre>".print_r($i, true)."</pre>";
            $dir = ((integer)floor(($i['id'])/10000));
            $file = C_PATH.'/members/uploads/messages/'.$dir.'/'.$i['id'];

            //delete_message($i['id']);

            if (file_exists($file)){
                unlink($file);
                if (count(scandir(C_PATH.'/members/uploads/messages/'.$dir)) == 2){
                    unlink(C_PATH.'/members/uploads/messages/'.$dir);
                }
            }
            @db_query("DELETE FROM ".C_MYSQL_MESSAGES." WHERE id = ".$i['id']);
        }
    }
}

# Az üzenetedet elolvasták és a kacsintások eltávolítása 1 héttel utána...

$before = date("Y-m-d H:i:s", time() - 7 * 86400);
$tmp=db_query("SELECT id FROM ".C_MYSQL_MESSAGES." WHERE sendtime <= '".$before."' AND (subject = '".$w[236]."' OR subject = '".$w[400]."') ORDER BY id ASC") or die(db_error());

$cnt = db_num_rows($tmp);
if($cnt)
{
    while ($i = db_fetch_array($tmp)) {
        //echo "<pre>".print_r($i, true)."</pre>";
        $dir = ((integer)floor(($i['id'])/10000));
        $file = C_PATH.'/members/uploads/messages/'.$dir.'/'.$i['id'];

        //delete_message($i['id']);

        if (file_exists($file)){
            unlink($file);
            if (count(scandir(C_PATH.'/members/uploads/messages/'.$dir)) == 2){
                unlink(C_PATH.'/members/uploads/messages/'.$dir);
            }
        }
        @db_query("DELETE FROM ".C_MYSQL_MESSAGES." WHERE id = ".$i['id']);
    }
}

# Remove old searches without viewname
$before = date("Y-m-d H:i:s", time() - 1 * 86400);
@db_query("DELETE FROM ".C_MYSQL_SEARCH." WHERE cdate <= '".$before."' AND search_name = ''");

?>