<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.8.0                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               forum/thread.php                 #
# File purpose            Thread main page                 #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once '../include/config.inc.php';
include_once '../include/options.inc.php';
include_once '../include/security.inc.php';
include_once '../include/functions.inc.php';
include_once '../templates/'.C_TEMP.'/config.php';
include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';

include_once C_PATH.'/sheader.php';
include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles_show.php';
include_once C_PATH.'/sfooter.php';

?>