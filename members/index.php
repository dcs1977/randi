<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               members/index.php                #
# File purpose            Members area                     #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once '../include/config.inc.php';
include_once C_PATH.'/include/options.inc.php';
include_once C_PATH.'/include/security.inc.php';
include_once C_PATH.'/include/functions.inc.php';
//include_once C_PATH.'/classes/cachequery.class.php';
include_once C_PATH.'/templates/'.C_TEMP.'/config.php';
security(TRUE, $w[152]);

#############################################################
# Secure Zone                                               #
#############################################################
if (!isset($a)) $a = '';

if($a == 'cok') {
	$tmp = db_query("SELECT t1.*, t2.*, t3.* FROM ".C_MYSQL_MEMBERS_MAIN." as t1,".C_MYSQL_MEMBERS_EXT." as t2,".C_MYSQL_MEMBERS_SEC." as t3 WHERE t1.id='".get_info('m')."' AND t1.id = t2.id AND t1.id = t3.id");
	while ($i = db_fetch_array($tmp))
	{
		$arr1=array($i['fname'],$i['lname'],$i['birthday'],$i['purposes'],$i['orientation'],$i['bodytype'],$i['country'],$i['url'],$i['icq'],$i['aim'],$i['phone'],$i['state'],$i['zip'],$i['city'],$i['address'],$i['marstat'],$i['child'],$i['height'],$i['weight'],$i['hcolor'],$i['ecolor'],$i['etnicity'],$i['religion'],$i['smoke'],$i['drink'],$i['education'],$i['languages'],$i['mylang'],$i['job'],$i['hobby'],$i['interests']);
		$arr2=array($i['descr'],$i['sgender'],$i['scountry'],$i['smarstat'],$i['schild'],$i['shcolor'],$i['secolor'],$i['ssmoke'],$i['sdrink'],$i['seducation'],$i['setnicity'],$i['sreligion'],$i['files'],$i['audio'],$i['hdyfu']);
		$notices=CheckNotices($arr1,$arr2);
	}
	if(C_NOTICES_TYPES != 0) printm($w[258].$w[630],2);
	else  printm($w[258],2);

}

if ($a == "v"){
    define('PROMPT_MESSAGES', '1');
}

if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

} else {
    if(isset($small_page)) include_once C_PATH.'/sheader.php';
    else  include_once C_PATH.'/header.php';
}

switch($a)
{
	######################
	# View messages per user - id=get_info('m')
	######################
	case "v":

                $id = get_info('m');
                if (!isset($from) || !is_numeric($from)) $from = 0;
                
                $b_tmp = "SELECT myid, user FROM ".C_MYSQL_BANLIST." WHERE myid = '".$id."' OR user = '".$id."'";
                $b_query = db_query($b_tmp);
                $banned = array();
                while($b = db_fetch_assoc($b_query)){
                    if ((integer)$b['myid'] == (integer)$id){
                       $banned[] = (integer)$b['user'];
                    } else {
                        $banned[] = (integer)$b['myid'];
                    }
                }
                
                $cikl = $from;
                $tmp = "SELECT DISTINCT m.fromid, MAX(m.id) as max_id FROM ".C_MYSQL_MESSAGES." m JOIN ".C_MYSQL_MEMBERS_MAIN." p ON m.fromid = p.id AND p.status > 5 WHERE m.toid='".$id."' AND m.deleted <> '1' AND m.deleted <> '3' GROUP BY m.fromid ORDER BY max_id DESC";
                $tquery = db_query($tmp);
                $fromid = array();
                while ($i = db_fetch_assoc($tquery)){
                    if (!in_array($i['fromid'], $banned)){
                        $fromid[] = $i['max_id'];
                    }
                }
                $count = count($fromid);
                rsort($fromid);
                $fromid = array_slice($fromid, $from, C_MPAGES);
                if (count($fromid)){
                    $search = 'm.id IN ('.implode(', ', $fromid).')';
                } else {
                    $search = '1 = 2';
                }
                
                //Admin üzenetek
                
                $tmp3 = "SELECT m.id, m.fromid, m.subject,".CorrectDate("sendtime").", m.readed, m.deleted, 7 AS status FROM ".C_MYSQL_MESSAGES." m WHERE m.fromid = 0 AND m.toid='".$id."' AND m.deleted <> '1' AND m.deleted <> '3' ORDER BY m.id DESC";
                $tquery3 = db_query($tmp3);
                $count += db_num_rows($tquery3);
                
                $tmp2 = "SELECT m.id, m.fromid, m.subject,".CorrectDate("sendtime").", m.readed, m.deleted, p.status FROM ".C_MYSQL_MESSAGES." m JOIN ".C_MYSQL_MEMBERS_MAIN." p ON m.fromid = p.id AND p.status > 5 WHERE m.toid='".$id."' AND m.deleted <> '1' AND m.deleted <> '3' AND ".$search." ORDER BY m.id DESC";
                $tquery2 = db_query($tmp2);
                
                if ($count == "0") printm($w[225], 2);
                
                /////////// Define variables for template
                define('TOTAL_MESSAGES', $w[876]);
                define('TOTAL_MESSAGES_VALUE', $count);
                define('NUM', $w[227]);
                define('FROM', $w[228]);
                define('SUBJECT', $w[168]);
                define('DATE', $w[229]);
                define('DEL', $w[230]);

                $vmreaded = array();
                $vmcolors = array();
                $vmnums = array();
                $vmfromids = array();
                $vmnames = array();
                $vmids = array();
                $vmsubjects = array();
                $vmsendtimes = array();
                $color = '';
                
                $j = array();
                $fromid = array();
                $key = 0;
                
                while ($i = db_fetch_array($tquery2)){
                    if (!isset($fromid[$i['fromid']])){
                        $fromid[$i['fromid']] = $key;
                        $j[$key] = $i;
                        $key++;
                    } else {
                        if ($i['readed']){
                            $k = $fromid[$i['fromid']];
                            $j[$k]['readed'] = $i['readed'];
                        }
                    }
                }
                
                while ($i = db_fetch_array($tquery3))
                {
                    $cikl++;
                    $color = ($color == COLOR4) ? COLOR3 :
                        COLOR4;
                    $vmcolors[] = $color;
                    $vmnums[] = $cikl;
                    $vmfromids[] = $i['fromid'];
                    $vmids[] = $i['id'];
                    $vmsubjects[] = ($i['subject'] == '') ? '>>>' : $i['subject'];
                    $vmsendtimes[] = mysql2data($i['sendtime'], 0, 1);
                    $name = "";
                    if (!C_NO_NAME)
                    {
                        $tmp2 = db_query("SELECT * FROM ".C_MYSQL_MEMBERS_MAIN." WHERE id='".$i['fromid']."' AND status > '5'");
                        while ($x = db_fetch_array($tmp2))
                        {
                            $name = username($x['id'], $x['username'], $x['fname'], $x['lname']);
                        }
                    }
                    $vmnames[] = $name;
                    $vmreaded[] = ($i['readed'] == '0') ? '' :
                        $w[231];
                }
                
                foreach ($j as $i){
                    $cikl++;
                    $color = ($color == COLOR4) ? COLOR3 :
                    COLOR4;
                    $vmcolors[] = $color;
                    $vmnums[] = $cikl;
                    $vmfromids[] = $i['fromid'];
                    $vmids[] = $i['id'];
                    $vmsubjects[] = ($i['subject'] == '') ? '>>>' : $i['subject'];
                    $vmsendtimes[] = mysql2data($i['sendtime'], 0, 1);
                    $name = "";
                    if (!C_NO_NAME)
                    {
                            $tmp2 = db_query("SELECT * FROM ".C_MYSQL_MEMBERS_MAIN." WHERE id='".$i['fromid']."' AND status > '5'");
                            while ($j = db_fetch_array($tmp2))
                            {
                                    $name = username($j['id'], $j['username'], $j['fname'], $j['lname']);
                            }
                    }
                    $vmnames[] = $name;
                    $vmreaded[] = ($i['readed'] == '0') ? '' :
                    $w[231];
                }
                
                $param = "l=".$l."&a=v";
                define('VMPAGES', pages($from, C_MPAGES, $count, $param, '5'));
                define('DELETE_SELECTED_MESSAGES', $w[232]);
                define('ADMINISTRATOR', $w[519]);
                $p = 0;

                include_once C_PATH.'/templates/'.C_TEMP.'/members_view_messages.php';

		break;
	######################
	# Sended messages per user - id=get_info('m')
	######################
	case "sm":
            
            $id = get_info('m');
            if (!isset($from) || !is_numeric($from)) $from = 0;
            
            $b_tmp = "SELECT myid, user FROM ".C_MYSQL_BANLIST." WHERE myid = '".$id."' OR user = '".$id."'";
            $b_query = db_query($b_tmp);
            $banned = array();
            while($b = db_fetch_assoc($b_query)){
                if ((integer)$b['myid'] == (integer)$id){
                   $banned[] = (integer)$b['user'];
                } else {
                    $banned[] = (integer)$b['myid'];
                }
            }
            
            $cikl = $from;
            $tmp = "SELECT DISTINCT m.toid, MAX(m.id) as max_id FROM ".C_MYSQL_MESSAGES." m JOIN ".C_MYSQL_MEMBERS_MAIN." p ON m.toid = p.id AND p.status > 5 WHERE m.fromid='".$id."' AND m.deleted <> '2' AND m.deleted <> '3' GROUP BY m.toid ORDER BY max_id DESC";
            $tquery = db_query($tmp);
            $toid = array();
            while ($i = db_fetch_assoc($tquery)){
                if (!in_array($i['toid'], $banned)){
                    $toid[] = $i['max_id'];
                }
            }
            $count = count($toid);
            rsort($toid);
            $toid = array_slice($toid, $from, C_MPAGES);
            if (count($toid)){
                $search = 'm.id IN ('.implode(', ', $toid).')';
            } else {
                $search = '1 = 2';
            }
            
            $tmp2 = "SELECT m.id, m.toid, m.subject,".CorrectDate("sendtime").", m.readed, m.deleted, p.status FROM ".C_MYSQL_MESSAGES." m JOIN ".C_MYSQL_MEMBERS_MAIN." p ON m.toid = p.id AND p.status > 5 WHERE m.fromid='".$id."' AND m.deleted <> '2' AND m.deleted <> '3' AND ".$search." ORDER BY m.id DESC";
            $tquery2 = db_query($tmp2);

            if ($count == "0") printm($w[225], 2);

            /////////// Define variables for template
            define('TOTAL_MESSAGES', $w[877]);
            define('TOTAL_MESSAGES_VALUE', $count);
            define('NUM', $w[227]);
            define('FROM', $w[228]);
            define('SUBJECT', $w[168]);
            define('DATE', $w[229]);
            define('DEL', $w[230]);

            $vmreaded = array();
            $vmcolors = array();
            $vmnums = array();
            $vmfromids = array();
            $vmnames = array();
            $vmids = array();
            $vmsubjects = array();
            $vmsendtimes = array();
            $color = '';

            $j = array();
            $toid = array();
            $key = 0;
                
            while ($i = db_fetch_array($tquery2)){
                if (!isset($toid[$i['toid']])){
                    $toid[$i['toid']] = $key;
                    $j[$key] = $i;
                    $key++;
                } else {
                    if ($i['readed']){
                        $k = $toid[$i['toid']];
                        $j[$k]['readed'] = $i['readed'];
                    }
                }
            }
            
            foreach ($j as $i){
                    $cikl++;
                    $color = ($color == COLOR4) ? COLOR3 :
                    COLOR4;
                    $vmcolors[] = $color;
                    $vmnums[] = $cikl;
                    $vmfromids[] = $i['toid'];
                    $vmids[] = $i['id'];
                    $vmsubjects[] = ($i['subject'] == '') ? '>>>' : $i['subject'];
                    $vmsendtimes[] = mysql2data($i['sendtime'], 0, 1);
                    $name = "";
                    if (!C_NO_NAME)
                    {
                            $tmp2 = db_query("SELECT * FROM ".C_MYSQL_MEMBERS_MAIN." WHERE id='".$i['toid']."' AND status > '5'");
                            while ($j = db_fetch_array($tmp2))
                            {
                                    $name = username($j['id'], $j['username'], $j['fname'], $j['lname']);
                            }
                    }
                    $vmnames[] = $name;
                    $vmreaded[] = '';
            }
            $param = "l=".$l."&a=sm";
            define('VMPAGES', pages($from, C_MPAGES, $count, $param, '5'));
            define('DELETE_SELECTED_MESSAGES', $w[232]);
            define('ADMINISTRATOR', $w[519]);
            $p = 0;

            include_once C_PATH.'/templates/'.C_TEMP.'/members_view_messages.php';

            break;
            ######################
            # Visited profiles
            ######################
	case "vp":
		if(IsAccess(C_VISITED_PROFILES))
		{

			### Visited profiles
			$id = MyID();
			if (	$id != 0)
			{
				$total = db_fetch_array(db_query("SELECT count(*) as total FROM ".C_MYSQL_PROFILEVIEW." as t1, ".C_MYSQL_MEMBERS_MAIN." as t2 WHERE visid = '".$id."' AND t1.myid = t2.id AND t2.status > '5' AND t2.hide = '0'"));
				$count = $total['total'];
				define('USERNAME', $w[118]);
				define('FROM', $w[59]);
				define('AGE', $w[120]);
				define('DATE', $w[229]);

				if ($count == '0') define('NOTHING_SEEN', template($w[511],array(C_WHOSEENTIME)));
				else
				{
					if (!isset($step) || !is_numeric($step)) $step = C_WHO_SEEN_STEP;
					if (!isset($from) || !is_numeric($from)) $from = 0;
					$result = db_query("SELECT t1.*,".CorrectDate("visdate").",t2.id,t2.username,t2.fname,t2.lname,t2.birthday,t2.gender,t2.country FROM ".C_MYSQL_PROFILEVIEW." as t1, ".C_MYSQL_MEMBERS_MAIN." as t2 WHERE visid = '".$id."' AND t1.myid = t2.id AND t1.visdate > DATE_SUB(NOW(), INTERVAL ".C_WHOSEENTIME." DAY) AND t2.status > '5' AND t2.hide = '0' order by t1.visdate DESC limit ".$from.",".$step) or die(db_error());
					$thiscnt = db_num_rows($result);
					if ($thiscnt == '0') define('NOTHING_SEEN', template($w[511],array(C_WHOSEENTIME)));
					else
					{
						$color='';
						$visid = array();
						$visdate = array();
						$names = array();
						$ages = array();
						$genders = array();
						$colors=array();
						while ($i = db_fetch_array($result))
						{
							$color = ($color == COLOR4) ? COLOR3 : COLOR4;
							$colors[] = $color;
							$visid[] = $i['myid'];
							$visdate[] = mysql2data($i['visdate'], 0, 1);
							$names[] = username($i['id'], $i['username'], $i['fname'], $i['lname']);
							$country[] = $wcr[$i['country']];
							$ages[] = abs(mysql2data($i['birthday'], 1));
							$genders[] = $wg[$i['gender']];
						}
						$param = 'a=vp&l='.$l.'&id='.$id;
						$colspan = 4;
						define('PAGES', pages($from, $step, $count, $param, $colspan));
					}
				}
			}

			$p=0;$k=1;
			include_once C_PATH.'/templates/'.C_TEMP.'/visited_profiles.php';
		}

		break;
		######################
		# Who seen my profile
		######################
	case "ws":
		if(IsAccess(C_WHO_SEEN_OWN))
		{
			### Who seen my profile
            $sr=explode(',',C_SR_COLUMNS);
			$id = MyID();
			if (	$id != 0)
			{
				$jointable = " LEFT JOIN ".C_MYSQL_BANLIST." as t4 ON (t4.myid = '".$id."' AND t4.user = t1.visid) OR (t4.myid = t1.visid AND t4.user = '".$id."') ";
				$joincon = " AND t4.id IS NULL ";
				
				$total = db_fetch_array(db_query("SELECT count(*) as total FROM ".C_MYSQL_PROFILEVIEW." as t1 LEFT JOIN ".C_MYSQL_MEMBERS_MAIN." as t2 ON t1.visid = t2.id ".$jointable." WHERE t1.myid = '".$id."' AND t1.visdate > DATE_SUB(NOW(), INTERVAL ".C_WHOSEENTIME." DAY)".$joincon." AND t2.status > '5'"));

				$count = $total['total'];
				
				### What to show
				function ShowConst($var,$acs)
				{
					Global $sr;
					if ($sr){
                        if(IsAccess($sr[$acs])) define($var,'1');
                        else define($var,'0');
                    } else {
                        define($var,'0');
                    }
				}
				
				/*ShowConst('SHOW_FNAME',2);
				ShowConst('SHOW_LNAME',3);*/
				define('SHOW_FNAME',false);
				define('SHOW_LNAME', false);
				ShowConst('SHOW_AGE',4);
				ShowConst('SHOW_GENDER',5);
				ShowConst('SHOW_BODYTYPE',6);
				ShowConst('SHOW_ORIENTATION',7);
				ShowConst('SHOW_PURPOSE',8);
				ShowConst('SHOW_COUNTRY',9);
				ShowConst('SHOW_CITY',10);
				ShowConst('SHOW_HEIGHT',11);
				ShowConst('SHOW_WEIGHT',12);
				define('CHILD', $w[135]);
				/*ShowConst('SHOW_SEEKING_GENDER',13);*/
				define('SHOW_SEEKING_GENDER', false);
				ShowConst('SHOW_PHOTO',14);
				ShowConst('SHOW_HORO',15);
				ShowConst('SHOW_REG_DATE',16);
				/*ShowConst('SHOW_LAST_ACCESS',17);*/
				define('SHOW_LAST_ACCESS',true);
				ShowConst('SHOW_RATING',18);
				ShowConst('SHOW_USERTYPE',19);
				ShowConst('SHOW_ONLINE_STATUS',20);
				/*ShowConst('SHOW_ZIP',21);*/
				define('SHOW_ZIP', false);
				define('ONLINE_STATUS',$w[352]);
				define('ONLINE', $w[350]);
				define('OFFLINE', $w[351]);
				define('WHENSEEN', true);
				define('TOPS', false);

				if ($count == '0') define('NOTHING_SEEN', $w[429]);
				else
				{
					if (!isset($step) || !is_numeric($step)) $step = C_WHO_SEEN_STEP;
					if (!isset($from) || !is_numeric($from)) $from = 0;
					
					### What to show
					$qage = (IsAccess($sr[4])) ? ',t2.birthday' : '';
					$qgender = (IsAccess($sr[5])) ? ',t2.gender' : '';
					$qbodytype = (IsAccess($sr[6])) ? ',t2.bodytype' : '';
					$qorientation = (IsAccess($sr[7])) ? ',t2.orientation' : '';
					$qpurpose = (IsAccess($sr[8])) ? ',t2.purposes' : '';
					$qcountry = (IsAccess($sr[9])) ? ',t2.country' : '';
					$qcity = (IsAccess($sr[10])) ? ',t2.city' : '';
					$qheight = (IsAccess($sr[11])) ? ',t2.height' : '';
					$qweight = (IsAccess($sr[12])) ? ',t2.weight' : '';
					$qsgender = (IsAccess($sr[13])) ? ',t2.sgender' : '';
					$qhoro = (IsAccess($sr[15])) ? ',t2.horo' : '';
					$qregdate = (IsAccess($sr[16])) ? ',t2.regdate' : '';
					/*$qeditdate = (IsAccess($sr[17])) ? ',t2.editdate' : '';*/
					$qeditdate = ',t2.editdate';
					$qusertype = (IsAccess($sr[19])) ? ',t2.req' : '';
					$qzip = (IsAccess($sr[21])) ? ',t2.zip' : '';
					if(IsAccess($sr[18])) {
						if(C_POPULARITY == 2) {
							$qrate =',ROUND(hits/hits_count,'.C_RATE_DIGITS.') as hits';
						} else {
							$qrate = ',ROUND(hits/(TO_DAYS(Now()) - TO_DAYS(IF(regdate > \''.C_LAST_CLEAR_DATE.'\', regdate, \''.C_LAST_CLEAR_DATE.'\')) + 1),'.C_RATE_DIGITS.') as hits';
						}
					}
					else $qrate='';
					$qchild = ',t3.child';
					
					$result = db_query("SELECT t2.id,t2.username,t2.fname,t2.lname,t2.files".$qage.$qgender.$qbodytype.$qorientation.$qpurpose.$qcountry.$qcity.$qheight.$qweight.$qsgender.$qhoro.$qregdate.$qeditdate.$qusertype.$qzip.$qrate.$qchild.",".CorrectDate("visdate")." FROM ".C_MYSQL_PROFILEVIEW." as t1 LEFT JOIN ".C_MYSQL_MEMBERS_MAIN." as t2 ON t1.visid = t2.id LEFT JOIN ".C_MYSQL_MEMBERS_EXT." as t3 ON t2.id = t3.id ".$jointable." WHERE  t1.myid = '".$id."'  AND t1.visdate > DATE_SUB(NOW(), INTERVAL ".C_WHOSEENTIME." DAY) ".$joincon." AND t2.status > '5' order by t1.visdate DESC limit ".$from.",".$step) or die(db_error());
					
					$thiscnt = db_num_rows($result);
					if ($thiscnt == '0') define('NOTHING_SEEN', $w[429]);
					else
					{
						$color='';
						$visid = array();
						$visdate = array();
						$country = array();
						$names = array();
						$ages = array();
						$genders = array();
						$childs = array();
						$colors=array();
						while ($i = db_fetch_array($result))
						{
							$pictures[] = SearchPhotoResult(IsAccess($sr[14]),$i['files'],$i['id'],$i['gender']);
							$color = ($color == COLOR4) ? COLOR3 : COLOR4;
							$colors[] = $color;
							$ids[]=$i['id'];
							$photoinfo[]=($i['files'] == '') ? $w[532] : template($w[533],array(PhotosNum($i['files'])));
							$names[] = username($i['id'],$i['username'],$i['fname'],$i['lname']);
							$fnames[] = (IsAccess($sr[2])) ? $i['fname'] : '';
							$lnames[] = (IsAccess($sr[3])) ? $i['lname'] : '';
							$genders[]=(IsAccess($sr[5])) ? $wg[$i['gender']] : '';
							$sgenders[]=(IsAccess($sr[13])) ? arr2val($wg, $i['sgender']) : '';
							$purposes[]=(IsAccess($sr[8])) ? arr2val($wp, $i['purposes']) : '';
							$orientations[]=(IsAccess($sr[7])) ? $wor[$i['orientation']] : '';
							$heights[]=(IsAccess($sr[11])) ? $wh[$i['height']] : '';
							$weights[]=(IsAccess($sr[12])) ? $ww[$i['weight']] : '';
							$childs[]= $wc[$i['child']];
							$horoscopes[]=(IsAccess($sr[15])) ? $whr[$i['horo']] : '';
							$bodytypes[]=(IsAccess($sr[6])) ? $wbt[$i['bodytype']] : '';
							$ages[]=(IsAccess($sr[4])) ? abs(mysql2data($i['birthday'],1)) : '';
							$countries[]=(IsAccess($sr[9])) ? $wcr[$i['country']] : '';
							$cities[]=(IsAccess($sr[10])) ? $i['city'] : '';
							$usertypes[]=(IsAccess($sr[19])) ? $wpu[$i['req']] : '';
							/*$editdates[]=(IsAccess($sr[17])) ? mysql2data($i['editdate']) : '';*/
							$editdates[]= mysql2data($i['editdate']);
							$regdates[]=(IsAccess($sr[16])) ? mysql2data($i['regdate']) : '';
							$rates[]=(IsAccess($sr[18])) ? $i['hits'] : '';
							$zips[]=(IsAccess($sr[21])) ? $i['zip'] : '';
							$visdate[] = mysql2data($i['visdate'], 0, 1);
						}
						$param = 'a=ws&l='.$l;
						$colspan = 4;
						define('VISITORS',$w[428]);
						define('PAGES', pages($from, $step, $count, $param, $colspan));
						define('SEARCH_RESULTS',search_results($from,$step,$count,IsAccess(C_SHOW_SEARCH_COUNT), VISITORS));
						define('USERNAME',$w[118]);
						define('PURPOSES',$w[119]);
						define('AGE',$w[120]);
						define('COUNTRY',$w[121]);
						define('CITY',$w[122]);
						define('LAST_ACCESS',$w[123]);
						define('REGISTER_DATE',$w[124]);
						define('PHOTO',$w[87]);
						define('RATING',$w[216]);
						define('FIRSTNAME',$w[127]);
						define('LASTNAME',$w[128]);
						define('GENDER',$w[57]);
						define('BODYTYPE',$w[525]);
						define('ORIENTATION',$w[524]);
						define('HEIGHT',$w[130]);
						define('WEIGHT',$w[131]);
						define('SEEKING_GENDER',$w[80]);
						define('HOROSCOPE',$w[129]);
						define('USERTYPE',$w[270]);
						define('ZIP',$w[285]);
						define('ADDITIONAL_INFO',$w[534]);
						define('DATE',$w[98]);
						define('WITH_SELECTED',$w[556]);
						$p=0;$k=1;
						
						include_once C_PATH.'/templates/'.C_TEMP.'/search_results.php';
					}
				}
			}
			
		}

		break;
	
	case "m":
		######################
		# View full message
		######################

		if (isset($mid) && is_numeric($mid))
		{
			$id = get_info('m');

			$tmp = db_query("SELECT id, toid, fromid, subject, ".CorrectDate('sendtime').", readed, deleted FROM ".C_MYSQL_MESSAGES." WHERE id='".$mid."' AND (toid='".$id."' OR fromid='".$id."')");
			while ($i = db_fetch_array($tmp))
			{
                if (isset($_SESSION['confirm'])){
                    define('CONFIRM', (($_SESSION['confirm'])?' checked':''));
                } else {
                    define('CONFIRM', '');
                }

				define('FROMID', $i['fromid']);
				define('TOID', $i['toid']);
				if ($id==FROMID)
				{
					define('SENDER', 'I');
					define('MYID', FROMID);
					define('HEID', TOID);
				}
				else 
				{
					define('SENDER', 'HE');
					define('MYID', TOID);
					define('HEID', FROMID);
				}

				/* Checking for ban */
				CheckForIgnor(HEID);

				/* Checking for allowing to send message to user with the same gender */
				IsAllowContact(HEID);

				$name = '';
				define('SENDTIME', $i['sendtime']);
				define('SUBJECT_VALUE', $i['subject']);
				define('MESSAGE_ID', $i['id']);
				include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';
				$message = read_message($i['id']);
				if($message) $i['message'] = $message;
				else $i['message'] = '';
				define('MESSAGE_VALUE', AzDGCode($i['message']));
				define('ADMINISTRATOR', $w[519]);
				
				$dir = ((integer)floor(($i['id'])/10000));
				$file = C_PATH.'/members/uploads/messages/'.$dir.'/'.$i['id'];
				
				include_once C_PATH.'/classes/crypt.class.php';
				$cr64 = new AzDGCrypt(C_CRYPT_KEY);
			
				if (file_exists($file) && $i['subject'] == $w[863])
					define ('ATTACHMENT', C_URL . '/pic.php?c=' . $cr64->crypt('messages/'.$dir.'/'.$i['id']).'&bigger');
				else
					define ('ATTACHMENT', '');
				
				if($i['fromid']) {
					if (!C_NO_NAME)
					{
						if (SENDER == 'HE') $tmp2 = db_query("SELECT * FROM ".C_MYSQL_MEMBERS_MAIN." WHERE id='".$i['fromid']."' AND status > '5'");
						else $tmp2 = db_query("SELECT * FROM ".C_MYSQL_MEMBERS_MAIN." WHERE id='".$i['toid']."' AND status > '5'");
						while ($j = db_fetch_array($tmp2))
						{
							$name = username($j['id'], $j['username'], $j['fname'], $j['lname']);
							define('NAME', $name);
						}
					} else define('NAME', '');
				}
				else define('NAME', '');

				define('ENTER_SUBJECT', $w[166]);
				define('ENTER_MESSAGE', $w[167]);
				define('MESSAGE_FROM', $w[233]);
				define('DATE', $w[229]);
				define('SUBJECT', $w[168]);
				define('MESSAGE', $w[169]);
				define('DELETE_MESSAGE', $w[366]);
				define('READ_NEXT_UNREAD', $w[408]);
				define('SUBJECTR', $w[168].$w[0]);
				define('MESSAGER', $w[169].$w[0]);
				
				if (SENDER == 'HE') define('REPLY', $w[234]);
				else define('REPLY', $w[358]);

				if (C_REPLYS)
				{
					define('SUBJECT_ANSWER', $i['subject']);
				}
				else define('SUBJECT_ANSWER', '');
				if (C_REPLYM)
				{
					$tm = array($i['sendtime'], tb($i['message']));
					define('MESSAGE_ANSWER', strip_tags(template($w[235], $tm)));
				}
				else define('MESSAGE_ANSWER', '');

				define('NOTIFY_ME', $w[203]);
				define('SEND_MESSAGE', $w[170]);

				BBCheck(C_BB_SM_ACL);
				
				$query = "SELECT id, toid, fromid, sendtime, subject FROM ".C_MYSQL_MESSAGES." WHERE toid='".MYID."' AND fromid='".HEID."' AND deleted <> '1' AND deleted <> '3' UNION SELECT id, toid, fromid, sendtime, subject FROM ".C_MYSQL_MESSAGES." WHERE toid='".HEID."' AND fromid='".MYID."' AND deleted <> '2' AND deleted <> '3'  ORDER BY id DESC";
				$temp = db_query($query)  or die(db_error());
				$meslog = array();
				while ($m = db_fetch_assoc($temp))
				{
					if ($m['subject'] != $w['400'] AND $m['subject'] != $w['236']){
						
						$message = read_message($m['id']);
						if($message) $m['message'] = $message;
						else $m['message'] = '';
					
						$m['message'] = AzDGCode($m['message']); 
						
						$dir = ((integer)floor(($m['id'])/10000));
						$file = C_PATH.'/members/uploads/messages/'.$dir.'/'.$m['id'];
						
						include_once C_PATH.'/classes/crypt.class.php';
						$cr64 = new AzDGCrypt(C_CRYPT_KEY);
					
						if (file_exists($file))
							$m['attachment'] = C_URL . '/pic.php?c=' . $cr64->crypt('messages/'.$dir.'/'.$m['id']).'&bigger';
						else
							$m['attachment'] = '';
						
						$meslog[] = $m; 
					}
				}

				include_once C_PATH.'/templates/'.C_TEMP.'/members_view_message.php';

				// Send confirm for message and set readed to 0 if it necessary
				if (($i['readed'] != '0')&&(SENDER == 'HE'))
				{
					// If message is not new
					if (($i['readed'] == '2'))
					{
						// Confirm needed
						$subject = $w[236];
						$date = date("d/m/Y H:i:s", time());
						$tmp2 = db_query("SELECT * FROM ".C_MYSQL_MEMBERS_MAIN." WHERE id='".$i['toid']."'");
						while ($j = db_fetch_array($tmp2))
						{
							$name2 = username($j['id'], $j['username'], $j['fname'], $j['lname']);
						}
						$message = read_message($i['id']);
						if($message) $i['message'] = $message;
						$tm = array($i['message'], $name2, $i['toid'], $date);
						$message = template($w[237], $tm);

						db_query("INSERT INTO ".C_MYSQL_MESSAGES." VALUES (Null,'".$id."','".$i['fromid']."','".$subject."',NOW(),'".ip2int(ip())."','0', '0')") or die(db_error());
						$lastid = db_insert_id();
						save_message($lastid, $message);
						AddUserToAddressBook($i['fromid'],null,1);
					}
					//db_query("UPDATE LOW_PRIORITY ".C_MYSQL_ADDRESSBOOK." SET new_mes = new_mes - 1 WHERE myid='".$i['fromid']."' AND user='".$id."' limit 1") or die(db_error());
					//db_query("UPDATE LOW_PRIORITY ".C_MYSQL_MESSAGES." SET readed='0' WHERE id='".$mid."' AND toid='".$id."'");
				}
                                
                                if (SENDER == 'HE'){
                                    db_query("UPDATE ".C_MYSQL_MESSAGES." SET readed='0' WHERE fromid='".$i['fromid']."' AND toid='".$id."' AND readed <> '0'");
                                }
				
				/* Checking for allowing to send message */
				if($i['fromid']) CheckAllowMessaging(get_info('m'),get_info('d'),True);
				
				//Delete the confirm letter
				if ($i['subject'] == $w['236']){
					$dir = ((integer)floor(($i['id'])/10000));
					$file = C_PATH.'/members/uploads/messages/'.$dir.'/'.$i['id'];
					
					delete_message($i['id']);
					
					if (file_exists($file)){
						unlink($file);
						if (count(scandir(C_PATH.'/members/uploads/messages/'.$dir)) == 2){
							unlink(C_PATH.'/members/uploads/messages/'.$dir);
						}
					}
					@db_query("DELETE FROM ".C_MYSQL_MESSAGES." WHERE id = ".$i['id']);
				}

				/////////////////////

			}
		}
		break;
	case "vmfu":
		######################
		# View full message
		######################
		if (isset($uid) && is_numeric($uid))
		{
			$id = get_info('m');
			/* Checking for ban */
			CheckForIgnor(get_info('d'));

			/* Checking for allowing to send message to user with the same gender */
			IsAllowContact(get_info('d'));
			$color='';
			define('ADMINISTRATOR', $w[519]);
			$names=$fromid=$sendtime=$subject=$message=$pictures=$ages=$countries=$cities=$onlines=$colors=array();
			$tmp = db_query("SELECT * FROM ".C_MYSQL_MEMBERS_MAIN." WHERE id='".$uid."' AND status > '5'") or die(db_error());
			while ($i = db_fetch_array($tmp))
			{
				$photo_wh=array();
				if($i['files'] == '') $pictures[] = '';
				else {
					if(C_SEARCH_PHOTO) {
						$photol=ShowPic(not_empty($i['files'],$i['id']));
						$link = (C_SEARCH_PHOTO_LINK) ? C_URL.'/view.php?l='.$l.'&id='.$i['id'] : $photol.ShowPictureBigger();
						if(C_SEARCH_PHOTO_AUTORESIZE) {
							$photo_wh=PhotoSize(C_PATH.'/members/uploads/'.not_empty($i['files'],$i['id']));
							$width = ($photo_wh[0] >= $photo_wh[1]) ? ' width="'.C_SEARCH_PHOTO_WIDTH.'"' : ' height="'.C_SEARCH_PHOTO_WIDTH.'"';
						} else $width=' width="'.C_SEARCH_PHOTO_WIDTH.'"';
						$pictures[]='<a href="'.$link.'"><img src="'.$photol.'" border="'.C_IMG_BRDR.'"'.$width.'></a>';
					} else $pictures[] = '';
				}
				$ages[]=abs(mysql2data($i['birthday'],1));
				$countries[]=$wcr[$i['country']];
				$cities[]=$i['city'];
				$fromid[] = $i['id'];
				$onlines[]=(IsOnline($i['id'])) ? $w[350] : $w[351];
				$names[] = username($i['id'], $i['username'], $i['fname'], $i['lname']);
			}

            if (isset($_SESSION['confirm'])){
                define('CONFIRM', (($_SESSION['confirm'])?' checked':''));
            } else {
                define('CONFIRM', '');
            }

			define('MESSAGER', $w[169]);
			define('ENTER_MESSAGE', $w[167]);
			define('SEND_MESSAGE', $w[170]);

			include_once C_PATH.'/templates/'.C_TEMP.'/members_view_message2.php';
			/* Checking for allowing to send message */
			if($i['fromid']) CheckAllowMessaging(get_info('m'),get_info('d'),True);

			/////////////////////

		}
		break;
	case "mnext":
		######################
		# View next unread message
		######################
		$id = get_info('m');
		$tmp = db_query("SELECT id, toid, fromid, subject, ".CorrectDate('sendtime').", readed, deleted FROM ".C_MYSQL_MESSAGES." WHERE toid='".$id."' AND readed <> '0' AND deleted <> '1' AND deleted <> '3' limit 1");
		$count = db_num_rows($tmp);
		if ($count == "0") printm($w[409], 1);

		while ($i = db_fetch_array($tmp))
		{
            if (isset($_SESSION['confirm'])){
                define('CONFIRM', (($_SESSION['confirm'])?' checked':''));
            } else {
                define('CONFIRM', '');
            }

			define('FROMID', $i['fromid']);
			$name = '';
			define('SENDTIME', $i['sendtime']);
			define('SUBJECT_VALUE', $i['subject']);
			define('MESSAGE_ID', $i['id']);
			include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';
			$message = read_message($i['id']);
			if($message) $i['message'] = $message;
			else $i['message'] = '';
			define('MESSAGE_VALUE', AzDGCode($i['message']));
			define('SENDER', 'HE');
			define('MYID', get_info('m'));
			define('HEID', FROMID);

			/* Checking for ban */
			CheckForIgnor(HEID);

			/* Checking for allowing to send message to user with the same gender */
			IsAllowContact(HEID);

			if (!C_NO_NAME)
			{
				$tmp2 = db_query("SELECT * FROM ".C_MYSQL_MEMBERS_MAIN." WHERE id='".$i['fromid']."' AND status > '5'");
				while ($j = db_fetch_array($tmp2))
				{
					$name = username($j['id'], $j['username'], $j['fname'], $j['lname']);
					define('NAME', $name);
				}
			}
			else define('NAME', '');

			define('ENTER_SUBJECT', $w[166]);
			define('ENTER_MESSAGE', $w[167]);
			define('MESSAGE_FROM', $w[233]);
			define('DATE', $w[229]);
			define('SUBJECT', $w[168]);
			define('MESSAGE', $w[169]);
			define('DELETE_MESSAGE', $w[366]);
			define('READ_NEXT_UNREAD', $w[408]);
			define('SUBJECTR', $w[168].$w[0]);
			define('MESSAGER', $w[169].$w[0]);
			define('REPLY', $w[234]);
			
			$dir = ((integer)floor(($i['id'])/10000));
			$file = C_PATH.'/members/uploads/messages/'.$dir.'/'.$i['id'];
			
			include_once C_PATH.'/classes/crypt.class.php';
			$cr64 = new AzDGCrypt(C_CRYPT_KEY);
		
			if (file_exists($file))
				define ('ATTACHMENT', C_URL . '/pic.php?c=' . $cr64->crypt('messages/'.$dir.'/'.$i['id']).'&bigger');
			else
				define ('ATTACHMENT', '');
		
			if (C_REPLYS)
			{
				define('SUBJECT_ANSWER', $w[349].$i['subject']);
			}
			else define('SUBJECT_ANSWER', '');
			if (C_REPLYM)
			{
				$tm = array($i['sendtime'], tb($i['message']));
				define('MESSAGE_ANSWER', template($w[235], $tm));
			}
			else define('MESSAGE_ANSWER', '');

			define('NOTIFY_ME', $w[203]);
			define('SEND_MESSAGE', $w[170]);
			BBCheck(C_BB_SM_ACL);
			
			$query = "SELECT id, toid, fromid, sendtime, subject FROM ".C_MYSQL_MESSAGES." WHERE toid='".MYID."' AND fromid='".HEID."' AND deleted <> '1' AND deleted <> '3' UNION SELECT id, toid, fromid, sendtime, subject FROM ".C_MYSQL_MESSAGES." WHERE toid='".HEID."' AND fromid='".MYID."' AND deleted <> '2' AND deleted <> '3' ORDER BY id DESC";
			$temp = db_query($query)  or die(db_error());
			$meslog = array();
			while ($i = db_fetch_assoc($temp))
			{
				if ($i['subject'] != $w['400'] AND $i['subject'] != $w['236']){
					$message = read_message($i['id']);
					if($message) $i['message'] = $message;
					else $i['message'] = '';
					$i['message'] = AzDGCode($i['message']);
					
					$dir = ((integer)floor(($i['id'])/10000));
					$file = C_PATH.'/members/uploads/messages/'.$dir.'/'.$i['id'];
					
					include_once C_PATH.'/classes/crypt.class.php';
					$cr64 = new AzDGCrypt(C_CRYPT_KEY);
				
					if (file_exists($file))
						$i['attachment'] = C_URL . '/pic.php?c=' . $cr64->crypt('messages/'.$dir.'/'.$i['id']).'&bigger';
					else
						$i['attachment'] = '';
						 
					$meslog[] = $i; 
				}
			}	
				
			include_once C_PATH.'/templates/'.C_TEMP.'/members_view_message.php';

			// Send confirm for message and set readed to 0 if it necessary
			if ($i['readed'] != '0')
			{
				// If message is not new
				if ($i['readed'] == '2')
				{
					// Confirm needed
					$subject = $w[236];
					$date = date("d/m/Y H:i:s", time());
					$tmp2 = db_query("SELECT * FROM ".C_MYSQL_MEMBERS_MAIN." WHERE id='".$i['toid']."'");
					while ($j = db_fetch_array($tmp2))
					{
						$name2 = username($j['id'], $j['username'], $j['fname'], $j['lname']);
					}
					$message = read_message($i['id']);
					if($message) $i['message'] = $message;
					$tm = array($i['message'], $name2, $i['toid'], $date);
					$message = template($w[237], $tm);

					db_query("INSERT INTO ".C_MYSQL_MESSAGES." VALUES (Null,'".$id."','".$i['fromid']."','".$subject."',NOW(),'".ip2int(ip())."','0', '0')") or die(db_error());
					$lastid = db_insert_id();
					save_message($lastid, $message);
					AddUserToAddressBook($i['fromid'],null,1);
				}
				
				//db_query("UPDATE LOW_PRIORITY ".C_MYSQL_ADDRESSBOOK." SET new_mes = new_mes - 1 WHERE myid='".$i['fromid']."' AND user='".$id."' limit 1") or die(db_error());
				db_query("UPDATE LOW_PRIORITY ".C_MYSQL_MESSAGES." SET readed='0' WHERE id='".$i['id']."' AND toid='".$id."'");
			}

			/* Checking for allowing to send message */
			CheckAllowMessaging(get_info('m'),get_info('d'),True);

			/////////////////////

		}
		break;
	case "s":
		######################
		# Send message
		######################
		//if (empty($subject) || empty($message)) printm($w[164]);
		if (empty($message)) send_response(500, $w[164]);

		if (!isset($id) || !is_numeric($id) || GetStatus() < 7) send_response(500, $w[1].'1');

        if (isset($_SESSION['recaptcha'])){
            $recaptcha_alert = false;
        } else {
            if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                $secret_key = RECAPTCHA_SECRET_KEY;
                if (!isset($token)){
                    send_response(500);
                } else if (empty($token)){
                    send_response(500);
                }
            } else {
                $secret_key = OLD_RECAPTCHA_SECRET_KEY;
                if (!isset($_POST['g-recaptcha-response'])){
                    printm($w[355].'<br/>Nem töltött be a recaptcha', 2);
                } else if (empty($_POST['g-recaptcha-response'])){
                    printm($w[355].'<br/>Nem töltött be a recaptcha', 2);
                }
                $token = $_POST['g-recaptcha-response'];
            }

            // call curl to POST request
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('secret' => $secret_key, 'response' => $token)));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);
            if (!empty($response)){
                $arrResponse = json_decode($response, true);
            }

            if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                if($arrResponse["success"] == '1' && $arrResponse["action"] == $action && $arrResponse["score"] >= 0.5) {
                    $recaptcha_alert = false;
                } else {
                    $recaptcha_alert = true;
                }
            } else {
                if ($arrResponse["success"]){
                    $recaptcha_alert = false;
                } else {
                    $recaptcha_alert = true;
                }
            }
        }

        if($recaptcha_alert === true){
            send_response(500, 'A recaptcha szerint SPAM küldő lehetsz.');
        } else {
            $_SESSION['recaptcha'] = true;
        }

        if(!isset($_POST['subject-honeypot'])){
            send_response(500, 'Nem támogatott böngészőt használhatsz!');
        } else if($_POST['subject-honeypot'] !== ''){
            send_response(500, 'Nem támogatott böngészőt használhatsz!');
        }

        //send_response(500, 'teszt.');

		/* Checking for ban */
		CheckForIgnor($id);

		/* Checking for allowing to send message to user with the same gender */
		IsAllowContact($id);

        if (!isset($_SESSION['confirm'])){
            $_SESSION['confirm'] = ((isset($confirm))?true:false);
        }

		$confirm = ( isset($confirm) && $confirm == "on") ? "2" : "1";
		//$subject=cb(trim($subject));$message=cb(trim($message));
		$subject=$w[862];$message=cb(trim($message));
		include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';
		$message=RemoveAzDGCode($message,C_BB_SM_ACL);
		if(trim($subject) == '' || trim($message) == '') send_response(500, $w[164]);


		// Checking for bad words
		if (C_BADWORDS_ENABLE)
		{
			if (C_BADWORDS_ERR)
			{
				if (is_badwords($subject) || is_badwords($message))
                    send_response(500, C_BADWORDS_MES);
			}
			elseif (C_BADWORDS_REP)
			{
				$subject = replace_badwords($subject);
				$message = replace_badwords($message);
			}
		}

		/* Checking for allowing to send message */
		CheckAllowMessaging(get_info('m'),get_info('d'),False);

		/* Check for spamming or flooding */
                $is_spam = CheckNotSpamMessage(get_info('m'), $id, $message);
                
                if ($is_spam === false){
                    IsNewMemberLimit(get_info('m'));
                    
                    db_query("INSERT INTO ".C_MYSQL_MESSAGES." VALUES (Null,'".get_info('m')."','".$id."','".$subject."',NOW(),'".ip2int(ip())."','".$confirm."', '0')");
                    $lastid = db_insert_id();
                    save_message($lastid, $message);
                    AddUserToAddressBook($id);

                    //Image attachment

                    include_once C_PATH.'/classes/upload.class.php';
                    include_once C_PATH.'/classes/thumbnail.class.php';

                    if ( !empty( $_FILES['image']['tmp_name'] ) ){


                            $dir = ((integer)floor(($lastid)/10000));

                            if (!file_exists(C_PATH.'/members/uploads/messages/'.$dir)) mkdir(C_PATH.'/members/uploads/messages/'.$dir,0777);


                            $upl = new Upload('image', C_MAXSZ, C_MAXWD, C_MAXHG, C_PATH.'/members/uploads/messages/'.$dir.'/'.$lastid);

                            if (!$upl->do_upload())
                            {
                                    unlink (C_PATH.'/members/uploads/messages/'.$dir.'/'.$lastid);
                                    send_response(500, $upl->getErrors());
                            }
                            else
                            {
                                    $thumb=new Thumbnail(C_PATH.'/members/uploads/messages/'.$dir.'/'.$lastid,null,True) or die('error');
                                    if(C_PICTURE_WIDTH) $thumb->Width(C_PICTURE_WIDTH);
                                    if(C_PICTURE_HEIGHT) $thumb->Height(C_PICTURE_HEIGHT);
                                    if(C_PICTURE_AUTO) $thumb->Auto(C_PICTURE_AUTO);
                                    $thumb->Quality(C_PICTURE_QUALITY);
                                    $thumb->Save(C_PATH.'/members/uploads/messages/'.$dir.'/'.$lastid,True);
                                    $subject = $w[863];
                                    db_query("UPDATE ".C_MYSQL_MESSAGES." SET subject='".$subject."' WHERE id='".$lastid."'");
                            }

                    }

                    //Push Notification
                    sendpushnotification($id, $_SESSION['username'], $subject, C_URL.'/members/index.php?l=hu&a=v');

                    // Popularity
                    if ((C_POPULARITY == '1') && (C_HITS_SEND_FROM)) pop_in(get_info('m'), C_HITS_MAIL_FROM);
                    if ((C_POPULARITY == '1') && (C_HITS_SEND_TO)) pop_in($id, C_HITS_MAIL_TO);
                    //////////////
                    alert_mes($w[165]);
                    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                        send_response(200, C_URL.'/members/index.php?l=hu&a=v');
                    }
                } else {
                    send_response(500, 'Gyakori üzenetküldés történt');
                }
        ?>
        <script language="JavaScript">
            window.location.replace('<?=C_URL."/members/index.php?l=".$l."&a=v"?>');
        </script>
        <?php
		break;
	case "p":
		######################
		# Change password - form
		######################

		define('ENTER_OLD_PASSWORD', $w[239]);
		define('ENTER_NEW_PASSWORD', $w[240]);
		define('PASSWORDS_NOT_IDENTICAL', $w[49]);
		define('OLD_PASSWORD', $w[243]);
		define('NEW_PASSWORD', $w[244]);
		define('RETYPE_NEW_PASSWORD', $w[245]);

		if (!isset($fpw)){
			$fpw = 0;
		}
		define('NOT_FORGOTTEN_PASSWORD', ($fpw == 0));

		include_once C_PATH.'/templates/'.C_TEMP.'/members_change_password.php';

		break;
	case "b":
		######################
		# My Favorites
		######################
		IsAccessToSeeThisPage();
		$sr=explode(',',C_SR_COLUMNS);

		$tmp = New CacheQuery("SELECT count(*) as total FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_BEDROOM." as t2 WHERE t2.myid='".MyID()."' and t2.user = t1.id AND t1.status > '5' AND t1.hide = '0'",0);
		$count = $tmp->data[0]['total'];
		if($count == "0") printm($w[246]);

		### What to show
		$qage = (IsAccess($sr[4])) ? ',t1.birthday' : '';
		$qgender = (IsAccess($sr[5])) ? ',t1.gender' : '';
		$qbodytype = (IsAccess($sr[6])) ? ',t1.bodytype' : '';
		$qorientation = (IsAccess($sr[7])) ? ',t1.orientation' : '';
		$qpurpose = (IsAccess($sr[8])) ? ',t1.purposes' : '';
		$qcountry = (IsAccess($sr[9])) ? ',t1.country' : '';
		$qcity = (IsAccess($sr[10])) ? ',t1.city' : '';
		$qheight = (IsAccess($sr[11])) ? ',t1.height' : '';
		$qweight = (IsAccess($sr[12])) ? ',t1.weight' : '';
		$qsgender = (IsAccess($sr[13])) ? ',t1.sgender' : '';
		$qhoro = (IsAccess($sr[15])) ? ',t1.horo' : '';
		$qregdate = (IsAccess($sr[16])) ? ',t1.regdate' : '';
		$qeditdate = (IsAccess($sr[17])) ? ',t1.editdate' : '';
		$qusertype = (IsAccess($sr[19])) ? ',t1.req' : '';
		if(IsAccess($sr[18])) {
			if(C_POPULARITY == 2) {
				$qrate =',ROUND(hits/hits_count,'.C_RATE_DIGITS.') as hits';
			} else {
				$qrate = ',ROUND(hits/(TO_DAYS(Now()) - TO_DAYS(IF(regdate > \''.C_LAST_CLEAR_DATE.'\', regdate, \''.C_LAST_CLEAR_DATE.'\')) + 1),'.C_RATE_DIGITS.') as hits';
			}
		}
		else $qrate='';


		$tmp = New CacheQuery("SELECT t1.id,t1.username,t1.fname,t1.lname,t1.files".$qage.$qgender.$qbodytype.$qorientation.$qpurpose.$qcountry.$qcity.$qheight.$qweight.$qsgender.$qhoro.$qregdate.$qeditdate.$qusertype.$qrate.",t2.reason,t2.date,t2.user FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_BEDROOM." as t2 WHERE t2.myid='".MyID()."' and t2.user = t1.id AND t1.status > '5' AND t1.hide = '0' limit ".$from.",".$step,0);
		$str='';$color='';
		$ids=$names=$photoinfo=$genders=$sgenders=$fnames=$lnames=$bodytypes=$orientations=$purposes=$ages=$countries=$cities=$heights=$weights=$horoscopes=$rates=$usertypes=$editdates=$regdates=$pictures=$colors=$fids=$freasons=array();
		### What to show
		function ShowConst($var,$acs)
		{
			Global $sr;
			if(IsAccess($sr[$acs])) define($var,'1');
			else define($var,'0');
		}

		/*ShowConst('SHOW_FNAME',2);
		ShowConst('SHOW_LNAME',3);*/
		define('SHOW_FNAME',false);
		define('SHOW_LNAME',false);
		ShowConst('SHOW_AGE',4);
		ShowConst('SHOW_GENDER',5);
		ShowConst('SHOW_BODYTYPE',6);
		ShowConst('SHOW_ORIENTATION',7);
		ShowConst('SHOW_PURPOSE',8);
		ShowConst('SHOW_COUNTRY',9);
		ShowConst('SHOW_CITY',10);
		ShowConst('SHOW_HEIGHT',11);
		ShowConst('SHOW_WEIGHT',12);
		/*ShowConst('SHOW_SEEKING_GENDER',13);*/
		define('SHOW_SEEKING_GENDER',false);
		ShowConst('SHOW_PHOTO',14);
		ShowConst('SHOW_HORO',15);
		ShowConst('SHOW_REG_DATE',16);
		ShowConst('SHOW_LAST_ACCESS',17);
		ShowConst('SHOW_RATING',18);
		ShowConst('SHOW_USERTYPE',19);
		ShowConst('SHOW_ONLINE_STATUS',20);
		define('ONLINE_STATUS',$w[352]);
		define('ONLINE', $w[350]);
		define('OFFLINE', $w[351]);
		define('DELETE_USERS', $w[248]);
		define('TOPS', false);

		while(list($k,$i)=@each($tmp->data)) {
			$pictures[] = SearchPhotoResult(IsAccess($sr[14]),$i['files'],$i['id'],$i['gender']);
			$color = ($color == COLOR4) ? COLOR3 : COLOR4;
			$colors[] = $color;
			$ids[]=$i['id'];
			$photoinfo[]=($i['files'] == '') ? $w[532] : template($w[533],array(PhotosNum($i['files'])));
			$names[] = username($i['id'],$i['username'],$i['fname'],$i['lname']);
			$fnames[] = (IsAccess($sr[2])) ? $i['fname'] : '';
			$lnames[] = (IsAccess($sr[3])) ? $i['lname'] : '';
			$genders[]=(IsAccess($sr[5])) ? $wg[$i['gender']] : '';
			$sgenders[]=(IsAccess($sr[13])) ? arr2val($wg, $i['sgender']) : '';
			$purposes[]=(IsAccess($sr[8])) ? arr2val($wp, $i['purposes']) : '';
			$orientations[]=(IsAccess($sr[7])) ? $wor[$i['orientation']] : '';
			$heights[]=(IsAccess($sr[11])) ? $wh[$i['height']] : '';
			$weights[]=(IsAccess($sr[12])) ? $ww[$i['weight']] : '';
			$horoscopes[]=(IsAccess($sr[15])) ? $whr[$i['horo']] : '';
			$bodytypes[]=(IsAccess($sr[6])) ? $wbt[$i['bodytype']] : '';
			$ages[]=(IsAccess($sr[4])) ? abs(mysql2data($i['birthday'],1)) : '';
			$countries[]=(IsAccess($sr[9])) ? $wcr[$i['country']] : '';
			$cities[]=(IsAccess($sr[10])) ? $i['city'] : '';
			$usertypes[]=(IsAccess($sr[19])) ? $wpu[$i['req']] : '';
			$editdates[]=(IsAccess($sr[17])) ? mysql2data($i['editdate']) : '';
			$regdates[]=(IsAccess($sr[16])) ? mysql2data($i['regdate']) : '';
			$rates[]=(IsAccess($sr[18])) ? $i['hits'] : '';
			$fids[] = $i['id'];
			$freasons[] = $i['reason'];
		}

		$param='l='.$l.'&a=b';
		$colspan=1;
		define('PAGES',pages($from,$step,$count,$param,$colspan));
		define('SEARCH_RESULTS',search_results($from,$step,$count,IsAccess(C_SHOW_SEARCH_COUNT),$w[97]));
		define('USERNAME',$w[118]);
		define('PURPOSES',$w[119]);
		define('AGE',$w[120]);
		define('COUNTRY',$w[121]);
		define('CITY',$w[122]);
		define('LAST_ACCESS',$w[123]);
		define('REGISTER_DATE',$w[124]);
		define('PHOTO',$w[87]);
		define('RATING',$w[216]);
		define('FIRSTNAME',$w[127]);
		define('LASTNAME',$w[128]);
		define('GENDER',$w[57]);
		define('BODYTYPE',$w[525]);
		define('ORIENTATION',$w[524]);
		define('HEIGHT',$w[130]);
		define('WEIGHT',$w[131]);
		define('SEEKING_GENDER',$w[80]);
		define('HOROSCOPE',$w[129]);
		define('USERTYPE',$w[270]);
		define('ADDITIONAL_INFO',$w[534]);
		$p=0;
		include_once C_PATH.'/templates/'.C_TEMP.'/members_my_favorites.php';

		break;
	case "adr":
		######################
		# Address book
		######################
		IsAccessToSeeThisPage();
		$sr=explode(',',C_SR_COLUMNS);
		
		$jointable = " LEFT JOIN ".C_MYSQL_BANLIST." as t4 ON (t4.myid = '".MyID()."' AND t4.user = t2.user) OR (t4.myid = t2.user AND t4.user = '".MyID()."') ";
		$joincon = " AND t4.id IS NULL ";

		$tmp = New CacheQuery("SELECT count(*) as total FROM ".C_MYSQL_MEMBERS_MAIN." as t1 LEFT JOIN ".C_MYSQL_ADDRESSBOOK." as t2 ON t2.myid = t1.id ".$jointable." WHERE t2.user='".MyID()."' and t2.lastdate != 0 and t1.status > 5 ".$joincon,0);
		$count = $tmp->data[0]['total'];
		if($count == "0") printm($w[551]);

		### What to show
		$qage = (IsAccess($sr[4])) ? ',t1.birthday' : '';
		$qgender = (IsAccess($sr[5])) ? ',t1.gender' : '';
		$qbodytype = (IsAccess($sr[6])) ? ',t1.bodytype' : '';
		$qorientation = (IsAccess($sr[7])) ? ',t1.orientation' : '';
		$qpurpose = (IsAccess($sr[8])) ? ',t1.purposes' : '';
		$qcountry = (IsAccess($sr[9])) ? ',t1.country' : '';
		$qcity = (IsAccess($sr[10])) ? ',t1.city' : '';
		$qheight = (IsAccess($sr[11])) ? ',t1.height' : '';
		$qweight = (IsAccess($sr[12])) ? ',t1.weight' : '';
		$qsgender = (IsAccess($sr[13])) ? ',t1.sgender' : '';
		$qhoro = (IsAccess($sr[15])) ? ',t1.horo' : '';
		$qregdate = (IsAccess($sr[16])) ? ',t1.regdate' : '';
		$qeditdate = (IsAccess($sr[17])) ? ',t1.editdate' : '';
		$qusertype = (IsAccess($sr[19])) ? ',t1.req' : '';
		$qzip = (IsAccess($sr[21])) ? ',t1.zip' : '';
		if(IsAccess($sr[18])) {
			if(C_POPULARITY == 2) {
				$qrate =',ROUND(hits/hits_count,'.C_RATE_DIGITS.') as hits';
			} else {
				$qrate = ',ROUND(hits/(TO_DAYS(Now()) - TO_DAYS(IF(regdate > \''.C_LAST_CLEAR_DATE.'\', regdate, \''.C_LAST_CLEAR_DATE.'\')) + 1),'.C_RATE_DIGITS.') as hits';
			}
		}
		else $qrate='';
		$qchild = ',t3.child';
		switch(C_ADRBOOK_SORT)
		{
			case "0":default:
				$case= 'order by t2.lastdate desc';
				break;
			case "1":
				$case= 'order by t1.editdate desc';
				break;
			case "2":
				$case= 'order by t1.username asc';
				break;
		}
		$tmp = New CacheQuery("SELECT t1.id,t1.username,t1.fname,t1.lname,t1.files,t1.editdate".$qage.$qgender.$qbodytype.$qorientation.$qpurpose.$qcountry.$qcity.$qheight.$qweight.$qsgender.$qhoro.$qregdate.$qusertype.$qzip.$qrate.$qchild.",t2.lastdate,t2.user FROM ".C_MYSQL_MEMBERS_MAIN." as t1 LEFT JOIN ".C_MYSQL_ADDRESSBOOK." as t2 ON t2.myid = t1.id LEFT JOIN ".C_MYSQL_MEMBERS_EXT." as t3 ON t1.id = t3.id ".$jointable." WHERE t2.user='".MyID()."' and t2.lastdate != 0 and t1.status > 5 ".$joincon.$case." limit ".$from.",".$step,0);
		$str='';$color='';
		$ids=$names=$photoinfo=$genders=$childs=$sgenders=$fnames=$lnames=$bodytypes=$orientations=$purposes=$ages=$countries=$cities=$heights=$weights=$horoscopes=$rates=$usertypes=$editdates=$regdates=$pictures=$colors=$fids=$freasons=array();
		### What to show
		function ShowConst($var,$acs)
		{
			Global $sr;
			if(IsAccess($sr[$acs])) define($var,'1');
			else define($var,'0');
		}

		/*ShowConst('SHOW_FNAME',2);
		ShowConst('SHOW_LNAME',3);*/
		define('SHOW_FNAME',false);
		define('SHOW_LNAME', false);
		ShowConst('SHOW_AGE',4);
		ShowConst('SHOW_GENDER',5);
		define('CHILD', $w[135]);
		ShowConst('SHOW_BODYTYPE',6);
		ShowConst('SHOW_ORIENTATION',7);
		ShowConst('SHOW_PURPOSE',8);
		ShowConst('SHOW_COUNTRY',9);
		ShowConst('SHOW_CITY',10);
		ShowConst('SHOW_HEIGHT',11);
		ShowConst('SHOW_WEIGHT',12);
		/*ShowConst('SHOW_SEEKING_GENDER',13);*/
		define('SHOW_SEEKING_GENDER', false);
		ShowConst('SHOW_PHOTO',14);
		ShowConst('SHOW_HORO',15);
		ShowConst('SHOW_REG_DATE',16);
		/*ShowConst('SHOW_LAST_ACCESS',17);*/
		define('SHOW_LAST_ACCESS',true);
		ShowConst('SHOW_RATING',18);
		ShowConst('SHOW_USERTYPE',19);
		ShowConst('SHOW_ONLINE_STATUS',20);
		/*ShowConst('SHOW_ZIP',21);*/
		//define('SHOW_ZIP', false);
		define('ONLINE_STATUS',$w[352]);
		define('ONLINE', $w[350]);
		define('OFFLINE', $w[351]);
		define('TOPS', false);
		define('WHENSEEN', false);

		while(list($k,$i)=@each($tmp->data)) {
			$pictures[] = SearchPhotoResult(IsAccess($sr[14]),$i['files'],$i['id'],$i['gender']);
			$color = ($color == COLOR4) ? COLOR3 : COLOR4;
			$colors[] = $color;
			$ids[]=$i['id'];
			$photoinfo[]=($i['files'] == '') ? $w[532] : template($w[533],array(PhotosNum($i['files'])));
			$names[] = username($i['id'],$i['username'],$i['fname'],$i['lname']);
			$fnames[] = (IsAccess($sr[2])) ? $i['fname'] : '';
			$lnames[] = (IsAccess($sr[3])) ? $i['lname'] : '';
			$genders[]=(IsAccess($sr[5])) ? $wg[$i['gender']] : '';
			$sgenders[]=(IsAccess($sr[13])) ? arr2val($wg, $i['sgender']) : '';
			$purposes[]=(IsAccess($sr[8])) ? arr2val($wp, $i['purposes']) : '';
			$orientations[]=(IsAccess($sr[7])) ? $wor[$i['orientation']] : '';
			$heights[]=(IsAccess($sr[11])) ? $wh[$i['height']] : '';
			$weights[]=(IsAccess($sr[12])) ? $ww[$i['weight']] : '';
			$horoscopes[]=(IsAccess($sr[15])) ? $whr[$i['horo']] : '';
			$bodytypes[]=(IsAccess($sr[6])) ? $wbt[$i['bodytype']] : '';
			$childs[]= $wc[$i['child']];
			$ages[]=(IsAccess($sr[4])) ? abs(mysql2data($i['birthday'],1)) : '';
			$countries[]=(IsAccess($sr[9])) ? $wcr[$i['country']] : '';
			$cities[]=(IsAccess($sr[10])) ? $i['city'] : '';
			$usertypes[]=(IsAccess($sr[19])) ? $wpu[$i['req']] : '';
			/*$editdates[]=(IsAccess($sr[17])) ? mysql2data($i['editdate']) : '';*/
			$editdates[]= mysql2data($i['editdate']);
			$regdates[]=(IsAccess($sr[16])) ? mysql2data($i['regdate']) : '';
			$rates[]=(IsAccess($sr[18])) ? $i['hits'] : '';
			$zips[]=(IsAccess($sr[21])) ? $i['zip'] : '';
		}

		$param='l='.$l.'&a=adr';
		$colspan=1;
		define('PAGES',pages($from,$step,$count,$param,$colspan));
		define('SEARCH_RESULTS',search_results($from,$step,$count,IsAccess(C_SHOW_SEARCH_COUNT),$w[557]));
		define('USERNAME',$w[118]);
		define('PURPOSES',$w[119]);
		define('AGE',$w[120]);
		define('COUNTRY',$w[121]);
		define('CITY',$w[122]);
		define('LAST_ACCESS',$w[123]);
		define('REGISTER_DATE',$w[124]);
		define('PHOTO',$w[87]);
		define('RATING',$w[216]);
		define('FIRSTNAME',$w[127]);
		define('LASTNAME',$w[128]);
		define('GENDER',$w[57]);
		define('BODYTYPE',$w[525]);
		define('ORIENTATION',$w[524]);
		define('HEIGHT',$w[130]);
		define('WEIGHT',$w[131]);
		define('SEEKING_GENDER',$w[80]);
		define('HOROSCOPE',$w[129]);
		define('USERTYPE',$w[270]);
		define('ZIP',$w[285]);
		define('ADDITIONAL_INFO',$w[534]);
		//define('WITH_SELECTED',$w[556]);
		$p=0;

		$p=0;$k=1;
		include_once C_PATH.'/templates/'.C_TEMP.'/search_results.php';

		break;
	case "n":
		if (IsAccess(C_BANSH))
		{
			IsAccessToSeeThisPage();

			$sql = db_query("SELECT count(*) as total FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_BANLIST." as t2 WHERE t2.myid = '".get_info('m')."' AND user='0' and t2.user = t1.id AND t1.status > '5' AND t1.hide = '0'") or die(db_error());
			$trows = db_fetch_array($sql);
			$total = $trows['total'];
			if ($total != '0') printm($w[328], 2);

			$sr=explode(',',C_SR_COLUMNS);

			$tmp = New CacheQuery("SELECT count(*) as total FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_BANLIST." as t2 WHERE t2.myid='".MyID()."' and t2.user = t1.id AND t1.status > '5' AND t1.hide = '0'",0);
			$count = $tmp->data[0]['total'];
			if($count == "0") printm($w[318]);

			### What to show
			$qage = (IsAccess($sr[4])) ? ',t1.birthday' : '';
			$qgender = (IsAccess($sr[5])) ? ',t1.gender' : '';
			$qbodytype = (IsAccess($sr[6])) ? ',t1.bodytype' : '';
			$qorientation = (IsAccess($sr[7])) ? ',t1.orientation' : '';
			$qpurpose = (IsAccess($sr[8])) ? ',t1.purposes' : '';
			$qcountry = (IsAccess($sr[9])) ? ',t1.country' : '';
			$qcity = (IsAccess($sr[10])) ? ',t1.city' : '';
			$qheight = (IsAccess($sr[11])) ? ',t1.height' : '';
			$qweight = (IsAccess($sr[12])) ? ',t1.weight' : '';
			$qsgender = (IsAccess($sr[13])) ? ',t1.sgender' : '';
			$qhoro = (IsAccess($sr[15])) ? ',t1.horo' : '';
			$qregdate = (IsAccess($sr[16])) ? ',t1.regdate' : '';
			$qeditdate = (IsAccess($sr[17])) ? ',t1.editdate' : '';
			$qusertype = (IsAccess($sr[19])) ? ',t1.req' : '';
			if(IsAccess($sr[18])) {
				if(C_POPULARITY == 2) {
					$qrate =',ROUND(hits/hits_count,'.C_RATE_DIGITS.') as hits';
				} else {
					$qrate = ',ROUND(hits/(TO_DAYS(Now()) - TO_DAYS(IF(regdate > \''.C_LAST_CLEAR_DATE.'\', regdate, \''.C_LAST_CLEAR_DATE.'\')) + 1),'.C_RATE_DIGITS.') as hits';
				}
			}
			else $qrate='';


			$tmp = New CacheQuery("SELECT t1.id,t1.username,t1.fname,t1.lname,t1.files".$qage.$qgender.$qbodytype.$qorientation.$qpurpose.$qcountry.$qcity.$qheight.$qweight.$qsgender.$qhoro.$qregdate.$qeditdate.$qusertype.$qrate.",t2.reason,t2.date,t2.user FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_BANLIST." as t2 WHERE t2.myid='".MyID()."' and t2.user = t1.id AND t1.status > '5' limit ".$from.",".$step,0);
			$str='';$color='';
			$ids=$names=$photoinfo=$genders=$sgenders=$fnames=$lnames=$bodytypes=$orientations=$purposes=$ages=$countries=$cities=$heights=$weights=$horoscopes=$rates=$usertypes=$editdates=$regdates=$pictures=$colors=$fids=$freasons=array();
			### What to show
			function ShowConst($var,$acs)
			{
				Global $sr;
				if(IsAccess($sr[$acs])) define($var,'1');
				else define($var,'0');
			}

			ShowConst('SHOW_FNAME',2);
			ShowConst('SHOW_LNAME',3);
			ShowConst('SHOW_AGE',4);
			ShowConst('SHOW_GENDER',5);
			ShowConst('SHOW_BODYTYPE',6);
			ShowConst('SHOW_ORIENTATION',7);
			ShowConst('SHOW_PURPOSE',8);
			ShowConst('SHOW_COUNTRY',9);
			ShowConst('SHOW_CITY',10);
			ShowConst('SHOW_HEIGHT',11);
			ShowConst('SHOW_WEIGHT',12);
			ShowConst('SHOW_SEEKING_GENDER',13);
			ShowConst('SHOW_PHOTO',14);
			ShowConst('SHOW_HORO',15);
			ShowConst('SHOW_REG_DATE',16);
			ShowConst('SHOW_LAST_ACCESS',17);
			ShowConst('SHOW_RATING',18);
			ShowConst('SHOW_USERTYPE',19);
			ShowConst('SHOW_ONLINE_STATUS',20);
			define('ONLINE_STATUS',$w[352]);
			define('ONLINE', $w[350]);
			define('OFFLINE', $w[351]);
			define('DELETE_USERS', $w[248]);
			define('TOPS', false);

			while(list($k,$i)=@each($tmp->data)) {
				$pictures[] = SearchPhotoResult(IsAccess($sr[14]),$i['files'],$i['id'],$i['gender']);
				$color = ($color == COLOR4) ? COLOR3 : COLOR4;
				$colors[] = $color;
				$ids[]=$i['id'];
				$photoinfo[]=($i['files'] == '') ? $w[532] : template($w[533],array(PhotosNum($i['files'])));
				$names[] = username($i['id'],$i['username'],$i['fname'],$i['lname']);
				$fnames[] = (IsAccess($sr[2])) ? $i['fname'] : '';
				$lnames[] = (IsAccess($sr[3])) ? $i['lname'] : '';
				$genders[]=(IsAccess($sr[5])) ? $wg[$i['gender']] : '';
				$sgenders[]=(IsAccess($sr[13])) ? arr2val($wg, $i['sgender']) : '';
				$purposes[]=(IsAccess($sr[8])) ? arr2val($wp, $i['purposes']) : '';
				$orientations[]=(IsAccess($sr[7])) ? $wor[$i['orientation']] : '';
				$heights[]=(IsAccess($sr[11])) ? $wh[$i['height']] : '';
				$weights[]=(IsAccess($sr[12])) ? $ww[$i['weight']] : '';
				$horoscopes[]=(IsAccess($sr[15])) ? $whr[$i['horo']] : '';
				$bodytypes[]=(IsAccess($sr[6])) ? $wbt[$i['bodytype']] : '';
				$ages[]=(IsAccess($sr[4])) ? abs(mysql2data($i['birthday'],1)) : '';
				$countries[]=(IsAccess($sr[9])) ? $wcr[$i['country']] : '';
				$cities[]=(IsAccess($sr[10])) ? $i['city'] : '';
				$usertypes[]=(IsAccess($sr[19])) ? $wpu[$i['req']] : '';
				$editdates[]=(IsAccess($sr[17])) ? mysql2data($i['editdate']) : '';
				$regdates[]=(IsAccess($sr[16])) ? mysql2data($i['regdate']) : '';
				$rates[]=(IsAccess($sr[18])) ? $i['hits'] : '';
				$fids[] = $i['id'];
				$freasons[] = $i['reason'];
			}

			$param='l='.$l.'&a=n';
			$colspan=1;
			define('PAGES',pages($from,$step,$count,$param,$colspan));
			define('SEARCH_RESULTS',search_results($from,$step,$count,IsAccess(C_SHOW_SEARCH_COUNT),$w[317]));
			define('USERNAME',$w[118]);
			define('PURPOSES',$w[119]);
			define('AGE',$w[120]);
			define('COUNTRY',$w[121]);
			define('CITY',$w[122]);
			define('LAST_ACCESS',$w[123]);
			define('REGISTER_DATE',$w[124]);
			define('PHOTO',$w[87]);
			define('RATING',$w[216]);
			define('FIRSTNAME',$w[127]);
			define('LASTNAME',$w[128]);
			define('GENDER',$w[57]);
			define('BODYTYPE',$w[525]);
			define('ORIENTATION',$w[524]);
			define('HEIGHT',$w[130]);
			define('WEIGHT',$w[131]);
			define('SEEKING_GENDER',$w[80]);
			define('HOROSCOPE',$w[129]);
			define('USERTYPE',$w[270]);
			define('ADDITIONAL_INFO',$w[534]);
			$p=0;
			include_once C_PATH.'/templates/'.C_TEMP.'/members_banlist.php';


		}
		break;
	case "h":
		######################
		# Delete profile
		######################
		if (C_REMOVE_ALLOW != '0')
		{

			define('ARE_YOU_SURE', $w[249]);
			define('ID', '#ID (Megtalálod az adatlapod alján)');
			define('PASSWORD', $w[54]);
			include_once C_PATH.'/templates/'.C_TEMP.'/members_delete_profile.php';
		}
		break;
	case "y":
		######################
		# Delete profile - MySQL remove (only add remove status)
		######################
		
		$tmp=db_query("SELECT t1.*, t2.*, t3.password, t3.wantpriv, t3.email FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2, ".C_MYSQL_MEMBERS_SEC." as t3 WHERE t1.id='".$deleteid."' AND t1.id = ".MyID()." AND t1.id = t3.id AND t1.id = t2.id LIMIT 1");
		$count=db_num_rows($tmp);
		if($count == '0') {
			include_once C_PATH.'/header.php';
			printm($w[186]);
		}
		$ptmp = db_query("SELECT SHA2('".$deletepassword."', 256) as password");
		$pi = db_fetch_array($ptmp);
		$username = "";
		$email = "";
		
		while($i=db_fetch_array($tmp)) {
			if($pi['password'] != $i['password']){
				include_once C_PATH.'/header.php';
				printm($w[186]);
			} else {
				$username = $i['username'];
				$email = $i['email'];
			}
		}
		
		if (C_REMOVE_ALLOW != '0')
		{
			unset($m);
			$id = get_info('m');
			if (C_REMOVE_ALLOW == '3')
			{
				// Remove profile from database
				if (isset($id) && is_numeric($id))
				{
					RemoveUser($id);
				}
				$tm = array($id);
				mes(template($w[250], $tm));
				printm(GoToURL(C_URL.'/logout.php?l='.$l,5000));
			}
			else
			{
				$remstat = (C_REMOVE_ALLOW == '1') ? '8' : '3';
				
				ChangeStatus(MyID(),$remstat);
				@db_query("UPDATE ".C_MYSQL_MEMBERS_MAIN." SET username='#".$username."' WHERE id ='".MyID()."'");
				@db_query("UPDATE ".C_MYSQL_MEMBERS_SEC." SET email='#".$email."' WHERE id ='".MyID()."'");

				@session_destroy();
				unset($s);
				unset($m);
				mes($w[251]);
				printm(GoToURL(C_URL.'/logout.php?l='.$l,5000));
			}
		}
		break;
	case "f":
		######################
		# Delete profiles from bedroom
		######################
		$arr=array();
		for($p = 0; $p <= (sizeof($act)-1); $p++)
		{
			$k=explode("|",$act[$p]);
			if(is_numeric($k[0])) $arr[]=intval($k[0]);
		}
		$cnt = count($arr);
		$str=implode(",",$arr);
		db_query("DELETE FROM ".C_MYSQL_BEDROOM." WHERE myid='".MyID()."' AND user IN (".cb($str).")") or die(db_error());
		$tm = array($cnt);
		mes(template($w[252], $tm));
		break;
	case "l":
		######################
		# Delete profiles from banlist
		######################
		$arr=array();
		for($p = 0; $p <= (sizeof($act)-1); $p++)
		{
			$k=explode("|",$act[$p]);
			if(is_numeric($k[0])) $arr[]=intval($k[0]);
		}
		$cnt = count($arr);
		$str=implode(",",$arr);
		db_query("DELETE FROM ".C_MYSQL_BANLIST." WHERE myid='".MyID()."' AND user IN (".cb($str).")") or die(db_error());
		$tm = array($cnt);
		mes(template($w[323], $tm));
		break;
	case "d":
		######################
		# Delete messages
		######################
		$arr=array();
                if (isset($user) && count($user)){
                    $search = "fromid IN (".implode(', ', $user).")";
                    $tmp = db_query("SELECT id FROM ".C_MYSQL_MESSAGES." WHERE ".$search." AND toid = ". MyID());
                    while ($i = db_fetch_array($tmp))
                    {
                            $del[] = $i['id'];
                    }
                }
            
		if(isset($del)) {
			for($p = 0; $p <= (sizeof($del)-1); $p++)
			{
				$k=$del[$p];
				if(is_numeric($k)) $arr[]=intval($k);
			}
		}
		$cnt = count($arr);

		if($cnt == 0) {
            alert_mes($w[164]);
        } else {
            RemoveMessages($arr,MyID());
            $tm = array($cnt);
            alert_mes(template($w[238], $tm));
        }

		?>
		<script language="JavaScript">
			window.location.replace('<?=C_URL."/members/index.php?l=".$l."&a=v"?>');
		</script>
		<?php
		break;

	case "ds":
		######################
		# Delete sent messages
		######################
		$arr=array();
                if (isset($user) && count($user)){
                    $search = "toid IN (".implode(', ', $user).")";
                    $tmp = db_query("SELECT id FROM ".C_MYSQL_MESSAGES." WHERE ".$search." AND fromid = ". MyID());
                    while ($i = db_fetch_array($tmp))
                    {
                            $del[] = $i['id'];
                    }
                }
                
		if(isset($del)) {
			for($p = 0; $p <= (sizeof($del)-1); $p++)
			{
				$k=$del[$p];
				if(is_numeric($k)) $arr[]=intval($k);
			}
		}
		$cnt = count($arr);
		if($cnt == 0) printm($w[164]);
		
		foreach ($arr as $message) {
			//read the message
			$tmp = db_query("Select * from ".C_MYSQL_MESSAGES." where id = ".$message." ") or die(db_error());
			while($i = db_fetch_array($tmp))
			{
				switch ($i['deleted']) {
					case 0:
						db_query("UPDATE ".C_MYSQL_MESSAGES." SET deleted='2' WHERE id='".$i['id']."'");
						break;
					case 1:
						db_query("UPDATE ".C_MYSQL_MESSAGES." SET deleted='3' WHERE id='".$i['id']."'");
						break;
				}
			}
		}
		
		$tm = array($cnt);
		mes(template($w[238], $tm));
		echo "<script language=\"JavaScript\">window.location.replace('".C_URL."/members/index.php?l=".$l."&a=sm');</script>";
		break;

	case "x":
		######################
		# Change password - check and MySQL change
		######################
		if (isset($fpw) && $fpw == '1'){
			if (empty($newpass) || empty($repass)) printm($w[164], 1);
			$newpass = cb($newpass);
			if ($newpass != $repass) printm($w[253], 1);
			$tmp = db_query("SELECT password FROM ".C_MYSQL_MEMBERS_SEC." WHERE id='".get_info('m')."' AND status > '5'");
			$count = db_num_rows($tmp);
			if ($count == "0") printm($w[254], 1);
			while ($i = db_fetch_array($tmp))
			{
				db_query("UPDATE ".C_MYSQL_MEMBERS_SEC." SET password=SHA2('".$newpass."', 256) WHERE id='".get_info('m')."' AND status > '5'");
				printm($w[256], 2);
			}
		} else {
			if (empty($oldpass) || empty($newpass) || empty($repass)) printm($w[164], 1);
			$newpass = cb($newpass);
			if ($newpass != $repass) printm($w[253], 1);
			$tmp = db_query("SELECT password FROM ".C_MYSQL_MEMBERS_SEC." WHERE id='".get_info('m')."' AND status > '5'");
			$count = db_num_rows($tmp);
			if ($count == "0") printm($w[254], 1);
			$ptmp = db_query("SELECT SHA2('".$oldpass."', 256) as oldpass");
			$pi = db_fetch_array($ptmp);
			while ($i = db_fetch_array($tmp))
			{
				if ($pi['oldpass'] != $i['password']){
					printm($w[255], 2);
				} else {
					db_query("UPDATE ".C_MYSQL_MEMBERS_SEC." SET password=SHA2('".$newpass."', 256) WHERE id='".get_info('m')."' AND status > '5'");
					printm($w[256], 2);
				}
			}
		}
		
		break;
	case "u":
		######################
		# Change profile - check and MySQL change
		######################
		$errors='';
		$approval_need = false;
		$tmp = db_query("SELECT t1.*, t2.*, t3.* FROM ".C_MYSQL_MEMBERS_MAIN." as t1,".C_MYSQL_MEMBERS_EXT." as t2,".C_MYSQL_MEMBERS_SEC." as t3 WHERE t1.id='".get_info('m')."' AND t1.id = t2.id AND t1.id = t3.id");
		while ($i = db_fetch_array($tmp))
		{
			if ((!C_CHANGE_EMAIL) && (isset($email))) printm($w[1].'8', 1);
			if ((!C_CHANGE_GENDER) && (isset($gender))) printm($w[1].'8', 1);

			function ReturnRegisterValues($val,$global,$type=null,$fld_acs=null,$arr=null,$interval=null,$req=null,$err=null,$return_max=null)
			{
				Global $$val,$errors,$w;
				$bool =  $global & pow(2,$fld_acs); // $bool, True or False
				if($type == 'num')
				{
					if($arr == null)
					{
						if(isset($$val) && is_numeric($$val) && $bool)
						{
							Return $$val;
						}
						elseif($bool && $req)
						{
							printm($err);
						}
						else Return ($return_max == null) ? 0 : sizeof($arr) - 1;
					}
					else
					{
						$count = (is_array($arr)) ? sizeof($arr) - 1 : $arr;
						$int_from = ($interval == null) ? 0 : $interval[0];
						$int_to = ($interval == null) ? $count : $interval[1];
						if(isset($$val) && is_numeric($$val) && $bool && ($$val > $int_from) && ($$val <= $int_to))
						{
							Return $$val;
						}
						elseif($bool && $req)
						{
							printm($err);
						}
						else Return ($return_max == null) ? $int_from : $int_to;
					}
				}
				else if($type == 'text')// Text value
				{
					if(isset($$val) && strlen($$val) && $bool)
					{
						Return cb($$val);
					}
					elseif($bool && $req)
					{
						printm($err);
					}
					else Return '';
				}
				else if($type == 'check')// checking only
				{
					Return (isset($$val) && $bool) ? True : False;
				}
				else if($type == 'multinum')
				{
					if(isset($$val) && $bool)
					{
						$tmp = implode(',',$$val);
						if(!preg_match("/^[0-9]|,$/", $tmp)) $tmp = '';
						Return $tmp;
					}
					elseif($bool && $req)
					{
						printm($err);
					}
					else Return '';
				}
				else if($type == 'list')// Text value
				{
					if(isset(${$val}) && $bool)
					{
						$p=0;$lst='';
						while (isset(${$val}[$p]))
						{
							if((${$val}[$p] < sizeof($arr)) && (${$val}[$p] >= 0))
							{
								$lst .= ${$val}[$p].',';
							}
							$p++;
						}
						Return substr($lst, 0, -1);
					}
					elseif($bool && $req)
					{
						printm($err);
					}
					else Return '';
				}
			}

			$fname=ReturnRegisterValues('fname',C_UPDATE_FIELDS1,'text',0);
			$lname=ReturnRegisterValues('lname',C_UPDATE_FIELDS1,'text',1);
			$address=ReturnRegisterValues('address',C_UPDATE_FIELDS1,'text',14,0,0,C_ADDRESSR,$w[377]);
			$url=ReturnRegisterValues('url',C_UPDATE_FIELDS1,'text',7);
			$icq=ReturnRegisterValues('icq',C_UPDATE_FIELDS1,'num',8);
			$aim=ReturnRegisterValues('aim',C_UPDATE_FIELDS1,'text',9);
			$phone=ReturnRegisterValues('phone',C_UPDATE_FIELDS1,'text',10,0,0,C_PHONER,$w[15]);
			$city=ReturnRegisterValues('city',C_UPDATE_FIELDS1,'text',13,0,0,C_CITYR,$w[16]);
			$job=ReturnRegisterValues('job',C_UPDATE_FIELDS1,'text',28,1,0,C_JOBR,$w[32]);
			$hobby=ReturnRegisterValues('hobby',C_UPDATE_FIELDS1,'text',29);
			$descr=ReturnRegisterValues('descr',C_UPDATE_FIELDS2,'text',0);
			$zip=ReturnRegisterValues('zip',C_UPDATE_FIELDS1,'text',12,0,0,C_ZIPR,$w[288]);
			$state=ReturnRegisterValues('state',C_UPDATE_FIELDS1,'text',11,0,0,C_STATER,$w[531].$w[527]);
			$langlist=ReturnRegisterValues('lang',C_UPDATE_FIELDS1,'list',26,$wlng,0,C_LANGR,$w[383]);
			$intlist=ReturnRegisterValues('interests',C_UPDATE_FIELDS1,'list',30,$win,0,C_INTERESTSR,$w[531].$w[526]);

			$day=ReturnRegisterValues('day',C_UPDATE_FIELDS1,'num',2,1,array(0,31),C_BIRTHR,$w[5]);
			$month=ReturnRegisterValues('month',C_UPDATE_FIELDS1,'num',2,1,array(0,12),C_BIRTHR,$w[5]);
			$yy = date("Y", time());
			$year=ReturnRegisterValues('year',C_UPDATE_FIELDS1,'num',2,1,array($yy-(C_AGEB+1),$yy-C_AGES),C_BIRTHR,$w[5]);
			#$gender=ReturnRegisterValues('gender',1,'num',0,$wg,0,C_GENDR,$w[7]);
			$bodytype=ReturnRegisterValues('bodytype',C_UPDATE_FIELDS1,'num',5,$wbt,0,C_BODYTYPER,$w[531].$w[525]);
			$orientation=ReturnRegisterValues('orientation',C_UPDATE_FIELDS1,'num',4,$wor,0,C_ORIENTATIONR,$w[531].$w[524]);
			$purpose=ReturnRegisterValues('purposes',C_UPDATE_FIELDS1,'list',3,$wp,0,C_PURPR,$w[9]);
			$country=ReturnRegisterValues('country',C_UPDATE_FIELDS1,'num',6,$wcr,0,C_CNTRR,$w[10]);
			$marstat=ReturnRegisterValues('marstat',C_UPDATE_FIELDS1,'num',15,$wm,0,C_MARSR,$w[17]);
			$child=ReturnRegisterValues('child',C_UPDATE_FIELDS1,'num',16,$wc,0,C_CHILDR,$w[18]);
			$height=ReturnRegisterValues('height',C_UPDATE_FIELDS1,'num',17,$wh,0,C_HGHTR,$w[19]);
			$weight=ReturnRegisterValues('weight',C_UPDATE_FIELDS1,'num',18,$ww,0,C_WGHTR,$w[20]);
			$hcolor=ReturnRegisterValues('hcolor',C_UPDATE_FIELDS1,'num',19,$whc,0,C_HAIRR,$w[23]);
			$ecolor=ReturnRegisterValues('ecolor',C_UPDATE_FIELDS1,'num',20,$we,0,C_EYER,$w[24]);
			$etnicity=ReturnRegisterValues('etnicity',C_UPDATE_FIELDS1,'num',21,$wet,0,C_ETNR,$w[25]);
			$religion=ReturnRegisterValues('religion',C_UPDATE_FIELDS1,'list',22,$wr,0,C_RELR,$w[26]);
			$smoke=ReturnRegisterValues('smoke',C_UPDATE_FIELDS1,'num',23,$ws,0,C_SMOKER,$w[29]);
			$drink=ReturnRegisterValues('drink',C_UPDATE_FIELDS1,'num',24,$wd,0,C_DRINKR,$w[30]);
			$education=ReturnRegisterValues('education',C_UPDATE_FIELDS1,'num',25,$wed,0,C_EDUCR,$w[31]);

			$sgender=ReturnRegisterValues('sgender',C_UPDATE_FIELDS2,'list',1,$wg,0,C_SGENDR,$w[8]);
			$agef=ReturnRegisterValues('agef',C_UPDATE_FIELDS2,'num',12,1,array(C_AGES,C_AGEB),C_SAGER,$w[33]);
			$aget=ReturnRegisterValues('aget',C_UPDATE_FIELDS2,'num',12,1,array(C_AGES,C_AGEB),C_SAGER,$w[33],1);
			$scountry=ReturnRegisterValues('scountry',C_UPDATE_FIELDS2,'list',2,$wcr,0,C_SCNTRR,$w[452].' '.$w[444]);
			$smarstat=ReturnRegisterValues('smarstat',C_UPDATE_FIELDS2,'list',3,$wm,0,C_SMARSR,$w[452].' '.$w[445]);
			$schild=ReturnRegisterValues('schild',C_UPDATE_FIELDS2,'list',4,$wc,0,C_SCHILDR,$w[452].' '.$w[446]);
			$heightf=ReturnRegisterValues('heightf',C_UPDATE_FIELDS2,'num',13,$wh);
			$weightf=ReturnRegisterValues('weightf',C_UPDATE_FIELDS2,'num',14,$ww);
			$heightt=ReturnRegisterValues('heightt',C_UPDATE_FIELDS2,'num',13,$wh,0,0,0,1);
			$weightt=ReturnRegisterValues('weightt',C_UPDATE_FIELDS2,'num',14,$ww,0,0,0,1);
			$shcolor=ReturnRegisterValues('shcolor',C_UPDATE_FIELDS2,'list',5,$whc,0,C_SHAIRR,$w[452].' '.$w[447]);
			$secolor=ReturnRegisterValues('secolor',C_UPDATE_FIELDS2,'list',6,$we,0,C_SEYER,$w[452].' '.$w[448]);
			$setnicity=ReturnRegisterValues('setnicity',C_UPDATE_FIELDS2,'list',10,$wet,0,C_SETNR,$w[27]);
			$sreligion=ReturnRegisterValues('sreligion',C_UPDATE_FIELDS2,'list',11,$wr,0,C_SRELR,$w[28]);
			$ssmoke=ReturnRegisterValues('ssmoke',C_UPDATE_FIELDS2,'list',7,$ws,0,C_SSMOKER,$w[452].' '.$w[449]);
			$sdrink=ReturnRegisterValues('sdrink',C_UPDATE_FIELDS2,'list',8,$wd,0,C_SDRINKR,$w[452].' '.$w[450]);
			$seducation=ReturnRegisterValues('seducation',C_UPDATE_FIELDS2,'list',9,$wed,0,C_SEDUCR,$w[452].' '.$w[451]);
			$hdyfu=ReturnRegisterValues('hdyfu',C_UPDATE_FIELDS2,'num',17,$wu,0,C_HDYFUR,$w[34]);

			include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';
			$descr = RemoveAzDGCode($descr,C_BB_DESC_ACL);

			if(ReturnRegisterValues('fname',C_UPDATE_FIELDS1,'check',0))
			{
				if (((C_FIRSTNR) || (!empty($fname))) && ((strlen($fname) > C_FIRSTNB) || (strlen($fname) < C_FIRSTNS)))
				{
					$tm = array(C_FIRSTNS, C_FIRSTNB);
					printm(template($w[3], $tm));
				}
			}
			if(ReturnRegisterValues('lname',C_UPDATE_FIELDS1,'check',1))
			{
				if (((C_LASTNR) || (!empty($lname))) && ((strlen($lname) > C_LASTNB) || (strlen($lname) < C_LASTNS)))
				{
					$tm = array(C_LASTNS, C_LASTNB);
					printm(template($w[4], $tm));
				}
			}

			if (C_CHANGE_GENDER)
			{
				if ((C_GENDR) && ($gender == "0")) printm($w[7]);
				if (!is_numeric($gender) || ($gender < 0) || ($gender >= sizeof($wg))) printm($w[1].'1 || 2', 1);
			}

			if (C_CHANGE_EMAIL)
			{
				$bad_email=array();
				$email = cb($email);
				if (c_email($email) == 0) printm($w[11]);
				# Checking bad words for username and email
				$tmp = db_query("Select * from ".C_MYSQL_BLACKLIST." order by bid");
				$cnt2 = db_num_rows($tmp);
				if($cnt2 != 0)
				{
					while($j=db_fetch_array($tmp))
					{
						if($j['bid'] == 0 || $j['bid'] == 2) $bad_email[]=$j['word'];
					}
					if (is_badwords($email,implode(',',$bad_email))) printm(C_BADWORDS_MES.': '.$w[60]);
				}
				#

			}

			if (!empty($url))
			{
				$url = checkurl($url);
				if (strlen($url) < 3)
				{
					$url = "";
					printm($w[12], 1);
				}
			}
			if (C_HOBBR && ReturnRegisterValues('hobby',C_UPDATE_FIELDS1,'check',29))
			{
				if (empty($hobby) || trim($hobby) == "") printm($w[35]);
				if (strlen($hobby) > C_HOBBB)
				{
					$tm = array(C_HOBBB);
					printm(template($w[36], $tm));
				}
				$es = explode(" ", $hobby);
				for ($as = 0; $as < sizeof($es); $as++)
				{
					$os = strlen($es[$as]);
					if ($os > C_HOBBW)
					{
						$tm = array(C_HOBBW);
						printm(template($w[37], $tm));
					}
				}
			}

			if(ReturnRegisterValues('descr',C_UPDATE_FIELDS2,'check',0))
			{
				if (empty($descr) || trim($descr) == "") printm($w[38]);
				if (strlen($descr) > C_DESCB)
				{
					$tm = array(C_DESCB);
					printm(template($w[39], $tm));
				}
				$es = explode(" ", $descr);
				for ($as = 0; $as < sizeof($es); $as++)
				{
					$os = strlen($es[$as]);
					if ($os > C_DESCW)
					{
						$tm = array(C_DESCW);
						printm(template($w[40], $tm));
					}
				}
			}
			if (C_BADWORDS_ENABLE)
			{
				if (C_BADWORDS_ERR)
				{
					if (is_badwords($hobby) || is_badwords($descr) || is_badwords($fname) || is_badwords($lname) || is_badwords($state) || is_badwords($zip) || is_badwords($city) || is_badwords($address) || is_badwords($job))
					$errors .= C_BADWORDS_MES;
				}
				elseif (C_BADWORDS_REP)
				{
					$hobby = replace_badwords($hobby);
					$descr = replace_badwords($descr);
					$fname = replace_badwords($fname);
					$lname = replace_badwords($lname);
					$state = replace_badwords($state);
					$zip = replace_badwords($zip);
					$city = replace_badwords($city);
					$address = replace_badwords($address);
					$job = replace_badwords($job);
				}
			}

			if ($hobby != $i['hobby'] || $descr != $i['descr'] || $fname != $i['fname'] || $lname != $i['lname'] || $state != $i['state'] || $zip != $i['zip'] || $city != $i['city'] || $address != $i['address'] || $job != $i['job']){
				$approval_need = true;
			}

			$tmpr=db_query("SELECT * FROM ".C_MYSQL_MEMBERS_RESTRICTS." WHERE id='".get_info('m')."'");
			$countr = db_num_rows($tmpr);
			if($countr) {
				while ($i2 = db_fetch_array($tmpr))
				{
					define('ALLOW_PHOTO',$i2['photo']);
					define('ALLOW_AUDIO',$i2['audio']);
				}
			} else
			{
				define('ALLOW_PHOTO','1');
				define('ALLOW_AUDIO','1');
			}

			///////////
			$raudio = 0;
			$cnt = $rem_photos = '0';
			$PicUpl = False;
			$AudioUpl = False;
			$photonum=$photonum_orig='';
			$grup = array();

					////////////////////////////
					include_once C_PATH.'/classes/upload.class.php';
					include_once C_PATH.'/classes/thumbnail.class.php';

					if(ReturnRegisterValues('a',C_UPDATE_FIELDS2,'check',15))
			{
				if(ALLOW_PHOTO)
				{
					$grup=explode(",",C_PH_REQ_GEN);
					if ((C_PHOTOR && C_PH_REQ_GEN == 0) || (C_PHOTOR && C_PH_REQ_GEN != 0 && in_array ($i['gender'], $grup)))
					{
						for($k = 0; $k <= $wpn[$i['req']]; $k++)
						{
							$tmp = 'delpic'.$k;
							if (isset($$tmp) && ($$tmp == 'on')) $cnt++;
						}
						if ($cnt >= PhotosNum($i['files'])) printm($w[257]);
						$rem_photos = PhotosNum($i['files']) - $cnt;
					}

					if(C_USE_GALLERY) {
						$rslt = db_query("SELECT * FROM ".C_MYSQL_NEW_PHOTOS." WHERE uid = '".get_info('m')."' limit 1 ") or die(db_error());
						while ($j = db_fetch_array($rslt))
						{
							$photonum=$photonum_orig=$j['photonum'];
						}
					}

					$time = time();
					for($p = 0; $p < $wpn[$i['req']]; $p++)
					{
						$file = 'file'.$p;
						if (!empty($_FILES[$file]['name']))
						{
							if(C_DECREASE_IMAGES)
							{
								$upl = new Upload($file, C_MAXSZ, C_MAXWD, C_MAXHG, C_PATH.'/members/uploads/'.$i['id'].'-'.$p);
							}
							else
							{
								$upl = new Upload($file, C_MAXSZ, C_MAXWD, C_MAXHG, C_PATH.'/members/uploads/'.PicPath($p,$i['id']));
							}
							
							if (!$upl->do_upload())
							{
								if(C_DECREASE_IMAGES) unlink (C_PATH.'/members/uploads/'.$i['id'].'-'.$p);
								else
								{
									unlink (C_PATH.'/members/uploads/'.PicPath($p,$i['id']));
									unlink (C_PATH.'/members/uploads/'.TPicPath($p,$i['id']));
								}

								printm($upl->getErrors());
							}
							else
							{
								if(C_DECREASE_IMAGES)
								{
									$thumb=new Thumbnail(C_PATH.'/members/uploads/'.$i['id'].'-'.$p,True,True) or die('error');
									if(C_PICTURE_WIDTH) $thumb->Width(C_PICTURE_WIDTH);
									if(C_PICTURE_HEIGHT) $thumb->Height(C_PICTURE_HEIGHT);
									if(C_PICTURE_AUTO) $thumb->Auto(C_PICTURE_AUTO);
									$thumb->Quality(C_PICTURE_QUALITY);
									$thumb->Save(C_PATH.'/members/uploads/'.PicPath($p,$i['id']),True);
									unlink (C_PATH.'/members/uploads/'.$i['id'].'-'.$p);
								}
								unlink (C_PATH.'/members/uploads/'.TPicPath($p,$i['id']));
								if(C_USE_GALLERY && C_DONT_SHOW_CHANGED_PHOTOS)
								{
									$photonum = AddPicElement($photonum,$p);
								}
								else if(C_USE_GALLERY && !C_DONT_SHOW_CHANGED_PHOTOS)
								{
									$photonum = AddPicElement($photonum,$p);
									$i['files'] = AddPicElement($i['files'],$p);
								}
								else $i['files'] = AddPicElement($i['files'],$p);

								$PicUpl=True;
								$rem_photos++;
								$approval_need = true;
							}
						}
					}
					if(C_REQ_PHOTO_NUM && ((C_PHOTOR && C_PH_REQ_GEN == 0) || (C_PHOTOR && C_PH_REQ_GEN != 0 && in_array ($i['gender'], $grup))) && ($rem_photos < C_REQ_PHOTO_NUM)) printm($w[543].C_REQ_PHOTO_NUM);
				}
			}
			$arr=explode(',',$i['files']);
			reset($arr);rsort($arr);
			$max = (($i['files'] != "") && ($arr[0]+1) > $wpn[$i['req']]) ? ($arr[0]+1) : $wpn[$i['req']];
			for($k = 0; $k <= $max; $k++)
			{
				$tmp = 'delpic'.$k;
				if (isset($$tmp) && ($$tmp == 'on'))
				{
					$i['files'] = DelPicElement($i['files'],$k);
					if(C_USE_GALLERY && $photonum != '') $photonum = DelPicElement($photonum,$k);
					unlink (C_PATH.'/members/uploads/'.PicPath($k,$i['id']));
					unlink (C_PATH.'/members/uploads/'.TPicPath($k,$i['id']));
				}
			}

			if(ReturnRegisterValues('a',C_UPDATE_FIELDS2,'check',16))
			{
				if(ALLOW_AUDIO)
				{
					if(isset($delaudio) && $delaudio == 'on')
					{
						unlink (C_PATH.'/members/uploads/'.id2pathAudio($i['id']).'.'.$i['audio']);
						$i['audio'] = '';
					}
					if (C_AUDIO_ALLOW)
					{
						$file = 'filea';
						if (!empty($_FILES[$file]['name']))
						{
							$upl = new Upload($file, C_AUDIO_SIZE, C_MAXWD, C_MAXHG, C_PATH.'/members/uploads/'.id2pathAudio($i['id']));

							if (!$upl->do_upload(C_AUDIO_ALLOW))
							{
								unlink (C_PATH.'/members/uploads/'.id2pathAudio($i['id']).'.'.$upl->getType());
								printm($upl->getErrors());
							}
							else
							{
								$i['audio'] = $upl->getType();
								$AudioUpl = True;
								$approval_need = true;
							}
						}
					}
					else $i['audio'] = '';
				}
			}

			$cst = '7';
			if(C_UPDATE_ALLOW != '1')
			{
				if(C_UPDATE_PARAMS)
				{
					switch(C_UPDATE_PARAMS)
					{
						case '1':
							if($PicUpl)
							{
								$cst = (C_UPDATE_ALLOW == '0') ? '2' : '9';
							}
							break;
						case '2':
							if($AudioUpl)
							{
								$cst = (C_UPDATE_ALLOW == '0') ? '2' : '9';
							}
							break;
						default:
							if(($PicUpl) || ($AudioUpl))
							{
								$cst = (C_UPDATE_ALLOW == '0') ? '2' : '9';
							}
							break;
					}
				}
				else
				{
					$cst = (C_UPDATE_ALLOW == '0') ? '2' : '9';
				}
				
				if ($i['status'] == '10' && $cst == '9'){
					$cst = '10';
				} elseif ($approval_need === false){
					$cst = $i['status'];
				}
			}
			
			/////////////
			$gnd = (C_CHANGE_GENDER) ? "gender='".$gender."'," :  '';
			$eml = (C_CHANGE_EMAIL) ? "email='".$email."'," :  '';
			db_query("UPDATE ".C_MYSQL_MEMBERS_MAIN." SET fname='".$fname."',lname='".$lname."',".$gnd."bodytype='".$bodytype."',orientation='".$orientation."',purposes='".$purpose."',country='".$country."',city='".$city."',zip='".$zip."',height='".$height."',weight='".$weight."',sgender='".$sgender."',horo='".horo($month, $day)."',editdate=NOW(),status='".$cst."',files='".$i['files']."' WHERE id='".$i['id']."'") or die(db_error());

			db_query("UPDATE ".C_MYSQL_MEMBERS_EXT." SET url='".$url."',icq='".$icq."',aim='".$aim."',phone='".$phone."',state='".$state."',address='".$address."',marstat='".$marstat."',child='".$child."',hcolor='".$hcolor."',ecolor='".$ecolor."',etnicity='".$etnicity."',religion='".$religion."',smoke='".$smoke."',drink='".$drink."',education='".$education."',scountry='".$scountry."',smarstat='".$smarstat."',schild='".$schild."',shcolor='".$shcolor."',secolor='".$secolor."',ssmoke='".$ssmoke."',sdrink='".$sdrink."',seducation='".$seducation."',languages='".$langlist."',interests='".$intlist."',job='".$job."',hobby='".$hobby."',descr='".$descr."',setnicity='".$setnicity."',sreligion='".$sreligion."',agef='".$agef."',aget='".$aget."',heightf='".$heightf."',heightt='".$heightt."',weightf='".$weightf."',weightt='".$weightt."',hdyfu='".$hdyfu."',audio='".$i['audio']."',status='".$cst."' WHERE id='".$i['id']."'") or die(db_error());
			db_query("UPDATE ".C_MYSQL_MEMBERS_SEC." SET ".$eml."ip='".ip2int(ip())."',status='".$cst."' WHERE id='".$i['id']."'") or die(db_error());
			@db_query("UPDATE ".C_MYSQL_ADDRESSBOOK." SET status='".$cst."' WHERE myid='".$i['id']."'");
			if(C_UPDPROF_PER)	db_query("REPLACE INTO ".C_MYSQL_OPTIONS." SET id = '".MyID()."', upddate = NOW()");
			if(C_USE_GALLERY) { // If we`re using the Gallery for admin, we add new photos for checking
				if(($photonum !== $photonum_orig) && (trim($photonum) != '')) {
					db_query("REPLACE INTO ".C_MYSQL_NEW_PHOTOS." VALUES ('".$i['id']."', '".$photonum."')") or die(db_error());
				}
				else if(trim($photonum) === '')
				{
					db_query("DELETE FROM ".C_MYSQL_NEW_PHOTOS." WHERE uid = ".$i['id']) or die(db_error());
				}
			}
			GoToURL("?l=".$l."&a=cok");

		}
		break;
	case "c":
		######################
		# Change profile - form
		######################

		# Allow to update profile only once in N hours
		if(C_UPDPROF_PER)
		{
			$tmp = db_fetch_array(db_query("SELECT count(id) as total FROM ".C_MYSQL_OPTIONS." WHERE id='".get_info('m')."' and (NOW() > DATE_ADD(upddate, INTERVAL ".cb(C_UPDPROF_PER)." HOUR) OR upddate = '0')"));
			$count = $tmp['total'];
			if(!$count) printm(template($w[748],array(C_UPDPROF_PER)));
		}

		$tmp = db_query("SELECT t1.*, t2.*, t3.* FROM ".C_MYSQL_MEMBERS_MAIN." as t1,".C_MYSQL_MEMBERS_EXT." as t2,".C_MYSQL_MEMBERS_SEC." as t3 WHERE t1.id='".get_info('m')."' AND t1.id = t2.id AND t1.id = t3.id");
		while ($i = db_fetch_array($tmp))
		{

			if (!C_ID)
			{
				if ($i['id'] == $i['username']) define('USERNAME_ALLOW', '1');
				else define('USERNAME_ALLOW', '0');
			}
			else define('USERNAME_ALLOW', '0');
			define('EMAIL_ERROR', $w[11]);
			define('USERNAME', $w[265]);

			function GenRegConst($const,$req,$err,$global,$fld_acs)
			{
				$bool =  $global & pow(2,$fld_acs); // $bool, True or False
				if ($req && $req != 0)
				{
					define($const.'_ALLOW', '1');
					define($const.'_ERROR', $err);
				}
				else
				{
					define($const.'_ALLOW', '0');
					define($const.'_ERROR', '');
				}
				if ($bool)
				{
					define($const.'_SHOW', '1');
				}
				else
				{
					define($const.'_SHOW', '0');
				}
			}

			GenRegConst('FIRSTNAME',C_FIRSTNR,template($w[3], array(C_FIRSTNS, C_FIRSTNB)),C_UPDATE_FIELDS1,0);
			GenRegConst('LASTNAME',C_LASTNR,template($w[4], array(C_LASTNS, C_LASTNB)),C_UPDATE_FIELDS1,1);
			GenRegConst('BIRTHDAY',C_BIRTHR,$w[5],C_UPDATE_FIELDS1,2);
			GenRegConst('PURPOSE',C_PURPR,$w[9],C_UPDATE_FIELDS1,3);
			GenRegConst('ORIENTATION',C_ORIENTATIONR,$w[531].$w[524],C_UPDATE_FIELDS1,4);
			GenRegConst('BODYTYPE',C_BODYTYPER,$w[531].$w[525],C_UPDATE_FIELDS1,5);
			GenRegConst('COUNTRY',C_CNTRR,$w[10],C_UPDATE_FIELDS1,6);
			GenRegConst('URL',0,0,C_UPDATE_FIELDS1,7);
			GenRegConst('ICQ',0,0,C_UPDATE_FIELDS1,8);
			GenRegConst('AIM',0,0,C_UPDATE_FIELDS1,9);
			GenRegConst('PHONE',C_PHONER,$w[15],C_UPDATE_FIELDS1,10);
			GenRegConst('STATE',C_STATER,$w[531].$w[527],C_UPDATE_FIELDS1,11);
			GenRegConst('ZIP',C_ZIPR,$w[288],C_UPDATE_FIELDS1,12);
			GenRegConst('CITY',C_CITYR,$w[16],C_UPDATE_FIELDS1,13);
			GenRegConst('ADDRESS',C_ADDRESSR,$w[377],C_UPDATE_FIELDS1,14);
			GenRegConst('MARSTAT',C_MARSR,$w[17],C_UPDATE_FIELDS1,15);
			GenRegConst('CHILD',C_CHILDR,$w[18],C_UPDATE_FIELDS1,16);
			GenRegConst('HEIGHT',C_HGHTR,$w[19],C_UPDATE_FIELDS1,17);
			GenRegConst('WEIGHT',C_WGHTR,$w[20],C_UPDATE_FIELDS1,18);
			GenRegConst('HAIR',C_HAIRR,$w[23],C_UPDATE_FIELDS1,19);
			GenRegConst('EYE',C_EYER,$w[24],C_UPDATE_FIELDS1,20);
			GenRegConst('ETHNICITY',C_ETNR,$w[25],C_UPDATE_FIELDS1,21);
			GenRegConst('RELIGION',C_RELR,$w[26],C_UPDATE_FIELDS1,22);
			GenRegConst('SMOKE',C_SMOKER,$w[29],C_UPDATE_FIELDS1,23);
			GenRegConst('DRINK',C_DRINKR,$w[30],C_UPDATE_FIELDS1,24);
			GenRegConst('EDUCATION',C_EDUCR,$w[31],C_UPDATE_FIELDS1,25);
			GenRegConst('LANG',C_LANGR,$w[383],C_UPDATE_FIELDS1,26);
			GenRegConst('JOB',C_JOBR,$w[32],C_UPDATE_FIELDS1,28);
			GenRegConst('HOBBY',C_HOBBR,$w[35],C_UPDATE_FIELDS1,29);
			GenRegConst('INTERESTS',C_INTERESTSR,$w[531].$w[526],C_UPDATE_FIELDS1,30);

			GenRegConst('DESCRIPTION',0,$w[38],C_UPDATE_FIELDS2,0);
			GenRegConst('SGENDER',C_SGENDR,$w[8],C_UPDATE_FIELDS2,1);
			GenRegConst('SCOUNTRY',0,0,C_UPDATE_FIELDS2,2);
			GenRegConst('SMARSTAT',0,0,C_UPDATE_FIELDS2,3);
			GenRegConst('SCHILD',0,0,C_UPDATE_FIELDS2,4);
			GenRegConst('SHAIR',0,0,C_UPDATE_FIELDS2,5);
			GenRegConst('SEYE',0,0,C_UPDATE_FIELDS2,6);
			GenRegConst('SSMOKE',0,0,C_UPDATE_FIELDS2,7);
			GenRegConst('SDRINK',0,0,C_UPDATE_FIELDS2,8);
			GenRegConst('SEDUCATION',0,0,C_UPDATE_FIELDS2,9);
			GenRegConst('SETHNICITY',C_SETNR,$w[27],C_UPDATE_FIELDS2,10);
			GenRegConst('SRELIGION',C_SRELR,$w[28],C_UPDATE_FIELDS2,11);
			GenRegConst('SAGE',0,0,C_UPDATE_FIELDS2,12);
			GenRegConst('SHEIGHT',0,0,C_UPDATE_FIELDS2,13);
			GenRegConst('SWEIGHT',0,0,C_UPDATE_FIELDS2,14);
			GenRegConst('PHOTO',C_PHOTOR,$w[41],C_UPDATE_FIELDS2,15);
			GenRegConst('AUDIO',0,0,C_UPDATE_FIELDS2,16);
			GenRegConst('HDYFU',C_HDYFUR,$w[34],C_UPDATE_FIELDS2,17);


			if (isset($i['username']))
			{
				define('USERNAME_VALUE', $i['username']);
			}
			else
			{
				define('USERNAME_VALUE', '');
			}

			$Require = (C_FIRSTNR) ? $w[0] :
			'';
			$Variable = (isset($i['fname'])) ? $i['fname'] :
			'';
			define('FIRSTNAME', $w[51].$Require.'['.C_FIRSTNS.'-'.C_FIRSTNB.' '.$w[52].']');
			define('FIRSTNAME_VALUE', $Variable);

			$Require = (C_LASTNR) ? $w[0] :
			'';
			$Variable = (isset($i['lname'])) ? $i['lname'] :
			'';
			define('LASTNAME', $w[53].$Require.'['.C_LASTNS.'-'.C_LASTNB.' '.$w[52].']');
			define('LASTNAME_VALUE', $Variable);

			$Variable = '';
			$Require = (C_BIRTHR) ? $w[0] :
			'';
			define('BIRTHDAY', $w[56]);
			$sqldata = str_replace(" ", "-", $i['birthday']);
			$val = explode('-', $sqldata);
			$year = $val[0];
			$month = $val[1];
			$day = $val[2];
			$p = 1;
			while (isset($wmm[$p]))
			{
				if (isset($month) && ($month == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wmm[$p];
				else $Variable .= '<option value="'.$p.'">'.$wmm[$p];
				$p++;
			}
			define('MONTH_VALUE', $Variable);
			$Variable = '';
			for($p = 1; $p < 32; $p++)
			{
				if (isset($day) && ($day == $p)) $Variable .= '<option selected>'.$p;
				else $Variable .= '<option>'.$p;
			}
			define('DAY_VALUE', $Variable);
			$Variable = '';
			$y = date("Y", time());
			$yfrom = $y-C_AGES;
			$yto = $y-C_AGEB;
			for($p = $yto; $p <= $yfrom; $p++)
			{
				if (isset($year) && ($year == $p)) $Variable .= '<option selected>'.$p;
				else $Variable .= '<option>'.$p;
			}
			define('YEAR_VALUE', $Variable);
			
			define('BD_MONTH', $val[1]);
			define('BD_DAY', $val[2]);
			define('BD_YEAR', $val[0]);

			$Require = (C_GENDR) ? $w[0] :
			'';
			$Variable = '';
			define('GENDER', $w[57].$Require);
			$p = 1;
			while (isset($wg[$p]))
			{
				if (isset($i['gender']) && ($i['gender'] == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wg[$p];
				else $Variable .= '<option value="'.$p.'">'.$wg[$p];
				$p++;
			}
			define('GENDER_VALUE', $Variable);
			$Require = (C_BODYTYPER) ? $w[0] :'';$Variable = '';
			define('BODYTYPE', $w[525].$Require);$p = 1;
			while (isset($wbt[$p])) {
				if (isset($i['bodytype']) && ($i['bodytype'] == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wbt[$p];
				else $Variable .= '<option value="'.$p.'">'.$wbt[$p];
				$p++;}
				define('BODYTYPE_VALUE', $Variable);

				$Require = (C_ORIENTATIONR) ? $w[0] :'';$Variable = '';
				define('ORIENTATION', $w[524].$Require);$p = 1;
				while (isset($wor[$p])) {
					if (isset($i['orientation']) && ($i['orientation'] == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wor[$p];
					else $Variable .= '<option value="'.$p.'">'.$wor[$p];
					$p++;}
					define('ORIENTATION_VALUE', $Variable);

					function GenField($const,$req,$show,$var,$arr,$windex)
					{
						Global $w,$i,$$var;
						$Require = ($req) ? $w[0] : '';$Variable = '';
						define(strtoupper($const), $w[$windex].$Require);$p = 1;
						$pur= (strpos($i[$var],',')) ? explode(',',$i[$var])  : array($i[$var]);
						while (isset($arr[$p])){
							$pur2 = @array_flip($pur);
							if (isset($pur2[$p])) $Variable .= '<label for="'.$var.$p.'" class="cb"><input name="'.$var.'[]" value="'.$p.'" type="checkbox" id="'.$var.$p.'" onclick="hld(this);" checked>'.$arr[$p].'</label>';
							else $Variable .= '<label for="'.$const.$p.'" class="lb"><input name="'.$var.'[]" value="'.$p.'" type="checkbox" id="'.$var.$p.'" onclick="hld(this);">'.$arr[$p].'</label>';$p++;}
							define(strtoupper($const.'_VALUE'), $Variable);
					}

					GenField('purpose',C_PURPR,C_PURPOSESH,'purposes',$wp,'58');
					GenField('interests',C_INTERESTSR,C_INTERESTSSH,'interests',$win,'526');


					define('STATE', $w[527]);
					$Variable = (isset($i['state'])) ? $i['state'] : '';
					define('STATE_VALUE', $Variable);


					$Require = (C_CNTRR) ? $w[0] : '';
					$Variable = '';
					define('COUNTRY', $w[59].$Require);
					$p = 0;
					asort($wcr);
					reset($wcr);
					while (list ($p, $val) = each ($wcr))
					{
						if (isset($i['country']) && ($i['country'] == $p)) $Variable .= '<option value="'.$p.'" selected>'.$val;
						else $Variable .= '<option value="'.$p.'">'.$val;
					}
					define('COUNTRY_VALUE', $Variable);
					define('EMAIL', $w[60].$w[0]);
					$Variable = (isset($i['email'])) ? $i['email'] :
					'';
					define('EMAIL_VALUE', $Variable);

					define('URL', $w[61]);
					$Variable = (isset($i['url'])) ? $i['url'] :
					'';
					define('URL_VALUE', $Variable);

					define('ICQ', $w[62]);
					$Variable = (isset($i['icq'])) ? $i['icq'] :
					'';
					define('ICQ_VALUE', $Variable);

					define('AIM', $w[63]);
					$Variable = (isset($i['aim'])) ? $i['aim'] :
					'';
					define('AIM_VALUE', $Variable);

					$Require = (C_PHONER) ? $w[0] :
					'';
					define('PHONE', $w[64].$Require);
					$Variable = (isset($i['phone'])) ? $i['phone'] :
					'';
					define('PHONE_VALUE', $Variable);

					$Require = (C_ZIPR) ? $w[0] :
					'';
					define('ZIP', $w[285].$Require);
					$Variable = (isset($i['zip'])) ? $i['zip'] :
					'';
					define('ZIP_VALUE', $Variable);

					$Require = (C_CITYR) ? $w[0] :
					'';
					define('CITY', $w[65].$Require);
					$Variable = (isset($i['city'])) ? $i['city'] :
					'';
					define('CITY_VALUE', $Variable);

					$Require = (C_ADDRESSR) ? $w[0] : '';
					define('ADDRESS', $w[378].$Require);
					$Variable = (isset($i['address'])) ? $i['address'] : '';
					define('ADDRESS_VALUE', $Variable);

					$Require = (C_MARSR) ? $w[0] :
					'';
					$Variable = '';
					define('MARITAL_STATUS', $w[66].$Require);
					$p = 1;
					while (isset($wm[$p]))
					{
						if (isset($i['marstat']) && ($i['marstat'] == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wm[$p];
						else $Variable .= '<option value="'.$p.'">'.$wm[$p];
						$p++;
					}
					define('MARITAL_STATUS_VALUE', $Variable);

					$Require = (C_CHILDR) ? $w[0] :
					'';
					$Variable = '';
					define('CHILD', $w[67].$Require);
					$p = 1;
					while (isset($wc[$p]))
					{
						if (isset($i['child']) && ($i['child'] == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wc[$p];
						else $Variable .= '<option value="'.$p.'">'.$wc[$p];
						$p++;
					}
					define('CHILD_VALUE', $Variable);


					$Require = (C_HGHTR) ? $w[0] :
					'';
					$Variable = '';
					define('HEIGHT', $w[68].$Require);
					$p = 1;
					while (isset($wh[$p]))
					{
						if (isset($i['height']) && ($i['height'] == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wh[$p];
						else $Variable .= '<option value="'.$p.'">'.$wh[$p];
						$p++;
					}
					define('HEIGHT_VALUE', $Variable);

					$Require = (C_WGHTR) ? $w[0] :
					'';
					$Variable = '';
					define('WEIGHT', $w[69].$Require);
					$p = 1;
					while (isset($ww[$p]))
					{
						if (isset($i['weight']) && ($i['weight'] == $p)) $Variable .= '<option value="'.$p.'" selected>'.$ww[$p];
						else $Variable .= '<option value="'.$p.'">'.$ww[$p];
						$p++;
					}
					define('WEIGHT_VALUE', $Variable);

					$Require = (C_HAIRR) ? $w[0] :
					'';
					$Variable = '';
					define('HAIR', $w[70].$Require);
					$p = 1;
					while (isset($whc[$p]))
					{
						if (isset($i['hcolor']) && ($i['hcolor'] == $p)) $Variable .= '<option value="'.$p.'" selected>'.$whc[$p];
						else $Variable .= '<option value="'.$p.'">'.$whc[$p];
						$p++;
					}
					define('HAIR_VALUE', $Variable);

					$Require = (C_EYER) ? $w[0] :
					'';
					$Variable = '';
					define('EYE', $w[71].$Require);
					$p = 1;
					while (isset($we[$p]))
					{
						if (isset($i['ecolor']) && ($i['ecolor'] == $p)) $Variable .= '<option value="'.$p.'" selected>'.$we[$p];
						else $Variable .= '<option value="'.$p.'">'.$we[$p];
						$p++;
					}
					define('EYE_VALUE', $Variable);

					$Require = (C_ETNR) ? $w[0] :
					'';
					$Variable = '';
					define('ETHNICITY', $w[72].$Require);
					$p = 1;
					while (isset($wet[$p]))
					{
						if (isset($i['etnicity']) && ($i['etnicity'] == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wet[$p];
						else $Variable .= '<option value="'.$p.'">'.$wet[$p];
						$p++;
					}
					define('ETHNICITY_VALUE', $Variable);

                    GenField('religion',C_RELR,C_RELIGIONSH,'religion',$wr,'73');

					$Require = (C_SMOKER) ? $w[0] :
					'';
					$Variable = '';
					define('SMOKE', $w[74].$Require);
					$p = 1;
					while (isset($ws[$p]))
					{
						if (isset($i['smoke']) && ($i['smoke'] == $p)) $Variable .= '<option value="'.$p.'" selected>'.$ws[$p];
						else $Variable .= '<option value="'.$p.'">'.$ws[$p];
						$p++;
					}
					define('SMOKE_VALUE', $Variable);

					$Require = (C_DRINKR) ? $w[0] :
					'';
					$Variable = '';
					define('DRINK', $w[75].$Require);
					$p = 1;
					while (isset($wd[$p]))
					{
						if (isset($i['drink']) && ($i['drink'] == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wd[$p];
						else $Variable .= '<option value="'.$p.'">'.$wd[$p];
						$p++;
					}
					define('DRINK_VALUE', $Variable);

					$Require = (C_EDUCR) ? $w[0] :
					'';
					$Variable = '';
					define('EDUCATION', $w[76].$Require);
					$p = 1;
					while (isset($wed[$p]))
					{
						if (isset($i['education']) && ($i['education'] == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wed[$p];
						else $Variable .= '<option value="'.$p.'">'.$wed[$p];
						$p++;
					}
					define('EDUCATION_VALUE', $Variable);

					$Require = (C_LANGR) ? $w[0] : '';
					$checklang=array();
					define('LANG', $w[382].$Require);
					$p = 0;
					$lang=explode(',',$i['languages']);
					while (isset($wlng[$p]))
					{
						$lang2 = @array_flip($lang);
						if (isset($lang2[$p])) $checklang[] = '<input type="checkbox" name="lang[]" value="'.$p.'" checked>';
						else  $checklang[] = '<input type="checkbox" name="lang[]" value="'.$p.'">';
						$p++;
					}


					$Require = (C_JOBR) ? $w[0] :
					'';
					define('JOB', $w[77].$Require);
					$Variable = (isset($i['job'])) ? $i['job'] :
					'';
					define('JOB_VALUE', $Variable);

					$Require = (C_HOBBR) ? $w[0] :
					'';
					define('HOBBY', $w[78].$Require);
					$Variable = (isset($i['hobby'])) ? $i['hobby'] :
					'';
					define('HOBBY_VALUE', $Variable);

					define('DESCRIPTION', $w[79].$w[0]);
					$Variable = (isset($i['descr'])) ? tb($i['descr']) :
					'';
					define('DESCRIPTION_VALUE', $Variable);




					GenField('seeking_gender',C_SGENDR,C_SGENDERSH,'sgender',$wg,'80');
					GenField('seeking_country',C_SCNTRR,C_SCOUNTRYSH,'scountry',$wcr,'444');
					GenField('seeking_marital_status',C_SMARSR,C_SMARSTATSH,'smarstat',$wm,'445');
					GenField('seeking_child',C_SCHILDR,C_SCHILDSH,'schild',$wc,'446');
					GenField('seeking_hair',C_SHAIRR,C_SHCOLORSH,'shcolor',$whc,'447');
					GenField('seeking_eye',C_SEYER,C_SECOLORSH,'secolor',$we,'448');
					GenField('seeking_smoke',C_SSMOKER,C_SSMOKESH,'ssmoke',$ws,'449');
					GenField('seeking_drink',C_SDRINKR,C_SDRINKSH,'sdrink',$wd,'450');
					GenField('seeking_education',C_SEDUCR,C_SEDUCATIONSH,'seducation',$wed,'451');
					GenField('seeking_ethnicity',C_SETNR,C_SETHNICITYSH,'setnicity',$wet,'81');
					GenField('seeking_religion',C_SRELR,C_SRELIGIONSH,'sreligion',$wr,'82');

					$Require = (C_SAGER) ? $w[0] :
					'';
					$Variable = '';
					define('SEEKING_AGE', $w[83].$Require);
					for($p = C_AGES; $p <= C_AGEB; $p++)
					{
						if (isset($i['agef']) && ($i['agef'] == $p)) $Variable .= '<option selected>'.$p;
						else $Variable .= '<option>'.$p;
					}
					define('SEEKING_AGE_FROM_VALUE', $Variable);
					$Variable = '';
					for($p = C_AGES; $p <= C_AGEB; $p++)
					{
						if (isset($i['aget']) && ($i['aget'] == $p)) $Variable .= '<option selected>'.$p;
						else
						{
							if (($p == C_AGEB) && ($a == '')) $Variable .= '<option selected>'.$p;
							else $Variable .= '<option>'.$p;
						}
					}
					define('SEEKING_AGE_TO_VALUE', $Variable);

					$Require = (C_SHGHTR) ? $w[0] :
					'';
					$Variable = '';
					define('SEEKING_HEIGHT', $w[84].$Require);
					$p = 1;
					while (isset($wh[$p]))
					{
						if (isset($i['heightf']) && ($i['heightf'] == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wh[$p];
						else $Variable .= '<option value="'.$p.'">'.$wh[$p];
						$p++;
					}
					define('SEEKING_HEIGHT_FROM_VALUE', $Variable);
					$Variable = '';
					$p = 1;
					while (isset($wh[$p]))
					{
						if (isset($i['heightt']) && ($i['heightt'] == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wh[$p];
						else
						{
							if (($p == (sizeof($wh)-1)) && ($a == '')) $Variable .= '<option value="'.$p.'" selected>'.$wh[$p];
							else $Variable .= '<option value="'.$p.'">'.$wh[$p];
						}
						$p++;
					}
					define('SEEKING_HEIGHT_TO_VALUE', $Variable);

					$Require = (C_SWGHTR) ? $w[0] :
					'';
					$Variable = '';
					define('SEEKING_WEIGHT', $w[85].$Require);
					$p = 1;
					while (isset($ww[$p]))
					{
						if (isset($i['weightf']) && ($i['weightf'] == $p)) $Variable .= '<option value="'.$p.'" selected>'.$ww[$p];
						else $Variable .= '<option value="'.$p.'">'.$ww[$p];
						$p++;
					}
					define('SEEKING_WEIGHT_FROM_VALUE', $Variable);
					$Variable = '';
					$p = 1;
					while (isset($ww[$p]))
					{
						if (isset($i['weightt']) && ($i['weightt'] == $p)) $Variable .= '<option value="'.$p.'" selected>'.$ww[$p];
						else
						{
							if (($p == (sizeof($ww)-1)) && ($a == '')) $Variable .= '<option value="'.$p.'" selected>'.$ww[$p];
							else $Variable .= '<option value="'.$p.'">'.$ww[$p];
						}
						$p++;
					}
					define('SEEKING_WEIGHT_TO_VALUE', $Variable);

					$Require = (C_HDYFUR) ? $w[0] :	'';
					$Variable = '';
					define('HOW_DID_YOU_FIND_US', $w[86].$Require);
					$p = 1;
					while (isset($wu[$p]))
					{
						$Variable .= '<option value="'.$p.'">'.$wu[$p];
						if (isset($i['hdyfu']) && ($i['hdyfu'] == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wu[$p];
						$p++;
					}
					define('HOW_DID_YOU_FIND_US_VALUE', $Variable);

					$tmpr=db_query("SELECT * FROM ".C_MYSQL_MEMBERS_RESTRICTS." WHERE id='".get_info('m')."'");
					$countr = db_num_rows($tmpr);
					if($countr) {
						while ($i2 = db_fetch_array($tmpr))
						{
							define('ALLOW_PHOTO',$i2['photo']);
							define('ALLOW_AUDIO',$i2['audio']);
						}
					} else
					{
						define('ALLOW_PHOTO','1');
						define('ALLOW_AUDIO','1');
					}

					define('DELETE_PHOTO', $w[259]);


					$photos = '';
					if(C_USE_GALLERY)
					{
						$tmpp=db_query("SELECT photonum FROM ".C_MYSQL_NEW_PHOTOS." WHERE uid='".get_info('m')."'");
						while ($kk = db_fetch_array($tmpp))
						{
							$photos = $kk['photonum'];
						}
					}

					if ($i['files'] != "" || $photos != "" )
					{

						$arr=explode(',',AddPicElement($i['files'],$photos));
					}
					else $arr=array();
					$k=0;$tm = array(C_MAXSZ);$PHOTO_NUM=$REMOVE_PHOTO=$PHOTO_VALUE=array();
					reset($arr);rsort($arr);
					$max = (($i['files'] != "") && ($arr[0]+1) > $wpn[$i['req']]) ? ($arr[0]+1) : $wpn[$i['req']];

					$MyGender = $i['gender'];$grup=explode(",",C_PH_REQ_GEN);
					for($ii = 0; $ii < $max; $ii++)
					{
						$k = $ii+1;
						$Require = (((C_PHOTOR && C_PH_REQ_GEN == 0)  || (C_PHOTOR && C_PH_REQ_GEN != 0 && in_array ($MyGender, $grup))) && ($ii < C_REQ_PHOTO_NUM)) ? $w[0] :	'';
						$PHOTO_NUM[] = $w[87].' '.$k.' '.template($w[223], $tm).' '.$Require;
						if (in_array($ii,$arr))
						{
							$REMOVE_PHOTO[$ii]=1;
							$REMOVE_VALUE[$ii]= ShowPic(PicPath($ii,$i['id']));
						}
						elseif($ii >= $wpn[$i['req']]) $REMOVE_PHOTO[$ii]=-1;
						else $REMOVE_PHOTO[$ii]=0;
					}
					define('NOT_AVAILABLE',$w['347']);


					$tm = array(C_AUDIO_SIZE);
					switch(C_AUDIO_ALLOW)
					{
						case '1': define('AUDIO', $w[292].' '.template($w[223], $tm));break;
						case '2': define('AUDIO', $w[670].' '.template($w[223], $tm));break;
						case '3': define('AUDIO', $w[671].' '.template($w[223], $tm));break;
					}

					if (C_AUDIO_AUTOSTART) {$autostart='true';define('AUTOSTART', 'true');}
					else {$autostart='false';define('AUTOSTART', 'false');}

					define('DELETE', $w[297]);

					if (!empty($i['audio']) && (C_AUDIO_ALLOW))
					{
						define('AUDIO_EXIST', '1');
						define('AUDIO_VALUE', id2pathAudio($i['id']).'.'.$i['audio']);
					}
					else
					{
						define('AUDIO_VALUE', '');
						define('AUDIO_EXIST', '0');

					}


		}

		$p=0;
		include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';
		BBCheck(C_BB_DESC_ACL);
		include_once C_PATH.'/templates/'.C_TEMP.'/members_change_profile.php';

		break;
}
if(isset($small_page)) 	include_once C_PATH.'/sfooter.php';
else include_once C_PATH.'/footer.php';
?>
