<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.8.0                    #
# Written by              AzDG (support@azdg.com)          #
# Created 03/01/03        Last Modified 05/02/03           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               hu.php                           #
# File purpose            hungarian langaue file           #
# File created by         Éjszárny Zöldi <info@egerszegnet.hu> #
############################################################

define('C_HTML_DIR','ltr'); // HTML direction for this language
define('C_CHARSET', 'utf-8'); // HTML charset for this language

# Do not insert here ' or " char due possible error
define('C_DEFAULT_TITLE','Hotbull');
define('C_DEFAULT_KEYWORDS','bisex, biszex, csavo, erotika, gay, gaypoint, gay tarskeresõ, homoszexuális, homoszexualitás, meleg, meleg tarskeresõ, partner, randi, randizas, sex, srac, swinger, swingerklub, szerelem, szex, tarskereso, hotbull.hu, meleg randi, melegrandi, gay szex, gayszex, travi, meleg szex, melegszex');
define('C_DEFAULT_DESCRIPTION','Meleg társkereső gay férfiaknak. Ha meg akarod találni a neked megfelelõ meleg srácokat, akkor nálunk megtalálod álmaid homo szex partnerét.');

# The same variables in new format
$C_HTML_DIR = 'ltr';
$C_CHARSET = 'utf-8';

# Don`t change following value
$C_LANGUAGE = 'hu';

### Rules for translation 
### 1. Be attentive while translating - do not use ' { } characters
###    You can use their html equivalents - &#39; &#123; &#125;
### 2. Don`t translate {some_number} templates - you can only replace it - 
###    {0},{1}... - is not number - it templates
###################################

$w=array(
'<font color=red size=3>*</font>', //0
'Biztonsági hiba - #', //1
'Ez a mailcím már szerepel rendszerünkben, egy emailcímhez,csak egy regisztrációt tudunk elfogadni.', //2
'rossz név,vagy becenév. A név vagy becenév ezeket a karaktereket tartalmazhatja {0} - {1} ', //3
'Rossz név. A név ezeket a karaktereket tartalmazhatja {0} - {1} ', //4
'Hibás születési idõ', //5
'Hibás jelszó. A jelszó ezeket a karaktereket tartalmazhatja {0} - {1} ', //6
'Válaszd ki, az elkötelezetséged', //7
'Válaszd ki a keresett személy elkötelezetségét', //8
'Milyen kapcsolatot keresel', //9
'Válaszd ki a megyét', //10
'Rossz,vagy hiányzó emailcím', //11
'Helytelen weblapcím', //12
'Rossz ICQ szám', //13
'Rossz AIM', //14
'Írd be a telefonszámodat', //15
'Írd be a várost', //16
'Kapcsolati státusz', //17
'Válassz a szexuális beállítottságra vonatkozóan', //18
'Válaszd ki a magasságodat', //19
'Válaszd ki a súlyodat', //20
'Válaszd ki a keresett magasságát', //21
'Válaszd ki a keresett súlyát', //22
'Válaszd ki a hajad színét', //23
'Válaszd ki a szemed színét', //24
'Válaszd ki a szőrzeted', //25
'Fétis', //26
'Válaszd ki a keresett szőrzetét', //27
'Válaszd ki a keresett fétisét', //28
'Válassz a dohányzással kapcsolatosan', //29
'Válassz az alkoholfogyasztással kapcsolatosan', //30
'Válaszd ki az iskolai végzettségedet', //31
'Írd le a foglalkozásodat,munkádat', //32
'keresett életkora', //33
'Hogy találtál ide?:)', //34
'Írd le a hobbidat', //35
'Rossz hobbimeghatározás. A hobbid talán nem ilyen hosszú {0} karakter', //36
'rossz a hobbid meghatározásának hossza. {0} karakter', //37
'Ide bármit írhatsz magadról', //38
'Rossz leírás mezõ. Talán nem lehet hosszabb {0} karakter', //39
'rossz a szavak mérete. {0} karakter', //40
'Szükséges a fotó! Tölts fel egyet', //41
'Gratulálunk! <br>Aktivációs kódodat elküldtük emailben. <br>Regisztrációd aktiválásához kattints a mailben elküldött linkre!', //42
'Regisztrációd megerõsítése', //43
'Köszönjük hogy regisztráltál hozzánk, a Randi HotBull.hu-ra.
Kérlek, klikkelj az alábbi linkre vagy másold a böngészõ címsorába a következő szöveget a regisztrációd megerõsítéséhez:

', //44
'Köszönjük hogy regisztráltál. Rövid idõn belül elkészül személyes oldalad a rendszerben. Hamarosan aktiváljuk...', //45
'Gratulálunk! <br>Adatlapod sikeresen bekerült rendszerünkbe!<br><br> Lépj be, hogy találkozhass a többi felhasználóval!', //46
'<br>Jelszavad:', //47
'Írd be újra a jelszót', //48
'A jelszó nem egyezik', //49
'Felhasználó regisztrálása<br>A piros * - gal jelölt mezõ kitöltése kötelezõ', //50
'Elõneved vagy beceneved', //51
'betûk', //52
'Utóneved vagy beceneved', //53
'Jelszó', //54
'Jelszó megerõsítése', //55
'Születési idõ', //56
'Profil típus ', //57
'Milyen kapcsolatot keres', //58
'Megye/Ország', //59
'Email', //60
'Honlap', //61
'ICQ', //62
'AIM', //63
'Telefon', //64
'Város', //65
'Kapcsolati státusz', //66
'Nemi szerep', //67
'Magasság', //68
'Súly', //69
'Hajszín', //70
'Szemszín', //71
'Szőrzet', //72
'Fétis', //73
'Dohányzás', //74
'Alkoholfogyasztás', //75
'Végzettség', //76
'Munka', //77
'Hobbi', //78
'<font style="font-size: 14px">Rövid bemutatkozás: </font><br><span class="yellow">Tilos megadni más elérhetőséget - telefon, email, skype, facebook link, stb.</span>', //79
'Keresek egy ', //80
'Keresett szőrzete', //81
'Keresett fétise', //82
'Keresett kora', //83
'Keresett magassága', //84
'Keresett súlya', //85
'Hogyan találtál ide?', //86
'Fénykép', //87
'Kezdõlap', //88
'Regisztráció', //89
'Belépés', //90
'Keresés', //91
'Email küldés az Adminnak', //92
'FAQ', //93
'Statisztika', //94
'MENÜ', //95
'Üzenetek', //96
'Kedvenceid', //97
'Megtekintés', //98
'Módosítás', //99
'Jelszó cseréje', //100
'Adatlap törlése', //101
'Kilépés', //102
'Folyamat idõtartama:', //103
'mp.', //104
'<font color=yellow size=-1>ONLINE</font>', //105
'Vendégek jelenleg:', //106
'Copyright <a href="https://www.hotbull.hu"> 2018 HotBull.hu </a><br>', //107
'Csak a regisztrált felhasználók vehetik igénybe a részletes keresést', //108
'A "kortól" érték nem kisebb, mint a "korig"', //109
'Nincs találat a keresésre', //110
'Nincs', //111
'Van', //112
'Nem sikerült csatlakozni a szerverhez<br>A mysql név vagy jelszó hibás.<br>Ellenõrizd a config fájlban', //113
'Nem sikerült csatlakozni<br>Az adatbázis nincs kész<br>Vagy cserélj adatbázisnevet a config fájlban', //114
'Oldalak:', //115
'Keresés találatai', //116
'Összesen: ', //117
'Felhasználónév', //118
'Cél', //119
'Kor', //120
'Megye/Ország', //121
'Város', //122
'Utolsó belépés', //123
'Regisztráció dátuma', //124
'Részletes keresés', //125
'Felhasználó ID#', //126
'Elõ vagy becenév', //127
'Utó vagy becenév', //128
'Csillagjegy', //129
'Magasság', //130
'Súly', //131
'Elkötelezetség', //132
'Milyen kapcsolatot keres', //133
'Kapcsolati státusz', //134
'Nemi szerep', //135
'Hajszín', //136
'Szemszín', //137
'Szőrzet', //138
'Fétis', //139
'Dohányzás', //140
'Alkoholfogyasztás', //141
'Végzettség', //142
'Felhasználó keresése', //143
'Honlap', //144
'ICQ', //145
'AIM', //146
'Telefon', //147
'Regisztrálva ', //148
'Rövid eredmények', //149
'Találat az oldalon', //150
'Egyszerû keresés', //151
'Belépés csak regisztrációval', //152
'Belépés lezárva rossz profil miatt', //153
'A felhasználó jóváhagyásra vár', //154
'Köszönjük, a felhasználót hamarosan jóváhagyjuk', //155
'Hozzáférés lezárva a kedvencek használatához', //156
'A felhasználó már a kedvenceid között van', //157
'Rendben, a felhasználó sikeresen hozzáadva a kedvenceidhez', //158
'Az adatlapod sikeresen ellenõrzve lett az admin által!', //159
'Adatlapod aktiválása megtörtént', //160
'Hiba az aktiválásnál. Elõfordulhat hogy már aktiválva lett', //161
'FAQ adatbázis üres', //162
'FAQ válaszok#', //163
'Az összes mezõt ki kell tölteni', //164
'Üzenetedet sikeresen elküldtük', //165
'Kérlek töltsd ki a tárgy mezõt', //166
'Irj üzenetet', //167
'Tárgy', //168
'Üzenet', //169
'Küldés', //170
'Regisztrált felhasználóknak', //171
'Belépõ ID', //172
'Elfelejtett jelszó', //173
'Ajánlj minket', //174
'Barát-{0} email', //175
'Boldog <br>születésnapot!', //176
'Ma nincs szülinapos', //177
'Köszöntünk a HotBull.hu Randi oldalán', //178
'Hotbull Randi - meleg társkereső oldal. Nagyszerû módja annak, hogy meleg szexpartnert keress, barátokra találj, vagy csak szórakozz, randizz vagy hosszabb kapcsolatra tegyél szert. Nálunk biztonságban és szórakoztató módon találkozhatsz és beszélgetsz másokkal. Csevegj más meleg fiúkkal, küldj üzenetben képet magadról másoknak, hogy jobban megismerjenek. Meleg társkereső profilod részletes kitöltésével és képek feltöltésével még jobb eredményt érhetsz el.', //179
'{0} mostanában aktív felhasználó', //180
'Gyors keresés', //181
'<font color=yellow>Részl. keresés</font>', //182
'Véletlen fénykép', //183
'Egyszerûsitett statisztika', //184
'Az ID-ednek numerikusnak kell lennie', //185
'Rossz felhasználónév vagy jelszó', //186
'Üzenet küldése emailként', //187
'Üzenet küldése emailként a következõ felhasználónak ID#', //188
'Jelenleg nincs tagunk belépve', //189
'Ajánlott oldal jelenleg nem elérhetõ', //190
'Ajánlat számodra {0} tól', //191
'szia  {0}!

Hogy vagy?:)

Nézd meg ezt a randis-társkeresõs oldalt, tök jó :
{1}', //192
'légy szíves ird be helyesen barátod cimét#{0} email', //193
'Kérlek ird be a nevedet és a mélcímedet', //194
'A jelszavad {0}', //195
'Ezt az adatlapot vagy törölték vagy nem létezett az adatbázisunkban.<br>Ha gondod van, ird meg az adminisztrátornak, amint tudunk válaszolunk. Mellékeld légyszives az azonosítódat is.', //196
'Szia!

Bejelentkezõ azonosítód ID#:{0}
Jelszavad:{1}

_________________________
{2}', //197
'Jelszavad módosításához elküldtük a további teendőkről az információt az általad megadott email címre.', //198
'Kérlek írd be az azonosítódat', //199
'Jelszó küldése', //200
'Hozzáférés lezárva a kimenõ üzeneteknek', //201
'Üzenet küldése ennek a felhasználónak ID#', //202
'Értesítés ha a címzett elolvasta az üzenetet', //203
'Nincs ilyen felhasználó az adatbázisunkban', //204
'Statisztika nem elérhetõ', //205
'A keresett nem aktív vagy nem létezik', //206
'Profil ID#', //207
'Felhasználó elõ vagy beceneve', //208
'Felhasználó utó vagy beceneve', //209
'Születési idõ', //210
'Email', //211
'Üzenet a randi és társkeresõbõl', //212
'Foglalkozás', //213
'Hobbi', //214
'<span class="yellow">Rövid bemutatkozás </span>', //215
'Népszerûség', //216
'Email küldése', //217
'Jelentem', //218
'Kedvelem', //219
'Nem sikerült a feltöltés, <br>vagy a fájl nagyobb mint a megengedett {0} Kb határ. Amit fel akartál tölteni az {1} Kb', //220
'A képnek amit fel akartál tölteni,vagy a szélessége nagyobb mint {0} px vagy a magassága nagyobb mint {1} px limit.', //221
'Ilyen kiterjesztésû képet nem tölthetsz fel. A fájltipusod - ', //222
'(Max. {0} Kb)', //223
'Statisztika országonként', //224
'Nincs új üzeneted', //225
'összes üzenet - ', //226
'Ssz.', //227
'Név', //228
'dátum', //229
'Törlés', //230
'<small>Új</small>', //231
'Kiválasztott üzenetek törlése', //232
'A másik felhasználó: ', //233
'Válasz', //234
'

-----------------
{0}
{1}', //235
'Az üzenetedet elolvasták', //236
'<span class=dat>{0}</span>', //237
'{0} üzenet törölve!', //238
'Kérem a régi jelszót', //239
'Kérem az új jelszót', //240
'Az új jelszó megerõsítése', //241
'Jelszó cseréje', //242
'Régi jelszó', //243
'Új jelszó', //244
'Új jelszó megerõsítése', //245
'Nincs felhasználó a kedvenceid között', //246
'Hozzáadás dátuma', //247
'Kiválasztott felhasználók törlése', //248
'Fontos tudnivalók!<br><br>
<font style="color: black; font-weight: normal;">Az adatlap törlése a következőképpen lehetséges:<br>
1. Ha törölni szeretnéd az adatlapodat, akkor add meg lent az ID-dat és a jelszavadat (Az ID-t megtalálod az adatlapod alsó részében).<br>
2. A törlés nem végleges és visszaállítható, az adatlapod 30 napig megőrzésre kerül.<br>
3. Amennyiben vissza szeretnéd állítani az adatlapodat, csak lépj be újra.<br>
4. Ha másik profilt szeretnél regisztrálni, fenn fogsz akadni, mert a törlésre jelölt profilokat is beszámítjuk.<br>
5. Ha letelik a 30 nap, akkor minden adatodat töröljük. Utána a visszaállítás már nem lehetséges.<br><br>
Figyelem! Az Admin nem kérhető arra, hogy törölje az adataidat. A 30 napos türelmi időt mindenképpen meg kell várnod.<br>
Ha ki vagy tiltva a rendszerből, törlés nem kérhető, mert adataidat a visszaélések megakadályozása miatt vannak megőrizve!<br> 
A GDPR szerinti felejtés joga akkor lép életbe, amikor a 30 nap törlési idő lejár, ekkor minden adatodat töröljük.</font>', //249
'A felhasználó ID#={0} törölve lett rendszerünkbõl', //250
'Az adatlapod 30 nap után lesz véglegesen törölve. Addig ha belépni próbálsz, adataidat visszaállítjuk. Felhasználói neveddel és email címeddel újra regisztrálhatsz.', //251
'{0} felhasználó(k) törölve a kedvenceid közül!', //252
'Valami gond van a jelszóval,esetleg érvénytelen karaktereket tartalmaz', //253
'Nincs jelenleg hozzáférésed a jelszócseréhez', //254
'Rosszul irtad be a régi jelszót,helyesbítsd!', //255
'Jelszó sikeresen megváltoztatva!', //256
'Nem biztos hogy az összes képet igy törölni tudod', //257
'Adatlapod megváltoztatása sikeres volt', //258
' - Kép törlése', //259
'A folyamat lezárult. Bezárhatod a böngészõdet', //260
'Zászlókép nem elérhetõ', //261
'Nyelvek', //262
'OK', //263
'Felhasználónév (3-16 ékezet nélküli betű vagy szám)', //264
'Felhasználónév', //265
'A felhasználóneved 3-16 karakter hosszú lehet és ékezetmentes betûket és számokat tartalmazhat', //266
'Ez a név már foglalt. Légy szíves válassz másikat!', //267
'<font color=yellow size=-1>Összes felhasználó</font> - {0}', //268
'A funkció használatához elõfízetés szükséges.<br><br>Elõfizetni a szolgáltatásra <a href="http://randi.hotbull.hu/members/index.php?l=hu&a=r" class=head>itt tudsz!</a>', //269
'Felhasználó típus', //270
'Fázis', //271
'Keresési eredmény pozíció', //272
'Ár', //273
'Hónap', //274
'Fizetés utolsó dátuma', //275
'Nagyobb mint', //276
'Fizetés', //277
'Fizetés ezzel', //278
'PayPal', //279
'Köszönjük a regisztrációt. Fizetés megtörtént,hamarosan ellenõrizzük.', //280
'Hiba! Próbáld újra vagy beszélj az adminnal', //281
'Tájékoztató mail küldése az elõnyökrõl', //282
'Felhasználó típusa módosítva.', //283
'Email a gratulációval sikeresen elküldve.', //284
'Irányítószám', //285
'Gratulálunk, 

Státuszod megváltozott {0}. A következõ {1} hónapban élvezheted a kibõvített szolgáltatásokat.

Most ellenõrizheted az üzeneteidet az üzenõdobozban.

__________________________________
{2}', //286
'Gratulálunk', //287
'Irányítószám csak numerikus lehet', //288
'Kulcsszavak', //289
'Bocsika de a következõ hiba történt:', //290
'Szia kedves felhasználó!
Adatlapodat sikeresen hozzáadtuk a rendszerhez {2}.

Ne felejtsd el:
Loginneved:{0}
Jelszavad:{1}

_________________________
{2}', //291
'Feltölthetsz egy hangfájlt a hangoddal', //292
'Képeslap küldése', //293
'Megerõsít', //294
'Ismeretlen', //295
'Hang', //296
'Törlés', //297
'Képeslap küldése...', //298
'A képeslap jelenleg nem elérhetõ', //299
'Válaszd ki a képet az üdvözlõlaphoz', //300
'Képeslap tõle: {0}', //301
'Szia {0}

Küldött neked valaki egy képeslapot. 
Hogy meg tudd nézni klikkelj az alábbi linkre:
{1} 

Képeslapodat {2} napig õrizzük meg
', //302
'Klikkelj ide', //303
'Ez a képeslap lejárt, vagy nem létezik', //304
'{0} Képeslap', //305
'Csatlakozás', //306
'Hogy használd a csatlakozást?', //307
'Hívd ismerõseidet a következõ linkkel:
<br>
{0} - Kezdõlap<br>
{1} - Adatlap keresése<br>
{2} - Nézd meg az adatlapom<br>
<br>
Bármelyik linket csatlakoztathatod. Csak add aff={3} url címedhez.<br><br>Például:<br>{4}<br><br>
Igy megy:<br> 
', //308
'<li>{0}$ mind a {1} felhasználónak</li>', //309
'Klikkelj ide hogy megnézd az infomat', //310
'Note: Minimum pénzfizetés - {0}$', //311
'Csatlakozási statisztika', //312
'Meghívott felhasználó', //313
'Pénz ($)', //314
'<br>Irhatsz észrevételeket a díjazással kapcsolatban is. Kérlek mellékeld az azonosítódat is [ID#{0}].', //315
'ellenõrzés', //316
'Tiltólista', //317
'Nincs senki a tiltólistádon', //318
'Letiltom', //319
'Ez a felhasználó már a tiltólistádon van', //320
'Hozzáférés lezárva a tiltólistához', //321
'Oké, a felhasználót hozzáadtuk a tiltólistádhoz', //322
'{0} felhasználó sikeresen eltávolítva a tiltólistádról!', //323
'Nem tudsz üzenni ennek a felhasználónak, <br/>vagy a tiltólistára rakott, vagy a tiltólistádon van. <br/><a href="{0}">Itt találod a tiltólistádat.</a>', //324
'Valószínüleg mindenkit tiltólistára tettél,próbáld meg eltávolitani a listáról õket.', //325
'Összes felhasználó tiltása', //326
'Összes tiltás feloldása', //327
'Mindenkit letiltottál', //328
'Minden felhasználót letiltottál', //329
'Minden felhasználó tiltása feloldva', //330
'{0} új üzeneted érkezett', //331
'Valaki a kedvenceihez adott', //332
'{0} hozzáadott a kedvenceihez.', //333
'Én egy', //334
'Keresek egy', //335
'Elõzõ', //336
'Következõ', //337
'<font color="yellow">{0} mostanában aktív fényképes felhasználó</font>', //338
'Chat', //339
'A chat csak a regisztrált tagoknak elérhetõ', //340
'A {0} legnépszerûbb felhasználó jelenleg', //341
'Jelenleg legnépszerûbbek', //342
'Jelenleg legnépszerûbb <font color=blue>Pár</font>', //343
'A Hotbull Randi újdonságai', //344
'Hírek jelenleg nincsenek', //345
'Hírarchívum', //346
'Nem elérhetõ', //347
'<br><small>Nem látható az adatlapon</small>', //348
'Válasz:', //349
'Online', //350
'Offline', //351
'Jelenleg', //352
'Fórum', //353
'Vendég', //354
'Hozzáférés megtagadva', //355
'Új téma létrehozása', //356
'Cím', //357
'Üzenet', //358
'Kérlek adj meg címet', //359
'Kérlek írd be az üzenetet', //360
'Vendég', //361
'Köszi!<br>Új téma sikeresen létrehozva.', //362
'Válasz erre', //363
'Köszi!<br>Üzenetedet hozzáfûztük', //364
'Adatlap nézete', //365
'Üzenet törlése', //366
'Képeslap elõnézete', //367
'Nem léphetsz be erre a területre', //368
'<div class="desc" style="text-align: center"><p>Szia!</p> <p>Nem vagy  <b>{0}</b> - vagy még nincs profilod</p> <p>Megtekintéshez kattints erre:<br/>{1}</p> <p>Ha nincs profilod, kattints erre:<br/>{2}</p> <p>hiszen több ezer kép és profil vár rád.</p> <p><b>A Hotbull csapat</b></p></div>', //369
'Senki', //370
'Téma eltávolítása', //371
'Téma és összes üzenet ehhez eltávolítva!', //372
'Az üzenet törölve', //373
'Státusz', //374
'Nincs link ehhez az adatbázisban', //375
'Linkek', //376
'Add meg a címedet', //377
'Cím', //378
'További találatok', //379
'Nincs hozzáférésed, valószínüleg bannolva van az IP-címed.', //380
'Köszöntünk {0} <br><br>', //381
'Ismert nyelvek', //382
'Válassz a nyelvek közül', //383
'Ahhoz hogy hozzáférj ehhez a részhez, ilyen szintû hozzáférés kell: {0} ', //384
'Kimerült az üzenetkvótád. Számodra mint: "{0}" napi {1} üzenet küldése biztosítható.', //385
'Feliratkozó E-mail (levelet küldünk az ellenõrzéshez)', //386
'Feliratkozás', //387
'Feliratkozom', //388
'Szia!

Valaki errõl a címrõl IP {0} megnézte,vagy megpróbálta módosítani az adatlapodat {1} .

Ha nem te voltál, ne tegyél semmit - automatikusan töröljük {2} nap múlva.

Ha te regisztráltál vagy módosítottad az adatlapodat,kérlek erõsítsd meg. Ezen a linken megteheted:

{3}

Klikkelj a linkre,vagy másold a böngészõ címsorába.
', //389
'{0} aláírás, megerõsítés', //390
'sikeresen megerõsítve', //391
'Köszönjük!

Sikeresen megerõsítve. 
Mailcímed: {0} bekerült az adatbázisba.
Értesítünk rendszeresen a változásokról.

Ha le szeretnél errõl iratkozni,jelezd és törlünk a listáról.
____________________
{1}', //392
'Fénykép nem található', //393
'Klikkelj ide további információkért', //394
'Ez az azonosító nem létezik,törölve lett,vagy megerõsítésre vár.', //395
'Email címed : {0} sikeresen eltávolítottuk.', //396
'Szia!

Emailcímed: {0} sikeresen eltávolítottuk adatbázisunkból {1}.
', //397
'Rákacsintok', //398
'Rákacsintottál', //399
'***Valaki rád kacsintott***', //400
'Valakinek tetszel. Nézd meg, ki az, és ha neked is tetszik, üzenj neki.', //401
'Minden felhasználó', //402
'Kérlek, hogy írd le röviden, hogy miért jelented ezt a profilt.', //403
'<br><small>Ez a profil nem látható, kivéve<br>ha a hozzáférési szint minimum {0}</small>', //404
'Új felhasználó regisztrált', //405
'Új felhasználó regisztrált a {0}-n.

A profilját itt nézheted meg:
{1}
', //406
'Új felhasználó regisztrált a {0}-n.

Ellenõrizheted az adatait az admin felületen', //407
'Következõ olvasatlan üzenet', //408
'Nincs több olvasatlan üzenet', //409
'Felhasználók elõfizetései', //410
'Látható lehetsz a profilok közt', //411
'Beléphetsz az oldalra', //412
'Mi az, amit tehetsz', //413
'Használhatsz más email címet a visszajelzéshez', //414
'Jelenthetsz szabálytalan profilokat', //415
'Megnézheted a profilokat', //416
'Küldhetsz email a felhasználó számára', //417
'Használhatsz más email címet az levélküldésnél', //418
'Sablon váltása', //419
'Boldog szülinapot {0} {1}', //420
'Boldog születésnapot {0} {1}!!! :)

"Boldog, boldog, boldog születésnapot
Kívánom hogy legyen még sok ilyen szép napod."

Élj sokáig {0}!!!! :)

___________________________
Sok szeretettel, {2}
{3}
', //421
'Beállítás', //422
'Beállítás mentése', //423
'Itt lehet szavazni a felhasználóra<br>(1 - tõl, 10 - ig)', //424
'Sajnálom, önmagadra nem szavazhatsz', //425
'Már szavaztál erre a felhasználóra!', //426
'Köszönjük a szavazatot.', //427
'Látogatóid', //428
'Senki sem látta még az adatlapod', //429
'Szavazás', //430
'Eredmény megtekintése', //431
'Kérlek, válassz a lehetõségek közül', //432
'Sajnálom, már szavaztál az elmúlt {0} órában!', //433
'Még nem szavaztál', //434
'Köszönjük, szavazatodat megkaptuk!', //435
'Nincs még szavazat', //436
'Összesen {0} a szavazat', //437
'szavazások', //438
'Elõfizetésed szerkesztése', //439
'Nincs ilyen felhasználó ezzel az ID-vel és kóddal', //440
'Szia!

Errõl az IP címrõl {0} léptél be, hogy megváltoztasd az elõfizetésedet.

Ha esetleg nem te voltál, akkor kérünk, hogy vedd fel a kapcsolatot az adminnal {1}.', //441
'Elõfizetési adataid sikeresen módosítva', //442
'Az utolsó megnézett oldal', //443
'Melyik megyébõl keres', //444
'Jelõlted kapcsolati státusza', //445
'Jelölted nemi szerepe', //446
'Jelölted hajszíne', //447
'Jelölted szemszíne', //448
'Jelölted dohányzása', //449
'Jelölt alkoholfogyasztása', //450
'Jelölt végzettsége', //451
'Válassz:', //452
'Robot Kerítõ', //453
'A Robot Kerítõ segíthet abban, hogy megtaláld azt a személyt, aki korban és megjelenésben illik hozzád.<br>A tökéletes eredmény kedvéért jelöld ki azt a szempontot, ami alapján összehasonlítjuk a profilt. Az összes paraméter bejelölése adná a legjobb eredményt, de lehet, hogy elenyészõ találatot fogsz kapni.', //454
'Találd meg', //455
'Adatlapod', //456
'Összehasonlítási adat', //457
'A céljainknak meg kell egyeznie', //458
'Nyelvem', //459
'Téma', //460
'Hozzászólás', //461
'Jelenleg online felhasználó', //462
'által', //463
'itt:', //464
'Téma', //465
'Téma felvetõje', //466
'Hozzászólások', //467
'Megnézték', //468
'Utolsó hozzászólás', //469
'Készítette', //470
'Téma szerkesztése', //471
'Moderátorok', //472
'Téma lezárása', //473
'Téma újranyitása', //474
'A témát lezártuk', //475
'A témát újra megnyitottuk', //476
'Téma szerkesztése', //477
'A téma módosítva lett', //478
'Üzenet szerkesztése', //479
'Az üzenet módosítva lett', //480
'SLépés', //481
'Nagy kép', //482
'Keresés részletesen a fiók típusa alapján', //483
'Adatlapra szavazás', //484
'Ki látta az adatlapom', //485
'Ki nézte meg más profilját', //486
'Mennyi üzenetet tud küldeni egy nap egy felhasználó', //487
'Mennyi képet tud feltölteni egy felhasználó', //488
'Találkoztál vele? Ajánlanád másoknak? Akkor írj ajánlást!<br><br>
<span class="yellow">Az ajánlásod azonnal megjelenik, de 24 órán belül átnézzük. Csakis a pozitív ajánlásokat tudjuk elfogadni. Semleges és negatív bejegyzéseket töröljük. <br/>Kapcsolatfelvételhez ne ezt használd, hanem küldj üzenetet! Köszönjük!</span>', //489
'Köszönjük!<br><br>Ajánlásodat hozzáadtuk a profilhoz.', //490
'Nincs ajánlás', //491
'Ajánlás', //492
'Választ kaptál:', //493
'FIGYELEM: Ezzel végleg törlöd a kiválasztott ajánlást. Biztos vagy benne, hogy ezt akarod?', //494
'A kiválasztott ajánlásodat végleg töröltük.', //495
'Köszönöm. A válaszodat a ajánlásra rögzítettük az adatbázisban.', //496
'Kérlek, hogy írd be a helyes Ellenõrzõ kódot.', //497
'Ellenõrzõ kód', //498
'A felhasználói fiók nem aktív. Aktiválnod kell a regisztrációdat a kapott email-lel.', //499
'OOPS: fennakadtál valamilyen szűrőn a regisztáció során. Regisztrációd elfogadása folyamatban. Amennyiben már van másik regisztrációd vagy 7 nappal ezelőtt törölted magad, akkor ezen regisztrációd törlésker kerül. Kérünk, várd meg, míg az adminisztrátor elfogadja, ez max 24 óra, de ennél kevesebb idõ szokott lenni.', //500
'Adatlapod módosítását az Adminisztrátor még nem fogadta el, profilod csak utána frissül. ', //501
'Az adatlapod deaktíválva, törlésre kijelölve. Várd meg, amíg a 7 nap le nem telik. Az újraregisztrálás eddig az ideig tilos. Visszakérni az adatlapodat az info@hotbull.hu címen tudod.', //502
'Az adatlapod nem aktív, valami hiba történt. A regisztrációs folyamat megszakadt, kérlek próbálj meg újra regisztrálni.', //503
'Az adatlapod nem aktív, valami hiba történt. Kérlek, vedd fel az adminnal a kapcsolatot az Email küldése az Admin menüponton keresztül (lap alján a link).', //504
'Várd meg, amíg a kép betöltõdik...', //505
'Összes fotó', //506
'Más képek', //507
'Érvénytelen tevékenység', //508
'Online', //509
'Megnézett profilok', //510
'Nincs adatlap, amit megnéztél az elmúlt {0} napon', //511
'Népszerû szóló férfiak fotóval', //512
'Népszerû párok fotóval', //513
'Üzenek', //514
'Email küldés', //515
'Lejárati dátum', //516
'Lehessen rád szavazni?', //517
'A beállítás módosítása megtörtént!', //518
'Adminisztrátor', //519
'Képeslap szerkesztése', //520
'Hozzáadhatsz némi információt, vagy hagyd üresen', //521
'{0} fénykép nem látható addig, míg a moderátor le nem ellenõrizte. Gyere vissza késõbb...', //522
'Várj, míg az oldal frissül...', //523
'Orientáció', //524
'Testalkat', //525
'Érdeklõdés', //526
'Megye/Ország', //527
'A felhasználói szinted "{0}" nem láthatsz többet {1} találati oldalnál!', //528
'Any', //529
'Keresési hiba.<BR>Valószínû, hogy<BR>- Hibás keresési kulcs<BR>- Nincs jogosultságod a kereséshez a megadott adatokkal<BR>- Hiba a keresésben.', //530
'Kérlek, töltsd ki a következõ adatokat:', //531
'nincs', //532
'{0}', //533
'Egyéb információ', //534
'Kuponok', //535
'Add meg a kuponkódot', //536
'Congratulations, 

Your status has been changed to {0}. 

These privileges will be available in current {1} days.
__________________________________
{2}', //537
'New {0} status is received on {1}', //538
'New privilegied user with IP:{0} has been registered in {1}
									User type: [{2}]
									User ID: {3}', //539
'New privilegied user has been registered!', //540
'Thanks! Your coupon code has been successfully accepted! <BR>Now you have {0} status!', //541
'Coupon code hasn`t been accepted. Please check it carefully!', //542
'Minimálisan feltöltendõ képek: ', //543
'{0} üzenet', //544
'Idõkülönbség órákban számolva (pl.: <B>-3</B> or <B>+2</B>)', //545
'Az oldal szerinti idõ: <B>{0}</B>', //546
'Az beállításaim szerinti idõ: <B>{0}</B>', //547
'Szeretnék elõfizetéses felhasználó lenni', //548
'You haven`t enough privilegies to create new threads in this forum.', //549
'You haven`t enough privilegies to reply in this forum.', //550
'Még senki nem vett fel kedvencnek', //551
'Vagy', //552
'És', //553
'Profil megtekintése', //554
'Please check at least one profile!', //555
'A kiválasztott profilokkal a következőt teszem:', //556
'Kedvelnek', //557
'Összes kapcsolat:', //558
'A fiókod törlésre kerül a {0}-ról', //559
'A fiókod törlésre kerül a {0}-ról.

Ez valamelyik okból történhetett:
- Korábban törlésre jelölted a profilodat, és most került véglegesen törlésre.
- Több mint egy éve léptél be utoljára.

--
Üdv,
A {0}
{1}', //560
'Payment membership privilegies', //561
'Privileged users timelife (in month)', //562
'Prices ({0})', //563
'How many search pages can user to see (0 - means unlimited)', //564
'Purchase link', //565
'You may purchase right now any from Privileged packages by your choice and to get Privileged rights immediately!', //566
'Can see other comments?', //567
'Can see own comments?', //568
'Can reply to comments', //569
'Can add new comments', //570
'Can remove comments', //571
'Can see in search results', //572
'Can see visited profiles', //573
'Can see random templates', //574
'Can not be in rating', //575
'Can see the day of birthday', //576
'Can use filter by genders in online users page', //577
'Can use filter by user type in online users page', //578
'Can use filter by picture in online users page', //579
'Allowable fields to search in the Simple Search', //580
'Allowable fields to search in the Advanced Search', //581
'Can use smiles and special tags in messages?', //582
'Can see user photo in the comments?', //583
'Can see number of search results?', //584
'Make sticky', //585
'Cancel sticky', //586
'Thread has been make sticky', //587
'Sticky property for this thread has been canceled', //588
'This thread is sticky', //589
'Search by forum', //590
'All forums', //591
'Search posts for', //592
'Search in and Show results as', //593
'Posts', //594
'Threads', //595
'No forum available for search', //596
'You can`t search more often than once in {0} seconds. <br><br>Try again after {1} seconds.', //597
'Slideshow', //598
'Ascending', //599
'Descending', //600
'Forum Options', //601
'Allow BB codes and smiles', //602
'All', //603
'Posts', //604
'Signature', //605
'Current signature', //606
'Change Signature', //607
'Signature has been successfully changed', //608
'Top users by forum posts', //609
'Forum`s Top', //610
'Top users by forum threads', //611
'Top threads by forums', //612
'Signature deprecated by moderator', //613
'No avatar', //614
'Select from list', //615
'Upload your avatar', //616
'Your avatar must be no bigger than {0} pixels by {1} pixels in size. Avatars must be no larger than {2} KB. The following file types are allowed: {3}', //617
'Avatar has been successfully changed', //618
'Keyword or Author field too long', //619
'Your search has been completed, please stand by as we take you to the search results.<br><br>Please wait while we transfer you...<br><br><a href="{0}"{1}>(Or click here if you do not wish to wait)</a>', //620
'Show the link to this post', //621
'Manually copy the direct link to this post below to store the link in your computer`s clipboard', //622
'WARNING: This will permanently remove the selected. Are you sure you wish to do this?', //623
'Remove the selected', //624
'Kijelöl/töröl', //625
'Selected has been successfully removed', //626
'<span style="font-size: 14px  !important">Senki nem tökéletes, a napnak is megvannak a maga foltjai.</span><!-- Profilodat {0}% ban töltötted ki {1}Ide kattintva{2} a hiányzó részt is  kitöltheted. -->', //627
'<span style="font-size: 14px  !important">A szerelem elõször egy tekintet, azután egy mosoly, azután egy szó, egy ígéret, egy találkozás.</span><!-- Még nem töltöttél fel képet. {0}Ide kattintva{1} ez pótolhatod. -->', //628
'<span style="font-size: 14px  !important">Csak az igazi barát figyelmeztet rá, hogy maszatos az arcod.</span><!-- Említettük, hogy nagyon kevés információt adtál meg magadról.  {0}Ide kattintva{1} türelmesen töltsd ki, hogy eredményesebb legyen az ismerkedésed. -->', //629
'<br><br>Módosításod azonnal érvénybe lépett, de az Adminisztrátor át fogja nézni a módosításod, hogy amennyiben szükséges, módosítást hajtson végre. Módosítási indokok: telefonszám, lakcím, email cím szereplése a profilon, valamint olyan kép törlése az adatlapról, ami nem tud megjelenni.', //630
'Elfogadom a felhasználói feltételeket', //631
'Felhasználói feltételek', //632
'Vélemény az oldalról', //633
'Vélemény hozzáadása', //634
'Véleményed', //635
'Köszönjük!<br><br>A véleményedet rögzítettük.', //636
'Jelenleg nincs elérhetõ vélemény az oldalról. De, küldj be magad, ezzel is növelve a lap színvonalát.', //637
'Kapjak hangjelzést, ha üzenetem érkezett', //638
'View Name', //639
'To create new view just enter name here, and define search parameters.', //640
'Views', //641
'View name', //642
'No view available', //643
'Create new View', //644
'Articles', //645
'No article available', //646
'My Blog', //647
'Add new entry', //648
'There are {0} entries total', //649
'Make public', //650
'New blog entry has been added', //651
'No blogs available<br><br> {0} Create new {1}', //652
'Creation date', //653
'Last update', //654
'Public', //655
'Private', //656
'Make private', //657
'Edit blog entry', //658
'Please select at least one entry!', //659
'Selected entry`s status has been changed to public', //660
'Selected entry`s status has been changed to private', //661
'Blog has been successfully changed', //662
'A felhasználó kedvenceinek listája', //663
'Kedvencek', //664
'Nincs olyan felhasználó, akit kedvel', //665
'Nincs jogosultságod megtekinteni a kedvenceinek listáját - ehhez neked is benne kell lennie a kedvencei között', //666
'---------------------
This email has been sent from {0} by online mail form', //667
'Video', //668
'Sound or Video', //669
'You can upload your video', //670
'You can upload your sound or video', //671
'Move Thread', //672
'Private photos', //673
'You can upload maximum <b>{0}</b> private photos', //674
'No private photos available<br><br> {0} Create new {1}', //675
'Upload new private photo', //676
'Description', //677
' (Max. 255 chars)', //678
'Who can to see this photo?', //679
'Your private photo has been successfully uploaded', //680
'There are {0} private photos total', //681
'Checking by moderator', //682
'{0} private photos have been successfully removed', //683
'Photo ID#', //684
'Wrong password', //685
'Don`t use', //686
'Select glitter chars for subject', //687
'Use glitter chars for subject', //688
'Select glitter chars for message', //689
'Use glitter chars for message', //690
'You should to use english chars only', //691
'Your user type <b>{0}</b> can create only <b>{1}</b> views', //692
'View "{0}" has been successfully created!', //693
'Points', //694
'Point`s number', //695
'You must be at least a {0} or to pay {1} points to access this data', //696
'You must to pay {0} points to access this data', //697
'You have total {0} points and can`t to see {1}, requires {2} points for this profile.<br>You may to purchase additonal points from <a href="{3}">here</a>', //698
'You have total <b>{0}</b> points. Are you sure to pay <b>{1}</b> point to see user`s <b>{2}</b>?', //699
'OK', //700
'Cancel', //701
'<b>{0}</b> points have been taken from your balance. Now you have <b>{1}</b> points.<br><br><b>Please do not reload this page to prevent taking a points again!</b>', //702
'Congratulations, 

You have been successfully purchased {0} points. 
__________________________________
{1}', //703
'{0} points has been received on {1}', //704
'Points has been purchased from IP:{0} has been registered in {1}
									User ID: {2}', //705
'{0} points has been purchased!', //706
'Felhasználói feltételek', //707
'Adatvédelmi nyilatkozat', //708
'Edit User', //709
'Deprecate using signature', //710
'Allow to use signature', //711
'Ban user', //712
'Select forums which user willn`t access', //713
'Upload / Select Avatar', //714
'Full List', //715
'Don`t ban user', //716
'You have been banned for this forum', //717
'Forum posts', //718
'Click here to see user`s forum posts', //719
'All smiles', //720
'Search within', //721
'miles', //722
'Click here to see user`s blog', //723
'Blog', //724
'The Blog of {0}', //725
'Can have signature for forum', //726
'Can use avatar', //727
'Can upload avatar', //728
'Can use forum options', //729
'Can see slideshow', //730
'Can see big photos in the slideshow', //731
'Can see testimonials', //732
'Can add testimonials', //733
'Can use smiles and BB codes in the testimonials', //734
'Can to have own blog', //735
'Can to see other blogs', //736
'Can to use smiles and BB code in the blog', //737
'Can to see articles', //738
'Can create views', //739
'Can upload private photos ', //740
'Can see private photos', //741
'Can to use glitters in the ecard subject', //742
'Can to use glitters in the ecard message', //743
'Can to see Favorites list of the user', //744
'Rejtsd el a profilom a keresési találatokban és az online felhasználók listájában', //745
'{0} rejtett felhasználó', //746
'You can`t to contact user with the same gender as yours.<br><br>', //747
'Sorry. You can only to change your profile once in {0} hours.', //748
'Tegnap', //749
'Ma', // 750
'Features',//751
'{0} hidden or filtered users',//752
'User made profile hidden',//753
'Users here: ',//754
'Private notes',//755
'No notes available<br><br> {0} Create new {1}',//756
'Entry has been successfully changed',//757
'Entry has been successfully added',//758
'Edit entry',//759
'Thanks! Your coupon code has been successfully accepted!<br>You have received {0} points!',//760
'Visible for access level and higher',//761
'Ne kapjak emaileket az oldalról',//762
'You can upload maximum <b>{0}</b> private photos for <u>{1}</u> category',//763
'User is registering',//764
'Post',//765
'A profil oldalam kinézete',//766
'Nem akarok kacsintásokat kapni másoktól',//767
'A felhasználó nem szeretne kacsintásokat kapni',//768
'Submit site',//769
'URL',//770
'Limit',//771
'Your name',//772
'Category',//773
'Reciprocal Link URL',//774
'To validate the reciprocal link please include the following HTML code in the page at the URL specified above, before submiting this form',//775
'URL is already exist in the our database',//776
'Incorrect Reciprocal Link URL',//777
'Confirm Link Submission',//778
'Thanks for submitting your website at '.C_SNAME.'.
Please click here to confirm your submitting:

',//779
'Error: {0} could not be opened, the location probably does not exist',//780
'Reciprocal Link URL must be from the same domain as a submitted URL',//781
'Our html code haven`t been found on the your Reciprocal URL. Please check all and resubmit again.',//782
'Thank you! Your URL has been successfully added!',//783
'Thank you! An email has been sent to you to confirm your submitting!',//784
'Thank you! Your URL will be added after admin checking!',//785
'URL hasn`t been found or already submitted!',//786
'Detail',//787
'Details information for',//788
'Link Owner',//789
'No new photos available for your criterias. Visit later or change criterias.',//790
'Select your photos participate in the Hot or Not rating',//791
'Hot or Not',//792
'You must enable "{0}" in the {1} to enable photos for rating',//793
'Rating',//794
'Rate users',//795
'Show me',//796
'Between age',//797
'Top Men Photos',//798
'Top Women Photos',//799
'Top Users Photos',//800
'Blogs',//801
'More',//802
'Full text',//803
'Blogs for last {0} days',//804
'Sorry, You have already rated!',//805
'Hozzám kapcsolódó profil',//806
'Megadhtasz egy profil azonosítóját, akivel kapcsolatban állsz. Csak add meg az ID számát, kattints a Check gombra, majd írj egy rövid leírást a kapcsolatotokról. ',//807
'Leírás (pl. "A partnerem")',//808
'You can`t link itself',//809
'{0} Hour',//810
'You haven`t enough points for this operation',//811
'How many hours to purchase?',//812
'Archive',//813
'There are no messages in the archive!',//814
'Clear',//815
'Other',//816
'All categories',//817
'There are no links in this Category',//818
'An error occured during loading of a {0} file. File may be corrupted. Please select other file!',//819
'Can contact users with the same gender',//820
'Can use filter by country in online users page',//821
'Can use filter by city in online users page',//822
'Can use filter by age in online users page',//823
'Can to deprecate view him profile',//824
'Can be anonymous when access other profiles',//825
'Can to change template for own profile',//826
'Can prefer not to receive emails from website',//827
'Can to see "Users here" on the forum',//828
'Can to see forum statistics',//829
'Can to get information about new messages without page reloading',//830
'Can create private notes',//831
'Can not to verify image in the: "{0}"',//832
'Blog comments',//833
'Can to select access level for each photo',//834
'Can prefer not to receive winks',//835
'Can use superwink feature',//836
'Can to add link',//837
'Can to Rate photos from the user profile',//838
'Can to Rate photos from "Hot or Not" random page',//839
'Can to participate in the "Hot or Not"',//840
'Can to see link to profile, when rate',//841
'Can to access to "More Results" page of the Best photos',//842
'Can to comment blogs',//843
'Can to see comments in the blogs',//844
'Can to rate blogs',//845
'Can to reply to comments for own blog',//846
'Can to link to other profile',//847
'BBCodes Access',//848
'Can to use smiles',//849
'Can to use <b>Bold</b>',//850
'Can to use <i>Italic</i>',//851
'Can to use <u>Underline</u>',//852
'Can to use <s>Strike</s>',//853
'Can to use QUOTES',//854
'Can to use URL',//855
'Can to use IMG',//856
'Can to use VIDEO',//857
'Profile comments',//858
'Limitations',//859
'Beérkező', //860
'Küldött', //861
'Üzenetet kaptál', //862
'Fényképes üzenetet kaptál', //863
'Feloldom', //864
'Felhasználót a tiltólistádról levetted.', //865
'Elveszett jelszó, aktiváció', //866
'Aktivációs kód', //867
'Elfogadom, hogy a megadott email címemet a Hotbull.hu csakis a kérdéseim megválaszolására használhatja.', //868
'El kell fogadnod a Hotbull.hu email cím kezelési szabályát.', //869
'Kötelező kitölteni', //870
'Üzenetet küldtél', //871
'Fényképes üzenetet küldtél', //872
'Valaki rád kacsintott', //873
'OOPS: fennakadtál valamilyen szűrőn a regisztáció során. Regisztrációdat rögzítettük, de fel kell venned az adminnal a kapcsolatot az Email küldése az Admin menüponton keresztül (lap alján a link). Bővebb információkkal csak az Admin tud szolgálni, valamint az Adminok is aktiválhatják a profilodat. Kérjük, hogy a regisztráció során megadott email címet megadva érdeklődj.', //874
'<span class="yellow">A profilkép mindig a legfelső kép. <br/> Csakis olyan képet tölthetsz fel, mely rólad készült. <br/> Tilos feltölteni pl. valahonnan másolt képet, képernyőképet, rajzot, képregényt, logót, csendéletet, tárgyról készült képet, pornósztár képet.<br/> A képek nem tartalmazhatnak vízjelet, nem lehetnek rajtuk az elérhetőségeid, és olyan képet se tölthetsz fel, ami nem egy meleg férfiaknak szóló társkeresőre való (pl.: meztelen nő is van rajta)</span>', //875
'Bejövő üzenetek', //876
'Kimenő üzenetek', //877
'Hozzáférésedet az üzenetküldéshez blokkoljuk a profilod jóváhagyásáig', //878
'Elfelejtett név', //879
'Elküldtük az információt az általad megadott email címre.', //880
);  
?>
