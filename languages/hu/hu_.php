<?php 
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.8.0                    #
# Written by              AzDG (support@azdg.com)          #
# Created 03/01/03        Last Modified 05/02/03           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               en_.php                          #
# File purpose            Addition to english langfile     #
# File created by         AzDG <support@azdg.com>          #
############################################################

$wg=array(
'Nem tudom',
'Férfi'
);

$wp=array(
    'Nem tudom',
    'Szex',
    'Randi',
    'Barátság',
);

// Privileged users names
$wpu=array(
'Regisztrált felhasználó', // Free user
'Silver member',
'Gold member',
'Platinum member');


$wm=array(
'Mindegy',
'Független',
'Párok',
'Kapcsolatot kereső',
'Elvált',
'Nõs/Házas',
'Kapcsolatban',
'Bonyolult'
);

$wmm=array(
'Mindegy',
'Január',
'Február',
'Március',
'Április',
'Május',
'Június',
'Július',
'Augusztus',
'Szeptember',
'Október', // [10]
'November',
'December'
);

$wc=array(
'Mindegy',
'Aktív',
'Passzív',
'Uni',
'Uni/aktív',
'Uni/passzív',
'Párban',
'Csak orál',
'Csak maszti',
'Csoportszex'
);

$wh=array(
'Mindegy',
'140cm', 
'142cm',
'145cm',
'147cm',
'150cm',
'152cm',
'155cm',
'157cm',
'160cm',
'162cm',
'165cm',
'167cm',
'170cm',
'172cm',
'175cm',
'177cm',
'180cm',
'182cm',
'185cm',
'187cm',
'190cm',
'192cm',
'195cm',
'197cm',
'200cm',
'202cm',
'205cm',
'207cm',
'210cm',
);

$ww=array(
'Mindegy',
'50kg',
'52kg',
'55kg',
'57kg',
'60kg',
'62kg',
'65kg',
'67kg',
'70kg',
'72kg',
'75kg',
'77kg', 
'80kg',
'82kg',
'85kg',
'87kg',
'90kg',
'92kg',
'95kg',
'97kg',
'100kg',
'102kg',
'105kg',
'107kg',
'110kg',
'112kg',
'115kg',
'117kg',
'120kg',
'122kg',
'125kg',
'127kg',
'130kg',
);

$whc=array(
'Mindegy',
'Fekete',
'Szõke',
'Barna',
'Szõkésbarna',
'Vörös',
'Õsz',
'Õszes'
);

$we=array(
'Mindegy',
'Fekete',
'Barna',
'Kék',
'Zöld',
'Szürke',
'Egyéb'
);

$wet=array(
'Mindegy',
'Szőrtelen',
'Borotvált',
'Enyhén szőrös',
'Szőrös',
'Nagyon szőrös'
);

$wr=array(
'Mindegy',
'Nincs',
'Szado',
'Mazo',
'Alsónemű',
'Sport cucc',
'Női ruhák',
'Extrém',
'Fist',
'Watersport',
'Dildó', // [10]
'Láb',
'Cipők',
'Öltöny',
'Munkásruha',
'Bőr',
'Paskolás',
'Egyéb'
);

$ws=array(
'Mindegy',
'Nem dohányzó',
'Alkalmanként',
'Rendszeresen'
);

$wd=array(
'Mindegy',
'Sohasem iszik',
'Alkalmanként',
'Rendszeresen'
);

$wed=array(
'Mindegy',
'Középiskola',
'Középiskolai hallgató',
'Egyetemi hallgató',
'Felsõfokú végzettség',
'Egyéb',
);

$wu=array(
'Mindegy',
'Internetes hirdetés',
'Újság',
'Televizió',
'Netes keresõ',
'Ismerõs ajánlotta',
'Egyéb'
);

$whr=array(
'Mindegy',
'Bak',
'Vízöntõ',
'Halak',
'Kos',
'Bika',
'Ikrek',
'Rák',
'Oroszlán',
'Szûz',
'Mérleg', // [10]
'Skorpió',
'Nyilas'
);

$wsb=array(
'Dátum szerint csökkenõ',
'Dátum szerint növekvõ',
'Névsor szerint csökkenõ',
'Névsor szerint növekvõ',
'Kor szerint csökkenõ',
'Kor szerint növekvõ',
'Magasság szerint csökkenõ',
'Magasság szerint növekvõ',
'Súly szerint csökkenõ',
'Súly szerint növekvõ'
);
/* 
$wsb only for search - you can`t add new or remove - 
only translate!
*/

$wrg=array(
'Mindegy mikor',
'Az utóbbi 7 napban',
'Az elmúlt hónapban',
'Az elmúlt 3 hónapban',
'Ebben az évben'
);

$wrgv=array(
'', // Not required - enter this value for All periods
'7', 
'31',
'92',
'365'
);


$wst=array(
'Összes felhasználó',
'Mail ellenõrzésre váró',
'Regisztráció ellenõrzésére váró',
'Frissités ellenõrzésére váró',
'Eltávolítás jóváhagyására váró',
'Kizárt felhasználó',
'Aktív felhasználó'
);

$wos=array(
'Nincs kuponkódja',
'Egyetlen napig sem volt fent'
);
 
# Languages - maximal 63 values!!! 
$wlng=array(
'', // Empty Value required
'English',
'German',
'French',
'Spanish',
'Arabic',
'Italian',
'Chinese',
'Bengali',
'Hindi/Urdu',
'Portuguese',
'Russian',
'Japanese',
'Dutch',
'Greek',
'Korean'
);

$wcr=array(
    '  Válassz!', // First space is required for sorting
    ' Budapest', //1
    ' Baranya', //2
    ' Bács-Kiskun', //3
    ' Békés', //4
    ' Borsod', //5
    ' Csongrád', //6
    ' Fejér', //7
    ' Gyõr-Moson-Sopron', //8
    ' Hajdú - Bihar', //9
    ' Heves', //10
    ' Jász-Nagykun-Szolnok', //11
    ' Komárom-Esztergom', //12
    ' Nógrád', //13
    ' Pest', //14
    ' Somogy', //15
    ' Szabolcs-Szatmár-Bereg', //16
    ' Tolna', //17
    ' Vas', //18
    ' Veszprém', //19
    ' Zala', //20
    'Szlovákia', //21
    'Dánia', //22
    'Szlovénia', //23
    'Csehország', //24
    'Bosznia-Hercegovina', //25
    'Szerbia', //26
    'Egyesült Királyság', //27
    'Ukrajna', //28
    'Lengyelország', //29
    'Horvátország', //30
    'Írország', //31
    'Hollandia', //32
    'Belgium', //33
    'Luxembourg', //34
    'Franciaország', //35
    'Svájc', //36
    'Olaszország', //37
    'Spanyolország', //38
    'Portugália', //39
    'Románia', //40
    'Németország', //41
    'Ausztria', //42
    'Svédország', //43
    'Norvégia', //44
    'Finnország', //45
    'Észtország', //46
    'Lettország', //47
    'Litvánia', //48
    'Oroszország', //49
    'Fehéroroszország', //50
    'Moldova', //51
    'Bulgária', //52
    'Macedónia', //53
    'Koszovó', //54
    'Montenegro', //55
    'Albánia', //56
    'Görögország', //57
    'Málta', //58
    'Törökország', //59
    'Ciprus', //60
    'Egyesült Államok', //61
    'Kanada', //62
    'Mexikó', //63
);
// No more 255 entries in this list!!!
// You can change this list to cities of your country
// Be careful!

$wor=array(
'Mindegy',
'Meleg',
'Biszex',
'Hetero',
'Transznemű',
'Biszex/CD',
'CD kedvelő');

$wbt=array(
'Mindegy',
'Szálkás',
'Sportos',
'Vékony',
'Zömök',
'Átlagos',
'Mackó',
'Óriás');

$win=array(
'Mindegy',
'Internet',
'Filmezés',
'Buli',
'Kirándulás',
'Olvasás',
'Politika',
'Egyéb');
// Forum sort by
$wfs=array('Last post date','Author name','Number of answers','Thread name');

// Access for private photos
$wpp=array('All users','Only users in "My Favorites"','All users by password','Only users in "My Favorites" by password');

//Random quotes
$rqu=array(
    'A külső csak látszat, a titulus csak szóvirág, az ember számít, semmi más.',
    'Ha váratlan öröm ér, ne ígérj senkinek semmit; hirtelen haragodban pedig ne válaszolj senki levelére.',
    'Levelet küldeni azért jó, mert úgy juthatunk el valahová, hogy csak a szívünk kel útra.',
    'Nem kell mindent kimondani. Nem kell mindent megkérdezni. Nem kell mindenre válaszolni.',
    'Vannak kérdések, amelyeket nem teszünk fel, mert félünk a választól.',
    'Hibázni fájdalmas, de csak így tudhatjuk meg, kik vagyunk valójában.',
    'Újra és újra neked fognak menni, fel fognak lökni, neked pedig talpra kell állnod! Bizonyítanod kell, hogy talpra tudsz állni!',
    'Egy valódi férfi nem fél veszíteni, mert tudja, hogy a vereség a győzelem kapuja.',
    'Ne szaladj olyan szekér után, amelyik nem vesz fel.',
    'Ne azon nyafogj, hogy ki nem vagy, inkább vállald fel önmagad.'
);
?>