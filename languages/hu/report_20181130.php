<div class="container-fluid my-1 text-center">
    <h1><font color="#ffff00"><?=C_SNAME?> tájékoztató 2018. 11. 30.</font></h1>
</div>

<div class="container-fluid my-1">
  <div class="row">
    <div class="col-12 col-lg-8 offset-lg-2">
<p><b>Kedves Hotbull randisok, </b></p>
<p>2018. november 29-én adatvesztés történt a Hotbull Randi rendszerében.</p>

<p>Hogy további adatvesztés ne történjen, este 9-kor a következőket tettük:<br/>
1. az oldalt lezártuk, <br/>
2. az üzeneteket tartalmazó adatbázist lementettük, <br/>
3. a tárhely szolgáltató által visszaállításra került a 2018. november 29. délután 5 órai biztonsági mentés<br/>
4. visszatöltöttük a délután 5 óta küldött üzeneteiteket, kivéve azokat, amiket az új felhasználóknak (akik 5 óra után regisztráltak) küldtetek. </p>

<p>Amit ti a honlap újra indítása után tapasztalhattok. <br/>
- Akik 2018 november 29-én délután 5 után regisztrált, annak újra kell regisztrálni. <br/>
- Aki 2018 november 29-én délután 5 után módosított az adatlapján, annak módosításai elvesztek, és az azelőtti állapotot fogja látni. <br/>
- Ha új jelszó lett kérve, ezt a kérést meg kell ismételni. <br/>
- Bár a délután 5 és este 9 után küldött üzenetek visszaállításra kerülnek, nem garantáljuk, hogy minden üzenet vissza is fog kerülni. Ennek oka az adatbázis akkori állapota. <br/>
- Minden ez idő alatti látogatás, kedvelés, szavazás adatai elvesztek.</p>

<p>A tárhely szolgáltató szerint a hibát nem a honlap okozta, hanem az adatbázis hibás futása és a szerverre feltelepített frissítések, amiről nem kaptunk figyelmeztetést. </p>

<p>Valamint megállapítottuk, hogy az adatok nem kerültek illetéktelenek kezébe, és a hiba nem illetéktelen személyek külső beavatkozása miatt történt. </p>

<p>Mivel az utóbbi időben már második alkalommal fordult elő, hogy adatvesztés miatt vissza kellett állítani a biztonsági mentést, ezért a programozónk által kidolgozásra fog kerülni egy olyan program, ami naplózni fog minden adatmódosítást, ami módosítja az adatbázist. Ennek köszönhető lesz majd, hogy senki adatai ne vesszen el, ha ismét megtörténik. Emiatt a szabályzatot is módosítanunk kell, amiről természetesen értesítést fogunk küldeni minden tagunknak. </p>

<p>Mint mindig, elkötelezettek vagyunk, hogy minden lehetséges eszközt bevessünk azért a célért, hogy ti felhasználók biztonságosan ismerkedjetek.</p>

<p>Tisztelettel:<br/>
<b>A Hotbull Randi üzemeltetői és munkatársai</b></p>
    </div>
  </div>
</div>