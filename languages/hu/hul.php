<?php
$wl=array(
    'registration_success'=> array(
        'subject' => 'Sikeres regisztráció a {1}-on',
        'text' => '
Szia kedves felhasználó!
Adatlapodat sikeresen hozzáadtuk a {1} rendszerhez.

Ne felejtsd el:
Loginneved:{0}
',
        'html' => '
<p>Szia kedves felhasználó!</p>
<p>Adatlapodat sikeresen hozzáadtuk a {1} rendszerhez.</p>

<p>Ne felejtsd el:<br/>
Loginneved:{0}</p>
',
    ),
    'registration_problem'=> array(
        'subject' => 'Köszönjük, hogy regisztráltál a {1}-on',
        'text' => '
Szia kedves felhasználó!
Adatlapodat sikeresen regisztráltad a {1}-on, de ütközési problémát tapasztaltunk.
Valószínű, hogy már korábban regisztráltál a {1}-on, de az is lehet, hogy ezen a gépen valaki már regisztrált vagy pont ugyanazt a jelszót használja, mint amit megadtál.
24 órán - vagy azon belül az Admin megnézi, és összehasonlítja a profilokat, hogy eldöntse, nem-e duplikációról van-e szó. Addig kérjük türelmedet.
',
        'html' => '
<p>Szia kedves felhasználó!</p>
<p>Adatlapodat sikeresen regisztráltad a {1}-on, de ütközési problémát tapasztaltunk.</p>
<p>Valószínű, hogy már korábban regisztráltál a {1}-on, de az is lehet, hogy ezen a gépen valaki már regisztrált vagy pont ugyanazt a jelszót használja, mint amit megadtál.</p>
<p>24 órán - vagy azon belül az Admin megnézi, és összehasonlítja a profilokat, hogy eldöntse, nem-e duplikációról van-e szó. Addig kérjük türelmedet.</p>
',
    ),
    'registration_activation'=> array(
        'subject' => 'Regisztrációd megerõsítése',
        'text' => '
Köszönjük hogy regisztráltál hozzánk, a Randi HotBull.hu-ra.
Kérlek, hogy kattints az alábbi linkre vagy másold a böngészõdbe a következő szöveget a regisztrációd megerõsítéséhez:
{0}{1}

Amennyiben nem sikerül az aktiváció, keresd fel a Hotbull Randi oldalát, és belépési résznél keresd meg a Segítség oldalt. 
Itt meg tudod adni a lenti Aktivációs kódot:
{1}
',
        'html' => '
<p>Köszönjük hogy regisztráltál hozzánk, a Randi HotBull.hu-ra.</p>
<p>Kérlek, hogy kattints az alábbi gombra a regisztrációd megerõsítéséhez:</p>
<p><a href="{0}{1}" style="background-color: blue; padding: 10px; color: #FFFFFF">Aktiválás</a></p>
{0}{1}

<p>Amennyiben nem sikerül az aktiváció, keresd fel a Hotbull Randi oldalát, és belépési résznél keresd meg a Segítség oldalt. 
Itt meg tudod adni a lenti Aktivációs kódot:</p>
<p><b>{1}</b></p>
',
    ),
    'admin_notify_new_member'=> array(
        'subject' => 'Új felhasználó regisztrált',
        'text' => '
Új felhasználó regisztrált a {0}-n.

A profilját itt nézheted meg:
{1}
',
        'html' => '
<p>Új felhasználó regisztrált a {0}-n.</p>

<p>A profilját itt nézheted meg:<br/>{1}</p>
',
    ),
    'newpass_activation'=> array(
        'subject' => 'Új jelszó kérése',
        'text' => '
Új jelszót kértél a Hotbull Randi oldalán.
Kérlek, hogy kattints az alábbi linkre vagy másold a böngészõdbe a következő szöveget a jelszó megváltoztatásához:
{0}{1}

Amennyiben nem sikerül új jelszót kérned, keresd fel a Hotbull Randi oldalát, és belépési résznél keresd meg a Segítség oldalt. 
Itt meg tudod adni a lenti Aktivációs kódot:
{1}

Amennyiben nem te kértél új jelszót, nem kell tenned semmit sem. Valószínűleg valaki elrontotta az email címét.

Felhasználói adataid:
Felhasználói név: {2}
Felhasználói email címe: {3}
',
        'html' => '
<p>Új jelszót kértél a Hotbull Randi oldalán.</p>
<p>Kérlek, hogy kattints az alábbi gombra a jelszó megváltoztatásához:</p>
<p><a href="{0}{1}" style="background-color: blue; padding: 10px; color: #FFFFFF">Új jelszó kérése</a></p>
{0}{1}

<p>Amennyiben nem sikerül új jelszót kérned, keresd fel a Hotbull Randi oldalát, és belépési résznél keresd meg a Segítség oldalt. 
Itt meg tudod adni a lenti Aktivációs kódot:</p>
<p><b>{1}</b></p>
<p>Amennyiben nem te kértél új jelszót, nem kell tenned semmit sem. Valószínűleg valaki elrontotta az email címét.</p>

<p>Felhasználói adataid:<br/>
Felhasználói név: {2}<br/>
Felhasználói email címe: {3}</p>
',
    ),
    'forgotten_username'=> array(
        'subject' => 'Felhasználónév küldése',
        'text' => '
A Hotbull Randi oldalán jelezted, hogy elfelejtetted a felhasználónevedet.

Amennyiben nem te kértél erről információt, nem kell tenned semmit sem. Valószínűleg valaki elrontotta az email címét.

Felhasználói adataid:
Felhasználói név: {0}
Felhasználói email címe: {1}
',
        'html' => '
<p>A Hotbull Randi oldalán jelezted, hogy elfelejtetted a felhasználónevedet.</p>

<p>Amennyiben nem te kértél erről információt, nem kell tenned semmit sem. Valószínűleg valaki elrontotta az email címét.</p>

<p>Felhasználói adataid:<br/>
Felhasználói név: {0}<br/>
Felhasználói email címe: {1}</p>
',
    ),
    'message_to_admin'=> array(
        'subject' => '',
        'text' => '{0}
-------------------------------------------------
Ezt az üzenetet a Hotbull Randi kapcsolati űrlapjáról küldték. Főbb adatok:
Felhasználói név: {1}
Felhasználói email címe: {2}
Bejelentkezés: {3}
',
        'html' => '{0}
<hr>
<p>Ezt az üzenetet a Hotbull Randi kapcsolati űrlapjáról küldték. Főbb adatok:</p>
<p>
Felhasználói név: {1}<br>
Felhasználói email címe: {2}<br>
Bejelentkezés: {3}
</p>
',
    ),
    'shield_activation'=> array(
        'subject' => 'Ismeretlen helyről léptél be',
        'text' => '
Ismeretlen helyről léptél be, ezért ideiglenesen kitiltottunk.
Ahhoz, hogy visszaengedjünk, erre a linkre kell kattintanod:
{0}{1}

Amennyiben nem sikerül, keresd fel a Hotbull Randi oldalát, és belépési résznél keresd meg a Segítség oldalt. 
Itt meg tudod adni a lenti Aktivációs kódot:
{1}

Ha nem te voltál az, aki belépni próbált, akkor belépés után azonnal változtasd meg a jelszavadat!
',
        'html' => '
<p>Ismeretlen helyről léptél be, ezért ideiglenesen kitiltottunk.</p>
<p>Ahhoz, hogy visszaengedjünk, erre a linkre kell kattintanod:</p>
<p><a href="{0}{1}" style="background-color: blue; padding: 10px; color: #FFFFFF">Feloldás</a></p>
{0}{1}

<p>Amennyiben nem sikerül, keresd fel a Hotbull Randi oldalát, és belépési résznél keresd meg a Segítség oldalt. 
Itt meg tudod adni a lenti Aktivációs kódot:</p>
<p><b>{1}</b></p>
<p>Ha nem te voltál az, aki belépni próbált, akkor belépés után azonnal változtasd meg a jelszavadat!</p>
',
    ),
'shield_protection'=> array(
        'subject' => 'Ismeretlen helyről léptél be',
        'text' => '
Ismeretlen helyről léptél be

Ha nem te voltál az, aki belépni próbált, akkor belépés után azonnal változtasd meg a jelszavadat!
',
        'html' => '
<p>Ismeretlen helyről léptél be.</p>
<p>Ha nem te voltál az, aki belépni próbált, akkor belépés után azonnal változtasd meg a jelszavadat!</p>
',
    ),

);
?>