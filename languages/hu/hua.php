<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.8.0                    #
# Wrote by                AzDG (support@azdg.com)          #
# Created 03/01/03        Last Modified 05/02/03           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               defaulta.php                     #
# File purpose            Additional langfile for admin    #
# File created by         AzDG <support@azdg.com>          #
############################################################
### !!!!! Please read: RULES for translating!!!!! ###
### 1. Be careful in translating - don`t use ' or { } characters!
###    You can use their html-equivalent - &#39; &#123; &#125;
### 2. Don`t translate {some_number} templates - you can only replace them 
###    {0} - is not a number - it is a template!!
###################################
$x=array(
'<font color=red size=3>*</font>', //0 - Symbol for required field
'Rossz név vagy jelszó - próbáld újra',//1
'Adminisztrációs rész',//2
'Admin név',//3
'Admin jelszó',//4
'Belépés',//5
'Hozzáférés megtagadva',//6
'Kezdõlap',//7
'Számlálók nullázása',//8
'Javítás és optimalizálás',//9
'Felhasználók kezelése',//10
'FAQ kezelése',//11
'Levelezõlista',//12
'Kilépés',//13
'Sikeresen kiléptél az admin részbõl',//14
'Összes mail',//15
'mail küldés',//16
'A következõ rész',//17
'{0} elküldve a felhasználónak',//18
'Olvasatlan üzeneteid vannak {0}',//19 {0} - SiteName (C_SNAME)
'Találva {0} felhasználó',//20
'levelezõlista kezelõ',//21
'Státusszal:',//22
'Mail opciók és mezõk:',//23
'Mail típus',//24
'From Mail',//25
'Mail tárgy',//26
'Mail testszöveg',//27
'megerõsítés az eredményekrõl',//28
'Vissza',//29
'Mail küldés',//30
'The unfinished dispatches - {0}',//31
'Tárgy : {0} [Sent to {1} users] :::>',//32
'Finish this dispatch',//33
'Szöveg',//34
'HTML',//35
'Elõnézet',//36
'Felhasználók olvasatlan üzenettel - ',//37
'Összes olvasatlan üzenet - ',//38
'Utolsó üzenet id - ',//39
'Megerõsítõ üzenet küldése azoknak akiknek van olvasatlan üzenetük',//40
'Profil sikeresen frissitve',//41
'FAQ kérdés sikeresen megcsinálva,frissitve',//42
'Új készítése',//43
'Kérem irjon kérdést',//44
'Kérem irjon választ',//45
'FAQ menedzsment',//46
'Új FAQ kategória létrehozása',//47
'FAQ kategória szerkesztése',//48
'FAQ kategória törlése',//49
'FAQ létrehozása',//50
'Kategória választása',//51
'FAQ kérdések',//52
'FAQ válaszok',//53
'Mindenekelõtt készíts kategóriákat',//54
'Ird be a kategória nevét',//55
'FAQ kategória készítés',//56
'Új kategória készítve:',//57
'Kategória neve',//58
'FAQ kategória szerkesztése',//59
'Szerkesztés',//60
'Kategória megváltoztatva',//61
'FAQ kategória szerkesztése',//62
'A választott lehetõségek törölve',//63
'FAQ kategória törlése',//64
'Csak okosan, a kategória törlésével az összes benne levõ dolog ugrik.',//65
'Törlés',//66
'0 - Várj email ellenõrzés<br>1 - ellenõrzésre vár regisztráció után<br>2 - ellenõrzésre vár frissités után<br>3,8 - eltávolitásra vár(without,with show)<br>7 - Felhasználó aktív<br>',//67
'Adatbázis javítva és optimalizálva',//68
'ez a felhasználó ID#={0} sikeresen eltávolítva',//69
'Számlálók nullázva',//70
'sikeresen engedélyezve a következõ felhasználó id={0}',//71
'IP',//72
'Követelmények [0 - 255]',//73
'Status - (BE CAREFUL)',//74
'Jóváhagy',//75
'Felhasználó management',//76
'Összes aktív felhasználó :',//77
'Regisztráció utáni aktiválásra várnak',//78
'aktiválás és frissités után várnak',//79
'eltávolításra várnak',//80

'Kiváltságos felhasználók',//81
'Kiváltságos felhasználók Management',//82
'Ajánlási statisztika',//83
'Küldõ Email',//84
'Küldõ IP',//85
'Címzett Email',//86
'Ajánló statisztika nullázása',//87
'Ajánló statisztika nullázva',//88
'Felhasználó statisztika nullázása',//89
'Státusz',//90
'Felhasználó típusa',//91
'Figyelmesen olvasd el a pénzzel kapcsolatos dolgokat',//92
'Csatlakozási statisztika ennél a felhasználónál ID#{0} sikeresen eltávolítva az adatbázisból.',//93
'Befejezetlen átirányítások törölve',//94
'Szia {0} {1}

Neked {2} olvasatlan üzeneted van {3} rendszerünkben.

Itt tudod elolvasni:
{4}

Emlékeztetõül a login-neved:
{5}
-------------------
{3} 
',//95 - {0} - Firstname, {1} - LastName, {2} - messages number, {3} - site name, {4} - login.php full url, {5} - user login 
'FAQ kérdés szerkesztése',//96
'FAQ kérdés törlése',//97
'kérdés kiválasztása',//98
'Népszerûség [max: 65535]',//99
'Hírek management',//100
'Hírek tárgya',//101
'Hírek szöveg',//102
'a hírt és a szöveget html formában kell beírni',//103
'Hír hozzáadása',//104
'Hír szerkesztése',//105
'Hír törlése',//106
'Hír prioritása [0-255]<br><i>válassz 1-255 az uj hírnél</i>',//107
'Hír tárgya,szövege üres vagy a prioritás nem numerikusan van megadva',//108
'Ezzel a szöveggel már van hír a rendszerben. Használd a "hír szerkesztése" funkciót a módositáshoz.',//109
'Hír inaktiv módban hozzáadva.<br><br> Klikkeléssel ellenõrizd [Hír elõnézete]<br> vagy tedd aktivvá [Aktívvá tesz].<br><br>Ha szerkeszteni akarod kilekkelj erre [Hír szerkesztése].<br>NE HASZNÁLD A BÖNGÉSZÕ VISSZA GOMBJÁT',//110
'Hír elõnézete',//111
'Aktívvá tesz',//112
'A kiválasztott hír aktív lett',//113
'Aktív',//114
'Inaktív',//115
'Ez a hír',//116
'A hír inaktiv vagy miylen',//117
'Inaktívvá tesz',//118
'Hír sikeresen megváltoztatva',//119
'Opciók',//120
'Konfiguráció',//121
'Template Neve',//122
'Weblap neve',//123
'Engedélyezés típusa',//124
'Szakaszok',//125
'Cookies',//126
'Admin Email',//127
'Email a válasz nélüli üzenetekhez',//128
'Mélküldés lépés adminoknak',//129
'utoljára regisztrált felhasználók száma',//130
'legnépszerûbb felhasználók száma',//131
'Keresési találatok az admin részben',//132
'Keresés oldalak száma [1 . . . 4 5 6 7 8 . . . 22]',//133
'Hogyan adj adatlapot az adatbázishoz',//134
'Küldjön gratula mélt ha sikeres volt a regisztráció?',//135
'Igen',//136
'Nem',//137
'Hány napig várjon a mail ellenõrzésre',//138
'hogyan frissithetõ profil?',//139
'hogy távolitható el felhasználóprofil',//140
'hogyan jelenitsen meg nyelveket?',//141
'alapértelmezett nyelv a multilang',//142
'alapértelmezett nyelv',//143
'böngészõ nyelve',//144
'Engedélyezed a népszerûségit?',//145
'Hány megtekintés után adja hozzá, amikor megnézik mások az adatlapját',//146
'Hányat adjon hozzá, ha mailt küld?',//147
'Hányat adjnon hozzá, ha kap mailt?',//148
'Hányat adjon hozzá, ha üzenetet küld?',//149
'Hányat adjon hozzá, ha üzenetet kap?',//150
'Hányat adjon hozzá, ha hozzáad a hálószobához?',//151
'Népszerûségi periódus - 1 IP címrõl egyszerre 1 EBBEN az órában',//152
'Hang',//153
'engedélyezed hang feltöltését??',//154
'Max.mérete a feltöltött fájloknak',//155
'Autostart audio lejátszás?',//156
'Loop hányszor?',//157
'Audio lejátszó típus',//158
'Képeslapok',//159
'Képeslapok száma egy oszlopban',//160
'Képeslap értesitõ küldése... ',//161
'Hány napig õrizze a képeslapot',//162
'Üzenetdoboz',//163
'Email',//164
'Csatlakozás',//165
'Csatlakozás engedélyezése?',//166
'Affiliate cookies élettartama (napokban)',//167
'Affiliate cookies neve',//168
'Maximal pénzküldés in $',//169
'Banner csere',//170
'Engedélyezed a banner csere?',//171
'Vegyes és egyéb Opciók',//172
'Various mezõ hossza',//173
'Elõnév legkisebb és legnagyobb mérete',//174
'Utónév legkisebb és legnagyobb mérete',//175
'Jelszó legkisebb és legnagyobb mérete',//176
'Minimal és maximalis kor',//177
'Hobbik legnagyobb mérete',//178
'Hobbik legnagyobb hossza',//179
'Meghatározás leghosszabb szómérete',//180
'Meghatározás max hossza',//181
'Város karakterszáma max',//182
'Kötelezõ mezõk a regisztrációnál',//183
' [ajánlott]',//184
'Mutassa az egyezményt',//185
'Jóváhagyja a nem cseréjét',//186
'Jóváhagyja a mailcím cseréjét',//187
'Minimális hozzáférés az adatlap mezõkhöz',//188
'Linkek',//189
'Mutassa az üzenetküldés linket?',//190
'Mutassa a képeslapküldés linket?',//191
'Mutassa a mail a felhasználónak linket?',//192
'Mutassa a hálószobához ad linket?',//193
'Mutassa a rossz adatlapokhoz ad linket?',//194
'Mutassa a tiltólistához ad linket?',//195
'Kezdõlap',//196
'Mutassa a "Ajánlj minket" scriptet?',//197
'Ismerõsök emailének száma "Ajánlj minket" scriptben',//198
'Születési dátum alapján keresés a gyorskeresõben? ',//199
'Csak az utoljára fényképpel regisztráltakat mutassa',//200
'Mutassa a legnépszerûbb felhasználókat a fõoldalon?',//201
'Engedélyezed a híreket?',//202
'Hány hírt mutasson?',//203
'Engedélyezed a top férfiakat?',//204
'Engedélyezed a top nõket?',//205
'Legnépszerûbb fotók száma nemenként',//206
'Keresési találatokban a fényképeseket mutassa elõbb',//207
'Maximális fényképméret Kb-ban',//208
'Fotók méretének ellenõrzése (Csak PHP safe_mode=off engedélyezve)',//209
'Fénykép maximális szélessége',//210
'Fénykép maximális magassága',//211
'Biztonságos mód',//212
'Felhasználok és vendégek jogai',//213
'Minimal hozzáférés az észrevételekhez',//214
'Can registered user specify another email for reply in send feedback?',//215
'Minimalis hozzáférési szint a részletes kereséshez',//216
'Minimalis hozzáférési szint a kereséshez',//217
'Csak a tagoknak mutassa az online felhasználókat?',//218
'Rossz adatlap értesités csak tagoknak?',//219
'Csak tagok nézhetik meg a felhasználókat?',//220
'Csak tagok küldhetnek mailt a felhasználóknak?',//221
'Can registered user specify another email for reply in send mail?',//222
'FAQ csak a tagoknak?',//223
'Mutassa a mai szülinaposokat?',//224
'Ajánlás csak tagoknak?',//225
'Mutassa az "Új üzeneted érkezett" üzenetet a tagoknak',//226
'Megerõsítés ha valaki hozzáad a "Kedvencek"',//227
'Whats show for user as Username in members area?',//228
'Ne legyen név "Üzenetek megtekintése"menüben, csak ID num?',//229
'Csatolja az eredeti üzenet tárgyát a válasz tárgyához',//230
'Csatolja az eredeti üzenetet a válaszhoz',//231
'Mutassa a felhasználók képét a keresés találatainál',//232
'Fizetõ rendszer',//233
'Fizetõ Mode',//234
'Which gender must to be a privilegied user?',//235
'Minden felhasználó',//236
'Mutassa a fizetõs linket a kiváltságosoknak?',//237
'PayPal Opciók',//238
'Engedélyezed PayPal?',//239
'PayPal üzleti email',//240
'Ellenõrzõ opciók',//241
'Engeded az ellenõrzést?',//242
'te ellenõrzöd a login ID',//243
'Engedlyezve Demo mód',//244
'Titkos kulcs',//245
'Login cookies elõtag',//246
'Chat Opciók',//247
'Chat engedélyezése',//248
'Csak regisztrált felhasználók belépése engedélyezett',//249
'Statisztika opciók',//250
'Opciók változtatása',//251
'Hány üzenetet mutasson itt "Üzenetek megtekintése"',//252
'Opciók sikeresen mósosítva',//253
'Whats to show as username felhasználónak a keresés találataiban?',//254
'You can use any or all of this templates:<br>ID - User id<br>USERNAME - Username (Only if login by username)<br>FNAME - User Firstname<br>LNAME - User Lastname<br><br>For example:<br>FNAME LNAME #[ID]',//255
'Moderator`s opciók',//256
'Moderator`s jogok',//257
'Note:Moderátor eltávolitáshoz töröld ki a nevét és jelszavát',//258
'Belépés Moderátoroknak',//259
'Jelszó Moderátornak',//260
'Ha akarsz tudsz hozzáadni Moderátort',//261
'Felhasználó jóváhagyás',//262
'Felhasználó szerkesztés',//263
'Felhasználó eltávolítás',//264
'Megnézés "Ki van Online?"',//265
'Megnézés "Rossz adatlap"',//266
'Számláló nullázás',//267
'Adatbázis javítás',//268
'Keresés a "Felhasználó Management"',//269
'Használni "Levelezõlista"',//270
'Hozzáad,szerkeszt,eltávolit kiváltságos felhasználót',//271
'Munka "Affiliates" menüvel',//272
'Hozzáadás szerkesztés törlés a FAQnál',//273
'Hozzáadás szerkesztés,törlés a híreknél',//274
'Megnéz "Ajánlj minket" statisztikát',//275
'Megnéz "Statisztika"',//276
'Nincs jogod ehhez a mûvelethez',//277
'Show AIM only in Privilegied User`s "View Profile"',//278
'Show ICQ only in Privilegied User`s "View Profile"',//279
'Show Webpage only in Privilegied User`s "View Profile"',//280
'Show Phone only in Privilegied User`s "View Profile"',//281
'Moderators Area',//282
'Moderator Belépés',//283
'Moderator Jelszó',//284
'Hozzáférés megtagadva ha nem vagy moderátor',//285
'Sikeresen kiléptél a moderátor részbõl<br><br>Kösz hogy használtad a rendszert',//286
'Üdvözlõ cím',//287
'Üdvözlõ üzenet',//288
'Fórum Management',//289
'Új fórum készítése',//290
'Elõször csinálj fórum kategóriákat',//291
'Új Fórum kategória készítése',//292
'Fórum kategória szerkesztése',//293
'Fórum kategória törlése',//294
'Fórum címe',//295
'Fórum meghatározása',//296
'Irj be fórumcímet',//297
'Irj be fórum meghatározást',//298
'Válassz kategóriát',//299
'Minimum hozzáférés',//300
'Moderators ID<br>(Enter here users ID who can moderation rights to this forum separated by commas, for example: 212,311,581)',//301
'Töltsd ki a cím, meghatározás és moderators mezõt rendesen!',//302
'Új fórum elkészült.',//303
'Rossz szó ellenõrzés',//304
'engedélyezed Karbantartás Mode?',//305
'Karbantartás üzenet',//306
'Rövid dátumformátum:<br>
{MONTH} - 3 first char of month<br>
{FULL_MONTH} - full name of month<br>
{NUMERIC_MONTH} - numeric value of month<br>
{DAY} - day<br>
{YEAR} - year',//307
'Hosszú dátumformátum:<br>
{MONTH} - 3 first char of month<br>
{FULL_MONTH} - full name of month<br>
{NUMERIC_MONTH} - numeric value of month<br>
{DAY} - day<br>
{YEAR} - year<br>
{TIME} - time',//308
'Idõeltérés a szervernél órékban',//309
'Minimal hozzáférés a felhasználótípus megnézéséhez (Regisztrált felhasználó, Silver member, etc...)',//310
'Minimal hozzáférés megnéz Profile Views number',//311
'Minimal hozzáférés megnéz felhasználók online status',//312
'legyen automatikus belépés sikeres regisztráció után',//313
'Engedélyezed fórumot?',//314
'Minimal hozzáférés fórum használathoz',//315
'If you using login by ID or email (not username), in chat must be using some unique username. The best way is to use email login + ID (for example user with ID=211 and email=myname@hotmail.com must have chat username: myname211). Here you can specify first N chars from email login using for chat username and for protection from very long usernames.',//316
'Minimal hozzáférés a népszerûség mutatásához',//317
'Minimal hozzáférés hanglejátszáshoz',//318
'Minimal hozzáférés a gyors kereséshez',//319
'Csak kiváltságos felhasználóknak',//320
'Csak haladó felhasználóknak:itt állíthatod meddig legyenek kiváltságosak (in YYYY-MM-DD format).<br> Ha üresen hagyod automatikus lesz.',//321
'Fórum szerkesztés',//322
'Forum törlés',//323
'Forum megváltoztatva',//324
'Fórum hozzáadás,szerkesztés,törlés',//325
'Linkek Management',//326
'Új kategória készítése',//327
'Kategória szerkesztése',//328
'Kategória törlése',//329
'HTML formában ird be a webcímet és a nevet. Például:<br>&lt;a href=&quot;http://www.hotbull.hu/&quot;&gt;HotBull.hu Website&lt;/a&gt;',//330
'HTML formában ird a weblap leirását.',//331
'Link szerkesztése',//332
'Link törlése',//333
'Link készítése',//334
'Elõször csinálj kategóriákat és frissitsd az oldalt',//335
'Prioritás',//336
'Mûvelet sikeresen végrehajtva!',//337
'Csak admin hagyhatja jóvá',//338
'Hozzáférés és biztonság',//339
'Section`s Opciók',//340
'Online felhazsnáló számláló ideje ( in seconds )',//341
'Kép opciók',//342
'Állítsd Hits to 1, amikor az admin nulláz.',//343
'Minimum hozzáférés a linkekhez',//344
'IP Hozzáférés Management',//345
'Tiltott IPs vesszõkkel elválasztva, például:<br>10.23.49.21,10.41.1.*,212.48.*.*,208.64.5.17',//346
'Kiváltságos IPk. <br>Users with this IPs will get privilegied rights in login.',//347
'What access will get users with privilegied IPs?',//348
'Adatbázis Management',//349
'Üzenetek control',//350
'Kiváltságok hossza (in month)',//351
'Fizetés tagtipusoknak dollárban. Valami iylesmi ex. 1.50',//352
'Fizetés csatlakozóknak. Valami iylesmi ex: 1.50',//353
'Hány üzenetet küldhetnek naponta (last 24 hours) can send each user type',//354
'Messages number which can send new member for free by gender',//355
'Engedély üezenet control',//356
'Minimum jogok az adatlap feliratkozáshoz',//357
'Mail a felhasználóknak akiknek olvasatlan üzeneteik vannak',//358
'Mail a feliratkozott felhasználóknak',//359
'Összes feliratkozott',//360
'Utolsó átirányitás avgy mi',//361
'Új adatlap az utolsó diszpaccs óta',//362
'Köszöntlek kedves felhasználó!
<br><br>
Feliratkoztál az ujdonságok listára. <br>
Ha le szeretnél iratkozni a lap aljánál megteheted.',//363
'Ha nem akarsz több levelet kapni ezügyben,iratkozz le a listáról ezen a linken:
<br><br>
{0}
<br><br>
Klikkelj az alábbi linkre vagy másold a böngészõbe.',//364
'Új felhazsnáló {0} jelentkezett be {1} a te feltételeid voltak:',//365 {0} - Site Name, {1} - Date
'Mail küldése...', //366
'sikeresen elvégezve!<br>
elküldött {0} email.', //367
'Köszöntünk, mikor jössz vissza az oldalra?', //368
'Emil küldése akinek elsõ olvasatlan üzenete van', //369
'Who can see "Töb találat" link a népszerû tagoknak', //370
'Fénykép méret ellenõrzés', //371
'Thumbnail opciók', //372
'Thumbnail`s maximal szélesség', //373
'Thumbnail`s maximal magasság', //374
'Thumbnail`s auto-option (maximal szélesség vagy magasság)', //375
'Thumbnail minõség [0-100]', //376
'Belépés pénz currency', //377
'Használjon automata ellenõrzést mindentranzakcióhoz admin nélkül', //378
'Küldjön mélt az adminnak sikeres tranzakciókról', //379
'Mutasd "Nem látható az adatlapon" mezõ hozzáféréshez:', //380
'Ne rejtse a mezõt, just show that only respective user can see it (with link for purchasing)', //381
'Crypt URL a képhez? (ajánlott engedélyezni)<br>By disabling this option, you disable Thumbnail using.', //382
'Crypt Key', //383
'Width of photos in results of search', //384
'Photos in results of search must link to:', //385
'Photos in search results', //386
'Can Add, Edit and Remove Links', //387
'Can use "Mail to users with unread messages"', //388
'Can use "Mail to subscribed users"', //389
'Maintenance', //390
'Cleaning messages table', //391
'Cleaning users table', //392
'Remove messages older than (90 or higher - recommended)', //393
'days', //394
'Do not remove unread messages', //395
'{0} messages have been removed from database', //396
'Are you sure that you want to remove messages by your choice?', //397
'Is recommending to run "Repair&Optimize" now!', //398
'Show new profiles in the "Users awaiting profile activation after registration" menu. <br>Note: Actual for only "Automatically allow all profiles" and "After confirm from email"', //399
'Minumum access for search by "Type of user" in the advanced search', //400
'Indoklás', //401
'Chars available for username (regexp)<br>For ex: A-Za-z0-9_<br>Note: Do not recommend to change', //402
'Caching System', //403
'Cache Type', //404
'Cache Life (in seconds)', //405
'Links in the Statistics Page', //406
'What requests to cache?', //407
'Remove inactive users', //408
'Remove users inactive last NN days (300 or higher - recommended)<br>Users will`nt remove, their status will be changed and you see them under "Removed Users" status link.', //409
'Don`t remove privilegied users', //410
'Are you sure that you want to remove these users?', //411
'Status for {0} users has been changed to "Removed Users"', //412
'Notify admin by email, if someone signs up on the site', //413
'Check only new profiles with photos (works if one of the Admin Checking mode selected)', //414
'Watermark options', //415
'Use Watermark', //416
'Watermark type', //417
'Watermark text', //418
'Watermark image <br>Should locate in the templates/YOURTEMPLATE/images/ directory and be in png format', //419
'Watermark position', //420
'Watermark text color', //421
'Watermark text orientation ', //422
'Note: Watermark works on the fly, and eats server resources. Disable it for improve performance', //423
'You may use the following variables in the mail subject and mail body (case sensitive):<br>
<br>
{USERNAME} - username<br>
{FNAME} - firstname<br>
{LNAME} - lastname<br>
{PASSWORD} - password<br>
{EMAIL} - email<br>
{ID} - id<br>
{USERTYPE} - type of user (Free member or Silver member or etc...)<br>
{REGDATE} - Registration date<br>
{EDITDATE} - Last access date
', //424
'Unactive more than N days (specify the number of days)', //425
'Backup Database', //426
'Compressing type', //427
'No compress', //428
'Select tables to backup', //429
'Options Control', //430
'Who may access Options page', //431
'Allow to select templates', //432
'Who may rate users (For rate system)', //433
'<strong>Note: Please clear hits, after changing popularity type!!!</strong>', //434
'The number of digits that will be displayed following the decimal point (For rate system)', //435
'Popularity Type', //436
'Minimum vote number to make profile available for top rating (For rate system)', //437
'Szavazás adminisztrációja', //438
'Szavazás létrehozása', //439
'Szavazás szerkesztése', //440
'Szavazás törlése', //441
'Kérdés:', //442
'Hányféle válasz van:', //443
'Választási lehetőség:', //444
'Egy választ adhat', //445
'Több választ adhat', //446
'Szavazás típusa:', //447
'Nyomd le a <strong>[Ctrl]</strong>-t vagy a <strong>[Shift]</strong>-et a több elem kiválasztásához', //448
'Szavazás ID:', //449
'Csak a következő nyelvűeknek mutasd', //450
'Szavazás kérdése', //451
'Kérdés programkódja:', //452
'Válaszok programkódja:', //453
'Elérhető ezen a nyelven', //454
'Legyen alapértelmezett szavazás', //455
'Alapértelmezett', //456
'vagy', //457
'Ez a szavazás alapértelmezettre állítva', //458
'Kiválasztott nyelv a szavazáshoz törölve', //459
'Adj hozzá másik nyelvet', //460
'Másik nyelv hozzáadva', //461
'Szavazás szerkesztése ezen a nyelven', //462
'A szavazás nyelvesítése módosítva lett', //463
'Eredmény', //464
'Szavazás választásai módosítva lett', //465
'Kérlek erősítsd meg a szavazás törlését!!!', //466
'Szavazás törölve lett!', //467
'Add new user', //468
'If you want to change subscription options, you may do it from following link:
<br><br>
{0}', //469
'Subscription Management', //470
'Search subscribed users', //471
'Selected entries have been removed', //472
'Language is <strong>{0}</strong>.', //473
'MultiLanguage Interface', //474
'Edit Phrases', //475
'File {0} doesn`t exist', //476
'File {0} doesn`t writeble', //477
'Phrases: ', //478
'Selected phrases have been changed', //479
'How many photos may upload each type of user', //480
'Popularity votes number <br>(Actual for rate system mode and counted as Popularity/Votes number)', //481
'Administration fields', //482
'Admin directory', //483
'Moderator directory', //484
'Temporary directory', //485
'Show "Send a wink"', //486
'How many hits add to user getting "Send a wink"', //487
'Confirm user if receiver read "Send a wink" message', //488
'Minimum access for view big photos', //489
'Minimum access for register new profiles', //490
'Registration', //491
'Adja meg, hogy milyen típusú felhasználó láthatja a jelenlegi területen a felhasználói profilt', //492
'The number of birthdays to show by pages', //493
'Minimum level of access to see Users Plans', //494
'"Who seen my profile" minimum access level', //495
'"Who seen another users profile" minimum access level', //496
'"Who seen profile" - Show N results by page', //497
'Vote graph length in pixels', //498
'Time to reset IPs (in hour), allow voting 1 IP in N hours', //499
'View form if already voted', //500
'Show "View results" link in the form?', //501
'Show results after succesfull vote for this type of user', //502
'Show results after unsuccesfull vote for this type of user', //503
'Forums count on page', //504
'Posts count on page', //505
'Who can use matchmaker?', //506
'Registration type', //507
'Who can vote?', //508
'Statistics page access', //509
'Allow to change photo', //510
'Allow to change audio', //511
'Blacklist Manager', //512
'Blacklist type', //513
'Word', //514
'Enter one or several words (if you enter several words, separate them by commas). wildcard * is allowed for any number of chars, examples:<BR><BR>badmail@yahoo.com<BR>*bitch@hotmail.com<BR>*fuck*,bitch*,hack*<BR>*@blackdomain.com<BR>*baduser', //515
'Blackwords have been successfully added into database!', //516
'Blacklist', //517
'Who may prefer to Rate or No own profile', //518
'New profiles should be set to "Prefer to Rate" Option`s value', //519
'New&Changed Photos', //520
'Remove selected & Reload for new', //521
'Total <B>{0}</B> users should be checked for new&changed photos', //522
'Check new&changed pictures you want to remove. All unchecked pictures will be approved.', //523
'Remove photo', //524
'No user available with unchecked photos', //525
'Change profile and Approve it!', //526
'Registration fields', //527
'Search fields', //528
'Search pages to show by access level - 0 means unlimited pages', //529
'Search results', //530
'Coupon payments', //531
'Enable image verification?', //532
'Messaging Management', //533
'No coupons available', //534
'Coupon`s name', //535
'Coupons, enter coupons per line, for ex.:<BR>PROMO-GSM5201-931<BR>PROMO-GSM3411-121<BR>PROMO-GSM9342-091', //536
'Privilegies period (in days)', //537
'Coupon`s number', //538
'Attention: Coupons will remove when user will enter it, to prevent to enter one coupon several times.', //539
'Selected coupon`s group has been removed!', //540
'Coupon`s with this identification name already exist, please select another!', //541
'Who can use smiles and special tags', //542
'Who can create new threads', //543
'Who can reply', //544
'<i>Security Alert</i>: Please remove install.php', //545
'Number of days to store information about "Who seen my profile"', //546
'Enable Bad Word checking', //547
'Specify Bad words delimited by commas. Search by this words is case insensitive. You can use * char for unknown begin and end of the word, for example: *@*,*_at_*,fuck*,bitch', //548
'What to make if badwords will be found? Choose one 1 or 2...', //549
'1. Show error message',//550
'Specify error message',//551
'2. Just to replace badword with another text',//552
'Specify replacement word. For example: ####',//553
'Welcome Title',//554
'Welcome message [in html format] (Welcome title and message will show in this 1 common language. If you want, you can remove 2 constants from options.inc.php (C_WELCOME_TITLE and C_WELCOME_MESSAGE) to show phrases from language files.)',//555
'Multilanguage Interface (MUI)',//556
'"Default" language code in 2 chars:',//557
'Choose editable language',//558
'News should be multilanguage',//559
'FAQ should be multilanguage',//560
'Multilanguage Interface (MUI) - Edit phrases',//561
'Fields to show in edit&view profile pages for admin',//562
'Don`t show fields with empty or not selected values',//563
'Field name',//564
'Edit mode',//565
'View mode',//566
'Mezõket mutatja a felhasználók regisztráció és frissítési a oldalain',//567
'Search fields to search by access level',//568
'Search results fields by access level',//569
'Please enter question',//570
'Please correct number of questions value',//571
'Please select language(s)',//572
'Total messages:',//573
'Sorry - this section doesn`t work in the demo mode',//574
'Quick links',//575
'Script News',//576
'Script Information',//577
'Script Version',//578
'Total messages:',//579
'Total uploaded files:',//580
'Size of uploaded files (Mb):',//581
'PHP Version',//582
'MySQL Version',//583
'Type of loader',//584
'Safe Mode',//585
'GD module',//586
'Hosting Information',//587
'To ID',//588
'From ID',//589
'Messages sent in last N days',//590
'You can use special symbols: * - any number of chars, ? - any 1 char',//591
'Approve selected profiles',//592
'Remove selected profiles',//593
'Remove selected messages',//594
'Message with ID#={0} has been successfully deleted from database',//595
'Allow to see own comments',//596
'Allow to see all user`s comments',//597
'Enable Bad word checking?',//598
'Allow to reply to own comments',//599
'Allow to add new comments',//600
'Allow to remove own comments',//601
'Search Options',//602
'Administrator area language',//603
'Who can see number of total users found in the search results?',//604
'Allow to see the photo of commenting',//605
'Image Verification',//606
'Enable Image Verification for Register',//607
'Enable Image Verification for Login',//608
'Decrease image size and dimensions when uploading to server?',//609
'Decrease to width',//610
'Decrease to height',//611
'Decrease automatically biggest size to following value',//612
'Decreased image quality',//613
'How many photos to show in the User Profile page',//614
'How many photos to show at one step in the Photos page',//615
'Allow to make right clicks in the Photos Page?',//616
'Show error message if user click right button?',//617
'Default number of search results',//618
'Who can to see Visited Profiles page?',//619
'Watermark text transparency',//620
'Select access to show random template or disable it',//621
'Show Expired Privilegies Date in the header',//622
'Who can prefer to be not rated?',//623
'Who can to see the day of birthday in the User Profile',//624
'Use Gallery - just new or changed photos will be show for admin checking',//625
'Show new or changed photos in Profiles only after administration checking in the "User Management => New&Changed photos". Gallery option should be also enable.',//626
'Number of columns in the Gallery',//627
'Number of rows in the Gallery',//628
'Online Users',//629
'Search by gender',//630
'Search by user type',//631
'Search by picture',//632
'Steps for search (divided by comma)',//633
'Show privilegied users first in search results',//634
'Resize photos in search results - make long side not more than "Width of photos"',//635
'Minimal requirement number of photos',//636
'Who can to use smiles and special tags in the Send Message form?',//637
'Show "No Photo" picture in the search results',//638
'File name of the "No photo" picture ({G} - replace with the user gender)',//639
'Messaging type <BR>(Select this once after installation and do not change in the future to prevent possible errors)',//640
'Table name',//641
'Repair',//642
'Optimize',//643
'Operation',//644
'Message type',//645
'Attention: Your website is working in non maitenance mode. Is highly recommending to <a href=index.php?do=opt&type=config&>Enable Maintenance Mode</a> before running Repair or Optimize tables.',//646
'Help',//647
'Release date',//648
'Documentation',//649
'Permanently remove user(s) <BR>(else just a change status to "Removed")',//650
'Your profile has been removed from {0}',//651
'Delete Profiles',//652
'Remove profiles from admin area permanently?',//653
'Attention: options below are actual for <B>"Delete"</B> links only, in the search results, view and edit profile pages.<BR><BR> Profiles will remove permanently without confirmation if you removing them by selecting checkboxes in search results and click on <B>"Delete selected profiles"</B><BR><BR>',//654
'Admin`s edit&view profile',//655
'Do not open the new window and remove profiles without confirmation',//656
'Allow to send email in the confirmation page?<BR>(By default form will be closed, so you need to check and open it)',//657
'Fill out subject field of email with default Subject value?',//658
'Fill out subject field of email with default Message value?',//659
'Send email in administrator language?',//660
'Confirm administrator when remove profiles using checkboxes in the search results?',//661
'WARNING: This will permanently remove the selected profiles. Are you sure you wish to do this?',//662
'Send email if user profile removed using checkboxes (or directly from if user itself requested it from members area)',//663
'Remove selected',//664
'Security information',//665
'Fail admin logins',//666
'<span style=color:red>Off</span>',//667
'<span style=color:green>On</span>',//668
'Security Errors',//669
'Users registered in last day',//670
'Users registered in last 3 days',//671
'Users registered in last 7 days',//672
'Notices',//673
'Select the fields should be counted for %',//674
'Enable notice for not filled profiles',//675
'Show notice only if % of filled(and selected here) fields lower than this value. For example show notice if less than 70% of fields are filled. Enter just a number (0-100) without %',//676
'Enable notice if photo not yet uploaded',//677
'Enable notice if "About" field have small number of words',//678
'How many words, minimal, should present in the "About" field',//679
'Currency',//680
'Testimonials results by access level',//681
'Can view testimonials',//682
'Can add new testimonials',//683
'Can use BB-codes and smiles when adding testimonials',//684
'How to add testimonials',//685
'Number of testimonials per page',//686
'Can see number of total testimonials number?',//687
'Testimonials Management',//688
'Testimonial has been changed to active',//689
'Testimonial has been changed to inactive',//690
'This testimonial is ',//691
'Edit&Reply testimonial',//692
'Remove testimonial',//693
'Testimonial has been removed!',//694
'You may reply to this testimonial, if it is requiring',//695
'Testimonial has been changed',//696
'Use SMTP',//697
'SMTP Host* (SMTP server to relay, like "smtp.myisp.com")',//698
'SMTP Port* (port of the SMTP server to use, like 25 or 465)',//699
'SMTP server requires an secure connection using SSL',//700
'POP3 authentication host if your SMTP server requires prior POP3 authentication',//701
'User name if the server requires authetication',//702
'Authetication password',//703
'Authetication realm, usually the authentication user e-mail domain',//704
'Mail options',//705
'Articles Management',//706
'Create Article',//707
'Edit Article',//708
'Delete Article',//709
'New article has been successfully added to database',//710
'Article has been changed',//711
'Attention: This will also delete all articles entire category',//712
'Edit Mail messages',//713
'phrase number #',//714
'Edit file:',//715
'Common subject - see the subject phrase number#{0} above',//716
'Mail subject the same as body or entered by user',//717
'Templates management',//718
'Can`t open file - read access denied',//719
'Can`t edit file - write access denied',//720
'File not found',//721
'File has been changed',//722
'Can`t create backup file - write access denied for file or directory',//723
'Restore original',//724
'Can`t restore original file - write access denied for file or directory',//725
'Original file {0} has been restored',//726
'Options below actual for both - audio and video',//727
'Addon: Allow to start dispatch in the backend - by cronjob (cronjob entry should be also created - see addons/cronjob-send_mail_to_subscribers.php).<br> Checkbox will be unchecked automatically when addon script will be finished',//728
'How to send',//729
'1 or more',//730
'New private photos',//731
'Check private photos you wish to remove. Unchecked photos will be approved',//732
'There are total {0} private photos should be checked',//733
'Access',//734
'Points number has been updated',//735
'Points management',//736
'Points to see profile fields',//737
'Point`s multiplier',//738
'Allow to multiply number of points personally for this user - only integer value possible',//739
'Show by user country first: ',//740
'Select country in the listing of ',//741
'IP2Country Management',//742
'Enable IP2Country mode?',//743
'Where to use IP2Country mode?',//744
'Search by ZIP Distance (requires to install ZIP data)',//745
'META tags',//746
'Title',//747
'Keywords',//748
'Description',//749
'Profile page',//750
'Available values',//751
'Default metas: title, keywords and description you can edit in the each using language file. Specific for some pages meta tags you can change here',//752
'Open IM in the new window',//753
'Search in the forum',//754
'How many ranges from IP to hide from forum moderators? <br>0 show full IP, 4 - hides all with *.*.*.*',//755
'Show first post in the each page of the thread',//756
'Can use signature',//757
'Top Forum values number',//758
'Can use forum options',//759
'Can use avatar',//760
'Can upload own avatar',//761
'Avatar maximum width',//762
'Avatar maximum height',//763
'Avatar maximum size in Kb',//764
'Decrease avatar when upload',//765
'Decreasing quality (0-100)',//766
'Existing avatars extension (only 1 common extension allowed)',//767
'Show existing avatars in row',//768
'Search results number',//769
'Search timeout<br>(deprecate to searching more often than once in N seconds)',//770
'Search results limit<br>(limit number of found results to this value)',//771
'How often to refresh messages window in the IM (in seconds)',//772
'Slideshow options',//773
'Can see slideshow',//774
'Slideshow interval in seconds',//775
'Use transformation filter',//776
'Transformation speed',//777
'Can see big photos',//778
'Minimal photos number for slideshow',//779
'Remove profiles marked as deleted, automatically, after number of days<br>(0 - disable this option) ',//780
'Show photos in the Last registered users listing',//781
'User should confirm agreement by checkbox',//782
'Can see number of user near each menu link',//783
'Can enable/disable IM sound when new message received',//784
'By default enable IM sound for new users',//785
'IM options',//786
'Can have blog',//787
'Can see user`s blogs',//788
'Can edit own blog entries',//789
'Can make public/private own blog entries',//790
'Can remove own blog entries',//791
'Can use smiles in the own blog entries',//792
'Make entry public by default',//793
'Number of entries per page',//794
'Can create views',//795
'Number of views per page',//796
'Show small index page',//797
'Can see articles',//798
'Articles should be multilanguage',//799
'Add "-f" send from option to sendmail',//800
'Backup template files when editing',//801
'Can create private photos',//802
'Can see private photos',//803
'How to add new private photos',//804
'Can use glitters in the subject',//805
'Can use glitters in the message',//806
'IDs of forum global moderators, divided by comma, ex: 52,326',//807
'Allow ban for forums',//808
'Can see who is in the User Favorites list',//809
'User should be present in the User Favorites list to see other',//810
'Can see forum posts',//811
'Can use points',//812
'Also sell access levels <br>(uncheck if you wish to sell points only)',//813
'How many smiles to show <br>(if you have more smiles, will show link to new page with all smiles). <br>This option is actual for all forms where smiles exist.<br><br>To edit smiles list upload smiles into template\*\forum\smiles\ and edit file template\*\forum\smiles.php',//814
'English file directory name (default)',//815
'Can search by ZIP radius in the Quick search',//816
'How many private photos can upload each user type',//817
'How many views can create each user type',//818
'How many points provide to new users by gender',//819
'Point multiplier value by gender',//820
'Points plans - number of points in each',//821
'Points prices per each plan',//822
'Access level for new users',//823
'Period of access level for user (in monthes)',//824
'Can create private photos for all users',//825
'Can to hide profile',//826
'Archiving messages',//827
'Remove thumbnails',//828
'Cleaning error photos',//829
'{0} users with {1} pictures have been fixed',//830
'{0} thumbnails have been removed',//831
'Archive messages older than (in days, 90 or higher - recommended)',//832
'Do not archive unread messages',//833
'Cleaning Archive',//834
'{0} messages have been successfully transfered to archive',//835
'Cleaning Cache',//836
'Cache has been successfully cleaned',//837
'Can create private notes',//838
'Can to use smiles',//839
'Send birthday`s congratulations',//840
'Ready to send',//841
'Already sent today',//842
'Dispatch has been successfully finished',//843
'Comments management',//844
'Comment for {0}',//845
'Reply from {0}',//846
'Coupon type',//847
'Points number',//848
'Access level for users filled own profiles (for unlimited time, while profile is filled)',//849
'Access level will be provided only for gender',//850
'Move to private photos',//851
'Kitiltott profilok',//852
'Kitiltás',//853
'Successfully suspended user with id={0}',//854
'Are you sure that you want to archive messages by your choice?',//855
'BBCodes Management',//856
'Can to use smiles',//857
'Can to use <b>Bold</b>',//858
'Can to use <i>Italic</i>',//859
'Can to use <u>Underline</u>',//860
'Can to use <s>Strike</s>',//861
'Can to use QUOTES',//862
'Can to use URL',//863
'Can to use IMG',//864
'Can to use VIDEO',//865
'You can to enter own copyrights here. <br>(Only " double quotes can be used)',//866
'Default language for users',//867
'Who can to access to write users with the same gender?',//868
'Genders required to upload photos',//869
'Allow to update profile only once in N hours (0 - disable this option and allow to update profile anytime)',//870
'Search by country',//871
'Search by city',//872
'Search by age',//873
'Show search results per page',//874
'Remove users from online users list after logout',//875
'Secure unmixed login sessions for admin and user <br>(uncheck if you want to make possible to login as user and then as admin without loosing access as user)',//876
'Show special checkbox to check all multiple options',//877
'Show last registered by gender',//878
'After login go to URL (if empty go to check messages).<br> You can to use these variables: {LANGUAGE} is user language, {URL} - website url, {ID} - user id, for ex, My profile page will be: {URL}/view.php?l={LANGUAGE}&id={ID}&MyProfile',//879
'Use "Today", "Yesterday" phrases for dates',//880
'How many stars to show for forum users based on number of posted messages. Each next number shows additional star. Ex: 5,20,50,100,500,1000',//881
'Select star image (You can also upload own into templates/TEMPLATE/images/s0.png)',//882
'Can to see "Features" link',//883
'Can to make own "View Profile" hidden',//884
'Can to be hidden in the "Who seen my profile" list',//885
'Can to select template for own "View profile" page',//886
'Can to link to other profile with description (ex. "My wife")',//887
'Can to prefer not to receive emails from website',//888
'Can to see "Users here" on the forum main page and thread',//889
'Can to see forum statistics',//890
'Enable AJAX(Synchronize - without page reloading) message checking for',//891
'Send AJAX requests every N seconds',//892
'Provide privilegied status for free users, if they filled own profile (Only if notices enabled)',//893
'Image verification (CAPTCHA) algorithm type',//894
'Can <b><u>NOT</u></b> to verify image for',//895
'send message to user form from the "View Profile"',//896
'forum thread and posts',//897
'blog comments',//898
'Enable quality images if possible (this will take additinal server resources)',//899
'Pixels to cut from the sides of thumbnail to improve portrait characteristics',//900
'Can to select access level for each photo',//901
'Limit photos number per each type:',//902
'How many private photos for each type can be uploaded in % (do not write %, just a 4 digitis for each, ex: 40,20,20,20).',//903
'If profile unactive, show the reason',//904
'SEF options',//905
'(SEF) Search Engine Friendly options are working if <b>mod_rewrite</b> is enabled and <b>.htaccess</b> file can be processed. Any changes made here should be also made manually in the <b>.htaccess</b> file of the main directory. Be careful by these options changing - disable SEF, if it will not work.',//906
'Enable SEF',//907
'Encode URL`s titles for non-english characters',//908
'Link to user profile',//909
'Default value:',//910
'Top users',//911
'MySQL tables charset. Leave this table blank by default. If returned find_in_set error, set it to _latin1 or _utf8',//912
'Autosubscribe users for new profiles, when register',//913
'Subscribe for gender',//914
'Subscribe for users with photos',//915
'Remove subscribers, if remove their profiles',//916
'Can prefer not to receive winks',//917
'Can to send winks, if is even deprecated by user',//918
'Webmoney options',//919
'Webmoney ID',//920
'Webmoney Purse',//921
'Can to add links',//922
'Enable Webmoney',//923
'Description size (in chars)',//924
'Request Reciprocal Link URL',//925
'Show Reciprocal Link URL field, even if not requested',//926
'Your reciprocal URL to request to show on the other websites<br>(Use " double quotes only)',//927
'Request exact code (else just your URL will be checked with any description)',//928
'How to approve added links',//929
'Can to Rate photos from the "View Profile"',//930
'Can to Rate photos from "Hot or Not" random page',//931
'Hot or Not is a popularity system for photos',//932
'Can to participate in the "Hot or Not"',//933
'Can to see link to profile, when rate',//934
'Key to crypt URL when "Hot or Not" rating showing. This allow to crypt user ID in URL',//935
'Number of votes for photo requiring to show in the top',//936
'Number of best photos',//937
'Can to access to "More Results" page of the Best photos',//938
'Show full list of all states for most of countries on the Registration page. This will increase size of the registration page up to 600 Kb.',//939
'Limit text size of the blog in search results to number of characters',//940
'Show blogs only for last X days, 0 - unlimited time',//941
'Show new/edited blogs only after admin checking (else it will show authomatically)',//942
'Request from user to select category (Create blogs categories first)',//943
'Minimal number of votes for rating',//944
'Can to comment blogs',//945
'Can to see comments in the blogs',//946
'Ignored users can`t to post in the user blogs',//947
'Disable to comment blog for users with the same gender as author',//948
'Enable badwords checking in the blog text',//949
'Replace badwords with #### (else deprecate to post and show error)',//950
'Enable badword checking in the blog comments',//951
'Can to rate blogs',//952
'How many comments to show per one page (option common for blog and profile comments)',//953
'Can to reply to comments for own blog',//954
'Max image width for [IMG] tag (if bigger, picture will be decreased)',//955
'Make pictures clickable to open in actual size (this will open images on the other websites)',//956
'How many Images in the post are allowed',//957
'Extensions allowed to post',//958
'How many Videos in the post are allowed',//959
'Video width',//960
'Video height',//961
'Can to see linked profile',//962
'Enable privilegies purchasing by points',//963
'Send mail if privilegies purchased by points',//964
'Points number required to purchase access level for 1 hour',//965
'Show banners in the search results after each N users (0 - disable this feature), 5 - shows just 1 banner in the middle of 10 results per search results. Note: banner codes should to be placed into templates/USING_TEMPLATE/banner_search_results.php',//966
'Find Link',//967
'Old format link - please edit to correct URL value',//968
'Status of entry with ID#={0} changed to "{1}"',//969
'Links by category',//970
'Links to show per one page',//971
'Blogs Management',//972
'Is not checked',//973
'Remove all entries entire <br>(uncheck, if you wish to leave all entries and after category deleting, all entries will have undefined Category)',//974
'Approval status',//975
'On the features page show only features, allowed for selected access level ("Guest" will show all features, "Nobody" - features deprecated for all)',//976
'Blogs awaiting approve',//977
'Not confirmed',//978
'Number of days to remove links not confirmed by email (if confirmation by email turn on)',//979
'Links awaiting approve',//980
'Total approved blogs',//981
'Total links',//982
'Total archive messages',//983
'Session lifetime (sec)',//984
'Server OS',//985
'Server API',//986
'Can suspend user profiles',//987
'All is Free - based on Access levels (Enable Payment system)',//988
'Ennyi nap után az üzenetek törlése (0 nem töröl)',//989
'Successfully add user with id={0} to the banlist of user with id={1}',//990
''
); 


$xr=array(
'Automatikusan engedélyezzen minden adatlapot',
'Email ellenõrzés után',
'Admin ellenõrzés után',
'Email és admin ellenõrzés után'
);
$xu=array(
'Rejtse és várjon admin ellenõrzésre',
'Authomatikusan frissitse',
'Mutassa és várjon admin ellenõrzésre'
);
$xup=array(
'Mindig ellenõrzés ha adatlap változik',
'Csak ha képet cserél vagy hozzáad',
'Csak ha hangot cserél vagy hozzáad',
'Csak ha hang vagy kép hozzáad vagy cserél'
);
$xd=array(
'Nem engedélyez adatlaptörlést',
'Mutasd és ellenõrzés után töröl',
'Elrejt és ellenõrzés után töröl',
'Automatikusan törli az adatlapot'
);
$xl=array(
'Ne mutassa - use default language',
'Ne mutassa - use browser language',
'Mutassa, if don`t selected by user',
'Mindig mutassa languages'
);
$xa=array(
'Rejtett',
'Magas és rövid',
'Magas és hosszú (biggest type)',
'Alacsony és rövid (smallest type)',
'Magas és rövid'
);
$xs=array(
'Ne ellenõrizzen IPt és böngészõt',
'Csak IP ellenõrzése',
'Csak böngészõ ellenõrzése',
'Minden ellenõrzése'
);
$xn=array(
'ID',
'Felhasználónév (Csak ha ezzel jelentkezik be)',
'Elõnév',
'Elõnév és utónév',
'ID és felhasználónév',
'ID és elõnév',
'ID és elõnév és utónév'
);
$xp=array(
'Fizetés nélküli - Minden ungyenes (Kizárva a fizetõ rendszer)',
'Minden ingyen, but user must be a privileged user for read messages from another user',
'Minden ingyen, but user must be privileged user for send message to another people. However, if user got message from privileged user, he can answer to them :)',
'Minden ingyen, but user must be privileged user for read messages and write messages');
$xst=array(
'Ne mutassa a statisztikát',
'Statisztika mutatása link nélkül',
'Statisztika mutatása linkekkel'
);

$xth=array(
'Don`t use thumbnails',
'Generate on the fly',
'Create and store on the server (recommended)'
);
$xlt=array(
'Fénykép',
'Adatlap');
$xcache=array('Don`t use Cache','MySQL queries caching');
$xwmtype=array('Text','Image');
$xwmpos=array('Top Left','Top Center','Top Right','Middle Left','Middle Center','Middle Right','Bottom Left','Bottom Center','Bottom Right','Random Position');
$xwmor=array('Automatic','Horizontally','Vertically');
$xpop=array('Disable','Based on different Action`s Hits','Based on Rate System');
$xreg=array('All-In-One','Step by Step');
$xbl=array('All types','Username','Email');
$xmes=array('Show By Messages (Old)','Show By Users (New)');
$xadr=array('Sort by last contacted users','Sort by user`s last access time','Sort by username ascending');
$xtst=array('Authomaticaly','Hide and wait admin checking','Show and wait admin checking');
$xav=array('Disable Audio and Video','Allow to upload audio','Allow to upload video','Allow to upload audio and video');
$xmm=array('Send as mail','Send as internal message','Send as internal message and notice by email');
$xip=array($x[740].'Last registered users (Main page)',$x[740].'Last registered users (More Results)',$x[740].'Most popular users (Main page)',$x[740].'Most popular users (More Results)',$x[740].'Top users with photos (Main page)',$x[740].'Top users with photos (More Results)',$x[740].'Birthdays (Main Page)',$x[740].'Birthdays (More Results)',$x[741].'Quick Search',$x[741].'Simple Search',$x[741].'Advanced Search',$x[741].'Register Page',$x[741].'Subscription Page');

//Kitiltás indokai
$xur = array(
    "rossz_kep" => array(
        "title" => "Nem megfelelő kép",
        "subject" => "Kitiltásra kerültél a Hotbull Randin",
        "text" => 'Szia,

Ki lettél tiltva a Hotbullról, mert nem megfelelő képet töltöttél fel. A képekre vonatkozó szabályaink a következők:

1. Profilkép csakis olyan kép lehet, ami rólad készült. Tehát nem lehet internetről másolt kép, rajz, képregény, csendélet vagy egy pornósztár vagy a vágyaidról készült kép.
2. A rólad készült képnek nem kell arcképnek lennie. Akár ki is takarhatod azt a részt, ami alapján bárki felismerhet.
3. Galériádba nem tölthetsz fel olyan képet, amivel reklámozol más oldalakat is - azaz van rajta egy olyan vízjel, mely más oldal logója, megnevezése.
4. Galériádba nem tölthetsz fel olyan képet, melyen rajta van más elérhetőséged. Pl. a telefonszámodat, vagy mondjuk a Messenger/Snapchat QR-ikonod. QR kód is elérhetőségnek számít.
5. Galériádba nem tölthetsz fel olyan képet, amely más személy tulajdonához tartozik és nincs engedélyed azt feltölteni - pl.: más tagunkról készült kép, és a tag ez ellen tiltakozik. Ha az az illető ezt megengedte, akkor természetesen feltöltheted a galériádba.
6. Galériádba nem tölthetsz fel olyan képet, mely nem illeszkedik a Hotbull profiljába. Pl.: meztelen nőt, női nemiszervet, heteroszexuális aktust.

Figyelem: Mindig az első megjelenő kép számít profilképnek. Tehát ha az adatlap módosításánál nem töltesz fel 1. képet, de feltöltesz 2. képet, akkor a 2. kép lesz profilkép. Tehát MINDIG lesz profilképed, ha van feltöltött képed!

Ha elfogadod a szabályokat, akkor visszaengedünk.

Üdv
Admin',
        "html" => '<p>Szia,</p>

<p>Ki lettél tiltva a Hotbullról, mert nem megfelelő képet töltöttél fel. A képekre vonatkozó szabályaink a következők:</p>

<p>1. Profilkép csakis olyan kép lehet, ami rólad készült. Tehát nem lehet internetről másolt kép, rajz, képregény, csendélet vagy egy pornósztár vagy a vágyaidról készült kép.<br/>
2. A rólad készült képnek nem kell arcképnek lennie. Akár ki is takarhatod azt a részt, ami alapján bárki felismerhet.<br/>
3. Galériádba nem tölthetsz fel olyan képet, amivel reklámozol más oldalakat is - azaz van rajta egy olyan vízjel, mely más oldal logója, megnevezése.<br/>
4. Galériádba nem tölthetsz fel olyan képet, melyen rajta van más elérhetőséged. Pl. a telefonszámodat, vagy mondjuk a Messenger/Snapchat QR-ikonod. QR kód is elérhetőségnek számít.<br/>
5. Galériádba nem tölthetsz fel olyan képet, amely más személy tulajdonához tartozik és nincs engedélyed azt feltölteni - pl.: más tagunkról készült kép, és a tag ez ellen tiltakozik. Ha az az illető ezt megengedte, akkor természetesen feltöltheted a galériádba.<br/>
6. Galériádba nem tölthetsz fel olyan képet, mely nem illeszkedik a Hotbull profiljába. Pl.: meztelen nőt, női nemiszervet, heteroszexuális aktust.</p>>

<p>Figyelem: Mindig az első megjelenő kép számít profilképnek. Tehát ha az adatlap módosításánál nem töltesz fel 1. képet, de feltöltesz 2. képet, akkor a 2. kép lesz profilkép. Tehát MINDIG lesz profilképed, ha van feltöltött képed!</p>

<p>Ha elfogadod a szabályokat, akkor visszaengedünk.</p>

<p>Üdv<br/>
Admin</p>',
        "chat" => 'Szia,

Emlékeztetőül küldjük, hogy korábban ki lettél tiltva a Hotbullról, mert nem megfelelő képet adtál meg profilképnek. 
A profil kép csakis egy ténylegesen rólad készült kép lehet. 
Tehát nem olyan kép, ami másolt (és látszik is, hogy képernyőképet küldtél el), 
nem egy rajz, egy tárgy vagy egy csendélet vagy egy rajzfilm figura, vagy egy pornósztár, vagy a vágyaidról készült kép.
Valamint nem fogadjuk el más társkereső logóját, vízjelét tartalmazó képet sem, mivel azzal reklámozod a másik társkeresőt.

Ha esetleg nem lenne világos mi is a profilkép... A profilkép a legelső megjelenő kép, ami a listázásra is bekerül. 
A többi képet galériaképnek nevezünk, és arra semmilyen korlátozás nincs (azaz van, ha más tulajdonát képező képről van szó vagy a Hotbull profiljába nem illő kép - pl.: meztelen nőkről készült kép)

Üdv
Admin'
    ),

    "elerhetoseg" => array(
        "title" => "Elérhetőség a szövegben",
        "subject" => "Kitiltásra kerültél a Hotbull Randin",
        "text" => 'Szia,

Azért tiltottunk ki, mert megadtad az elérhetőségedet a profilodban. Ez szigorúan tilos.

Amint válaszodban elfogadod, hogy semmilyen elérhetőséget nem adhatsz a nyilvános profilodon, 
mint pl. a telefonszám, az email cím, a Facebook vagy a Skype elérhetőség,
akkor visszaengedünk a rendszerbe.

Üdv
Admin',
        "html" => '<p>Szia,</p>

<p>Azért tiltottunk ki, mert megadtad az elérhetőségedet a profilodban. Ez szigorúan tilos.</p>

<p>Amint válaszodban elfogadod, hogy semmilyen elérhetőséget nem adhatsz a nyilvános profilodon,<br/> 
mint pl. a telefonszám, az email cím, a Facebook vagy a Skype elérhetőség,<br/>
akkor visszaengedünk a rendszerbe.</p>

<p>Üdv<br/>
Admin</p>',
        "chat" => 'Szia,

Emlékeztetőül küldjük, hogy korábban ki lettél tiltva, mert megadtad az elérhetőségedet a profilodban. Ez szigorúan tilos.
Semmilyen elérhetőséget nem adhatsz a nyilvános profilodon, mint pl. a telefonszám, az email cím, a Facebook vagy a Skype elérhetőség.

Üdv
Admin'
    ),

    "feketelista" => array(
        "title" => "Feketelista, vagyis név szerinti említés a profilon",
        "subject" => "Kitiltásra kerültél a Hotbull Randin",
        "text" => 'Szia,

Ki lettél tiltva a Hotbullról nem megfelelő profilszöveg miatt, mivel a feketelista vezetése tilos.
Mi a feketelista? Feketelista az, ha valakit néven nevezel meg és azzal rossz színben tünteted fel.
Nem érdekel minket, hogy mi az indok, a feketelista vezetése szigorúan tilos, mivel a feketelistában feltüntetett személyek a jó hírnév megsértése jogcímen be is perelhet téged is és minket is, mert nem szedtük le.

Kérjük, hogy a sérelmeidet ne nyilvánosan vezesd le. Amint válaszoddal elfogadod, hogy ezt nem teszed, visszaengedünk.

Üdv
Admin',
        "html" => '<p>Szia,</p>

<p>Ki lettél tiltva a Hotbullról nem megfelelő profilszöveg miatt, mivel a feketelista vezetése tilos.<br/>
Mi a feketelista? Feketelista az, ha valakit néven nevezel meg és azzal rossz színben tünteted fel.<br/>
Nem érdekel minket, hogy mi az indok, a feketelista vezetése szigorúan tilos, mivel a feketelistában feltüntetett személyek a jó hírnév megsértése jogcímen be is perelhet téged is és minket is, mert nem szedtük le.</p>

<p>Kérjük, hogy a sérelmeidet ne nyilvánosan vezesd le. Amint válaszoddal elfogadod, hogy ezt nem teszed, visszaengedünk.</p>

<p>Üdv<br/>
Admin</p>',
        "chat" => 'Szia,

Emlékeztetőül küldjük, hogy korábban ki lettél tiltva a Hotbullról nem megfelelő profilszöveg miatt, mivel a feketelista vezetése tilos.
Mi a feketelista? Feketelista az, ha valakit néven nevezel meg és azzal rossz színben tünteted fel.
Nem érdekel minket, hogy mi az indok, a feketelista vezetése szigorúan tilos, 
mivel a feketelistában feltüntetett személyek a jó hírnév megsértése jogcímen be is perelhet téged is és minket is, mert nem szedtük le.

Kérjük, hogy a sérelmeidet ne nyilvánosan vezesd le.

Üdv
Admin'
    ),

    "mas_tarskereso" => array(
        "title" => "Más társkeresőre hivatkozás akár képpel, akár szöveggel",
        "subject" => "Kitiltásra kerültél a Hotbull Randin",
        "text" => 'Szia,

Ki lettél tiltva a Hotbullról, mert más társkeresőt ill. weboldalt reklámoztál. 
Vagy nem megfelelő profilszöveg miatt, vagy a képeid vízjelet tartalmaznak.
Amint válaszoddal elfogadod, hogy nem reklámozhatsz már társkeresőt ill. oldalt a profilodon, akkor visszaengedünk.

Üdv
Admin',
        "html" => '<p>Szia,</p>

<p>Ki lettél tiltva a Hotbullról, mert más társkeresőt ill. weboldalt reklámoztál.<br/> 
Vagy nem megfelelő profilszöveg miatt, vagy a képeid vízjelet tartalmaznak.<br/>
Amint válaszoddal elfogadod, hogy nem reklámozhatsz már társkeresőt ill. oldalt a profilodon, akkor visszaengedünk.</p>

<p>Üdv<br/>
Admin</p>',
        "chat" => 'Szia,

Emlékeztetőül küldjük, hogy korábban ki lettél tiltva a Hotbullról nem megfelelő profilszöveg vagy kép miatt: más társkeresőt reklámoztál.
Nem reklámozhatsz más társkeresőt a profilodon, vagy nem töltesz fel más társkeresőből származó képet.

Üdv
Admin'
    ),

    "penz_ajanlas" => array(
        "title" => "Pénz ajánl a szexért",
        "subject" => "Kitiltásra kerültél a Hotbull Randin",
        "text" => 'Szia,

Nem megfelelő profilszöveg miatt, miszerint támogatást adsz szexért, kitiltottunk a Hotbull Randiról.

A Hotbull Randi nem támogatja az escort tevékenységet, és tiltja azokat a személyeket is, akik ezt a tevékenységet támogatják akár szavakban is.

Amennyiben ezt válaszleveledben elfogadod, visszaengedünk.

Üdv
Admin',
        "html" => '<p>Szia,</p>

<p>Nem megfelelő profilszöveg miatt, miszerint támogatást adsz szexért, kitiltottunk a Hotbull Randiról.</p>

<p>A Hotbull Randi nem támogatja az escort tevékenységet, és tiltja azokat a személyeket is, akik ezt a tevékenységet támogatják akár szavakban is.</p>

<p>Amennyiben ezt válaszleveledben elfogadod, visszaengedünk.</p>

<p>Üdv<br/>
Admin</p>',
        "chat" => 'Szia,

Emlékeztetőül küldjük, hogy korábban ki lettél tiltva nem megfelelő profilszöveg miatt, miszerint támogatást adsz szexért.
A Hotbull Randi nem támogatja az escort tevékenységet, és tiltja azokat a személyeket is, akik ezt a tevékenységet támogatják akár szavakban is.

Üdv
Admin'
    ),

    "escort" => array(
        "title" => "Escort tevékenység hirdetése",
        "subject" => "Kitiltásra kerültél a Hotbull Randin",
        "text" => 'Szia,

Escort tevékenység miatt ki vagy tiltva a Hotbullról.

Ha esetleg nem tudnád, mi az:
1. Escort tevékenység szexuális szolgáltatás nyújtása pénzért cserébe.
2. Az illegális prostitúció törvénybe ütköző cselekedet, ezért is tiltjuk ki a rendszerből az escortokat.
3. A legális prostítúció (merthogy az is van már) viszont üzleti tevékenység, és a Hotbullt csakis magánszemélyek használhatják, üzleti célra nem használható. Emiatt is kitiltás jár.
4. Vagyis mindenképpen kitiltás a büntetés. 

Bizonyára ráutaló magatartással (pl.: profilszövegben virágnyelven beszélsz róla - mondjuk támogatást kérsz), vagy történt egy jelentés egy felhasználótól, akit zaklattál az ajánlataiddal.

A kizárásunk végleges, az újraregisztráció letiltva.

Üdv
Admin',
        "html" => '<p>Szia,</p>

<p>Escort tevékenység miatt ki vagy tiltva a Hotbullról.</p>

<p>Ha esetleg nem tudnád, mi az:<br/>
1. Escort tevékenység szexuális szolgáltatás nyújtása pénzért cserébe.<br/>
2. Az illegális prostitúció törvénybe ütköző cselekedet, ezért is tiltjuk ki a rendszerből az escortokat.<br/>
3. A legális prostítúció (merthogy az is van már) viszont üzleti tevékenység, és a Hotbullt csakis magánszemélyek használhatják, üzleti célra nem használható. Emiatt is kitiltás jár.<br/>
4. Vagyis mindenképpen kitiltás a büntetés.</p> 

<p>Bizonyára ráutaló magatartással (pl.: profilszövegben virágnyelven beszélsz róla - mondjuk támogatást kérsz), vagy történt egy jelentés egy felhasználótól, akit zaklattál az ajánlataiddal.</p>

<p>A kizárásunk végleges, az újraregisztráció letiltva.</p>

<p>Üdv<br/>
Admin</p>',
        "chat" => 'Szia,

Escort tevékenység miatt ki vagy tiltva a Hotbullról.
Ha esetleg nem tudnád, mi az:
1. Escort tevékenység szexuális szolgáltatás nyújtása pénzért cserébe.
2. Az illegális prostitúció törvénybe ütköző cselekedet, ezért is tiltjuk ki a rendszerből az escortokat.
3. A legális prostítúció (merthogy az is van már) viszont üzleti tevékenység, és a Hotbullt csakis magánszemélyek használhatják, üzleti célra nem használható. Emiatt is kitiltás jár.
4. Vagyis mindenképpen kitiltás a büntetés. 

Bizonyára ráutaló magatartással (pl.: profilszövegben virágnyelven beszélsz róla - mondjuk támogatást kérsz), 
vagy történt egy jelentés egy felhasználótól, akit zaklattál az ajánlataiddal.

Üdv
Admin'
    ),

    "vallakozas" => array(
        "title" => "Vállalkozói tevékenység vagy nonprofit szervezet hirdetése",
        "subject" => "Kitiltásra kerültél a Hotbull Randin",
        "text" => 'Szia,

Üzleti vállalkozások regisztrációja alapban tiltva vannak a Hotbull-on. De ha szeretnél egy banner hirdetést, ami a Hotbullon és Gaypointon is fut, akkor kérj ajánlatot tőlünk.

Üdv
Admin',
        "html" => '<p>Szia,</p>

<p>Üzleti vállalkozások regisztrációja alapban tiltva vannak a Hotbull-on. De ha szeretnél egy banner hirdetést, ami a Hotbullon és Gaypointon is fut, akkor kérj ajánlatot tőlünk.</p>

<p>Üdv<br/>
Admin</p>',
        "chat" => 'Szia,

Üzleti vállalkozások regisztrációja alapban tiltva vannak a Hotbull-on. De ha szeretnél egy banner hirdetést, ami a Hotbullon és Gaypointon is fut, akkor kérj ajánlatot tőlünk.

Üdv
Admin'
    ),

    "kiskoru" => array(
        "title" => "Feltételezett kiskorú",
        "subject" => "Kitiltásra kerültél a Hotbull Randin",
        "text" => 'Szia,

Feltételezzük, hogy nem múltál el 18 éves.
Ha viszont elmúltál, akkor azt a személyi igazolványod képével igazolhatod.
A küldött képet csakis az életkorod igazolásához használhatjuk fel, nem rögzíthetjük róla az adatokat.

Üdv
Admin',
        "html" => '<p>Szia,</p>

<p>Feltételezzük, hogy nem múltál el 18 éves.<br/>
Ha viszont elmúltál, akkor azt a személyi igazolványod képével igazolhatod.<br/>
A küldött képet csakis az életkorod igazolásához használhatjuk fel, nem rögzíthetjük róla az adatokat.</p>

<p>Üdv<br/>
Admin</p>',
        "chat" => 'Szia,

Emlékeztetőül küldjük, hogy korábban feltételeztük, hogy nem múltál el 18 éves. 
Amennyiben ennek gyanúja továbbra is fennáll, és jelentik számunkra, a kitiltásod végleges lesz.

Üdv
Admin'
    ),

    "animalszex" => array(
        "title" => "Törvényekbe és jogszabályokba ütköző tevékenység hirdetése",
        "subject" => "Kitiltásra kerültél a Hotbull Randin",
        "text" => 'Szia,

A magyar törvényeket is sértő tevékenységet hirdetsz vagy reklámozol vagy csak említesz akár virágnyelven is, 
és ezért ki lettél véglegesen tiltva a Hotbullról.

Hogy milyen tevékenységekről van szó? Hát csak néhány példa:
- animalszex
- drogok
- pedofília

Üdv
Admin',
        "html" =>'<p>Szia,</p>

<p>A magyar törvényeket is sértő tevékenységet hirdetsz vagy reklámozol vagy csak említesz akár virágnyelven is,<br/> 
és ezért ki lettél véglegesen tiltva a Hotbullról.</p>

<p>Hogy milyen tevékenységekről van szó? Hát csak néhány példa:<br/>
- animalszex<br/>
- drogok<br/>
- pedofília</p>

<p>Üdv<br/>
Admin</p>',
        "chat" => 'Szia,

A magyar törvényeket is sértő tevékenységet hirdetsz vagy reklámozol vagy csak említesz akár virágnyelven is,
és ezért ki lettél véglegesen tiltva a Hotbullról.

Hogy milyen tevékenységekről van szó? Hát csak néhány példa:
- animalszex
- drogok
- pedofília

Üdv
Admin'
    ),

    "tobb_profil" => array(
        "title" => "Több profillal rendelkezik",
        "subject" => "Kitiltásra kerültél a Hotbull Randin",
        "text" => 'Szia,

Kitiltásra kerültél, mert több profillal rendelkezel. Amennyiben válaszolsz, hogy ezt a profilt szeretnéd megtartani, akkor engedélyezzük ezt a profilt.

Üdv
Admin',
        "html" => '<p>Szia,</p>

<p>Kitiltásra kerültél, mert több profillal rendelkezel. Amennyiben válaszolsz, hogy ezt a profilt szeretnéd megtartani, akkor engedélyezzük ezt a profilt.</p>

<p>Üdv<br/>
Admin</p>',
        "chat" => 'Szia,

Emlékeztetőül küldjük, hogy ki lettél tiltva azon okból, mert több profilt használtál. 
Amennyiben ismételten megszeged a szabályokat és regisztrálsz és használsz mégegy profilt a Hotbull Randin, kitiltásod végleges lesz.

Üdv
Admin'
    ),

    "spammeles" => array(
        "title" => "Spamüzeneket küldése",
        "subject" => "Kitiltásra kerültél a Hotbull Randin",
        "text" => 'Szia,

Feltörték a fiókodat, hogy spam üzenetet küldjenek a nevedben.

Visszaengedünk, ha visszajelzed, hogy megértetted a következőket:
1. Visszaengedés után azonnal megváltoztatod a jelszót.
2. Minden honlapon más jelszót használsz.
3. Gyanús linkre nem fogsz kattintani.
4. Gyakran végzel vírusírtás a gépem.

Üdv
Admin',
        "html" => '<p>Szia,</p>

<p>Feltörték a fiókodat, hogy spam üzenetet küldjenek a nevedben.</p>

<p>Visszaengedünk, ha visszajelzed, hogy megértetted a következőket:<br/>
1. Visszaengedés után azonnal megváltoztatod a jelszót.<br/>
2. Minden honlapon más jelszót használsz.<br/>
3. Gyanús linkre nem fogsz kattintani.<br/>
4. Gyakran végzel vírusírtás a gépem.</p>

<p>Üdv<br/>
Admin</p>',
        "chat" => 'Szia,

Emlékeztetőül küldjük, hogy korábban kitiltásra kerültél, mert fiókodat feltörték és spammelésre használták.

Javasoljuk, hogy tartsd be az általános biztonsági szabályokat:
1. Minden honlapon más-más jelszóval regisztrálsz
2. Gyanús linkekre nem fogsz kattintani
3. Gyakran végzel vírusírtást a gépeden
4. Igyekezz bonyolultabb vagy hosszabb de beszédesebb jelszavakat használni - megjegyzésükhöz javasoljuk a jelszószéf alkalmazásokat, mint pl. Lastpass  

Üdv
Admin'
    ),
        "agressziv" => array(
                "title" => "Agresszív szöveg a profilban",
                "subject" => "Kitiltásra kerültél a Hotbull Randin",
                "text" => 'Szia,

Profil szöveged agresszív vagy bizonyos csoportokra sértő megjegyzést tartalmaz vagy túlságosan is negatív.

Megértjük, hogy frusztrál a társkeresés kudarcai és stresszes lehet, de nem hiszünk abban, hogy ezt jó ötlet a nyilvánossággal is megosztani.
Erőteljesen kérjük, hogy sérelmeidet ne a profilodon mutasd ki. Javasoljuk, hogy kevesebb negatív jelzőt használj, és törekedj a pozitívumok kiemelésére.

Visszaengedünk, ha válaszüzenetben megerősíted, hogy elfogadod a szabályainkat.

Üdv
Admin',
                "html" => '<p>Szia,</p>

<p>Profil szöveged agresszív vagy bizonyos csoportokra sértő megjegyzést tartalmaz vagy túlságosan is negatív.</p>

<p>Megértjük, hogy frusztrál a társkeresés kudarcai és stresszes lehet, de nem hiszünk abban, hogy ezt jó ötlet a nyilvánossággal is megosztani.<br/>
Erőteljesen kérjük, hogy sérelmeidet ne a profilodon mutasd ki. Javasoljuk, hogy kevesebb negatív jelzőt használj, és törekedj a pozitívumok kiemelésére.</p>

<p>Visszaengedünk, ha válaszüzenetben megerősíted, hogy elfogadod a szabályainkat.</p>

<p>Üdv<br/>
Admin</p>',
                "chat" => 'Szia,

Emlékeztetőül küldjük, hogy korábban kitiltásra kerültél, mert profil szöveged nagyon agresszív vagy bizonyos csoportokra sértő megjegyzést tartalmaz vagy túlságosan is negatív.

Megértjük, hogy frusztrál a társkeresés kudarcai és rendkívül stresszes, de nem hiszünk abban, hogy ezt jó ötlet a nyilvánossággal is megosztani.
Erőteljesen kérjük, hogy sérelmeidet ne a profilodon mutasd ki. Javasoljuk, hogy kevesebb negatív jelzőt használj, és törekedj a pozitívumok kiemelésére.  

Üdv
Admin'
            ),
        "viselkedes" => array(
                "title" => "Agresszív viselkedés",
                "subject" => "Kitiltásra kerültél a Hotbull Randin",
                "text" => 'Szia,

Több személy jelezte nekünk, hogy agresszíven viselkedsz a Hotbull Randin.

Javasoljuk, hogy ha visszautasítanak, akkor azt egyszerűen vedd tudomásul.
Mindenkinek joga van arra, hogy azt válasszon, akit ő szeretne.
        
Javasoljuk, hogy frusztrációdat ne vezesd le úgy, hogy sértegetsz és szavakkal bántasz másokat.
Ne adj hangot az irigységnek, vagy ne éreztesd a másikkal, hogy szerinted a másik kevesebbet ér.
Mindenkinek ugyanannyi joga van társat keresnie rendszerünkben.
        
Javasoljuk, hogy kerüld a szitkokat és a káromkodásokat, ha azokat sértés céljából használnád.

Visszaengedünk, ha válaszüzenetben megerősíted, hogy elfogadod a szabályainkat.

Üdv
Admin',
                "html" => '<p>Szia,</p>

<p>Több személy jelezte nekünk, hogy agresszíven viselkedsz a Hotbull Randin.</p>

<p>Javasoljuk, hogy ha visszautasítanak, akkor azt egyszerűen vedd tudomásul.<br/>
Mindenkinek joga van arra, hogy azt válasszon, akit ő szeretne.</p>
        
<p>Javasoljuk, hogy frusztrációdat ne vezesd le úgy, hogy sértegetsz és szavakkal bántasz másokat. <br/>
Ne adj hangot az irigységnek, vagy ne éreztesd a másikkal, hogy szerinted a másik kevesebbet ér.<br/>
Mindenkinek ugyanannyi joga van társat keresnie rendszerünkben.</p>

<p>Javasoljuk, hogy kerüld a szitkokat és a káromkodásokat, ha azokat sértés céljából használnád.</p>

<p>Visszaengedünk, ha válaszüzenetben megerősíted, hogy elfogadod a szabályainkat.</p>

<p>Üdv<br/>
Admin</p>',
                "chat" => 'Szia,

Emlékeztetőül küldjük, hogy korábban kitiltásra kerültél, mert agresszíven viselkedtél a Hotbull Randin.

Javasoljuk, hogy ha visszautasítanak, akkor azt egyszerűen vedd tudomásul.
Mindenkinek joga van arra, hogy azt válasszon, akit ő szeretne. 

Javasoljuk, hogy frusztrációdat ne vezesd le úgy, hogy sértegetsz és szavakkal bántasz másokat.
Ne adj hangot az irigységnek, vagy ne éreztesd a másikkal, hogy szerinted a másik kevesebbet ér.
Mindenkinek ugyanannyi joga van társat keresnie rendszerünkben.
        
Javasoljuk, hogy kerüld a szitkokat és a káromkodásokat, ha azokat sértés céljából használnád.

Üdv
Admin'
            ),
);

?>
