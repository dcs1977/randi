<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingLite          Version 2.1.2                    #
# Written by              AzDG (support@azdg.com)          #
# Created 03/01/03        Last Modified 04/05/03           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               inst.php                         #
# File purpose            Installation language file       #
# File created by         AzDG <support@azdg.com>          #
############################################################

define('C_HTML_DIR','ltr'); // HTML direction for this language
define('C_CHARSET', 'utf-8'); // HTML charset for this language

$y=array( // $y - installation phrases array
'<b>Please change short_open_tag variable in your php.ini file to:<br><br>short_open_tag=On</b>', //0
'OK', //1
'FALSE', //2
'INSTALLATION', //3
'Válassz nyelvet az installáláshoz', //4
'Nyelvek', //5
'Nincsenek nyelvek a Langaues/ könyvtárban. Töltsd fel õket.', //6
'Licensz elfogadása', //7
'Töltsd fel a gpl licenszet az elfogadáshoz. File:gpl.txt', //8
'Javítsd a hibákat és folytasd a folyamatot', //9
'Oldal frissitése', //10
'Nem fogadom el', //11
'Elfogadom', //12
'STEP', //13 - For example: Step 1
'A szükséges értékek ellenõrzése', //14
'Követelmények', //15
'Minimális konfiguráció', //16
'Szerver konfiguráció', //17
'Results', //18
'PHP Version', //19
'MySQL Version', //20
'Könyvtárak és fájlok ellenõrzése íráshoz', //21
'Ellenõrzés íráshoz', //22
'Javítsd a hibákat és folytasd a folyamatot', //23
'Következõ lépés', //24
'Konfiguráció készítése', //25
'Ird be az url', //26
'Ird be a path', //27
'Ird be az oldal nevét', //28
'Ird be a mysql host', //29
'Ird be mysql felhasználónevet', //30
'Ird be a mysql jelszót', //31
'Ird be a mysql database nevét', //32
'Ird be az admin loginnevét', //33
'Ird be az admin jelszót', //34
'Ird be az admin email', //35
'Ellenõrizd hogy a mezõk megfelelnek e a honlapod értékeinek', //36
'Script URL', //37
'ne legyen "/" a végén. Azt kell ideirnod amiylen urlen a script található.', //38
'Server path to script', //39
'Ne legyen  "/" a végén. Internal server path. Please ask it from your hosting if it empty.', //40
'Weblap neve', //41
'MySQL Data', //42
'MySQL hostname', //43
'Usually', //44
'MySQL Database Neve', //45
'Ha nincs kész database,csinálnod kell egyet', //46
'MySQL Felhasználónév', //47
'MySQL Jelszó', //48
'Tables elõtag', //49
'Administrator data', //50
'Admin login neve', //51
'Admin jelszó', //52
'Admin email', //53
'Admin nyelv', //54
'Administration rész nyelve', //55
'Menj vissza és töltsd ki jól a Script URL variable - ne irj iylet "/" a végére baszki', //56
'Menj vissza és töltsd ki jól a Server Path variable - ne irj iylet "/" a végére baszki', //57
'Menj vissza és töltsd ki jól a Server Path variable', //58
'Nem sikerült a szerverhez csatlakozni<br>A te mysql login or mysql jelszavad nem jó.<br>Nézd meg a config fájlban', //59
'Nem sikerült a szerverhez csatlakozni<br>Az adatbázis nem létezik<br>Vagy cserélj adatbázisnevet a configban', //60
'Létrehozzuk a MySQL tables', //61
'Table neve', //62
'Install status', //63
'Elkészült vagy már készen volt', //64
'Nem készült el', //65
'Installation results', //66
'Sikeres bazmeg', //67
'Admin oldal', //68
'Kezdõ oldal', //69
'Biztonsági notes', //70
'Töröld az install.php a szerverrõl', //71
'Kézi installation', //72
'', //73
'', //74
'', //75
'', //76
'', //77
'', //78
'', //79
'', //80
'', //81
'', //82
'', //83
'', //84
'', //85
'', //86
'', //87
'', //88
'', //89
'', //90
'', //91
'', //92
'', //93
'', //94
'', //95
'', //96
'', //97
'', //98
'' //99
);
?>