<div align=justify>
<div align=center><b class=head><?=C_SNAME?> Privacy Statement</b></div>
<br>
<br>

This statement discloses the information gathering process and the privacy and dissemination practices for the <?=C_SNAME?> Web site. <?=C_SNAME?> has created this privacy statement in order to demonstrate our firm commitment to protect your personal privacy. <br>
<br>
<?=C_SNAME?> is an Internet singles portal designed to provide an easy way to meet other singles on the Internet. Members create a profile and are then paired with a number of other members based on their pre-selected dating criteria. 
<br>
<br>

<?=C_SNAME?> is a membership-based service that not collects credit card numbers, addresses, and bank information. Any visitors to <?=C_SNAME?> may choose to browse the Site, search the profiles and/or view any articles by signing up for a free trial membership. 
<br>
<br>

<?=C_SNAME?>'s registration form requires users to provide contact information such as an email address and demographic information (such as zip code, age, or income level). <?=C_SNAME?> also occasionally runs contests and conducts online surveys that ask visitors for contact information such as name and email address and demographic information such as zip code, age, or income level. 
<br>
<br>

<?=C_SNAME?> contains links to other web sites and <?=C_SNAME?> is not responsible for the privacy practices or the content of such sites. 
<br>
<br>
<br>
<b>Private, Public and Aggregate Information obtained by <?=C_SNAME?></b><br>
<br>


PRIVATE INFORMATION is any information that allows others to contact a user other than through <?=C_SNAME?> or allows the collection of information about the user other than what is displayed on the site. Private information is gathered from free trial members and regular members. <br>
<br>


PUBLIC INFORMATION is personal information that may be displayed on the site, such as gender, age and height, but cannot be linked to a particular user. Public information is personal information such as gender, age, ethnic background, appearance, and more. This information is meant to be seen by members and visitors to the site but cannot be used to identify a specific member by name, address, or other profile information. Members can post images of themselves but no identifying information is allowed to be posted with that image. Please note that images posted can be used by <?=C_SNAME?> in ads or press releases about our service. People attending <?=C_SNAME?> events can be included in photos and images taken of such events and used in ads or press releases about our service. Posting images or attending <?=C_SNAME?> events constitutes agreement with the fair use of the images involved. 
<br>
<br>

AGGREGATE This is information we collect that is not personally identifiable, such as browser type and IP address. This information is gathered from all visitors to the site. 
<br>
<br>
<br>

<b>Use of Information Obtained by <?=C_SNAME?></b>
<br>
<br>
PRIVATE INFORMATION: We use email addresses submitted by members when forwarding anonymous emails. Responders' email addresses are not disclosed to the original sender nor is any other personally identifiable information. We may use a user's email or street address to send news regarding our site or offers we feel appropriate to our members but the user may choose not to receive email of this type by indicating upon joining up to not receive such mail or later at any time through our unsubscribe methods or by modifying their account information. Any sharing of email or street addresses with third party marketers is done only with the permission of the user by giving consent on signup or by changing their email setting preferences at a later date. Your preferences can be changed by logging onto your account, going to the "My Account" setting at the top of the page and choosing "settings". To remove yourself regarding postal third party offers, please write to the company address at the bottom of this document. <br>
<br>
Email and street addresses may also be used in private correspondence with the user for customer service issues. Financial information that is collected is used to bill the user for services. <?=C_SNAME?> utilizes a third party service provider for credit card processing to facilitate the user's transaction and the service provider does not use personal information provided by the site for its own promotional purposes. All other private information submitted by the user is confidential and will not be disclosed except as required by law or to protect members of <?=C_SNAME?>. All private information can be updated or deleted by the user at any time. 
<br>
<br>

PUBLIC INFORMATION: Public information supplied by the user is intended to be viewed by registrants and subscribers to the site. This information is used by registrants and subscribers for searching and matching purposes. 
<br>
<br>

AGGREGATE: We use your server, IP address, and browser-type related information in the general administration of our website. It is not personally identifiable information and is not released to third parties. 
<br>
<br>

Email-A-Friend: If you choose to use our referral service to tell a friend about our site, we will ask you for your friend's name and email address. We will automatically send your friend a one-time email inviting him or her to visit the site. <?=C_SNAME?> does not store this information. 
<br>
<br>
<br>



<b>Chat Rooms and Forums</b>
<br>
<br>

<?=C_SNAME?> makes chat rooms available to its users. Please remember that any information that is disclosed in these areas becomes public information and you should exercise caution when deciding to disclose your personal information online. 
<br>
<br>
<br>

Use of Cookies
<br>
<br>

We currently use cookies in our Chat areas and for our own advertising purposes. We use third-party advertising technology to serve ads when you visit our Web site and sites upon which we advertise. This technology uses information about your visits to this Web site and the sites upon which we advertise, (not including your name, address, or other personal information), to serve our ads to you. In the course of serving our advertisements to you, a unique third-party cookie may be placed or recognized on your browser. In addition, we use web beacons, provided by our ad serving partner, to help manage our online advertising. These web beacons enable our ad server to recognize a browser's cookie when a browser visits this site and to learn which banner ads bring users to our Web site. The information we collect and share through this technology is not personally identifiable. To learn more about our third party ad serving partner, cookies, and how to "opt-out," please click here. 
<br>
<br>
<br>
Change/Delete
<br>
<br>

<?=C_SNAME?> gives trial members and paid members an option for modifying or removing their information from our database. Your privacy is one of our highest priorities. The security of your personal information is important to us. When you enter sensitive information (such as credit card number and/or social security number) on our registration or order forms, we don`t store this information and encrypt that information using secure socket layer technology (SSL). [To learn more about SSL, follow this link <a href=http://www.webopedia.com/TERM/S/SSL.html>http://www.webopedia.com/TERM/S/SSL.html</a> ] 
<br>
<br>

We follow generally accepted industry standards to protect the personal information submitted to us, both during transmission and once we receive it. No method of transmission over the Internet, or method of electronic storage, is 100% secure, however. Therefore, while we strive to use commercially acceptable means to protect your personal information, we cannot guarantee its absolute security. 
<br>
<br>
We reserve the right to disclose your personally identifiable information as required by law and when we believe that disclosure is necessary to protect our rights and/or comply with a judicial proceeding, court order, or legal process served on our Web site. <br>
<br>


If we decide to change our privacy policy, we will post those changes to this privacy statement, the homepage, and other places we deem appropriate so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we disclose it. We reserve the right to modify this privacy statement at any time, so please review it frequently. If we make material changes to this policy, we will notify you here, by email, or by means of a notice on our homepage. 

<br>
<br>
If you have any questions about the Privacy Statement of <?=C_SNAME?>, please contact us using <a href=feedback.php?l=<?=LANGUAGE?>>this page</a>
</div>