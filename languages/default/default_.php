<?php 
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 03/01/03        Last Modified 05/02/03           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               en_.php                          #
# File purpose            Addition to english langfile     #
# File created by         AzDG <support@azdg.com>          #
############################################################

$wg=array(
'Unspecified', 
'Male',
'Female'
);

$wp=array(
'Unspecified',
'for romance',
'for long relationship',
'for friendship',
'for dating only'
);

// Privileged users names
$wpu=array(
'Free member', // Free user
'Silver member',
'Gold member',
'Platinum member');


$wm=array(
'Unspecified', 
'Single',
'Divorced',
'Separated',
'Widowed',
'Married'
);

$wmm=array(
'Unspecifed',
'January',
'February',
'March',
'April',
'May',
'June',
'July',
'August',
'September',
'October', // [10]
'November',
'December'
);

$wc=array(
'Unspecified',
'None',
'1',
'2',
'3',
'4',
'5 or more'
);

$wh=array(
'Unspecified',
'4\'2&quot; (127cm)',
'4\'4&quot; (132cm)',
'4\'6&quot; (137cm)',
'4\'8&quot; (142cm)',
'4\'11&quot; (152cm)',
'5\'0&quot; (155cm)',
'5\'2&quot; (157cm)',
'5\'3&quot; (160cm)',
'5\'4&quot; (163cm)',
'5\'5&quot; (166cm)', // [10]
'5\'6&quot; (168cm)',
'5\'7&quot; (171cm)',
'5\'8&quot; (173cm)',
'5\'9&quot; (175cm)',
'5\'10&quot; (178cm)',
'6\'0&quot; (183cm)',
'6\'2&quot; (188cm)',
'6\'4&quot; (193cm)',
'6\'6&quot; (198cm)',
'6\'8&quot; (203cm)', // [20]
'6\'10&quot; (208cm)',
'7\'0&quot; (213cm)'
);

$ww=array(
'Unspecified',
'80 lbs (36kg)',
'85 lbs (39kg)',
'90 lbs (41kg)',
'95 lbs (43kg)',
'100 lbs (45kg)',
'105 lbs (48kg)',
'110 lbs (50kg)',
'115 lbs (52kg)',
'120 lbs (54kg)',
'125 lbs (57kg)', // [10]
'130 lbs (59kg)',
'135 lbs (61kg)',
'140 lbs (63kg)',
'145 lbs (66kg)',
'150 lbs (68kg)',
'155 lbs (70kg)',
'160 lbs (73kg)',
'165 lbs (75kg)',
'170 lbs (77kg)',
'175 lbs (79kg)', // [20]
'180 lbs (82kg)',
'185 lbs (84kg)',
'190 lbs (86kg)',
'195 lbs (89kg)',
'200 lbs (91kg)',
'205 lbs (93kg)',
'210 lbs (95kg)',
'215 lbs (97kg)',
'220 lbs (100kg)',
'225 lbs (102kg)', // [30]
'230 lbs (104kg)',
'235 lbs (107kg)',
'240 lbs (109kg)',
'245 lbs (111kg)',
'250 lbs (113kg)',
'255 lbs (116kg)',
'260 lbs (118kg)',
'265 lbs (120kg)',
'270 lbs (122kg)',
'275 lbs (125kg)', // [40]
'280 lbs (127kg)',
'285 lbs (129kg)',
'290 lbs (131kg)',
'295 lbs (134kg)',
'300 lbs (136kg)'
);

$whc=array(
'Unspecified',
'Black',
'Blonde',
'Brown',
'Brunette',
'Chestnut',
'Auburn',
'Dark-blonde',
'Golden',
'Red',
'Gray', // [10]
'Silver',
'White',
'Bald',
'Other'
);

$we=array(
'Unspecified',
'Black',
'Brown',
'Blue',
'Green',
'Gray',
'Other'
);

$wet=array(
'Unspecified',
'White/Caucasion',
'Hispanic',
'Asian',
'African American/Black',
'Mixed',
'Other'
);

$wr=array(
'Unspecified',
'Agnostic',
'Atheist',
'Baptist',
'Belief in God',
'Buddhist',
'Catholic',
'Christian',
'Eastern Orthodox',
'Humanist',
'Judaism', // [10]
'Lutheran',
'Mormon (LDS)',
'Muslim',
'New Age',
'Non-believer',
'Protestant',
'Other'
);

$ws=array(
'Unspecified',
'Non-Smoker',
'Occasionally',
'Regularly'
);

$wd=array(
'Unspecified',
'Never drink',
'Occasionally',
'Frequently'
);

$wed=array(
'Unspecified',
'High school',
'College student',
'University student',
'College graduate',
'Post graduate'
);

$wu=array(
'Unspecified',
'Banner Ads',
'Newspaper',
'Television',
'Search engine',
'Friend recommendation',
'Other'
);

$whr=array(
'Unspecified',
'Capricorn',
'Aquarius',
'Pisces',
'Aries',
'Taurus',
'Gemini',
'Cancer',
'Leo',
'Virgo',
'Libra', // [10]
'Scorpio',
'Sagittarius'
);

$wsb=array(
'Date descending',
'Date ascending',
'First Name descending',
'First Name ascending',
'Age descending',
'Age ascending',
'Height descending',
'Height ascending',
'Weight descending',
'Weight ascending'
);
/* 
$wsb only for search - you can`t add new or remove - 
only translate!
*/

$wrg=array(
'Any period',
'last 7 days',
'last month',
'last 3 month',
'last year'
);

$wrgv=array(
'', // Not required - enter this value for All periods
'7', 
'31',
'92',
'365'
);


$wst=array(
'All users',
'Waiting check from mail',
'Waiting registration check',
'Waiting update check',
'Waiting admin allow to remove',
'Suspended profile',
'Active user'
);
 
# Languages - maximal 63 values!!! 
$wlng=array(
'', // Empty Value required
'English',
'German',
'French',
'Spanish',
'Arabic',
'Italian',
'Chinese',
'Bengali',
'Hindi/Urdu',
'Portuguese',
'Russian',
'Japanese',
'Dutch',
'Greek',
'Korean'
);

$wcr=array(
' Unspecified', // First space is required for sorting
'Afghanistan',
'Albania',
'Algeria',
'Andorra',
'Angola',
'Antigua and Barbuda',
'Argentina', 
'Armenia',
'Australia',
'Austria',
'Azerbaijan',
'Bahamas',
'Bahrain',
'Bangladesh',
'Barbados',
'Belarus', 
'Belgium',
'Bolivia',
'Bosnia and Herzegovina',
'Botswana',
'Brazil',
'Brunei Darussalam',
'Bulgaria',
'Burundi',
'Cambodia',
'Cameroon',
'Canada',
'Central African Republic',
'Chad',
'Chile',
'China',
'Colombia',
'Congo',
'Costa Rica',
'Ivory Coast',
'Croatia',
'Cuba',
'Cyprus',
'Czech Republic',
'Denmark',
'Dominican Republic',
'Ecuador',
'Egypt',
'El Salvador',
'Equatorial Guinea',
'Estonia',
'Ethiopia',
'Fiji',
'Finland',
'France',
'Gabon',
'Gambia',
'Georgia',
'Germany', 
'Ghana',
'Gibraltar',
'Greece',
'Greenland',
'Grenada',
'Guadeloupe',
'Guatemala',
'Guinea-Bissau',
'Guinea', 
'Guyana',
'Haiti',
'Honduras',
'Hong Kong',
'Hungary',
'Iceland',
'India',
'Indonesia',
'Iran', 
'Iraq',
'Ireland',
'Israel',
'Italy',
'Jamaica',
'Japan',
'Jordan',
'Kazakhstan',
'Kenya',
'North Korea',
'South Korea',
'Kuwait',
'Kyrgyzstan',
'Laos',
'Latvia',
'Lebanon',
'Lesotho',
'Liberia',
'Libya',
'Liechtenstein',
'Lithuania',
'Luxembourg',
'Macedonia',
'Madagascar',
'Malaysia',
'Mali', 
'Malta',
'Mauritania',
'Mexico',
'Moldova',
'Monaco', 
'Mongolia',
'Morocco',
'Mozambique',
'Namibia',
'Nepal',
'Netherlands', 
'New Zealand',
'Nicaragua',
'Nigeria',
'Niger',
'Norway',
'Oman', 
'Pakistan',
'Panama',
'Papua New Guinea',
'Paraguay',
'Peru',
'Philippines',
'Poland',
'Portugal', 
'Puerto Rico',
'Qatar',
'Romania',
'Russian Federation',
'Rwanda',
'Samoa',
'San Marino',
'Saudi Arabia',
'Senegal',
'Sierra Leone',
'Singapore',
'Slovak Republic',
'Slovenia', 
'Solomon Islands',
'Somalia',
'South Africa',
'Spain',
'Sri Lanka',
'Sudan',
'Swaziland',
'Sweden',
'Switzerland',
'Syria',
'Taiwan',
'Tajikistan',
'Tanzania',
'Thailand',
'Tunisia',
'Turkey',
'Turkmenistan',
'Uganda',
'Ukraine', 
'United Arab Emirates',
'United Kingdom',
'United States',
'Uruguay',
'Uzbekistan',
'Vatican City State',
'Venezuela',
'Viet Nam',
'Yemen',
'Yugoslavia',
'Zaire',
'Zambia',
'Zimbabwe',
'Trinidad and Tobago',
'Serbia',
'Montenegro'
);
// No more 255 entries in this list!!!
// You can change this list to cities of your country
// Be careful!

$wor=array(
'Unspecified',
'Heterosexual',
'Homosexual',
'Bisexual',
'Transsexual');

$wbt=array(
'Unspecified',
'Ample',
'Athletic',
'Average',
'Beautiful',
'Cuddly',
'Curvy',
'Full-figured',
'Heavyset',
'Large/Broad',
'Lean/Slender',
'Muscular',
'Petite',
'Slim',
'Stocky',
'Very Cuddly');

$win=array(
'Unspecified',
'Animals/Pets',
'Arts / Crafts',
'Astrology / New Age',
'Athletics',
'Baseball / Softball',
'Billiards / Pool / Darts',
'Boxing / Wrestling',
'Cars / Motorcycles',
'Computers / Internet',
'Cooking',
'Cricket',
'Cycling',
'Dancing',
'DIY',
'Food and Wine',
'Football / Soccer / Rugby',
'Gardening',
'Golf',
'Gym / Aerobics / Keep fit',
'Hiking / Camping',
'Ice / Snow Sports',
'Literature / History',
'Martial Arts',
'Motor Racing',
'Mountaineering',
'Movies / Cinema',
'Museums / Galleries',
'Music - Alternative',
'Music - Blues/Jazz',
'Music - Christian / Gospel',
'Music - Classical / Opera',
'Music - Country',
'Music - Dance / Electronic',
'Music - Latin',
'Music - New Age',
'Music - Pop / R&B',
'Music - Rock',
'Music - World',
'Nature',
'Nightclubs / Clubs',
'Outdoors',
'Politics',
'Reading',
'Religion',
'Restaurants',
'Sailing / Boating',
'Shopping',
'Singing / Playing Instrument',
'Sports (Playing)',
'Sports (Watching)',
'Television / Radio',
'Tennis / Racket Sports',
'Theatre / Ballet',
'Travel / Sightseeing',
'Volleyball / Basketball',
'Volunteer / Charity',
'Water Sports',
'Yoga / Meditation');

// Forum sort by
$wfs=array('Last post date','Author name','Number of answers','Thread name');

// Access for private photos
$wpp=array('All users','Only users in "My Favorites"','All users by password','Only users in "My Favorites" by password');
?>