<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 03/01/03        Last Modified 05/02/03           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               en.php                           #
# File purpose            English language file            #
# File created by         AzDG <support@azdg.com>          #
############################################################

define('C_HTML_DIR','ltr'); // HTML direction for this language
define('C_CHARSET', 'iso-8859-1'); // HTML charset for this language

# Do not insert here ' or " char due possible error
define('C_DEFAULT_TITLE','Dating website');
define('C_DEFAULT_KEYWORDS','dating,matchmaker,women,love,wedding,men,personals,speed dating,looking for women,looking for men');
define('C_DEFAULT_DESCRIPTION','Dating website - great way to find new friends or partners, for fun, dating and long term relationships');

# The same variables in new format
$C_HTML_DIR = 'ltr';
$C_CHARSET = 'iso-8859-1';

# Don`t change following value
$C_LANGUAGE = '';

### Rules for translation 
### 1. Be attentive while translating - do not use ' { } characters
###    You can use their html equivalents - &#39; &#123; &#125;
### 2. Don`t translate {some_number} templates - you can only replace it - 
###    {0},{1}... - is not number - it templates
###################################

$w=array(
'<font color=red size=3>*</font>', //0 - Symbol for requirement field
'Security error - #', //1
'This email account is already in our database. We only allow one user to be registered per mail account.', //2
'Wrong first (nick)name. First (nick)name must be {0} - {1} chars', //3 - Don`t change {0} and {1} - See rule 2 !!!
'Wrong last (nick)name. Last (nick)name must be {0} - {1} chars', //4
'Wrong Birthday', //5
'Wrong password. Password must be {0} - {1} chars', //6
'Please select your gender', //7
'Please select seeking gender', //8
'Enter your Type of Relationship', //9
'Please select your country', //10
'Incorrect or empty email', //11
'Incorrect webpage', //12
'Wrong ICQ number', //13
'Wrong AIM', //14
'Enter your phone number', //15
'Enter your city', //16
'Select your marital status', //17
'Please choose the answer about your children', //18
'Select your height', //19
'Select your weight', //20
'Select seeking height', //21
'Select seeking weight', //22
'Select your hair color', //23
'Select your eyes color', //24
'Select your ethnicity', //25
'Select your religion', //26
'Select seeking ethnicity', //27
'Select seeking religion', //28
'Select about smoking', //29
'Select about drinking', //30
'Select about your education', //31
'Write your work', //32
'Enter seeking age', //33
'Select how you did find us', //34
'Please enter your hobby', //35
'Wrong hobby field. Hobby may not be bigger than {0} symbols', //36
'Wrong hobby word size. Hobby word size may not be bigger than {0} symbols', //37
'Please write something about yourself', //38
'Wrong description field. Description may not be bigger than {0} symbols', //39
'Wrong description word size. Description word size may not be bigger than {0} symbols', //40
'Your photo is required!', //41
'Congratulations! <br>Your activation code has been sent to your email. <br>You have to confirm your registration by clicking on the link in your email!', //42 - Message after registration if confirmation is needed by email
'Confirm your registration', //43 - Confirm mail subject
'Thanks for your registration at '.C_SNAME.'.
Please click here to confirm your registration:

', //44
'Thanks for your registration. Your profile will be approved in short time. Please visit later...', //45 - Message after registering if admin allowing is needed
'Congratulations! <br>Your profile has been added to our database!<br><br>Your login is - ', //46
'<br>Your password:', //47
'Please retype your password', //48
'The passwords are not identical', //49
'Register user', //50
'Your first (nick)name', //51
'chars', //52
'Your last (nick)name', //53
'Password', //54
'Retype password', //55
'Birthday', //56
'Gender', //57
'Type of Relationship', //58
'Country', //59
'Email', //60
'Webpage', //61
'ICQ', //62
'AIM', //63
'Phone', //64
'City', //65
'Marital status', //66
'Children', //67
'Height', //68
'Weight', //69
'Hair color', //70
'Eye color', //71
'Ethnicity', //72
'Religion', //73
'Smoking', //74
'Drinking', //75
'Education', //76
'Job', //77
'Hobby', //78
'Describe yourself and the kind of person you are looking for as a potential partner.', //79
'Seeking gender', //80
'Seeking ethnicity', //81
'Seeking religion', //82
'Seeking age', //83
'Seeking height', //84
'Seeking weight', //85
'How did you find us?', //86
'Photo', //87
'Home', //88
'Register', //89
'Members area', //90
'Search', //91
'Feedback', //92
'FAQ', //93
'Statistics', //94
'Members menu', //95
'View messages', //96
'My Favorites', //97
'My profile', //98
'Change profile', //99
'Change password', //100
'Remove profile', //101
'Exit', //102
'Processing time:', //103
'sec.', //104
'Users online:', //105
'Guests online:', //106
'Powered by <a href="http://www.azdg.com" target="_blank" class="desc">AzDG</a>', //107 - Don`t change link - only for translate - read GPL!!!
'Only registered users have access to the advanced search option', //108
'Sorry, "Age From" must be smaller than "Age to"', //109
'Search doesn`t return any matches', //110
'None', //111 Picture available?
'Yes', //112 Picture available?
'Can`t connect to server<br>Your mysql login or mysql password is wrong.<br>Check it in config file', //113
'Can`t connect to server<br>Database doesn`t exist<br>Or Change Database name in config', //114
'Pages :', //115
'Search results', //116
'Total : ', //117 
'Username', //118
'Purposes', //119
'Age', //120
'Country', //121
'City', //122
'Last access', //123
'Registration date', //124
'Advanced Search', //125
'User ID#', //126
'First (nick)name', //127
'Last (nick)name', //128
'Sign of the Zodiac', //129
'Height', //130
'Weight', //131
'Gender', //132
'Type of Relationship', //133
'Marital status', //134
'Children', //135
'Hair color', //136
'Eyes color', //137
'Ethnicity', //138
'Religion', //139
'Smoke', //140
'Drink', //141
'Education', //142
'Search users with', //143
'Webpage', //144
'ICQ', //145
'AIM', //146
'Phone', //147
'Registered in ', //148
'Sort results by', //149
'Results on page', //150
'Simple Search', //151
'Access closed for non members', //152
'Access closed for reporting bad profiles', //153
'User has already been reported and will be checked by admin in short time', //154
'Thanks, user has been reported and will be checked by admin in short time', //155
'Access closed for using favorites', //156
'User is already in your favorites list', //157
'Thanks, user has been successfully added to your favorites', //158
'Your profile has been successfully added for admin check!', //159
'Your profile has been successfully added to our database', //160
'Profile activation error. Maybe it is already active', //161
'FAQ database is empty', //162
'FAQ answer#', //163
'All fields must be filled out', //164
'Your message has been successfully sent', //165
'Please enter your subject', //166
'Please enter your message', //167
'Subject', //168
'Message', //169
'Send message', //170
'For members', //171
'Login ID', //172
'Forgot password', //173
'Recommend Us', //174
'Friend-{0} email', //175
'Today Birthdays', //176
'No birthdays', //177
'Welcome to our AzDGDating Site', //178 Welcome message header
'AzDGDating - is a great way to find new friends or partners, for fun, dating and long term relationships. Meeting and socializing with people is both fun and safe. Common sense precautions should be taken however when arranging to meet anyone face to face for the first time.<br><br>You can also find new friends through our own privately hosted email system. This lets you communicate with other members to find out more about each other and develop a relationship.<br>', //179 Welcome message
'Last {0} registered users', //180
'Quick search', //181
'Adv. search', //182
'Photo of the day', //183
'Simple Statistics', //184
'Your ID must be numeric', //185
'Incorrect Login ID# or password', //186
'Access closed for sending messages to email', //187
'Send message to email to user ID#', //188
'No users online', //189
'Recommend page unavailable', //190
'Greetings from {0}', //191
'Hello from {0}!

How are you:)

Please visit this great dating site:
{1}', //192
'Please write correct friend#{0} email', //193
'Please enter your name and e-mail', //194
'Your password from {0}', //195
'This account is deactivated or doesn`t exist in our database.<br>Please use our feedback link to inform us about this problem from feedback. Include your ID please.', //196
'Hello!

Your login ID#:{0}
Your Password:{1}

_________________________
{2}', //197
'Your password has been successfully sent to your email.', //198
'Please enter your ID', //199
'Send password', //200
'Access closed for sending messages', //201
'Send message to user ID#', //202
'Notify me when user will read the message', //203
'No such user in our database', //204
'Statistics not available', //205
'Such active ID does not exist', //206
'Profile ID#', //207
'User first (nick)name', //208
'User last (nick)name', //209
'Birthday', //210
'Email', //211
'Message from AzDGDating', //212 - Subject for email
'Job', //213
'Hobby', //214
'About', //215
'Popularity', //216
'Send email', //217
'Report this profile', //218
'Add to my favorites', //219
'Either there was no file uploaded, <br>or the file you tried to upload was bigger than the {0} Kb limit. Your file is {1} Kb', //220
'The file you tried to upload was in width bigger than the {0} px or in height bigger than the {1} px limit.', //221
'The type of file that you tried to upload was incorrect. Your type - ', //222
'(Max. {0} Kb)', //223
'Statistics by Countries', //224
'You have no messages', //225
'Total messages - ', //226
'Num', //227 Number
'From', //228
'Date', //229
'Del', //230 Delete
'<sup>New</sup>', //231 New messages
'Delete selected messages', //232
'Message from - ', //233
'Reply', //234
'Hello, You wrote {0}:
{1}
_________________', //235 Reply to message {0} - date, {1} - message
'Your message has been read', //236
'Your message:<br><br><span class=dat>{0}</span><br><br>has been read by {1} [ID#{2}] on {3}', //237 {0} - message, {1} - Username, {2} - UserID, {3} - Date and Time
'{0} messages successfully deleted!', //238
'Please enter old password', //239
'Please enter new password', //240
'Please retype new password', //241
'Change password', //242
'Old password', //243
'New password', //244
'Retype new password', //245
'You have no users in your favorites list', //246
'Date of addition', //247
'Delete selected users', //248
'Are you sure that you want to delete your own profile?<br>All of your messages and pictures will be removed from our database.', //249
'User with ID#={0} has been successfully deleted from our database', //250
'Your profile will be removed after admin secure check', //251
'{0} users successfully removed from your favorites!', //252
'No identical passwords or password content incorrect chars', //253
'You have no access to change password', //254
'Incorrect old password. Please go back and retype it!', //255
'Password has been successfully changed!', //256
'You can not remove all photos', //257
'Your profile has been successfully changed', //258
' - Delete picture', //259
'Your session has been ended. You can close your browser', //260
'Flag images not available', //261
'Languages', //262
'Enter', //263
'Login [3-16 chars [A-Za-z0-9_]]', //264
'Login', //265
'Your login must consist of 3-16 chars and only A-Za-z0-9_ chars is available', //266
'This login already exists in our database. Please select another!', //267
'Total users - {0}', //268
'You should be a privileged User to use this feature.<br><br>You can purchase from <a href="'.C_URL.'/members/index.php?l=default&a=r" class=head>here</a>', //269 change l=default to l=this_language_name
'User type', //270
'Period', //271
'Search results position', //272
'Price', //273
'month', //274
'Purchase last date', //275
'Higher than', //276
'Purchase', //277
'Purchase with', //278
'PayPal', //279
'Thanks for your registration. Payment has been successfully sent and will be checked by admin in short time.', //280
'Incorrect error. Please try again, or contact with admin!', //281
'Send congratulation letter about activating privileges', //282
'User type has successfully changed.', //283
'Email with congratulations has been sent to user.', //284
'ZIP',// 285 Zip code
'Congratulations, 

Your status has been changed to {0}. This privileges will be available in next {1} month.

Now you can check your messages in your box.

__________________________________
{2}', //286
'Congratulations', //287
'ZIP code must be numeric', //288
'Keywords', //289
'We are sorry, but the following error occurred:', //290
'Hello Dear customer!
Your profile has been successfully added to {2}.

Please remember that:
Your Login:{0}
Your Password:{1}

_________________________
{2}', //291
'You can upload a sound file with your speech', //292
'Send ecard', //293
'Confirmed from', //294
'Unknown', //295
'Sound', //296
'Delete', //297
'Send ecard to', //298
'Sorry, but ecards are not available', //299
'Please select image for card', //300
'Virtual card from {0}', //301
'Hello {0}

Someone has sent you a virtual card. 
To view this card click on the following link:
{1} 

Your card will be accessible during {2} days
', //302
'Click here', //303
'This virtual card has expired or does not exist', //304
'{0} Virtual Card', //305 {0} - site name
'Affiliate', //306
'How to use affiliates?', //307
'Just invite your friends from following links:
<br>
{0} - Main page<br>
{1} - Search Profiles<br>
{2} - Look to my profile<br>
<br>
You can create affiliate link from any page. Just add aff={3} variable to your url.<br><br>Example:<br>{4}<br><br>
You can get:<br> 
', //308
'<li>{0}$ for each {1} user</li>', //309
'Click here for view my Info', //310
'Note: Minimum payment for money transfer - {0}$', //311
'Affiliate Statistics', //312
'Invited user', //313
'Money ($)', //314
'<br>You can write to us from feedback form for money reception. Please include your affiliate ID number [ID#{0}].', //315
'2CheckOut',  //316
/* Version 1.3.1 */
'Ban list', //317
'You have no users in your ban list', //318
'Add to ban list', //319
'User is already in your ban list', //320
'Access closed for using ban list', //321
'Thanks, user has been successfully added to your ban list', //322
'{0} users successfully removed from your ban list!', //323
'You can`t send any message to this user, because you are banned', //324
'You have banned all users. You can unban their from affiliate.', //325
'Ban all users', //326
'Unban all users', //327
'All users are already banned', //328
'All users have been banned', //329
'All users have been unbanned', //330
'You have {0} new messages', //331
'Someone has added you in favorites', //332
'{0} add your profile into own Favorites list.', //333 {0} - user name
'I am a', //334
'Seeking a', //335
'Previous', //336 
'Next', //337 
'Last {0} registered users with photo', //338
'Chat', //339
'Sorry - Chat available only for registered users', //340
'The {0} most popular users', //341
'Most Popular Men', //342
'Most Popular Women', //343
'News and Announcements', //344
'News while is not present', //345
'News archive', //346
'Not available', //347
'<br><sup>Not visible in profile</sup>', //348
'Re:', //349 Answer chars, for ex. Re:Subject
'Online', //350 Online status - you can insert picture here
'Offline', //351 Offline status - you can insert picture here
'User online status', //352
'Forum', //353
'Guest', //354
'Access denied', //355
'Create New Thread', //356
'Title', //357
'Message', //358
'Please enter title', //359
'Please enter message', //360
'Guest', //361
'Thanks!<br>New Thread has been successfully created.', //362
'Post Reply', //363
'Thanks!<br>Your message has been successfully posted', //364
'Profile Views', //365
'Delete message', //366
'Preview Ecard', //367
'No access to this area', //368
'You must be at least a {0} to access this page', //369
'Nobody', //370
'Remove Thread', //371
'This thread and thread`s messages has been removed!', //372
'This message has been removed', //373
'Status', //374
'There are no links in the database', //375
'Links', //376
'Please enter your address', //377
'Address', //378
'More results', //379
'You have`nt access to this website because your IP has been banned.', //380
'Welcome back {0}', //381 {0} Name of user
'Known languages', //382
'Please select known languages', //383
'You must be at least a {0} to view this data', //384
'Your limit of messages has finished. Your user`s type: "{0}" can send {1} messages per 24 hours.', //385
'Subscribed E-mail (An e-mail requiring a verifying reply will be sent to this e-mail address)', //386
'Subscription', //387
'Subscribe', //388
'Hello!

Somebody from IP {0} has subscribed your email {1} for receiving new profiles.

If you didn`t make it, please do nothing - your email will remove automatically after {2} days.

If you wish to receive new profiles to this email, please confirm your registration. Just follow this link:

{3}

Click this link, or copy and paste the address into your browser.
', //389
'{0} subscription', //390
'You have been successfully subscribed', //391
'Thanks!

You have been successfully subscribed. 
Your email: {0} has been added to our database.
You will receive new profiles to email periodically.

Anytime you can unsubscribe by clicking to unsubscribe link in each email from us.
____________________
{1}', //392
'Photo doesn`t exist', //393
'Click here for more info', //394
'This ID or code doesn`t exist in database, or you have already unsubscribed.', //395
'Your email: {0} has been successfully unsubscribed.', //396
'Hello!

Your email: {0} has been successfully unsubscribed from {1}.
', //397
'Send a wink', //398
'Wink has been sent to user', //399
'You have received a wink', //400
'You have received a wink. This user has shown an interest in you.', //401
'All users', //402
'Please describe exactly, why do you report this profile?', //403
'<br><sup>Don`t visible in profile,<br>except for {0} and higher</sup>', //404
'New user has been registered', //405
'New user has been registered on {0}.

His profile availables here:
{1}
', //406
'New user has been registered on {0}.

You should check him from admin area', //407
'Read next unread', //408
'You haven`t unread messages', //409
'Users Plans', //410
'May to see in the User Profile', //411
'Access to pages', //412
'Available actions', //413
'Using another email in feedback', //414
'Using Report Profiles page', //415
'Using View Profile page', //416
'Using Email to User page', //417
'Using another email in Email to User page', //418
'Change design', //419
'Happy Birthday {0} {1}', //420 {0} - Firstname, {1} - Lastname
'Happy Birthday {0} {1}!!! :)

"Happy birthday to you
Happy birthday to you
Happy birthday dear {0}
Happy birthday to you :)"

We hope you had a lovely day with lots and lots of presents. :)

___________________________
Your`s {2}
{3}
', //421 {0} - Firstname, {1} - Lastname, {2} - Site name, {3} - URL
'Options', //422Vote
'Change Options', //423
'Rate this profile<br>(1 - Worse, 10 - Better)', //424
'Sorry, you can not rate itself', //425
'Sorry, you`ve already rated this profile', //426
'Thanks, your rate has been added', //427
'Who seen profile', //428
'Nobody seen this profile yet', //429
'Vote', //430
'View Results', //431
'Please select anything from options', //432
'Sorry, you have already voted in last {0} hours!', //433
'No such vote', //434
'Thanks, your vote has been succesfully received!', //435
'Not yet votes available', //436
'Total {0} votes have been made', //437
'votes', //438
'Edit Subscription', //439
'No such subscription user with this id and code', //440
'Hello!

Somebody from IP {0} changed subscribed data.

If you didn`t make it, please contact with administrator {1}.', //441
'Subscription data has been successfully changed', //442
'Last accessed page', //443
'Seeking country', //444
'Seeking marital status', //445
'Seeking children', //446
'Seeking hair color', //447
'Seeking eyes color', //448
'Seeking smoking', //449
'Seeking drinking', //450
'Seeking education', //451
'Please select', //452
'Matchmaker', //453
'Matchmaker is a most easy way to connect with singles who are an ideal match in age, appearance and hoping to meet you<br>You may to search perfect match by checking or unchecking one or more options specified in your profile. Checking all parameters will give better match, but may`nt return any results.', //454
'Find my perfect match', //455
'My data', //456
'Match data', //457
'Our purposes should coincided', //458
'My Language', //459
'Threads', //460
'Posts', //461
'Currently active users', //462
'by', //463
'in', //464
'Thread', //465
'Thread starter', //466
'Replies', //467
'Views', //468
'Last post', //469
'Author', //470
'Edit Thread', //471
'Moderators', //472
'Lock Thread', //473
'Unlock Thread', //474
'Thread has been locked', //475
'Thread has been unlocked', //476
'Edit Thread', //477
'Thread has been changed', //478
'Edit message', //479
'Thread message has been changed', //480
'Step', //481
'Large photo', //482
'Search by user`s types in advanced search', //483
'Rate profile', //484
'Who seen my profile', //485
'Who seen another users profile', //486
'How many messages per day user may to send', //487
'How many photos may upload user', //488
'Add Comments', //489
'Thanks!<br><br>Your comment has been successfully added.', //490
'No comments available.', //491
'Comments', //492
'User replied:', //493
'WARNING: This will permanently remove the selected comment. Are you sure you wish to do this?', //494
'Selected comment has been successfully removed.', //495
'Thanks. Your reply to comment has been successfully added to database.', //496
'Please enter chars from Verification field correctly.', //497
'Verification', //498
'Account is inactive. You should to confirm your registration from email.',//499
'Account is inactive, in Pending status. It is waiting administrator approval. Please wait, you`ll receive an email when your profile is approved.',//500
'Account is inactive, in Pending status. It is waiting administrator approval after profile update. ',//501
'Account is inactive, in Removed status. It is waiting administrator approval for removal. ',//502
'Account is inactive, in Error status. You`re registration is not complete - please try to register again',//503
'Account is inactive, in Error status. Please contact Administrator using Feedback form.',//504
'Please wait while image is loading...',//505
'Total photos',//506
'See another photos',//507
'This action is deprecated',//508
'Online users only',//509
'Visited profiles',//510
'No Profile has been seen by you in last {0} days',//511
'Top men with photo',//512
'Top women with photo',//513
'Send message to',//514
'Send mail to', //515
'Expiry date',//516
'Participate in Rating?',//517
'Options has been successfully changed!',//518
'Administrator',//519
'Edit ecard',//520
'You may describe a reason or leave this field empty',//521
'{0} photographs aren`t shown since they have not ben checked by moderators. Please come back soon...',//522
'Please wait while page is reloading...',//523
'Orientation',//524
'Body type',//525
'Interests',//526
'States/Province',//527
'Your access level "{0}" can`t see more than {1} search result`s pages!',//528
'Any',//529
'Search error occured.<BR>Possible reasons are:<BR>- Incorrect search id<BR>- You haven`t rights to search using required data<BR>- Search cache corrupt or expired.',//530
'Please fill out the following field:',//531
'Photos unavailable',//532
'Have {0} photos',//533
'Additional information',//534
'Coupons',//535
'Enter coupon',//536
'Congratulations, 

Your status has been changed to {0}. 

These privileges will be available in current {1} days.
__________________________________
{2}', //537
'New {0} status is received on {1}', //538
'New privilegied user with IP:{0} has been registered in {1}
									User type: [{2}]
									User ID: {3}', //539
'New privilegied user has been registered!', //540
'Thanks! Your coupon code has been successfully accepted! <BR>Now you have {0} status!',//541
'Coupon code hasn`t been accepted. Please check it carefully!',//542
'Minimal number of uploaded photos: ',//543
'{0} messages',//544
'Time difference in hours (for ex: <B>-3</B> or <B>+2</B>)',//545
'Current website date: <B>{0}</B>',//546
'My Current Date (using Time Difference value): <B>{0}</B>',//547
'I want to be a privileged user',//548
'You haven`t enough privileges to create new threads in this forum.',//549
'You haven`t enough privileges to reply in this forum.',//550
'Address book is empty',//551
'Or',//552
'And',//553
'Open profile',//554
'Please check at least one profile!',//555
'With selected:',//556
'Address Book',//557
'Total contacts:',//558
'Your profile has been removed from {0}', //559
'Your profile has been removed from {0}.

Possible reasons are:
- Profile removed per your request
- Profile hasn`t been filled correctly
- Website agreement has been violated

--
Best regards,
Administration of the {0}
{1}', //560
'Payment membership privileges',//561
'Privileged users timelife (in month)',//562
'Prices ({0})',//563
'How many search pages can user see (0 - means unlimited)',//564
'Purchase link',//565
'You may purchase the Privileged Package of your choice to get Privileged rights immediately!',//566
'Can see other comments?',//567
'Can see own comments?',//568
'Can reply to comments',//569
'Can add new comments',//570
'Can remove comments',//571
'Can see in search results',//572
'Can see visited profiles',//573
'Can see random templates',//574
'Can not be in rating',//575
'Can see the day of birthday',//576
'Can use filter by genders in online users page',//577
'Can use filter by user type in online users page',//578
'Can use filter by picture in online users page',//579
'Allowable fields to search in the Simple Search',//580
'Allowable fields to search in the Advanced Search',//581
'Can use smiles and special tags in messages?',//582
'Can see user photo in the comments?',//583
'Can see number of search results?',//584
'Make sticky',//585
'Cancel sticky',//586
'Thread has been make sticky',//587
'Sticky property for this thread has been canceled',//588
'This thread is sticky',//589
'Search by forum',//590
'All forums',//591
'Search posts for',//592
'Search in and Show results as',//593
'Posts',//594 Search in 
'Threads',//595
'No forum available for search',//596
'You can`t search more often than once in {0} seconds. <br><br>Try again after {1} seconds.',//597
'Slideshow',//598 
'Ascending',//599
'Descending',//600
'Forum Options',//601
'Allow BB codes and smiles',//602
'All',//603
'Posts',//604 Posts: Num
'Signature',//605
'Current signature',//606
'Change Signature',//607
'Signature has been successfully changed',//608
'Top users by forum posts',//609
'Forum`s Top',//610
'Top users by forum threads',//611
'Top threads by forums',//612
'Signature deprecated by moderator',//613
'No avatar',//614
'Select from list',//615
'Upload your avatar',//616
'Your avatar must be no bigger than {0} pixels by {1} pixels in size. Avatars must be no larger than {2} KB. The following file types are allowed: {3}',//617
'Avatar has been successfully changed',//618
'Keyword or Author field too long',//619
'Your search has been completed, please stand by as we take you to the search results.<br><br>Please wait while we transfer you...<br><br><a href="{0}"{1}>(Or click here if you do not wish to wait)</a>',//620
'Show the link to this post',//621
'Manually copy the direct link to this post below to store the link in your computer`s clipboard',//622
'WARNING: This will permanently remove the selected. Are you sure you wish to do this?',//623
'Remove the selected',//624
'Check/Uncheck all',//625
'Selected has been successfully removed',//626
'You have filled {0}% of your profile. {1}Click here{2} to fill all fields',//627
'You haven`t uploaded photo yet. {0}Click here{1} to upload photo',//628
'You have written very little information about itself in the "About" field. {0}Click here{1} to fill out this field more thoroughly.',//629
'<br><br>Some notice messages should be rewritten or reset. Continue with any link to update notices.',//630
'I agree with this `Terms of Use Agreement`',//631
'You should be agree with the `Terms of Use Agreement`',//632
'Testimonials',//633
'Add new testimonial',//634
'Enter your testimonial here',//635
'Thanks!<br><br>Your testimonial has been successfully added.',//636
'No testimonials available!',//637
'Enable sound when new message arrived',//638
'View Name',//639
'To create new view just enter name here, and define search parameters.',//640
'Views',//641 ` Search views
'View name',//642
'No view available',//643
'Create new View',//644
'Articles',//645
'No article available',//646
'My Blog',//647
'Add new entry',//648
'There are {0} entries total',//649
'Make public',//650
'New blog entry has been added',//651
'No blogs available<br><br> {0} Create new {1}',//652
'Creation date',//653
'Last update',//654
'Public',//655
'Private',//656
'Make private',//657
'Edit blog entry',//658
'Please select at least one entry!',//659
'Selected entry`s status has been changed to public',//660
'Selected entry`s status has been changed to private',//661
'Blog has been successfully changed',//662
'Click here to see user`s Favorites list',//663
'Favorites list',//664
'No user in this Favorites list available',//665
'You haven`t access to current user Favorites list - you should be present in the him(her) Favorites to access it',//666
'
---------------------
This email has been sent from {0} by online mail form',//667
'Video',//668
'Sound or Video',//669
'You can upload your video',//670
'You can upload your sound or video',//671
'Move Thread',//672
'Private photos',//673
'You can upload maximum <b>{0}</b> private photos',//674
'No private photos available<br><br> {0} Create new {1}',//675
'Upload new private photo',//676
'Description',//677
' (Max. 255 chars)',//678
'Who can to see this photo?',//679
'Your private photo has been successfully uploaded',//680
'There are {0} private photos total',//681
'Checking by moderator',//682
'{0} private photos have been successfully removed',//683
'Photo ID#',//684
'Wrong password',//685
'Don`t use',//686
'Select glitter chars for subject',//687
'Use glitter chars for subject',//688
'Select glitter chars for message',//689
'Use glitter chars for message',//690
'You should to use english chars only',//691
'Your user type <b>{0}</b> can create only <b>{1}</b> views',//692
'View "{0}" has been successfully created!',//693
'Points',//694
'Point`s number',//695'
'You must be at least a {0} or to pay {1} points to access this data',//696
'You must to pay {0} points to access this data',//697
'You have total {0} points and can`t to see {1}, requires {2} points for this profile.<br>You may to purchase additonal points from <a href="{3}" target="_blank">here</a>',//698
'You have total <b>{0}</b> points. Are you sure to pay <b>{1}</b> point to see user`s <b>{2}</b>?',//699
'OK',//700
'Cancel',//701
'<b>{0}</b> points have been taken from your balance. Now you have <b>{1}</b> points.<br><br><b>Please do not reload this page to prevent taking a points again!</b>',//702
'Congratulations, 

You have been successfully purchased {0} points. 
__________________________________
{1}', //703
'{0} points has been received on {1}', //704
'Points has been purchased from IP:{0} has been registered in {1}
									User ID: {2}', //705
'{0} points has been purchased!', //706
'Terms Of Use',//707
'Privacy Statement',//708
'Edit User',//709
'Deprecate using signature',//710
'Allow to use signature',//711
'Ban user',//712
'Select forums which user willn`t access',//713
'Upload / Select Avatar',//714
'Full List',//715
'Don`t ban user',//716
'You have been banned for this forum',//717
'Forum posts',//718
'Click here to see user`s forum posts',//719
'All smiles',//720
'Search within',//721
'miles',//722
'Click here to see user`s blog',//723
'Blog',//724
'The Blog of {0}',//725
'Can have signature for forum',//726
'Can use avatar',//727
'Can upload avatar',//728
'Can use forum options',//729
'Can see slideshow',//730
'Can see big photos in the slideshow',//731
'Can see testimonials',//732
'Can add testimonials',//733
'Can use smiles and BB codes in the testimonials',//734
'Can to have own blog',//735
'Can to see other blogs',//736
'Can to use smiles and BB code in the blog',//737
'Can to see articles',//738
'Can create views',//739
'Can upload private photos ',//740
'Can see private photos',//741
'Can to use glitters in the ecard subject',//742
'Can to use glitters in the ecard message',//743
'Can to see Favorites list of the user',//744
'Hide profile from all types of search results and online users list',//745
'{0} hidden users',//746
'You can`t to contact user with the same gender as yours.<br><br>',//747
'Sorry. You can only to change your profile once in {0} hours.',//748
'Yesterday',//749
'Today',//750
'Features',//751
'{0} hidden or filtered users',//752
'User made profile hidden',//753
'Users here: ',//754
'Private notes',//755
'No notes available<br><br> {0} Create new {1}',//756
'Entry has been successfully changed',//757
'Entry has been successfully added',//758
'Edit entry',//759
'Thanks! Your coupon code has been successfully accepted!<br>You have received {0} points!',//760
'Visible for access level and higher',//761
'Don`t receive emails from website',//762
'You can upload maximum <b>{0}</b> private photos for <u>{1}</u> category',//763
'User is registering',//764
'Post',//765
'Template for `My Profile` page',//766
'Don`t receive winks from users',//767
'User prefered not to receive winks',//768
'Submit site',//769
'URL',//770
'Limit',//771
'Your name',//772
'Category',//773
'Reciprocal Link URL',//774
'To validate the reciprocal link please include the following HTML code in the page at the URL specified above, before submiting this form',//775
'URL is already exist in the our database',//776
'Incorrect Reciprocal Link URL',//777
'Confirm Link Submission',//778
'Thanks for submitting your website at '.C_SNAME.'.
Please click here to confirm your submitting:

',//779
'Error: {0} could not be opened, the location probably does not exist',//780
'Reciprocal Link URL must be from the same domain as a submitted URL',//781
'Our html code haven`t been found on the your Reciprocal URL. Please check all and resubmit again.',//782
'Thank you! Your URL has been successfully added!',//783
'Thank you! An email has been sent to you to confirm your submitting!',//784
'Thank you! Your URL will be added after admin checking!',//785
'URL hasn`t been found or already submitted!',//786
'Detail',//787
'Details information for',//788
'Link Owner',//789
'No new photos available for your criterias. Visit later or change criterias.',//790
'Select your photos participate in the Hot or Not rating',//791
'Hot or Not',//792
'You must enable "{0}" in the {1} to enable photos for rating',//793
'Rating',//794
'Rate users',//795
'Show me',//796
'Between age',//797
'Top Men Photos',//798
'Top Women Photos',//799
'Top Users Photos',//800
'Blogs',//801
'More',//802
'Full text',//803
'Blogs for last {0} days',//804
'Sorry, You have already rated!',//805
'Linked profile',//806
'You can to select profile you want to link with. Enter User ID and check, write description and send request. ',//807
'Description (ex. "My wife")',//808
'You can`t link itself',//809
'{0} Hour',//810
'You haven`t enough points for this operation',//811
'How many hours to purchase?',//812
'Archive',//813
'There are no messages in the archive!',//814
'Clear',//815
'Other',//816
'All categories',//817
'There are no links in this Category',//818
'An error occured during loading of a {0} file. File may be corrupted. Please select other file!',//819
'Can contact users with the same gender',//820
'Can use filter by country in online users page',//821
'Can use filter by city in online users page',//822
'Can use filter by age in online users page',//823
'Can to deprecate view him profile',//824
'Can be anonymous when access other profiles',//825
'Can to change template for own profile',//826
'Can prefer not to receive emails from website',//827
'Can to see "Users here" on the forum',//828
'Can to see forum statistics',//829
'Can to get information about new messages without page reloading',//830
'Can create private notes',//831
'Can not to verify image in the: "{0}"',//832
'Blog comments',//833
'Can to select access level for each photo',//834
'Can prefer not to receive winks',//835
'Can use superwink feature',//836
'Can to add link',//837
'Can to Rate photos from the user profile',//838
'Can to Rate photos from "Hot or Not" random page',//839
'Can to participate in the "Hot or Not"',//840
'Can to see link to profile, when rate',//841
'Can to access to "More Results" page of the Best photos',//842
'Can to comment blogs',//843
'Can to see comments in the blogs',//844
'Can to rate blogs',//845
'Can to reply to comments for own blog',//846
'Can to link to other profile',//847
'BBCodes Access',//848
'Can to use smiles',//849
'Can to use <b>Bold</b>',//850
'Can to use <i>Italic</i>',//851
'Can to use <u>Underline</u>',//852
'Can to use <s>Strike</s>',//853
'Can to use QUOTES',//854
'Can to use URL',//855
'Can to use IMG',//856
'Can to use VIDEO',//857
'Profile comments',//858
'Limitations',//859
'' //860
);  
?>