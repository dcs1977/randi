<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Wrote by                AzDG (support@azdg.com)          #
# Created 03/01/03        Last Modified 05/02/03           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               defaulta.php                     #
# File purpose            Additional langfile for admin    #
# File created by         AzDG <support@azdg.com>          #
############################################################
### !!!!! Please read: RULES for translating!!!!! ###
### 1. Be careful in translating - don`t use ' or { } characters!
###    You can use their html-equivalent - &#39; &#123; &#125;
### 2. Don`t translate {some_number} templates - you can only replace them 
###    {0} - is not a number - it is a template!!
###################################
$x=array(
'<font color=red size=3>*</font>', //0 - Symbol for required field
'Incorrect login or password - try again',//1
'Administration Area',//2
'Admin Login',//3
'Admin Password',//4
'Enter',//5
'Access denied for non-Admins',//6
'Main Page',//7
'Clear hits',//8
'Repair&Optimize',//9
'User Management',//10
'FAQ Management',//11
'Maillist',//12
'Log out',//13
'You have been succesfully logged out of Administration Area',//14
'Total mail',//15
'Sent mail',//16
'The following part',//17
'{0} mail has been sent to users', //18
'You have unread messages in {0}', //19
'Found {0} members',//20
'Maillist manager',//21
'With status:',//22
'Mail options and fields:',//23
'Mail type',//24
'From Mail',//25
'Mail subject',//26
'Mail body',//27
'Confirm about results',//28
'Return back',//29
'Send mail',//30
'The unfinished dispatches - {0}',//31
'Subject : {0} [Sent to {1} users] :::>',//32
'Finish this dispatch',//33
'Text',//34
'HTML',//35
'Preview',//36
'Users with unread messages - ',//37
'Total unread messages - ',//38
'Last message id - ',//39
'Send confirm email to users with unread messages',//40
'Profile successfully updated',//41
'FAQ question has been succesfully created',//42
'Create new',//43
'Please enter question',//44
'Please enter answer',//45
'FAQ Management',//46
'Create new FAQ category',//47
'Edit FAQ category',//48
'Delete FAQ category',//49
'Create FAQ',//50
'Select Category',//51
'FAQ question',//52
'FAQ answer',//53
'First of all you must create categories',//54
'Please enter category name',//55
'FAQ category create',//56
'New category created:',//57
'Category name',//58
'FAQ category edit',//59
'Edit',//60
'Category has been changed',//61
'FAQ category edit',//62
'Selected choice has been deleted',//63
'FAQ category delete',//64
'Note : Be careful. With delete category, you will remove all questions in this category.',//65
'Delete',//66
'0 - Wait email checking<br>1,10 - Wait checking after register (invisible,visible)<br>2,9 - Wait checking after update (invisible,visible)<br>3,8 - Wait allowing to remove (invisible,visible)<br>4 - User is registering step by step<br>5 - Profile suspended<br>7 - User is active<br>',//67
'Database has been repaired and optimizied',//68
'User with ID#={0} has been successfully deleted from database',//69
'Hits have been cleared',//70
'Successfully allowed user with id={0}',//71
'IP',//72
'Privilegied array number [0 - 255]',//73
'Status - (BE CAREFUL)',//74
'Approve',//75
'User management',//76
'Total active users :',//77
'Users awaiting profile activation after registration',//78
'Users awaiting profile activation after update',//79
'Users awaiting removal',//80
'Privileged users',//81
'Privileged users Management',//82
'Recommend Statistics',//83
'Sender Email',//84
'Sender IP',//85
'Recipient Email',//86
'Clear Recommend Statistics',//87
'Recommend Statistics have been successfully cleared',//88
'Clear user statistics',//89
'Status',//90
'User type',//91
'Be sure that you have all necessary information for send money to user',//92
'All affiliate statistics for user with ID#{0} have been successfully removed from database.',//93
'Unfinished dispatch has been successfully removed from database',//94
'Hello {0} {1}

You have {2} unread messages in {3}.

You can check it in:
{4}

Remember that your login is:
{5}
-------------------
{3} 
', //95
'FAQ question edit',//96
'FAQ question delete',//97
'Select question',//98
'Popularity [max: 65535]',//99
'News management',//100
'News subject',//101
'News text',//102
'Note:Subject and text must be entered in html format',//103
'Add News',//104
'Edit News',//105
'Delete News',//106
'News Priority [0-255]<br><i>Set 1-255 for sticky news</i>',//107
'News Subject and Text is empty or Priority is not numeric',//108
'The News with this text is already in database. Use "Edit News" form for edit this news.',//109
'News has been added in inactive mode.<br><br> You can check it by click [Preview News]<br> or make it active by [Make Active].<br><br>If you want to edit something please click to [Edit News].<br>PLEASE DON`T USE BACK BUTTON IN BROWSER',//110
'Preview News',//111
'Make Active',//112
'The given news is made active',//113
'Active',//114
'Inactive',//115
'This news is',//116
'The given news is made inactive',//117
'Make Inactive',//118
'News has been successfully changed',//119
'Options',//120
'Configuration',//121
'Template Name',//122
'Website Name',//123
'Authorization Type',//124
'Sessions',//125
'Cookies',//126
'Admin Email',//127
'Email For non-reply purposes',//128
'Sendmail step for admin',//129
'The number of last registered users',//130
'The number of most popular members',//131
'Count of search results in administration area',//132
'Search page links number [1 . . . 4 5 6 7 8 . . . 22]',//133
'How to add profiles to database',//134
'Send congratulations email when registration is complete?',//135
'Yes',//136
'No',//137
'Number of days for awaiting user mail autorization',//138
'How to update profile?',//139
'How to remove profile for users',//140
'How to show languages?',//141
'Default language for multilanguage interface',//142
'Default language',//143
'Browser language',//144
'Enable Popularity?',//145
'How many hits add for user, when his profile viewing by another user',//146
'How many hits add for user, who send mail?',//147
'How many hits add for user, who got a mail?',//148
'How many hits add for user, who sent message?',//149
'How many hits add for user, who got a message?',//150
'How many hits add for user, when he add in a bedroom?',//151
'Popularity period in hours - 1 IP may be vote 1 time in THIS hours',//152
'Audio',//153
'Allow to upload audio file?',//154
'Maximal size for uploaded file',//155
'Autostart audio play?',//156
'Loop times?',//157
'Audio player type',//158
'Ecards',//159
'Cards number in 1 row',//160
'Send card notify to ',//161
'Days number the card will be alive',//162
'Message Box',//163
'Email',//164
'Affiliate',//165
'Enable Affiliates?',//166
'Affiliate cookies life time (in days)',//167
'Affiliate cookies name',//168
'Maximal money to send in $',//169
'Banner exchange',//170
'Enable banner exchange?',//171
'Miscellaneous Options',//172
'Various fields length',//173
'FirstName smaller and biggest length',//174
'LastName smaller and biggest length',//175
'Password smaller and biggest length',//176
'Minimum and maximal age',//177
'Hobbies largest word size',//178
'Hobbies largest length',//179
'Description`s largest word size',//180
'Description`s largest length',//181
'City biggest word size',//182
'Requirement fields on Register',//183
' [recommended]',//184
'Show Agreement',//185
'Allow to change gender',//186
'Allow to change email',//187
'Minimum Access for View Profile fields',//188
'Links',//189
'Show send message link?',//190
'Show send virtual card link?',//191
'Show mail to user link?',//192
'Show add to bedroom link?',//193
'Show link to add in bad profiles?',//194
'Show link to add in banlist?',//195
'Main Page',//196
'Show "Recommend Us" script?',//197
'Number of Friends email in "Recommend Us" script',//198
'Search by birthday in main page in quick search? ',//199
'Show only Last registered users with photos',//200
'Show most popular users in main page?',//201
'Enable News?',//202
'How many news items to show?',//203
'Enable Top Men?',//204
'Enable Top Women?',//205
'Number of top photos for each of gender',//206
'In search results show users with photos first',//207
'Maximal photos size in Kb',//208
'Check for photo width and height (Only in PHP safe_mode=off is allowed)',//209
'Photo`s maximum width',//210
'Photo`s maximum height',//211
'Security Mode',//212
'Members and Guests rights',//213
'Minimum access for using Feedback',//214
'Can registered user specify another email for reply in send feedback?',//215
'Minimum access for using Advanced Search',//216
'Minimum access for using Search',//217
'Show online users only for members?',//218
'Bad profiles note only for members?',//219
'View users only for members?',//220
'Mail to users only for members?',//221
'Can registered user specify another email for reply in send mail?',//222
'FAQ only for members?',//223
'Show today`s Birthdays?',//224
'Recommend only for members?',//225
'Show "You have new message" message for users',//226
'Confirm user when another users add their profiles into "Favorites list"',//227
'Whats show for user as Username in members area?',//228
'No name in "View Messages", only ID num?',//229
'Include original subject to each answer subject',//230
'Include original message to each answer message',//231
'Show user photo in search results',//232
'Payment System',//233
'Payment Mode',//234
'Which gender must to be a privileged user?',//235
'All Users',//236
'Show Payment link for already privileged users too?',//237
'PayPal Options',//238
'Enable PayPal?',//239
'PayPal business email',//240
'2CheckOut Options',//241
'Enable 2CheckOut?',//242
'Your 2CheckOut Login ID',//243
'Enable Demo mode',//244
'Secret Key',//245
'Login cookies prefix',//246
'Chat Options',//247
'Enable Chat',//248
'Allow to login for only registered members',//249
'Statistics Options',//250
'Change Options',//251
'How many messages show in "View Messages"',//252
'Options has been changed successfully',//253
'Whats to show as username for user in search results?',//254
'You can use any or all of this templates:<br>ID - User id<br>USERNAME - Username (Only if login by username)<br>FNAME - User Firstname<br>LNAME - User Lastname<br><br>For example:<br>FNAME LNAME #[ID]',//255
'Moderator`s options',//256
'Moderator`s rights',//257
'Note:For remove moderator, just make the login and password fields empty',//258
'Login for Moderator',//259
'Password for Moderator',//260
'If you want, you can add new moderator',//261
'Can Approve Users',//262
'Can Edit Users',//263
'Can Remove Users',//264
'Can view "Who is Online?"',//265
'Can view "Bad Profiles"',//266
'Can Clear Hits',//267
'Can Repair Database',//268
'Can use Search in "User Management"',//269
'Can use "Mailing List"',//270
'Can Add, Edit and Remove Privilegied Users',//271
'Can work with "Affiliates" menu',//272
'Can Add, Edit and Remove FAQ',//273
'Can Add, Edit and Remove News',//274
'Can view "Recommend Us" statistics',//275
'Can view "Statistics"',//276
'You dont have the rights to perform this operation',//277
'Show AIM only in Privileged User`s "View Profile"',//278
'Show ICQ only in Privileged User`s "View Profile"',//279
'Show Webpage only in Privileged User`s "View Profile"',//280
'Show Phone only in Privileged User`s "View Profile"',//281
'Moderators Area',//282
'Moderator Login',//283
'Moderator Password',//284
'Access closed for non moderators',//285
'You have successfully logged out from the moderators area<br><br>Thanks for using our script',//286
'Welcome Title',//287
'Welcome message',//288
'Forum Management',//289
'Create New forum',//290
'First of all you must create Forum categories',//291
'Create new Forum Category',//292
'Edit Forum Category',//293
'Delete Forum Category',//294
'Forum Title',//295
'Forum Description',//296
'Please enter forum title',//297
'Please enter forum description',//298
'Select Category',//299
'Minimum Access',//300
'Moderators ID<br>(Enter here users ID who can moderation rights to this forum separated by commas, for example: 212,311,581)',//301
'Please fill out title, description and moderators fields correctly!',//302
'New forum has been successfully created.',//303
'Bad Words Checking',//304
'Enable Maintenance Mode?',//305
'Maintenance message',//306
'Short Date Format:<br>
{MONTH} - 3 first char of month<br>
{FULL_MONTH} - full name of month<br>
{NUMERIC_MONTH} - numeric value of month<br>
{DAY} - day<br>
{YEAR} - year',//307
'Long Date Format:<br>
{MONTH} - 3 first char of month<br>
{FULL_MONTH} - full name of month<br>
{NUMERIC_MONTH} - numeric value of month<br>
{DAY} - day<br>
{YEAR} - year<br>
{TIME} - time',//308
'Difference time with server in hours',//309
'Minimum access for view user`s type (Free member, Silver member, etc...)',//310
'Minimum access for view Profile Views number',//311
'Minimum access for view user online status',//312
'Make auto-login after successful registration',//313
'Enable forum?',//314
'Minimum access for using forum',//315
'If you using login by ID or email (not username), in chat must be using some unique username. The best way is to use email login + ID (for example user with ID=211 and email=myname@hotmail.com must have chat username: myname211). Here you can specify first N chars from email login using for chat username and for protection from very long usernames.',//316
'Minimum access for show popularity',//317
'Minimum access for play audio speech',//318
'Minimum access for using Quick Search',//319
'Privilegied users only',//320
'For Advanced users only: Here you can enter a date closed of privileges of the user (in YYYY-MM-DD format).<br> If you will leave this field empty, the script will automatically add the necessary date.',//321
'Forum Edit',//322
'Forum Delete',//323
'Forum has been changed',//324
'Can Add, Edit and Remove Forums',//325
'Links Management',//326
'Create New Category',//327
'Edit Category',//328
'Delete Category',//329
'Please enter website URL and Name in HTML format. For example:<br>&lt;a href=&quot;http://www.azdg.com/&quot; target=&quot;_blank&quot;&gt;AzDG Website&lt;/a&gt;',//330
'Please enter website description in HTML format.',//331
'Edit Link',//332
'Delete Link',//333
'Create Link',//334
'First of all create categories, and then reload this page',//335
'Priority',//336
'Operation has been successfully finished!',//337
'Confirm admin only if',//338
'Access and Security',//339
'Section`s Options',//340
'Online members counter`s timeout ( in seconds )',//341
'Image Options',//342
'Set Hits to 1, when admin clear hits.',//343
'Minimum access level for see links',//344
'IP Access Management',//345
'Banned IPs separated by commas, for example:<br>10.23.49.21,10.41.1.*,212.48.*.*,208.64.5.17',//346
'Privilegied IPs separated by commas. <br>Users with this IPs will get privilegied rights in login.',//347
'What access will get users with privilegied IPs?',//348
'Database Management',//349
'Messages control',//350
'Privileged users timelife (in month)',//351
'Payment for each member type in dollars. Can be float for ex. 1.50',//352
'Payment for affiliate users. Can be float number for ex: 1.50',//353
'How many messages per day (last 24 hours) can send each user type',//354
'Messages number which can send new member for free by gender',//355
'Enable messages control',//356
'Minimum rights for subscribe profiles',//357
'Mail to users with unread messages',//358
'Mail to subscribed users',//359
'Total Subscribers',//360
'Last dispatch was',//361
'New profiles from last dispatch`s date',//362
'Hello dear customer!
<br><br>
You have subscribed to our new profiles list. <br>
If you wish to unsubscribe, you can make it at the bottom of this page.',//363
'If you don`t wish to receive new profiles, please unsubscribe. Just follow this link:
<br><br>
{0}
<br><br>
Click this link, or copy and paste the address into your browser.',//364
'New profiles from {0} since {1} selected by your criteria of search:',//365 {0} - Site Name, {1} - Date
'Mail sent to', //366
'Dispatch to subscribers has been successfully finished!<br>
Has been sent {0} emails.', //367
'Welcome user, when he returns to the site?', //368
'Send email to user if he got first unreaded message', //369
'Who can see "More Results" link for popular users', //370
'Photos size checking', //371
'Thumbnail options', //372
'Thumbnail`s maximal width', //373
'Thumbnail`s maximal height', //374
'Thumbnail`s auto-option (maximal width or height)', //375
'Thumbnail quality [0-100]', //376
'The Sign of money currency', //377
'Use autoprocessing for all transactions without admin checking', //378
'Send email to admin when transaction will be successfully delivered', //379
'Show "Not visible in profile" for fields in register only if access:', //380
'Don`t hide fields, just show that only respective user can see it (with link for purchasing)', //381
'Crypt URL to image? (recommend to enable)<br>By disabling this option, you disable Thumbnail using.', //382
'Crypt Key', //383
'Width of photos in results of search', //384
'Photos in results of search must link to:', //385
'Photos in search results', //386
'Can Add, Edit and Remove Links', //387
'Can use "Mail to users with unread messages"', //388
'Can use "Mail to subscribed users"', //389
'Maintenance', //390
'Cleaning messages table', //391
'Cleaning users table', //392
'Remove messages older than (90 or higher - recommended)', //393
'days', //394
'Do not remove unread messages', //395
'{0} messages have been removed from database', //396
'Are you sure that you want to remove messages by your choice?', //397
'Is recommending to run "Repair&Optimize" now!', //398
'Show new profiles in the "Users awaiting profile activation after registration" menu. <br>Note: Actual for only "Automatically allow all profiles" and "After confirm from email"', //399
'Minumum access for search by "Type of user" in the advanced search', //400
'Reason', //401
'Chars available for username (regexp)<br>For ex: A-Za-z0-9_<br>Note: Do not recommend to change', //402
'Caching System', //403
'Cache Type', //404
'Cache Life (in seconds)', //405
'Links in the Statistics Page', //406
'What requests to cache?', //407
'Remove inactive users', //408
'Remove users inactive last NN days (300 or higher - recommended)<br>Users will`nt remove, their status will be changed and you see them under "Removed Users" status link.', //409
'Don`t remove privileged users', //410
'Are you sure that you want to remove these users?', //411
'Status for {0} users has been changed to "Removed Users"', //412
'Notify admin by email, if someone signs up on the site', //413
'Check only new profiles with photos (works if one of the Admin Checking mode selected)', //414
'Watermark options', //415
'Use Watermark', //416
'Watermark type', //417
'Watermark text', //418
'Watermark image <br>Should locate in the templates/YOURTEMPLATE/images/ directory and be in png format', //419
'Watermark position', //420
'Watermark text color', //421
'Watermark text orientation ', //422
'Note: Watermark works on the fly, and eats server resources. Disable it for improve performance', //423
'You may use the following variables in the mail subject and mail body (case sensitive):<br>
<br>
{USERNAME} - username<br>
{FNAME} - firstname<br>
{LNAME} - lastname<br>
{PASSWORD} - password<br>
{EMAIL} - email<br>
{ID} - id<br>
{USERTYPE} - type of user (Free member or Silver member or etc...)<br>
{REGDATE} - Registration date<br>
{EDITDATE} - Last access date
', //424
'Unactive more than N days (specify the number of days)', //425
'Backup Database', //426
'Compressing type', //427
'No compress', //428
'Select tables to backup', //429
'Options Control', //430
'Who may access Options page', //431
'Allow to select templates', //432
'Who may rate users (For rate system)', //433
'<strong>Note: Please clear hits, after changing popularity type!!!</strong>', //434
'The number of digits that will be displayed following the decimal point (For rate system)', //435
'Popularity Type', //436
'Minimum vote number to make profile available for top rating (For rate system)', //437
'Vote Management', //438
'Create Vote', //439
'Edit Vote', //440
'Delete Vote', //441
'Please enter your Question', //442
'Please enter number of answers', //443
'Please enter answers Number', //444
'Radio button', //445
'Checkbox', //446
'Type of answer', //447
'Press <strong>[Ctrl]</strong> or <strong>[Shift]</strong> to select multiply', //448
'Vote ID:', //449
'Show only for users with following languages', //450
'Vote Question', //451
'Show Form:', //452
'Show Results:', //453
'Available languages', //454
'Make Vote as Default', //455
'Default Vote', //456
'or', //457
'Vote set as default', //458
'Selected language for vote has been removed', //459
'Add new language', //460
'New languages have been added', //461
'Vote language edit', //462
'Language phrases for this vote have been changed', //463
'Result', //464
'Vote options have been changed', //465
'Please confirm, that you really want to remove vote!!!', //466
'Vote has been successfully removed!', //467
'Add new user', //468
'If you want to change subscription options, you may do it from following link:
<br><br>
{0}', //469
'Subscription Management', //470
'Search subscribed users', //471
'Selected entries have been removed', //472
'Language is <strong>{0}</strong>.', //473
'MultiLanguage Interface', //474
'Edit Phrases', //475
'File {0} doesn`t exist', //476
'File {0} doesn`t writeble', //477
'Phrases: ', //478
'Selected phrases have been changed', //479
'How many photos may upload each type of user', //480
'Popularity votes number <br>(Actual for rate system mode and counted as Popularity/Votes number)', //481
'Administration fields', //482
'Admin directory', //483
'Moderator directory', //484
'Temporary directory', //485
'Show "Send a wink"', //486
'How many hits add to user getting "Send a wink"', //487
'Confirm user if receiver read "Send a wink" message', //488
'Minimum access for view big photos', //489
'Minimum access for register new profiles', //490
'Registration', //491
'Specify in register, what type of user may see current field in user profile', //492
'The number of birthdays to show by pages', //493
'Minimum level of access to see Users Plans', //494
'"Who seen my profile" minimum access level', //495
'"Who seen another users profile" minimum access level', //496
'"Who seen profile" - Show N results by page', //497
'Vote graph length in pixels', //498
'Time to reset IPs (in hour), allow voting 1 IP in N hours', //499
'View form if already voted', //500
'Show "View results" link in the form?', //501
'Show results after succesfull vote for this type of user', //502
'Show results after unsuccesfull vote for this type of user', //503
'Forums count on page', //504
'Posts count on page', //505
'Who can use matchmaker?', //506
'Registration type', //507
'Who can vote?', //508
'Statistics page access', //509
'Allow to change photo', //510
'Allow to change audio', //511
'Blacklist Manager', //512
'Blacklist type', //513
'Word', //514
'Enter one or several words (if you enter several words, separate them by commas). wildcard * is allowed for any number of chars, examples:<BR><BR>badmail@yahoo.com<BR>*bitch@hotmail.com<BR>*fuck*,bitch*,hack*<BR>*@blackdomain.com<BR>*baduser', //515
'Blackwords have been successfully added into database!', //516
'Blacklist', //517
'Who may prefer to Rate or No own profile', //518
'New profiles should be set to "Prefer to Rate" Option`s value', //519
'New&Changed Photos', //520
'Remove selected & Reload for new', //521
'Total <B>{0}</B> users should be checked for new&changed photos', //522
'Check new&changed pictures you want to remove. All unchecked pictures will be approved.', //523
'Remove photo', //524
'No user available with unchecked photos', //525
'Change profile and Approve it!', //526
'Registration fields', //527
'Search fields', //528
'Search pages to show by access level - 0 means unlimited pages', //529
'Search results', //530
'Coupon payments', //531
'Enable image verification?', //532
'Messaging Management', //533
'No coupons available', //534
'Coupon`s name', //535
'Coupons, enter coupons per line, for ex.:<BR>PROMO-GSM5201-931<BR>PROMO-GSM3411-121<BR>PROMO-GSM9342-091', //536
'Privilegies period (in days)', //537
'Coupon`s number', //538
'Attention: Coupons will be removed when user enters it, to prevent the entering one coupon several times.', //539
'Selected coupon`s group has been removed!', //540
'Coupon`s with this identification name already exist, please select another!', //541
'Who can use smiles and special tags', //542
'Who can create new threads', //543
'Who can reply', //544
'<i>Security Alert</i>: Please remove install.php', //545
'Number of days to store information about "Who seen my profile"', //546
'Enable Bad Word checking', //547
'Specify Bad words delimited by commas. Search by this words is case insensitive. You can use * char for unknown begin and end of the word, for example: *@*,*_at_*,fuck*,bitch', //548
'What to make if badwords will be found? Choose one 1 or 2...', //549
'1. Show error message',//550
'Specify error message',//551
'2. Just to replace badword with another text',//552
'Specify replacement word. For example: ####',//553
'Welcome Title',//554
'Welcome message [in html format] (Welcome title and message will show in this 1 common language. If you want, you can remove 2 constants from options.inc.php (C_WELCOME_TITLE and C_WELCOME_MESSAGE) to show phrases from language files.)',//555
'Multilanguage Interface (MUI)',//556
'"Default" language code in 2 chars:',//557
'Choose editable language',//558
'News should be multilanguage',//559
'FAQ should be multilanguage',//560
'Multilanguage Interface (MUI) - Edit phrases',//561
'Fields to show in edit&view profile pages for admin',//562
'Don`t show fields with empty or not selected values',//563
'Field name',//564
'Edit mode',//565
'View mode',//566
'Fields to show in register&update pages for user',//567
'Search fields to search by access level',//568
'Search results fields by access level',//569
'Please enter question',//570
'Please correct number of questions value',//571
'Please select language(s)',//572
'Total messages:',//573
'Sorry - this section doesn`t work in the demo mode',//574
'Quick links',//575
'Script News',//576
'Script Information',//577
'Script Version',//578
'Total messages:',//579
'Total uploaded files:',//580
'Size of uploaded files (Mb):',//581
'PHP Version',//582
'MySQL Version',//583
'Type of loader',//584
'Safe Mode',//585
'GD module',//586
'Hosting Information',//587
'To ID',//588
'From ID',//589
'Messages sent in last N days',//590
'You can use special symbols: * - any number of chars, ? - any 1 char',//591
'Approve selected profiles',//592
'Remove selected profiles',//593
'Remove selected messages',//594
'Message with ID#={0} has been successfully deleted from database',//595
'Allow to see own comments',//596
'Allow to see all user`s comments',//597
'Enable Bad word checking?',//598
'Allow to reply to own comments',//599
'Allow to add new comments',//600
'Allow to remove own comments',//601
'Search Options',//602
'Administrator area language',//603
'Who can see number of total users found in the search results?',//604
'Allow to see the photo of commenting',//605
'Image Verification',//606
'Enable Image Verification for Register',//607
'Enable Image Verification for Login',//608
'Decrease image size and dimensions when uploading to server?',//609
'Decrease to width',//610
'Decrease to height',//611
'Decrease automatically biggest size to following value',//612
'Decreased image quality',//613
'How many photos to show in the User Profile page',//614
'How many photos to show at one step in the Photos page',//615
'Allow to make right clicks in the Photos Page?',//616
'Show error message if user clicks right button?',//617
'Default number of search results',//618
'Who can to see Visited Profiles page?',//619
'Watermark text transparency',//620
'Select access to show random template or disable it',//621
'Show Expired Privileges Date in the header',//622
'Who can prefer to be not rated?',//623
'Who can see the day of birthday in the User Profile',//624
'Use Gallery - just new or changed photos will be shown for admin checking',//625
'Show new or changed photos in Profiles only after administration checking in the "User Management => New&Changed photos". Gallery option should also be enabled.',//626
'Number of columns in the Gallery',//627
'Number of rows in the Gallery',//628
'Online Users',//629
'Search by gender',//630
'Search by user type',//631
'Search by picture',//632
'Steps for search (divided by comma)',//633
'Show privileged users first in search results',//634
'Resize photos in search results - make long side not more than "Width of photos"',//635
'Minimal requirement number of photos',//636
'Who can use smiles and special tags in the Send Message form?',//637
'Show "No Photo" picture in the search results',//638
'File name of the "No photo" picture ({G} - replace with the user gender)',//639
'Messaging type <BR>(Select this once after installation and do not change in the future to prevent possible errors)',//640
'Table name',//641
'Repair',//642
'Optimize',//643
'Operation',//644
'Message type',//645
'Attention: Your website is working in non maitenance mode. It is highly recommending to <a href=index.php?do=opt&type=config&>Enable Maintenance Mode</a> before running Repair or Optimize tables.',//646
'Help',//647
'Release date',//648
'Documentation',//649
'Permanently remove user(s) <BR>(else just a change status to "Removed")',//650
'Your profile has been removed from {0}',//651
'Delete Profiles',//652
'Remove profiles from admin area permanently?',//653
'Attention: options below are actually for <B>"Delete"</B> links only, in the search results, view and edit profile pages.<BR><BR> Profiles will be removed permanently without confirmation if you remove them by selecting checkboxes in search results and click on <B>"Delete selected profiles"</B><BR><BR>',//654
'Admin`s edit&view profile',//655
'Do not open the new window and remove profiles without confirmation',//656
'Allow to send email in the confirmation page?<BR>(By default form will be closed, so you need to check and open it)',//657
'Fill out subject field of email with default Subject value?',//658
'Fill out subject field of email with default Message value?',//659
'Send email in administrator language?',//660
'Confirm administrator when removing profiles using checkboxes in the search results?',//661
'WARNING: This will permanently remove the selected profiles. Are you sure you wish to do this?',//662
'Send email if user profile removed using checkboxes (or directly from if user itself requested it from members area)',//663
'Remove selected',//664
'Security information',//665
'Fail admin logins',//666
'<span style=color:red>Off</span>',//667
'<span style=color:green>On</span>',//668
'Security Errors',//669
'Users registered in last day',//670
'Users registered in last 3 days',//671
'Users registered in last 7 days',//672
'Notices',//673
'Select the fields should be counted for %',//674
'Enable notice for not filled profiles',//675
'Show notice only if % of filled(and selected here) fields lower than this value. For example show notice if less than 70% of fields are filled. Enter just a number (0-100) without %',//676
'Enable notice if photo not yet uploaded',//677
'Enable notice if "About" field have small number of words',//678
'How many words, minimal, should present in the "About" field',//679
'Currency',//680
'Testimonials results by access level',//681
'Can view testimonials',//682
'Can add new testimonials',//683
'Can use BB-codes and smiles when adding testimonials',//684
'How to add testimonials',//685
'Number of testimonials per page',//686
'Can see number of total testimonials number?',//687
'Testimonials Management',//688
'Testimonial has been changed to active',//689
'Testimonial has been changed to inactive',//690
'This testimonial is ',//691
'Edit&Reply testimonial',//692
'Remove testimonial',//693
'Testimonial has been removed!',//694
'You may reply to this testimonial, if it is requiring',//695
'Testimonial has been changed',//696
'Use SMTP',//697
'SMTP Host* (SMTP server to relay, like "smtp.myisp.com")',//698
'SMTP Port* (port of the SMTP server to use, like 25 or 465)',//699
'SMTP server requires an secure connection using SSL',//700
'POP3 authentication host if your SMTP server requires prior POP3 authentication',//701
'User name if the server requires authetication',//702
'Authetication password',//703
'Authetication realm, usually the authentication user e-mail domain',//704
'Mail options',//705
'Articles Management',//706
'Create Article',//707
'Edit Article',//708
'Delete Article',//709
'New article has been successfully added to database',//710
'Article has been changed',//711
'Attention: This will also delete all articles entire category',//712
'Edit Mail messages',//713
'phrase number #',//714
'Edit file:',//715
'Common subject - see the subject phrase number#{0} above',//716
'Mail subject the same as body or entered by user',//717
'Templates management',//718
'Can`t open file - read access denied',//719
'Can`t edit file - write access denied',//720
'File not found',//721
'File has been changed',//722
'Can`t create backup file - write access denied for file or directory',//723
'Restore original',//724
'Can`t restore original file - write access denied for file or directory',//725
'Original file {0} has been restored',//726
'Options below actual for both - audio and video',//727
'Addon: Allow to start dispatch in the backend - by cronjob (cronjob entry should be also created - see addons/cronjob-send_mail_to_subscribers.php).<br> Checkbox will be unchecked automatically when addon script will be finished',//728
'How to send',//729
'1 or more',//730
'New private photos',//731
'Check private photos you wish to remove. Unchecked photos will be approved',//732
'There are total {0} private photos should be checked',//733
'Access',//734
'Points number has been updated',//735
'Points management',//736
'Points to see profile fields',//737
'Point`s multiplier',//738
'Allow to multiply number of points personally for this user - only integer value possible',//739
'Show by user country first: ',//740
'Select country in the listing of ',//741
'IP2Country Management',//742
'Enable IP2Country mode?',//743
'Where to use IP2Country mode?',//744
'Search by ZIP Distance (requires to install ZIP data)',//745
'META tags',//746
'Title',//747
'Keywords',//748
'Description',//749
'Profile page',//750
'Available values',//751
'Default metas: title, keywords and description you can edit in the each using language file. Specific for some pages meta tags you can change here',//752
'Open IM in the new window',//753
'Search in the forum',//754
'How many ranges from IP to hide from forum moderators? <br>0 show full IP, 4 - hides all with *.*.*.*',//755
'Show first post in the each page of the thread',//756
'Can use signature',//757
'Top Forum values number',//758
'Can use forum options',//759
'Can use avatar',//760
'Can upload own avatar',//761
'Avatar maximum width',//762
'Avatar maximum height',//763
'Avatar maximum size in Kb',//764
'Decrease avatar when upload',//765
'Decreasing quality (0-100)',//766
'Existing avatars extension (only 1 common extension allowed)',//767
'Show existing avatars in row',//768
'Search results number',//769
'Search timeout<br>(deprecate to searching more often than once in N seconds)',//770
'Search results limit<br>(limit number of found results to this value)',//771
'How often to refresh messages window in the IM (in seconds)',//772
'Slideshow options',//773
'Can see slideshow',//774
'Slideshow interval in seconds',//775
'Use transformation filter',//776
'Transformation speed',//777
'Can see big photos',//778
'Minimal photos number for slideshow',//779
'Remove profiles marked as deleted, automatically, after number of days<br>(0 - disable this option) ',//780
'Show photos in the Last registered users listing',//781
'User should confirm agreement by checkbox',//782
'Can see number of user near each menu link',//783
'Can enable/disable IM sound when new message received',//784
'By default enable IM sound for new users',//785
'IM options',//786
'Can have blog',//787
'Can see user`s blogs',//788
'Can edit own blog entries',//789
'Can make public/private own blog entries',//790
'Can remove own blog entries',//791
'Can use smiles in the own blog entries',//792
'Make entry public by default',//793
'Number of entries per page',//794
'Can create views',//795
'Number of views per page',//796
'Show small index page',//797
'Can see articles',//798
'Articles should be multilanguage',//799
'Add "-f" send from option to sendmail',//800
'Backup template files when editing',//801
'Can create private photos',//802
'Can see private photos',//803
'How to add new private photos',//804
'Can use glitters in the subject',//805
'Can use glitters in the message',//806
'IDs of forum global moderators, divided by comma, ex: 52,326',//807
'Allow ban for forums',//808
'Can see who is in the User Favorites list',//809
'User should be present in the User Favorites list to see other',//810
'Can see forum posts',//811
'Can use points',//812
'Also sell access levels <br>(uncheck if you wish to sell points only)',//813
'How many smiles to show <br>(if you have more smiles, will show link to new page with all smiles). <br>This option is actual for all forms where smiles exist.<br><br>To edit smiles list upload smiles into template\*\forum\smiles\ and edit file template\*\forum\smiles.php',//814
'English file directory name (default)',//815
'Can search by ZIP radius in the Quick search',//816
'How many private photos can upload each user type',//817
'How many views can create each user type',//818
'How many points provide to new users by gender',//819
'Point multiplier value by gender',//820
'Points plans - number of points in each',//821
'Points prices per each plan',//822
'Access level for new users',//823
'Period of access level for user (in monthes)',//824
'Can create private photos for all users',//825
'Can to hide profile',//826
'Archiving messages',//827
'Remove thumbnails',//828
'Cleaning error photos',//829
'{0} users with {1} pictures have been fixed',//830
'{0} thumbnails have been removed',//831
'Archive messages older than (in days, 90 or higher - recommended)',//832
'Do not archive unread messages',//833
'Cleaning Archive',//834
'{0} messages have been successfully transfered to archive',//835
'Cleaning Cache',//836
'Cache has been successfully cleaned',//837
'Can create private notes',//838
'Can to use smiles',//839
'Send birthday`s congratulations',//840
'Ready to send',//841
'Already sent today',//842
'Dispatch has been successfully finished',//843
'Comments management',//844
'Comment for {0}',//845
'Reply from {0}',//846
'Coupon type',//847
'Points number',//848
'Access level for users filled own profiles (for unlimited time, while profile is filled)',//849
'Access level will be provided only for gender',//850
'Move to private photos',//851
'Suspended profiles',//852
'Suspend',//853
'Successfully suspended user with id={0}',//854
'Are you sure that you want to archive messages by your choice?',//855
'BBCodes Management',//856
'Can to use smiles',//857
'Can to use <b>Bold</b>',//858
'Can to use <i>Italic</i>',//859
'Can to use <u>Underline</u>',//860
'Can to use <s>Strike</s>',//861
'Can to use QUOTES',//862
'Can to use URL',//863
'Can to use IMG',//864
'Can to use VIDEO',//865
'You can to enter own copyrights here. <br>(Only " double quotes can be used)',//866
'Default language for users',//867
'Who can to access to write users with the same gender?',//868
'Genders required to upload photos',//869
'Allow to update profile only once in N hours (0 - disable this option and allow to update profile anytime)',//870
'Search by country',//871
'Search by city',//872
'Search by age',//873
'Show search results per page',//874
'Remove users from online users list after logout',//875
'Secure unmixed login sessions for admin and user <br>(uncheck if you want to make possible to login as user and then as admin without loosing access as user)',//876
'Show special checkbox to check all multiple options',//877
'Show last registered by gender',//878
'After login go to URL (if empty go to check messages).<br> You can to use these variables: {LANGUAGE} is user language, {URL} - website url, {ID} - user id, for ex, My profile page will be: {URL}/view.php?l={LANGUAGE}&id={ID}&MyProfile',//879
'Use "Today", "Yesterday" phrases for dates',//880
'How many stars to show for forum users based on number of posted messages. Each next number shows additional star. Ex: 5,20,50,100,500,1000',//881
'Select star image (You can also upload own into templates/TEMPLATE/images/s0.png)',//882
'Can to see "Features" link',//883
'Can to make own "View Profile" hidden',//884
'Can to be hidden in the "Who seen my profile" list',//885
'Can to select template for own "View profile" page',//886
'Can to link to other profile with description (ex. "My wife")',//887
'Can to prefer not to receive emails from website',//888
'Can to see "Users here" on the forum main page and thread',//889
'Can to see forum statistics',//890
'Enable AJAX(Synchronize - without page reloading) message checking for',//891
'Send AJAX requests every N seconds',//892
'Provide privilegied status for free users, if they filled own profile (Only if notices enabled)',//893
'Image verification (CAPTCHA) algorithm type',//894
'Can <b><u>NOT</u></b> to verify image for',//895
'send message to user form from the "View Profile"',//896
'forum thread and posts',//897
'blog comments',//898
'Enable quality images if possible (this will take additinal server resources)',//899
'Pixels to cut from the sides of thumbnail to improve portrait characteristics',//900
'Can to select access level for each photo',//901
'Limit photos number per each type:',//902
'How many private photos for each type can be uploaded in % (do not write %, just a 4 digitis for each, ex: 40,20,20,20).',//903
'If profile unactive, show the reason',//904
'SEF options',//905
'(SEF) Search Engine Friendly options are working if <b>mod_rewrite</b> is enabled and <b>.htaccess</b> file can be processed. Any changes made here should be also made manually in the <b>.htaccess</b> file of the main directory. Be careful by these options changing - disable SEF, if it will not work.',//906
'Enable SEF',//907
'Encode URL`s titles for non-english characters',//908
'Link to user profile',//909
'Default value:',//910
'Top users',//911
'MySQL tables charset. Leave this table blank by default. If returned find_in_set error, set it to _latin1 or _utf8',//912
'Autosubscribe users for new profiles, when register',//913
'Subscribe for gender',//914
'Subscribe for users with photos',//915
'Remove subscribers, if remove their profiles',//916
'Can prefer not to receive winks',//917
'Can to send winks, if is even deprecated by user',//918
'Webmoney options',//919
'Webmoney ID',//920
'Webmoney Purse',//921
'Can to add links',//922
'Enable Webmoney',//923
'Description size (in chars)',//924
'Request Reciprocal Link URL',//925
'Show Reciprocal Link URL field, even if not requested',//926
'Your reciprocal URL to request to show on the other websites<br>(Use " double quotes only)',//927
'Request exact code (else just your URL will be checked with any description)',//928
'How to approve added links',//929
'Can to Rate photos from the "View Profile"',//930
'Can to Rate photos from "Hot or Not" random page',//931
'Hot or Not is a popularity system for photos',//932
'Can to participate in the "Hot or Not"',//933
'Can to see link to profile, when rate',//934
'Key to crypt URL when "Hot or Not" rating showing. This allow to crypt user ID in URL',//935
'Number of votes for photo requiring to show in the top',//936
'Number of best photos',//937
'Can to access to "More Results" page of the Best photos',//938
'Show full list of all states for most of countries on the Registration page. This will increase size of the registration page up to 600 Kb.',//939
'Limit text size of the blog in search results to number of characters',//940
'Show blogs only for last X days, 0 - unlimited time',//941
'Show new/edited blogs only after admin checking (else it will show authomatically)',//942
'Request from user to select category (Create blogs categories first)',//943
'Minimal number of votes for rating',//944
'Can to comment blogs',//945
'Can to see comments in the blogs',//946
'Ignored users can`t to post in the user blogs',//947
'Disable to comment blog for users with the same gender as author',//948
'Enable badwords checking in the blog text',//949
'Replace badwords with #### (else deprecate to post and show error)',//950
'Enable badword checking in the blog comments',//951
'Can to rate blogs',//952
'How many comments to show per one page (option common for blog and profile comments)',//953
'Can to reply to comments for own blog',//954
'Max image width for [IMG] tag (if bigger, picture will be decreased)',//955
'Make pictures clickable to open in actual size (this will open images on the other websites)',//956
'How many Images in the post are allowed',//957
'Extensions allowed to post',//958
'How many Videos in the post are allowed',//959
'Video width',//960
'Video height',//961
'Can to see linked profile',//962
'Enable privilegies purchasing by points',//963
'Send mail if privilegies purchased by points',//964
'Points number required to purchase access level for 1 hour',//965
'Show banners in the search results after each N users (0 - disable this feature), 5 - shows just 1 banner in the middle of 10 results per search results. Note: banner codes should to be placed into templates/USING_TEMPLATE/banner_search_results.php',//966
'Find Link',//967
'Old format link - please edit to correct URL value',//968
'Status of entry with ID#={0} changed to "{1}"',//969
'Links by category',//970
'Links to show per one page',//971
'Blogs Management',//972
'Is not checked',//973
'Remove all entries entire <br>(uncheck, if you wish to leave all entries and after category deleting, all entries will have undefined Category)',//974
'Approval status',//975
'On the features page show only features, allowed for selected access level ("Guest" will show all features, "Nobody" - features deprecated for all)',//976
'Blogs awaiting approve',//977
'Not confirmed',//978
'Number of days to remove links not confirmed by email (if confirmation by email turn on)',//979
'Links awaiting approve',//980
'Total approved blogs',//981
'Total links',//982
'Total archive messages',//983
'Session lifetime (sec)',//984
'Server OS',//985
'Server API',//986
'Can suspend user profiles',//987
'All is Free - based on Access levels (Enable Payment system)',//988
''
); 

$xr=array(
'Automatically allow all profiles',
'After confirm from email',
'After admin checking',
'After confirm from email and admin checking'
);
$xu=array(
'Hide and wait admin checking',
'Authomaticaly update',
'Show and wait admin checking'
);
$xup=array(
'Always confirm when profile update',
'Only if Photo changed or added',
'Only if Audio changed or added',
'Only if Photo or Audio changed or added'
);
$xd=array(
'Disable remove profiles',
'Show and remove after checking',
'Hide and remove after checking',
'Automaticaly remove profile'
);
$xl=array(
'Don`t show - use default language',
'Don`t show - use browser language',
'Show, if don`t selected by user',
'Always show languages'
);
$xa=array(
'Hidden',
'High and short',
'High and long (biggest type)',
'Low and short (smallest type)',
'Low and long'
);
$xs=array(
'Don`t check IP and Browser',
'Check IP only',
'Check Browser only',
'Check all'
);
$xn=array(
'ID',
'Username (Only If login by Username)',
'FirstName',
'FirstName and LastName',
'ID and Username',
'ID and FirstName',
'ID and FirstName and LastName'
);
$xp=array(
'Work without payment - all is FREE (Disable Payment system)',
'All is free, but user must be a privileged user to read messages from another user',
'All is free, but user must be privileged user to send message to another people. However, if user got message from privileged user, he can answer them :)',
'All is free, but user must be privileged user for read messages and write messages',
'All is Free - based on Access levels (Enable Payment system)');
$xst=array(
'Don`t show statistics',
'Show Statistics without links',
'Show Statistics with links'
);

$xth=array(
'Don`t use thumbnails',
'Generate on the fly',
'Create and store on the server (recommended)'
);
$xlt=array(
'Photo',
'Profile');
$xcache=array('Don`t use Cache','MySQL queries caching');
$xwmtype=array('Text','Image');
$xwmpos=array('Top Left','Top Center','Top Right','Middle Left','Middle Center','Middle Right','Bottom Left','Bottom Center','Bottom Right','Random Position');
$xwmor=array('Automatic','Horizontally','Vertically');
$xpop=array('Disable','Based on different Action`s Hits','Based on Rate System');
$xreg=array('All-In-One','Step by Step');
$xbl=array('All types','Username','Email');
$xmes=array('Show By Messages (Old)','Show By Users (New)');
$xadr=array('Sort by last contacted users','Sort by user`s last access time','Sort by username ascending');
$xtst=array('Authomaticaly','Hide and wait admin checking','Show and wait admin checking');
$xav=array('Disable Audio and Video','Allow to upload audio','Allow to upload video','Allow to upload audio and video');
$xmm=array('Send as mail','Send as internal message','Send as internal message and notice by email');
$xip=array($x[740].'Last registered users (Main page)',$x[740].'Last registered users (More Results)',$x[740].'Most popular users (Main page)',$x[740].'Most popular users (More Results)',$x[740].'Top users with photos (Main page)',$x[740].'Top users with photos (More Results)',$x[740].'Birthdays (Main Page)',$x[740].'Birthdays (More Results)',$x[741].'Quick Search',$x[741].'Simple Search',$x[741].'Advanced Search',$x[741].'Register Page',$x[741].'Subscription Page');
$xsu=array('All new users','Man for women / Woman for men');
$xiva=array('Easy and Faster','Difficult and Slower');
$xhal=array('Authomatically','Check by email','Check by administrator','Check by email and administrator');
?>