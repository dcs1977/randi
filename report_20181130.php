<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 18/04/07           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               terms.php                        #
# File purpose            Terms of use                     #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';
include_once C_PATH.'/header.php';

# Including "Terms of Use" file from language directory
@include_once(C_PATH.'/languages/'.LANGUAGE.'/report_20181130.php');

?>
<script>
    var el = document.getElementById('cookie_attention');
	el.parentNode.removeChild(el);
</script>
<?php
include_once C_PATH.'/footer.php';
?>