<?php 
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 21.09.2008      Last Modified 21.09.2008         #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               go.php                           #
# File purpose            Secure redirect to other website #
# File created by         AzDG <support@azdg.com>          #
############################################################

if(!isset($_GET['url'])) die();
$url = htmlspecialchars($_GET['url']);
$url = str_replace("\"", "", $url);
header("Refresh:0;URL=".$url);
?>