<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               sendk.php                        #
# File purpose            send a wink                      #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';
IsSecure(C_SEND_KISS);
include_once C_PATH.'/sheader.php';

if (isset($id)&&is_numeric($id)) {
unset($m);
/* Checking for allowing to send message */
CheckAllowMessaging(get_info('m'),get_info('d'),True);

/* Checking for allowing to send message to user with the same gender */
IsAllowContact($id,True);

/* Checking for ban */
CheckForIgnor($id);

/* Checking for NoKiss */
CheckNoKiss($id);


$subject=$w[400];
$message=$w[401];
$confirm = (C_KISS_NOTIFY == "1") ? "1" : "3";

CheckAllowMessaging(get_info('m'),get_info('d'),False);

// Send Confirm if it first unreaded email
if(C_KISS_NOTIFY) IsFirstMessage($id);

db_query("INSERT INTO ".C_MYSQL_MESSAGES." VALUES (Null,'".get_info('m')."','".$id."','".$subject."',NOW(),'".ip2int(ip())."','".$confirm."', '0')");
$lastid = db_insert_id();
save_message($lastid, $message);
AddUserToAddressBook($id);

//Push Notification
sendpushnotification($id, $_SESSION['username'], $subject, C_URL.'/members/index.php?l=hu&a=v');

if(C_POPULARITY == '1') pop_in($id,C_HITS_KISS);
sprintm($w[399]);
}
include_once C_PATH.'/sfooter.php';
?>