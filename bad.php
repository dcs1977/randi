<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               bad.php                          #
# File purpose            Sent bad profiles to admin       #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';
ssecurity(TRUE,$w[152]);
include_once C_PATH.'/sheader.php';
if(!IsAccess(C_BADSH)) sprintm($w[153]);

if (isset($id)&&is_numeric($id)) {

	if(isset($do)) {
		if( !isset($reason) || (isset($reason) && trim($reason) == '')) sprintm($w[403]);
		$reason = cb($reason);
		/*$sql = db_query("SELECT count(id) as total FROM ".C_MYSQL_BAD_PROFILES." WHERE user = '".$id."'") or die(db_error());
		$trows = db_fetch_array($sql);
		$total = $trows['total'];
		if($total != '0') sprintm($w[154]);*/

		unset($m);
		$who = (!is_numeric(get_info('m'))) ?  '0' :  get_info('m');
		db_query("INSERT INTO ".C_MYSQL_BAD_PROFILES." VALUES(null,NOW(),'".ip2int(ip())."','".$who."','".$id."','".$reason."',0)") or die(db_error());
		
		if($_FILES["file"]["name"]) {
			$name_file = db_insert_id();
			$upload_folder = C_PATH.'/bad/';
			$upload_file = basename($_FILES['file']['name']);
			
			if (move_uploaded_file($_FILES['file']['tmp_name'], $upload_folder.$upload_file)) {
				$zip = new ZipArchive();
				$fileconpress = $upload_folder.$name_file.".zip";

				$conpress = $zip->open($fileconpress, ZIPARCHIVE::CREATE);
				if ($conpress === true)
				{
					$zip->addFile($upload_folder.$upload_file, $upload_file);
					$zip->close();
					unlink($upload_folder.$upload_file);
				}
			}
		}

		if(GetStatus($id) == 10){
            ChangeStatus($id, '6');
        }
		
		sprintm($w[155]);
	} else {
		/*$sql = db_query("SELECT count(id) as total FROM ".C_MYSQL_BAD_PROFILES." WHERE user = '".$id."'") or die(db_error());
		$trows = db_fetch_array($sql);
		$total = $trows['total'];
		if($total != '0') sprintm($w[154]);*/
		define('REPORT_THIS_PROFILE',$w[218]);
		define('ENTER_YOUR_REASON',$w[403]);
		define('SEND_MESSAGE',$w[170]);
		define('ID_VALUE',$id);
	  if(isset($uname)) define('USER_NAME',$uname);
		else define('USER_NAME','');

		include_once C_PATH.'/templates/'.C_TEMP.'/bad.php';
	}
}
include_once C_PATH.'/sfooter.php';
?>