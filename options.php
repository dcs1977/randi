<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               options.php                      #
# File purpose            Allow to change some settings    #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';
IsSecure(C_OPTIONS);

if(isset($act) && ($act == 'change'))
{
	if(IsAccess(C_CAN_NO_RATE))
	{
		$rate_value = (isset($rate) && $rate == 'on') ? 1 : 0;
		db_query("UPDATE ".C_MYSQL_MEMBERS_MAIN." SET rate = '".$rate_value."' WHERE id = '".MyID()."'");
		if(!$rate_value) @db_query("DELETE FROM ".C_MYSQL_HOTORNOT." WHERE uid = '".MyID()."'");
	}
	if(IsAccess(C_MY_TEMP))
	{
		$MyTemplate_value = (isset($MyTemplate) && (array_search($MyTemplate,GetTemplates()))) ? $MyTemplate : '';
		db_query("UPDATE ".C_MYSQL_MEMBERS_EXT." SET mytemp = '".$MyTemplate."' WHERE id = '".MyID()."'");
	}
	if(IsAccess(C_OPT_NOMAIL) && IsAccess(C_NO_KISS))
	{
		$nomail_value = (isset($nomail) && $nomail == 'on') ? 1 : 0;
		$nokiss_value = (isset($nokiss) && $nokiss == 'on') ? 1 : 0;
		db_query("UPDATE ".C_MYSQL_MEMBERS_SEC." SET nomail = '".$nomail_value."', nokiss = '".$nokiss_value."' WHERE id = '".MyID()."'");
	}
	else
	{
		if(IsAccess(C_OPT_NOMAIL))
		{
			$nomail_value = (isset($nomail) && $nomail == 'on') ? 1 : 0;
			db_query("UPDATE ".C_MYSQL_MEMBERS_SEC." SET nomail = '".$nomail_value."' WHERE id = '".MyID()."'");
		}
		if(IsAccess(C_NO_KISS))
		{
			$nokiss_value = (isset($nokiss) && $nokiss == 'on') ? 1 : 0;
			db_query("UPDATE ".C_MYSQL_MEMBERS_SEC." SET nokiss = '".$nokiss_value."' WHERE id = '".MyID()."'");
		}
	}
	if(IsAccess(C_HIDE_PROFILE))
	{
		$hide_value = (isset($hide) && $hide == 'on') ? 1 : 0;
		db_query("UPDATE ".C_MYSQL_MEMBERS_MAIN." SET hide = '".$hide_value."' WHERE id = '".MyID()."'");
		if (C_AUTH)	$_SESSION['hd'] = $hide_value;
		else	{$cr64 = new AzDGCrypt(C_CRYPT_KEY);set_login('hd', $cr64->crypt($hide_value));}
	}
	$timediff = str_replace('+','',$timediff);
	$timediff = (is_numeric($timediff) && $timediff > -25 && $timediff < 25) ? $timediff : 0;
	$imsound = (isset($imsound)) ? 1 : 0;
	db_query("REPLACE INTO ".C_MYSQL_OPTIONS." SET id = '".MyID()."', timediff = '".$timediff."', imsound = '".$imsound."'");
	if (C_AUTH)
	{
		$_SESSION['opt_timediff'] = $timediff;
		$_SESSION['imsound'] = $imsound;
	}
	else
	{
		// If working on Cookies
		$cr64 = new AzDGCrypt(C_CRYPT_KEY);
		set_login('opt_timediff', $cr64->crypt($timediff));
		set_login('imsound', $cr64->crypt($imsound));
	}

	include_once C_PATH.'/header.php';
	printm($w[518]);


}
elseif(isset($act) && ($act == 'change_link'))
{
	if(IsAccess(C_PROF_LNK))
	{
		$link_value = (isset($link) && is_numeric($link)) ? $link : 0;
		if($link_value)
		{
			if($link_value == MyID()) printm($w[809]);
			if(SQLCount(C_MYSQL_MEMBERS_SEC, " id = '".$link."' AND status > '5'"))
			{
				$link_txt_value = cb(replace_badwords($link_txt));
				if(trim($link_txt_value) == '') printm($w[164]);
				db_query("UPDATE ".C_MYSQL_MEMBERS_SEC." SET link = '".$link_value."', link_txt = '".$link_txt_value."' WHERE id = '".MyID()."'");
				printm($w[518]);
			} else  printm($w[204]);
		} printm($w[185]);
	}
}
else
{
	include_once C_PATH.'/header.php';

	$trows = db_fetch_array(db_query("SELECT t1.rate, t1.hide, t2.nomail, t2.nokiss, t3.mytemp FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_SEC." as t2, ".C_MYSQL_MEMBERS_EXT." as t3 WHERE t1.id = '".MyID()."' AND t1.id = t3.id AND t1.id = t2.id AND t1.status >= '7'"));
	$rate = ($trows['rate']) ? ' checked' : '';
	$hide = ($trows['hide']) ? ' checked' : '';
	$nomail = ($trows['nomail']) ? ' checked' : '';
	$nokiss = ($trows['nokiss']) ? ' checked' : '';
	$mytemp = $trows['mytemp'];

	$handle=opendir(C_PATH.'/templates/'); $out=$out2='';
	while (false!==($TemplateName = readdir($handle)))
	{
		if ($TemplateName != "." && $TemplateName != ".." && is_dir(C_PATH.'/templates/'.$TemplateName))
		{
			$select_current = ((isset($_COOKIE['TemplateName']) && ( $_COOKIE['TemplateName'] == $TemplateName)) || (!isset($_COOKIE['TemplateName']) && ( C_TEMP == $TemplateName))) ? 'selected' : '';
			$select_current2 = (strtolower($mytemp) == strtolower($TemplateName)) ? 'selected' : '';
			$out.='<option value="'.$TemplateName.'" '.$select_current.'>'.ucfirst($TemplateName);
			$out2.='<option value="'.$TemplateName.'" '.$select_current2.'>'.ucfirst($TemplateName);
		}
	}
	closedir($handle);

	define('PARTICIPATE_IN_RATE',$w[517]);
	define('IMSOUND',$w[638]);
	define('HIDE_PROFILE',$w[745]);
	define('NOMAIL',$w[762]);
	define('NOKISS',$w[767]);
	define('PARTICIPATE_IN_RATE_VALUE',$rate);
	define('HIDE_PROFILE_VALUE',$hide);
	define('NOMAIL_VALUE',$nomail);
	define('NOKISS_VALUE',$nokiss);
	define('AVAILABLE_OPTIONS',$out);
	define('MY_AVAILABLE_OPTIONS',$out2);
	define('CHANGE_OPTIONS',$w[423]);
	define('MY_DESIGN',$w[766]);

	if(IsAccess(C_PROF_LNK))
	{
		#Linking profiles
		define('LINKING_PROFILES',$w[806]);
		define('LINKING_NOTE',$w[807]);
		define('LINKING_DESC',$w[808]);
		define('USER_ID',$w[126]);

		$i=SQLSelect(C_MYSQL_MEMBERS_SEC, array('link','link_txt')," id = '".MyID()."'");
		if($i)
		{
			define('USER_ID_VALUE',$i['link']);
			define('LINKING_DESCR',$i['link_txt']);
		} else {
			define('USER_ID_VALUE',0);
			define('LINKING_DESCR','');
		}
	}

	$tdiff = (is_numeric(get_info('opt_timediff'))) ? get_info('opt_timediff') + cb(C_TIME_DIFF) : cb(C_TIME_DIFF);
	$tmp=db_query("SELECT DATE_ADD(NOW(), INTERVAL ".cb(C_TIME_DIFF)." HOUR) as sdate, DATE_ADD(NOW(), INTERVAL ".$tdiff." HOUR) as mydate");
	$tq=db_fetch_array($tmp);
	define('TIME_DIFFERENCE',$w[545]);
	define('CURRENT_DATE',template($w[546],array(mysql2data($tq['sdate'],0,1))));
	define('MY_CURRENT_DATE',template($w[547],array(mysql2data($tq['mydate'],0,1))));
	$tmp=db_query("SELECT * FROM ".C_MYSQL_OPTIONS." WHERE id = ".MyID());
	$count=db_num_rows($tmp);
	$tq=db_fetch_array($tmp);
	if(!$count)
	{
		define('CURRENT_DATE_VALUE',0);
	}
	else
	{
		$tdif = ($tq['timediff'] > 0) ? '+'.$tq['timediff'] : $tq['timediff'];
		define('CURRENT_DATE_VALUE',$tdif);

	}
	if($tq['imsound']) define('IMSOUND_VALUE','checked');
	else define('IMSOUND_VALUE','');

	include_once C_PATH.'/templates/'.C_TEMP.'/options.php';
}
include_once C_PATH.'/footer.php';
?>