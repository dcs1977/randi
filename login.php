<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               login.php                        #
# File purpose            Login users to members area      #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';

if(!isset($p)) $p='';
if ($p == "s") {
    $filename = 'login-'.date('Y-m').'.log';
    $folder = C_PATH.'/logs/';
    $file_open_new = fopen($folder.$filename, 'a');
    fwrite($file_open_new, 'Login: '.cb($id).' - '.date('Y-m-d H:i:s').PHP_EOL);
    fclose($file_open_new);

	if(C_VERIFY_IMAGE_LOGIN) {
		$verifyimage = cb($verifyimage);
		if(strtoupper($verifyimage) != strtoupper(@$_SESSION['vc']))
		{
			include_once C_PATH.'/header.php';
			printm($w[497]);
		}
	}
	@MakeExit();
	$inquiry='';$usr='';

	if(isset($id)) $id=trim($id);
	switch (C_ID) {
		case '0':
		if (!preg_match("/^[".C_LOGIN_CHARS."]{3,16}$/", $id) || empty($id)) {
			include_once C_PATH.'/header.php';
			printm($w[266]);
		}
		$inquiry=" (t1.username='".cb($id)."' OR t1.username='#".cb($id)."' OR t1.username='##".cb($id)."')";
		$usr="t1.username,";
		break;
		case '2':
		if (c_email($id) == 0) {
			include_once C_PATH.'/header.php';
			printm($w[11]);
		}
		$inquiry=" t3.email='".cbmail($id)."'";
		break;
		default: // default by ID
		if (!is_numeric($id) || empty($id)) {
			include_once C_PATH.'/header.php';
			printm($w[185]);
		}
		$inquiry=" t1.id='".$id."'";
		break;
	}
	$tmp=db_query("SELECT t1.*, t2.*, ".$usr."t3.email, t3.password, t3.wantpriv, t3.approved_term, t3.ip, t3.fingerprint, t3.template FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2, ".C_MYSQL_MEMBERS_SEC." as t3 WHERE".$inquiry." AND t1.id = t3.id AND t1.id = t2.id ORDER BY t1.editdate DESC LIMIT 1");
	$count=db_num_rows($tmp);
	if($count == '0') {
		include_once C_PATH.'/header.php';
		printm($w[186]);
	}

	while($i=db_fetch_array($tmp)) {

		if(in_array($i['status'], array('0','1', '2', '5', '6'))){
			include_once C_PATH.'/header.php';
			switch($i['status']) {
				case '0':
				printm($w[499]);
				break;
				case '1':
				printm($w[500]);
				break;
				case '2':
				printm($w[501]);
				break;
				default:
				printm($w[504]);
				break;
			}
		}

		$ptmp = db_query("SELECT SHA2('".$password."', 256) as password");
		$pi = db_fetch_array($ptmp);
		
		if($pi['password'] == $i['password'])
		{
		    $status = $i['status'];

			if($i['status'] == 3 || $i['status'] == 4){
                $status = 9;
                //ip check
                $ipsql = "SELECT count(t3.id) as total FROM ".C_MYSQL_MEMBERS_SEC." as t3 WHERE ip = ".$i['ip']." AND status IN (7,8,9,10)";

                $ipquery = db_query($ipsql) or die(db_error());

                $iprows = db_fetch_array($ipquery);
                if($iprows['total'] > 0) {
                    $status = $i['status'];
                } else {
                    //password check
                    $passsql = "SELECT count(t3.id) as total FROM ".C_MYSQL_MEMBERS_SEC." as t3 WHERE password = '".$i['password']."' AND status IN (7,8,9,10)";

                    $passquery = db_query($passsql) or die(db_error());

                    $passrows = db_fetch_array($passquery);
                    if($passrows['total'] > 0) {
                        $status = $i['status'];
                    } elseif ($i['fingerprint']){
                        //fingerprint check
                        $fpsql = "SELECT count(t3.id) as total FROM ".C_MYSQL_MEMBERS_SEC." as t3 WHERE fingerprint = '".$i['fingerprint']."' AND status IN (7,8,9,10)";

                        $fpquery = db_query($fpsql) or die(db_error());

                        $fprows = db_fetch_array($fpquery);
                        if($fprows['total'] > 0) {
                            $status = $i['status'];
                        }
                    }
                }
			}

			if ($status != $i['status'] && in_array($status, array('7', '8', '9', '10'))){
                ChangeStatus($i['id'], $status);
                $i['status'] = $status;
            }

			if ($status == $i['status'] && in_array($i['status'], array('7', '8', '9', '10'))){
			    //if (CheckDevices($i['ip'], $['fingerprint'])){
                    if(!isset($i['username'])) $i['username']='';
                    $arr1=array($i['fname'],$i['lname'],$i['birthday'],$i['purposes'],$i['orientation'],$i['bodytype'],$i['country'],$i['url'],$i['icq'],$i['aim'],$i['phone'],$i['state'],$i['zip'],$i['city'],$i['address'],$i['marstat'],$i['child'],$i['height'],$i['weight'],$i['hcolor'],$i['ecolor'],$i['etnicity'],$i['religion'],$i['smoke'],$i['drink'],$i['education'],$i['languages'],$i['mylang'],$i['job'],$i['hobby'],$i['interests']);
                    $arr2=array($i['descr'],$i['sgender'],$i['scountry'],$i['smarstat'],$i['schild'],$i['shcolor'],$i['secolor'],$i['ssmoke'],$i['sdrink'],$i['seducation'],$i['setnicity'],$i['sreligion'],$i['files'],$i['audio'],$i['hdyfu']);
                    $notices=CheckNotices($arr1,$arr2);
                    define('ID',$i['id']);
                    MakeLogin($i['id'], $i['username'], $i['fname'], $i['lname'], $i['gender'], $i['req'], $i['points'], $i['hide'],$notices);
                    $_SESSION['lastlogin']=$i['editdate'];
                    $_SESSION['approved_term'] = $i['approved_term'];
                    $_SESSION['mobile'] = $i['template'];
                /*} else {
                    ChangeStatus($i['id'], 6);
                    unset($s);unset($m);unset($g);unset($o);
                    include_once C_PATH.'/header.php';
                    printm($w[504]);
                }*/
				$ip = ip();
                $fingerprint = $_COOKIE['fingerprint'];
				$secure = CheckDevices(ip2int($ip), $fingerprint, $browser);
				if ($secure === false){
                    //ChangeStatus($i['id'], 6);
                    //do{
                    //    $codegen = random_str(10);
                    //    $temp = db_query("SELECT count(*) as total FROM ".C_MYSQL_TEMP." WHERE code='".$codegen."'");
                    //    $row = db_fetch_array($temp);
                    //    $count = $row['total'];
                    //}while($count > 0);

                    //$tm = array(C_URL.'/check.php?l='.$l.'&code=', $codegen);
$tm = '';
                    $html = template($wl['shield_protection']['html'], $tm);
                    $text = template($wl['shield_protection']['text'], $tm);
                    //db_query("INSERT INTO ".C_MYSQL_TEMP." VALUES ('".$i['id']."',NOW(),'".$codegen."', 9)");
                    sendmail(C_FROMM, $i['email'], $wl['shield_protection']['subject'], $html, $text);

                    //include_once C_PATH.'/header.php';
                    //printm($w[499]);
                }
            } else {
                unset($s);unset($m);unset($g);unset($o);
                include_once C_PATH.'/header.php';
                printm($w[504]);
            }
		}
		else
		{
			unset($s);unset($m);unset($g);unset($o);
			include_once C_PATH.'/header.php';
			printm($w[186]);
		}
	}

	$gturl = array('LANGUAGE'=>LANGUAGE,'URL'=>C_URL,'ID'=>ID);
	//if(C_LOGIN_URL == '') $goto = (isset($goto) && !empty($goto)) ? $goto : C_URL.'/members/index.php?l='.$l.'&a=v';
	if(C_LOGIN_URL == '') $goto = (isset($goto) && !empty($goto)) ? $goto : C_URL.'/index.php?l='.$l;
	else $goto = (isset($goto) && !empty($goto)) ? $goto : template(C_LOGIN_URL,$gturl);

	GoToURL(urldecode($goto));
}
else {
	include_once C_PATH.'/header.php';
	if (C_VERIFY_IMAGE_LOGIN)
	{
		define('VERIFY_ALLOW', '1');
		define('VERIFY_ERROR', $w[497]);
	}
	else
	{
		define('VERIFY_ALLOW', '0');
		define('VERIFY_ERROR', '');
	}

	define('VERIFICATION', $w[498]);
	define('LOGIN_NAME',login(C_ID));
	define('PASSWORD',$w[54]);
	define('ENTER',$w[263]);
	define('FORGOT_PASSWORD',$w[173]);
	$goto = (isset($goto)) ? $goto : '';
	define('GO_TO',$goto);
	
	include_once C_PATH.'/templates/'.C_TEMP.'/login.php';
	include_once C_PATH.'/footer.php';
}
?>