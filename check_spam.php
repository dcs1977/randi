<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 18/04/07           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               terms.php                        #
# File purpose            Terms of use                     #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';
include_once C_PATH.'/header.php';

function dump(){
    echo "<pre>";
    foreach (func_get_args() as $var) {
        if (is_string($var)){
            echo '"'.$var.'"';
        } else {
            echo json_encode($var);
        }
        echo '&nbsp;';
    }
    echo "</pre><hr/>";
}

function transliterate($txt) {
    $transliterationTable = array(
        'á' => 'a', 'Á' => 'A', 'à' => 'a', 'À' => 'A', 'ă' => 'a', 'Ă' => 'A', 'â' => 'a', 'Â' => 'A', 'å' => 'a', 'Å' => 'A', 'ã' => 'a', 'Ã' => 'A', 
        'ą' => 'a', 'Ą' => 'A', 'ā' => 'a', 'Ā' => 'A', 'ä' => 'a', 'Ä' => 'A', 'æ' => 'e', 'Æ' => 'E', 
        'ḃ' => 'b', 'Ḃ' => 'B', 
        'ć' => 'c', 'Ć' => 'C', 'ĉ' => 'c', 'Ĉ' => 'C', 'č' => 'c', 'Č' => 'C', 'ċ' => 'c', 'Ċ' => 'C', 'ç' => 'c', 'Ç' => 'C', 
        'ď' => 'd', 'Ď' => 'D', 'ḋ' => 'd', 'Ḋ' => 'D', 'đ' => 'd', 'Đ' => 'D', 'ð' => 'd', 'Ð' => 'D', 
        'é' => 'e', 'É' => 'E', 'è' => 'e', 'È' => 'E', 'ĕ' => 'e', 'Ĕ' => 'E', 'ê' => 'e', 'Ê' => 'E', 'ě' => 'e', 'Ě' => 'E', 'ë' => 'e', 'Ë' => 'E', 
        'ė' => 'e', 'Ė' => 'E', 'ę' => 'e', 'Ę' => 'E', 'ē' => 'e', 'Ē' => 'E', 
        'ḟ' => 'f', 'Ḟ' => 'F', 'ƒ' => 'f', 'Ƒ' => 'F', 
        'ğ' => 'g', 'Ğ' => 'G', 'ĝ' => 'g', 'Ĝ' => 'G', 'ġ' => 'g', 'Ġ' => 'G', 'ģ' => 'g', 'Ģ' => 'G', 
        'ĥ' => 'h', 'Ĥ' => 'H', 'ħ' => 'h', 'Ħ' => 'H', 
        'í' => 'i', 'Í' => 'I', 'ì' => 'i', 'Ì' => 'I', 'î' => 'i', 'Î' => 'I', 'ï' => 'i', 'Ï' => 'I', 'ĩ' => 'i', 'Ĩ' => 'I', 'į' => 'i', 'Į' => 'I', 
        'ī' => 'i', 'Ī' => 'I', 
        'ĵ' => 'j', 'Ĵ' => 'J', 'ķ' => 'k', 'Ķ' => 'K', 
        'ĺ' => 'i', 'Ĺ' => 'L', 'ľ' => 'I', 'Ľ' => 'L', 'ļ' => 'l', 'Ļ' => 'L', 'ł' => 'l', 'Ł' => 'L', 
        'ṁ' => 'm', 'Ṁ' => 'M', 
        'ń' => 'n', 'Ń' => 'N', 'ň' => 'n', 'Ň' => 'N', 'ñ' => 'n', 'Ñ' => 'N', 'ņ' => 'n', 'Ņ' => 'N', 
        'ó' => 'o', 'Ó' => 'O', 'ò' => 'o', 'Ò' => 'O', 'ô' => 'o', 'Ô' => 'O', 'ő' => 'o', 'Ő' => 'O', 'õ' => 'o', 'Õ' => 'O', 'ø' => 'o', 'Ø' => 'O', 
        'ō' => 'o', 'Ō' => 'O', 'ơ' => 'o', 'Ơ' => 'O', 'ö' => 'oe', 'Ö' => 'O', 
        'ṗ' => 'p', 'Ṗ' => 'P', 
        'ŕ' => 'r', 'Ŕ' => 'R', 'ř' => 'r', 'Ř' => 'R', 'ŗ' => 'r', 'Ŗ' => 'R', 
        'ś' => 's', 'Ś' => 'S', 'ŝ' => 's', 'Ŝ' => 'S', 'š' => 's', 'Š' => 'S', 'ṡ' => 's', 'Ṡ' => 'S', 'ş' => 's', 'Ş' => 'S', 'ș' => 's', 'Ș' => 'S', 
        'ß' => 'S', 
        'ť' => 't', 'Ť' => 'T', 'ṫ' => 't', 'Ṫ' => 'T', 'ţ' => 't', 'Ţ' => 'T', 'ț' => 't', 'Ț' => 'T', 'ŧ' => 't', 'Ŧ' => 'T', 
        'ú' => 'u', 'Ú' => 'U', 'ù' => 'u', 'Ù' => 'U', 'ŭ' => 'u', 'Ŭ' => 'U', 'û' => 'u', 'Û' => 'U', 'ů' => 'u', 'Ů' => 'U', 'ű' => 'u', 'Ű' => 'U', 
        'ũ' => 'u', 'Ũ' => 'U', 'ų' => 'u', 'Ų' => 'U', 'ū' => 'u', 'Ū' => 'U', 'ư' => 'u', 'Ư' => 'U', 'ü' => 'u', 'Ü' => 'U', 
        'ẃ' => 'w', 'Ẃ' => 'W', 'ẁ' => 'w', 'Ẁ' => 'W', 'ŵ' => 'w', 'Ŵ' => 'W', 'ẅ' => 'w', 'Ẅ' => 'W', 
        'ý' => 'y', 'Ý' => 'Y', 'ỳ' => 'y', 'Ỳ' => 'Y', 'ŷ' => 'y', 'Ŷ' => 'Y', 'ÿ' => 'y', 'Ÿ' => 'Y', 
        'ź' => 'z', 'Ź' => 'Z', 'ž' => 'z', 'Ž' => 'Z', 'ż' => 'z', 'Ż' => 'Z', 
        'þ' => 'th', 'Þ' => 'Th', 'µ' => 'u', 'а' => 'a', 'А' => 'a', 'б' => 'b', 'Б' => 'b', 'в' => 'b', 'В' => 'B', 'г' => 't', 'Г' => 'T', 'д' => 'a', 
        'Д' => 'A', 'е' => 'e', 'Е' => 'E', 'ё' => 'e', 'Ё' => 'E', 'ж' => 'x', 'Ж' => 'X', 'з' => 'e', 'З' => 'E', 'и' => 'n', 'И' => 'N', 'й' => 'n', 
        'Й' => 'N', 'к' => 'k', 'К' => 'k', 'л' => 'h', 'Л' => 'H', 'м' => 'm', 'М' => 'M', 'н' => 'h', 'Н' => 'H', 'о' => 'o', 'О' => 'o', 'п' => 'n', 
        'П' => 'N', 'р' => 'p', 'Р' => 'P', 'с' => 'c', 'С' => 'C', 'т' => 't', 'Т' => 't', 'у' => 'y', 'У' => 'Y', 'ф' => 'f', 'Ф' => 'f', 'х' => 'x', 
        'Х' => 'X', 'ц' => 'u', 'Ц' => 'U', 'ч' => 'u', 'Ч' => 'U', 'ш' => 'm', 'Ш' => 'M', 'щ' => 'w', 'Щ' => 'W', 'ъ' => '', 'Ъ' => '', 
        'ы' => 'y', 'Ы' => 'y', 'ь' => '', 'Ь' => '', 'э' => 's', 'Э' => 'S', 'ю' => 'ju', 'Ю' => 'ju', 'я' => 'ja', 'Я' => 'ja',
        'ｍ' => 'm', 'ａ' => 'a', 'ｃ' => 'c', 'ｏ' => 'o', 'ｍ' => 'm', 'ｉ' => 'i', 'ｄ' => 'd');
    $return = str_replace(array_keys($transliterationTable), array_values($transliterationTable), $txt);
    
    $array = str_split($return);
    $return = '';
    
    foreach ($array as $a){
        if (json_encode($a) !== 'null'){
            $return .= str_replace('"', '', json_encode($a));
        }
    }
    return $return;
}

function _same_words($base, $target){
    dump(__LINE__.':', $base, $target);
    $base = explode(' ', $base);
    $target = explode(' ', $target);
    dump($base);
    dump($target);

    $word_count = 0;
    $base_count = count($base);
    $target_count = count($target);
    dump(__LINE__.':', $base_count, $target_count);

    if ($base_count < 4 || $target_count < 4){
        return false;
    }

    foreach ($base as $base_word){
        foreach ($target as $target_word){
            if ($base_word == $target_word){
                dump(__LINE__.':', $base_word, $target_word);
                $word_count++;
                continue 2;
            }
        }
    }
    dump(__LINE__.':', $word_count);

    //return false;
    if ($base_count > $target_count){
        dump(__LINE__.':', ($base_count - $word_count));
        return (($base_count - $word_count) <= 3);
    } else {
        dump(__LINE__.':', ($target_count - $word_count));
        return (($target_count - $word_count) <= 3);
    }
}

function CheckSpamMessage($message_id){
    Global $w;
    
    $message = db_query("SELECT * FROM ".C_MYSQL_MESSAGES." WHERE id = '".$message_id."'");
    while ($i = db_fetch_array($message)) {
        $user_id = $i['fromid'];
        $toid = $i['toid']; 
        $message = read_message($i['id']);
        $sendtime = $i['sendtime'];
    }
    //dump(__LINE__.':', $user_id, $toid, $message);
    
    $is_spam = false;
    $is_link = false;
    $is_first = false;
    $check_num = 30;
    
    if (strlen($message) >= 20){
        $pattern = "/(?i)\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))/i";
        if (preg_match_all($pattern,$message,$matches)) {
            $is_link = true;
        } else {
            $pattern = "/\[\/?URL[^\]]*\]/i";
            if (preg_match_all($pattern,$message,$matches)) {
                $is_link = true;
            }
        }

        //if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $check = db_query("SELECT * FROM ".C_MYSQL_MESSAGES." WHERE fromid = '".$user_id."' AND toid = '".$toid."' AND subject = '" . $w['862'] . "'");
            $num_rows = db_num_rows($check);            
            $is_first = ($num_rows == 1);
            //dump(__LINE__.':', $is_link, $num_rows, $is_first);
                        
            if ($is_link){
                if ($is_first){
                    $check_num = 5;
                    $spam_alert_num = 1;
                    $before_time = 3600;
                } else {
                    $check_num = 15;
                    $spam_alert_num = 5;
                    $before_time = 1800;
                }
            } else {
                if ($is_first){
                    $check_num = 15;
                    $spam_alert_num = 5;
                    $before_time = 1800;
                } else {
                    $check_num = 30;
                    $spam_alert_num = 5;
                    $before_time = 900;
                }
            }
            
            $before = date("Y-m-d H:i:s", strtotime($sendtime) - $before_time);
            $query = "SELECT * FROM ".C_MYSQL_MESSAGES." WHERE sendtime >= '".$before."' AND fromid = '".$user_id."' AND subject = '" . $w['862'] . "'";
            $tmp = db_query($query);
            $total = db_num_rows($tmp);
            
            //dump(__LINE__.':',$query,$total,$check_num,$spam_alert_num,$before_time,$before); 
            if ($total >= $check_num ) {
                $same_message = 0;
                $message_slug = transliterate($message);
                //dump(__LINE__.':',$message,$message_slug);
                //Read the last messages
                while ($i = db_fetch_array($tmp)){
                    $before_message = read_message($i['id']);
                    $before_message_slug = transliterate($before_message);
                    //dump(__LINE__.':',$before_message,$before_message_slug);
                    if ($message_slug == $before_message_slug){
                        $same_message++;
                    } else {
                        $same_words = _same_words($message_slug, $before_message_slug);
                        if ($same_words){
                            $same_message++;
                        }
                    }
                    
                    //dump(__LINE__.':',$same_message);
                    if ($same_message >= 10){
                        $is_spam = true;

                        $user_q = db_query("SELECT * FROM ".C_MYSQL_MEMBERS_SEC." WHERE id = '".$user_id."'");
                        while ($j = db_fetch_array($user_q))
                        {
                            $email = $j['email'];
                        }
                        $save_log = empty_log_array();
                        $save_log['time'] = date('Y-m-d H:i:s');
                        $save_log['id'] = $user_id;
                        $save_log['email'] = $email;
                        $save_log['action'] = 'Spamgyanú';
                        log_csv(array($save_log));

                        //sprintm($w[1]);
                        $get_logs = user_csv($user_id, NULL, 'Spamgyanú');
                        
                        if (count($get_logs) > $spam_alert_num){
                            ChangeStatus($user_id, '6');
                        }
                    }
                }
                
            }
        //}
    }
    
    //$is_spam = false;
    dump(__LINE__.':',$is_spam);
    if ($is_spam === true){
        return true;
    } else {
        return false; 
    }
}

CheckSpamMessage($id);

include_once C_PATH.'/footer.php';
?>