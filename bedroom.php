<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               bedroom.php                      #
# File purpose            Add users to bedroom             #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';
ssecurity(TRUE,$w[152]);
include_once C_PATH.'/sheader.php';
if(!IsAccess(C_BEDSH)) sprintm($w[156]);

if (isset($id)&&is_numeric($id)) {
unset($m);

/* Checking for ban */
CheckForIgnor($id,True);

$sql = db_query("SELECT count(id) as total FROM ".C_MYSQL_BEDROOM." WHERE user = '".$id."' AND myid='".get_info('m')."'") or die(db_error());
$trows = db_fetch_array($sql);
$total = $trows['total'];
if($total != '0') sprintm($w[157]);

if(!isset($reason)) $reason='';
db_query("INSERT INTO ".C_MYSQL_BEDROOM." VALUES(null,'".get_info('m')."',NOW(),'".$id."','".cb($reason)."')") or die(db_error());

$subject=$w[332];
$tm=array('<a href='.C_URL.'/view.php?l='.$l.'&id='.get_info('m').'>'.get_info('m').'</a>');
$message=template($w[333],$tm);
if(C_ADD_CONFIRM) {
  db_query("INSERT INTO ".C_MYSQL_MESSAGES." VALUES (Null,'".get_info('m')."','".$id."','".$subject."',NOW(),'".ip2int(ip())."','3', '0')");
  $lastid = db_insert_id();
  save_message($lastid, $message);

  //Push Notification
  sendpushnotification($id, $_SESSION['username'], $subject, C_URL.'/members/index.php?l=hu&a=v');
	AddUserToAddressBook($id);
}

if(C_POPULARITY == '1') pop_in($id,C_HITS_BEDROOM);
sprintm($w[158]);

/*
	if(isset($do)) {
	if(!isset($reason)) $reason='';
db_query("INSERT INTO ".C_MYSQL_BEDROOM." VALUES(null,'".get_info('m')."',NOW(),'".$id."','".cb($reason)."')") or die(db_error());

$subject=$w[332];
$tm=array('<a href='.C_URL.'/view.php?l='.$l.'&id='.get_info('m').'>'.get_info('m').'</a>');
$message=template($w[333],$tm);
if(C_ADD_CONFIRM) {
  db_query("INSERT INTO ".C_MYSQL_MESSAGES." VALUES (Null,'".get_info('m')."','".$id."','".$subject."',NOW(),'".ip2int(ip())."','3', '0')");
  $lastid = db_insert_id();
  save_message($lastid, $message);

  //Push Notification
  sendpushnotification($id, $_SESSION['username'], $subject, C_URL.'/members/index.php?l=hu&a=v');
	AddUserToAddressBook($id);
}

if(C_POPULARITY == '1') pop_in($id,C_HITS_BEDROOM);
sprintm($w[158]);
	} else {
		define('ADD_TO_LIST',$w[219]);
		define('ENTER_YOUR_REASON',$w[521]);
		define('SAVE_FAVOURITE',$w[219]);
		define('ID_VALUE',$id);
	  if(isset($uname)) define('USER_NAME',$uname);
		else define('USER_NAME','');

		include_once C_PATH.'/templates/'.C_TEMP.'/bedroom.php';
	}
*/
}
include_once C_PATH.'/sfooter.php';
?>
