<?php
@ob_start();$k=@ob_get_contents();
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               img.php                          #
# File purpose            Show Picture for verify          #
# File created by         AzDG <support@azdg.com>          #
############################################################
error_reporting(0);
define('DONT_COUNT_ONLINE',True);
require_once('include/config.inc.php');
require_once('include/options.inc.php');
if(C_VERIFY_ALG) require_once(C_PATH.'/classes/VerifyImage2.class.php');
else require_once(C_PATH.'/classes/VerifyImage.class.php');
@ob_end_clean();
$p=new VerifyImage();
echo $p->ShowImage();
?>