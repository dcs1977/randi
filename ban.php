<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 27/07/03        Last Modified 27/07/03           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               ban.php                          #
# File purpose            Add users to banlist             #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';
ssecurity(TRUE,$w[152]);
include_once C_PATH.'/sheader.php';
if(!IsAccess(C_BANSH)) sprintm($w[321]);

if (isset($id)&&is_numeric($id)) {
unset($m);
$sql = db_query("SELECT count(id) as total FROM ".C_MYSQL_BANLIST." WHERE (user='".$id."' or user='0') AND myid='".get_info('m')."'") or die(db_error());
$trows = db_fetch_array($sql);
$total = $trows['total'];
if($total != '0') sprintm($w[320]);

if(!isset($reason)) $reason='';
db_query("INSERT INTO ".C_MYSQL_BANLIST." VALUES(null,'".get_info('m')."',NOW(),'".$id."','".cb($reason)."')") or die(db_error());
sprintm($w[322]);

/*	if(isset($do)) {
	if(!isset($reason)) $reason='';
db_query("INSERT INTO ".C_MYSQL_BANLIST." VALUES(null,'".get_info('m')."',NOW(),'".$id."','".cb($reason)."')") or die(db_error());
sprintm($w[322]);
	} else {
		define('ADD_TO_LIST',$w[319]);
		define('ENTER_YOUR_REASON',$w[521]);
		define('SEND_MESSAGE',$w[170]);
		define('ID_VALUE',$id);
	  if(isset($uname)) define('USER_NAME',$uname);
		else define('USER_NAME','');

		include_once C_PATH.'/templates/'.C_TEMP.'/ban.php';
	}
*/
}
include_once C_PATH.'/sfooter.php';
?>
