<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 27/07/03        Last Modified 27/07/03           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               os.php                           #
# File purpose            Library for ajax replies         #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once '../include/config.inc.php';
include_once '../include/options.inc.php';
include_once '../include/security.inc.php';
include_once '../include/functions.inc.php';
include_once '../classes/ajax.class.php';

$Ajax = new Ajax(C_CHARSET);

$post['is_supported'] = ($post['is_supported'])?1:0;
$post['is_enabled'] = ($post['is_enabled'])?1:0;
$ip = ip2int(ip());

if (isset($post['fingerprint']) && empty($post['fingerprint'])){
	$post['fingerprint'] = date('Y-m-d').'T'.date('H:i:s').'.000Z';
}

if ($post['player_id']){
    if (MyID() > 0){
        $sql = "SELECT * FROM ".C_MYSQL_DEVICES." 
        WHERE user_id = '".MyID()."' 
        AND player_id = '".$post['player_id']."'
        AND ip = '".$ip."'
        AND fingerprint = '".$post['fingerprint']."'";
        $query = db_query($sql) or die(db_error());
        $count = db_num_rows($query);

        if ($count == 0){
            db_query("UPDATE ".C_MYSQL_DEVICES." 
                SET is_supported = '".$post['is_supported']."', 
                player_id = '".$post['player_id']."', 
                is_enabled = '".$post['is_enabled']."', 
                is_logged_in = '1', 
                last_visit = NOW() 
                WHERE user_id = '".MyID()."'
                AND ip = '".$ip."'
                AND fingerprint = '".$post['fingerprint']."'") or die(db_error());
        } else {
            db_query("UPDATE ".C_MYSQL_DEVICES." 
            SET is_supported = '".$post['is_supported']."', 
            is_enabled = '".$post['is_enabled']."', 
            is_logged_in = '1', 
            last_visit = NOW() 
            WHERE user_id = '".MyID()."' 
            AND player_id = '".$post['player_id']."'") or die(db_error());
        }
    } else {
        db_query("UPDATE ".C_MYSQL_DEVICES." 
        SET is_supported = '".$post['is_supported']."', 
        is_enabled = '".$post['is_enabled']."', 
        is_logged_in = '".$post['is_logged_in']."', 
        last_visit = NOW() 
        WHERE player_id = '".$post['player_id']."'") or die(db_error());
    }
    $GLOBALS['_RESULT'] = array( 'status' => 'OK');
} else {
    $GLOBALS['_RESULT'] = array( 'status' => 'ERROR');
}

if (MyID() > 0){
	db_query("UPDATE ".C_MYSQL_MEMBERS_SEC." SET ip = '".$ip."', fingerprint = '".$post['fingerprint']."' WHERE id='".MyID()."'") or die(db_error());
}

die();
?>