<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 27/07/03        Last Modified 27/07/03           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               dl.php                           #
# File purpose            Library for ajax replies         #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once '../include/config.inc.php';
include_once '../include/options.inc.php';
include_once '../include/security.inc.php';
include_once '../include/functions.inc.php';
include_once '../classes/ajax.class.php';

	if (!empty($_SERVER['HTTP_CLIENT_IP'])) $ip=$_SERVER['HTTP_CLIENT_IP'];
	elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	else $ip=$_SERVER['REMOTE_ADDR'];
	
	$fajl= C_PATH."/dl_log/".$ip."_".date("H_i").".txt";
	
	$fp = fopen($fajl, 'a');
	fwrite($fp, '.');
	fclose($fp);
	
	if (is_file($fajl))
	{
		if (filesize($fajl) > 500)
		{
			$fp = fopen(C_PATH."/ajax/.htaccess", 'a');
			fwrite($fp, "deny from ".$ip."\n");
			fclose($fp);
			unlink($fajl);	
		}
	}
	if (date("s")==11)
	{
		exec('find /web/zsebkft/hotbull.hu/randi/dl_log/ -name "*.txt" -mmin +2 -type f -delete');	
	}
	
	

$Ajax = new Ajax(C_CHARSET);
if(!isset($check)) $check = 1;
switch($check)
{
	case '1':default: // Bad username when register
	$username = isset($_REQUEST['username']) ? cb($_REQUEST['username']) : '';
	if (!ereg("^[".C_LOGIN_CHARS."]{3,16}$", $username) || empty($username)) die($_RESULT = array( 'username' => $w[266]));
	else {
		$result = db_query('SELECT count(id) as count FROM '.C_MYSQL_MEMBERS_MAIN.' WHERE username = \''.$username.'\'');
		$trows = db_fetch_array($result);
		$count = $trows['count'];

		if ($count != '0') die($_RESULT = array( 'username' => $username.': ' . $w[267]));
		else die($_RESULT = array( 'username' => ''));
	}
	break;
	case '2': // Return number of new messages
	//Check the account not suspended
	$result = db_query('SELECT status FROM '.C_MYSQL_MEMBERS_MAIN.' WHERE id = \''.MyID().'\'');
	if (db_num_rows($result)==0)
	{
		die($GLOBALS['_RESULT'] = array( 'logout' => '1'));
	    break;
	}
	$trows = db_fetch_array($result);
	if ($trows['status'] < '7')
	{
	    die($GLOBALS['_RESULT'] = array( 'logout' => '1'));
	    break;
	}
	
	if(IsAccess(C_AJAX_MSGINF) && C_HAVE_MESSAGE && MyID() > 0) {
		$result = db_query("SELECT editdate FROM ".C_MYSQL_MEMBERS_MAIN." WHERE id = '".MyID()."'");
		$trows = db_fetch_array($result);
		$lastvisit = $trows['editdate'];
		if ($lastvisit < date('Y-m-d H:i').':00'){
			db_query("UPDATE ".C_MYSQL_MEMBERS_MAIN." SET editdate = NOW() WHERE id = '".MyID()."'") or die(db_error());
		}
//		$newmsg = cb($_REQUEST['newmsg']);
		$count = UnreadedMessages(MyID());
		if($count > 0 && $count != '')  die($GLOBALS['_RESULT'] = array( 'newmsg' => template($w[331],array($count)), 'icon' => NewMsgIcon(), 'num' => $count));
		else  die($GLOBALS['_RESULT'] = array( 'newmsg' => '', 'icon' => '', 'num' => '0'));
	} else  die($GLOBALS['_RESULT'] = array( 'newmsg' => '', 'icon' => '', 'num' => '0'));

	break;
}

die;
?>