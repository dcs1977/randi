<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 18/04/07           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               privacy.php                      #
# File purpose            Privacy Statement                #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';
include_once C_PATH.'/header.php';

# Including "Privacy Statement" file from language directory
@include_once(C_PATH.'/languages/'.LANGUAGE.'/privacy.php');
?>
<script>
    var el = document.getElementById('cookie_attention');
	el.parentNode.removeChild(el);
</script>
<?php
include_once C_PATH.'/footer.php';
?>