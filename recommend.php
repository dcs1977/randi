<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               recommend.php                    #
# File purpose            Recommend to friends             #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';

include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';
security(!C_RECOMMENDED,$w[190]);
security(C_RECOM,$w[152]);
include_once C_PATH.'/sheader.php';

if (!empty($yname)&&!empty($ymail)) {
$yname=cb($yname);
if (c_email($ymail) == 0) sprintm($w[11]);
$tm=array($yname);$subject = template($w[191],$tm);
$tm=array($yname,C_URL);$message = template($w[192],$tm);
for($p=1;$p<=C_RECOM_NUM;$p++) {
   $tm=array($p);
if(!empty($mail[$p])) {
   if (c_email($mail[$p]) == 0) sprintm(template($w[193],$tm));
   sendmail($ymail,$mail[$p],$subject,nl2br($message),$message);
   db_query("INSERT INTO ".C_MYSQL_RECOMMEND." VALUES (Null,'".$ymail."','".ip2int(ip())."','".$mail[$p]."',NOW())") or die(db_error());

}
}
sprintm($w[165]);
} 
else sprintm($w[194]);
include_once C_PATH.'/sfooter.php'; ?>