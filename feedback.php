<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               feedback.php                     #
# File purpose            Feedback with webmaster          #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

} else {
    include_once C_PATH . '/header.php';
}
IsSecure(C_FEEDBACK);

if(!isset($a)) $a='';
if ($a == "s") {
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $secret_key = RECAPTCHA_SECRET_KEY;
        if (!isset($token)){
            send_response(500);
        } else if (empty($token)){
            send_response(500);
        }
    } else {
        $secret_key = OLD_RECAPTCHA_SECRET_KEY;
        if (!isset($_POST['g-recaptcha-response'])){
            printm($w[355].'<br/>Nem töltött be a recaptcha', 2);
        } else if (empty($_POST['g-recaptcha-response'])){
            printm($w[355].'<br/>Nem töltött be a recaptcha', 2);
        }
        $token = $_POST['g-recaptcha-response'];
    }

    // call curl to POST request
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('secret' => $secret_key, 'response' => $token)));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);
    if (!empty($response)){
        $arrResponse = json_decode($response, true);
    }

    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        if($arrResponse["success"] == '1' && $arrResponse["action"] == $action && $arrResponse["score"] >= 0.5) {
            $recaptcha_alert = false;
        } else {
            $recaptcha_alert = true;
        }
    } else {
        if ($arrResponse["success"]){
            $recaptcha_alert = false;
        } else {
            $recaptcha_alert = true;
        }
    }

    if($recaptcha_alert === true){
        send_response(500, 'A recaptcha szerint SPAM küldő lehetsz.');
    }

    if(!isset($_POST['website-honeypot'])){
        send_response(500, 'Nem támogatott böngészőt használhatsz!');
    } else if($_POST['website-honeypot'] !== ''){
        send_response(500, 'Nem támogatott böngészőt használhatsz!');
    }

    //send_response(500, 'teszt.');

    if(!IsAccess(C_VERIMG_FDBK)) {
			$verifyimage = cb($verifyimage);
			if(strtoupper($verifyimage) != strtoupper(@$_SESSION['vc']))
			{
				include_once C_PATH.'/header.php';
				printm($w[497]);
			}
    }

if(empty($subject)||empty($message)||empty($remail)||!isset($approve)) send_response(500, $w[164]);
if (c_email($remail) == 0) send_response(500, $w[11]);
$subject=cb($subject);$message=cbmail($message);$remail=cb($remail);
unset($deactive);unset($m); // Special for hackers:)

$from = array();

if(is_numeric(get_info('m'))) { 
    $tmp=db_query("SELECT email FROM ".C_MYSQL_MEMBERS_SEC." WHERE id='".get_info('m')."' AND status > '5'");
    $row=db_fetch_array($tmp);
    $from['email'] = IsAccess(C_FEEDBACK_MAIL) ? $remail : $row['email'];

    $tmp=db_query("SELECT username FROM ".C_MYSQL_MEMBERS_MAIN." WHERE id='".get_info('m')."' AND status > '5'");
    $row=db_fetch_array($tmp);
    $from['name'] = IsAccess(C_FEEDBACK_MAIL) ? $rname : $row['username'];
    $is_online = $w[350];
} else {
    $from['email']=$remail;
    $from['name'] = $rname;
    $is_online = $w[354];
}

$tm = array($message, $from['name'], $from['email'], $is_online);
$text = template($wl['message_to_admin']['text'], $tm);

$tm[0] = nl2br($tm[0]);
$html = template($wl['message_to_admin']['html'], $tm);


sendmail($from,C_ADMINM,$subject,$html,$text);
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    alert_mes($w[165]);
    send_response(200, C_URL.'/index.php');
} else {
    printm($w[165],2);
}

} else {

define('ENTER_YOUR_NAME',$w[166]);
define('ENTER_YOUR_EMAIL',$w[11]);
define('ENTER_YOUR_SUBJECT',$w[166]);
define('ENTER_YOUR_MESSAGE',$w[167]);
define('APPROVE_EMAIL',$w[868].$w[0]);
define('ENTER_APPROVE_EMAIL',$w[869]);

define('MUST_FILL', $w[0].' '.$w[870]);

define('SUBJECT',$w[168].$w[0]);
define('MESSAGE',$w[169].$w[0]);
unset($deactive);unset($m); // Special for hackers:)
if(is_numeric(get_info('m'))) { // If user registered and login
$tmp=db_query("SELECT email FROM ".C_MYSQL_MEMBERS_SEC." WHERE id='".get_info('m')."' AND status > '5'");
$row=db_fetch_array($tmp);
$deactive = IsAccess(C_FEEDBACK_MAIL) ? 'value="'.$row['email'].'"' : 'value="'.$row['email'].'" readonly="true"'; 
}
else $deactive='';
define('EMAIL_VALUE_DISABLED',$deactive);
define('EMAIL',$w[60].$w[0]);

if(is_numeric(get_info('m'))) { // If user registered and login
	$tmp=db_query("SELECT username FROM ".C_MYSQL_MEMBERS_MAIN." WHERE id='".get_info('m')."' AND status > '5'");
	$row=db_fetch_array($tmp);
	$deactive = IsAccess(C_FEEDBACK_MAIL) ? 'value="'.$row['username'].'"' : 'value="'.$row['username'].'" readonly="true"'; 
}
else $deactive='';
define('NAME_VALUE_DISABLED',$deactive);
define('NAME',$w[118].$w[0]);

define('SEND_MESSAGE',$w[170]);

	if (IsAccess(C_VERIMG_FDBK)) // NOT
	{
		define('VERIFY_ALLOW', '0');
		define('VERIFY_ERROR', '');
	}
	else
	{
		define('VERIFY_ALLOW', '1');
		define('VERIFY_ERROR', $w[497]);
	}
	define('VERIFICATION', $w[498]);
include_once(C_PATH.'/templates/'.C_TEMP.'/feedback.php');

}
include_once C_PATH.'/footer.php';?>