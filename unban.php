<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 27/07/03        Last Modified 27/07/03           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               ban.php                          #
# File purpose            Add users to banlist             #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';
ssecurity(TRUE,$w[152]);
include_once C_PATH.'/sheader.php';
if(!IsAccess(C_BANSH)) sprintm($w[321]);

if (isset($id)&&is_numeric($id)) {
unset($m);
$sql = db_query("DELETE FROM ".C_MYSQL_BANLIST." WHERE user='".$id."' AND myid='".get_info('m')."'") or die(db_error());
sprintm($w[865]);
}
include_once C_PATH.'/sfooter.php';
?>
