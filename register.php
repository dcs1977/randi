<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               register.php                     #
# File purpose            Register users by step in DB     #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
define('REQ_COUNTRIES',True);
include_once 'include/security.inc.php';

include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';
define('C_TITLE',template(C_REGISTER_TITLE,array('REGISTER'=>$w[50],'SNAME'=>C_SNAME)));

include_once C_PATH.'/header.php';
if(!((get_info('adminip') == md5(ip())) && (get_info('adminlogin') == md5(C_ADMINL)) && (get_info('adminpass') == md5(C_ADMINP)))) IsSecure(C_REGISTER_ACCESS);

$filename = 'error-'.date('Y-m').'.log';
$folder = C_PATH.'/logs/';
$file_open_new = fopen($folder.$filename, 'a');
fwrite($file_open_new, 'BAD register.php - '.date('Y-m-d H:i:s').PHP_EOL);
fclose($file_open_new);

$errors = ''; // By default  - no errors
if(!isset($step) || !is_numeric($step) || $step < 1 || $step > 4) $step = 1;
if(!isset($id) || !is_numeric($id)) $id = 0;
if(!isset($passw) || !is_md5($passw)) $passw = '';
if (!isset($a)) $a = '';
if (!isset($have_error)) $have_error = 0;
if ($a == 'a')
{
	function ReturnRegisterValues($val,$global,$type=null,$fld_acs=null,$arr=null,$interval=null,$req=null,$err=null,$return_max=null)
	{
		Global $$val,$errors,$w;
		$bool =  $global & pow(2,$fld_acs); // $bool, True or False
		if($type == 'num')
		{
			if($arr == null)
			{
				if(isset($$val) && is_numeric($$val) && $bool)
				{
					Return $$val;
				}
				elseif($bool && $req)
				{
					$errors .= $err.'<br>';
				}
				else Return ($return_max == null) ? 0 : sizeof($arr) - 1;
			}
			else
			{
				$count = (is_array($arr)) ? sizeof($arr) - 1 : $arr;
				$int_from = ($interval == null) ? 0 : $interval[0];
				$int_to = ($interval == null) ? $count : $interval[1];
				if(isset($$val) && is_numeric($$val) && $bool && ($$val > $int_from) && ($$val <= $int_to))
				{
					Return $$val;
				}
				elseif($bool && $req)
				{
					$errors .= $err.'<br>';
				}
				else Return ($return_max == null) ? $int_from : $int_to;
			}
		}
		else if($type == 'text')// Text value
		{
			if(isset($$val) && !empty($$val) && $bool)
			{
				Return cb($$val);
			}
			elseif($bool && $req)
			{
				$errors .= $err.'<br>';
			}
			else Return '';
		}
		else if($type == 'check')// checking only
		{
			Return (isset($$val) && $bool) ? 1 : 0;
		}
		else if($type == 'multinum')
		{
			if(isset($$val) && $bool)
			{
				$tmp = implode(',',$$val);
				if(!preg_match("/^[0-9]|,$/", $tmp)) $tmp = '';
				Return $tmp;
			}
			elseif($bool && $req)
			{
				$errors .= $err.'<br>';
			}
			else Return '';
		}
		else if($type == 'list')// Text value
		{
			if(isset($$val) && $bool)
			{
				$p=0;$lst='';
				while (isset(${$val}[$p]))
				{
					if((${$val}[$p] < sizeof($arr)) && (${$val}[$p] >= 0))
					{
						$lst .= ${$val}[$p].',';
					}
					$p++;
				}
				Return substr($lst, 0, -1);
			}
			elseif($bool && $req)
			{
				$errors .= $err.'<br>';
			}
			else Return '';
		}
	}

	if($id == 0) $passw = $pass;
	else
	{
		$tmp=db_query("SELECT t1.id, t1.gender, t1.username, t3.email, t3.password FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_SEC." as t3 WHERE t1.id = '".$id."' AND t1.id = t3.id AND md5(t3.password) = '".$passw."'");
		$count=db_num_rows($tmp);
		if($count == '0') printm($w[1].'20', 1);
		$i=db_fetch_array($tmp);
		$pass = $passw = $i['password'];
		if($step >= 4)
		{
			$gender = $i['gender'];
			$email = $i['email'];
			$username = $i['username'];
		}
	}
	switch($step)
	{
		case '1':
			$pass = cb($pass);
			$rpass = cb($rpass);
			$email = cb($email);

			if(C_VERIFY_IMAGE_REGISTER) {
				$verifyimage = cb($verifyimage);
				if(strtoupper($verifyimage) != strtoupper($_SESSION['vc']))
				{

					$errors .= $w[497].'<br>';
				}
			}
			// Check for unic. email
			if (C_UNICM)
			{
				$result = db_query('SELECT count(id) as count FROM '.C_MYSQL_MEMBERS_SEC.' WHERE email = \''.$email.'\'');
				$trows = db_fetch_array($result);
				$count = $trows['count'];
				if ($count != '0') $errors .= $w[2].'<br>';
			}
			if (c_email($email) == 0) $errors .= $w[11].'<br>';
			if (C_BADWORDS_ENABLE)
			{
				$bad_username=$bad_email=array();
				$tmp = db_query("Select * from ".C_MYSQL_BLACKLIST." order by bid");
				$cnt2 = db_num_rows($tmp);
				if($cnt2 != 0)
				{
					while($i=db_fetch_array($tmp))
					{
						if($i['bid'] == 0 || $i['bid'] == 1) $bad_username[]=$i['word'];
						if($i['bid'] == 0 || $i['bid'] == 2) $bad_email[]=$i['word'];
					}
					if (is_badwords($email,implode(',',$bad_email))) $errors .= C_BADWORDS_MES.': '.$w[60].'<br>';
				}
			}

			if (!C_ID)
			{
				$username = cb($username);
				if (!preg_match("/^[".C_LOGIN_CHARS."]{3,16}$/", $username) || empty($username)) $errors .= $w[266].'<br>';
				$result = db_query('SELECT count(id) as count FROM '.C_MYSQL_MEMBERS_MAIN.' WHERE username = \''.$username.'\'');
				$trows = db_fetch_array($result);
				$count = $trows['count'];
				if ($count != '0') $errors .= $w[267].'<br>';
				if (C_BADWORDS_ENABLE)
				{
					if (is_badwords($username,implode(',',$bad_username))) $errors .= C_BADWORDS_MES.': '.$w[265].'<br>';
				}
			}
			else
			{
				$username = '';
			}

			if(ReturnRegisterValues('fname',C_REGISTER_FIELDS1,'check',0))
			{
				if (((C_FIRSTNR) || (!empty($fname))) && ((strlen($fname) > C_FIRSTNB) || (strlen($fname) < C_FIRSTNS)))
				{
					$tm = array(C_FIRSTNS, C_FIRSTNB);
					$errors .= template($w[3], $tm).'<br>';
				}
			}

			if(ReturnRegisterValues('lname',C_REGISTER_FIELDS1,'check',1))
			{
				if (((C_LASTNR) || (!empty($lname))) && ((strlen($lname) > C_LASTNB) || (strlen($lname) < C_LASTNS)))
				{
					$tm = array(C_LASTNS, C_LASTNB);
					$errors .= template($w[4], $tm).'<br>';
				}
			}

			// Final checks for most real values for profiles
			if ((strlen($pass) > C_PASSB) || (strlen($pass) < C_PASSS))
			{
				$tm = array(C_PASSS, C_PASSB);
				$errors .= template($w[6], $tm).'<br>';
			}

			if (c_email($email) == 0) $errors .= $w[11].'<br>';

			if (!empty($url))
			{
				$urll = checkurl($url);
				if (strlen($urll) < 3)
				{
					$errors .= $w[12].'<br>';
				}
			}
			if ((!empty($aim)) && ((strlen($aim) < 3) || (strlen($aim) > 16))) $errors .= $w[14].'<br>';
			if (C_HOBBR && ReturnRegisterValues('hobby',C_REGISTER_FIELDS1,'check',29))
			{
				if (empty($hobby) || trim($hobby) == "") $errors .= $w[35].'<br>';
				if (strlen($hobby) > C_HOBBB)
				{
					$tm = array(C_HOBBB);
					$errors .= template($w[36], $tm).'<br>';
				}
				$es = explode(" ", $hobby);
				for ($as = 0; $as < sizeof($es); $as++)
				{
					$os = strlen($es[$as]);
					if ($os > C_HOBBW)
					{
						$tm = array(C_HOBBW);
						$errors .= template($w[37], $tm).'<br>';
					}
				}
			}
	
	
			if(ReturnRegisterValues('descr',C_REGISTER_FIELDS2,'check',0))
			{
			include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';
			$descr=RemoveAzDGCode($descr,C_BB_DESC_ACL);
				if (empty($descr) || trim($descr) == "") $errors .= $w[38];
				if (strlen($descr) > C_DESCB)
				{
					$tm = array(C_DESCB);
					$errors .= template($w[39], $tm).'<br>';
				}
				$es = explode(" ", $descr);
				for ($as = 0; $as < sizeof($es); $as++)
				{
					$os = strlen($es[$as]);
					if ($os > C_DESCW)
					{
						$tm = array(C_DESCW);
						$errors .= template($w[40], $tm).'<br>';
					}
				}
			}


			if ((strlen($pass) > C_PASSB) || (strlen($pass) < C_PASSS))
			{
				$tm = array(C_PASSS, C_PASSB);
				$errors .= template($w[6], $tm).'<br>';
			}
			$wantpriv=ReturnRegisterValues('wantpriv',C_REGISTER_FIELDS2,'check',18);


			if ($errors == '')
			{
				$usr = (C_ID) ? '' : $username;
				$rate = (C_DEF_RATE) ? 1 : 0;
				db_query("INSERT INTO ".C_MYSQL_MEMBERS_MAIN." VALUES (Null, '".$usr."','','','0','0','0','0','','0','','','0','0','','','0',NOW(),NOW(),'".$rate."','0','0','4','0','1','0','0')") or die(db_error());
				$last_id = db_fetch_array(db_query("SELECT LAST_INSERT_ID() AS last_id"));
				$last_id = $last_id['last_id'];
				$id=$last_id;
				db_query("INSERT INTO ".C_MYSQL_MEMBERS_SEC." VALUES ('".$last_id ."','".$pass."','".$email."','0','0','".ip2int(ip())."','0','".$wantpriv."','0','','4')") or die(db_error());
				db_query("INSERT INTO ".C_MYSQL_MEMBERS_EXT." VALUES ('".$last_id ."','','','','','','','','','','','','','','','','','','".cb(LANGUAGE)."','','','','','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','','','4')") or die(db_error());
				db_query("INSERT INTO ".C_MYSQL_MEMBERS_FORUM." VALUES ('".$last_id ."','0','0','0','0','','')") or die(db_error());

				$step = 2;
				$have_error = 0;
			}
			else
			{
				$step = 1;
				mes($w[290].'<p align=left>'.$errors.'</p>');
				$have_error = 1;
			}

			break;
		case '2':

			$fname=ReturnRegisterValues('fname',C_REGISTER_FIELDS1,'text',0);
			$lname=ReturnRegisterValues('lname',C_REGISTER_FIELDS1,'text',1);
			$address=ReturnRegisterValues('address',C_REGISTER_FIELDS1,'text',14,0,0,C_ADDRESSR,$w[377]);
			$url=ReturnRegisterValues('url',C_REGISTER_FIELDS1,'text',7);
			$icq=ReturnRegisterValues('icq',C_REGISTER_FIELDS1,'num',8);
			$aim=ReturnRegisterValues('aim',C_REGISTER_FIELDS1,'text',9);
			$phone=ReturnRegisterValues('phone',C_REGISTER_FIELDS1,'text',10,0,0,C_PHONER,$w[15]);
			$city=ReturnRegisterValues('city',C_REGISTER_FIELDS1,'text',13,0,0,C_CITYR,$w[16]);
			$job=ReturnRegisterValues('job',C_REGISTER_FIELDS1,'text',28,1,0,C_JOBR,$w[32]);
			$hobby=ReturnRegisterValues('hobby',C_REGISTER_FIELDS1,'text',29);
			$descr=ReturnRegisterValues('descr',C_REGISTER_FIELDS2,'text',0);
			$zip=ReturnRegisterValues('zip',C_REGISTER_FIELDS1,'text',12,0,0,C_ZIPR,$w[288]);
			if(isset($statelist) && isset($state) && $statelist != '' && $state == '') $state = $statelist;
			$state=ReturnRegisterValues('state',C_REGISTER_FIELDS1,'text',11,0,0,C_STATER,$w[531].$w[527]);
			$langlist=ReturnRegisterValues('lang',C_REGISTER_FIELDS1,'list',26,$wlng,0,C_LANGR,$w[383]);
			$intlist=$interests=ReturnRegisterValues('interests',C_REGISTER_FIELDS1,'list',30,$win,0,C_INTERESTSR,$w[531].$w[526]);

			$day=ReturnRegisterValues('day',C_REGISTER_FIELDS1,'num',2,1,array(0,31),C_BIRTHR,$w[5]);
			$month=ReturnRegisterValues('month',C_REGISTER_FIELDS1,'num',2,1,array(0,12),C_BIRTHR,$w[5]);
			$yy = date("Y", time());
			$year=ReturnRegisterValues('year',C_REGISTER_FIELDS1,'num',2,1,array($yy-(C_AGEB+1),$yy-C_AGES),C_BIRTHR,$w[5]);

			$gender=ReturnRegisterValues('gender',1,'num',0,$wg,0,C_GENDR,$w[7]);
			$bodytype=ReturnRegisterValues('bodytype',C_REGISTER_FIELDS1,'num',5,$wbt,0,C_BODYTYPER,$w[531].$w[525]);
			$orientation=ReturnRegisterValues('orientation',C_REGISTER_FIELDS1,'num',4,$wor,0,C_ORIENTATIONR,$w[531].$w[524]);
			$purpose=ReturnRegisterValues('purpose',C_REGISTER_FIELDS1,'list',3,$wp,0,C_PURPR,$w[9]);
			$country=ReturnRegisterValues('country',C_REGISTER_FIELDS1,'num',6,$wcr,0,C_CNTRR,$w[10]);
			$marstat=ReturnRegisterValues('marstat',C_REGISTER_FIELDS1,'num',15,$wm,0,C_MARSR,$w[17]);
			$child=ReturnRegisterValues('child',C_REGISTER_FIELDS1,'num',16,$wc,0,C_CHILDR,$w[18]);
			$height=ReturnRegisterValues('height',C_REGISTER_FIELDS1,'num',17,$wh,0,C_HGHTR,$w[19]);
			$weight=ReturnRegisterValues('weight',C_REGISTER_FIELDS1,'num',18,$ww,0,C_WGHTR,$w[20]);
			$hcolor=ReturnRegisterValues('hcolor',C_REGISTER_FIELDS1,'num',19,$whc,0,C_HAIRR,$w[23]);
			$ecolor=ReturnRegisterValues('ecolor',C_REGISTER_FIELDS1,'num',20,$we,0,C_EYER,$w[24]);
			$etnicity=ReturnRegisterValues('etnicity',C_REGISTER_FIELDS1,'num',21,$wet,0,C_ETNR,$w[25]);
			$religion=ReturnRegisterValues('religion',C_REGISTER_FIELDS1,'num',22,$wr,0,C_RELR,$w[26]);
			$smoke=ReturnRegisterValues('smoke',C_REGISTER_FIELDS1,'num',23,$ws,0,C_SMOKER,$w[29]);
			$drink=ReturnRegisterValues('drink',C_REGISTER_FIELDS1,'num',24,$wd,0,C_DRINKR,$w[30]);
			$education=ReturnRegisterValues('education',C_REGISTER_FIELDS1,'num',25,$wed,0,C_EDUCR,$w[31]);
			$hdyfu=ReturnRegisterValues('hdyfu',C_REGISTER_FIELDS2,'num',17,$wu,0,C_HDYFUR,$w[34]);
			if(ReturnRegisterValues('fname',C_REGISTER_FIELDS1,'check',0))
			{
				if (((C_FIRSTNR) || (!empty($fname))) && ((strlen($fname) > C_FIRSTNB) || (strlen($fname) < C_FIRSTNS)))
				{
					$tm = array(C_FIRSTNS, C_FIRSTNB);
					$errors .= template($w[3], $tm).'<br>';
				}
			}

			if(ReturnRegisterValues('lname',C_REGISTER_FIELDS1,'check',1))
			{
				if (((C_LASTNR) || (!empty($lname))) && ((strlen($lname) > C_LASTNB) || (strlen($lname) < C_LASTNS)))
				{
					$tm = array(C_LASTNS, C_LASTNB);
					$errors .= template($w[4], $tm).'<br>';
				}
			}

			if (!empty($url))
			{
				$urll = checkurl($url);
				if (strlen($urll) < 3)
				{
					$errors .= $w[12].'<br>';
				}
			}
			if ((!empty($aim)) && ((strlen($aim) < 3) || (strlen($aim) > 16))) $errors .= $w[14].'<br>';
			if (C_HOBBR && ReturnRegisterValues('hobby',C_REGISTER_FIELDS1,'check',29))
			{
				if (empty($hobby) || trim($hobby) == "") $errors .= $w[35].'<br>';
				if (strlen($hobby) > C_HOBBB)
				{
					$tm = array(C_HOBBB);
					$errors .= template($w[36], $tm).'<br>';
				}
				$es = explode(" ", $hobby);
				for ($as = 0; $as < sizeof($es); $as++)
				{
					$os = strlen($es[$as]);
					if ($os > C_HOBBW)
					{
						$tm = array(C_HOBBW);
						$errors .= template($w[37], $tm).'<br>';
					}
				}
			}

			if(ReturnRegisterValues('descr',C_REGISTER_FIELDS2,'check',0))
			{
				if (empty($descr) || trim($descr) == "") $errors .= $w[38];
				if (strlen($descr) > C_DESCB)
				{
					$tm = array(C_DESCB);
					$errors .= template($w[39], $tm).'<br>';
				}
				$es = explode(" ", $descr);
				for ($as = 0; $as < sizeof($es); $as++)
				{
					$os = strlen($es[$as]);
					if ($os > C_DESCW)
					{
						$tm = array(C_DESCW);
						$errors .= template($w[40], $tm).'<br>';
					}
				}
			}

			if (C_BADWORDS_ENABLE)
			{
				if (C_BADWORDS_ERR)
				{
					if (is_badwords($hobby) || is_badwords($descr) || is_badwords($fname) || is_badwords($lname) || is_badwords($state) || is_badwords($zip) || is_badwords($city) || is_badwords($address) || is_badwords($job))
					$errors .= C_BADWORDS_MES.'<br>';
				}
				elseif (C_BADWORDS_REP)
				{
					$hobby = replace_badwords($hobby);
					$descr = replace_badwords($descr);
					$fname = replace_badwords($fname);
					$lname = replace_badwords($lname);
					$state = replace_badwords($state);
					$zip = replace_badwords($zip);
					$city = replace_badwords($city);
					$address = replace_badwords($address);
					$job = replace_badwords($job);
				}
			}

			if ($errors == '')
			{
				$points = 0;
				$pointm = 1;
				db_query("UPDATE ".C_MYSQL_MEMBERS_MAIN." SET fname='".$fname."',lname='".$lname."',birthday='".$year."-".$month."-".$day."',gender='".$gender."',bodytype='".$bodytype."',orientation='".$orientation."',purposes='".$purpose."',country='".$country."',city='".$city."',zip='".$zip."',height='".$height."',weight='".$weight."',points='".$points."',pointm='".$pointm."',horo='".horo($month,$day)."' WHERE id = '".$id."'") or die(db_error());

				db_query("REPLACE INTO ".C_MYSQL_MEMBERS_EXT." VALUES ('".$id ."','".$url."','".$icq."','".$aim."','".$phone."','".$state."','".$address."','".$marstat."','".$child."','".$hcolor."','".$ecolor."','".$etnicity."','".$religion."','".$smoke."','".$drink."','".$education."','".$langlist."','".$intlist."','".cb(LANGUAGE)."','".$job."','".$hobby."','".$descr."','','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','".$hdyfu."','','4')") or die(db_error());
				$step = 3;
				$have_error = 0;
			}
			else
			{
				$step = 2;
				mes($w[290].'<p align=left>'.$errors.'</p>');
				$have_error = 1;
			}
			break;
		case '3':
			$sgender=ReturnRegisterValues('sgender',C_REGISTER_FIELDS2,'list',1,$wg,0,C_SGENDR,$w[8]);
			$scountry=ReturnRegisterValues('scountry',C_REGISTER_FIELDS2,'list',2,$wcr,0,C_SCNTRR,$w[452].' '.$w[444]);
			$smarstat=ReturnRegisterValues('smarstat',C_REGISTER_FIELDS2,'list',3,$wm,0,C_SMARSR,$w[452].' '.$w[445]);
			$schild=ReturnRegisterValues('schild',C_REGISTER_FIELDS2,'list',4,$wc,0,C_SCHILDR,$w[452].' '.$w[446]);
			$shcolor=ReturnRegisterValues('shcolor',C_REGISTER_FIELDS2,'list',5,$whc,0,C_SHAIRR,$w[452].' '.$w[447]);
			$secolor=ReturnRegisterValues('secolor',C_REGISTER_FIELDS2,'list',6,$we,0,C_SEYER,$w[452].' '.$w[448]);
			$setnicity=ReturnRegisterValues('setnicity',C_REGISTER_FIELDS2,'list',10,$wet,0,C_SETNR,$w[27]);
			$sreligion=ReturnRegisterValues('sreligion',C_REGISTER_FIELDS2,'list',11,$wr,0,C_SRELR,$w[28]);
			$ssmoke=ReturnRegisterValues('ssmoke',C_REGISTER_FIELDS2,'list',7,$ws,0,C_SSMOKER,$w[452].' '.$w[449]);
			$sdrink=ReturnRegisterValues('sdrink',C_REGISTER_FIELDS2,'list',8,$wd,0,C_SDRINKR,$w[452].' '.$w[450]);
			$seducation=ReturnRegisterValues('seducation',C_REGISTER_FIELDS2,'list',9,$wed,0,C_SEDUCR,$w[452].' '.$w[451]);

			$heightf=ReturnRegisterValues('heightf',C_REGISTER_FIELDS2,'num',13,$wh);
			$weightf=ReturnRegisterValues('weightf',C_REGISTER_FIELDS2,'num',14,$ww);
			$heightt=ReturnRegisterValues('heightt',C_REGISTER_FIELDS2,'num',13,$wh,0,0,0,1);
			$weightt=ReturnRegisterValues('weightt',C_REGISTER_FIELDS2,'num',14,$ww,0,0,0,1);
			$agef=ReturnRegisterValues('agef',C_REGISTER_FIELDS2,'num',12,1,array(C_AGES,C_AGEB),C_SAGER,$w[33]);
			$aget=ReturnRegisterValues('aget',C_REGISTER_FIELDS2,'num',12,1,array(C_AGES,C_AGEB),C_SAGER,$w[33],1);

			if ($errors == '')
			{
				db_query("UPDATE ".C_MYSQL_MEMBERS_MAIN." SET sgender='".$sgender."' WHERE id = '".$id."'");

				db_query("UPDATE ".C_MYSQL_MEMBERS_EXT." SET scountry='".$scountry."',smarstat='".$smarstat."',schild='".$schild."',shcolor='".$shcolor."',secolor='".$secolor."',ssmoke='".$ssmoke."',sdrink='".$sdrink."',seducation='".$seducation."',setnicity='".$setnicity."',sreligion='".$sreligion."',agef='".$agef."',aget='".$aget."',heightf='".$heightf."',heightt='".$heightt."',weightf='".$weightf."',weightt='".$weightt."' WHERE id = '".$id."'");
				$step = 4;
				$have_error = 0;
			}
			else
			{
				$step = 3;
				mes($w[290].'<p align=left>'.$errors.'</p>');
				$have_error = 1;
			}
			break;
		case '4':

			$picture = $grup = array();
			$audio = $audio_ext = '';

			if(ReturnRegisterValues('a',C_REGISTER_FIELDS2,'check',15))
			{
				$MyGender = 0;$grup=explode(",",C_PH_REQ_GEN);
				if(C_PH_REQ_GEN != 0)
				{
					# detecting gender for Require photo per gender feature
					$tmpg = db_fetch_array(db_query("SELECT gender FROM ". C_MYSQL_MEMBERS_MAIN ." WHERE id = '".cb($id)."'"));
					$MyGender = $tmpg['gender'];
				}
				if ((C_PHOTOR && C_PH_REQ_GEN == 0) || (C_PHOTOR && C_PH_REQ_GEN != 0 && in_array ($MyGender, $grup)))
				{
					$uploaded_num=0;
					for($i = 0; $i < $wpn[0]; $i++)
					{
						if(empty($_FILES['file'.$i]['name'])) $uploaded_num++;
					}
					if($uploaded_num == $wpn[0]) $errors .= $w[41].'<br>';
					if(C_REQ_PHOTO_NUM) {if(($wpn[0] - $uploaded_num) < C_REQ_PHOTO_NUM) $errors .= $w[543].C_REQ_PHOTO_NUM.'<br>';}
				}
				////////////// Include class for file uploading!
				include_once 'classes/upload.class.php';
				//////////////
				$time = time();$have_photo=0;
				for($p = 0; $p < $wpn[0]; $p++)
				{

					$file = 'file'.$p;
					$k = $p+1;
					if (!empty($_FILES[$file]['name']))
					{
						$dir = $slash = "";
						$fb = date("dHis", $time);
						$fn = $fb."-".$p;
						$intpic = $dir.$slash.$fn.'.';
						$upl = new Upload($file, C_MAXSZ, C_MAXWD, C_MAXHG, C_PATH.'/members/uploads/'.$intpic);
						if (!$upl->do_upload()) $errors .= $upl->getErrors().'<br>';
						$picture[] = $intpic;
						$have_photo=1;
					}
				}
			}

			if(ReturnRegisterValues('a',C_REGISTER_FIELDS2,'check',16))
			{
				if (C_AUDIO_ALLOW)
				{
					$file = 'filea';
					if (!empty($_FILES[$file]['name']))
					{
						$dir = $slash = "";
						$fb = date("dHis", $time);
						$fe = rand(100, 999);
						$fn = $fb."-".$fe;
						$intpic = $dir.$slash.$fn;
						$upl = new Upload($file, C_AUDIO_SIZE, C_MAXWD, C_MAXHG, C_PATH.'/members/uploads/'.$intpic);
						if (!$upl->do_upload(C_AUDIO_ALLOW)) $errors .= $upl->getErrors().'<br>';
						$audio = $intpic;
						$audio_ext=$upl->getType();
					}
				}
				else $audio = '';
			}

			if (C_CHECK_REGISTER == '0') {
				$cst = (C_SHOW_REGISTERED) ? 10 : 7;
			}
			elseif (C_CHECK_REGISTER == '2') $cst = 1;
			else $cst = 0;

			if((C_CHECK_REGISTER == '2') && $have_photo == '0' && C_CHECK_WPHOTOS) $cst = 7;

			if ($errors == '')
			{
				$ins_pictures = (C_USE_GALLERY && C_DONT_SHOW_CHANGED_PHOTOS) ? '' : implode(array_keys($picture),',');
				db_query("UPDATE ".C_MYSQL_MEMBERS_MAIN." SET files='".$ins_pictures."', status = '".$cst."'  WHERE id = '".$id."'");
				$last_id=$id;
				$rate = (C_DEF_RATE) ? 1 : 0;
				$audio_val = ($audio_ext == '') ? '' : $audio_ext;
				db_query("UPDATE ".C_MYSQL_MEMBERS_EXT." SET audio='".$audio_val."', status = '".$cst."' WHERE id = '".$id."'");
				db_query("UPDATE ".C_MYSQL_MEMBERS_SEC." SET status = '".$cst."' WHERE id = '".$id."'");
				if(C_USE_GALLERY) { // If we`re using the Gallery for admin, we add new photos for checking
					db_query("REPLACE INTO ".C_MYSQL_NEW_PHOTOS." VALUES ('".$last_id."','".implode(array_keys($picture),',')."')");
				}
				
				### If PHP in safe_mode=off, we can create directories
				CheckUploadDirectories($last_id);
				## Copying pictures and audio into right place and removing old
				#### Pictures
				$newpath = id2path($last_id);
				$p=0;while(isset($picture[$p]))
				{
					if($picture[$p] != '')
					{
						if(C_DECREASE_IMAGES)
						{
							include_once 'classes/thumbnail.class.php';
							$thumb=new Thumbnail(C_PATH.'/members/uploads/'.$picture[$p],null,True);
							if(C_PICTURE_WIDTH) $thumb->Width(C_PICTURE_WIDTH);
							if(C_PICTURE_HEIGHT) $thumb->Height(C_PICTURE_HEIGHT);
							if(C_PICTURE_AUTO) $thumb->Auto(C_PICTURE_AUTO);
							$thumb->Quality(C_PICTURE_QUALITY);
							$thumb->Save(C_PATH.'/members/uploads/'.$newpath.$p);
						}
						else
						{
							copy(C_PATH.'/members/uploads/'.$picture[$p],C_PATH.'/members/uploads/'.$newpath.$p);
						}
						unlink(C_PATH.'/members/uploads/'.$picture[$p]);
					}
					$p++;
				}

				#### Audio
				$newpath = id2pathAudio($last_id);
				if($audio != '')
				{
					copy(C_PATH.'/members/uploads/'.$audio.'.'.$audio_val,C_PATH.'/members/uploads/'.$newpath.'.'.$audio_val);
					unlink(C_PATH.'/members/uploads/'.$audio.'.'.$audio_val);
				}
				
				/// Check for users with unfinished registration (status = 4) for last C_REG_DAYS and remove these users
				/*$delus=array();
				$tmp = db_query("SELECT t1.id, t1.files, t2.audio FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2 WHERE t1.regdate < DATE_SUB(NOW(), INTERVAL ".C_REG_DAYS." DAY) AND t1.id = t2.id and t1.status = '4'") or die(db_error());
				$count = db_num_rows($tmp);
				if($count)
				{
					while ($j = db_fetch_array($tmp))
					{
						$delus[]=	$j['id'];
					}
					RemoveUser($delus);
				}*/

				if(C_ENABLE_MSCTRL)
				{
			db_query("INSERT INTO ".C_MYSQL_OPTIONS." VALUES ('".$last_id."', '".$wmg[$gender]."','".$wps[0]."', DATE_ADD(NOW(), INTERVAL 24 HOUR),'0',NOW(),'".intval(C_IMSOUND_DEFAULT)."')") or die(db_error());
				}

				$codegen = code_gen();
				
				if ((C_CHECK_REGISTER == '1') || (C_CHECK_REGISTER == '3'))
				{
					$str = $w[42];
					db_query("INSERT INTO ".C_MYSQL_TEMP." VALUES ('".$last_id."',NOW(),'".$codegen."')");
					sendmail(C_ADMINM, $email, $w[43], nl2br($w[44].C_URL.'/check.php?l='.$l.'&id='.$last_id.'&code='.$codegen), $w[44].C_URL.'/check.php?l='.$l.'&id='.$last_id.'&code='.$codegen);
				}
				elseif(C_CHECK_REGISTER == '2')
				{
					$str = $w[45];

					if (C_NOTIFY_ADMIN)
					{
						$subject = $w[405];
						$tm = array(C_SNAME);
						$message = template($w[407], $tm);
						sendmail(C_FROMM, C_ADMINM, $subject, nl2br($message), $message);
					}

				}
				elseif(C_CHECK_REGISTER == '0')
				{

					switch (C_ID)
					{
						case '0':
							$usrn = $username;
							break;
						case '2':
							$usrn = $email;
							break;
						default:
							$usrn = $last_id;
							break;
					}

					if (C_REG_EMAIL)
					{
						$tm = array(C_SNAME);
						$subject = template($w[195], $tm);
						$tm = array($usrn, $pass, C_SNAME);
						$message = template($w[291], $tm);
						sendmail(C_FROMM, $email, $subject, nl2br($message), $message);
					}

					if (C_NOTIFY_ADMIN)
					{
						$subject = $w[405];
						$tm = array(C_SNAME, C_URL.'/view.php?l='.C_ADMINLANG.'&id='.$last_id);
						$message = template($w[406], $tm);
						sendmail(C_FROMM, C_ADMINM, $subject, nl2br($message), $message);
					}


					if (C_AUTO_LOGIN)
					{
						if(C_VERIFY_IMAGE_LOGIN) {
							@$_SESSION['vc'] = "REGISTER";
							$tmps="&verifyimage=REGISTER";
						} else $tmps="";

        ?>
        <script language="JavaScript">
        <!--
        window.location.replace( '<?=C_URL."/login.php?l=".$l."&".s()."&p=s&id=".$usrn."&password=".$pass.$tmps?>');
        // -->
        </script>
        <?php
					}
					$str = $w[46].$usrn.$w[47].$pass;
				}

				printm($str, 2);
			}
			else
			{

				# Removing uploaded files
				for($p = 0; $p < $wpn[0]; $p++)
				{
					if(isset($picture[$p]) && !empty($picture[$p])) unlink (C_PATH.'/members/uploads/'.$picture[$p]);
				}
				if(!empty($audio)) unlink (C_PATH.'/members/uploads/'.$audio);

				# Show Register Page again

				mes($w[290].'<p align=left>'.$errors.'</p>');
				$have_error = 1;
			}
			break;
	}
}


function GenRegConst($const,$req,$err,$global,$fld_acs)
{
	$bool =  $global & pow(2,$fld_acs); // $bool, True or False
	if ($req && $req != 0)
	{
		define($const.'_ALLOW', '1');
		define($const.'_ERROR', $err);
	}
	else
	{
		define($const.'_ALLOW', '0');
		define($const.'_ERROR', '');
	}
	if ($bool)
	{
		define($const.'_SHOW', '1');
	}
	else
	{
		define($const.'_SHOW', '0');
	}
}

GenRegConst('USERNAME',!C_ID,$w[266],1,1);
GenRegConst('GENDER',C_GENDR,$w[7],1,1);
GenRegConst('VERIFY',C_VERIFY_IMAGE_REGISTER,$w[497],1,1);
$tm = array(C_PASSS, C_PASSB);
define('PASSWORD_ERROR_1', template($w[6], $tm));
define('PASSWORD_ERROR_2', $w[48]);
define('PASSWORD_ERROR_3', $w[49]);
define('EMAIL_ERROR', $w[11]);
GenRegConst('FIRSTNAME',C_FIRSTNR,template($w[3], array(C_FIRSTNS, C_FIRSTNB)),C_REGISTER_FIELDS1,0);
GenRegConst('LASTNAME',C_LASTNR,template($w[4], array(C_LASTNS, C_LASTNB)),C_REGISTER_FIELDS1,1);
GenRegConst('BIRTHDAY',C_BIRTHR,$w[5],C_REGISTER_FIELDS1,2);
GenRegConst('PURPOSE',C_PURPR,$w[9],C_REGISTER_FIELDS1,3);
GenRegConst('ORIENTATION',C_ORIENTATIONR,$w[531].$w[524],C_REGISTER_FIELDS1,4);
GenRegConst('BODYTYPE',C_BODYTYPER,$w[531].$w[525],C_REGISTER_FIELDS1,5);
GenRegConst('COUNTRY',C_CNTRR,$w[10],C_REGISTER_FIELDS1,6);
GenRegConst('URL',0,0,C_REGISTER_FIELDS1,7);
GenRegConst('ICQ',0,0,C_REGISTER_FIELDS1,8);
GenRegConst('AIM',0,0,C_REGISTER_FIELDS1,9);
GenRegConst('PHONE',C_PHONER,$w[15],C_REGISTER_FIELDS1,10);
GenRegConst('STATE',C_STATER,$w[531].$w[527],C_REGISTER_FIELDS1,11);
GenRegConst('ZIP',C_ZIPR,$w[288],C_REGISTER_FIELDS1,12);
GenRegConst('CITY',C_CITYR,$w[16],C_REGISTER_FIELDS1,13);
GenRegConst('ADDRESS',C_ADDRESSR,$w[377],C_REGISTER_FIELDS1,14);
GenRegConst('MARSTAT',C_MARSR,$w[17],C_REGISTER_FIELDS1,15);
GenRegConst('CHILD',C_CHILDR,$w[18],C_REGISTER_FIELDS1,16);
GenRegConst('HEIGHT',C_HGHTR,$w[19],C_REGISTER_FIELDS1,17);
GenRegConst('WEIGHT',C_WGHTR,$w[20],C_REGISTER_FIELDS1,18);
GenRegConst('HAIR',C_HAIRR,$w[23],C_REGISTER_FIELDS1,19);
GenRegConst('EYE',C_EYER,$w[24],C_REGISTER_FIELDS1,20);
GenRegConst('ETHNICITY',C_ETNR,$w[25],C_REGISTER_FIELDS1,21);
GenRegConst('RELIGION',C_RELR,$w[26],C_REGISTER_FIELDS1,22);
GenRegConst('SMOKE',C_SMOKER,$w[29],C_REGISTER_FIELDS1,23);
GenRegConst('DRINK',C_DRINKR,$w[30],C_REGISTER_FIELDS1,24);
GenRegConst('EDUCATION',C_EDUCR,$w[31],C_REGISTER_FIELDS1,25);
GenRegConst('LANG',C_LANGR,$w[383],C_REGISTER_FIELDS1,26);
GenRegConst('JOB',C_JOBR,$w[32],C_REGISTER_FIELDS1,28);
GenRegConst('HOBBY',C_HOBBR,$w[35],C_REGISTER_FIELDS1,29);
GenRegConst('INTERESTS',C_INTERESTSR,$w[531].$w[526],C_REGISTER_FIELDS1,30);

GenRegConst('DESCRIPTION',1,$w[38],C_REGISTER_FIELDS2,0);
GenRegConst('SGENDER',C_SGENDR,$w[8],C_REGISTER_FIELDS2,1);
GenRegConst('SCOUNTRY',C_SCNTRR,$w[452].' '.$w[444],C_REGISTER_FIELDS2,2);
GenRegConst('SMARSTAT',C_SMARSR,$w[452].' '.$w[445],C_REGISTER_FIELDS2,3);
GenRegConst('SCHILD',C_SCHILDR,$w[452].' '.$w[446],C_REGISTER_FIELDS2,4);
GenRegConst('SHAIR',C_SHAIRR,$w[452].' '.$w[447],C_REGISTER_FIELDS2,5);
GenRegConst('SEYE',C_SEYER,$w[452].' '.$w[448],C_REGISTER_FIELDS2,6);
GenRegConst('SSMOKE',C_SSMOKER,$w[452].' '.$w[449],C_REGISTER_FIELDS2,7);
GenRegConst('SDRINK',C_SDRINKR,$w[452].' '.$w[450],C_REGISTER_FIELDS2,8);
GenRegConst('SEDUCATION',C_SEDUCR,$w[452].' '.$w[451],C_REGISTER_FIELDS2,9);
GenRegConst('SETHNICITY',C_SETNR,$w[27],C_REGISTER_FIELDS2,10);
GenRegConst('SRELIGION',C_SRELR,$w[28],C_REGISTER_FIELDS2,11);
GenRegConst('SAGE',0,0,C_REGISTER_FIELDS2,12);
GenRegConst('SHEIGHT',0,0,C_REGISTER_FIELDS2,13);
GenRegConst('SWEIGHT',0,0,C_REGISTER_FIELDS2,14);
GenRegConst('PHOTO',C_PHOTOR,$w[41],C_REGISTER_FIELDS2,15);
GenRegConst('AUDIO',0,0,C_REGISTER_FIELDS2,16);
GenRegConst('HDYFU',C_HDYFUR,$w[34],C_REGISTER_FIELDS2,17);
GenRegConst('WANTPRIV',0,0,C_REGISTER_FIELDS2,18);
GenRegConst('REG_TERMS',C_REG_TERMS,$w[632],C_REG_TERMS,0);


function is_hide($var)
{
	Global $w,$wpu;
	if(C_REGISTER_NOTE && $var < 255)
	return ($var <= C_HIDE_FIELDS) ? '' : template($w[404],array($wpu[$var]));
	else
	return ($var <= C_HIDE_FIELDS) ? '' : $w[348];
}


define('STEP',$step);
define('WSTEP',$w[481]);
if(isset($passw)) define('MD5PASS',md5($passw));
else define('MD5PASS','');
if(isset($id)) define('UID',$id);
else define('UID','');
define('HAVE_ERROR',$have_error);


define('REGISTER_USER', $w[50]);
define('CHECK_UNCHECK_ALL', $w[625]);
define('USERNAME', $w[264].$w[0].is_hide(C_USERNAMESH));
if (isset($username))
{
	define('USERNAME_VALUE', $username);
}
else
{
	define('USERNAME_VALUE', '');
}

$Require = (C_FIRSTNR) ? $w[0] :'';
$Variable = (isset($fname)) ? $fname :'';
define('FIRSTNAME', $w[51].$Require.'['.C_FIRSTNS.'-'.C_FIRSTNB.' '.$w[52].']'.is_hide(C_FIRSTSH));
define('FIRSTNAME_VALUE', $Variable);

define('VERIFICATION', $w[498].$Require);

$Require = (C_LASTNR) ? $w[0] :'';
$Variable = (isset($lname)) ? $lname :'';
define('LASTNAME', $w[53].$Require.'['.C_LASTNS.'-'.C_LASTNB.' '.$w[52].']'.is_hide(C_LASTSH));
define('LASTNAME_VALUE', $Variable);

$Variable = (isset($pass)) ? $pass :'';
define('PASSWORD', $w[54].$w[0].'['.C_PASSS.'-'.C_PASSB.' '.$w[52].']');
define('PASSWORD_VALUE', $Variable);

$Variable = (isset($rpass)) ? $rpass :'';
define('RETYPE_PASSWORD', $w[55].$w[0].'['.C_PASSS.'-'.C_PASSB.' '.$w[52].']');
define('RETYPE_PASSWORD_VALUE', $Variable);

$Require = (C_BIRTHR) ? $w[0] :'';
define('BIRTHDAY', $w[56].$Require.is_hide(C_BIRTHDAYSH));

$Variable = '';
$p = 1;
while (isset($wmm[$p]))
{
	if (isset($month) && ($month == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wmm[$p];
	else $Variable .= '<option value="'.$p.'">'.$wmm[$p];
	$p++;
}
define('MONTH_VALUE', $Variable);
$Variable = '';
for($p = 1; $p < 32; $p++)
{
	if (isset($day) && ($day == $p)) $Variable .= '<option selected>'.$p;
	else $Variable .= '<option>'.$p;
}
define('DAY_VALUE', $Variable);
$Variable = '';
$y = date("Y", time());
$yfrom = $y-C_AGES;
$yto = $y-C_AGEB;
for($p = $yto; $p <= $yfrom; $p++)
{
	if (isset($year) && ($year == $p)) $Variable .= '<option selected>'.$p;
	else $Variable .= '<option>'.$p;
}
define('YEAR_VALUE', $Variable);

$Require = (C_GENDR) ? $w[0] :'';$Variable = '';
define('GENDER', $w[57].$Require.is_hide(C_GENDERSH));$p = 1;
while (isset($wg[$p]))
{
	if (isset($gender) && ($gender == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wg[$p];
	else $Variable .= '<option value="'.$p.'">'.$wg[$p];
	$p++;
}
define('GENDER_VALUE', $Variable);

$Require = (C_BODYTYPER) ? $w[0] :'';$Variable = '';
define('BODYTYPE', $w[525].$Require.is_hide(C_BODYTYPESH));$p = 1;
while (isset($wbt[$p]))
{
	if (isset($bodytype) && ($bodytype == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wbt[$p];
	else $Variable .= '<option value="'.$p.'">'.$wbt[$p];
	$p++;
}
define('BODYTYPE_VALUE', $Variable);

$Require = (C_ORIENTATIONR) ? $w[0] :'';$Variable = '';
define('ORIENTATION', $w[524].$Require.is_hide(C_ORIENTATIONSH));$p = 1;
while (isset($wor[$p]))
{
	if (isset($orientation) && ($orientation == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wor[$p];
	else $Variable .= '<option value="'.$p.'">'.$wor[$p];
	$p++;
}
define('ORIENTATION_VALUE', $Variable);


function GenField($const,$req,$show,$var,$arr,$windex,$sort=null)
{
	Global $w,$$var;
	$Require = ($req) ? $w[0] : '';$Variable = '';
	define(strtoupper($const), $w[$windex].$Require.is_hide($show));$p = 1;
	$pur= (isset($$var)) ? explode(',',$$var)  : array();
	$pur2 = @array_flip($pur);
	if($sort === null)
	{
		while (isset($arr[$p])){
			if (isset($pur2[$p])) $Variable .= '<label for="'.$var.$p.'" class="cb"><input name="'.$var.'[]" value="'.$p.'" type="checkbox" id="'.$var.$p.'" onclick="hld(this);" checked>'.$arr[$p].'</label>';
			else $Variable .= '<label for="'.$var.$p.'" class="lb"><input name="'.$var.'[]" value="'.$p.'" type="checkbox" id="'.$var.$p.'" onclick="hld(this);">'.$arr[$p].'</label>';$p++;
		}
	}
	else
	{
		while (list ($p, $val) = each ($arr))
		{
			if($p == '0') continue;
			if (isset($pur2[$p])) $Variable .= '<label for="'.$var.$p.'" class="cb"><input name="'.$var.'[]" value="'.$p.'" type="checkbox" id="'.$var.$p.'" onclick="hld(this);" checked>'.$arr[$p].'</label>';
			else $Variable .= '<label for="'.$var.$p.'" class="lb"><input name="'.$var.'[]" value="'.$p.'" type="checkbox" id="'.$var.$p.'" onclick="hld(this);">'.$arr[$p].'</label>';$p++;
		}
	}
	define(strtoupper($const.'_VALUE'), $Variable);
}

GenField('purpose',C_PURPR,C_PURPOSESH,'purpose',$wp,'58');
GenField('interests',C_INTERESTSR,C_INTERESTSSH,'interests',$win,'526');

$Require = (C_CNTRR) ? $w[0] : '';
$Variable = '';
define('COUNTRY', $w[59].$Require.is_hide(C_COUNTRYSH));
$cntr0 = null;
$p = 0;asort($wcr);reset($wcr);
while (list ($p, $val) = each ($wcr))
{
	if (isset($country))
	{
		if($country == $p) $Variable .= '<option value="'.$p.'" selected>'.$val;
		else $Variable .= '<option value="'.$p.'">'.$val;
	} else $Variable .=  '<option value="'.$p.'"'.sv($p,$cntr0).'>'.$val;
}
define('COUNTRY_VALUE', $Variable);

$Variable = '';$Check=array();
if(C_STATES_FULL) include_once C_PATH.'/classes/provinces.php';
else include_once C_PATH.'/languages/'.LANGUAGE.'/provinces.php';
$p = 0;
while (list ($p, $val) = each ($wpcr))
{
	$Variable .= 'rc['.$p.']=",' . implode(',',$val) . '";
';
	$Check[] = "index == '".$p."'";
}
define('STATELIST_CHECK', implode(' || ',$Check));
define('STATELIST_VALUE', $Variable);
if(isset($statelist) && isset($state) && $statelist != '' && $state == '') $state = $statelist;
if(!isset($state)) $state = '';
if(!isset($statelist)) $statelist = '';
define('STATELIST_CUR', $state);

$Require = (C_STATER) ? $w[0] : '';
define('STATE', $w[527].$Require.is_hide(C_STATESH));
$Variable = (isset($state)) ? $state : '';
define('STATE_VALUE', $Variable);

define('EMAIL', $w[60].$w[0].is_hide(C_EMAILSH));
$Variable = (isset($email)) ? $email :
'';
define('EMAIL_VALUE', $Variable);

define('URL', $w[61].is_hide(C_URLSH));
$Variable = (isset($url)) ? $url : '';
define('URL_VALUE', $Variable);

define('ICQ', $w[62].is_hide(C_ICQSH));
$Variable = (isset($icq)) ? $icq :
'';
define('ICQ_VALUE', $Variable);

define('AIM', $w[63].is_hide(C_AIMSH));
$Variable = (isset($aim)) ? $aim :
'';
define('AIM_VALUE', $Variable);

$Require = (C_PHONER) ? $w[0] :
'';
define('PHONE', $w[64].$Require.is_hide(C_PHONESH));
$Variable = (isset($phone)) ? $phone :
'';
define('PHONE_VALUE', $Variable);

$Require = (C_ZIPR) ? $w[0] :
'';
define('ZIP', $w[285].$Require.is_hide(C_ZIPSH));
$Variable = (isset($zip)) ? $zip :
'';
define('ZIP_VALUE', $Variable);

$Require = (C_CITYR) ? $w[0] : '';
define('CITY', $w[65].$Require.is_hide(C_CITYSH));
$Variable = (isset($city)) ? $city :
'';
define('CITY_VALUE', $Variable);

$Require = (C_ADDRESSR) ? $w[0] : '';
define('ADDRESS', $w[378].$Require.is_hide(C_ADDRESSSH));
$Variable = (isset($address)) ? $address : '';
define('ADDRESS_VALUE', $Variable);

$Require = (C_MARSR) ? $w[0] :'';
$Variable = '';
define('MARITAL_STATUS', $w[66].$Require.is_hide(C_MARSTATSH));
$p = 1;
while (isset($wm[$p]))
{
	if (isset($marstat) && ($marstat == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wm[$p];
	else $Variable .= '<option value="'.$p.'">'.$wm[$p];
	$p++;
}
define('MARITAL_STATUS_VALUE', $Variable);

$Require = (C_CHILDR) ? $w[0] :'';
$Variable = '';
define('CHILD', $w[67].$Require.is_hide(C_CHILDSH));
$p = 1;
while (isset($wc[$p]))
{
	if (isset($child) && ($child == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wc[$p];
	else $Variable .= '<option value="'.$p.'">'.$wc[$p];
	$p++;
}
define('CHILD_VALUE', $Variable);


$Require = (C_HGHTR) ? $w[0] :
'';
$Variable = '';
define('HEIGHT', $w[68].$Require.is_hide(C_HEIGHTSH));
$p = 1;
while (isset($wh[$p]))
{
	if (isset($height) && ($height == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wh[$p];
	else $Variable .= '<option value="'.$p.'">'.$wh[$p];
	$p++;
}
define('HEIGHT_VALUE', $Variable);

$Require = (C_WGHTR) ? $w[0] :
'';
$Variable = '';
define('WEIGHT', $w[69].$Require.is_hide(C_WEIGHTSH));
$p = 1;
while (isset($ww[$p]))
{
	if (isset($weight) && ($weight == $p)) $Variable .= '<option value="'.$p.'" selected>'.$ww[$p];
	else $Variable .= '<option value="'.$p.'">'.$ww[$p];
	$p++;
}
define('WEIGHT_VALUE', $Variable);

$Require = (C_HAIRR) ? $w[0] :'';
$Variable = '';
define('HAIR', $w[70].$Require.is_hide(C_HCOLORSH));
$p = 1;
while (isset($whc[$p]))
{
	if (isset($hcolor) && ($hcolor == $p)) $Variable .= '<option value="'.$p.'" selected>'.$whc[$p];
	else $Variable .= '<option value="'.$p.'">'.$whc[$p];
	$p++;
}
define('HAIR_VALUE', $Variable);

$Require = (C_EYER) ? $w[0] :'';
$Variable = '';
define('EYE', $w[71].$Require.is_hide(C_ECOLORSH));
$p = 1;
while (isset($we[$p]))
{
	if (isset($ecolor) && ($ecolor == $p)) $Variable .= '<option value="'.$p.'" selected>'.$we[$p];
	else $Variable .= '<option value="'.$p.'">'.$we[$p];
	$p++;
}
define('EYE_VALUE', $Variable);

$Require = (C_ETNR) ? $w[0] :
'';
$Variable = '';
define('ETHNICITY', $w[72].$Require.is_hide(C_ETHNICITYSH));
$p = 1;
while (isset($wet[$p]))
{
	if (isset($etnicity) && ($etnicity == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wet[$p];
	else $Variable .= '<option value="'.$p.'">'.$wet[$p];
	$p++;
}
define('ETHNICITY_VALUE', $Variable);

$Require = (C_RELR) ? $w[0] :
'';
$Variable = '';
define('RELIGION', $w[73].$Require.is_hide(C_RELIGIONSH));
$p = 1;
while (isset($wr[$p]))
{
	if (isset($religion) && ($religion == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wr[$p];
	else $Variable .= '<option value="'.$p.'">'.$wr[$p];
	$p++;
}
define('RELIGION_VALUE', $Variable);

$Require = (C_SMOKER) ? $w[0] :'';
$Variable = '';
define('SMOKE', $w[74].$Require.is_hide(C_SMOKESH));
$p = 1;
while (isset($ws[$p]))
{
	if (isset($smoke) && ($smoke == $p)) $Variable .= '<option value="'.$p.'" selected>'.$ws[$p];
	else $Variable .= '<option value="'.$p.'">'.$ws[$p];
	$p++;
}
define('SMOKE_VALUE', $Variable);

$Require = (C_DRINKR) ? $w[0] :'';
$Variable = '';
define('DRINK', $w[75].$Require.is_hide(C_DRINKSH));
$p = 1;
while (isset($wd[$p]))
{
	if (isset($drink) && ($drink == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wd[$p];
	else $Variable .= '<option value="'.$p.'">'.$wd[$p];
	$p++;
}
define('DRINK_VALUE', $Variable);

$Require = (C_EDUCR) ? $w[0] :'';
$Variable = '';
define('EDUCATION', $w[76].$Require.is_hide(C_EDUCATIONSH));
$p = 1;
while (isset($wed[$p]))
{
	if (isset($education) && ($education == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wed[$p];
	else $Variable .= '<option value="'.$p.'">'.$wed[$p];
	$p++;
}
define('EDUCATION_VALUE', $Variable);

$Require = (C_LANGR) ? $w[0] : '';$checklang=array();
define('LANG', $w[382].$Require.is_hide(C_LANGSH));$p = 0;
while (isset($wlng[$p])){
	$lang2 = @array_flip($lang);
	if (isset($lang2[$p])) $checklang[] = '<input type="checkbox" name="lang[]" value="'.$p.'" checked>';
	else  $checklang[] = '<input type="checkbox" name="lang[]" value="'.$p.'">';
	$p++;}

	$Require = (C_JOBR) ? $w[0] :	'';
	define('JOB', $w[77].$Require.is_hide(C_JOBSH));
	$Variable = (isset($job)) ? $job :
	'';
	define('JOB_VALUE', $Variable);

	$Require = (C_HOBBR) ? $w[0] :	'';
	define('HOBBY', $w[78].$Require.is_hide(C_HOBBYSH));
	$Variable = (isset($hobby)) ? $hobby :
	'';
	define('HOBBY_VALUE', $Variable);

	define('DESCRIPTION', $w[79].$w[0].is_hide(C_DESCRSH));
	$Variable = (isset($descr)) ? tb($descr) :
	'';
	define('DESCRIPTION_VALUE', $Variable);

	GenField('seeking_gender',C_SGENDR,C_SGENDERSH,'sgender',$wg,'80');
	GenField('seeking_country',C_SCNTRR,C_SCOUNTRYSH,'scountry',$wcr,'444',true);
	GenField('seeking_marital_status',C_SMARSR,C_SMARSTATSH,'smarstat',$wm,'445');
	GenField('seeking_child',C_SCHILDR,C_SCHILDSH,'schild',$wc,'446');
	GenField('seeking_hair',C_SHAIRR,C_SHCOLORSH,'shcolor',$whc,'447');
	GenField('seeking_eye',C_SEYER,C_SECOLORSH,'secolor',$we,'448');
	GenField('seeking_smoke',C_SSMOKER,C_SSMOKESH,'ssmoke',$ws,'449');
	GenField('seeking_drink',C_SDRINKR,C_SDRINKSH,'sdrink',$wd,'450');
	GenField('seeking_education',C_SEDUCR,C_SEDUCATIONSH,'seducation',$wed,'451');
	GenField('seeking_ethnicity',C_SETNR,C_SETHNICITYSH,'setnicity',$wet,'81');
	GenField('seeking_religion',C_SRELR,C_SRELIGIONSH,'sreligion',$wr,'82');


	$Require = (C_SAGER) ? $w[0] :	'';
	$Variable = '';
	define('SEEKING_AGE', $w[83].$Require.is_hide(C_SAGESH));
	for($p = C_AGES; $p <= C_AGEB; $p++)
	{
		if (isset($agef) && ($agef == $p)) $Variable .= '<option selected>'.$p;
		else $Variable .= '<option>'.$p;
	}
	define('SEEKING_AGE_FROM_VALUE', $Variable);
	$Variable = '';
	for($p = C_AGES; $p <= C_AGEB; $p++)
	{
		if (isset($aget) && ($aget == $p)) $Variable .= '<option selected>'.$p;
		else
		{
			if (($p == C_AGEB) && !isset($aget)) $Variable .= '<option selected>'.$p;
			else $Variable .= '<option>'.$p;
		}
	}
	define('SEEKING_AGE_TO_VALUE', $Variable);

	$Require = (C_SHGHTR) ? $w[0] :	'';
	$Variable = '';
	define('SEEKING_HEIGHT', $w[84].$Require.is_hide(C_SHEIGHTSH));
	$p = 1;
	while (isset($wh[$p]))
	{
		if (isset($heightf) && ($heightf == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wh[$p];
		else $Variable .= '<option value="'.$p.'">'.$wh[$p];
		$p++;
	}
	define('SEEKING_HEIGHT_FROM_VALUE', $Variable);
	$Variable = '';
	$p = 1;
	while (isset($wh[$p]))
	{
		if (isset($heightt) && ($heightt == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wh[$p];
		else
		{
			if (($p == (sizeof($wh)-1)) && !isset($heightt)) $Variable .= '<option value="'.$p.'" selected>'.$wh[$p];
			else $Variable .= '<option value="'.$p.'">'.$wh[$p];
		}
		$p++;
	}
	define('SEEKING_HEIGHT_TO_VALUE', $Variable);

	$Require = (C_SWGHTR) ? $w[0] :	'';
	$Variable = '';
	define('SEEKING_WEIGHT', $w[85].$Require.is_hide(C_SWEIGHTSH));
	$p = 1;
	while (isset($ww[$p]))
	{
		if (isset($weightf) && ($weightf == $p)) $Variable .= '<option value="'.$p.'" selected>'.$ww[$p];
		else $Variable .= '<option value="'.$p.'">'.$ww[$p];
		$p++;
	}
	define('SEEKING_WEIGHT_FROM_VALUE', $Variable);
	$Variable = '';
	$p = 1;
	while (isset($ww[$p]))
	{
		if (isset($weightt) && ($weightt == $p)) $Variable .= '<option value="'.$p.'" selected>'.$ww[$p];
		else
		{
			if (($p == (sizeof($ww)-1)) && !isset($weightt)) $Variable .= '<option value="'.$p.'" selected>'.$ww[$p];
			else $Variable .= '<option value="'.$p.'">'.$ww[$p];
		}
		$p++;
	}
	define('SEEKING_WEIGHT_TO_VALUE', $Variable);

	$Require = (C_HDYFUR) ? $w[0] :	'';
	$Variable = '';
	define('HOW_DID_YOU_FIND_US', $w[86].$Require.is_hide(FALSE));
	$p = 1;
	while (isset($wu[$p]))
	{
		$Variable .= '<option value="'.$p.'">'.$wu[$p];
		if (isset($hdyfu) && ($hdyfu == $p)) $Variable .= '<option value="'.$p.'" selected>'.$wu[$p];
		$p++;
	}
	define('HOW_DID_YOU_FIND_US_VALUE', $Variable);

	$Variable = '';
	define('WANTPRIV', $w[548]);
	$p = 1;
	$wantp = (isset($wantpriv))  ? 'checked' : '';
	define('WANTPRIV_VALUE', $wantp);

	$Variable = '';
	define('REG_TERMS', $w[631].$w[0]);
	$p = 1;
	$reg_terms = (isset($reg_terms))  ? 'checked' : '';
	define('REG_TERMS_VALUE', $reg_terms);

	$MyGender = 0;$grup=explode(",",C_PH_REQ_GEN);
	if(C_PH_REQ_GEN != 0)
	{
		# detecting gender for Require photo per gender feature
		$tmpg = db_fetch_array(db_query("SELECT gender FROM ". C_MYSQL_MEMBERS_MAIN ." WHERE id = '".cb($id)."'"));
		$MyGender = $tmpg['gender'];
	}

	$tm = array(C_MAXSZ);$PHOTO_NUM=array();
	for($i = 0; $i < $wpn[0]; $i++)
	{
		$k = $i+1;
		$Require = (((C_PHOTOR && C_PH_REQ_GEN == 0)  || (C_PHOTOR && C_PH_REQ_GEN != 0 && in_array ($MyGender, $grup))) && ($i < C_REQ_PHOTO_NUM)) ? $w[0] :	'';

		$PHOTO_NUM[] = $w[87].' '.$k.' '.template($w[223], $tm).is_hide(C_PHOTOSH).' '.$Require;
	}

	$tm = array(C_AUDIO_SIZE);
	switch(C_AUDIO_ALLOW)
	{
		case '1': define('AUDIO', $w[292].' '.template($w[223], $tm));break;
		case '2': define('AUDIO', $w[670].' '.template($w[223], $tm));break;
		case '3': define('AUDIO', $w[671].' '.template($w[223], $tm));break;
	}

	if ((C_AGR)) define('AGREEMENT', '1');
	else define('AGREEMENT', '0');

	if (!file_exists(C_PATH.'/languages/'.$l.'/agr.php') || (empty($l))) $l = 'default';
	if (!defined("REGISTER")) define('REGISTER', $w[89]);
	////////
	$p = 0;
	$k = 1;

	//include_once C_PATH.'/classes/Ajax.class.php';
	//$checkUsername = new Ajax('UsernameValue');
	
		
	include_once C_PATH.'/templates/'.C_TEMP.'/forum/smiles.php';
	BBCheck(C_BB_DESC_ACL);
	include_once C_PATH.'/templates/'.C_TEMP.'/register.php';


	include_once C_PATH.'/footer.php';
?>