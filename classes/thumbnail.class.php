<?php
class Thumbnail
{
	var $img;
	var $decr_only;
	var $cut_img;
	var $origfile;
	function __construct($imgfile,$decrease_size=null,$only_decrease=null,$Cutting=null)
	{
		$this->decr_only = ($only_decrease != null) ? True : False;
		$this->cut_img = ($Cutting != null) ? True : False;
		$this->origfile = $imgfile;
		$val = @getimagesize($imgfile);
		$arr = $val[2];
		$this->img['dwidth']= intval($val[0]);
		$this->img['dheight'] = intval($val[1]);
		ini_set ('gd.jpeg_ignore_warning', 1);
		switch($arr)
		{
			case '1':
				$this->img['format']='gif';
				$im = (function_exists('ImageCreateFromGIF')) ? @ImageCreateFromGIF($imgfile) : False;
				$this->img['src'] = ($im) ? $im : $this->ShowAsIs($imgfile);
				break;
			case '2':default:
				$this->img['format']='jpeg';
				$im = (function_exists('ImageCreateFromJPEG')) ? @ImageCreateFromJPEG($imgfile) : False;
				$this->img['src'] = ($im) ? $im : $this->ShowAsIs($imgfile);
				break;
			case '3':
				$this->img['format']='png';
				$im = (function_exists('ImageCreateFromPNG')) ? @ImageCreateFromPNG($imgfile) : False;
				$this->img['src'] = ($im) ? $im : $this->ShowAsIs($imgfile);
				break;
		}
		@$this->img['x'] = imagesx($this->img['src']);
		@$this->img['y'] = imagesy($this->img['src']);
		@$this->img['Quality']=75;
	}

	function ext($file)
	{
		if(function_exists("exif_imagetype")) // More faster extension detecting function
		{
			$arr = @exif_imagetype($file);
		}
		else
		{
			$val = @getimagesize($file);
			$arr = $val[2];
		}
		switch($arr)
		{
			case '1': return '.gif';break;
			case '2':default: return '.jpg';break;
			case '3': return '.png';break;
			case '4': return '.swf';break;
			case '5': return '.psd';break;
			case '6': return '.bmp';break;
			case '7': return '.tiff';break;
			case '8': return '.tiff';break;
			case '9': return '.jpc';break;
			case '10': return '.jp2';break;
			case '11': return '.jpx';break;
		}
	}

	function CheckFunctions()
	{
		Return (function_exists("ImageCreateTrueColor")) ? True : False;
	}

	function Height($size=100)
	{
		$this->img['y_thumb']=$size;
		@$this->img['x_thumb'] = ($this->img['y_thumb']/$this->img['y'])*$this->img['x'];
	}

	function Width($size=100)
	{
		$this->img['x_thumb']=$size;
		@$this->img['y_thumb'] = ($this->img['x_thumb']/$this->img['x'])*$this->img['y'];
	}

	function info()
	{
		echo $this->img['dwidth'];
		echo $this->img['dheight'];
	}

	function Auto($size=100)
	{
		if ($this->img['x']>=$this->img['y']) {
			$this->img['x_thumb']=$size;
			@$this->img['y_thumb'] = ($this->img['x_thumb']/$this->img['x'])*$this->img['y'];
		} else {
			$this->img['y_thumb']=$size;
			@$this->img['x_thumb'] = ($this->img['y_thumb']/$this->img['y'])*$this->img['x'];
		}
	}

	function Quality($Quality=75)
	{
		$this->img['Quality']=$Quality;
	}

	function ShowAsIs($Path)
	{
		if(!$this->decr_only)
		{
			header('Content-Type: image/'.$this->img['format']);
			header("Cache-control: public");
			header("Expires: ".date("r",mktime(0,0,0,1,1,2037)));
			header("Cache-control: max-age=".(60*60*24*7));
			header("Last-Modified: ".date("r",filemtime($Path)));
			$fp = fopen ($Path, "rb");
			$data = fread($fp, filesize($Path));
			#fpassthru($fp); // Alternate output
			fclose ($fp);
			Print $data;
			exit();
		}
		else
		{
			unlink($this->origfile);
			printm(template($GLOBALS['w'][819],array($this->img['format'])));
		}
	}

	function Show()
	{
		@Header('Content-Type: image/'.$this->img['format']);

		if ($this->CheckFunctions())
		$this->img['des'] = ImageCreateTrueColor($this->img['x_thumb'],$this->img['y_thumb']);
		else
		$this->img['des'] = ImageCreate($this->img['x_thumb'],$this->img['y_thumb']);


		if(function_exists('imagecopyresampled') && C_GD2_QUAL) @imagecopyresampled ($this->img['des'], $this->img['src'], 0, 0, 0, 0, $this->img['x_thumb'], $this->img['y_thumb'], $this->img['x'], $this->img['y']);
		else @imagecopyresized ($this->img['des'], $this->img['src'], 0, 0, $cutsize, $cutsize, $this->img['x_thumb'], $this->img['y_thumb'], $this->img['x']-($cutsize*2), $this->img['y']-($cutsize*2));


		if ($this->img['format']=='jpg' || $this->img['format']=='jpeg')
		{
			if(function_exists('imageJPEG')) imageJPEG($this->img['des'],'',$this->img['Quality']);
			else Return False;
		}
		elseif ($this->img['format']=='png')
		{
			if(function_exists('imagePNG')) imagePNG($this->img['des']);
			else Return False;
		}
		elseif ($this->img['format']=='gif')
		{
			if(function_exists('imageGIF')) imageGIF($this->img['des']);
			else Return False;
		}
	}

	function Save($Save='',$decrease_size=null)
	{

		if($decrease_size!=null)
		{
			$maxsize = ($this->img['dwidth'] >= $this->img['dheight']) ? $this->img['dwidth'] : $this->img['dheight'];
			if(($this->img['x_thumb'] > $this->img['dwidth']) || ($this->img['y_thumb'] > $this->img['dheight']))
			{
				$this->img['x_thumb'] = $this->img['dwidth'];
				$this->img['y_thumb'] = $this->img['dheight'];
			}
		}
		if (empty($Save)) $Save=strtolower('./t'.$this->img['format']);

		if($this->cut_img)
		{
			$cutsize = ($this->cut_img) ? intval(C_THUMB_SIDE) : 0;
			if ($this->img['x']>=$this->img['y']) {
				$otn = $this->img['x']/$this->img['y'];
				$ctx = $cutsize;$ctx2 = $ctx * 2;
				$cty = $cutsize / $otn;$cty2 = $cty * 2;
				@$this->img['y_thumb'] = round(($this->img['x_thumb']/($this->img['x']-$ctx2))*($this->img['y']-$cty2));
			} else {
				$otn = $this->img['y']/$this->img['x'];
				$cty = $cutsize;$cty2 = $cty * 2;
				$ctx = $cutsize / $otn;$ctx2 = $ctx * 2;
				@$this->img['x_thumb'] = round(($this->img['y_thumb']/($this->img['y']-$cty2))*($this->img['x']-$ctx2));
			}
			if ($this->CheckFunctions()) $this->img['des'] = ImageCreateTrueColor($this->img['x_thumb'],$this->img['y_thumb']);
			else $this->img['des'] = ImageCreate($this->img['x_thumb'],$this->img['y_thumb']);

			if(function_exists('imagecopyresampled') && C_GD2_QUAL)	@imagecopyresampled ($this->img['des'], $this->img['src'], 0, 0, $ctx, $cty, $this->img['x_thumb'], $this->img['y_thumb'], $this->img['x']-$ctx2, $this->img['y']-$cty2);
			else @imagecopyresized ($this->img['des'], $this->img['src'], 0, 0, $ctx, $cty, $this->img['x_thumb'], $this->img['y_thumb'], $this->img['x']-$ctx2, $this->img['y']-$cty2);

		}
		else {
			if ($this->CheckFunctions()) $this->img['des'] = ImageCreateTrueColor($this->img['x_thumb'],$this->img['y_thumb']);
			else $this->img['des'] = ImageCreate($this->img['x_thumb'],$this->img['y_thumb']);

			if(function_exists('imagecopyresampled') && C_GD2_QUAL)	@imagecopyresampled ($this->img['des'], $this->img['src'], 0, 0, 0, 0, $this->img['x_thumb'], $this->img['y_thumb'], $this->img['x'], $this->img['y']);
			else @imagecopyresized ($this->img['des'], $this->img['src'], 0, 0, 0, 0, $this->img['x_thumb'], $this->img['y_thumb'], $this->img['x'], $this->img['y']);
		}
		if ($this->img['format']=='jpg' || $this->img['format']=='jpeg')
		{
			if(function_exists('imageJPEG')) imageJPEG($this->img['des'],$Save,$this->img['Quality']);
			else Return False;
		}
		elseif ($this->img['format']=='png')
		{
			if(function_exists('imagePNG')) imagePNG($this->img['des'],$Save);
			else Return False;
		}
		elseif ($this->img['format']=='gif')
		{
			if(function_exists('imageGIF')) imageGIF($this->img['des'],$Save);
			else Return False;
		}
	}
}
?>