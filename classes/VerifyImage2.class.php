<?php
 # KCAPTCHA is a free software. You can freely use it for building own site or software.
# If you use this software as a part of own sofware, you must leave copyright notices intact or add KCAPTCHA copyright notices to own.
# Website: http://www.captcha.ru
# Changed by AzDG for better performance
class VerifyImage{
	function __construct (){
		$symb = "0123456789abcdefghijklmnopqrstuvwxyz";
		$oksymb = "23456789abcdeghkmnpqsuvxyz";
		$length = mt_rand(4,5);
		$jpg = 84;
		$width = 84;
		$height = 38;
		$flam = 3;

		$fc = array(mt_rand(0,100), mt_rand(0,100), mt_rand(0,100));
		$bgcol = array(mt_rand(200,255), mt_rand(200,255), mt_rand(200,255));

		$fonts=array(C_PATH.'/images/fonts/f1s.png');
		$symb_length=strlen($symb);

		while(true){
			$this->vc='';
			for($i=0;$i<$length;$i++){
				$this->vc.=$oksymb{mt_rand(0,strlen($oksymb)-1)};
			}
			if(!preg_match('/cp|cb|ck|c6|c9|rn|rm|mm|co|do|cl|db|qp|qb|dp|ww/', $this->vc)) break;
		}
		$font_file=$fonts[mt_rand(0, count($fonts)-1)];
		$font=imagecreatefrompng($font_file);
		imagealphablending($font, true);
		$f_width=imagesx($font);
		$f_height=imagesy($font)-1;
		$f_m=array();
		$symbol=0;
		$rs=false;
		for($i=0;$i<$f_width && $symbol<$symb_length;$i++){
			$transparent = (imagecolorat($font, $i, 0) >> 24) == 127;
			if(!$rs && !$transparent){
				$f_m[$symb{$symbol}]=array('start'=>$i);
				$rs=true;
				continue;
			}

			if($rs && $transparent){
				$f_m[$symb{$symbol}]['end']=$i;
				$rs=false;
				$symbol++;
				continue;
			}
		}
		$img=imagecreatetruecolor($width, $height);
		imagealphablending($img, true);
		$white=imagecolorallocate($img, 255, 255, 255);
		$black=imagecolorallocate($img, 0, 0, 0);

		imagefilledrectangle($img, 0, 0, $width-1, $height-1, $white);

		$x=1;
		for($i=0;$i<$length;$i++){
			$m=$f_m[$this->vc{$i}];
			$y=mt_rand(-$flam, $flam)+($height-$f_height)/2+2;
			$shift = ($i) ? 4 : 1;
			imagecopy($img, $font, $x-$shift, $y, $m['start'], 1, $m['end']-$m['start'], $f_height);
			$x+=$m['end']-$m['start']-$shift;
		}

		$center=$x/2;
		$rand1=mt_rand(750000,1200000)/10000000;
		$rand2=mt_rand(750000,1200000)/10000000;
		$rand3=mt_rand(750000,1200000)/10000000;
		$rand4=mt_rand(750000,1200000)/10000000;
		$rand5=mt_rand(0,31415926)/10000000;
		$rand6=mt_rand(0,31415926)/10000000;
		$rand7=mt_rand(0,31415926)/10000000;
		$rand8=mt_rand(0,31415926)/10000000;
		$rand9=mt_rand(330,420)/110;
		$rand10=mt_rand(330,450)/110;

		$img2=imagecreatetruecolor($width, $height);
		$foreground=imagecolorallocate($img2, $fc[0], $fc[1], $fc[2]);
		$background=imagecolorallocate($img2, $bgcol[0], $bgcol[1], $bgcol[2]);
		imagefilledrectangle($img2, 0, 0, $width-1, $height-1, $background);

		for($x=0;$x<$width;$x++){for($y=0;$y<$height;$y++){$buf[$x][$y] = imagecolorat($img, $x, $y) & 0xFF;}}
		for($x=0;$x<$width;$x++){
			for($y=0;$y<$height;$y++){
				$sx=$x+(sin($x*$rand1+$rand5)+sin($y*$rand3+$rand6))*$rand9-$width/2+$center+1;
				$sy=$y+(sin($x*$rand2+$rand7)+sin($y*$rand4+$rand8))*$rand10;
				if($sx<0 || $sy<0 || $sx>=$width-1 || $sy>=$height-1){
					continue;
				}else{
					$c = $buf[$sx][$sy];
					$c_x = $buf[$sx+1][$sy];
					$c_y = $buf[$sx][$sy+1];
					$c_xy = $buf[$sx+1][$sy+1];
				}
				if($c==255 && $c_x==255 && $c_y==255 && $c_xy==255){
					continue;
				}else if($c==0 && $c_x==0 && $c_y==0 && $c_xy==0){
					$newred=$fc[0];
					$newgreen=$fc[1];
					$newblue=$fc[2];
				}else{
					$frsx=$sx-floor($sx);
					$frsy=$sy-floor($sy);
					$frsx1=1-$frsx;
					$frsy1=1-$frsy;

					$nc = $c * $frsx1 * $frsy1 + $c_x * $frsx  * $frsy1 + $c_y * $frsx1 * $frsy + $c_xy * $frsx * $frsy;
					$nc = ($nc > 254) ? 1 : $nc / 255;
					$nc0 = 1 - $nc;

					$newred = $nc0 * $fc[0] + $nc * $bgcol[0];
					$newgreen = $nc0 * $fc[1] + $nc * $bgcol[1];
					$newblue = $nc0 * $fc[2] + $nc * $bgcol[2];
				}
				imagesetpixel($img2, $x, $y, imagecolorallocate($img2, $newred, $newgreen, $newblue));
			}
		}

		unset($buf);

		header('Expires: Mon, 21 Dec 1999 09:09:09 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0', FALSE);
		header('Pragma: no-cache');

		if(function_exists("imagejpeg")){
			header("Content-Type: image/jpeg");
			imagejpeg($img2, null, $jpg);
		}else if(function_exists("imagegif")){
			header("Content-Type: image/gif");
			imagegif($img2);
		}else if(function_exists("imagepng")){
			header("Content-Type: image/x-png");
			imagepng($img2);
		}
	}

	function ShowImage($pname=null) {
		if($pname === null) $pname = '';
        session_set_cookie_params(0, '/', C_DOMAIN, true, true);
		@session_start();$_SESSION['vc'.$pname] = $this->vc;
	}
}
?>