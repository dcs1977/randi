<?php
class VideoPlayer
{
    var $width  = 425;
    var $height = 350;   

    function _setReplace($posting, $results, $tmpl_player, $index=1)
    {
        foreach($results as $item)
            $posting = str_replace($item[0], str_replace('[ID]', $item[$index], $tmpl_player), $posting);
        return $posting;
    }    
    function getYoutube($posting)
    {
        $tmpl_player= '<object width="'.$this->width.'" height="'.$this->height.'">
        <param name="movie" value="http://www.youtube.com/v/[ID]"></param>
        <param name="wmode" value="transparent"></param>
        <embed src="http://www.youtube.com/v/[ID]" type="application/x-shockwave-flash" wmode="transparent" width="'.$this->width.'" height="'.$this->height.'"></embed>
        </object>';
        preg_match_all('/http:\/\/(www\.|uk\.)?youtube\.com\/watch\?v=([\d\w-_]+)/', $posting, $results, PREG_SET_ORDER);
        if($results) $posting = $this->_setReplace($posting, $results, $tmpl_player, $index=2);
        return $posting;
    }
    function getGoogle($posting)
    {
        $tmpl_player= '<object type="application/x-shockwave-flash" data="http://video.google.com/googleplayer.swf?docId=[ID]" height="'.$this->height.'" width="'.$this->width.'">
        <param name="movie" value="http://video.google.com/googleplayer.swf?docId=[ID]">
        </object>';
        preg_match_all('/http:\/\/video\.google\.com\/(googleplayer\.swf|videoplay)\?doc[Ii]d=([\d-]+)/', $posting, $results, PREG_SET_ORDER); 
        if($results) 
            $posting = $this->_setReplace($posting, $results, $tmpl_player, $index=2);
        return $posting;
    }
    function getClipfish($posting)
    {
        $tmpl_player = '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" width="'.$this->width.'" height="'.$this->height.'" id="player" align="middle">
        <param name="allowScriptAccess" value="sameDomain" />
        <param name="movie" value="http://www.clipfish.de/videoplayer.swf?as=0&videoid=[ID]&r=1" />
        <param name="wmode" value="transparent">  
        <param name="quality" value="high" />
        <param name="bgcolor" value="#999999" />
        <embed src="http://www.clipfish.de/videoplayer.swf?as=0&videoid=[ID]&r=1" quality="high" bgcolor="#999999" width="'.$this->width.'" height="'.$this->height.'" name="player" align="middle" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>
        </object>';
        preg_match_all('/http:\/\/www\.clipfish\.de\/player\.php\?videoid=([\d\w-_]+)/', $posting, $results, PREG_SET_ORDER);
        if($results) 
            $posting = $this->_setReplace($posting, $results, $tmpl_player);
        return $posting;
    }
    function getMyvideo($posting)
    {
        $tmpl_player = '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="'.$this->width.'" height="'.$this->height.'"><param name="movie" value="http://www.myvideo.de/movie/[ID]"></param><embed src="http://www.myvideo.de/movie/[ID]" width="'.$this->width.'" height="'.$this->height.'" type="application/x-shockwave-flash"></embed></object>';
        preg_match_all('/http:\/\/www\.myvideo\.de\/watch\/([\d]+)/', $posting, $results, PREG_SET_ORDER);
        if($results)
            $posting = $this->_setReplace($posting, $results, $tmpl_player);
        return $posting;
    }
    function getVideotube($posting)
    {
        $tmpl_player = '<object type="application/x-shockwave-flash" data="http://www.videotube.de/ci/flash/videotube_player_4.swf?videoId=[ID]&svsf=0&lang=german&host=www.videotube.de" width="'.$this->width.'" height="'.$this->height.'" wmode="transparent"><param name="movie" value="http://www.videotube.de/ci/flash/videotube_player_4.swf?videoId=[ID]&svsf=0&lang=german&host=www.videotube.de" /></object>';
        preg_match_all('/http:\/\/videotube\.de\/watch\/([\d]+)/', $posting, $results, PREG_SET_ORDER);
        if($results)
            $posting = $this->_setReplace($posting, $results, $tmpl_player);
        return $posting;
    }
    function getMyspacetv($posting)
    {
        $tmpl_player = '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" width="'.$this->width.'" height="'.$this->height.'" id="player" align="middle"><embed src="http://lads.myspace.com/videos/myspacetv_vplayer0005.swf?fuseaction=vids.individual&videoid=[ID]" width="'.$this->width.'" height="'.$this->height.'" flashvars="m=[ID]&type="application/x-shockwave-flash"></embed></object>';
        preg_match_all('/http:\/\/(myspacetv|vids\.myspace)\.com\/index\.cfm\?fuseaction=vids.individual&(videoid|VideoID)=([\d\w-_]+)/', $posting, $results, PREG_SET_ORDER);
        print $result;
        if($results) 
            $posting = $this->_setReplace($posting, $results, $tmpl_player, $index=3);
        return $posting;
    }
    function getAbcnews($posting)
    {
		$tmp_player = '<embed width="'.$this->width.'" height="'.$this->height.'" wmode="transparent" flashvars="file=http://ugv.abcnews.go.com/ChannelRssHandler.ashx%3fcontentItemID%3d[ID]%26mi%3d1&autostart=false&overstretch=false&LinkOnScreenClick=0&InitialVolume=40" quality="high" name="mpl" id="mpl" src="http://ugv.abcnews.go.com/dbox/3/flvplayer_abc_small.swf" type="application/x-shockwave-flash"/>';
		preg_match_all('/http:\/\/ugv\.abcnews\.go\.com\/player\.aspx\?id=([\d]+)/', $posting, $results, PREG_SET_ORDER);
		if ($results)
			$posting = $this->_setReplace($posting, $results, $tmp_player, $index=1);	
    	return $posting;
    }
}

function url_bld( $url=array() )
{
	$skip_it = 0;
	if ( preg_match( "/([\.,\?]|&#33;)$/", $url['html'], $match) )
	{
		$url['end'] .= $match[1];
		$url['html'] = preg_replace( "/([\.,\?]|&#33;)$/", "", $url['html'] );
		$url['show'] = preg_replace( "/([\.,\?]|&#33;)$/", "", $url['show'] );
	}
	if (preg_match( "/\[\/(html|quote|code|sql)/i", $url['html']) )
	{
		return $url['html'];
	}
	$url['html'] = str_replace( "&amp;" , "&"   , $url['html'] );
	$url['html'] = str_replace( "["     , "%5b" , $url['html'] );
	$url['html'] = str_replace( "]"     , "%5d" , $url['html'] );
	$url['html'] = preg_replace( "/javascript:/i", "java script&#58; ", $url['html'] );
	if ( ! preg_match("#^(http|news|https|ftp|aim)://#", $url['html'] ) )
	{
		$url['html'] = 'http://'.$url['html'];
	}
	if ( preg_match( "/^<img src/i", $url['show'] ) )
	{
		$skip_it     = 1;
		$url['show'] = stripslashes($url['show']);
	}

	$url['show'] = preg_replace( "/&amp;/" , "&" , $url['show'] );
	$url['show'] = preg_replace( "/javascript:/i", "javascript&#58; ", $url['show'] );

	if ( (strlen($url['show']) -58 ) < 3 )  $skip_it = 1;
	if ( ! preg_match( "/^(http|ftp|https|news):\/\//i", $url['show'] )) $skip_it = 1;

	$show = $url['show'];

	if ($skip_it != 1)
	{
		$stripped = preg_replace( "#^(http|ftp|https|news)://(\S+)$#i", "\\2", $url['show'] );
		$uri_type = preg_replace( "#^(http|ftp|https|news)://(\S+)$#i", "\\1", $url['show'] );

		$show = $uri_type.'://'.substr( $stripped , 0, 35 ).'...'.substr( $stripped , -15   );
	}

	return "<a href=\"".$url['html']."\" target=\"_blank\">".$show."</a>";
}

function video_bld( $url=array() )
{
	$skip_it = 0;
	if ( preg_match( "/([\.,\?]|&#33;)$/", $url['html'], $match) )
	{
		$url['end'] .= $match[1];
		$url['html'] = preg_replace( "/([\.,\?]|&#33;)$/", "", $url['html'] );
		$url['show'] = preg_replace( "/([\.,\?]|&#33;)$/", "", $url['show'] );
	}
	if (preg_match( "/\[\/(html|quote|code|sql)/i", $url['html']) )
	{
		return $url['html'];
	}
	$url['html'] = str_replace( "&amp;" , "&"   , $url['html'] );
	$url['html'] = str_replace( "["     , "%5b" , $url['html'] );
	$url['html'] = str_replace( "]"     , "%5d" , $url['html'] );
	$url['html'] = preg_replace( "/javascript:/i", "java script&#58; ", $url['html'] );
	if ( ! preg_match("#^(http|news|https|ftp|aim)://#", $url['html'] ) )
	{
		$url['html'] = 'http://'.$url['html'];
	}
	if ( preg_match( "/^<img src/i", $url['show'] ) )
	{
		$skip_it     = 1;
		$url['show'] = stripslashes($url['show']);
	}

	$url['show'] = preg_replace( "/&amp;/" , "&" , $url['show'] );
	$url['show'] = preg_replace( "/javascript:/i", "javascript&#58; ", $url['show'] );

	if ( (strlen($url['show']) -58 ) < 3 )  $skip_it = 1;
	if ( ! preg_match( "/^(http|ftp|https|news):\/\//i", $url['show'] )) $skip_it = 1;

	$show = $url['show'];

	if ($skip_it != 1)
	{
		$stripped = preg_replace( "#^(http|ftp|https|news)://(\S+)$#i", "\\2", $url['show'] );
		$uri_type = preg_replace( "#^(http|ftp|https|news)://(\S+)$#i", "\\1", $url['show'] );

		$show = $uri_type.'://'.substr( $stripped , 0, 35 ).'...'.substr( $stripped , -15   );
	}
	
	#$VideoPlayer = new VideoPlayer;

	#if ( preg_match( '/http:\/\/(www\.|uk\.)?youtube\.com\/watch\?v=([\d\w-_]+)/', $url['show'] ) ) return $VideoPlayer->getYoutube($url['show']);
}

function rem_str($start_tok, $end_tok)
{
	$start_tok = str_replace( '&amp;', '&', $start_tok );
	$end_tok   = str_replace( '&amp;', '&', $end_tok   );

	if ($start_tok == '?' and $end_tok == '')	return "";
	else if ($start_tok == '?' and $end_tok == '&')	return '?';
	else if ($start_tok == '&' and $end_tok == '') return "";
	else if ($start_tok == '&' and $end_tok == '&') return "&";
	else return $start_tok.$end_tok;
}

$BBimg_count = 0;
function img_bld($url="")
{
	Global $BBimg_count;
	if (! $url )	return;
	$url = trim($url);
	$default = "[img]".$url."[/img]";
	$BBimg_count++;

	if (C_BB_IMG_MAX)
	{
		if ($BBimg_count > C_BB_IMG_MAX)	return $default;
	}

	$url = urldecode( $url );
	$url = str_replace( "document.cookie", "", $url );

	if ( preg_match( "/[?&;]/", $url) || preg_match( "/java(\s+?)?script(\:|\s)/is", $url ) )	return $default;
	if ( C_BB_IMG_EXT )
	{
		$extension = preg_replace( "#^.*\.(\w+)(\?.*$|$)#", "\\1", $url );
		$extension = strtolower($extension);
		if ( (! $extension) OR ( preg_match( "#/#", $extension ) ) )		return $default;
	}

	$tmp = strtolower(C_BB_IMG_EXT);
	if ( ! preg_match( "/".preg_quote($extension, '/')."(,|$)/", $tmp ))	return $default;
	$url = str_replace( " ", "%20", $url );
	
	return "<img src=\"$url\" border=\"0\" />";
}

?>