<?php
class Upload {
    var $up_file;
	var $f;
    var $p;
	var $name;
	var $size;
	var $width;
	var $height;
	var $type;
	var $max_size;
	var $path;
	var $directory;
	var $errors;
	var $move;
	
	function __construct($input_name,$size,$width,$height,$dir) {
	    $this->f = $input_name;
		$this->max_size = $size;
		$this->directory = $dir;
		$this->max_width = $width;
		$this->max_height = $height;
	}
	
  function do_upload($audio=NULL) {
  //global $_FILES;
	    $this->up_file = $_FILES[$this->f]['tmp_name'];
		$this->name = $_FILES[$this->f]['name'];
		$this->size = $_FILES[$this->f]['size']/1000;
   	    $this->type = $_FILES[$this->f]['type'];
		$this->exif = false;
        if    (preg_match("/.gif$/", $this->type) && ($audio == NULL)) $this->type="gif";
        elseif((preg_match("/.png$/", $this->type)||
                preg_match("/.x-png$/", $this->type)) && ($audio == NULL)) $this->type="png";
        elseif((preg_match("/.jpg$/", $this->type)||
                preg_match("/.jpeg$/", $this->type)||
                preg_match("/.pjpeg$/", $this->type)) && ($audio == NULL)) $this->type="jpg";
        elseif(preg_match("/.mp3$/", $this->type) && (($audio == '1') || ($audio == '3'))) $this->type="mp3";
        elseif(preg_match("/.wav$/", $this->type) && (($audio == '1') || ($audio == '3'))) $this->type="wav";
        elseif(preg_match("/.mpeg$/", $this->type) && (($audio == '1') || ($audio == '3'))) $this->type="mp3";
        elseif(preg_match("/.midi$/", $this->type) && (($audio == '1') || ($audio == '3'))) $this->type="mid";
        elseif(preg_match("/.asf$/", $this->type) && ($audio >= '2')) $this->type="asf";
        elseif(preg_match("/.wmv$/", $this->type) && ($audio >= '2')) $this->type="wmv";
        elseif(preg_match("/.mpeg$/", $this->type) && ($audio >= '2')) $this->type="mpg";
        elseif(preg_match("/.mpg$/", $this->type) && ($audio >= '2')) $this->type="mpg";
        elseif(preg_match("/.msvideo$/", $this->type) && ($audio >= '2')) $this->type="avi";
        elseif(preg_match("/.octet-stream$/", $this->type) && ($audio >= '2')) $this->type="3gp";
        else $this->type=":".$this->type;
		$this->path = $this->directory;
        $this->width = $this->height = '';
        if($audio != NULL) {
          if(!empty($this->up_file)&&(C_HACK3)) {
            $this->p = getimagesize($this->up_file);
            $this->width = $this->p[0];  
            $this->height = $this->p[1];
			
            }  
        }
		$this->errors = '';
		
	  if($this->move_file($audio)) return true;
    else return false;
	}
	
	function getName() {
	  return $this->name;
	}
	
	function getMaxSize() {
	  return $this->max_size;
	}
	
	function getSize() {
	  return $this->size;
	}

	function getType() {
	  return $this->type;
	}
	
	function move_file($audio=NULL) {
	  if($this->check_size() && $this->check_extension() && $this->check_uploaded() && $this->check_wh()) {
		$newpath = ($audio == NULL) ? $this->path : $this->path.'.'.$this->type;
		move_uploaded_file($this->up_file, $newpath);
		chmod($newpath, 0644);
		
		if ($this->type == 'jpg')
		{
			if(function_exists("exif_read_data")){
				  $exif = exif_read_data($newpath);
				  
				  if(!empty($exif['Orientation'])) {
					  ini_set ('gd.jpeg_ignore_warning', 1);
						$src = ImageCreateFromJPEG($newpath);
					  switch($exif['Orientation']) {
					  case 1:
						  break;
					  case 2: 
						  $src = imageflip($src,IMG_FLIP_HORIZONTAL);
						   break;
					  case 3:
						  $src = imagerotate($src,180,0);
						  break;
					  case 4: 
						  $src = imageflip($src,IMG_FLIP_VERTICAL);
						  break;
					  case 5: 
						  $src = imageflip($src,IMG_FLIP_VERTICAL);
						  $src = imagerotate($src,-90,0);
						  break;
					  case 6:
						  $src = imagerotate($src,-90,0);
						  break;
					  case 7:
						  $src = imageflip($src,IMG_FLIP_HORIZONTAL);
						  $src = imagerotate($src,-90,0);
						  break;
					  case 8:
						  $src = imagerotate($src,90,0);
						  break;
					  
					  }
					  imagejpeg($src, $newpath);
				  }
			}
		}
		
		return true;
		} else {
		  return false;
		}
	}
	
	function check_size() {
    global $w; 
	  if(($this->up_file != "")&&($this->size <= $this->max_size)) {
      return true;
		} else {
		  if($this->errors == '') {
              $tm=array($this->max_size,$this->size);
			  $this->errors = $this->errors.template($w[220],$tm);
			}
		  return false;
		}
	}
	
	function check_wh() { // Check width and height
    global $w; 
	  if(($this->up_file != "")&&($this->width <= $this->max_width)&&($this->height <= $this->max_height)) {
      return true;
		} else {
		  if($this->errors == '') {
              $tm=array($this->max_width,$this->max_height);
			  $this->errors = $this->errors.template($w[221],$tm);
			}
		  return false;
		}
	}
	
	function check_uploaded() {
    global $w; 
	  if(is_uploaded_file($this->up_file)) {
			return true;
		} else {
		  if($this->errors == '') {
			  $this->errors = $this->errors.$w[1].'4';
			}
		  return false;
		}
	}
	
	function check_extension() {
    global $w; 
  		if (substr($this->type,0,1) == ":") {
  		    if($this->errors == '') {
  			   $this->errors = $this->errors.$w[222].$this->type;
  			}
  		  return false;
  		} else {
        return true;
		}
	}
	
	function getErrors() {
	  return $this->errors;
	}
	
}
?>