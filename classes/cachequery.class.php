<?php
//define('QUERY_LOG_DAY', '2021-04-13');

function db_query($query, $link_identifier = null){
    Global $mlink;
    $start = microtime(true);
    if ($link_identifier){
        $q = mysqli_query($link_identifier, $query);
    } else {
        $q = mysqli_query($mlink, $query);
    }
    $time_elapsed_secs = microtime(true) - $start;
    if ($time_elapsed_secs > 5){
        log_csv(array(array('timestamp' => date('Y-m-d H:i:s.u'), 'microtime' => $time_elapsed_secs, 'query' => $query)), date('Y-m-d-H').'-query.csv');
    }
    return $q;
}

function db_num_rows($result){
    return mysqli_num_rows($result);
}

function db_fetch_array($result, $resulttype = MYSQLI_BOTH){
    return mysqli_fetch_array($result, $resulttype);
}

function db_fetch_assoc($result){
    return mysqli_fetch_assoc($result);
}

function db_error($link_identifier = null){
    Global $mlink;
    if ($link_identifier){
        return mysqli_error($link_identifier);
    } else {
        return mysqli_error($mlink);
    }
}

function db_insert_id(){
    Global $mlink;
    return mysqli_insert_id($mlink);
}

$mlink=@mysqli_connect(C_HOST, C_USER, C_PASS);
@mysqli_select_db($mlink, C_BASE) or die("Can`t connect to server<br>Database doesn`t exist<br>Or Change Database name in config");

function db_fetch_row($result){
    return mysqli_fetch_row($result);
}

function db_get_server_info(){
    Global $mlink;
    return mysqli_get_server_info($mlink);
}

db_query("SET NAMES utf8");

class CacheQuery
{
	var $filename;
	var $query;

	function __construct($query)
	{
        $this->query = $query;
		$this->data = false;
		$this->read_mysql();
	}

	function read_mysql()
	{
	    Global $mlink;

		if(!$rs = mysqli_query($mlink, $this->query))
            trigger_error("Query Failed! SQL: ".$this->query." - Error: ".mysqli_error($mlink), E_USER_ERROR);
		//die(mysqli_error($this->mlink));

		while($row = mysqli_fetch_assoc($rs))
		$this->data[] = $row;

		if(empty($this->data))
		return false;
		else
		return true;
	}
}
?>