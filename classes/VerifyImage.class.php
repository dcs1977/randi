<?php
  class VerifyImage {
    var $key;       // ultra private static text
    var $long;      // size of text
    var $lx;        // width of picture
    var $ly;        // height of picture
    var $nb_noise;  // nb of background noisy characters
    var $filename;  // file of captcha picture stored on disk
    var $imgborder="1"; // image border
    var $imagetype="png"; // can also be "png";
    var $public_key;    // public key
    var $fonts=array();
    var $border_color = '0 0 0';
    var $grid_color = '64 94 153';
		var $grid_step = '12';
		var $grid_using = True;
		var $grid_random = True;
		
		function __construct($long=6,$lx=120,$ly=30,$nb_noise=20) {
      $this->key=md5("Background chars");
      $this->long=$long;
      $this->lx=$lx;
      $this->ly=$ly;
      $this->nb_noise=$nb_noise;
      $this->public_key=substr(md5(uniqid(rand(),true)),0,$this->long); // generate public key with entropy
    }

		function GetFont() {
			srand ((float) microtime() * 10000000);
			return $this->fonts[array_rand($this->fonts)];
		}
		
    function CreateImage() {
			$grid_color = $this->grid_color;
			$border_color = $this->border_color;
			$grid_step = $this->grid_step;
      $private_key = $this->public_key;
			$this->font = $this->GetFont();
      $image = imagecreatetruecolor($this->lx,$this->ly);
      $back=ImageColorAllocate($image,intval(rand(224,255)),intval(rand(224,255)),intval(rand(224,255)));
			$rgb = explode(" ",$border_color);
			imagefill($image, 0, 0, ImageColorAllocate($image, intval($rgb[0]), intval($rgb[1]), intval($rgb[2])));
      ImageFilledRectangle($image,$this->imgborder, $this->imgborder, $this->lx-$this->imgborder*2, $this->ly-$this->imgborder*2,$back);
			
			if($this->grid_using) {
				if($this->grid_random) {
        	for ($i=$grid_step;$i<$this->lx;$i+=$grid_step) {
          	$color=imagecolorallocate($image,intval(rand(160,224)),intval(rand(160,224)),intval(rand(160,224)));
          	imageline($image,$i,0,$i,$this->ly,$color);
        	}
        	for ($i=$grid_step;$i<$this->ly;$i+=$grid_step) {
          	$color=imagecolorallocate($image,intval(rand(160,224)),intval(rand(160,224)),intval(rand(160,224)));
          	imageline($image,0,$i,$this->lx,$i,$color);
        	}
				}
				else
				{
					$rgb = explode(" ",$grid_color);
					for($i=$grid_step;$i<$this->lx;$i+=$grid_step) {
						imageline($image,$i,0,$i,$this->ly, ImageColorAllocate($image, intval($rgb[0]), intval($rgb[1]), intval($rgb[2])));
					}
					for($i=$grid_step;$i<$this->ly;$i+=$grid_step) {
						imageline($image,0,$i,$this->lx,$i, ImageColorAllocate($image, intval($rgb[0]), intval($rgb[1]), intval($rgb[2])));
					}
				}
			}
        for ($i=0;$i<$this->nb_noise;$i++) {
          $size=intval(rand(10,12));
          $angle=intval(rand(0,360));
          $x=intval(rand(1,$this->lx-10));
          $y=intval(rand(1,$this->ly-5));
          $color=imagecolorallocate($image,intval(rand(160,224)),intval(rand(160,224)),intval(rand(160,224)));
          $text=chr(intval(rand(45,250)));
					#(function_exists('ImageTTFText')) ? ImageTTFText ($image,$size,$angle,$x,$y,$color,$this->font,$text) : imagechar($image, 5, $x,$y, $text, $color);
					imagechar($image, 5, $x,$y, $text, $color);
        }
      for ($i=0,$x=5; $i<$this->long;$i++) {
        $r=intval(rand(0,128));
        $g=intval(rand(0,128));
        $b=intval(rand(0,128));
        $color = ImageColorAllocate($image, $r,$g,$b);
        $shadow= ImageColorAllocate($image, $r+128, $g+128, $b+128);
        $size=intval(rand(14,18));
        $y=intval(rand(1,16));
        $angle=intval(rand(-30,30));
        $text=strtoupper(substr($private_key,$i,1));
        #if(function_exists('ImageTTFText')) ImageTTFText($image,$size,$angle,$x+2,26,$shadow,$this->font,$text);
        #(function_exists('ImageTTFText')) ? ImageTTFText($image,$size,$angle,$x,24,$color,$this->font,$text) : imagechar($image, 5, $x,4, $text, $color);
        imagechar($image, 5, $x,$y, $text, $color);
        $x+=$size+2;
      }
      if ($this->imagetype=="jpg")
        imagejpeg($image, 0, 80);
      else
        imagepng($image);
      ImageDestroy($image);
    }
    
    function ShowImage($pname=null) {
	if($pname === null) $pname = '';
	session_set_cookie_params(0, '/', C_DOMAIN, true, true);
	@session_start();$_SESSION['vc'.$pname] = $this->public_key;
    $this->CreateImage();
    }
  }
?>