<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
<div class="container-fluid mt-1 md-1">
  <div class="row">
    <div class="col-12 col-lg-8 offset-lg-2">

      <div class="card">
        <div class="card-header">
        <?=$_STATNAME?>
        </div>
        <div class="card-body">
          <table class="table table-striped">
            <tbody>
            <?php foreach($colors as $p => $item) { ?>
              <tr>
                <td>
                <?=$values[$p]?>
                </td>
                <Td>
                <?=$counts[$p]?>
                </td>
              </tr>
            <?php }?>
            </tbody>
          </table>
        </div>
      </div>

    </div>
  </div>
</div>