/***********************************************
* Disable select-text script- © Dynamic Drive (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit http://www.dynamicdrive.com/ for full source code
* Modified here to exclude form tags properly and cross browser by jscheuer1
***********************************************/

//form tags to omit:
var omitformtags=["input", "textarea", "select", "option", "button"]

function disableselect(e){
	for (i = 0; i < omitformtags.length; i++)
	if (omitformtags[i]==(e.target.tagName.toLowerCase()))
	return;
	return false
}

function reEnable(){
return true
}

function noSelect(){
if (typeof document.onselectstart!="undefined"){
document.onselectstart=new Function ("return false")
if (document.getElementsByTagName){
tags=document.getElementsByTagName('*')
for (j = 0; j < tags.length; j++){
for (i = 0; i < omitformtags.length; i++)
if (tags[j].tagName.toLowerCase()==omitformtags[i]){
tags[j].onselectstart=function(){
document.onselectstart=new Function ('return true')
}
tags[j].onmouseup=function(){
document.onselectstart=new Function ('return false')
}
}
}
}
}
else{
document.onmousedown=disableselect;
document.onmouseup=reEnable;
}
}

window.onload=noSelect();

if (document.addEventListener) {
    document.addEventListener('contextmenu', function(e) {
        //alert("You've tried to open context menu"); //here you draw your own menu
        for (i = 0; i < omitformtags.length; i++)
        if (e.target.tagName.toLowerCase()==omitformtags[i]){
			return;
		}
        e.preventDefault();
    }, false);
} else {
    document.attachEvent('oncontextmenu', function() {
        //alert("You've tried to open context menu");
        for (i = 0; i < omitformtags.length; i++)
        if (e.target.tagName.toLowerCase()==omitformtags[i]){
			return;
		}
        window.event.returnValue = false;
    });
}