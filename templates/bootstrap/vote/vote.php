<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
<form method="Post" action="<?=C_URL?>/vote/vote.php?l=<?=LANGUAGE?>&id=<?=VOTE_ID?>" target='vote' name='vote'>
    <div class="card mt-3 md-3">
        <div class="card-header"><?=VOTE?></div>
        <div class="card-body">
            <div class="form-group row">
                <div class="col-sm-12"><?=QUESTION?></div>
            </div>
            <?php while(isset($answers[$p])){?>
            <div class="form-group row">
                <div class="col-sm-12">
                    <div class="form-check">
                        <input id="vote_<?=$p?>" class="form-check-input" type="<?=$STYPE?>" name="vote[]" value="<?=$p?>">
                        <label class="form-check-label" for="vote_<?=$p?>">
                            <?=$answers[$p]?>
                        </label>
                    </div>
                </div>
            </div>
            <?php $p++;}?>
        </div>
        <div class="card-footer text-right">
            <button id="vote-button" class="btn btn-primary" type="submit"><?=VOTE?></button>
        </div>
    </div>
</form>
<script>
    $(document).ready(function () {
        $('#vote-button').click(function(e){
            e.preventDefault();
            MT.modalWindow.show({
                target: 'vote',
                iframeUrl: 'about:blank',
                animated: true,
                onComplete: function(){
                    $('form[name="vote"]').submit();
                }
            });
        })
    });
</script>
<div class="text-center">
    <a href="javascript:open_win('<?=C_URL?>/vote/view.php?l=<?=LANGUAGE?>&id=<?=VOTE_ID?>','vote');"><?=VIEW_RESULTS?></a>
</div>