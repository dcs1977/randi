<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>

<div class="container-fluid mt-3 md-3">
    <h3 class="text-center"><?=VOTE?></h3>
    <div class="row mt-3 md-3">
        <div class="col-12">
            <b><?=QUESTION?></b>
        </div>
    </div>
    <?php while(isset($result[$p])){?>
        <div class="row mt-2 md-2">
            <div class="col-12">
                <?=$text[$p]?> [<?=$votes[$p]?> <?=VOTES?>, <?=$proc[$p]?>%]
            </div>
        </div>
        <div class="row mt-2 md-2">
            <div class="col-12">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="<?=$graphic[$p]?>" aria-valuemin="0" aria-valuemax="150" style="width: <?=(ceil($graphic[$p] / 150 * 100))?>%"></div>
                </div>
            </div>
        </div>
    <?php $p++;}?>
    <div class="row mt-3 md-3">
        <div class="col-12">
            <?=TOTAL?>
        </div>
    </div>
</div>
