$(document).ready(function() {
	
	$('#banner_top a img').ready(function() {
	  var banner = $('#banner_top a img').attr('alt');
	  ga('send', 'event', 'banner_top', 'view', banner);
	  //console.log(banner);
	  
	  $('#banner_top a').click( function() {
		  var banner = $('#banner_top a img').attr('alt');
		  ga('send', 'event', 'banner_top', 'click', banner);
		  //console.log(banner);
		});
	});
	
	
	$('#right_b1 a img').ready( function() {
	  var banner = $('#right_b1 a img').attr('alt');
	  ga('send', 'event', 'right_b1', 'view', banner);
	  //console.log(banner);
	  
	  $('#right_b1 a').click( function() {
		  var banner = $('#right_b1 a img').attr('alt');
		  ga('send', 'event', 'right_b1', 'click', banner);
		  //console.log(banner);
		});
	});
	
	$('a').click( function() {
	  var link = $(this).attr('href');
	  if (link.substr(0,4)=='http')
	  {
	  		ga('send', 'event', 'link', 'click', link);
	  } 
	  //console.log(link);
	});
});