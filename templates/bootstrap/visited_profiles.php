<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
<div class="container-fluid mt-1 md-1">
	<div class="row">
		<div class="col-12 col-md-8 offset-md-2">
			<div class="card mb-1">
        <div class="card-header">
					<div class="row">
						<div class="col-12 text-center">
              <?=VISITED_PROFILES?>
						</div>
					</div>					
				</div>
        <div class="card-body">
					<div class="row">
						<div class="col-12">
              <?php if(defined("NOTHING_SEEN")) {?>
                <div class="text-center"><?=NOTHING_SEEN?></div>
              <?php } else {?>

                <table class="table table-striped">
                  <thead>
                  <tr>
                      <th>
                          <div class="row">
                              <div class="col-12 col-sm-3">
                                  <?=USERNAME?>
                              </div>
                              <div class="col-12 col-sm-3">
                                  <?=FROM?>
                              </div>
                              <div class="col-12 col-sm-3">
                                  <?=AGE?>
                              </div>
                              <div class="col-12 col-sm-3">
                                  <?=DATE?>
                              </div>
                          </div>
                      </th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php while(isset($visid[$p])) {?>     
                    <tr>
                        <td>
                            <div class="row">
                                <div class="col-12 col-sm-3">
                                    <a href="<?=GenLink('UserProfile',array(LANGUAGE,$visid[$p]))?>"><?=$names[$p]?></a>
                                </div>
                                <div class="col-12 col-sm-3">
                                    <?=$country[$p]?>
                                </div>
                                <div class="col-12 col-sm-3">
                                    <?=$ages[$p]?>
                                </div>
                                <div class="col-12 col-sm-3">
                                    <?=$visdate[$p]?>
                                </div>
                            </div>
                        </td>
                    </tr>
                  <?php $p++;}$p=0;?>  
                  </tbody>
                  <?=PAGES?>
                </table>
              <?php }?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
