<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>

<script language="JavaScript">
function sel() {
	with(document.form) {
		for(i=0;i<elements.length;i++) {
			thiselm = elements[i];
			if(thiselm.name.substring(0,3) == 'act')
			thiselm.checked = !thiselm.checked
		}
	}
}

function CheckAct() {
	with(document.form) {
		if(myactions.value == "" || myactions.value == "0") {
			alert("<?=$w[432]?>");
		} else {
			cnt=0;
			for(i=0;i<elements.length;i++) {thiselm = elements[i];
			if(thiselm.name.substring(0,3) == 'act') if(thiselm.checked) {
				cnt=cnt+1;
				v = thiselm.value.split("|");
				if(myactions.value >= 1 && myactions.value <= 8) {
					if(myactions.value == "1") {
						window.open("<?=C_URL?>/view.php?l=<?=LANGUAGE?>&id="+v[0], "_blank");
					} else if(myactions.value == "2") {
						window.open("<?=C_URL?>/sendk.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					} else if(myactions.value == "3") {
						window.open("<?=C_URL?>/send.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					} else if(myactions.value == "5") {
						window.open("<?=C_URL?>/mail.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					} else if(myactions.value == "6") {
						window.open("<?=C_URL?>/bedroom.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					} else if(myactions.value == "7") {
						window.open("<?=C_URL?>/ban.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					} else {
						window.open("<?=C_URL?>/bad.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					}
				}
			}
			}
			if(cnt == 0) alert("<?=$w[555]?>");
		}
	}
}
</script>
<form action="index.php" method="post" name=form>
<input class=input type=hidden name="l" value="<?=LANGUAGE?>">
<input class=input type=hidden name="a" value="d">
<TABLE align=center WIDTH="98%" BORDER="0" CELLSPACING="0" CELLPADDING="0"><TR><TD>

<br><div align="center"><span class=head>
<?php if(defined("VIEW_CREATED") && VIEW_CREATED){?><?=VIEW_CREATED?><br><br><?php }?>
<?=SEARCH_RESULTS?></span><br><br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_BWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td><Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_BWIDTH?>" class=mes>
  <Tr align="center" bgcolor="<?=COLORH?>">
    <Td></td>
	<td>
	<?php if(SHOW_PURPOSE){?><?=PURPOSES?><?php }?>
    </td>
    <Td width="400"><?=ADDITIONAL_INFO?></td>
    <td>
    <input type="checkbox" onClick="sel()">
    </td>
  </Tr>
<?php  while(isset($ids[$p])) { /* Search results - begin */?>  
  <?php if(C_SR_BANNER) show_banners('search_results',C_SR_BANNER,$p);?>
  <Tr align="center" bgcolor="<?=$colors[$p]?>">
    <Td width=100>
		<?php if(SHOW_PHOTO){?><p><?=$pictures[$p]?></p><?php }?>
		<p><a class=link2 href="<?=GenLink('UserProfile',array(LANGUAGE,$ids[$p]))?>"><?=$names[$p]?></a></p>
		<?php if(SHOW_AGE){?><p><?=$ages[$p]?> éves</p><?php }?>
		<?php if(SHOW_ORIENTATION){?><p><?=$orientations[$p]?></p><?php }?>
		<?php if(SHOW_ONLINE_STATUS){?><?php if(IsOnline($ids[$p])){?> <span style="color:green"><B><?=ONLINE?></B></span> 
		<?php }else{?> <?=OFFLINE?> <?php }?>
	</td>
    <td>
		<?php if(SHOW_COUNTRY){?><p><?=$countries[$p]?></p><?php }?>
		<?php if(SHOW_PURPOSE){?><p><?=$purposes[$p]?></p><?php }?>
	</td>
        
    <Td align=left valign=top>
        
    <table Border=0 CellSpacing="1" CellPadding="1" width="100%" class=mes>
    <?php if(SHOW_FNAME){?><tr><td width=50%><?=FIRSTNAME?>:</td><td width=50%><?=$fnames[$p]?></td></tr><?php }?>
    <?php if(SHOW_LNAME){?><tr><td><?=LASTNAME?>:</td><td><?=$lnames[$p]?></td></tr><?php }?>
	<?php if(WHENSEEN === true){?><tr><td width=50%><font color="#FFFF00"><?=DATE?>:</font></td><td width=50%><font color="#FFFF00"><?=$visdate[$p]?></font></td></tr><?php }?>
		<?php if(SHOW_CITY){?><tr><td><?=CITY?>:</td><td><?=$cities[$p]?></td></tr><?php }?>
		<?php if(SHOW_HORO){?><tr><td><?=HOROSCOPE?>:</td><td><?=$horoscopes[$p]?></td></tr><?php }?>
		<?php if(SHOW_BODYTYPE){?><tr><td><?=BODYTYPE?>:</td><td><?=$bodytypes[$p]?></td></tr><?php }?>
		<?php if(SHOW_ZIP){?><tr><td><?=ZIP?>:</td><td><?=$zips[$p]?></td></tr><?php }?>
		<?php if(SHOW_HEIGHT){?><tr><td><?=HEIGHT?>:</td><td><?=$heights[$p]?></td></tr><?php }?>
		<?php if(SHOW_WEIGHT){?><tr><td><?=WEIGHT?>:</td><td><?=$weights[$p]?></td></tr><?php }?>
		<?php if(SHOW_SEEKING_GENDER){?><tr><td><?=SEEKING_GENDER?>:</td><td><?=$sgenders[$p]?></td></tr><?php }?>
		<?php if(SHOW_PHOTO){?><tr><td><?=PHOTO?>:</td><td><?=$photoinfo[$p]?></td></tr><?php }?>
		<?php if(SHOW_REG_DATE){?><tr><td><?=REGISTER_DATE?>:</td><td><?=$regdates[$p]?></td></tr><?php }?>
		<?php if(SHOW_LAST_ACCESS){?><tr><td><?=LAST_ACCESS?>:</td><td><?=$editdates[$p]?></td></tr><?php }?>
    <?php if(SHOW_RATING){?><tr><td><?=RATING?>:</td><td><?=$rates[$p]?></td></tr><?php }?>
		</td></tr><?php }?>
    </table></td>
    <td><input type="checkbox" name="act[]" value="<?=$ids[$p].'|'.$names[$p]?>"></td>
  </Tr>
<?php  $p++;} /* Search results - end */?>
  </table>
  </td>
</tr>
</table>
<BR>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_BWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td><Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_BWIDTH?>" class=mes><?=PAGES?></table></td></tr></table>
<BR>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_BWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td><Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_BWIDTH?>" class=mes><tr><td bgcolor="<?=COLORH?>" align=right>
<?=WITH_SELECTED?> 
<select name=myactions class="select">
<option value=0> --------
<?=ShowActions()?>
</select> 
<input type="button" value="<?=$w[263]?>" class=button OnClick="javascript:CheckAct()"> 
</td></tr>
</table></td></tr></table>
  </TD>
 </TR>
</TABLE>
</form><br><br><br>
</div>