<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>

<script language="JavaScript">
function sel() {
	with(document.form) {
		for(i=0;i<elements.length;i++) {
			thiselm = elements[i];
			if(thiselm.name.substring(0,3) == 'act')
			thiselm.checked = !thiselm.checked
		}
	}
}

function CheckAct() {
	with(document.form) {
		if(myactions.value == "" || myactions.value == "0") {
			alert("<?=$w[432]?>");
		} else {
			cnt=0;
			for(i=0;i<elements.length;i++) {thiselm = elements[i];
			if(thiselm.name.substring(0,3) == 'act') if(thiselm.checked) {
				cnt=cnt+1;
				v = thiselm.value.split("|");
				if(myactions.value >= 1 && myactions.value <= 8) {
					if(myactions.value == "1") {
						window.open("<?=C_URL?>/view.php?l=<?=LANGUAGE?>&id="+v[0], "_blank");
					} else if(myactions.value == "2") {
						window.open("<?=C_URL?>/sendk.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					} else if(myactions.value == "3") {
						window.open("<?=C_URL?>/send.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					} else if(myactions.value == "5") {
						window.open("<?=C_URL?>/mail.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					} else if(myactions.value == "6") {
						window.open("<?=C_URL?>/bedroom.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					} else if(myactions.value == "7") {
						window.open("<?=C_URL?>/ban.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					} else {
						window.open("<?=C_URL?>/bad.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					}
				}
			}
			}
			if(cnt == 0) alert("<?=$w[555]?>");
		}
	}
}
</script>
<form action="index.php" method="post" name=form>
<input class=input type=hidden name="l" value="<?=LANGUAGE?>">
<input class=input type=hidden name="a" value="d">

<div class="container-fluid mt-1 md-1">
	<div class="row">
		<div class="col-12 col-md-6 offset-md-3">
			<div class="card mb-1">
                <div class="card-header">
					<div class="row">
						<div class="col-12 text-center">
							<?php if(defined("VIEW_CREATED") && VIEW_CREATED){?><?=VIEW_CREATED?><br><br><?php }?>
							<?=SEARCH_RESULTS?>
						</div>
					</div>					
				</div>
                <div class="card-body">
					<div class="row">
						<div class="col-12 text-right">
							<div class="btn-group-toggle" data-toggle="buttons" onClick="sel()">
								<label class="btn btn-light">
									<input type="checkbox" autocomplete="off"> Mindet jelöl
								</label>
							</div>
						</div>
					</div>
					<div class="row mt-1 md-1">
						<div class="col-12">
							<table class="table table-striped">
								<tbody>
								<?php  while(isset($ids[$p])) { /* Search results - begin */?>
									<tr>
										<td>
										<div class="row">
											<div class="col-6 text-left">
												<?php if(SHOW_ONLINE_STATUS){?><?php if(IsOnline($ids[$p])){?> <span style="color:green"><i class="fas fa-circle"></i></span> 
												<?php }else{?><i class="far fa-circle"></i><?php }}?>
												<a class=link2 href="<?=GenLink('UserProfile',array(LANGUAGE,$ids[$p]))?>"><?=$names[$p]?></a>
											</div>
											<div class="col-6 text-right">
												<div class="form-check">
													<input class="form-check-input position-static" type="checkbox" name="act[]" value="<?=$ids[$p].'|'.$names[$p]?>">
												</div>
											</div>
										</div>
										<div class="row mt-2">
											<div class="col-12 col-lg-6 text-center">
											<?php if(SHOW_PHOTO){?>
											<?=$pictures[$p]?>
											<div><?=$photoinfo[$p]?> kép</div>
											<?php }?>
											<?php if(SHOW_ORIENTATION){?><div><?=$orientations[$p]?></div><?php }?>
											</div>

											<div class="col-12 col-lg-6">
											<?php if(WHENSEEN === true){?>
											<div class="row py-lg-2">
												<div class="col-6"><b><font color="#FFFF00"><?=DATE?>:</font></b></div>
												<div class="col-6"><font color="#FFFF00"><?=$visdate[$p]?></font></div>
											</div>
											<?php }?>
                                                <?php if(SHOW_AGE){?>
                                                    <div class="row py-lg-2">
                                                        <div class="col-6"><b><?=AGE?>:</b></div>
                                                        <div class="col-6"><?=$ages[$p]?> éves</div>
                                                    </div>
                                                <?php }?>
											<?php if(SHOW_COUNTRY){?>
											<div class="row py-lg-2">
												<div class="col-6"><b><?=COUNTRY?>:</b></div>
												<div class="col-6"><?=$countries[$p]?></div>
											</div>
											<?php }?>
											<?php if(SHOW_CITY){?>
											<div class="row py-lg-2">
												<div class="col-6"><b><?=CITY?>:</b></div>
												<div class="col-6"><?=$cities[$p]?></div>
											</div>
											<?php }?>
											<?php if(SHOW_PURPOSE){?>
											<div class="row py-lg-2">
												<div class="col-6"><b><?=PURPOSES?>:</b></div>
												<div class="col-6"><?=$purposes[$p]?></div>
											</div>
											<?php }?>
											<?php if(SHOW_BODYTYPE){?>
											<div class="row py-lg-2">
												<div class="col-6"><b><?=BODYTYPE?>:</b></div>
												<div class="col-6"><?=$bodytypes[$p]?></div>
											</div>
											<?php }?>
											<?php if(SHOW_HEIGHT){?>
											<div class="row py-lg-2">
												<div class="col-6"><b><?=HEIGHT?>:</b></div>
												<div class="col-6"><?=$heights[$p]?></div>
											</div>
											<?php }?>
											<?php if(SHOW_WEIGHT){?>
											<div class="row py-lg-2">
												<div class="col-6"><b><?=WEIGHT?>:</b></div>
												<div class="col-6"><?=$weights[$p]?></div>
											</div>
											<?php }?>
											<?php if(CHILD){?>
											<div class="row py-lg-2">
												<div class="col-6"><b><?=CHILD?>:</b></div>
												<div class="col-6"><?=$childs[$p]?></div>
											</div>
											<?php }?>
											<?php if(SHOW_LAST_ACCESS){?>
											<div class="row py-lg-2">
												<div class="col-6"><b><?=LAST_ACCESS?>:</b></div>
												<div class="col-6"><?=$editdates[$p]?></div>
											</div>
											<?php }?>
											<?php if(SHOW_RATING && TOPS){?>
											<div class="row py-lg-2">
												<div class="col-6"><b><?=RATING?>:</b></div>
												<div class="col-6"><?=$rates[$p]?></div>
											</div>
											<?php }?>
											</div>
										</div>
										</td>
									</tr>
								<?php  $p++;} $p=0; /* Search results - end */?>
								</tbody>
							</table>
						</div>
					</div>
                </div>
                <div class="card-footer">
					<div class="row">
						<div class="form-group col-6"><?=WITH_SELECTED?></div>
						<div class="form-group col-4">
							<select class="form-control" name=myactions class="select">
							<option value=0> --------
							<?=ShowActions()?>
							</select>
						</div>
						<div class="form-group col-2">
							<button type="button" class="btn btn-primary btn-block" OnClick="javascript:CheckAct()"><?=$w[263]?></button>
						</div>
					</div>
				</div>
            </div>

			<div class="text-center"><?=PAGES?></div>

		</div>
	</div>

</form>
</div>