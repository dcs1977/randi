<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
<script language="JavaScript">
<!--
 self.focus();var cp="<?=UID?>";var CurImage="<?=PICNUM?>";var photo_loaded;var big_photo=false; var big_photo_text=false; var big_photo_wait=false;
 // if no DOM, work with reload
 if (document.getElementById) var noreload=1;  
 else var noreload=0;

 function showPhoto(uid,myurl,PicNum)
  {
  	if (!noreload) return true;

    td=document.getElementById("td_"+uid+PicNum);
    if (PicNum!=CurImage)
  	{
  		td_current=document.getElementById("td_"+uid+PicNum);
  	}
  	CurImage=PicNum;
    photo_small=document.getElementById("ps_"+uid+PicNum);
    big_photo_wait.innerHTML="<br><br><br><br><span class=head><?=PLEASE_WAIT?></span><br><br><br><br><br><br><br><br><br><br>";
    big_photo.style.display="none";
    big_photo.alt=photo_small.alt;
    big_photo_text.innerHTML=photo_small.alt;
    big_photo.src="<?=C_URL?>/templates/<?=C_TEMP?>/images/n.gif";

    photo_loaded=new Image();
    photo_loaded.src=myurl;
    IsPhotoLoaded();
    return false;
  }
 function displayPhoto()
  {
    if (big_photo)
     {
       big_photo.style.display="block";
       big_photo.src = photo_loaded.src;
     }
    if (big_photo_wait)
     {
       big_photo_wait.innerHTML="";
     }
  }

 function IsPhotoLoaded()
  {
    (photo_loaded.complete) ? setTimeout("displayPhoto()",500) : setTimeout("IsPhotoLoaded()",100);
  }

 function init()
  {
    big_photo=document.getElementById("bp");
    big_photo_text=document.getElementById("bp_text");
    big_photo_wait=document.getElementById("bp_wait");
  }
//-->
</script> 
<?php DisableRightClick();?>

<div class="container-fluid mt-1 md-1">
  <div class="row">
    <div class="col-12">
      <h3 class="text-center"><b><a href="<?=GenLink('UserProfile',array(LANGUAGE,UID))?>"><?=USERNAME_VALUE?></a>, <?=AGE_VALUE?> </b> <?=COUNTRY_VALUE?>, <?=CITY_VALUE?></h3>
      <div class="row">
        <div class="col-12">
          <div class="row">
            <div class="col-1 my-1 d-none d-lg-block">&nbsp;</div>
            <?php if (SHOW_PHOTOS) { /* If allow to show photos - Begin */ ?>
              <?php while(isset($pictures[$p])) { /* Show Photos - Begin */ ?>
              <div class="col-4 col-lg-1 col-md-2 col-sm-3 my-1 text-center" id="td_<?=UID?><?=$PicNum[$p]?>">
                <a href="<?=$pictures[$p]?>" onclick="return showPhoto('<?=UID?>','<?=$pictures[$p].ShowPictureBigger().'&picnum='.$PicNum[$p]?>',<?=$PicNum[$p]?>);"><img src="<?=$pictures[$p]?>" class="img-fluid img-thumbnail" id="ps_<?=UID?><?=$PicNum[$p]?>"></a>
              </div>
              <?php $p++;?>
              <?php if(C_VIEW_PHOTOS_STEP+$from == $p) break;} /* Show Photos - End */ ?>
            <?php } else { /* If allow to show photos - End */ ?>
              &nbsp; 
            <?php }?>
          </div>
        </div>
        <div class="col-12">
          <div id="bp_text" class="text-center"></div>
          <div id="bp_wait" class="text-center"></div>
          <div class="text-center">
            <img id="bp" class="img-fluid img-thumbnail mx-auto" src="<?=$pictures[PICNUM].ShowPictureBigger().'&picnum='.PICNUM?>" border="0">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script language="JavaScript"><!-- 
init(); 
//--> </script>
