<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>

<form action="?" method="post" name=form>
<input type="hidden" name="l" value="<?=LANGUAGE?>">
<input type="hidden" name="act" value="change">

<div class="container-fluid my-1 text-center">
    <h2><?=OPTIONS?></h2>
</div>

<div class="container-fluid my-1">
    <div class="row">
      <div class="col-12 col-lg-8 offset-lg-2">
        <div class="card mb-1">
            <div class="card-body">

				<?php if(IsAccess(C_OPTIONS_TEMPLATE)) {?>
				<div class="form-group row">
					<label class="col-12 col-md-3" for="ChangeTemplate"><?=CHANGE_DESIGN?></label>
					<div class="col-12 col-md-9">
					<select name="ChangeTemplate" class="form-control">
						<?=AVAILABLE_OPTIONS?>
					</select>
					</div>
				</div>
				<?php }?>

				<?php if(IsAccess(C_MY_TEMP)) {?>
				<div class="form-group row">
					<label class="col-12 col-md-4" for="ChangeTemplate"><?=MY_DESIGN?></label>
					<div class="col-12 col-md-8">
					<select name="ChangeTemplate" class="form-control">
						<?=MY_AVAILABLE_OPTIONS?>
					</select>
					</div>
				</div>
				<?php }?>

				<?php if(IsAccess(C_CAN_NO_RATE)) {?>
				<div class="form-group row">
				<label class="col-12 col-md-4" for="rate"><?=PARTICIPATE_IN_RATE?></label>
				<div class="col-12 col-md-8 form-check text-center">
					<input class="form-check-input position-static" type="checkbox" name="rate" id="rate" <?=PARTICIPATE_IN_RATE_VALUE?>>
				</div>
				</div>
				<?php }?>

				<?php if(IsAccess(C_HIDE_PROFILE)) {?>
				<div class="form-group row">
				<label class="col-12 col-md-4" for="hide"><?=HIDE_PROFILE?></label>
				<div class="col-12 col-md-8 form-check text-center">
					<input class="form-check-input position-static" type="checkbox" name="hide" id="hide" <?=HIDE_PROFILE_VALUE?>>
				</div>
				</div>
				<?php }?>

				<?php if(IsAccess(C_OPT_NOMAIL)) {?>
				<div class="form-group row">
				<label class="col-12 col-md-4" for="nomail"><?=NOMAIL?></label>
				<div class="col-12 col-md-8 form-check text-center">
					<input class="form-check-input position-static" type="checkbox" name="nomail" id="nomail" <?=NOMAIL_VALUE?>>
				</div>
				</div>
				<?php }?>

				<?php if(IsAccess(C_NO_KISS)) {?>
				<div class="form-group row">
				<label class="col-12 col-md-4" for="nokiss"><?=NOKISS?></label>
				<div class="col-12 col-md-8 form-check text-center">
					<input class="form-check-input position-static" type="checkbox" name="nokiss" id="nokiss" <?=NOKISS_VALUE?>>
				</div>
				</div>
				<?php }?>

				<?php if(IsAccess(C_IMSOUND)) {?>
				<div class="form-group row">
				<label class="col-12 col-md-4" for="imsound"><?=IMSOUND?></label>
				<div class="col-12 col-md-8 form-check text-center">
					<input class="form-check-input position-static" type="checkbox" name="imsound" id="imsound" <?=IMSOUND_VALUE?>>
				</div>
				</div>
				<?php }?>

				<div class="form-group row">
					<label class="col-12 col-md-4" for="timediff">
					<?=TIME_DIFFERENCE?> <BR>
					<?=CURRENT_DATE?> <BR>
					<?=MY_CURRENT_DATE?> 
					</label>
					<div class="col-12 col-md-8">
						<input type="text" class="form-control" name="timediff" value=<?=CURRENT_DATE_VALUE?>>
					</div>
				</div>
			
			</div>
		</div>
		</div>
	</div>
</div>

<div class="container-fluid my-1 text-center">
    <button class="btn btn-primary" type="submit" name="submit" value="<?=CHANGE_OPTIONS?>"><?=CHANGE_OPTIONS?></button>
</div>
</form>


<?php if(IsAccess(C_PROF_LNK)){?>
<form action="?" method="post" name=link>
<input type="hidden" name="l" value="<?=LANGUAGE?>">
<input type="hidden" name="act" value="change_link">
<div class="container-fluid my-1 text-center">
    <h2><?=LINKING_PROFILES?></h2>
	<p class="text-center"><?=LINKING_NOTE?></p>
</div>

<div class="container-fluid my-1">
    <div class="row">
      	<div class="col-12 col-lg-8 offset-lg-2">
			<div class="card mb-1">
				<div class="card-body">

				<div class="form-group row">
					<label class="col-12 col-md-4" for="link"><?=USER_ID?></label>
					<div class="col-12 col-md-4">
						<input type="text" class="form-control" name="link" value=<?=USER_ID_VALUE?>>
					</div>
					<div class="col-12 col-md-4">
						<button type="button" class="btn btn-default btn-block" name="submit"  OnCLick="javascript:window.open('<?=C_URL?>/view.php?l=<?=LANGUAGE?>&id='+document.link.link.value,'vp')" value="Check">Check</button>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-12 col-md-4" for="link_txt"><?=LINKING_DESC?></label>
					<div class="col-12 col-md-8">
						<input type="text" class="form-control" name="link_txt" value=<?=LINKING_DESCR?>>
					</div>
				</div>

				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid my-1 text-center">
    <button class="btn btn-primary" type="submit" name="submit" value="<?=CHANGE_OPTIONS?>"><?=CHANGE_OPTIONS?></button>
</div>
</form>
<?php }?>