<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>

<div class="container-fluid mt-1 md-1">
  <div class="row">
    <?php if(HAVE_USER_WITH_PHOTO): /* If User with photo exist - Begin */ ?>
      <?php if(is_numeric(get_info('m'))):?>
      <div class="col-12 col-lg-8">
          <?php else:?>
          <div class="col-12 offset-lg-2 col-lg-8">
          <?php endif;?>
              
              <?php if(C_NEWS_ALLOW && NEWS_NUMBER):?>
                <?php while(isset($ndates[$p])){?>
                  <div class="card mb-1">
                      <div class="card-header"><?=$nsubjects[$p]?></div>
                      <div class="card-body">
                        <?=$nnews[$p]?>
                      </div>
                      <div class="card-footer"><?=$ndates[$p]?></div>
                  </div>
                <?php $p++; } $p = 0;?>
            <?php endif;?>

            <?php if(SHOW_ARCHIVE):?>
                <div class="text-center">
                  <a href="javascript:open_win('<?=C_URL?>/news.php?l=<?=LANGUAGE?>','news');"><?=NEWS_ARCHIVE?></a>
                </div>
            <?php endif;?>
              
        <div class="card">
        <div class="card-header">
        <?=PHOTO_OF_DAY?>
        </div>
        <div class="card-body row pt-1 pb-1">
          <div class="col-4 col-sm-3 col-md-2 my-2 pl-1 pr-1 align-middle">
            <a href="<?=GenLink('UserProfile',array(LANGUAGE,$_SESSION['photos'][0]['id']))?>"><img class="img-fluid img-thumbnail" src="<?=$_SESSION['photos'][0]['name']?>" alt="<?=PHOTO_OF_DAY?>"></a>
          </div>
          <div class="col-4 col-sm-3 col-md-2 my-2 pl-1 pr-1 align-middle">
            <a href="<?=GenLink('UserProfile',array(LANGUAGE,$_SESSION['photos'][1]['id']))?>"><img class="img-fluid img-thumbnail" src="<?=$_SESSION['photos'][1]['name']?>" alt="<?=PHOTO_OF_DAY?>"></a>
          </div>
          <div class="col-4 col-sm-3 col-md-2 my-2 pl-1 pr-1 align-middle">
            <a href="<?=GenLink('UserProfile',array(LANGUAGE,$_SESSION['photos'][2]['id']))?>"><img class="img-fluid img-thumbnail" src="<?=$_SESSION['photos'][2]['name']?>" alt="<?=PHOTO_OF_DAY?>"></a>
          </div>
          <div class="col-4 col-sm-3 col-md-2 my-2 pl-1 pr-1 align-middle">
            <a href="<?=GenLink('UserProfile',array(LANGUAGE,$_SESSION['photos'][3]['id']))?>"><img class="img-fluid img-thumbnail" src="<?=$_SESSION['photos'][3]['name']?>" alt="<?=PHOTO_OF_DAY?>"></a>
          </div>
          <div class="col-4 col-sm-3 col-md-2 my-2 pl-1 pr-1 align-middle">
            <a href="<?=GenLink('UserProfile',array(LANGUAGE,$_SESSION['photos'][4]['id']))?>"><img class="img-fluid img-thumbnail" src="<?=$_SESSION['photos'][4]['name']?>" alt="<?=PHOTO_OF_DAY?>"></a>
          </div>
          <div class="col-4 col-sm-3 col-md-2 my-2 pl-1 pr-1 align-middle">
            <a href="<?=GenLink('UserProfile',array(LANGUAGE,$_SESSION['photos'][5]['id']))?>"><img class="img-fluid img-thumbnail" src="<?=$_SESSION['photos'][5]['name']?>" alt="<?=PHOTO_OF_DAY?>"></a>
          </div>
          <div class="col-4 col-sm-3 col-md-2 my-2 pl-1 pr-1 align-middle">
            <a href="<?=GenLink('UserProfile',array(LANGUAGE,$_SESSION['photos'][6]['id']))?>"><img class="img-fluid img-thumbnail" src="<?=$_SESSION['photos'][6]['name']?>" alt="<?=PHOTO_OF_DAY?>"></a>
          </div>
          <div class="col-4 col-sm-3 col-md-2 my-2 pl-1 pr-1 align-middle">
            <a href="<?=GenLink('UserProfile',array(LANGUAGE,$_SESSION['photos'][7]['id']))?>"><img class="img-fluid img-thumbnail" src="<?=$_SESSION['photos'][7]['name']?>" alt="<?=PHOTO_OF_DAY?>"></a>
          </div>
          <div class="col-4 col-sm-3 col-md-2 my-2 pl-1 pr-1 align-middle">
            <a href="<?=GenLink('UserProfile',array(LANGUAGE,$_SESSION['photos'][8]['id']))?>"><img class="img-fluid img-thumbnail" src="<?=$_SESSION['photos'][8]['name']?>" alt="<?=PHOTO_OF_DAY?>"></a>
          </div>
          <div class="col-4 col-sm-3 col-md-2 my-2 pl-1 pr-1 align-middle">
            <a href="<?=GenLink('UserProfile',array(LANGUAGE,$_SESSION['photos'][9]['id']))?>"><img class="img-fluid img-thumbnail" src="<?=$_SESSION['photos'][9]['name']?>" alt="<?=PHOTO_OF_DAY?>"></a>
          </div>
          <div class="col-4 col-sm-3 col-md-2 my-2 pl-1 pr-1 align-middle">
            <a href="<?=GenLink('UserProfile',array(LANGUAGE,$_SESSION['photos'][10]['id']))?>"><img class="img-fluid img-thumbnail" src="<?=$_SESSION['photos'][10]['name']?>" alt="<?=PHOTO_OF_DAY?>"></a>
          </div>
          <div class="col-4 col-sm-3 col-md-2 my-2 pl-1 pr-1 align-middle">
            <a href="<?=GenLink('UserProfile',array(LANGUAGE,$_SESSION['photos'][11]['id']))?>"><img class="img-fluid img-thumbnail" src="<?=$_SESSION['photos'][11]['name']?>" alt="<?=PHOTO_OF_DAY?>"></a>
          </div>
        </div>
      </div>
      </div>
    
    <div class="col-12 mt-3 col-lg-4 mt-lg-0">
    <?php else:?>
    <div class="col-12">
    <?php endif;?>

        <?php if(is_numeric(get_info('m'))):?>
        
    <?=ShowVote()?>
        
      <div class="card">
        <input type="hidden" name="l" value="<?=LANGUAGE?>">
        <input type="hidden" name="a" value="s">
          <div class="card-header">
            <?=QUICK_SEARCH?>
          </div>
          <form class="mb-0" action="<?=C_URL?>/qsearch.php" method="get">
          <input type="hidden" name="l" value="<?=LANGUAGE?>">
          <input type="hidden" name="a" value="s">
          <div class="card-body">
            
            <?php if(I_AM_A_SHOW): /* If is allow to show this section */ ?>
              <div class="form-group row">
                <label for="sgender" class="col-sm-2 col-form-label"><?=I_AM_A?></label>
                <div class="col-sm-10">
                  <select class="form-control" name="sgender">
                    <?=I_AM_A_VALUE?>
                  </select>
                </div>
              </div>
            <?php endif;?>

            <?php if(SEEKING_FOR_SHOW): /* If is allow to show this section */ ?>
              <div class="form-group row">
                <label for="gender" class="col-sm-2 col-form-label"><?=SEEKING_FOR?></label>
                <div class="col-sm-10">
                  <select class="form-control" name="gender[]">
                    <?=SEEKING_FOR_VALUE?>
                  </select>
                </div>
              </div>
            <?php endif;?>

            <?php if(AGE_SHOW): /* If is allow to show this section */ ?>
              <div class="form-group row">
                <label for="agef" class="col-sm-2 col-form-label"><?=AGE?></label>
                <div class="col-sm-5">
                  <input name="agef" class="form-control">
                </div>
                <div class="col-sm-5">
                  <input name="aget" class="form-control">
                </div>
              </div>
            <?php endif;?>

            <?php if(COUNTRY_SHOW): /* If is allow to show this section */ ?>
              <div class="form-group row">
                <label for="country" class="col-sm-2 col-form-label"><?=COUNTRY?></label>
                <div class="col-sm-10">
                  <select class="form-control" name="country[]">
                    <?=COUNTRY_VALUE?>
                  </select>
                </div>
              </div>
            <?php endif;?>

            <?php if(PHOTO_SHOW || ONLINE_USERS_ONLY_SHOW): /* If is allow to show this section */ ?>
              <div class="form-group row">
                <label for="photo" class="col-sm-2 col-form-label"> </label>
                  <?php if(PHOTO_SHOW): /* If is allow to show this section */ ?>
                  <div class="col-sm-5">
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" name="photo" id="quicksearch-photo">
                      <label class="form-check-label" for="quicksearch-photo">
                        <?=PHOTO?>
                      </label>
                    </div>
                  </div>
                  <?php endif;?>
                  <?php if(ONLINE_USERS_ONLY_SHOW): /* If is allow to show this section */ ?>
                  <div class="col-sm-5">
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" name="online" id="quicksearch-online">
                      <label class="form-check-label" for="quicksearch-online">
                        <?=ONLINE_USERS_ONLY?>
                      </label>
                    </div>
                  </div>
                  <?php endif;?>
              </div>            
            <?php endif;?>

            <?php if(ALLOW_BSEARCH): /* If is allow to show this section */ ?>
              <div class="form-group row">
                <label for="year" class="col-sm-2 col-form-label"><?=BIRTHDAY?></label>
                <div class="col-sm-3">
                  <select class="form-control" name="year">
                    <option selected value=0>-- 
                    <?=YEAR_VALUE?>
                  </select>
                </div>
                <div class="col-sm-3">
                  <select class="form-control" name="month">
                    <option selected value=0>--
                    <?=MONTH_VALUE?>
                  </select>
                </div>
                <div class="col-sm-3">
                  <select class="form-control" name="day">
                    <option selected value=0>--
                    <?=DAY_VALUE?>
                  </select>
                </div>
              </div>
          <?php else:?>
            <input type="hidden" name="month" value="0">
            <input type="hidden" name="day" value="0">
            <input type="hidden" name="year" value="0">
          <?php endif;?>

          </div>

          <div class="card-footer">
            <div class="row">
              <div class="col-sm-6 text-left">
                <!--a href="<?=C_URL?>/search.php?l=<?=LANGUAGE?>"><?=SEARCH?></a-->
                <a href="<?=C_URL?>/advsearch.php?l=<?=LANGUAGE?>"><?=SEARCH?></a>
              </div>
              <div class="col-sm-6 text-right">
                <button type="submit" class="btn btn-primary"><?=QUICK_SEARCH?></button>
              </div>
            </div>
          </div>

        </form>
      </div>
        
    </div>
    <?php endif;?>

  </div>
</div>

<div class="container-fluid mt-3 md-3">
  <div class="row">

      <?php if(is_numeric(get_info('m'))):?>
    <div class="col-12 col-sm-4 order-1 order-sm-6"> <!-- Középső -->
        <?php else:?>
        <div class="col-12 offset-sm-2 col-sm-8 order-1 order-sm-6">
        <?php endif;?>

        <?php if(is_numeric(get_info('m'))):?>
      <div class="card">
          <div class="card-header text-center"><?=LAST_REGISTERED1?></div>
          <div class="card-body">
            <table class="table table-striped">
                <tbody>
                  <?php while(isset($_SESSION['last_profiles']['lids'][1][$p])) { /* Top Mans show - Begin */ ?>
                  <tr>
                    <td>
                      <div class="row">
                        <div class="col-12 col-lg-6">
                        <?php if(IsAccess(C_LAST_SHOW_PHOTOS)) {?>
                          <?=$_SESSION['last_profiles']['lpics'][1][$p]?>
                        <?php } else {?>
                          <?=$_SESSION['last_profiles']['lphotos'][1][$p]?>
                        <?php }?>
                        </div>
                        <div class="col-12 col-lg-6">
                          <div class="text-center py-lg-2"><a href="<?=GenLink('UserProfile',array(LANGUAGE,$_SESSION['last_profiles']['lids'][1][$p]))?>"><?=$_SESSION['last_profiles']['lnames'][1][$p]?></a></div>
                          <div class="text-center py-lg-2"><?=$_SESSION['last_profiles']['lages'][1][$p]?> éves</div>
                          <div class="text-center pt-lg-3 pb-lg-1"><?=REGDATE?>:</div>
                          <div class="text-center pt-lg-1 pb-lg-3"><b><?=$_SESSION['last_profiles']['lregdate'][1][$p]?></b></div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <?php $p++;}$p=0; /* Show last registered users - Begin */ ?>
                </tbody>
              </table>
          <div class="text-center">
          <a href="<?=GenLink('Last',array(LANGUAGE,'0','0'))?>" class="link3"><?=MORE_RESULTS?></a>
          </div>
          </div>
          <div class="card-footer text-center"><?=TOTAL_USERS?></div>
      </div>
        <?php endif;?>
    </div>

      <?php if(is_numeric(get_info('m'))):?>
    <div class="col-12 mt-3 col-sm-4 mt-sm-0 order-6 order-sm-1"> <!-- Bal -->
      <div class="card">
          <div class="card-header text-center"><?=THE_BEST_MANS?></div>
          <div class="card-body">
            <table class="table table-striped">
              <tbody>
                <?php while(isset($tmids[$p])) { /* Top Mans show - Begin */ ?>
                <tr>
                  <td>
                    <div class="row">
                      <div class="col-12 col-lg-6">
                        <a href="<?=GenLink('UserProfile',array(LANGUAGE,$tmids[$p]))?>">
                          <img  class="img-fluid img-thumbnail" src="<?=$tmpics[$p]?>">
                        </a>
                      </div>
                      <div class="col-12 col-lg-6">
                        <div class="text-center py-lg-2"><a href="<?=GenLink('UserProfile',array(LANGUAGE,$tmids[$p]))?>"><?=$tmnames[$p]?></a></div>
                        <div class="text-center py-lg-2"><?=$tmages[$p]?> éves</div>
                        <div class="text-center py-lg-2"><?=$tmcountries[$p]?></div>
                        <div class="text-center py-lg-2"><b><?=$tmhits[$p]?></b></div>
                      </div>
                    </div>
                  </td>
                </tr>
                <?php $p++;}$p=0; /* Show last registered users - Begin */ ?>
              </tbody>
            </table>
          </div>
          <div class="card-footer text-center">
            <?php if(SHOW_MORE_RESULTS){?>
            <a href="<?=GenLink('Top',array(LANGUAGE,0,1,1))?>"><?=MORE_RESULTS?></a>
            <?php }?>
          </div>
      </div>
    </div>
      <?php endif;?>

      <?php if(is_numeric(get_info('m'))):?>
    <div class="col-12 mt-3 col-sm-4 mt-sm-0 order-12 order-sm-12"> <!-- Jobb -->
      <div class="card">
          <div class="card-header text-center">Boldog születésnapot</div>
          <div class="card-body">
            <?php if(BIRTHDAYS_EXIST) echo NO_BIRTHDAYS; else {  ?>
              <table class="table table-striped">
                <tbody>
                  <?php while(isset($bids[$p])) { /* Show all birthdays - Begin */ ?>
                  <tr>
                    <td>
                      <div class="row">
                        <div class="col-12 col-lg-6">
                          <?=$bpics[$p]?>
                        </div>
                        <div class="col-12 col-lg-6">
                          <div class="text-center py-lg-4"><a href="<?=GenLink('UserProfile',array(LANGUAGE,$bids[$p]))?>"><?=$bnames[$p]?></a></div>
                          <div class="text-center py-lg-4"><?=$bages[$p]?> éves</div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <?php $p++;}$p=0; /* Show all birthdays - End */ ?>
                </tbody>
              </table>
            <?php } ?>
          </div>
          <div class="card-footer text-center">
            <a href="birthday.php?l=<?=LANGUAGE?>" class="desc"><?=MORE_RESULTS?></a>
          </div>
      </div>
    </div>
      <?php endif;?>

  </div>
</div>
