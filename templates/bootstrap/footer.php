<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
<div class="container-fluid mt-3">
  <div class="row">
    <div class="col-md-8 offset-md-2">
      <div class="card mb-1">
          <div class="card-header text-center">
            <a href="<?=C_URL?>/online.php?l=<?=LANGUAGE?>"><?=USERS_ONLINE?></a> <?=online_users()?>, <?=GUESTS_ONLINE?> <?=online_quests()?>
          </div>
          <div class="card-body text-center">
            <div class="text-center">
              <A HREF="<?=FEEDBACK_LINK?>"><?=FEEDBACK?></A> 
              <!--A HREF="<?=TESTIMONIALS_LINK?>"><?=TESTIMONIALS?></A--> | 
              <A HREF="<?=TERMS_OF_USE_LINK?>"><?=TERMS_OF_USE?></A> | 
              <A HREF="<?=PRIVACY_STATEMENT_LINK?>"><?=PRIVACY_STATEMENT?></A>
            </div>
              <div>
                <small>This site is protected by reCAPTCHA and the Google 
                    <a href="https://policies.google.com/privacy">Privacy Policy</a> and
                    <a href="https://policies.google.com/terms">Terms of Service</a> apply.
                </small>
              </div>
              <p><a href="<?=C_URL?>/old">Visszatérés a régi felületre</a></p>
          </div>
          <!--div class="card-footer text-center">
          <?php 
            /*
            # You Can`t to remove our copyright link! Read license!
            #
            # Copyright link removing is possible only if purchased from:
            # http://www.shareit.com/product.html?productid=211505&languageid=1
            #
            # Thank you for your understanding!
            */
            echo COPYRIGHTS;
          ?>
          </div-->
      </div>
    </div>
  </div>
</div>

</div>
	
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-51360727-3', 'hotbull.hu');
  ga('send', 'pageview');

</script>

<script src="<?=C_URL?>/templates/<?=C_TEMP?>/swfobject-2.2.min.js"></script>
<script src="<?=C_URL?>/templates/<?=C_TEMP?>/evercookie.js"></script>
<script src="<?=C_URL?>/templates/<?=C_TEMP?>/cookieconsent.min.js"></script>
<script src="<?=C_URL?>/templates/<?=C_TEMP?>/script.js"></script>
</body>
</html>
