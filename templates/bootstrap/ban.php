<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
<form action="ban.php" method="post" name=form>
    <input class=input type=hidden name="l" value="<?=LANGUAGE?>">
    <input class=input type=hidden name="do" value="1">
    <input class=input type=hidden name="id" value="<?=ID_VALUE?>">

    <div class="container-fluid my-1 text-center">
        <strong><?=ADD_TO_LIST?> <?=USER_NAME?></strong>
    </div>

    <div class="container-fluid my-1">
        <div class="row">
            <div class="col-12 col-lg-6 offset-lg-3">
                <div class="card mb-1">
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-12 col-md-4" for="id"><?=ENTER_YOUR_REASON?></label>
                            <div class="col-12 col-md-8">
                                <textarea class="form-control" name="reason" rows=3></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid my-1 text-center">
        <button class="btn btn-primary" type="submit" name="submit" value="<?=SEND_MESSAGE?>"><?=SEND_MESSAGE?></button>
    </div>
</form>
