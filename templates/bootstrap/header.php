<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 TRANSITIONAL//EN">
<html dir=<?=C_HTML_DIR?>>
<head>
<title><?=C_TITLE?></title>
<link rel="stylesheet" type="text/css" href="<?=C_URL?>/templates/<?=C_TEMP?>/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?=C_URL?>/templates/<?=C_TEMP?>/style.css">
<link rel="stylesheet" type="text/css" href="<?=C_URL?>/templates/<?=C_TEMP?>/cookieconsent.min.css">
<META name="robots" content="noimageindex, noarchive">
<META HTTP-EQUIV="Expires" Content="0">
<meta name="rating" content="mature">
<meta http-equiv=Content-Type content="text/html; charset=<?=C_CHARSET?>">
<meta name=Copyright content="<?=C_SNAME?>">
<meta name=Author content="AzDG.com">
<META name=description content="<?=C_DESCRIPTION?>">
<META name=keywords content="<?=C_KEYWORDS?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- To support old sizes -->
<link rel="apple-touch-icon" sizes="57x57" href="<?=C_URL?>/templates/<?=C_TEMP?>/img/hotbulllogo_57.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?=C_URL?>/templates/<?=C_TEMP?>/img/hotbulllogo_72.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?=C_URL?>/templates/<?=C_TEMP?>/img/hotbulllogo_114.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?=C_URL?>/templates/<?=C_TEMP?>/img/hotbulllogo_144.png">

<!– To support new sizes –>
<link rel=”apple-touch-icon” sizes=”60×60″ href=”<?=C_URL?>/templates/<?=C_TEMP?>/img/hotbulllogo_60.png”>
<link rel=”apple-touch-icon” sizes=”76×76″ href=”<?=C_URL?>/templates/<?=C_TEMP?>/img/hotbulllogo_76.png”>
<link rel=”apple-touch-icon” sizes=”120×120″ href=”<?=C_URL?>/templates/<?=C_TEMP?>/img/hotbulllogo_120.png”>
<link rel=”apple-touch-icon” sizes=”152×152″ href=”<?=C_URL?>/templates/<?=C_TEMP?>/img/hotbulllogo_152.png”>
<link rel=”apple-touch-icon” sizes=”180×180″ href=”<?=C_URL?>/templates/<?=C_TEMP?>/img/hotbulllogo_180.png”>

<!– To support Android –>
<link rel=”icon” sizes=”192×192″ href=”<?=C_URL?>/templates/<?=C_TEMP?>/img/hotbulllogo_192.png”>
<link rel=”icon” sizes=”128×128″ href=”<?=C_URL?>/templates/<?=C_TEMP?>/img/hotbulllogo_128.png”>

<!-- Global site tag (gtag.js) - Google AdWords: 1029183088 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-762404243"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'AW-762404243');
</script>
<!--script type="text/javascript" src="<?=C_URL?>/templates/<?=C_TEMP?>/copy-disabler.js"></script-->
<script type="text/javascript" src="<?=C_URL?>/templates/<?=C_TEMP?>/jquery-3.4.1.min.js"></script>

<!--link rel="stylesheet" href="<?=C_URL?>/templates/<?=C_TEMP?>/iframe-modal.css"-->
<script type="text/javascript" src="<?=C_URL?>/templates/<?=C_TEMP?>/modalWindow.js"></script>
<script type="text/javascript" src="<?=C_URL?>/templates/<?=C_TEMP?>/initialize.js"></script>

<script src="https://www.google.com/recaptcha/api.js?render=<?=RECAPTCHA_PUBLIC_KEY?>"></script>
<script src="https://www.google.com/recaptcha/api.js?render=explicit" async defer></script>
<script type="text/javascript" src="<?=C_URL?>/templates/<?=C_TEMP?>/bootstrap.bundle.min.js"></script>
<?php 
	$c = get_defined_constants();
	unset($c['C_ADMINL']);
    unset($c['C_ADMINP']);
    unset($c['C_ADMINM']);
    unset($c['C_ONESIGNAL_REST_API_KEY']);
    unset($c['RECAPTCHA_SECRET_KEY']);
    unset($c['OLD_RECAPTCHA_SECRET_KEY']);
    unset($c['C_HOST']);
    unset($c['C_USER']);
    unset($c['C_PASS']);
    unset($c['C_BASE']);
	$c = json_encode(unserialize(str_replace(array('NAN;','INF;'),'0;',serialize($c))));
?>
<script>
<!-- 
window.W = function(key){
	var w = <?=json_encode($w)?>;
	return w[key];
}

window.CONST = function(key){
    var c = <?=$c?>;
    return c[key];
}

window.MyID = '<?=MyID()?>';
window.C_URL = '<?=C_URL?>';
window.C_TEMP = '<?=C_TEMP?>';
window.access_newmsg = <?=((IsAccess(C_AJAX_MSGINF) && C_HAVE_MESSAGE && MyID() > 0)?('true'):('false'))?>;
window.imsound = <?=((get_info('imsound'))?('true'):('false'))?>;

-->
</script>

<script language="JavaScript" src="<?=C_URL?>/classes/Ajax.js"></script>
<link rel="manifest" href="/manifest.json" />
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
<script src="<?=C_URL?>/templates/<?=C_TEMP?>/sjcl.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>
<body bgcolor=#BDD3F4 leftmargin=0 topmargin=0>
<noscript>
<style>
	.attention{
		color: black;
		width: 100%;
		text-align: center;
		padding: 5px 0;
		font-size: 20px;
	}
	.wrapper{
		display: none;
	}
</style>
<div style="text-align: center">
	<img src="https://hotbull.hu/bannerrandi.png">
</div>
<div class="attention">Úgy látjuk, javascript szűrőt használsz. <br/>
Az oldal használatához szükséged lesz rá! <br/>
Engedélyezd a böngésződben! <br/>
Köszönjük.</div>
</noscript>

<div class="window-modal js-modal-window js-window-overlay">
    <div class="window-modal__in js-modal-window-in"></div>
</div>
<style>
    .window-modal {
        display: none;
        align-items: center;
        justify-content: center;
        flex-wrap: wrap;
        overflow-y: auto;
        position: fixed;
        padding: 15px;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        z-index: 1000;
        background-color: rgba(0,0,0,0.7);
    }

    .window-modal__in {
        position: relative;
        max-width: 100%;
        padding: 1px;
        z-index: 1001;
        background-color: #fff;
        border-radius: 3px;
        box-shadow: 0 0 6px 2px rgba(50,50,50,0.3);
    }

    .window-activated {
        overflow: hidden;
        margin-right: 17px;
    }
    
    .grecaptcha-badge { 
        visibility: hidden;
    }
</style>

<div id="sidebarButton">
	<button id="sidebarCollapse" class="btn btn-light" type="button">
		<i class="fas fa-bars"></i>
	</button>
</div>

<div class="wrapper">
	<div id="sidebar">
		<div id="sidebar_in">
		    <div class="container-fluid my-2">
                <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                  <a href="<?=C_URL?>/old">Vissza a régi felületre</a>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
            </div>
			<?php if(is_numeric(get_info('m'))) :/* If User is Registered - Begin */?>
			<div class="nav flex-column nav-pills mt-10" id="v-pills-tab" role="tablist" aria-orientation="vertical">

			    <a class="nav-link" href="<?=MAIN_PAGE_LINK?>"><?=MAIN_PAGE?></a>

				<div class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
				<a class="nav-link" data-toggle="collapse" href="#menu_messages" role="button" aria-expanded="false" aria-controls="menu_messages">
				<?=VIEW_MESSAGES?> <i class="fas fa-caret-down"></i>
				</a>
				</div>
				
				<div class="collapse" id="menu_messages">
				<div class="nav flex-column nav-pills ml-3" role="tablist" aria-orientation="vertical">
					<a class="nav-link" href="<?=VIEW_INBOX_LINK?>"><?=VIEW_INBOX?></a>
					<a class="nav-link" href="<?=VIEW_OUTBOX_LINK?>"><?=VIEW_OUTBOX?></a>
				</div>
				</div>
                            
                                <?php if(IsAccess(C_WHO_SEEN_OWN)){?><a class="nav-link" href="<?=WHO_SEEN_MY_PROFILE_LINK?>"><?=WHO_SEEN_MY_PROFILE?> [<?=VisitFromLastLogin($_SESSION['lastlogin'])?>]</a><?php }?>
                                <a class="nav-link" href="<?=MY_FAVORITES_LINK?>"><?=MY_FAVORITES?></a>

				<div class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
				<a class="nav-link" data-toggle="collapse" href="#menu_mydata" role="button" aria-expanded="false" aria-controls="menu_mydata">
				<?=MY_DATA?> <i class="fas fa-caret-down"></i>
				</a>
				</div>
				
				<div class="collapse" id="menu_mydata">
				<div class="nav flex-column nav-pills ml-3" role="tablist" aria-orientation="vertical">
					<a class="nav-link" href="<?=MY_PROFILE_LINK?>"><?=MY_PROFILE?></a>
					<a class="nav-link" href="<?=CHANGE_PROFILE_LINK?>"><?=CHANGE_PROFILE?></a>
					<a class="nav-link" href="<?=CHANGE_PASSWORD_LINK?>"><?=CHANGE_PASSWORD?></a>
					<?php if(IsAccess(C_OPTIONS)) {?><a class="nav-link" href="<?=OPTIONS_LINK?>"><?=OPTIONS?></a><?php }?>
					<a class="nav-link" href="<?=BANLIST_LINK?>"><?=BANLIST?></a>
					<?php if(C_REMOVE_ALLOW){?><a class="nav-link" href="<?=REMOVE_PROFILE_LINK?>"><?=REMOVE_PROFILE?></a><?php }?>
				</div>
				</div>

				<div class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
				<a class="nav-link" data-toggle="collapse" href="#menu_search" role="button" aria-expanded="false" aria-controls="menu_search">
				<?=SEARCH?> <i class="fas fa-caret-down"></i>
				</a>
				</div>

				<div class="collapse" id="menu_search">
				<div class="nav flex-column nav-pills ml-3" role="tablist" aria-orientation="vertical">
					<a class="nav-link text-warning" href="<?=ONLINE_LINK?>">Online</a>
					<a class="nav-link" href="<?=SEARCH_LINK?>"><?=SEARCH?></a>
					<a class="nav-link" href="<?=VISITED_PROFILES_LINK?>"><?=VISITED_PROFILES?></a>
					<a class="nav-link" href="<?=ADDRESS_BOOK_LINK?>"><?=ADDRESS_BOOK?></a>
					<a class="nav-link" href="<?=ALL_USERS_LINK?>"><?=ALL_USERS?></a>
					<a class="nav-link" href="<?=STATISTICS_LINK?>"><?=STATISTICS?></a>
					<!--a class="nav-link" href="<?=MATCHMAKER_LINK?>"><?=MATCHMAKER?></a-->
				</div>
				</div>

					<a class="nav-link" href="<?=EXIT_LINK?>"><?=EXIT_FROM?></a>
				</div>
				<?php else: /* If User is Registered - End */?>
            <form action="<?=C_URL?>/login.php" method="post" name="login">
				<input type="hidden" name="l" value="<?=LANGUAGE?>">
				<input type="hidden" name="p" value="s">
                <input type="hidden" name="browser" value="">
				<div class="form-group text-center">
				<?=LOGIN?>
				</div>
				<div class="form-group">
				<input class="form-control" name="id" type="search" placeholder="Név">
				</div>
				<div class="form-group">
				<input class="form-control" name="password" type="password" placeholder="Jelszó">
				</div>
				<div class="form-group text-center">
				<button class="btn btn-primary" type="submit" value="OK">OK</button>
				</div>
			</form>
            <script language="javascript">
                document.login.browser.value = navigator.platform + ' ' + navigator.userAgent + ' ' + Intl.DateTimeFormat().resolvedOptions().timeZone;
            </script>

			<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
				<a class="nav-link" href="<?=MAIN_PAGE_LINK?>"><?=MAIN_PAGE?></a>
				<a class="nav-link" href="<?=REGISTER_LINK?>"><?=REGISTER?></a>
				<a class="nav-link" href="<?=FORGOT_PASS_LINK?>"><?=FORGOT_PASS?></a>
				<!--a class="nav-link" href="<?=SEARCH_LINK?>"><?=SEARCH?></a-->
				<!--a class="nav-link" href="<?=STATISTICS_LINK?>"><?=STATISTICS?></a-->
			</div>
			<?php endif; /* If User is Registered - End */?>
			<div id="right_b1" class="text-center">
				<!--/* OpenX Javascript Tag v2.8.1 */-->
				<p>
				<script type='text/javascript'><!--//<![CDATA[
					var m3_u = (location.protocol=='https:'?'https://ads.gaypoint.hu/www/delivery/ajs.php':'http://ads.gaypoint.hu/www/delivery/ajs.php');
					var m3_r = Math.floor(Math.random()*99999999999);
					if (!document.MAX_used) document.MAX_used = ',';
					document.write ("<scr"+"ipt type='text/javascript' src='"+m3_u);
					document.write ("?zoneid=21");
					document.write ('&amp;cb=' + m3_r);
					if (document.MAX_used != ',') document.write ("&amp;exclude=" + document.MAX_used);
					document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
					document.write ("&amp;loc=" + escape(window.location));
					if (document.referrer) document.write ("&amp;referer=" + escape(document.referrer));
					if (document.context) document.write ("&context=" + escape(document.context));
					if (document.mmm_fo) document.write ("&amp;mmm_fo=1");
					document.write ("'><\/scr"+"ipt>");
				//]]>--></script><noscript><a href='http://ads.gaypoint.hu/www/delivery/ck.php?n=ab2e97ef&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://ads.gaypoint.hu/www/delivery/avw.php?zoneid=21&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=ab2e97ef' border='0' alt='' /></a></noscript>
				</p>
				<p>	
					<!--/* OpenX Javascript Tag v2.8.1 */-->

				<script type='text/javascript'><!--//<![CDATA[
					var m3_u = (location.protocol=='https:'?'https://ads.gaypoint.hu/www/delivery/ajs.php':'http://ads.gaypoint.hu/www/delivery/ajs.php');
					var m3_r = Math.floor(Math.random()*99999999999);
					if (!document.MAX_used) document.MAX_used = ',';
					document.write ("<scr"+"ipt type='text/javascript' src='"+m3_u);
					document.write ("?zoneid=29");
					document.write ('&amp;cb=' + m3_r);
					if (document.MAX_used != ',') document.write ("&amp;exclude=" + document.MAX_used);
					document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
					document.write ("&amp;loc=" + escape(window.location));
					if (document.referrer) document.write ("&amp;referer=" + escape(document.referrer));
					if (document.context) document.write ("&context=" + escape(document.context));
					if (document.mmm_fo) document.write ("&amp;mmm_fo=1");
					document.write ("'><\/scr"+"ipt>");
				//]]>--></script><noscript><a href='http://ads.gaypoint.hu/www/delivery/ck.php?n=af9dfb79&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://ads.gaypoint.hu/www/delivery/avw.php?zoneid=29&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=af9dfb79' border='0' alt='' /></a></noscript>

				</p>
			</div>  
		</div>
    </div>

	<div id="content">
		<nav class="navbar navbar-light" style="background-color: #73AED2;">
			<div class="col-md-2 offset-md-1 order-md-1 col-6 order-1 text-center">
				<a href="https://hotbull.hu/randi"><img class="w-100" alt="Hotbull Randi" src="https://hotbull.hu/bannerrandi.png"></a>
			</div>
	
			<div id="banner_top" class="col-md-6 order-md-6 order-12 col-12 mt-1 mb-1 pl-0 pr-0 text-center">
				<script type='text/javascript'><!--//<![CDATA[
					var m3_u = (location.protocol=='https:'?'https://ads.gaypoint.hu/www/delivery/ajs.php':'http://ads.gaypoint.hu/www/delivery/ajs.php');
					var m3_r = Math.floor(Math.random()*99999999999);
					if (!document.MAX_used) document.MAX_used = ',';
					document.write ("<scr"+"ipt type='text/javascript' src='"+m3_u);
					document.write ("?zoneid=20");
					document.write ('&amp;coupon=<?php echo Get_MY_Coupon(); ?>');
					document.write ('&amp;cb=' + m3_r);
					if (document.MAX_used != ',') document.write ("&amp;exclude=" + document.MAX_used);
					document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
					document.write ("&amp;loc=" + escape(window.location));
					if (document.referrer) document.write ("&amp;referer=" + escape(document.referrer));
					if (document.context) document.write ("&context=" + escape(document.context));
					if (document.mmm_fo) document.write ("&amp;mmm_fo=1");
					document.write ("'><\/scr"+"ipt>");
				//]]>--></script>
			</div>
			<div class="col-md-2 order-md-12 col-6 order-6 text-center">
				<a href="https://hotbull.hu/portal"><img class="w-100" alt="Hotbull Randi" src="https://hotbull.hu/bannerportal.png"></a>
			</div>
		</nav>

<?php if(is_numeric(get_info('m'))) {/* If User is Registered - Begin */?>
<?php 
	$show_term = false;
	if(!isset($_SESSION['approved_term'])){
		$show_term = true;
	} else if($_SESSION['approved_term'] < C_TERM_RULES){
		$show_term = true;
	}
?>
<?php if($show_term):?>
<!--div class="container text-center" id="term_attention">
	<p>Még nem fogadtad el az aktuális <a href="<?=C_URL?>/terms.php?l=hu" target="_blank">Felhasználói Feltételeinket</a> és <a href="<?=C_URL?>/privacy.php?l=hu" target="_blank">Adatvédelmi Tájékoztatónkat</a>.<br/>
		Ahhoz, hogy használd az oldalunkat, el kell olvasnod és el kell fogadnod mindkettőt.</p>
	<p><a href="<?=OPTIONS_LINK?>&term=<?=C_TERM_RULES?>" class="approve_button">Megismertem és elfogadom</a></p>
</div-->
<?php endif;?>

<div class="container-fluid text-center">
    <span id="newalert" class="badge badge-light" role="alert" <?=(NEW_ICON?' style="display:inline-block; padding: 5px; margin-bottom: 10px;"':' style="display:none; padding: 5px; margin-bottom: 10px;"')?>><span id=newicon><?=(NEW_ICON?'<i class="far fa-envelope"></i> ':'')?></span><span id=num></span><a href="<?=VIEW_INBOX_LINK?>"><span id=newmsg><?=YOU_HAVE_NEW_MESSAGE?></span></a></span>
</div>
<?php } ?>

<?php if(isset($_SESSION['alert_mes'])){
    echo $_SESSION['alert_mes'];
    unset($_SESSION['alert_mes']);
}?>

<?php if(defined("PROMPT_MESSAGES")):?>
    <div class="container-fluid mt-3">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="card mb-1">
                    <div class="card-body text-center">
                        <div class='onesignal-customlink-container'></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .onesignal-customlink-subscribe{
            text-transform: none !important;
            display: inline-block;
            font-weight: 400 !important;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .375rem .75rem;
            font-size: 1rem;
            line-height: 1.5;
            border-radius: .25rem;
            transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            box-shadow: 1px 1px 1px transparent !important;
        }
    </style>
<?php endif;?>
