<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
<script language="JavaScript">
<!--
// Select Multiple js-function: hld()
function hld(nid){ln=nid.parentNode;if(nid.checked){ln.style.backgroundColor='#0a246a';ln.style.color='#fff';}else{ln.style.backgroundColor='#fff';ln.style.color='#000';}}

function formCheck(form) {
<?php if (USERNAME_ALLOW) {?>
if (form.username.value == "") {
    alert("<?=USERNAME_ERROR?>");
    return false;
}
<?php }?>
<?php if (ORIENTATION_ALLOW) {?>
if (form.orientation.selectedIndex=="") {
    alert("<?=ORIENTATION_ERROR?>");
    return false;
}
<?php }?>
<?php if (FIRSTNAME_ALLOW) {?>
if (form.fname.value == "") {
    alert("<?=FIRSTNAME_ERROR?>");
    return false;
}
<?php }?>

<?php if (LASTNAME_ALLOW) {?>
if (form.lname.value == "") {
    alert("<?=LASTNAME_ERROR?>");
    return false;
}
<?php }?>

<?php if (C_CHANGE_EMAIL) { ?>
if (form.email.value == "") {
    alert("<?=EMAIL_ERROR?>");
    return false;
}
<?php }?>

<?php if (BODYTYPE_ALLOW) {?>
if (form.bodytype.selectedIndex=="") {
    alert("<?=BODYTYPE_ERROR?>");
    return false;
}
<?php }?>

<?php if (INTERESTS_ALLOW) {?>
if (form.interests.selectedIndex=="") {
    alert("<?=INTERESTS_ERROR?>");
    return false;
}
<?php }?>

<?php if (ZIP_ALLOW) {?>
if (form.zip.value=="") {
    alert("<?=ZIP_ERROR?>");
    return false;
}
<?php }?>

<?php if (PHONE_ALLOW) {?>
if (form.phone.value == "") {
    alert("<?=PHONE_ERROR?>");
    return false;
}
<?php }?>

<?php if (CITY_ALLOW) {?>
if (form.city.value == "") {
    alert("<?=CITY_ERROR?>");
    return false;
}
<?php }?>

<?php if (STATE_ALLOW) {?>
if (form.state.value == "") {
    alert("<?=STATE_ERROR?>");
    return false;
}
<?php }?>

<?php if (ADDRESS_ALLOW) {?>
if (form.address.value == "") {
    alert("<?=ADDRESS_ERROR?>");
    return false;
}
<?php }?>

<?php if (JOB_ALLOW) {?>
if (form.job.value=="") {
    alert("<?=JOB_ERROR?>");
    return false;
}
<?php }?>

<?php if (HOBBY_ALLOW) {?>
if (form.hobby.value=="") {
    alert("<?=HOBBY_ERROR?>");
    return false;
}
<?php }?>

if (form.descr.value=="") {
    alert("<?=DESCRIPTION_ERROR?>");
    return false;
}

if (document.form.submit.action != "") {
document.form.submit.disabled=1;}
}

function ins(prop,type)
{
	var input = document.form.descr;var mysel;var text;
	if (window.getSelection) mysel = window.getSelection();
	else if (document.selection) mysel = document.selection.createRange().text;
	else if (document.getSelection)	mysel = document.getSelection();
	b = (type == 0) ? '['+prop+']' : prop;
	e = (type == 0) ? '[/'+prop+']' : '';
	if (input.createTextRange)
	{
		var text;
		input.focus(input.caretPos);
		input.caretPos = document.selection.createRange().duplicate();
		input.caretPos.text = (input.caretPos.text.length>0) ? b + input.caretPos.text + e : b + mysel + e;
	}
	else input.value += b + mysel + e;
}
// -->
</script>

<form action="index.php" method="post" enctype="multipart/form-data" name=form OnSubmit="return formCheck(this)">
<input type="hidden" name="l" value="<?=LANGUAGE?>">
<input type="hidden" name="a" value="u">
  <div class="container-fluid my-1 text-center">
    <h2><?=CHANGE_PROFILE?></h2>
  </div>
  <div class="container-fluid my-1">
    <div class="row">
      
      <div class="col-12 col-lg-8 offset-lg-2">
        <div class="card mb-1">
            <div class="card-body">

              <div class="form-group row">
                <label class="col-12 col-md-2" for="username"><?=USERNAME?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="text" id="username" name="username" maxlength="16" value="<?=USERNAME_VALUE?>" OnChange="SR(true)"<?php if(!USERNAME_ALLOW){?> disabled="disabled"<?php }?>>
                </div>                
              </div>
                  
              <div class="form-group row">
                <label class="col-12 col-md-2" for="email"><?=EMAIL?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="email" name="email" value="<?=EMAIL_VALUE?>">
                </div>                
              </div>

              <div class="form-group row">
                <label class="col-12 col-md-2" for="descr"><?=DESCRIPTION?></label>
                <div class="col-12 col-md-10">
                  <?php if(USE_SMILES || USE_BB){?>
                  <Table Border=0 width="100%" CellSpacing="2" CellPadding="6"><Tr align="left" bgcolor="<?=COLORH?>"><Td>
                  <?php if(USE_SMILES){?><?php $p=0;foreach ($smiles as $k=>$v){$p++;?><a href="javascript:ins(' <?=$k?> ',1)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/forum/smiles/<?=$v?>" border=0 /></a>
                  <?php if(C_SMILES_NUMBER <= $p){?><br><a href="javascript:open_win('<?=C_URL?>/forum/smiles_show.php','_blank')"><?=$w[720]?></a><br><?php break;}?><?php }?><?php }?> 
                  <?php if(USE_BB && USE_SMILES){?><br><br><?php }?>
                  <?php if(USE_BOLD){?><a href="javascript:ins('B',0)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Bold.gif" border=0 /></a><?php }?>
                  <?php if(USE_ITALIC){?><a href="javascript:ins('I',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Italic.gif" border=0 /></a><?php }?>
                  <?php if(USE_UNDERLINE){?><a href="javascript:ins('U',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Underline.gif" border=0 /></a><?php }?>
                  <?php if(USE_STRIKE){?><a href="javascript:ins('S',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Strike.gif" border=0 /></a><?php }?>
                  <?php if(USE_QUOTE){?><a href="javascript:ins('QUOTE',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Quote.gif" border=0 /></a><?php }?>  
                  <?php if(USE_URL){?><a href="javascript:ins('URL',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/URL.gif" border=0 /></a><?php }?>  
                  <?php if(USE_IMG){?><a href="javascript:ins('IMG',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Image.gif" border=0 /></a><?php }?>  
                  <?php if(USE_VIDEO){?><a href="javascript:ins('VIDEO',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Video.gif" border=0 /></a><?php }?>  
                  </td></tr></table><?php }?>

                  <textarea class="form-control" rows="6" name="descr"><?=DESCRIPTION_VALUE?></textarea>
                </div>
              </div>

              <?php if(FIRSTNAME_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="fname"><?=FIRSTNAME?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="text" name="fname" maxlength="<?=C_FIRSTNB?>" value="<?=FIRSTNAME_VALUE?>">
                </div>
              </div>
              <?php }?>

              <?php if(LASTNAME_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="lname"><?=LASTNAME?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="text" name="lname" maxlength="<?=C_LASTNB?>" value="<?=LASTNAME_VALUE?>">
                </div>
              </div>
              <?php }?>

              <?php if(BIRTHDAY_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="year"><?=BIRTHDAY?></label>
                <div class="col-12 col-md-10">
                  <div class="row">
                    <div class="col-4">
                      <input class="form-control" type="text" name="year" value="<?=BD_YEAR?>" readonly="readonly">
                    </div>
                    <div class="col-5">
                      <input class="form-control" type="text" name="month" value="<?=BD_MONTH?>" readonly="readonly">
                    </div>
                    <div class="col-3">
                      <input class="form-control" type="text" name="day" value="<?=BD_DAY?>" readonly="readonly">
                    </div>
                  </div>
                </div>                    
              </div>
              <?php }?>

              <?php if(COUNTRY_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="country"><?=COUNTRY?></label>
                <div class="col-12 col-md-10">
                  <select name="country" class="form-control"<?php if(STATE_SHOW) {?> onChange="ShowRegion(this.options[this.selectedIndex].value)"<?php }?>>
                    <?=COUNTRY_VALUE?>
                  </select>
                </div>
              </div>
              <?php }?>

              <?php if(STATE_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="state"><?=STATE?></label>
                <div class="col-12 col-md-10">
                  <div id='state'>
                    <input class="form-control" type="text" name="state" value="<?=STATE_VALUE?>">
                  </div>
                  <div id='statelist' style="display:none;">
                      <select name="statelist" class="form-control">
                      </select>
                  </div>
                </div>
              </div>
              <script language="JavaScript">
              var ids=new Array('state','statelist');
              function switchid(id){hideallids();	showdiv(id);}
              function hideallids(){for (var i=0;i<ids.length;i++){hidediv(ids[i]);}}
              function hidediv(id) {if (document.getElementById) {document.getElementById(id).style.display = 'none';}
                else {if (document.layers) {document.id.display = 'none';}
                  else {document.all.id.style.display = 'none';}}}
              function showdiv(id) {	  
                if (document.getElementById) {document.getElementById(id).style.display = 'block';}
                else {if (document.layers) {document.id.display = 'block';}
                  else {document.all.id.style.display = 'block';}}}
              var rc= new Array(500);
              <?=STATELIST_VALUE?>
              function GetIndex(index){var sc = rc[index];return sc.split(","); }
              function ShowRegion(index){
              if(<?=STATELIST_CHECK?>) {
              switchid('statelist');
              document.forms["form"].elements["state"].value = '';
              var ind = GetIndex(index);var cur = '<?=STATELIST_CUR?>';var cnt = ind.length;var lst = document.forms["form"].elements["statelist"];var lstOptionsCnt = lst.options.length;lst.length = 0; 
                  lst.options[0] = new Option('--------', '', false, false);
                for (i = 1; i < cnt; i++){
                      lst.options[i] = (cur == ind[i]) ? new Option(ind[i], ind[i], true, false) : new Option(ind[i], ind[i], false, false);
                  }
              } else {
              switchid('state');
              for (var i = 0; i < document.forms["form"].elements["statelist"].length; i++) {document.forms["form"].elements["statelist"].selected = false;}
              document.forms["form"].elements["statelist"].value = '';
              }
              }
              ShowRegion(document.forms["form"].elements["country"].options[document.forms["form"].elements["country"].options.selectedIndex].value);
              // -->
              </script>
              <?php }?>

              <?php if(ZIP_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="zip"><?=ZIP?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="text" name="zip" value="<?=ZIP_VALUE?>">
                </div>
              </div>
              <?php }?>

              <?php if(CITY_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="city"><?=CITY?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="text" name="city" value="<?=CITY_VALUE?>">
                </div>
              </div>
              <?php }?>

              <?php if(ORIENTATION_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="orientation"><?=ORIENTATION?></label>
                <div class="col-12 col-md-10">
                  <select name="orientation" class="form-control">
                    <option value=0> --------- 
                    <?=ORIENTATION_VALUE?>
                  </select>
                </div>
              </div>
              <?php }?>

              <?php if(MARSTAT_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="marstat"><?=MARITAL_STATUS?></label>
                <div class="col-12 col-md-10">
                  <select name="marstat" class="form-control">
                    <option value=0> --------- 
                    <?=MARITAL_STATUS_VALUE?>
                  </select>
                </div>
              </div>
              <?php }?>

              <?php if(CHILD_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="child"><?=CHILD?></label>
                <div class="col-12 col-md-10">
                  <select name="child" class="form-control">
                    <option value=0> --------- 
                    <?=CHILD_VALUE?>
                  </select>
                </div>
              </div>
              <?php }?>

              <!-- Not showed values -->

              <?php if(ADDRESS_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="address"><?=ADDRESS?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="text" name="address" value="<?=ADDRESS_VALUE?>">
                </div>
              </div>
              <?php }?>

              <?php if(URL_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="url"><?=URL?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="text" name="url" value="<?=URL_VALUE?>">
                </div>
              </div>
              <?php }?>

              <?php if(ICQ_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="icq"><?=ICQ?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="text" name="icq" value="<?=ICQ_VALUE?>">
                </div>
              </div>
              <?php }?>

              <?php if(AIM_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="aim"><?=AIM?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="text" name="aim" value="<?=AIM_VALUE?>">
                </div>
              </div>
              <?php }?>

              <?php if(PHONE_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="phone"><?=PHONE?></label>
                <div class="col-12 col-md-10">  
                  <input class="form-control" type="text" name="phone" value="<?=PHONE_VALUE?>">
                </div>
              </div>
              <?php }?>

              <?php if(LANG_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="phone"><?=LANG?></label>
                <div class="col-12 col-md-10">
                  <div class="form-check">
                    <?php $k = 1;while (isset($wlng[$k])) {?>
                        <label><?=$checklang[$k]?> <?=$wlng[$k]?></label>
                    <?php $k++;}?>
                  </div>
                </div>
              </div>
              <?php }?>

              <?php if(JOB_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="job"><?=JOB?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="text" name="job" value="<?=JOB_VALUE?>">
                </div>
              </div>
              <?php }?>

              <?php if(HOBBY_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="hobby"><?=HOBBY?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="text" name="hobby" value="<?=HOBBY_VALUE?>">
                </div>
              </div>
              <?php }?>

              <?php if(INTERESTS_SHOW) {?>
                <div class="form-group row">
                  <label class="col-12 col-md-2"><?=INTERESTS?></label>
                  <div class="col-12 col-md-10">
                    <div class="form-check">
                      <?=INTERESTS_VALUE?>
                    </div>
                  </div>
                </div>
              <?php }?>

              <?php if(PURPOSE_SHOW) {?>
                <div class="form-group row">
                  <label class="col-12 col-md-2"><?=PURPOSE?></label>
                  <div class="col-12 col-md-10">
                    <div class="form-check">
                      <?=PURPOSE_VALUE?>
                    </div>
                  </div>
                </div>
              <?php }?>

              <?php if(BODYTYPE_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="bodytype"><?=BODYTYPE?></label>
                <div class="col-12 col-md-10">
                  <select name="bodytype" class="form-control">
                    <option value=0> --------- 
                    <?=BODYTYPE_VALUE?>
                  </select>
                </div>
              </div>
              <?php }?>

              <?php if(HEIGHT_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="height"><?=HEIGHT?></label>
                <div class="col-12 col-md-10">
                  <select name="height" class="form-control">
                    <option value=0> --------- 
                    <?=HEIGHT_VALUE?>
                  </select>
                </div>
              </div>
              <?php }?>

              <?php if(WEIGHT_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="weight"><?=WEIGHT?></label>
                <div class="col-12 col-md-10">
                  <select name="weight" class="form-control">
                    <option value=0> --------- 
                    <?=WEIGHT_VALUE?>
                  </select>
                </div>
              </div>
              <?php }?>

              <?php if(HAIR_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="hcolor"><?=HAIR?></label>
                <div class="col-12 col-md-10">
                  <select name="hcolor" class="form-control">
                    <option value=0> --------- 
                    <?=HAIR_VALUE?>
                  </select>
                </div>
              </div>
              <?php }?>

              <?php if(EYE_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="ecolor"><?=EYE?></label>
                <div class="col-12 col-md-10">
                  <select name="ecolor" class="form-control">
                    <option value=0> --------- 
                    <?=EYE_VALUE?>
                  </select>
                </div>
              </div>
              <?php }?>

              <?php if(ETHNICITY_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="etnicity"><?=ETHNICITY?></label>
                <div class="col-12 col-md-10">
                  <select name="etnicity" class="form-control">
                    <option value=0> --------- 
                    <?=ETHNICITY_VALUE?>
                  </select>
                </div>
              </div>
              <?php }?>

              <?php if(RELIGION_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="religion"><?=RELIGION?></label>
                  <div class="col-12 col-md-10">
                      <div class="form-check">
                          <?=RELIGION_VALUE?>
                      </div>
                  </div>
              </div>
              <?php }?>

              <?php if(SMOKE_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="smoke"><?=SMOKE?></label>
                <div class="col-12 col-md-10">
                  <select name="smoke" class="form-control">
                    <option value=0> --------- 
                    <?=SMOKE_VALUE?>
                  </select>
                </div>
              </div>
              <?php }?>

              <?php if(DRINK_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="drink"><?=DRINK?></label>
                <div class="col-12 col-md-10">
                  <select name="drink" class="form-control">
                    <option value=0> --------- 
                    <?=DRINK_VALUE?>
                  </select>
                </div>
              </div>
              <?php }?>

              <?php if(EDUCATION_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="education"><?=EDUCATION?></label>
                <div class="col-12 col-md-10">
                  <select name="education" class="form-control">
                    <option value=0> --------- 
                    <?=EDUCATION_VALUE?>
                  </select>
                </div>
              </div>
              <?php }?>
              
            </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-12 col-lg-8 offset-lg-2">

        <?php if(PHOTO_SHOW) {?>
        <div class="card mb-1">
            <div class="card-body">
                <div class="alert alert-info">
                    <?=$w['875']?>
                </div>
              <div class="row">
                <?php $p=0;while(isset($PHOTO_NUM[$p])) {?>
                  <div class="col-12 col-lg-6">
                    <?php if($REMOVE_PHOTO[$p] > 0) { ?>
                      <div class="form-group">
                        <label for="delpic<?=$p?>">
                          <?=$PHOTO_NUM[$p]?> <?=DELETE_PHOTO?>: <input type="checkbox" name="delpic<?=$p?>">
                        </label>
                      </div>
                      <div class="text-center align-middle">
                        <a href="<?=$REMOVE_VALUE[$p]?>&bigger=1"><img class="w-50 img-thumbnail" src="<?=$REMOVE_VALUE[$p]?>" border="<?=C_IMG_BRDR?>" width=100></a>
                      </div>
                    <?php } elseif($REMOVE_PHOTO[$p] < 0) { ?>
                    <?php } else { ?>
                      <div class="form-group">
                      <?=$PHOTO_NUM[$p]?>
                      <input type="file" class="form-control-file" name="file<?=$p?>">
                      </div>
                    <?php } ?>
                  </div>
                <?php $p++;}$p=0;?>
              </div>
            </div>
        </div>
        <?php }?>

      </div>
      
    </div>

    <div class="row">
      <div class="col-12 col-lg-8 offset-lg-2">
        <div class="card mb-1">
            <div class="card-body">

              <?php if(SAGE_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="agef"><?=SEEKING_AGE?></label>
                <div class="col-12 col-md-10">
                  <div class="row">
                    <div class="col-6">
                      <select name="agef" class="form-control">
                        <?=SEEKING_AGE_FROM_VALUE?>
                      </select>
                    </div>
                    <div class="col-6">
                      <select name="aget" class="form-control">
                        <?=SEEKING_AGE_TO_VALUE?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <?php }?>

              <?php if(SCOUNTRY_SHOW) {?>
                <div class="form-group row">
                  <label class="col-12 col-md-2"><?=SEEKING_COUNTRY?></label>
                  <div class="col-12 col-md-10">
                    <div class="form-check">
                      <?=SEEKING_COUNTRY_VALUE?>
                    </div>
                  </div>
                </div>
              <?php }?>

              <?php if(SHEIGHT_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="heightf"><?=SEEKING_HEIGHT?></label>
                <div class="col-12 col-md-10">
                  <div class="row">
                    <div class="col-6">
                      <select name="heightf" class="form-control">
                        <?=SEEKING_HEIGHT_FROM_VALUE?>
                      </select>
                    </div>
                    <div class="col-6">
                      <select name="heightt" class="form-control">
                        <?=SEEKING_HEIGHT_TO_VALUE?>
                      </select>
                    </div>
                  </div> 
                </div>
              </div>
              <?php }?>

              <?php if(SWEIGHT_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="weightf"><?=SEEKING_WEIGHT?></label>
                <div class="col-12 col-md-10">
                  <div class="row">
                    <div class="col-6">
                      <select name="weightf" class="form-control">
                        <?=SEEKING_WEIGHT_FROM_VALUE?>
                      </select>
                    </div>
                    <div class="col-6">
                      <select name="weightt" class="form-control">
                        <?=SEEKING_WEIGHT_TO_VALUE?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <?php }?>

              <?php if(SHAIR_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2"><?=SEEKING_HAIR?></label>
                    <div class="col-12 col-md-10">
                      <div class="form-check">
                        <?=SEEKING_HAIR_VALUE?>
                      </div>
                    </div>
                  </div>
              <?php }?>

              <?php if(SEYE_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2"><?=SEEKING_EYE?></label>
                    <div class="col-12 col-md-10">
                      <div class="form-check">
                        <?=SEEKING_EYE_VALUE?>
                      </div>
                    </div>
                  </div>
              <?php }?>

              <?php if(SSMOKE_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2"><?=SEEKING_SMOKE?></label>
                    <div class="col-12 col-md-10">
                      <div class="form-check">
                        <?=SEEKING_SMOKE_VALUE?>
                      </div>
                    </div>
                  </div>
              <?php }?>

              <?php if(SDRINK_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2"><?=SEEKING_DRINK?></label>
                    <div class="col-12 col-md-10">
                      <div class="form-check">
                        <?=SEEKING_DRINK_VALUE?>
                      </div>
                    </div>
                  </div>
              <?php }?>

              <?php if(SMARSTAT_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2"><?=SEEKING_MARITAL_STATUS?></label>
                    <div class="col-12 col-md-10">
                      <div class="form-check">
                        <?=SEEKING_MARITAL_STATUS_VALUE?>
                      </div>
                    </div>
                  </div>
              <?php }?>

              <?php if(SCHILD_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2"><?=SEEKING_CHILD?></label>
                    <div class="col-12 col-md-10">
                      <div class="form-check">
                        <?=SEEKING_CHILD_VALUE?>
                      </div>
                    </div>
                  </div>
              <?php }?>

              <?php if(SGENDER_SHOW) {?>
                <div class="form-group row">
                  <label class="col-12 col-md-2"><?=SEEKING_GENDER?></label>
                  <div class="col-12 col-md-10">
                    <div class="form-check">
                      <?=SEEKING_GENDER_VALUE?>
                    </div>
                  </div>
                </div>
              <?php }?>

              <?php if(SEDUCATION_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2"><?=SEEKING_EDUCATION?></label>
                    <div class="col-12 col-md-10">
                      <div class="form-check">
                        <?=SEEKING_EDUCATION_VALUE?>
                      </div>
                    </div>
                  </div>
              <?php }?>

              <?php if(SETHNICITY_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2"><?=SEEKING_ETHNICITY?></label>
                    <div class="col-12 col-md-10">
                      <div class="form-check">
                        <?=SEEKING_ETHNICITY_VALUE?>
                      </div>
                    </div>
                  </div>
              <?php }?>

              <?php if(SRELIGION_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2"><?=SEEKING_RELIGION?></label>
                    <div class="col-12 col-md-10">
                      <div class="form-check">
                        <?=SEEKING_RELIGION_VALUE?>
                      </div>
                    </div>
                  </div>
              <?php }?>

            </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container-fluid my-1 text-center">
    <button class="btn btn-primary" type="submit" name="submit" value="<?=CHANGE_PROFILE?>"><?=CHANGE_PROFILE?></button>
  </div>
</form>