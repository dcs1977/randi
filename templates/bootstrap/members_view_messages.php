<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
<script language="JavaScript">
function sel() {
	with(document.form) {
		for(i=0;i<elements.length;i++) {
			thiselm = elements[i];
			if(thiselm.name.substring(0,4) == 'user')
				thiselm.checked = !thiselm.checked
		}
	}
}
</script>

<form action="index.php" method="post" name=form>
<input class=input type=hidden name="l" value="<?=LANGUAGE?>">
<input class=input type=hidden name="a" value="<?=(($a!='sm')?'d':'ds')?>">

<div class="container-fluid mt-1 md-1">
	<div class="row">
		<div class="col-12 col-md-8 offset-md-2">
			<div class="card mb-1">
        <div class="card-header">
					<div class="row">
						<div class="col-12 text-center">
              <?=TOTAL_MESSAGES?> (<?=TOTAL_MESSAGES_VALUE?>)
						</div>
					</div>					
				</div>
        <div class="card-body">
          <div class="row">
            <div class="col-12 text-right">
              <div class="btn-group-toggle" data-toggle="buttons" onClick="sel()">
                <label class="btn btn-light">
                  <input type="checkbox" autocomplete="off"> Mindet jelöl
                </label>
              </div>
            </div>
          </div>
          <div class="row mt-1 md-1">
            <div class="col-12">

                <table class="table table-striped">
                    <thead>
                        <tr class="d-xl-table-row d-lg-table-row d-md-table-row d-sm-table-row d-none">
                            <th>
                                <div class="row">
                                    <div class="col-4 order-6 order-sm-1 d-xl-block d-lg-block d-md-block d-sm-block d-none">
                                        <?=SUBJECT?>
                                    </div>
                                    <div class="col-4 order-12 order-sm-6 d-xl-block d-lg-block d-md-block d-sm-block d-none">
                                        <?=DATE?>
                                    </div>
                                    <div class="col-4 order-1 order-sm-12 d-xl-block d-lg-block d-md-block d-sm-block d-none">
                                        <?=FROM?>
                                    </div>
                                </div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php while(isset($vmids[$p])) { /* Show messages - Begin */ ?>
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col-12 text-right">
                                        <input type="checkbox" name="user[]" value="<?=$vmfromids[$p]?>">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 order-6 col-sm-4 order-sm-1" style="line-height: 28px">
                                        <a class=link2 href="?l=<?=LANGUAGE?>&a=m&mid=<?=$vmids[$p]?>" style="line-height: 28px">
                                            <?php
                                            $icon = '';
                                            $subject = '';
                                            if($vmsubjects[$p] == $w[862]){
                                                $icon = "<i class='fas fa-envelope'></i>";
                                                if ($a!='sm') {
                                                    $subject = $w[862];
                                                } else {
                                                    $subject = $w[871];
                                                }
                                            } elseif ($vmsubjects[$p] == $w[863]){
                                                $icon = "<i class='fas fa-image'></i>";
                                                if ($a!='sm') {
                                                    $subject = $w[863];
                                                } else {
                                                    $subject = $w[872];
                                                }
                                            } elseif ($vmsubjects[$p] == $w[236]){
                                                $icon = "<i class='fas fa-check'></i>";
                                                $subject = $w[236];
                                            } elseif ($vmsubjects[$p] == $w[400]){
                                                $icon = "<i class='fas fa-smile-wink'></i>";
                                                if ($a!='sm') {
                                                    $subject = $w[400];
                                                } else {
                                                    $subject = $w[873];
                                                }
                                            } elseif(!$vmfromids[$p]){
                                                $subject = $w[862];
                                            }
                                            ?>
                                            <span style="color: white; font-size: 26px;" data-toggle="tooltip" title="<?=$subject?>"><?=$icon?></span>
                                            <span<?=((!empty($vmreaded[$p]))?' style="font-weight: bold"':'')?>><?=$subject?></span>
                                        </a>
                                    </div>
                                    <div class="col-12 order-12 col-sm-4 order-sm-6" style="line-height: 28px">
                                        <span<?=((!empty($vmreaded[$p]))?' style="font-weight: bold"':'')?>><?=$vmsendtimes[$p]?></span>
                                    </div>
                                    <div class="col-12 order-1 col-sm-4 order-sm-12" style="line-height: 28px">
                                        <?php if(!$vmfromids[$p]) { /* If message from administrator */?>
                                            <B><?=ADMINISTRATOR?></B>
                                        <?php } else { /* Message from user*/ ?>
                                            <span<?=((!empty($vmreaded[$p]))?' style="font-weight: bold"':'')?>><a class=link2 href="<?=GenLink('UserProfile',array(LANGUAGE,$vmfromids[$p]))?>"><?=$vmnames[$p]?></a></span>
                                        <?php }?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php $p++;}$p=0; /* Show messages - End */ ?>
                    </tbody>
                </table>

            </div>
          </div>
        </div>
        <div class="card-footer">
          <div class="form-group text-center">
            <button type="submit" class="btn btn-primary"><?=DELETE_SELECTED_MESSAGES?></button>
          </div>
				</div>
      </div>
      <div class="text-center"><?=VMPAGES?></div>
    </div>
  </div>
</div>

</form>
