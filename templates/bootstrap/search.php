<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0"><TR><TD align=center VALIGN="TOP"><br><br>
<form action="<?=C_URL?>/search.php" method="post">
<input type="hidden" name="l" value="<?=LANGUAGE?>">
<input type="hidden" name="a" value="s">
<span class=head><?=SEARCH?></span>
<br><br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>">
<Tr>
  <Td>
  <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
<?php if(I_AM_A_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=I_AM_A?>
    </td>
    <td>
    <select name="sgender" class="input">
      <?=I_AM_A_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(SEEKING_FOR_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_FOR?>
    </td>
    <td>
    <select name="gender[]" class="inputm" multiple>
      <?=SEEKING_FOR_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(AGE_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=AGE?>
    </td>
    <td>
    <input name="agef" class="sinput"> -  <input name="aget" class="sinput">
    </td>
  </tr>
<?php }?>	
<?php if(COUNTRY_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=COUNTRY?>
    </td>
    <td>
    <select name="country[]" class="inputm" multiple>
      <?=COUNTRY_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(TYPE_OF_RELATION_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=TYPE_OF_RELATION?>
    </td>
    <td>
    <select name="purpose[]" class="inputm" multiple>
      <?=TYPE_OF_RELATION_VALUE?>
    </select>
    <BR>
		<span class=mes><input type=radio name=purpose_oper value=0 checked><?=WOR?> <input type=radio name=purpose_oper value=1><?=WAND?></span>
    </td>
  </tr>
<?php }?>	
<?php if(ORIENTATION_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=ORIENTATION?>
    </td>
    <td>
    <select name="orientation[]" class="inputm" multiple>
      <?=ORIENTATION_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(BODYTYPE_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=BODYTYPE?>
    </td>
    <td>
    <select name="bodytype[]" class="inputm" multiple>
      <?=BODYTYPE_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(HOROSCOPE_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=HOROSCOPE?>
    </td>
    <td>
    <select name="horo[]" class="inputm" multiple>
      <?=HOROSCOPE_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(USERID_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=USERID?>
    </td>
    <td>
    <input type="text" name="id" class="minput">
    </td>
  </tr>
<?php }?>	
<?php if(USERNAME_ALLOW && USERNAME_SHOW) {?>	
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=USERNAME?>
    </td>
    <td>
    <input type="text" name="usr" class="minput">
    </td>
  </tr>
<?php }?>	
<?php if(FIRSTNAME_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=FIRSTNAME?>
    </td>
    <td>
    <input class=input type=text name=fname maxlength="<?=C_FIRSTNB?>">
    </td>
  </tr>
<?php }?>	
<?php if(LASTNAME_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=LASTNAME?>
    </td>
    <td>
    <input class=input type=text name=lname maxlength="<?=C_LASTNB?>">
    </td>
  </tr>
<?php }?>	
<?php if(CITY_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=CITY?>
    </td>
    <td>
    <input class=minput type=text name=city maxlength="32">
    </td>
  </tr>	
<?php }?>	
<?php if(ZIP_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=ZIP?>
    </td>
    <td>
    <input class=minput type=text name=zip maxlength="16">
    </td>
  </tr>	
<?php }?>	
<?php if(DISTANCE_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=DISTANCE?>
    </td>
    <td><input class=sinput type=text name=distance maxlength="16"> <?=$w[722]?></td>
  </tr>	
<?php }?>	
<?php if(HEIGHT_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=HEIGHT?>
    </td>
    <td>
    <select name="heightf" class="minput"> 
      <?=HEIGHT_FROM?>
    </select>-<select name="heightt" class="minput">
      <?=HEIGHT_TO?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(WEIGHT_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=WEIGHT?>
    </td>
    <td>
    <select name="weightf" class="minput"> 
      <?=WEIGHT_FROM?>
    </select>-<select name="weightt" class="minput">
      <?=WEIGHT_TO?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(PHOTO_SHOW || ONLINE_USERS_ONLY_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEARCH_USERS_WITH?>
    </td>
    <td>
<?php if(PHOTO_SHOW) { /* If is allow to show this section */ ?>
    <?=PHOTO?> <input type="checkbox" name="photo"><br>
<?php }?>	
<?php if(ONLINE_USERS_ONLY_SHOW) { /* If is allow to show this section */ ?>
    <?=ONLINE_USERS_ONLY?> <input type="checkbox" name="online"> 
<?php }?>	
    </td>
  </tr>
<?php }?>	
<?php if(REGISTERED_IN_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=REGISTERED_IN?> 
    </td>
    <td>
    <select name="regin" class="input">
      <?=REGISTERED_IN_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(USERTYPE_SHOW) {?>	
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=USERTYPE?> 
    </td>
    <td>
    <select name="usertype" class="input">
      <?=USERTYPE_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(SORT_BY_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SORT_BY?> 
    </td>
    <td>
    <select name="sortby" class="input">
      <?=SORT_BY_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(RESULTS_ON_PAGE_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=RESULTS_ON_PAGE?> 
    </td>
    <td>
    <select name="step" class="sinput">
      <?=RESULTS_ON_PAGE_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td colspan=2>
    <table width=100%>
    <tr>
      <td align=left>
      <a href="advsearch.php?l=<?=LANGUAGE?>" class=link2>[<?=ADVANCED_SEARCH?>]</a>
      </td>
      <td align=right>
      <input type="submit" value="<?=SEARCH?>" class=button>
      </td>
    </tr>
    </table>
    </td>
  </tr>
  </table>
  </td>
</tr></table></form></TD></TR></TABLE>