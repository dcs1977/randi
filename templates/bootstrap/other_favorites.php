<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
<TABLE align=center WIDTH="98%" BORDER="0" CELLSPACING="0" CELLPADDING="0"><TR><TD><br><div align="center"><span class=head><?=SEARCH_RESULTS?></span></div><br><br>
<center>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_BWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td><Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_BWIDTH?>" class=mes>
  <Tr align="center" bgcolor="<?=COLORH?>">
    <Td><?=PHOTO?></td>
    <?php if(SHOW_PURPOSE){?><Td><?=PURPOSES?></td><?php }?>
    <?php if(SHOW_COUNTRY){?><Td><?=COUNTRY?></td><?php }?>
    <?php if(SHOW_AGE){?><Td><?=AGE?></td><?php }?>
    <Td><?=ADDITIONAL_INFO?></td>
  </Tr>
<?php  while(isset($ids[$p])) { /* Search results - begin */?>  
  <Tr align="center" bgcolor="<?=$colors[$p]?>">
    <Td width=100>
    <?php if(SHOW_PHOTO){?><?=$pictures[$p]?><?php }?></td>
    <?php if(SHOW_PURPOSE){?><Td width="<?=C_ROWWIDTH_B?>"><?=$genders[$p]?><BR><?=$purposes[$p]?></td><?php }?>
    <?php if(SHOW_COUNTRY){?><Td><?=$countries[$p]?></td><?php }?>
    <?php if(SHOW_AGE){?><Td><?=$ages[$p]?></td><?php }?>
    <Td align=left valign=top width="<?=C_ROWWIDTH_A?>">
        
    <table Border=0 CellSpacing="1" CellPadding="1" width="100%" class=mes>
		<tr><td width=50%><a class=link2 href="<?=GenLink('UserProfile',array(LANGUAGE,$ids[$p]))?>"><?=$names[$p]?></a></td></tr>
    <?php if(SHOW_FNAME){?><tr><td width=50%><?=FIRSTNAME?>:</td><td width=50%><?=$fnames[$p]?></td></tr><?php }?>
    <?php if(SHOW_LNAME){?><tr><td><?=LASTNAME?>:</td><td><?=$lnames[$p]?></td></tr><?php }?>
		<?php if(SHOW_BODYTYPE){?><tr><td><?=BODYTYPE?>:</td><td><?=$bodytypes[$p]?></td></tr><?php }?>
		<?php if(SHOW_ORIENTATION){?><tr><td><?=ORIENTATION?>:</td><td><?=$orientations[$p]?></td></tr><?php }?>
		<?php if(SHOW_CITY){?><tr><td><?=CITY?>:</td><td><?=$cities[$p]?></td></tr><?php }?>
		<?php if(SHOW_HEIGHT){?><tr><td><?=HEIGHT?>:</td><td><?=$heights[$p]?></td></tr><?php }?>
		<?php if(SHOW_WEIGHT){?><tr><td><?=WEIGHT?>:</td><td><?=$weights[$p]?></td></tr><?php }?>
		<?php if(SHOW_SEEKING_GENDER){?><tr><td><?=SEEKING_GENDER?>:</td><td><?=$sgenders[$p]?></td></tr><?php }?>
		<?php if(SHOW_PHOTO){?><tr><td><?=PHOTO?>:</td><td><?=$photoinfo[$p]?></td></tr><?php }?>
		<?php if(SHOW_HORO){?><tr><td><?=HOROSCOPE?>:</td><td><?=$horoscopes[$p]?></td></tr><?php }?>
		<?php if(SHOW_REG_DATE){?><tr><td><?=REGISTER_DATE?>:</td><td><?=$regdates[$p]?></td></tr><?php }?>
		<?php if(SHOW_LAST_ACCESS){?><tr><td><?=LAST_ACCESS?>:</td><td><?=$editdates[$p]?></td></tr><?php }?>
    <?php if(SHOW_RATING){?><tr><td><?=RATING?>:</td><td><?=$rates[$p]?></td></tr><?php }?>
		<?php if(SHOW_USERTYPE){?><tr><td><?=USERTYPE?>:</td><td><?=$usertypes[$p]?></td></tr><?php }?>
		<?php if(SHOW_ONLINE_STATUS){?><tr><td><?=ONLINE_STATUS?>:</td><td>
		<?php if(IsOnline($ids[$p])){?> <span style="color:green"><B><?=ONLINE?></B></span> <?php }else{?> <?=OFFLINE?> <?php }?>
		</td></tr><?php }?>
    </table></td>
  </Tr>
<?php  $p++;} /* Search results - end */?>
  </table>
  </td>
</tr>
</table>
</center>
<BR>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_BWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td><Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_BWIDTH?>" class=mes><?=PAGES?></table></td></tr></table>
  </TD>
 </TR>
</TABLE>
</form><br><br><br>