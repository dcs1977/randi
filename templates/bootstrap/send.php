<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>

   <TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="6">
    <TR> 
     <TD ALIGN="CENTER" VALIGN="TOP">

<script language="JavaScript">
<!--
function ins(prop,type)
{
	var input = document.form.message;
	var mysel;var text;
	if (window.getSelection) mysel = window.getSelection();
	else if (document.selection) mysel = document.selection.createRange().text;
	else if (document.getSelection)	mysel = document.getSelection();
	b = (type == 0) ? '['+prop+']' : prop;
	e = (type == 0) ? '[/'+prop+']' : '';
  if (input.createTextRange)
  {
	  var text;
    input.focus(input.caretPos);
    input.caretPos = document.selection.createRange().duplicate();
    input.caretPos.text = (input.caretPos.text.length>0) ? b + input.caretPos.text + e : b + mysel + e;
   }
   else input.value += b + mysel + e;
}

function formCheck(form) {
    if (form.message.value == "") {
        alert("<?=ENTER_YOUR_MESSAGE?>");
        return false;
    }

    if (captchaContainer !== null){
        return true;
    }

    $('#messageForm button[type="submit"]').prop('disabled', true);

    var form = $('#messageForm')[0];
    var formData = new FormData(form);

    $.ajax({
        method: 'POST',
        url: "send.php",
        processData: false,
        contentType: false,
        dataType: "json",
        data: formData
    })
    .done(function(msg){
        console.log(msg);
        if (msg.status == 200){
            location = msg.href;
        } else if (msg.status == 500){
            const nodeBadge = document.querySelector('.grecaptcha-badge');
            if (nodeBadge) {
                document.body.removeChild(nodeBadge.parentNode);
            }

            const scriptSelector = 'script[src=\'https://www.google.com/recaptcha/api.js?render=<?=RECAPTCHA_PUBLIC_KEY?>\']';
            const script = document.querySelector(scriptSelector);
            if (script) {
                script.remove();
            }

            captchaContainer = grecaptcha.render('captcha_container', {
                'sitekey' : '<?=OLD_RECAPTCHA_PUBLIC_KEY?>',
                'callback' : function(response) {
                    $('#messageForm button[type="submit"]').prop('disabled', false);
                }
            });

            $('#recaptcha_token').remove();
            return false;
        }
    });
    return false;
}
// -->
</script>

<script>
    var captchaContainer = null;

 grecaptcha.ready(function () {
     grecaptcha.execute('<?=RECAPTCHA_PUBLIC_KEY?>', {action: 'message'}).then(function(token) {
         $('#messageForm').prepend('<input id="recaptcha_token" type="hidden" name="token" value="' + token + '">');
         $('#messageForm').prepend('<input type="hidden" name="action" value="message">');
         $('#messageForm button[type="submit"]').prop('disabled', false);
     });
     setInterval(function () {
         grecaptcha.execute('<?=RECAPTCHA_PUBLIC_KEY?>', { action: 'message' }).then(function (token) {
             if ($("#recaptcha_token").length) {
                 $("#recaptcha_token").val(token);
             }
         });
     }, 60000);
 });
</script>

<form action="?" method="post" name=form id="messageForm" OnSubmit="return formCheck(this)" enctype="multipart/form-data" >
<input class=input type=hidden name="l" value="<?=LANGUAGE?>">
<input class=input type=hidden name="id" value="<?=ID_VALUE?>">
<input class=input type=hidden name="a" value="s">
    <div class="container-fluid mt-1 md-1">
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3 text-center">
                <h3><?=SEND_MESSAGE_TO_USER?></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3">
                <div class="card mb-1">
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-12 col-md-2"><?=$w[228]?></label>
                            <div class="col-12 col-md-10 text-left">
                                <?=USER_NAME?>
                            </div>
                        </div>

                        <div class="form-group row email_again">
                            <label class="col-12 col-md-2" for="subject">Ha látod ezt a mezőt, akkor kérlek hagyd üresen!</label>
                            <div class="col-12 col-md-10">
                                <input type="text" name="subject-honeypot" class="form-control" autocomplete="off">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-12 col-md-2" for="message"><?=MESSAGE?></label>
                            <div class="col-12 col-md-10">
                                <div>
                                    <textarea class="form-control" name="message" id="message" rows="4"></textarea>
                                </div>
                                <div class="text-right"><small><span style="color: red">*</span> Kötelező kitölteni.</small></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-12 col-md-2" for="image"><?=$w[87]?></label>
                            <div class="col-12 col-md-10">
                                <input type="file" name="image" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12 col-md-10 offset-md-2">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="confirm" id="confirm"<?=CONFIRM?>>
                                    <label class="form-check-label" for="confirm">
                                        <?=NOTIFY_ME?>
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-12 text-center">
                                <button class="btn btn-primary" type="submit" value="<?=SEND_MESSAGE?>" disabled="disabled"><?=SEND_MESSAGE?></button>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 text-center" id="captcha_container"></div>
                            <style>
                                #captcha_container div{
                                    margin: auto;
                                }
                            </style>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
