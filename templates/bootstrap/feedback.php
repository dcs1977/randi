<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
<script language="JavaScript">
<!--
function formCheck(form) {
    if (form.subject.value == ""){
        alert("<?=ENTER_YOUR_SUBJECT?>");
        return false;
    }

    if (form.message.value == ""){
        alert("<?=ENTER_YOUR_MESSAGE?>");
        return false;
    }

    if (form.remail.value == ""){
        alert("<?=ENTER_YOUR_EMAIL?>");
        return false;
    }

    if (form.approve.checked == 0){
        alert("<?=ENTER_APPROVE_EMAIL?>");
        return false;
    }

    if (captchaContainer !== null){
        return true;
    }

    $('#contactForm button[type="submit"]').prop('disabled', true);

    var form = $('#contactForm')[0];
    var formData = new FormData(form);

    $.ajax({
        method: 'POST',
        url: "feedback.php",
        processData: false,
        contentType: false,
        dataType: "json",
        data: formData
    })
        .done(function(msg){
            console.log(msg);
            if (msg.status == 200){
                location = msg.href;
            } else if (msg.status == 500){
                const nodeBadge = document.querySelector('.grecaptcha-badge');
                if (nodeBadge) {
                    document.body.removeChild(nodeBadge.parentNode);
                }

                const scriptSelector = 'script[src=\'https://www.google.com/recaptcha/api.js?render=<?=RECAPTCHA_PUBLIC_KEY?>\']';
                const script = document.querySelector(scriptSelector);
                if (script) {
                    script.remove();
                }

                captchaContainer = grecaptcha.render('captcha_container', {
                    'sitekey' : '<?=OLD_RECAPTCHA_PUBLIC_KEY?>',
                    'callback' : function(response) {
                        $('#contactForm button[type="submit"]').prop('disabled', false);
                    }
                });

                $('#recaptcha_token').remove();
                return false;
            }
        });
    return false;
}
// -->
</script>

<script>
    var captchaContainer = null;

    grecaptcha.ready(function () {
        grecaptcha.execute('<?=RECAPTCHA_PUBLIC_KEY?>', {action: 'feedback'}).then(function(token) {
            $('#contactForm').prepend('<input id="recaptcha_token" type="hidden" name="token" value="' + token + '">');
            $('#contactForm').prepend('<input type="hidden" name="action" value="feedback">');
            $('#contactForm button[type="submit"]').prop('disabled', false);
        });
        setInterval(function () {
            grecaptcha.execute('<?=RECAPTCHA_PUBLIC_KEY?>', { action: 'feedback' }).then(function (token) {
                if ($("#recaptcha_token").length) {
                    $("#recaptcha_token").val(token);
                }
            });
        }, 60000);
    });
</script>

<form action="feedback.php" method="post" name=form id="contactForm" OnSubmit="return formCheck(this)">
<input class=input type=hidden name="l" value="<?=LANGUAGE?>">
<input class=input type=hidden name="a" value="s">
<div class="container-fluid my-1 text-center">
    <?=FEEDBACK?>
</div>

<div class="container-fluid my-1">
  <div class="row">
    <div class="col-12 col-lg-8 offset-lg-2">
      
      <div class="card mb-1">
        <div class="card-body">

          <div class="form-group row">
            <label class="col-12 col-md-2" for="state"><?=NAME?></label>
            <div class="col-12 col-md-10">
              <input type="text" name="rname" class="form-control" <?=NAME_VALUE_DISABLED?>>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2" for="state"><?=EMAIL?></label>
            <div class="col-12 col-md-10">
              <input type="text" name="remail" class="form-control" <?=EMAIL_VALUE_DISABLED?>>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2" for="state"><?=SUBJECT?></label>
            <div class="col-12 col-md-10">
              <input type="text" name="subject" class="form-control" maxlength="64">
            </div>
          </div>

            <div class="form-group row email_again">
                <label class="col-12 col-md-2" for="state">Ha látod ezt a mezőt, akkor kérlek hagyd üresen!
                <div class="col-12 col-md-10">
                    <input type="text" name="website-honeypot" class="form-control" autocomplete="off">
                </div>
            </div>

          <div class="form-group row">
            <label class="col-12 col-md-2" for="state"><?=MESSAGE?></label>
            <div class="col-12 col-md-10">
              <textarea class="form-control" name="message" rows="15"></textarea>
            </div>
          </div>

            <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" name="approve" id="checkbox_approve" value="igen">
                <label class="form-check-label" for="checkbox_approve">
                    <?=APPROVE_EMAIL?>
                </label>
            </div>

            <div class="text-center"><?=MUST_FILL?></div>

        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid my-1 text-center">
  <!--a href="search.php?l=<?=LANGUAGE?>" class=link2>[<?=SIMPLE_SEARCH?>]</a-->
  <button class="btn btn-primary" type="submit" name="submit" value="<?=SEND_MESSAGE?>" disabled="disabled"><?=SEND_MESSAGE?></button>
</div>

<div class="container-fluid my-1 text-center">
    <div class="col-12 text-center" id="captcha_container"></div>
        <style>
            #captcha_container div{
                margin: auto;
            }
        </style>
</div>

</form>