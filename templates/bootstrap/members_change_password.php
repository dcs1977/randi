<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>


<script language="JavaScript">
<!--
function formCheck(form) {
  if (form.oldpass.value == "") {
    alert("<?=ENTER_OLD_PASSWORD?>");
    return false;
  }
  if (form.newpass.value == "") {
    alert("<?=ENTER_NEW_PASSWORD?>");
    return false;
  }
  if (form.repass.value == "") {
    alert("<?=RETYPE_NEW_PASSWORD?>");
    return false;
  }
  if (form.newpass.value != form.repass.value) {
    alert("<?=PASSWORDS_NOT_IDENTICAL?>");
    return false;
  }

  if (document.form.submit.action != "") {
    document.form.submit.disabled=1;
  }
}
// -->
</script>

<form action="index.php" method="post" name=form OnSubmit="return formCheck(this)">
<input class=input type=hidden name="l" value="<?=LANGUAGE?>">
<input class=input type=hidden name="a" value="x">

<div class="container-fluid my-1 text-center">
    <h2><?=CHANGE_PASSWORD?></h2>
</div>

<div class="container-fluid my-1">
    <div class="row">
      	<div class="col-12 col-lg-8 offset-lg-2">
			<div class="card mb-1">
				<div class="card-body">

                    <?php if (NOT_FORGOTTEN_PASSWORD):?>
				<div class="form-group row">
					<label class="col-12 col-md-4" for="oldpass"><?=OLD_PASSWORD?></label>
					<div class="col-12 col-md-8">
						<input type="password" class="form-control" name="oldpass">
					</div>
				</div>
                    <?php else:?>
                        <input type="hidden" name="fpw" value="1">
                    <?php endif;?>
        <div class="form-group row">
					<label class="col-12 col-md-4" for="newpass"><?=NEW_PASSWORD?></label>
					<div class="col-12 col-md-8">
						<input type="password" class="form-control" name="newpass" maxlength="<?=C_PASSB?>">
					</div>
				</div>

        <div class="form-group row">
					<label class="col-12 col-md-4" for="repass"><?=RETYPE_NEW_PASSWORD?></label>
					<div class="col-12 col-md-8">
						<input type="password" class="form-control" name="repass" maxlength="<?=C_PASSB?>">
					</div>
				</div>

				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid my-1 text-center">
    <button class="btn btn-primary" type="submit" name="submit" value="<?=CHANGE_PASSWORD?>"><?=CHANGE_PASSWORD?></button>
</div>

</form>