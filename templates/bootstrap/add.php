<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
<script language="JavaScript">
<!--
// Select Multiple js-function: hld()
//function hld(nid){ln=nid.parentNode;if(nid.checked){ln.style.backgroundColor='#0a246a';ln.style.color='#fff';}else{ln.style.backgroundColor='transparent';ln.style.color='inherit';}}
function hld(nid){};

var requpl = false;

function IsReqPhoto()
{
	<?php if (PHOTO_ALLOW) {?>
	requpl = true;
	<?php  if(C_PH_REQ_GEN != '0') {?>
	requpl = false;
	var gn = '<?=C_PH_REQ_GEN?>';
	gnr = gn.split(",");
	for (i = 0; i < gnr.length; i++)	{
	 if (document.form.gender.selectedIndex == gnr[i]) requpl = true;
  }
	<?php }?><?php }?><?php 	for($i = 0; $i < $wpn[0]; $i++)	{	if((C_PHOTOR) && ($i < C_REQ_PHOTO_NUM)) {?>
	if(requpl) document.getElementById('PhotoReq_<?=$i?>').innerHTML = '<?=$w[0]?>';
	else document.getElementById('PhotoReq_<?=$i?>').innerHTML = '';
	<?php }}?>
}

function formCheck(form) {
	var cnt = form.elements.length;
	var flds = form.elements;
	<?php if (USERNAME_ALLOW) {?>
	if (form.username.value == "") {
		alert("<?=USERNAME_ERROR?>");
		return false;
	}
	<?php }?>
	<?php if (FIRSTNAME_ALLOW) {?>
	if (form.fname.value == "") {
		alert("<?=FIRSTNAME_ERROR?>");
		return false;
	}
	<?php }?>
	<?php if (LASTNAME_ALLOW) {?>
	if (form.lname.value == "") {
		alert("<?=LASTNAME_ERROR?>");
		return false;
	}
	<?php }?>
	if (form.pass.value == "") {
		alert("<?=PASSWORD_ERROR_1?>");
		return false;
	}
	if (form.rpass.value == "") {
		alert("<?=PASSWORD_ERROR_2?>");
		return false;
	}
	if (form.pass.value != form.rpass.value) {
		alert("<?=PASSWORD_ERROR_3?>");
		return false;
	}
	<?php if (BIRTHDAY_ALLOW) {?>
	if ( (form.month.selectedIndex == "") || (form.day.selectedIndex == "") || (form.year.selectedIndex == "")) {
		alert("<?=BIRTHDAY_ERROR?>");
		return false;
	} else {
		dateString = form.year.value + '-' + ((form.month.value<10)?'0':'') + form.month.value + '-' + ((form.day.value<10)?'0':'') + form.day.value;
		var today = new Date();
		var birthDate = new Date(dateString);
		var age = today.getFullYear() - birthDate.getFullYear();
		var m = today.getMonth() - birthDate.getMonth();
		if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
			age--;
		}
		if(age < 18){
			alert("<?=BIRTHDAY_ERROR?>");
			return false;
		}
	}
	<?php }?>
	<?php if (GENDER_ALLOW) {?>
	if (form.gender.selectedIndex=="") {
		alert("<?=GENDER_ERROR?>");
		return false;
	}
	<?php }?>
	<?php if (BODYTYPE_ALLOW) {?>
	if (form.bodytype.selectedIndex=="") {
		alert("<?=BODYTYPE_ERROR?>");
		return false;
	}
	<?php }?>
	<?php if (ORIENTATION_ALLOW) {?>
	if (form.orientation.selectedIndex=="") {
		alert("<?=ORIENTATION_ERROR?>");
		return false;
	}
	<?php }?>
	<?php if (SGENDER_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,7) == "sgender" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=SGENDER_ERROR?>");return false;}
	<?php }?>
	<?php if (PURPOSE_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,7) == "purpose" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=PURPOSE_ERROR?>");return false;}
	<?php }?>

	<?php if (ZIP_ALLOW) {?>
	if (form.zip.value=="") {
		alert("<?=ZIP_ERROR?>");
		return false;
	}
	<?php }?>
<?php if (COUNTRY_ALLOW) {?>
	if (form.country.selectedIndex=="") {
		alert("<?=COUNTRY_ERROR?>");
		return false;
	}
	<?php }?>
	<?php if (STATE_ALLOW) {?>
	if (form.state.value == "" && form.statelist.selectedIndex <= "0") {
		alert("<?=STATE_ERROR?>");
		return false;
	}
	<?php }?>
	if (form.email.value == "") {
		alert("<?=EMAIL_ERROR?>");
		return false;
	}
	<?php if (PHONE_ALLOW) {?>
	if (form.phone.value == "") {
		alert("<?=PHONE_ERROR?>");
		return false;
	}
	<?php }?>

	<?php if (CITY_ALLOW) {?>
	if (form.city.value == "") {
		alert("<?=CITY_ERROR?>");
		return false;
	}
	<?php }?>

	<?php if (ADDRESS_ALLOW) {?>
	if (form.address.value == "") {
		alert("<?=ADDRESS_ERROR?>");
		return false;
	}
	<?php }?>

	<?php if (MARSTAT_ALLOW) {?>
	if (form.marstat.selectedIndex=="") {
		alert("<?=MARSTAT_ERROR?>");
		return false;
	}
	<?php }?>

	<?php if (CHILD_ALLOW) {?>
	if (form.child.selectedIndex=="") {
		alert("<?=CHILD_ERROR?>");
		return false;
	}
	<?php }?>

	<?php if (HEIGHT_ALLOW) {?>
	if (form.height.selectedIndex=="") {
		alert("<?=HEIGHT_ERROR?>");
		return false;
	}
	<?php }?>

	<?php if (WEIGHT_ALLOW) {?>
	if (form.weight.selectedIndex=="") {
		alert("<?=WEIGHT_ERROR?>");
		return false;
	}
	<?php }?>

	<?php if (HAIR_ALLOW) {?>
	if (form.hcolor.selectedIndex=="") {
		alert("<?=HAIR_ERROR?>");
		return false;
	}
	<?php }?>

	<?php if (EYE_ALLOW) {?>
	if (form.ecolor.selectedIndex=="") {
		alert("<?=EYE_ERROR?>");
		return false;
	}
	<?php }?>

	<?php if (ETHNICITY_ALLOW) {?>
	if (form.etnicity.selectedIndex=="") {
		alert("<?=ETHNICITY_ERROR?>");
		return false;
	}
	<?php }?>

	<?php if (RELIGION_ALLOW) {?>
	if (form.religion.selectedIndex=="") {
		alert("<?=RELIGION_ERROR?>");
		return false;
	}
	<?php }?>

	<?php if (SMOKE_ALLOW) {?>
	if (form.smoke.selectedIndex=="") {
		alert("<?=SMOKE_ERROR?>");
		return false;
	}
	<?php }?>

	<?php if (DRINK_ALLOW) {?>
	if (form.drink.selectedIndex=="") {
		alert("<?=DRINK_ERROR?>");
		return false;
	}
	<?php }?>

	<?php if (EDUCATION_ALLOW) {?>
	if (form.education.selectedIndex=="") {
		alert("<?=EDUCATION_ERROR?>");
		return false;
	}
	<?php }?>

	<?php if (JOB_ALLOW) {?>
	if (form.job.value=="") {
		alert("<?=JOB_ERROR?>");
		return false;
	}
	<?php }?>
	
	<?php if (LANG_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,4) == "lang" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=LANG_ERROR?>");return false;}
	<?php }?>

	
	<?php if (HOBBY_ALLOW) {?>
	if (form.hobby.value=="") {
		alert("<?=HOBBY_ERROR?>");
		return false;
	}
	<?php }?>

	<?php if (INTERESTS_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,9) == "interests" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=INTERESTS_ERROR?>");return false;}
	<?php }?>
	
	<?php if (DESCRIPTION_SHOW) {?>
function trim(s)
{
  return s.replace(/^\s+|\s+$/, '');
}    
if (trim(form.descr.value)=="") {
		alert("<?=DESCRIPTION_ERROR?>");
		return false;
	}
	<?php }?>

	<?php if (SCOUNTRY_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,8) == "scountry" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=SCOUNTRY_ERROR?>");return false;}
	<?php }?>
	<?php if (SMARSTAT_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,8) == "smarstat" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=SMARSTAT_ERROR?>");return false;}
	<?php }?>
	<?php if (SCHILD_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,6) == "schild" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=SCHILD_ERROR?>");return false;}
	<?php }?>
	<?php if (SHAIR_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,7) == "shcolor" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=SHAIR_ERROR?>");return false;}
	<?php }?>
	<?php if (SEYE_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,7) == "secolor" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=SEYE_ERROR?>");return false;}
	<?php }?>
	<?php if (SSMOKE_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,6) == "ssmoke" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=SSMOKE_ERROR?>");return false;}
	<?php }?>
	<?php if (SDRINK_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,6) == "sdrink" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=SDRINK_ERROR?>");return false;}
	<?php }?>
	<?php if (SEDUCATION_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,10) == "seducation" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=SEDUCATION_ERROR?>");return false;}
	<?php }?>
	
	<?php if (SETHNICITY_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,9) == "setnicity" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=SETHNICITY_ERROR?>");return false;}
	<?php }?>

	<?php if (SRELIGION_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,9) == "sreligion" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=SRELIGION_ERROR?>");return false;}
	<?php }?>
	<?php if (HDYFU_ALLOW) {?>
	if (form.hdyfu.selectedIndex=="") {
		alert("<?=HDYFU_ERROR?>");
		return false;
	}
	<?php }?>
	<?php if (PHOTO_ALLOW) {?>
	if ((form.file0.value == "") && (form.file1.value == "") && (form.file2.value == "")) {
	requpl = true;
	<?php  if(C_PH_REQ_GEN != '0') {?>
	requpl = false;
	var gnd = '<?=C_PH_REQ_GEN?>';
	gndr = gnd.split(",");
	for (ind = 0; ind < gndr.length; ind++)	{
	 if (form.gender.selectedIndex == gndr[ind]) {requpl = true;}
  }
	<?php }?>
		if(requpl) {
			alert("<?=PHOTO_ERROR?>");
			return false;
		}
	}
	<?php }?>
	<?php if (VERIFY_ALLOW) {?>
	if (form.verifyimage.value == "") {
		alert("<?=VERIFY_ERROR?>");
		return false;
	}
	<?php }?>
<?php if (REG_TERMS_ALLOW) {?>
if (form.reg_terms.checked == 0) {
    alert("<?=REG_TERMS_ERROR?>");
    return false;
}
<?php }?>

    if (captchaContainer !== null){
        return true;
    }

    $('#regisForm button[type="submit"]').prop('disabled', true);

    var form = $('#regisForm')[0];
    var formData = new FormData(form);

    $.ajax({
        method: 'POST',
        url: "add.php",
        processData: false,
        contentType: false,
        dataType: "json",
        data: formData
    })
        .done(function(msg){
            console.log(msg);
            if (msg.status == 200){
                location = msg.href;
            } else if (msg.status == 500){
                const nodeBadge = document.querySelector('.grecaptcha-badge');
                if (nodeBadge) {
                    document.body.removeChild(nodeBadge.parentNode);
                }

                const scriptSelector = 'script[src=\'https://www.google.com/recaptcha/api.js?render=<?=RECAPTCHA_PUBLIC_KEY?>\']';
                const script = document.querySelector(scriptSelector);
                if (script) {
                    script.remove();
                }

                captchaContainer = grecaptcha.render('captcha_container', {
                    'sitekey' : '<?=OLD_RECAPTCHA_PUBLIC_KEY?>',
                    'callback' : function(response) {
                        $('#regisForm button[type="submit"]').prop('disabled', false);
                    }
                });

                $('#recaptcha_token').remove();
                return false;
            }
        });
    return false;
}

function CheckUncheckAll(itemname) {
  console.log(document.form);
	with(document.form) {
		for(i=0;i<elements.length;i++) {
			thiselm = elements[i];
			if(thiselm.name == itemname) thiselm.checked = !thiselm.checked
		}
	}
}

function ins(prop,type)
{
	var input = document.form.descr;var mysel;var text;
	if (window.getSelection) mysel = window.getSelection();
	else if (document.selection) mysel = document.selection.createRange().text;
	else if (document.getSelection)	mysel = document.getSelection();
	b = (type == 0) ? '['+prop+']' : prop;
	e = (type == 0) ? '[/'+prop+']' : '';
	if (input.createTextRange)
	{
		var text;
		input.focus(input.caretPos);
		input.caretPos = document.selection.createRange().duplicate();
		input.caretPos.text = (input.caretPos.text.length>0) ? b + input.caretPos.text + e : b + mysel + e;
	}
	else input.value += b + mysel + e;
}
// -->
</script>
<script language="JavaScript" src="<?=C_URL?>/classes/Ajax.js"></script>
<script>
	function SR(force) {
		var username = '' + document.getElementById('username').value;
		var req = new Ajax();
		req.onreadystatechange = function() {
			if (req.readyState == 4) {
				if (req.resp) {
					document.getElementById('UsernameValue').innerHTML = req.resp.username;
				}
			}
		}
		req.caching = true;
		req.open('POST', '<?=C_URL?>/ajax/dl.php', true);
		req.send({ check:'1',username: username});
	}
	
</script>

<script>
    var captchaContainer = null;

    grecaptcha.ready(function () {
        grecaptcha.execute('<?=RECAPTCHA_PUBLIC_KEY?>', {action: 'registration'}).then(function(token) {
            $('#regisForm').prepend('<input id="recaptcha_token" type="hidden" name="token" value="' + token + '">');
            $('#regisForm').prepend('<input type="hidden" name="action" value="registration">');
            $('#regisForm button[type="submit"]').prop('disabled', false);
        });
        setInterval(function () {
            grecaptcha.execute('<?=RECAPTCHA_PUBLIC_KEY?>', { action: 'registration' }).then(function (token) {
                if ($("#recaptcha_token").length) {
                    $("#recaptcha_token").val(token);
                }
            });
        }, 60000);
    });
</script>

<form action="<?=filename()?>" method="post" enctype="multipart/form-data" name=form id="regisForm" OnSubmit="return formCheck(this)">
<input type="hidden" name="l" value="<?=LANGUAGE?>">
<input type="hidden" name="a" value="a">
<input type="hidden" name="fingerprint" id="fingerprint" value="">
  <div class="container-fluid my-1 text-center">
    <h2><?=REGISTER_USER?></h2>
  </div>
  <div class="container-fluid my-1">
    <div class="row">
      <div class="col-12 col-lg-8 offset-lg-2">
        <div class="card mb-1">
            <div class="card-body">

              <?php if(USERNAME_ALLOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="username"><?=USERNAME?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="text" id="username" name="username" maxlength="16" value="<?=USERNAME_VALUE?>" OnChange="SR(true)">
                </div>                
              </div>
              <?php }?>

              <div class="form-group row">
                <label class="col-12 col-md-2" for="email"><?=EMAIL?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="text" name="email" value="<?=EMAIL_VALUE?>">
                </div>
              </div>

                <div class="form-group row email_again">
                    <label class="col-12 col-md-2" for="email-again-honeypot">Ha látod ezt a mezőt, akkor kérlek hagyd üresen!
                    <div class="col-12 col-md-10">
                        <input class="form-control" type="text" name="email-again-honeypot" value="<?=EMAIL_VALUE?>" autocomplete="off">
                    </div>
                </div>

              <div class="form-group row">
                <label class="col-12 col-md-2" for="pass"><?=PASSWORD?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="password" name="pass" maxlength="<?=C_PASSB?>" value="<?=PASSWORD_VALUE?>">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-12 col-md-2" for="rpass"><?=RETYPE_PASSWORD?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="password" name="rpass" maxlength="<?=C_PASSB?>" value="<?=RETYPE_PASSWORD_VALUE?>">
                </div>
              </div>

              <?php if(WANTPRIV_SHOW) {?>
                <div class="form-group">
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="wantpriv" id="wantpriv" <?=WANTPRIV_VALUE?>>
                    <label class="form-check-label" for="wantpriv">
                      <?=WANTPRIV?>
                    </label>
                  </div>
                </div>
              <?php }?>

              <?php if(VERIFY_ALLOW) {?>
                <div class="form-group">
                  <label for="verifyimage"><?=VERIFICATION?></label>
                  <div>
                    <img src=img.php border=0>
                  </div>
                  <input class="form-control" type="text" name="verifyimage">
                </div>
              <?php }?>

              <?php if(AGREEMENT){?>
                <div class="form-group">
                  <textarea class="form-control" rows="12" disabled="disabled"><?php include_once C_PATH.'/languages/'.LANGUAGE.'/agr.php';?></textarea>
                </div>
              <?php }?>

              <?php if(REG_TERMS_SHOW) {?>
                <div class="form-group">
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="reg_terms" id="reg_terms" <?=REG_TERMS_VALUE?>>
                    <label class="form-check-label" for="reg_terms">
                      <?=REG_TERMS?>
                    </label>
                  </div>
                </div>
              <?php }?>

              <?php if(HDYFU_SHOW) {?>
                <div class="form-group row">
                  <label class="col-12 col-md-2" for="hdyfu"><?=HOW_DID_YOU_FIND_US?></label>
                  <div class="col-12 col-md-10">
                    <select name="hdyfu" class="form-control">
                      <option selected value=0> --------- 
                      <?=HOW_DID_YOU_FIND_US_VALUE?>
                    </select>
                  </div>
                </div>
              <?php }?>

              <!-- Not showed values -->

              <?php if(ADDRESS_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="address"><?=ADDRESS?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="text" name="address" value="<?=ADDRESS_VALUE?>">
                </div>
              </div>
              <?php }?>

              <?php if(URL_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="url"><?=URL?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="text" name="url" value="<?=URL_VALUE?>">
                </div>                
              </div>
              <?php }?>

              <?php if(ICQ_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="icq"><?=ICQ?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="text" name="icq" value="<?=ICQ_VALUE?>">
                </div>
              </div>
              <?php }?>

              <?php if(AIM_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="aim"><?=AIM?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="text" name="aim" value="<?=AIM_VALUE?>">
                </div>
              </div>
              <?php }?>

              <?php if(PHONE_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="phone"><?=PHONE?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="text" name="phone" value="<?=PHONE_VALUE?>">
                </div>
              </div>
              <?php }?>

              <?php if(LANG_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="phone"><?=LANG?></label>
                <div class="col-12 col-md-10">
                  <div class="form-check">
                    <?php $k = 1;while (isset($wlng[$k])) {?>
                        <label><?=$checklang[$k]?> <?=$wlng[$k]?></label>
                    <?php $k++;}?>
                  </div>
                </div>
              </div>
              <?php }?>

              <?php if(JOB_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="job"><?=JOB?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="text" name="job" value="<?=JOB_VALUE?>">
                </div>
              </div>
              <?php }?>

              <?php if(HOBBY_SHOW) {?>
              <div class="form-group row">
                <label class="col-12 col-md-2" for="hobby"><?=HOBBY?></label>
                <div class="col-12 col-md-10">
                  <input class="form-control" type="text" name="hobby" value="<?=HOBBY_VALUE?>">
                </div>
              </div>
              <?php }?>

              <?php if(INTERESTS_SHOW) {?>
                <div class="form-group row">
                  <label class="col-12 col-md-2"><?=INTERESTS?></label>
                  <div class="col-12 col-md-10">
                    <div class="form-check">
                      <?=INTERESTS_VALUE?>
                      <?php if(C_CHECK_ALL){?><label for="interestss"><input type="checkbox" id="interestss" OnClick="javascript:CheckUncheckAll('interests[]')"> <?=CHECK_UNCHECK_ALL?></label><?php }?>
                    </div>
                  </div>
                </div>
              <?php }?>

            </div>

        </div>
      </div>

      <div class="col-12 col-lg-8 offset-lg-2 mt-1">
        <div class="card mb-1">
            <div class="card-body">
              <div class="row">

                <div class="col-12">

                  <div class="form-group row">
                    <label class="col-12 col-md-2" for="descr"><?=DESCRIPTION?></label>
                    <div class="col-12 col-md-10">
                      <?php if(USE_SMILES || USE_BB){?>
                      <Table Border=0 width="100%" CellSpacing="2" CellPadding="6"><Tr align="left" bgcolor="<?=COLORH?>"><Td>
                      <?php if(USE_SMILES){?><?php $p=0;foreach ($smiles as $k=>$v){$p++;?><a href="javascript:ins(' <?=$k?> ',1)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/forum/smiles/<?=$v?>" border=0 /></a>
                      <?php if(C_SMILES_NUMBER <= $p){?><br><a href="javascript:open_win('<?=C_URL?>/forum/smiles_show.php','_blank')"><?=$w[720]?></a><br><?php break;}?><?php }?><?php }?> 
                      <?php if(USE_BB && USE_SMILES){?><br><br><?php }?>
                      <?php if(USE_BOLD){?><a href="javascript:ins('B',0)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Bold.gif" border=0 /></a><?php }?>
                      <?php if(USE_ITALIC){?><a href="javascript:ins('I',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Italic.gif" border=0 /></a><?php }?>
                      <?php if(USE_UNDERLINE){?><a href="javascript:ins('U',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Underline.gif" border=0 /></a><?php }?>
                      <?php if(USE_STRIKE){?><a href="javascript:ins('S',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Strike.gif" border=0 /></a><?php }?>
                      <?php if(USE_QUOTE){?><a href="javascript:ins('QUOTE',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Quote.gif" border=0 /></a><?php }?>  
                      <?php if(USE_URL){?><a href="javascript:ins('URL',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/URL.gif" border=0 /></a><?php }?>  
                      <?php if(USE_IMG){?><a href="javascript:ins('IMG',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Image.gif" border=0 /></a><?php }?>  
                      <?php if(USE_VIDEO){?><a href="javascript:ins('VIDEO',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Video.gif" border=0 /></a><?php }?>  
                      </td></tr></table><?php }?>

                      <textarea class="form-control" rows="6" name="descr"><?=DESCRIPTION_VALUE?></textarea>
                    </div>
                  </div>
                  
                  <?php if(FIRSTNAME_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2" for="fname"><?=FIRSTNAME?></label>
                    <div class="col-12 col-md-10">
                      <input class="form-control" type="text" name="fname" maxlength="<?=C_FIRSTNB?>" value="<?=FIRSTNAME_VALUE?>">
                    </div>
                  </div>
                  <?php }?>

                  <?php if(LASTNAME_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2" for="lname"><?=LASTNAME?></label>
                    <div class="col-12 col-md-10">
                      <input class="form-control" type="text" name="lname" maxlength="<?=C_LASTNB?>" value="<?=LASTNAME_VALUE?>">
                    </div>
                  </div>
                  <?php }?>

                  <?php if(BIRTHDAY_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2" for="year"><?=BIRTHDAY?></label>
                    <div class="col-12 col-md-10">
                      <div class="row">
                        <div class="col-4">
                          <select name="year" class="form-control">
                            <option value=0> ----- 
                            <?=YEAR_VALUE?>
                          </select>
                        </div>
                        <div class="col-5">
                          <select name="month" class="form-control">
                            <option value=0> ----- 
                            <?=MONTH_VALUE?>
                          </select>
                        </div>
                        <div class="col-3">
                          <select name="day" class="form-control">
                            <option value=0> ----- 
                            <?=DAY_VALUE?>
                          </select>
                        </div>
                      </div>
                    </div>                 
                  </div>
                  <?php }?>

                  <input type="hidden" name="gender" value="1">

                  <?php if(COUNTRY_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2" for="country"><?=COUNTRY?></label>
                    <div class="col-12 col-md-10">
                      <select name="country" class="form-control"<?php if(STATE_SHOW) {?> onChange="ShowRegion(this.options[this.selectedIndex].value)"<?php }?>>
                        <?=COUNTRY_VALUE?>
                      </select>
                    </div>
                  </div>
                  <?php }?>

                  <?php if(STATE_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2" for="state"><?=STATE?></label>
                    <div class="col-12 col-md-10">
                      <div id='state'>
                        <input class="form-control" type="text" name="state" value="<?=STATE_VALUE?>">
                      </div>
                      <div id='statelist' style="display:none;">
                          <select name="statelist" class="form-control">
                          </select>
                      </div>
                    </div>                    
                  </div>

                  <script language="JavaScript">
                  var ids=new Array('state','statelist');
                  function switchid(id){hideallids();	showdiv(id);}
                  function hideallids(){for (var i=0;i<ids.length;i++){hidediv(ids[i]);}}
                  function hidediv(id) {if (document.getElementById) {document.getElementById(id).style.display = 'none';}
                    else {if (document.layers) {document.id.display = 'none';}
                      else {document.all.id.style.display = 'none';}}}
                  function showdiv(id) {	  
                    if (document.getElementById) {document.getElementById(id).style.display = 'block';}
                    else {if (document.layers) {document.id.display = 'block';}
                      else {document.all.id.style.display = 'block';}}}
                  var rc= new Array(500);
                  <?=STATELIST_VALUE?>
                  function GetIndex(index){var sc = rc[index];return sc.split(","); }
                  function ShowRegion(index){
                  if(<?=STATELIST_CHECK?>) {
                  switchid('statelist');
                  document.forms["form"].elements["state"].value = '';
                  var ind = GetIndex(index);var cur = '<?=STATELIST_CUR?>';var cnt = ind.length;var lst = document.forms["form"].elements["statelist"];var lstOptionsCnt = lst.options.length;lst.length = 0; 
                      lst.options[0] = new Option('--------', '', false, false);
                    for (i = 1; i < cnt; i++){
                          lst.options[i] = (cur == ind[i]) ? new Option(ind[i], ind[i], true, false) : new Option(ind[i], ind[i], false, false);
                      }
                  } else {
                  switchid('state');
                  for (var i = 0; i < document.forms["form"].elements["statelist"].length; i++) {document.forms["form"].elements["statelist"].selected = false;}
                  document.forms["form"].elements["statelist"].value = '';
                  }
                  }
                  ShowRegion(document.forms["form"].elements["country"].options[document.forms["form"].elements["country"].options.selectedIndex].value);
                  // -->
                  </script>
                  <?php }?>

                  <?php if(ZIP_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2" for="zip"><?=ZIP?></label>
                    <div class="col-12 col-md-10">
                      <input class="form-control" type="text" name="zip" value="<?=ZIP_VALUE?>">
                    </div>
                  </div>
                  <?php }?>

                  <?php if(CITY_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2" for="city"><?=CITY?></label>
                    <div class="col-12 col-md-10">
                      <input class="form-control" type="text" name="city" value="<?=CITY_VALUE?>">
                    </div>
                  </div>
                  <?php }?>

                  <?php if(ORIENTATION_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2" for="orientation"><?=ORIENTATION?></label>
                    <div class="col-12 col-md-10">
                      <select name="orientation" class="form-control">
                        <option value=0> --------- 
                        <?=ORIENTATION_VALUE?>
                      </select>
                    </div>
                  </div>
                  <?php }?>

                  <?php if(MARSTAT_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2" for="marstat"><?=MARITAL_STATUS?></label>
                    <div class="col-12 col-md-10">
                      <select name="marstat" class="form-control">
                        <option value=0> --------- 
                        <?=MARITAL_STATUS_VALUE?>
                      </select>
                    </div>
                  </div>
                  <?php }?>

                  <?php if(CHILD_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2" for="child"><?=CHILD?></label>
                    <div class="col-12 col-md-10">
                      <select name="child" class="form-control">
                        <option value=0> --------- 
                        <?=CHILD_VALUE?>
                      </select>
                    </div>
                  </div>
                  <?php }?>

                </div>

                <div class="col-12">

                  <?php if(PURPOSE_SHOW) {?>
                    <div class="form-group row">
                      <label class="col-12 col-md-2"><?=PURPOSE?></label>
                      <div class="col-12 col-md-10">
                        <div class="form-check">
                          <?=PURPOSE_VALUE?>
                          <?php if(C_CHECK_ALL){?><label for="purposes"><input type="checkbox" id="purposes" OnClick="javascript:CheckUncheckAll('purpose[]')"> <?=CHECK_UNCHECK_ALL?></label><?php }?>
                        </div>
                      </div>
                    </div>
                  <?php }?>

                  <?php if(BODYTYPE_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2" for="bodytype"><?=BODYTYPE?></label>
                    <div class="col-12 col-md-10">
                      <select name="bodytype" class="form-control">
                        <option value=0> --------- 
                        <?=BODYTYPE_VALUE?>
                      </select>
                    </div>
                  </div>
                  <?php }?>

                  <?php if(HEIGHT_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2" for="height"><?=HEIGHT?></label>
                    <div class="col-12 col-md-10">
                      <select name="height" class="form-control">
                        <option value=0> --------- 
                        <?=HEIGHT_VALUE?>
                      </select>
                    </div>
                  </div>
                  <?php }?>

                  <?php if(WEIGHT_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2" for="weight"><?=WEIGHT?></label>
                    <div class="col-12 col-md-10">
                      <select name="weight" class="form-control">
                        <option value=0> --------- 
                        <?=WEIGHT_VALUE?>
                      </select>
                    </div>
                  </div>
                  <?php }?>

                  <?php if(HAIR_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2" for="hcolor"><?=HAIR?></label>
                    <div class="col-12 col-md-10">
                      <select name="hcolor" class="form-control">
                        <option value=0> --------- 
                        <?=HAIR_VALUE?>
                      </select>
                    </div>
                  </div>
                  <?php }?>

                  <?php if(EYE_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2" for="ecolor"><?=EYE?></label>
                    <div class="col-12 col-md-10">
                      <select name="ecolor" class="form-control">
                        <option value=0> --------- 
                        <?=EYE_VALUE?>
                      </select>
                    </div>
                  </div>
                  <?php }?>

                  <?php if(ETHNICITY_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2" for="etnicity"><?=ETHNICITY?></label>
                    <div class="col-12 col-md-10">
                      <select name="etnicity" class="form-control">
                        <option value=0> --------- 
                        <?=ETHNICITY_VALUE?>
                      </select>
                    </div>
                  </div>
                  <?php }?>

                  <?php if(RELIGION_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2" for="religion"><?=RELIGION?></label>
                      <div class="col-12 col-md-10">
                          <div class="form-check">
                              <?=RELIGION_VALUE?>
                              <?php if(C_CHECK_ALL){?><label for="religions"><input type="checkbox" id="religions" OnClick="javascript:CheckUncheckAll('religion[]')"> <?=CHECK_UNCHECK_ALL?></label><?php }?>
                          </div>
                      </div>
                  </div>
                  <?php }?>

                  <?php if(SMOKE_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2" for="smoke"><?=SMOKE?></label>
                    <div class="col-12 col-md-10">
                      <select name="smoke" class="form-control">
                        <option value=0> --------- 
                        <?=SMOKE_VALUE?>
                      </select>
                    </div>
                  </div>
                  <?php }?>

                  <?php if(DRINK_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2" for="drink"><?=DRINK?></label>
                    <div class="col-12 col-md-10">
                      <select name="drink" class="form-control">
                        <option value=0> --------- 
                        <?=DRINK_VALUE?>
                      </select>
                    </div>
                  </div>
                  <?php }?>

                  <?php if(EDUCATION_SHOW) {?>
                  <div class="form-group row">
                    <label class="col-12 col-md-2" for="education"><?=EDUCATION?></label>
                    <div class="col-12 col-md-10">
                      <select name="education" class="form-control">
                        <option value=0> --------- 
                        <?=EDUCATION_VALUE?>
                      </select>
                    </div>
                  </div>
                  <?php }?>

                </div>

              </div>
            </div>
        </div>
      </div>

      <div class="col-12 col-lg-8 offset-lg-2 mt-1">

        <?php if(PHOTO_SHOW) {?>
        <div class="card mb-1">
            <div class="card-body">
                <div class="alert alert-info">
                    <?=$w['875']?>
                </div>
              <?php $p=0;while(isset($PHOTO_NUM[$p])) {?>
                <div class="form-group row">
                  <label class="col-12 col-md-2" for="file<?=$p?>"><?=$PHOTO_NUM[$p]?></label>
                  <div class="col-12 col-md-10">
                    <input type="file" class="form-control-file" name="file<?=$p?>">
                  </div>
                </div>
              <?php $p++;}$p=0;?>
            </div>
        </div>
        <?php }?>

      </div>
      
    </div>

    <div class="row">
      <div class="col-12 col-lg-8 offset-lg-2 mt-1">
        <div class="card mb-1">
            <div class="card-body">
                <div class="row">
                  <div class="col-12">

                    <?php if(SAGE_SHOW) {?>
                    <div  class="form-group row">
                      <label class="col-12 col-md-2" for="agef"><?=SEEKING_AGE?></label>
                      <div class="col-12 col-md-10">
                        <div class="row">
                          <div class="col-6">
                            <select name="agef" class="form-control">
                              <?=SEEKING_AGE_FROM_VALUE?>
                            </select>
                          </div>
                          <div class="col-6">
                            <select name="aget" class="form-control">
                              <?=SEEKING_AGE_TO_VALUE?>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php }?>

                    <?php if(SCOUNTRY_SHOW) {?>
                      <div class="form-group row">
                        <label class="col-12 col-md-2"><?=SEEKING_COUNTRY?></label>
                        <div class="col-12 col-md-10">
                          <div class="form-check">
                            <?=SEEKING_COUNTRY_VALUE?>
                            <?php if(C_CHECK_ALL){?><label for="scountrys"><input type="checkbox" id="scountrys" OnClick="javascript:CheckUncheckAll('scountry[]')"> <?=CHECK_UNCHECK_ALL?></label><?php }?>
                          </div>
                        </div>
                      </div>
                    <?php }?>

                  </div>
                  <div class="col-12">

                    <?php if(SHEIGHT_SHOW) {?>
                    <div class="form-group row">
                      <label class="col-12 col-md-2" for="heightf"><?=SEEKING_HEIGHT?></label>
                      <div class="col-12 col-md-10">
                        <div class="row">
                          <div class="col-6">
                            <select name="heightf" class="form-control">
                              <?=SEEKING_HEIGHT_FROM_VALUE?>
                            </select>
                          </div>
                          <div class="col-6">
                            <select name="heightt" class="form-control">
                              <?=SEEKING_HEIGHT_TO_VALUE?>
                            </select>
                          </div>
                        </div>
                      </div>                    
                    </div>
                    <?php }?>

                    <?php if(SWEIGHT_SHOW) {?>
                    <div class="form-group row">
                      <label class="col-12 col-md-2" for="weightf"><?=SEEKING_WEIGHT?></label>
                      <div class="col-12 col-md-10">
                        <div class="row">
                          <div class="col-6">
                            <select name="weightf" class="form-control">
                              <?=SEEKING_WEIGHT_FROM_VALUE?>
                            </select>
                          </div>
                          <div class="col-6">
                            <select name="weightt" class="form-control">
                              <?=SEEKING_WEIGHT_TO_VALUE?>
                            </select>
                          </div>
                        </div>
                      </div>                    
                    </div>
                    <?php }?>

                      <?php if(SHAIR_SHOW) {?>
                          <div class="form-group row">
                            <label class="col-12 col-md-2"><?=SEEKING_HAIR?></label>
                            <div class="col-12 col-md-10">
                              <div class="form-check">
                                <?=SEEKING_HAIR_VALUE?>
                                <?php if(C_CHECK_ALL){?><label for="shcolors"><input type="checkbox" id="shcolors" OnClick="javascript:CheckUncheckAll('shcolor[]')"> <?=CHECK_UNCHECK_ALL?></label><?php }?>
                              </div>
                            </div>
                          </div>
                      <?php }?>

                      <?php if(SEYE_SHOW) {?>
                          <div class="form-group row">
                            <label class="col-12 col-md-2"><?=SEEKING_EYE?></label>
                            <div class="col-12 col-md-10">
                              <div class="form-check">
                                <?=SEEKING_EYE_VALUE?>
                                <?php if(C_CHECK_ALL){?><label for="secolors"><input type="checkbox" id="secolors" OnClick="javascript:CheckUncheckAll('secolor[]')"> <?=CHECK_UNCHECK_ALL?></label><?php }?>
                              </div>
                            </div>
                          </div>
                      <?php }?>

                      <?php if(SSMOKE_SHOW) {?>
                          <div class="form-group row">
                            <label class="col-12 col-md-2"><?=SEEKING_SMOKE?></label>
                            <div class="col-12 col-md-10">
                              <div class="form-check">
                                <?=SEEKING_SMOKE_VALUE?>
                                <?php if(C_CHECK_ALL){?><label for="ssmokes"><input type="checkbox" id="ssmokes" OnClick="javascript:CheckUncheckAll('ssmoke[]')"> <?=CHECK_UNCHECK_ALL?></label><?php }?>
                              </div>
                            </div>
                          </div>
                      <?php }?>

                      <?php if(SDRINK_SHOW) {?>
                          <div class="form-group row">
                            <label class="col-12 col-md-2"><?=SEEKING_DRINK?></label>
                            <div class="col-12 col-md-10">
                              <div class="form-check">
                                <?=SEEKING_DRINK_VALUE?>
                                <?php if(C_CHECK_ALL){?><label for="sdrinks"><input type="checkbox" id="sdrinks" OnClick="javascript:CheckUncheckAll('sdrink[]')"> <?=CHECK_UNCHECK_ALL?></label><?php }?>
                              </div>
                            </div>
                          </div>
                      <?php }?>

                    </div>
                    <div class="col-12">

                      <?php if(SMARSTAT_SHOW) {?>
                          <div class="form-group row">
                            <label class="col-12 col-md-2"><?=SEEKING_MARITAL_STATUS?></label>
                            <div class="col-12 col-md-10">
                              <div class="form-check">
                                <?=SEEKING_MARITAL_STATUS_VALUE?>
                                <?php if(C_CHECK_ALL){?><label for="smarstats"><input type="checkbox" id="smarstats" OnClick="javascript:CheckUncheckAll('smarstat[]')"> <?=CHECK_UNCHECK_ALL?></label><?php }?>
                              </div>
                            </div>
                          </div>
                      <?php }?>

                      <?php if(SCHILD_SHOW) {?>
                          <div class="form-group row">
                            <label class="col-12 col-md-2"><?=SEEKING_CHILD?></label>
                            <div class="col-12 col-md-10">
                              <div class="form-check">
                                <?=SEEKING_CHILD_VALUE?>
                                <?php if(C_CHECK_ALL){?><label for="schilds"><input type="checkbox" id="schilds" OnClick="javascript:CheckUncheckAll('schild[]')"> <?=CHECK_UNCHECK_ALL?></label><?php }?>
                              </div>
                            </div>
                          </div>
                      <?php }?>

                      <?php if(SGENDER_SHOW) {?>
                        <div class="form-group row">
                          <label class="col-12 col-md-2"><?=SEEKING_GENDER?></label>
                          <div class="col-12 col-md-10">
                            <div class="form-check">
                              <?=SEEKING_GENDER_VALUE?>
                              <?php if(C_CHECK_ALL){?><label for="sgenders"><input type="checkbox" id="sgenders" OnClick="javascript:CheckUncheckAll('sgender[]')"> <?=CHECK_UNCHECK_ALL?></label><?php }?>
                            </div>
                          </div>
                        </div>
                      <?php }?>

                      <?php if(SEDUCATION_SHOW) {?>
                          <div class="form-group row">
                            <label class="col-12 col-md-2"><?=SEEKING_EDUCATION?></label>
                            <div class="col-12 col-md-10">
                              <div class="form-check">
                                <?=SEEKING_EDUCATION_VALUE?>
                                <?php if(C_CHECK_ALL){?><label for="seducations"><input type="checkbox" id="seducations" OnClick="javascript:CheckUncheckAll('seducation[]')"> <?=CHECK_UNCHECK_ALL?></label><?php }?>
                              </div>
                            </div>
                          </div>
                      <?php }?>

                      <?php if(SETHNICITY_SHOW) {?>
                          <div class="form-group row">
                            <label class="col-12 col-md-2"><?=SEEKING_ETHNICITY?></label>
                            <div class="col-12 col-md-10">
                              <div class="form-check">
                                <?=SEEKING_ETHNICITY_VALUE?>
                                <?php if(C_CHECK_ALL){?><label for="setnicitys"><input type="checkbox" id="setnicitys" OnClick="javascript:CheckUncheckAll('setnicity[]')"> <?=CHECK_UNCHECK_ALL?></label><?php }?>
                              </div>
                            </div>
                          </div>
                      <?php }?>

                      <?php if(SRELIGION_SHOW) {?>
                          <div class="form-group row">
                            <label class="col-12 col-md-2"><?=SEEKING_RELIGION?></label>
                            <div class="col-12 col-md-10">
                              <div class="form-check">
                                <?=SEEKING_RELIGION_VALUE?>
                                <?php if(C_CHECK_ALL){?><label for="sreligions"><input type="checkbox" id="sreligions" OnClick="javascript:CheckUncheckAll('sreligion[]')"> <?=CHECK_UNCHECK_ALL?></label><?php }?>
                              </div>
                            </div>
                          </div>
                      <?php }?>
                      
                  </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container-fluid my-1 text-center">
    <button class="btn btn-primary" type="submit" name="submit" value="<?=REGISTER?>" disabled><?=REGISTER?></button>
  </div>

    <div class="container-fluid my-1 text-center">
        <div class="col-12 text-center" id="captcha_container"></div>
        <style>
            #captcha_container div{
                margin: auto;
            }
        </style>
    </div>
  <script>IsReqPhoto();</script>
</form>