<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
<form action="<?=C_URL?>/advsearch.php" method="post" name=form>
<input type="hidden" name="l" value="<?=LANGUAGE?>">
<input type="hidden" name="a" value="s">
<div class="container-fluid my-1 text-center">
    <?=ADVANCED_SEARCH?>
</div>

<div class="container-fluid my-1">
  <div class="row">
    <div class="col-12 col-lg-8 offset-lg-2">
      
      <div class="card mb-1">
        <div class="card-body">

        <?php if(STATE_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="state"><?=STATE?></label>
            <div class="col-12 col-md-10">
              <input type="text" name="state" class="form-control">
            </div>
          </div>
        <?php }?>

        <?php if(COUNTRY_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="country"><?=COUNTRY?></label>
            <div class="col-12 col-md-10">
              <select name="country[]" class="form-control" multiple>
                <?=COUNTRY_VALUE?>
              </select>
            </div>
          </div>
        <?php }?>

        <?php if(CITY_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="city"><?=CITY?></label>
            <div class="col-12 col-md-10">
              <input type="text" name="city" class="form-control" maxlength="32">
            </div>
          </div>
        <?php }?>

        <?php if(ZIP_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="zip"><?=ZIP?></label>
            <div class="col-12 col-md-10">
              <input type="text" name="zip" class="form-control" maxlength="7">
            </div>
          </div>
        <?php }?>

        <?php if(DISTANCE_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="distance"><?=DISTANCE?></label>
            <div class="col-12 col-md-10">
              <input type="text" name="distance" class="form-control" maxlength="16"> <?=$w[722]?>
            </div>
          </div>
        <?php }?>

        <?php if(I_AM_A_SHOW) { ?>
           <div class="form-group row">
            <label class="col-12 col-md-2" for="sgender"><?=I_AM_A?></label>
            <div class="col-12 col-md-10">
              <select name="sgender" class="form-control">
                <?=I_AM_A_VALUE?>
              </select>
            </div>
          </div>
        <?php }?>

        <?php if(SEEKING_FOR_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="gender"><?=SEEKING_FOR?></label>
            <div class="col-12 col-md-10">
              <select name="gender[]" class="form-control" multiple>
                <?=SEEKING_FOR_VALUE?>
              </select>
            </div>
          </div>
        <?php }?>

        <?php if(ORIENTATION_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="orientation"><?=ORIENTATION?></label>
            <div class="col-12 col-md-10">
              <select name="orientation[]" class="form-control" multiple>
                <?=ORIENTATION_VALUE?>
              </select>
            </div>
          </div>
        <?php }?>

        <?php if(MARITAL_STATUS_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="marstat"><?=MARITAL_STATUS?></label>
            <div class="col-12 col-md-10">
              <select name="marstat[]" class="form-control" multiple>
                <?=MARITAL_STATUS_VALUE?>
              </select>
            </div>
          </div>
        <?php }?>

        <?php if(CHILD_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="child"><?=CHILD?></label>
            <div class="col-12 col-md-10">
              <select name="child[]" class="form-control" multiple>
                <?=CHILD_VALUE?>
              </select>
            </div>
          </div>
        <?php }?>

        <?php if(TYPE_OF_RELATION_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="purpose"><?=TYPE_OF_RELATION?></label>
            <div class="col-12 col-md-10">
              <select name="purpose[]" class="form-control" multiple>
                <?=TYPE_OF_RELATION_VALUE?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-12 col-md-10 offset-md-2">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="purpose_oper" id="purpose_oper0" value="0" checked>
                <label class="form-check-label" for="purpose_oper0">
                  <?=WOR?>
                </label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="purpose_oper" id="purpose_oper1" value="1" checked>
                <label class="form-check-label" for="purpose_oper1">
                  <?=WAND?>
                </label>
              </div>
            </div>
          </div>
        <?php }?>

        <?php if(INTERESTS_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="interests"><?=INTERESTS?></label>
            <div class="col-12 col-md-10">
              <select name="interests[]" class="form-control" multiple>
                <?=INTERESTS_VALUE?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-12 col-md-10 offset-md-2">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="interests_oper" id="interests_oper0" value="0" checked>
                <label class="form-check-label" for="interests_oper0">
                  <?=WOR?>
                </label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="interests_oper" id="interests_oper1" value="1" checked>
                <label class="form-check-label" for="interests_oper1">
                  <?=WAND?>
                </label>
              </div>
            </div>
          </div>
        <?php }?>

        <?php if(HOROSCOPE_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="horo"><?=HOROSCOPE?></label>
            <div class="col-12 col-md-10">
              <select name="horo[]" class="form-control" multiple>
                <?=HOROSCOPE_VALUE?>
              </select>
            </div>
          </div>
        <?php }?>

        <?php if(KEYWORDS_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="keywords"><?=KEYWORDS?></label>
            <div class="col-12 col-md-10">
              <input type="text" name="keywords" class="form-control">
            </div>
          </div>
        <?php }?>

        </div>
      </div>

    </div>

    <div class="col-12 col-lg-8 offset-lg-2 mt-1">
      <div class="card mb-1">
        <div class="card-body">
        
        <?php if(AGE_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="agef "><?=AGE?></label>
            <div class="col-12 col-md-10">
              <div class="form-inline">
                <input name="agef" class="form-control mr-1"> - 
                <input name="aget" class="form-control ml-1">
              </div>
            </div>
          </div>
        <?php }?>	

        <?php if(BODYTYPE_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="bodytype"><?=BODYTYPE?></label>
            <div class="col-12 col-md-10">
              <select name="bodytype[]" class="form-control" multiple>
                <?=BODYTYPE_VALUE?>
              </select>
            </div>
          </div>
        <?php }?>

        <?php if(HEIGHT_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="heightf "><?=HEIGHT?></label>
            <div class="col-12 col-md-10">
              <div class="form-inline">
                <select name="heightf" class="form-control mr-1"> 
                <?=HEIGHT_FROM?>
                </select> - 
                <select name="heightt" class="form-control ml-1"> 
                <?=HEIGHT_TO?>
                </select>
              </div>
            </div>
          </div>
        <?php }?>

        <?php if(WEIGHT_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="weightf "><?=WEIGHT?></label>
            <div class="col-12 col-md-10">
              <div class="form-inline">
                <select name="weightf" class="form-control mr-1"> 
                <?=WEIGHT_FROM?>
                </select> - 
                <select name="weightt" class="form-control ml-1"> 
                <?=WEIGHT_TO?>
                </select>
              </div>
            </div>
          </div>
        <?php }?>	

        

        <?php if(HAIR_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="hcolor"><?=HAIR?></label>
            <div class="col-12 col-md-10">
              <select name="hcolor[]" class="form-control" multiple>
                <?=HAIR_VALUE?>
              </select>
            </div>
          </div>
        <?php }?>

        <?php if(EYE_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="ecolor"><?=EYE?></label>
            <div class="col-12 col-md-10">
              <select name="ecolor[]" class="form-control" multiple>
                <?=EYE_VALUE?>
              </select>
            </div>
          </div>
        <?php }?>

        <?php if(ETHNICITY_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="etnicity"><?=ETHNICITY?></label>
            <div class="col-12 col-md-10">
              <select name="etnicity[]" class="form-control" multiple>
                <?=ETHNICITY_VALUE?>
              </select>
            </div>
          </div>
        <?php }?>

        <?php if(RELIGION_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="religion"><?=RELIGION?></label>
            <div class="col-12 col-md-10">
              <select name="religion[]" class="form-control" multiple>
                <?=RELIGION_VALUE?>
              </select>
            </div>
          </div>
            <div class="form-group row">
                <div class="col-12 col-md-10 offset-md-2">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="religion_oper" id="religion_oper0" value="0" checked>
                        <label class="form-check-label" for="religion_oper0">
                            <?=WOR?>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="religion_oper" id="religion_oper1" value="1" checked>
                        <label class="form-check-label" for="religion_oper1">
                            <?=WAND?>
                        </label>
                    </div>
                </div>
            </div>
        <?php }?>

        <?php if(SMOKE_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="smoke"><?=SMOKE?></label>
            <div class="col-12 col-md-10">
              <select name="smoke[]" class="form-control" multiple>
                <?=SMOKE_VALUE?>
              </select>
            </div>
          </div>
        <?php }?>

        <?php if(DRINK_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="drink"><?=DRINK?></label>
            <div class="col-12 col-md-10">
              <select name="drink[]" class="form-control" multiple>
                <?=DRINK_VALUE?>
              </select>
            </div>
          </div>
        <?php }?>

        <?php if(EDUCATION_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="education"><?=EDUCATION?></label>
            <div class="col-12 col-md-10">
              <select name="education[]" class="form-control" multiple>
                <?=EDUCATION_VALUE?>
              </select>
            </div>
          </div>
        <?php }?>

        <?php if(LANG_SHOW) { /* If is allow to show this section */ ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="language"><?=LANG?></label>
            <div class="col-12 col-md-10">
              <table>
                <tbody>
                <?php $k = 1;while (isset($wlng[$k])) {?>
                  <TR> 
                  <TD>
                    <?=$wlng[$k]?> <?=$checklang[$k]?> <?php $k++;?> <br>
                  </Td>
              
                  <TD>
                    <?php if(isset($wlng[$k])){?><?=$wlng[$k]?> <?=$checklang[$k]?> <?php $k++;}?> <br>
                  </Td>
                  </Tr>
              
              <?php }?>
              </tbody>
            </table>
            </div>
          </div>
          
          <div class="form-group row">
            <div class="col-12 col-md-10 offset-md-2">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="lang_oper" id="lang_oper0" value="0" checked>
                <label class="form-check-label" for="lang_oper0">
                  <?=WOR?>
                </label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="lang_oper" id="lang_oper1" value="1" checked>
                <label class="form-check-label" for="lang_oper1">
                  <?=WAND?>
                </label>
              </div>
            </div>
          </div>
        <?php }?>	

        </div>
      </div>
    </div>

    <div class="col-12 col-lg-8 offset-lg-2 mt-1">
      <div class="card mb-1">
        <div class="card-body">

        <?php if(USERID_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="id"><?=USERID?></label>
            <div class="col-12 col-md-10">
              <input type="text" name="id" class="form-control">
            </div>
          </div>
        <?php }?>

        <?php if(USERNAME_ALLOW && USERNAME_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="usr"><?=USERNAME?></label>
            <div class="col-12 col-md-10">
              <input type="text" name="usr" class="form-control">
            </div>
          </div>
        <?php }?>

        <?php if(FIRSTNAME_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="fname"><?=FIRSTNAME?></label>
            <div class="col-12 col-md-10">
              <input type="text" name="fname" class="form-control" maxlength="<?=C_FIRSTNB?>">
            </div>
          </div>
        <?php }?>

        <?php if(LASTNAME_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="lname"><?=LASTNAME?></label>
            <div class="col-12 col-md-10">
              <input type="text" name="lname" class="form-control" maxlength="<?=C_LASTNB?>">
            </div>
          </div>
        <?php }?>
        
        <?php if(PHOTO_SHOW || URL_SHOW || ICQ_SHOW || AIM_SHOW || PHONE_SHOW || ONLINE_USERS_ONLY_SHOW) {?>
          <div class="form-group row">
            <label class="col-12 col-md-2"><?=SEARCH_USERS_WITH?></label>
            <div class="col-12 col-md-10">
            <?php if(PHOTO_SHOW) { ?>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" name="photo" id="checkbox_photo">
                <label class="form-check-label" for="checkbox_photo">
                  <?=PHOTO?>
                </label>
              </div>
            <?php }?>

            <?php if(URL_SHOW) { ?>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" name="url" id="checkbox_url">
                <label class="form-check-label" for="checkbox_url">
                  <?=URL?>
                </label>
              </div>
            <?php }?>

            <?php if(ICQ_SHOW) { ?>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" name="icq" id="checkbox_icq">
                <label class="form-check-label" for="checkbox_icq">
                  <?=ICQ?>
                </label>
              </div>
            <?php }?>

            <?php if(AIM_SHOW) { ?>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" name="aim" id="checkbox_aim">
                <label class="form-check-label" for="checkbox_aim">
                  <?=AIM?>
                </label>
              </div>
            <?php }?>

            <?php if(PHONE_SHOW) { ?>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" name="phone" id="checkbox_phone">
                <label class="form-check-label" for="checkbox_phone">
                  <?=PHONE?>
                </label>
              </div>
            <?php }?>

            <?php if(ONLINE_USERS_ONLY_SHOW) { ?>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" name="online" id="checkbox_online">
                <label class="form-check-label" for="checkbox_online">
                  <?=ONLINE_USERS_ONLY?>
                </label>
              </div>
            <?php }?>
            </div>
          </div>
        <?php }?>

        <?php if(REGISTERED_IN_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="regin"><?=REGISTERED_IN?></label>
            <div class="col-12 col-md-10">
              <select name="regin" class="form-control">
                <?=REGISTERED_IN_VALUE?>
              </select>
            </div>
          </div>
        <?php }?>

        <?php if(USERTYPE_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="usertype"><?=USERTYPE?></label>
            <div class="col-12 col-md-10">
              <select name="usertype" class="form-control">
                <?=USERTYPE_VALUE?>
              </select>
            </div>
          </div>
        <?php }?>

        <?php if(SORT_BY_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="sortby"><?=SORT_BY?></label>
            <div class="col-12 col-md-10">
              <select name="sortby" class="form-control">
                <?=SORT_BY_VALUE?>
              </select>
            </div>
          </div>
        <?php }?>

        <?php if(RESULTS_ON_PAGE_SHOW) { ?>
          <div class="form-group row">
            <label class="col-12 col-md-2" for="step"><?=RESULTS_ON_PAGE?></label>
            <div class="col-12 col-md-10">
              <select name="step" class="form-control">
                <?=RESULTS_ON_PAGE_VALUE?>
              </select>
            </div>
          </div>
        <?php }?>

        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid my-1 text-center">
  <!--a href="search.php?l=<?=LANGUAGE?>" class=link2>[<?=SIMPLE_SEARCH?>]</a-->
  <button class="btn btn-primary" type="submit" name="submit" value="<?=SEARCH?>"><?=SEARCH?></button>
</div>

</form>