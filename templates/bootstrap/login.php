<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>

<form action="login.php" method="post" name="form">
<input type="hidden" name="l" value="<?=LANGUAGE?>">
<input type="hidden" name="p" value="s">
<input type="hidden" name="browser" value="">
<input type="hidden" name="goto" value="<?=GO_TO?>">

<div class="container-fluid my-1 text-center">
    <h2><?=LOGIN?></h2>
</div>

<div class="container-fluid my-1">
    <div class="row">
      	<div class="col-12 col-lg-6 offset-lg-3">
			<div class="card mb-1">
				<div class="card-body">

				<div class="form-group row">
					<label class="col-12 col-md-4" for="id"><?=LOGIN_NAME?></label>
					<div class="col-12 col-md-8">
						<input type="text" class="form-control" name="id">
					</div>
				</div>

        <div class="form-group row">
					<label class="col-12 col-md-4" for="password"><?=PASSWORD?></label>
					<div class="col-12 col-md-8">
						<input type="password" class="form-control" name="password">
					</div>
				</div>

        <?php if (VERIFY_ALLOW) {?>
        <div class="form-group row">
					<label class="col-12 col-md-4" for="verifyimage"><?=VERIFICATION?></label>
					<div class="col-12 col-md-8">
            <div>
              <img src=img.php border=0>
            </div>
            <div>
              <input type="text" class="form-control" name="verifyimage">
            </div>
					</div>
				</div>
        <?php }?>

				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid my-1 text-center">
    <button class="btn btn-primary" type="submit" name="submit" value="<?=ENTER?>"><?=ENTER?></button>
</div>

<div class="container-fluid my-3 text-center">
  <div class="row">
    <div class="col-12 col-md-3 offset-md-3">
      <a href="add.php?l=<?=LANGUAGE?>"><?=REGISTER?></a>
    </div>
    <div class="col-12 col-md-3">
      <a href="remind.php?l=<?=LANGUAGE?>"><?=FORGOT_PASSWORD?></a>
    </div>
  </div>
</div>

</form>

<script language="javascript">
    document.form.browser.value = navigator.platform + ' ' + navigator.userAgent + ' ' + Intl.DateTimeFormat().resolvedOptions().timeZone;
</script>