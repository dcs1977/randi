<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
<link type="text/css" rel="stylesheet" href="<?=C_URL?>/templates/<?=C_TEMP?>/css/lightgallery.min.css" />
<link type="text/css" rel="stylesheet" href="<?=C_URL?>/templates/<?=C_TEMP?>/css/lg-transitions.min.css" />

<script language="JavaScript">
<!--
function ins(prop,type)
{
	var input = document.form.message;
	var mysel;var text;
	if (window.getSelection) mysel = window.getSelection();
	else if (document.selection) mysel = document.selection.createRange().text;
	else if (document.getSelection)	mysel = document.getSelection();
	b = (type == 0) ? '['+prop+']' : prop;
	e = (type == 0) ? '[/'+prop+']' : '';
  if (input.createTextRange)
  {
	  var text;
    input.focus(input.caretPos);
    input.caretPos = document.selection.createRange().duplicate();
    input.caretPos.text = (input.caretPos.text.length>0) ? b + input.caretPos.text + e : b + mysel + e;
   }
   else input.value += b + mysel + e;
}
function formCheck(form) {
  if (form.message.value == "") {
    alert("<?=ENTER_MESSAGE?>");
    return false;
  }

    if (captchaContainer !== null){
        return true;
    }

    $('#replyForm button[type="submit"]').prop('disabled', true);

    var form = $('#replyForm')[0];
    var formData = new FormData(form);

    $.ajax({
        method: 'POST',
        url: "index.php",
        processData: false,
        contentType: false,
        dataType: "json",
        data: formData
    })
        .done(function(msg){
            console.log(msg);
            if (msg.status == 200){
                location = msg.href;
            } else if (msg.status == 500){
                const nodeBadge = document.querySelector('.grecaptcha-badge');
                if (nodeBadge) {
                    document.body.removeChild(nodeBadge.parentNode);
                }

                const scriptSelector = 'script[src=\'https://www.google.com/recaptcha/api.js?render=<?=RECAPTCHA_PUBLIC_KEY?>\']';
                const script = document.querySelector(scriptSelector);
                if (script) {
                    script.remove();
                }

                captchaContainer = grecaptcha.render('captcha_container', {
                    'sitekey' : '<?=OLD_RECAPTCHA_PUBLIC_KEY?>',
                    'callback' : function(response) {
                        $('#replyForm button[type="submit"]').prop('disabled', false);
                    }
                });

                $('#recaptcha_token').remove();
                return false;
            }
        });
    return false;
}
// -->
</script>

<script>
    var captchaContainer = null;

    grecaptcha.ready(function () {
        grecaptcha.execute('<?=RECAPTCHA_PUBLIC_KEY?>', {action: 'reply'}).then(function(token) {
            $('#replyForm').prepend('<input id="recaptcha_token" type="hidden" name="token" value="' + token + '">');
            $('#replyForm').prepend('<input type="hidden" name="action" value="reply">');
            $('#replyForm button[type="submit"]').prop('disabled', false);
        });
        setInterval(function () {
            grecaptcha.execute('<?=RECAPTCHA_PUBLIC_KEY?>', { action: 'reply' }).then(function (token) {
                if ($("#recaptcha_token").length) {
                    $("#recaptcha_token").val(token);
                }
            });
        }, 60000);
    });
</script>

<form id="replyForm" action="index.php" method="post" name=form OnSubmit="return formCheck(this)" enctype="multipart/form-data">
<input class=input type=hidden name="l" value="<?=LANGUAGE?>">
<input class=input type=hidden name="id" value="<?=(SENDER=='HE'?FROMID:TOID)?>">
<input class=input type=hidden name="a" value="s">

<div class="container-fluid mt-1 md-1">
	<div class="row">
		<div class="col-12 col-md-6 offset-md-3">
			<div class="card mb-1">
        <div class="card-header">
					<div class="row">
						<div class="col-12 text-center">
            <?php if(!FROMID) { /* If message from administrator */?>
              <B><?=ADMINISTRATOR?></B>
            <?php } else { /* Message from user*/ ?>
              <a href="<?=GenLink('UserProfile',array(LANGUAGE,(SENDER=='HE'?FROMID:TOID)))?>"><?=NAME?></a></span>
            <?php }?>
						</div>
					</div>					
				</div>
        <div class="card-body">
					<div class="row">
						<div class="col-12">
              <table class="table table-striped">
                <tbody>
                  <tr>
                    <th>
                    <?=DATE?>
                    </th>
                    <td>
                    <?=SENDTIME?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                    <?=MESSAGE?>
                    </th>
                    <td>
                    <?=MESSAGE_VALUE?>
                    </td>
                  </tr>
                  <?php  if(ATTACHMENT){?>
                  <tr>
                    <th>
                    <?=$w[87]?>
                    </th>
                    <td class="text-center">
                        <a class="lightgallery-message" href="<?=ATTACHMENT?>">
                            <img class="img-fluid" src="<?=ATTACHMENT?>">
                        </a>
                    </td>
                  </Tr>
                  <?php  } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <?php  if(SENDER=='HE'){?>
        <div class="card-footer">
          <div class="row">
            <div class="col-12 text-center">
              <button class="btn btn-warning" type="button" value="<?=DELETE_MESSAGE?>" onclick="location.href='<?=C_URL?>/members/index.php?l=<?=LANGUAGE?>&a=d&del[]=<?=MESSAGE_ID?>'"><?=DELETE_MESSAGE?></button>
                &nbsp;
              <!--button class="btn btn-default" type="button" value="<?=READ_NEXT_UNREAD?>" onclick="location.href='<?=C_URL?>/members/index.php?l=<?=LANGUAGE?>&a=mnext'"><?=READ_NEXT_UNREAD?></button-->
            </div>
          </div>
        </div>
        <?php }?>
      </div>

      <div class="card mb-1">
        <div class="card-body">

            <div class="form-group row email_again">
                <label class="col-12 col-md-2" for="state">Ha látod ezt a mezőt, akkor kérlek hagyd üresen!
                    <div class="col-12 col-md-10">
                        <input type="text" name="subject-honeypot" class="form-control" autocomplete="off">
                    </div>
            </div>

          <div class="form-group row">
            <label class="col-12 col-md-2" for="message"><?=MESSAGER?></label>
            <div class="col-12 col-md-10">
              <div>
                <textarea class="form-control" name="message" id="message" rows="4"><?=((SENDER=='HE')?MESSAGE_ANSWER:'')?></textarea>
              </div>
                <div class="text-right"><small><span style="color: red">*</span> Kötelező kitölteni.</small></div>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-12 col-md-2" for="image"><?=$w[87]?></label>
            <div class="col-12 col-md-10">
              <input type="file" name="image" class="form-control">
            </div>
          </div>

          <div class="form-group row">
            <div class="col-12 col-md-10 offset-md-2">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" name="confirm" id="confirm"<?=CONFIRM?>>
                <label class="form-check-label" for="confirm">
                  <?=NOTIFY_ME?>
                </label>
              </div>
            </div>
          </div>

        </div>
        <div class="card-footer">
          <div class="row">
            <div class="col-12 text-center">
              <button class="btn btn-primary" type="submit" value="<?=SEND_MESSAGE?>" disabled="disabled"><?=SEND_MESSAGE?></button>
            </div>
          </div>

            <div class="row">
                <div class="col-12 text-center" id="captcha_container"></div>
                <style>
                    #captcha_container div{
                        margin: auto;
                    }
                </style>
            </div>

        </div>
      </div>

    </div>
  </div>
</div>

</form>

<div class="container-fluid mt-1 md-1">
	<div class="row">
    <div class="col-12 col-md-6 offset-md-3 text-center">
      <h3><?=$w['96']?></h3>
    </div>
  </div>
  
  <div class="row">
    <div id="lightgallery" class="col-12 col-md-6 offset-md-3">
      <?php foreach ($meslog as $mes) {?>
        <div class="row mb-1">
          <?php if($mes['fromid']!=get_info('m')){?>
            <div class="col-2 text-center"></div>
          <?php }?>
          <div class="col-10">
            <div class="card">
                <div class="card-header">
                    <?php if($mes['fromid']!=get_info('m')){?>
                        <strong><?=NAME?></strong>
                    <?php } else {?>
                        <strong><?=MyName()?></strong>
                    <?php } ?>
                </div>
              <div class="card-body">
                <p>
                  <?=$mes['message']?>
                </p>
                <?php if ($mes['attachment'] != ''){?>
                <p class="text-center">
                    <a class="lightgallery-item" href="<?=$mes['attachment']?>">
                        <img class="img-fluid" src="<?=$mes['attachment']?>">
                    </a>
                </p>
                <?php }?>
              </div>
              <div class="card-footer text-left">
                <small><?=$mes['sendtime']?></small>
              </div>
            </div>
          </div>
          <?php if($mes['fromid']==get_info('m')){?>
            <div class="col-2 text-center"></div>
          <?php } ?>
        </div>
      <?php }?>
    </div>
  </div>
  
</div>

<script src="<?=C_URL?>/templates/<?=C_TEMP?>/js/lightgallery-all.min.js"></script>
<script src="<?=C_URL?>/templates/<?=C_TEMP?>/js/jquery.mousewheel.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#lightgallery").lightGallery({
            download: false,
            share: false,
            selector: '.lightgallery-item'
        });

        $(".lightgallery-message").lightGallery({
            download: false,
            share: false,
            selector: 'this'
        });

        $(document).on('contextmenu', '.lightgallery-item', function(e) {
            return false;
        });

        $(document).on('contextmenu', '.lightgallery-message', function(e) {
            return false;
        });

        $(document).on('mousedown', '.lightgallery-item', function(e) {
            return false;
        });

        $(document).on('mousedown', '.lightgallery-message', function(e) {
            return false;
        });

        $(document).on('mouseup', '.lightgallery-item', function(e) {
            return false;
        });

        $(document).on('mouseup', '.lightgallery-message', function(e) {
            return false;
        });

        $(document).on('contextmenu', '.lg-image', function(e) {
            return false;
        });
    });

</script>