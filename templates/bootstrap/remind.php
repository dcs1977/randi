<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>

<script language="JavaScript">
<!--
function formCheck(form) {
if (form.id.value == "")
{alert("<?=$w[164]?>");return false;}

if (document.form.submit.action != "") {
document.form.submit.disabled=1;}
}
// -->
</script>
<form action="remind.php" method="post" name=form OnSubmit="return formCheck(this)">
<input class=input type=hidden name="l" value="<?=LANGUAGE?>">
<input class=input type=hidden name="p" value="s">
<input type="hidden" name="fingerprint" id="fingerprint" value="">

<div class="container-fluid my-1 text-center">
    <?=FORGOT_PASSWORD?>
</div>

<div class="container-fluid my-1">
  <div class="row">
    <div class="col-12 col-lg-8 offset-lg-2">
      
      <div class="card mb-1">
        <div class="card-body">
          
          <div class="form-group row">
            <label class="col-12 col-md-2" for="state"><?=EMAIL?></label>
            <div class="col-12 col-md-10">
              <input type="text" name="id" class="form-control" maxlength="64">
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid my-1 text-center">
  <button class="btn btn-primary" type="submit" name="submit" value="<?=SEND_PASSWORD?>"><?=SEND_PASSWORD?></button>
</div>

</form>

<form action="remind.php" method="post" name=form OnSubmit="return formCheck(this)">
<input class=input type=hidden name="l" value="<?=LANGUAGE?>">
<input class=input type=hidden name="p" value="u">
<input type="hidden" name="fingerprint" id="fingerprint" value="">

<div class="container-fluid my-1 text-center">
    <?=FORGOT_USERNAME?>
</div>

<div class="container-fluid my-1">
  <div class="row">
    <div class="col-12 col-lg-8 offset-lg-2">
      
      <div class="card mb-1">
        <div class="card-body">
          
          <div class="form-group row">
            <label class="col-12 col-md-2" for="state"><?=EMAIL?></label>
            <div class="col-12 col-md-10">
              <input type="text" name="id" class="form-control" maxlength="64">
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid my-1 text-center">
  <button class="btn btn-primary" type="submit" name="submit" value="<?=SEND_EMAIL?>"><?=SEND_EMAIL?></button>
</div>

</form>

<form action="check.php" method="get" name=form>
<input class=input type=hidden name="l" value="<?=LANGUAGE?>">

<div class="container-fluid my-1 text-center">
    <?=ACTIVATION_CODE?>
</div>

<div class="container-fluid my-1">
  <div class="row">
    <div class="col-12 col-lg-8 offset-lg-2">
      
      <div class="card mb-1">
        <div class="card-body">
          
          <div class="form-group row">
            <label class="col-12 col-md-2" for="state"><?=CODE?></label>
            <div class="col-12 col-md-10">
              <input type="text" name="code" class="form-control" maxlength="10">
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid my-1 text-center">
  <button class="btn btn-primary" type="submit" name="submit" value="<?=$w[170]?>"><?=$w[170]?></button>
</div>

</form>