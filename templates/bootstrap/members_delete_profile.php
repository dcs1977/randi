<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>

<div class="container-fluid my-1">
  <div class="row">
    <div class="col-12 col-lg-8 offset-lg-2">
      <div class="card mb-1">
        <div class="card-body text-center">
        <?=ARE_YOU_SURE?>
        </div>
      </div>
    </div>
  </div>
</div>

<form action="index.php" method="post" autocomplete="off">
<input type="hidden" name="l" value="<?=LANGUAGE?>">
<input type="hidden" name="a" value="y">

<div class="container-fluid my-1">
  <div class="row">
    <div class="col-12 col-lg-8 offset-lg-2">
      <div class="card mb-1">
        <div class="card-body">
          
          <div class="row">

            <div class="col-5">
              <div class="form-group row">
                <label class="col-12" for="deleteid"><?=ID?></label>
                <div class="col-12">
                  <input class="form-control" name="deleteid" type="text" value="" autocomplete="off">
                </div>
              </div>
            </div>

            <div class="col-5">
              <div class="form-group row">
                <label class="col-12" for="deletepassword"><?=PASSWORD?></label>
                <div class="col-12">
                  <input class="form-control" name="deletepassword" type="password" value="" autocomplete="off">
                </div>
              </div>
            </div>

            <div class="col-2">
              <div class="form-group row">
                <label class="col-12">&nbsp;</label>
                <div class="col-12">
                  <button type="submit" class="btn btn-danger btn-block"><?=REMOVE_PROFILE?></button>
                </div>
              </div>
            </div>

          </div>

        </div>
      </div>
    </div>
  </div>
</div>

</form>