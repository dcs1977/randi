<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
<link type="text/css" rel="stylesheet" href="<?=C_URL?>/templates/<?=C_TEMP?>/css/lightgallery.min.css" />
<link type="text/css" rel="stylesheet" href="<?=C_URL?>/templates/<?=C_TEMP?>/css/lg-transitions.min.css" />

<div class="container-fluid mt-1 md-1">
  <div class="row">
    <div class="col-12 col-md-8 offset-md-2">
      <h3 class="text-center"><?=USERNAME_VALUE?></h3>
        <div class="row">
          <div class="col-4 text-center">
          <?php if (SHOW_PHOTOS) { /* If allow to show photos - Begin */ ?>
              <div id="lightgallery">
                <?php while(isset($pictures[$p])) { /* Show Photos - Begin */ ?>
                  <a class="lightgallery-item" href="<?=$pictures[$p].ShowPictureBigger().'&picnum='.$p?>"><img src="<?=$pictures[$p]?>" class="img-fluid img-thumbnail mb-2"></a>
                <?php $p++;if(C_VIEW_PHOTOS_SHOW == $p) break;} /* Show Photos - End */ ?>
              </div>
          <?php } else { /* If allow to show photos - End */ ?>
            &nbsp; 
          <?php }?>
          <p><?=TOTAL_PHOTOS?>: <strong><?=PICTURES_NUMBER?></strong></p>
          <?php if(IS_NOT_CHECKED) {?>
            <p><?=IS_NOT_CHECKED?></p>
          <?php }?>
          <?php if(PICTURES_NUMBER > C_VIEW_PHOTOS_SHOW) {?>
            <p><a href="<?=$photos[$p]?>"><?=SEE_ANOTHER?></a></p>
          <?php }$p=0;?>
          <?php if(PRIVATE_PHOTOS_NUM){?>
            <p>
              <a href="<?=C_URL?>/private_photos.php?l=<?=LANGUAGE?>&id=<?=ID?>"><?=PRIVATE_PHOTOS_TOTAL?></a>
            </p>
          <?php }?>
          <?php /*if(SLIDESHOW_SHOW) {*/?>
            <!--p>
              <a href="javascript:slide_win('<?=C_URL?>/slideshow.php?l=<?=LANGUAGE?>&id=<?=ID?>','_blank');" class="btn btn-light"><?=SLIDESHOW?></a>
            </p-->
          <?php /*}*/?>
          </div>

          <div class="col-8">
            <div class="card">
              <div class="card-header text-center">
              <?php if(SEND_KISS && !BANNED) {?>
                    <a class="btn btn-light mb-1" href="javascript:open_win('<?=C_URL?>/sendk.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>','wink');"><?=SEND_KISS_MESSAGE?></a>
              <?php }?>
              <?php if(SEND_SHOW && !BANNED) {?>
                    <a class="btn btn-light mb-1" href="<?=C_URL?>/send.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>"><?=SEND_MESSAGE?></a>
              <?php }?>
              <?php if(MAIL_SHOW && !BANNED) {?>
                    <a class="btn btn-light mb-1" href="<?=C_URL?>/mail.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>"><?=MAIL_MESSAGE?></a>
              <?php }?>
              <?php if(BED_SHOW && !BANNED) {?>
                    <a class="btn btn-light mb-1" href="javascript:open_win('<?=C_URL?>/bedroom.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>','bed');"><?=BED_MESSAGE?></a>
              <?php }?>
              <?php if(BAN_SHOW) {?>
                <?php if(ON_MYBANLIST){?>
                    <a class="btn btn-light mb-1" href="javascript:open_win('<?=C_URL?>/unban.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>','unban');"><?=UNBAN_MESSAGE?></a>
                <?php } else {?>
                  <a class="btn btn-light mb-1" href="javascript:open_win('<?=C_URL?>/ban.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>','ban');"><?=BAN_MESSAGE?></a>
                <?php }?>
              <?php }?>
              <?php if(BAD_SHOW) {?>
                    <a class="btn btn-light mb-1" href="javascript:open_win('<?=C_URL?>/bad.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>','bad');"><?=BAD_MESSAGE?></a>
              <?php }?>
              <?php if(MyAccess() < 0) echo $w[152];?>
              </div>
              <div class="card-body">
                <div class="row">
						      <div class="col-12">
                    <table class="table table-striped">
                      <tbody>
                      <?php if(BANNED){?>
                        <?php if(ON_MYBANLIST){?>
                          <tr>
                            <td>
                            <div class="text-center" style="color: yellow; font-weight: bold; background-color: blue"><?=ON_BANLIST?></div>
                            </td>
                          </tr>
                        <?php } else{?>
                          <tr>
                            <td>
                              <div class="text-center" style="color: yellow; font-weight: bold; background-color: blue">Ez a felhasználó bannolt téged</div>
                            </td>
                          </tr>
                        <?php }?>
                      <?php }?>

                      <?php if(HIDDEN){?>
                        <tr>
                          <td>
                            <div class="text-center" style="color: yellow; font-weight: bold; background-color: blue">Ez a felhasználó rejtetten van jelen</div>
                          </td>
                        </tr>
                      <?php }?>

                      <?php if(DESCR_SHOW && !BANNED && !HIDDEN) {?>
                        <tr>
                          <td>
                              <div class="row">
                                  <div class="col-12 col-md-6">
                                      <b><?=DESCR?></b>
                                  </div>
                                  <div class="col-12 col-md-6">
                                      <?=DESCR_VALUE?>
                                  </div>
                              </div>
                          </td>
                        </tr>
                      <?php }?>

                      <?php if (COUPON !== FALSE) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b>Kuponkód:</b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <form method="post">
                                              <div class="input-group">
                                                  <input class="form-control" type="text" name="kupon" value="<?=COUPON?>">
                                                  <div class="input-group-append">
                                                      <button class="btn btn-primary" type="submit">OK</button>
                                                  </div>
                                              </div>
                                          </form>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>
                          
                      <?php if(COUNTRY_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=COUNTRY?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=COUNTRY_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>
                          
                      <?php if(STATE_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=STATE?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=STATE_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(ZIP_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=ZIP?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=ZIP_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(CITY_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=CITY?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=CITY_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>                          

                      <?php if(AGE_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=AGE?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=AGE_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(GENDER_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=GENDER?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=GENDER_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(ORIENTATION_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=ORIENTATION?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=ORIENTATION_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(BODYTYPE_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=BODYTYPE?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=BODYTYPE_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(PURPOSES_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=PURPOSES?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=PURPOSES_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(EMAIL_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=EMAIL?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=EMAIL_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(URL_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=URL?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?php if(IsAccess(C_URLSH)) {?>
                                              <a href="<?=URL_VALUE?>"><?=URL_VALUE?></a>
                                          <?php }else{?>
                                              <?=URL_VALUE?>
                                          <?php }?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(ICQ_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=ICQ?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?php if(IsAccess(C_ICQSH)) {?>
                                              <?=ICQ_VALUE?> <img src="http://web.icq.com/whitepages/online?icq=<?=ICQ_VALUE?>&img=5">
                                          <?php }else{?>
                                              <?=ICQ_VALUE?>
                                          <?php }?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(AIM_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=AIM?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=AIM_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(PHONE_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=PHONE?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=PHONE_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(ADDRESS_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=ADDRESS?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=ADDRESS_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(MARSTAT_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=MARSTAT?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=MARSTAT_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(LINK_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=LINKED_PROFILE?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <a class=link3 href="<?=GenLink('UserProfile',array(LANGUAGE,LINK_VALUE))?>"><?=LINKED_PROFILE_TXT?></a>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(CHILD_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=CHILD?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=CHILD_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(HEIGHT_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=HEIGHT?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=HEIGHT_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(WEIGHT_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=WEIGHT?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=WEIGHT_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(HCOLOR_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=HCOLOR?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=HCOLOR_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(ECOLOR_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=ECOLOR?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=ECOLOR_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(ETHNICITY_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=ETHNICITY?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=ETHNICITY_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(RELIGION_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=RELIGION?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=RELIGION_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(SMOKE_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=SMOKE?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=SMOKE_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(DRINK_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=DRINK?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=DRINK_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>
                          
                      <?php if(HOROSCOPE_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=HOROSCOPE?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=HOROSCOPE_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>
                          
                      <?php if(EDUCATION_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=EDUCATION?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=EDUCATION_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(LANG_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=KNOWN_LANGUAGES?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=LANG_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(JOB_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=JOB?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=JOB_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(HOBBY_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=HOBBY?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=HOBBY_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(INTERESTS_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=INTERESTS?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=INTERESTS_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(SGENDER_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=SGENDER?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=SGENDER_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(SCOUNTRY_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=SCOUNTRY?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=SCOUNTRY_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(SMARSTAT_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=SMARSTAT?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=SMARSTAT_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(SCHILD_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=SCHILD?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=SCHILD_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(SHCOLOR_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=SHCOLOR?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=SHCOLOR_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(SECOLOR_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=SECOLOR?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=SECOLOR_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(SSMOKE_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=SSMOKE?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=SSMOKE_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(SDRINK_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=SDRINK?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=SDRINK_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(SEDUCATION_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=SEDUCATION?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=SEDUCATION_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(SETHNICITY_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=SETHNICITY?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=SETHNICITY_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(SRELIGION_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=SRELIGION?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=SRELIGION_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(SAGE_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=SAGE?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=SAGEF_VALUE?> - <?=SAGET_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(SHEIGHT_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=SHEIGHT?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=SHEIGHTF_VALUE?> - <?=SHEIGHTT_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(SWEIGHT_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=SWEIGHT?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=SWEIGHTF_VALUE?> - <?=SWEIGHTT_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(REGDATE_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=REGDATE?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=REGDATE_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(ACCDATE_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=ACCDATE?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=ACCDATE_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(USERTYPE_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=USERTYPE?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=USERTYPE_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(ONLINESTAT_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=ONLINESTAT?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=ONLINESTAT_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(FAVORITES_SHOW && !BANNED && !HIDDEN && 1==2) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=USER_FAVORITES?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=FAVORITES_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(VIEWS_SHOW && VIEWS_VALUE && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=VIEWS?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=VIEWS_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(POPULARITY_SHOW && POPULARITY_VALUE && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=POPULARITY?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?=POPULARITY_VALUE?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <?php if(AUDIO_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=AUDIO?></b>
                                      </div>
                                      <div class="col-12 col-md-6">
                                          <?php if(IsAV(AUDIO_VALUE)) {?>
                                              <script language="JavaScript">
                                                  <!--
                                                  var soundFile = "<?=C_URL?>/members/uploads/<?=AUDIO_VALUE?>";
                                                  if(navigator.plugins.length>0){
                                                      document.write('<embed src="'+soundFile+'" autostart="<?=AUTOSTART?>" loop="<?=C_AUDIO_LOOP?>" <?=audio_player(C_AUDIO_PLAYER,'0');?>></embed>');}
                                                  else {
                                                      document.write('<embed src="'+soundFile+'" autostart="<?=AUTOSTART?>" loop="<?=C_AUDIO_LOOP?>" <?=audio_player(C_AUDIO_PLAYER,'1');?>></embed>');}
                                                  //-->
                                              </script>
                                              <noscript><embed src="<?=C_URL?>/members/uploads/<?=AUDIO_VALUE?>" autostart="<?=AUTOSTART?>" loop="<?=C_AUDIO_LOOP?>"></embed></noscript>
                                              <noembed><bgsound src="<?=C_URL?>/members/uploads/<?=AUDIO_VALUE?>" loop="<?=C_AUDIO_LOOP?>" /></noembed>
                                          <?php  } else { ?>
                                              <OBJECT id='mediaPlayer' width="320" height="285"
                                                      classid='CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95'
                                                      codebase='http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701'
                                                      standby='Loading Microsoft Windows Media Player components...' type='application/x-oleobject'>
                                                  <param name='fileName' value="<?=C_URL?>/members/uploads/<?=AUDIO_VALUE?>">
                                                  <param name='animationatStart' value='true'>
                                                  <param name='transparentatStart' value='true'>
                                                  <param name='autoStart' value="<?=AUTOSTART?>">
                                                  <param name='showControls' value="true">
                                                  <param name='loop' value="<?=C_AUDIO_LOOP?>">
                                                  <EMBED type='application/x-mplayer2'
                                                         pluginspage='http://microsoft.com/windows/mediaplayer/en/download/'
                                                         id='mediaPlayer' name='mediaPlayer' displaysize='4' autosize='-1'
                                                         bgcolor='darkblue' showcontrols="true" showtracker='-1'
                                                         showdisplay='0' showstatusbar='-1' videoborder3d='-1' width="320" height="285"
                                                         src="<?=C_URL?>/members/uploads/<?=AUDIO_VALUE?>" autostart="<?=AUTOSTART?>" designtimesp='5311' loop="<?=C_AUDIO_LOOP?>">
                                                  </EMBED>
                                              </OBJECT>
                                          <?php }?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?><!--1-->

                      <?php if(RATE_SHOW && !BANNED && !HIDDEN) {?>
                          <tr>
                              <td>
                                  <div class="row">
                                      <div class="col-12 col-md-6">
                                          <b><?=RATE?></b>
                                      </div>
                                      <div class="col-12 col-md-6" style="line-height: 2em;">
                                          <?php  for($i=1;$i<=10;$i++) { ?>
                                              <a href="javascript:open_win('<?=C_URL?>/rate.php?l=<?=LANGUAGE?>&id=<?=ID?>&rate=<?=$i?>','rate');"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/rate_<?=$i?>.gif" width=30 height=30 border=0></a>
                                          <?php }?>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      <?php }?>

                      <tr>
                          <td>
                              <div class="row">
                                  <div class="col-12 col-md-6">
                                      <b>#ID:</b>
                                  </div>
                                  <div class="col-12 col-md-6">
                                      <?=ID?>
                                  </div>
                              </div>
                          </td>
                      </tr>

                      </tbody>
                    </table>                  
                  </div>
                </div>
              </div>
              <div class="card-footer text-center">
                <?php if(SEND_KISS && !BANNED) {?>
                      <a class="btn btn-light mb-1" href="javascript:open_win('<?=C_URL?>/sendk.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>','wink');"><?=SEND_KISS_MESSAGE?></a>
                <?php }?>
                <?php if(SEND_SHOW && !BANNED) {?>
                      <a class="btn btn-light mb-1" href="<?=C_URL?>/send.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>"><?=SEND_MESSAGE?></a>
                <?php }?>
                <?php if(MAIL_SHOW && !BANNED) {?>
                      <a class="btn btn-light mb-1" href="<?=C_URL?>/mail.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>"><?=MAIL_MESSAGE?></a>
                <?php }?>
                <?php if(BED_SHOW && !BANNED) {?>
                      <a class="btn btn-light mb-1" href="javascript:open_win('<?=C_URL?>/bedroom.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>','bed');"><?=BED_MESSAGE?></a>
                <?php }?>
                <?php if(BAN_SHOW) {?>
                  <?php if(ON_MYBANLIST){?>
                      <a class="btn btn-light mb-1" href="javascript:open_win('<?=C_URL?>/unban.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>','unban');"><?=UNBAN_MESSAGE?></a>
                  <?php } else {?>
                    <a class="btn btn-light mb-1" href="javascript:open_win('<?=C_URL?>/ban.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>','ban');"><?=BAN_MESSAGE?></a>
                  <?php }?>
                <?php }?>
                <?php if(BAD_SHOW) {?>
                      <a class="btn btn-light mb-1" href="javascript:open_win('<?=C_URL?>/bad.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>','bad');"><?=BAD_MESSAGE?></a>
                <?php }?>
                <?php if(MyAccess() < 0) echo $w[152];?>
              </div>
            </div>

            <?php if(SHOW_VIEWERS) {?>
              <div class="card mt-2">
                <div class="card-header text-center"><strong><?=WHO_SEEN?></strong></div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-12">
                      <?php if(defined("NOBODY_SEEN_PROFILE")) {?>
                      	<div class="text-center"><?=NOBODY_SEEN_PROFILE?></div>
                      <?php } else {?>
                        <table class="table table-striped">
                          <thead>
                          <tr align="center" bgcolor="<?=COLORH?>" class=mes>
                            <th><?=USERNAME?></th>
                            <th><?=GENDER?></th>
                            <th><?=AGE?></th>
                            <th><?=DATE?></th>
                          </tr>
                          </thead>
                          <tbody>
                          <?php while(isset($visid[$p])) {?>     
                            <tr>
                              <td><a href="<?=GenLink('UserProfile',array(LANGUAGE,$visid[$p]))?>"><?=$names[$p]?></a></td>
                              <td><?=$genders[$p]?></td>
                              <td><?=$ages[$p]?></td>
                              <td><?=$visdate[$p]?></td>
                            </tr>
                          <?php $p++;}$p=0; /* If popular users exist - Begin */ ?>
                          </tbody>
                        </table>
                      <?php } ?>
                    </div>
                  </div>
                </div>
                <?php if(!defined("NOBODY_SEEN_PROFILE")) {?>
                  <div class="card-footer text-center">
                    <?=PAGES?>
                  </div>
                <?php }?>
              </div>
            <?php }?>

            <?php if(SHOW_COMMENTS) {?>

              <?php //if(MyID() == ID){?>
              <script language="JavaScript">
              <!--
              function ConfirmDeleteComment(CommentId)
              {
                if(confirm('<?=WARNING?>'))
                {
                  open_win('<?=C_URL?>/comments.php?l=<?=LANGUAGE?>&cid='+CommentId+'&remove','remove');
                }
              }
              // -->
              </script>
              <?php //}?>

              <div class="card mt-2">
                <div class="card-header text-center"><strong><?=NO_COMMENTS?></strong></div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-12">
                      <?php if(defined("NO_COMMENTS")) {?>
                      	<div class="text-center"><?=NO_COMMENTS?></div>
                      <?php } else {?>
                        <?php $p=0;while(isset($cfromid[$p])) {?>
                        <table class="table table-striped">
                          <tbody>
                            <tr>
                              <td colspan="2" class="text-center"><?=$cdate[$p]?></td>
                            </tr>

                            <tr>
                              <td class="text-center"><?=$ccomment[$p]?></td>
                              <td class="text-center">
                                <div><?=$cphotos[$cfromid[$p]]?></div>
                                <?php if($cfromid[$p]){?><a href="<?=GenLink('UserProfile',array(LANGUAGE,$cfromid[$p]))?>"><?=$cnames[$cfromid[$p]]?></a><?php } else {?><?=$cnames[$cfromid[$p]]?><?php }?>
                              </td>
                            </tr>

                            <?php if(!empty($creply[$p])){?>
                              <tr>
                                <td class="text-center" style='color:red'><?=$creply[$p]?></td>
                                <td class="text-center" valign=top><?=$rdate[$p]?></td>
                              </tr>
                            <?php }?>

                            <?php if(MyID() == ID OR MyID() == $cfromid[$p]){?>
                              <tr>
                                <td class="text-center" colspan=2>
                                <?php if(SHOW_REPLY_LINK AND MyID() == ID){?><a class=link2 href="javascript:open_win('<?=C_URL?>/comments.php?l=<?=LANGUAGE?>&cid=<?=$cid[$p]?>&reply','reply');"><?=REPLY?></a><?php }?> &nbsp; 
                                <?php if(SHOW_REMOVE_LINK){?><a class=link2 href="javascript:ConfirmDeleteComment('<?=$cid[$p]?>');"><?=DELETE?></a><?php }?> &nbsp;
                                </td>
                              </tr>
                            <?php }?>

                          </tbody>
                        </table>
                        <?php $p++;}$p=0; /* If popular users exist - Begin */ ?>
                      <?php } ?>
                    </div>
                  </div>
                </div>
                <?php if(!defined("NO_COMMENTS") && CPAGES) {?>
                  <div class="card-footer text-center">
                    <?=CPAGES?>
                  </div>
                <?php }?>
              </div>
            <?php }?>

          </div>
        </div>
    </div>
  </div>
</div>

<?php if(SHOW_COMMENTS_FORM){?>
<script language="JavaScript">
<!--
function formCheck(form) {
if (form.message.value=="") {
    alert("<?=MESSAGE_ERROR?>");
    return false;
}
if (document.form.submit.action != "") {
document.form.submit.disabled=1;}
}
// -->
</script>
<script language="JavaScript">
<!--
function ins(prop,type)
	{
	var input = document.form.message;var mysel;var text;
	if (window.getSelection) mysel = window.getSelection();
	else if (document.selection) mysel = document.selection.createRange().text;
	else if (document.getSelection)	mysel = document.getSelection();
	b = (type == 0) ? '['+prop+']' : prop;
	e = (type == 0) ? '[/'+prop+']' : '';
	if (input.createTextRange)
		{
		var text;
		input.focus(input.caretPos);
		input.caretPos = document.selection.createRange().duplicate();
		input.caretPos.text = (input.caretPos.text.length>0) ? b + input.caretPos.text + e : b + mysel + e;
		}
	else input.value += b + mysel + e;
	}
// -->
</script>

<form action="<?=C_URL?>/comments.php" method="post" enctype="multipart/form-data" name=form OnSubmit="return formCheck(this)">
<input type="hidden" name="l" value="<?=LANGUAGE?>">
<input type="hidden" name="a" value="a">
<input type="hidden" name="id" value="<?=ID?>">
                <div align="center"><span><b>
                  <?=ADD_COMMENTS?>
                </b></span></div>
                <br>
                <br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" align=center width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>">
<Tr>
  <td width="<?=C_WIDTH?>" bgcolor="<?=COLOR1?>" valign="top">
  <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
  <tr>
    <td width="30%"><?=COMMENTS?> *</Td>
    <td width="70%">
    <?php if(USE_SMILES || USE_BB){?>
    <Table Border=0 width="350" CellSpacing="1" CellPadding="4"><Tr align="left" bgcolor="<?=COLORH?>"><td>
<?php if(USE_SMILES){?><?php $p=0;foreach ($smiles as $k=>$v){$p++;?><a href="javascript:ins(' <?=$k?> ',1)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/forum/smiles/<?=$v?>" border=0 hspace=1/></a><?php if(C_SMILES_NUMBER <= $p){?><br><a href="javascript:open_win('<?=C_URL?>/forum/smiles_show.php','_blank')"><?=$w[720]?></a><br><?php break;}?><?php }?><?php }?> 
<?php if(USE_BB && USE_SMILES){?><br><br><?php }?>
<?php if(USE_BOLD){?><a href="javascript:ins('B',0)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Bold.gif" border=0 /></a><?php }?>
<?php if(USE_ITALIC){?><a href="javascript:ins('I',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Italic.gif" border=0 /></a><?php }?>
<?php if(USE_UNDERLINE){?><a href="javascript:ins('U',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Underline.gif" border=0 /></a><?php }?>
<?php if(USE_STRIKE){?><a href="javascript:ins('S',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Strike.gif" border=0 /></a><?php }?>
<?php if(USE_QUOTE){?><a href="javascript:ins('QUOTE',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Quote.gif" border=0 /></a><?php }?>  
<?php if(USE_URL){?><a href="javascript:ins('URL',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/URL.gif" border=0 /></a><?php }?>  
<?php if(USE_IMG){?><a href="javascript:ins('IMG',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Image.gif" border=0 /></a><?php }?>  
<?php if(USE_VIDEO){?><a href="javascript:ins('VIDEO',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Video.gif" border=0 /></a><?php }?>  
</td></tr></table><?php }?>
<textarea class=forum_input type=text name=message rows=12></textarea></Td></Tr>
<?php if (VERIFY_ALLOW) {?>
  	<tr>
    <td><?=VERIFICATION?><br><img src="<?=C_URL?>/img.php" border=0></Td>
    <td><input class=minput type=text name=verifyimage></Td>
  </Tr>
	<?php }?>  
	  <tr>
                          <td colspan=2 align=right>
    <input class=input1 type=submit value="OK" name="submit">
    </Td>
  </Tr>
                    </table></td>
</tr>
</table>
              </form>
<?php }?>
<script src="<?=C_URL?>/templates/<?=C_TEMP?>/js/lightgallery-all.min.js"></script>
<script src="<?=C_URL?>/templates/<?=C_TEMP?>/js/jquery.mousewheel.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#lightgallery").lightGallery({
            download: false,
            share: false,
            selector: '.lightgallery-item'
        });

        $(document).on('contextmenu', '.lightgallery-item', function(e) {
            return false;
        });

        $(document).on('mousedown', '.lightgallery-item', function(e) {
            return false;
        });

        $(document).on('mouseup', '.lightgallery-item', function(e) {
            return false;
        });

        $(document).on('contextmenu', '.lg-image', function(e) {
            return false;
        });
    });

</script>
