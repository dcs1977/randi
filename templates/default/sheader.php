<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 TRANSITIONAL//EN">
<html dir=<?=C_HTML_DIR?>>
<head>
<title><?=C_SNAME?></title>
<link rel="stylesheet" type="text/css" href="<?=C_URL?>/templates/<?=C_TEMP?>/style.css">
<META name=robots content=all>
<META HTTP-EQUIV="Expires" Content="0">
<meta http-equiv=Content-Type content="text/html; charset=<?=C_CHARSET?>">
<meta name=Copyright content="AzDG.com">
<meta name=Author content="AzDG.com">
<META name=description content="dating script from AzDG">
<META name=keywords content="dating script from AzDG">
<style>
    #modal-frame{
        background-color: rgba(0,0,0,0.5);
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0px;
        left: 0px;
    }

    #modal-content{
        position: relative;;
        top: 50px;
        left: 50%;
        transform: translate(-50%, 0%);
    }
</style>
<script>
    <!--
    // matches polyfill
    this.Element && function(ElementPrototype) {
        ElementPrototype.matches = ElementPrototype.matches ||
            ElementPrototype.matchesSelector ||
            ElementPrototype.webkitMatchesSelector ||
            ElementPrototype.msMatchesSelector ||
            function(selector) {
                var node = this, nodes = (node.parentNode || node.document).querySelectorAll(selector), i = -1;
                while (nodes[++i] && nodes[i] != node);
                return !!nodes[i];
            }
    }(Element.prototype);

    function addEvent(el, type, handler) {
        if (el.attachEvent) el.attachEvent('on'+type, handler); else el.addEventListener(type, handler);
    }

    // live binding helper using matchesSelector
    function live(selector, event, callback, context) {
        addEvent(context || document, event, function(e) {
            var found, el = e.target || e.srcElement;
            while (el && el.matches && el !== context && !(found = el.matches(selector))) el = el.parentElement;
            if (found) callback.call(el, e);
        });
    }

    function open_win(win_file, win_title)
    {
        close_win();
        var el = document.createElement('div');
        el.id = 'modal-frame';
        el.innerHTML = '<iframe id="modal-content" width="<?=C_WIN_WIDTH?>" height="<?=C_WIN_HEIGHT?>" src="' + win_file + '"></iframe>';
        document.body.appendChild(el);
    }

    function close_win(){
        var before = document.getElementById('modal-frame');
        if (before !== null) before.parentNode.removeChild(before);
    }

    live('#modal-frame', 'click', function(){
        if (typeof  press_f5 !== 'undefined'){
            press_f5();
        } else {
            location.reload();
        }
    });

    // event handler function
    function handler(e) {
        var key = window.event ? e.keyCode : e.which;
        console.log(key, e.shiftKey, e.altKey, e.ctrlKey);
        if (key == 27){
            e.preventDefault();
            if (typeof  press_f5 !== 'undefined'){
                press_f5();
            } else {
                location.reload();
            }
        } else if(key == 116 && typeof press_f5 !== 'undefined'){
            e.preventDefault();
            press_f5();
        } else if(key == 120 && typeof press_f9 !== 'undefined'){
            e.preventDefault();
            press_f9();
        }  else if(key == 121 && typeof press_f10 !== 'undefined'){
            e.preventDefault();
            press_f10();
        }
    }

    // attach handler to the keydown event of the document
    if (document.attachEvent) document.attachEvent('onkeydown', handler);
    else document.addEventListener('keydown', handler);
    -->
</script>
</head>
<body bgcolor=#BDD3F4 leftmargin=0 topmargin=0>
