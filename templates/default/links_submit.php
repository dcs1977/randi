<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0">
<TR> 
  <TD align=center VALIGN="TOP"><br><br>
<script language="JavaScript">
<!--

function CountChars(t, c, max) {
    if(t != null && t.value != null) {
	    if (t.value.length > max){
            alert('<?=LIMIT_ERROR?>');
            t.value = t.value.substring(0, max);
	    } else c.value = max - t.value.length;
    }
}

function formCheck(form) {
	if (form.email.value == "") {
		alert("<?=EMAIL_ERROR?>");
		return false;
	}

	if (document.form.submit.action != "") {
		document.form.submit.disabled=1;}
}


// -->
</script>
<form action="<?=filename()?>" method="post" name=form OnSubmit="return formCheck(this)">
<input type="hidden" name="l" value="<?=LANGUAGE?>">
<input type="hidden" name="a" value="add">
<input type="hidden" name="a2" value="true">

<span class=head><?=SUBMIT_SITE?></span>
<br><br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>">
<Tr>
  <Td>
  <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td width=40%>
    <?=CATEGORY?> *
    </Td>
    <Td>
    <select name="cat" class="input">
      <option selected value=0> --------- 
      <?=CATEGORY_VALUE?>
    </select>
    </Td>
  </Tr>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=TITLE?> *
    </Td>
    <Td>
    <input class=input type=text name=title value="<?=TITLE_VALUE?>">
    </Td>
  </Tr>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=URL?> *
    </Td>
    <Td>
    <input class=input type=text id=link name=link value="<?=URL_VALUE?>"">
    </Td>
  </Tr>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=DESCRIPTION?> *
    </Td>
    <Td>
    <textarea class=textarea cols=20 rows=8 name=descr onkeyup="javascript:CountChars(this.form.elements['descr'],this.form.elements['descr_limit'],<?=C_LINK_DESCR_LIMIT?>)"><?=DESCRIPTION_VALUE?>
    </textarea><br>
		<?=LIMIT?>: <input type="text" name="descr_limit" size="4" class="sinput" readonly="readonly" value="<?=C_LINK_DESCR_LIMIT?>">
		<script>CountChars(this.form.elements['descr'],this.form.elements['descr_limit'],<?=C_LINK_DESCR_LIMIT?>);</script>
    </Td>
  </Tr>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=KEYWORDS?> *
    </Td>
    <Td>
    <input class=input type=text name=keywords value="<?=KEYWORDS_VALUE?>" maxlength=500>
    </Td>
  </Tr>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=NAME?> *
    </Td>
    <Td>
    <input class=input type=text name=uname value="<?=NAME_VALUE?>" maxlength=50>
    </Td>
  </Tr>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=EMAIL?> *
    </Td>
    <Td>
    <input class=input type=text name=email value="<?=EMAIL_VALUE?>">
    </Td>
  </Tr>
	<?php if (C_REC_SHOW || C_REC_LINK) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td valign=top>
    <?=REC?><?php if (C_REC_LINK) {?> *<?php }?>
    </Td>
    <Td>
    <input class=input type=text name=rec value="<?=REC_VALUE?>"><br><br>

		<?=REC_TXT?><br>
		<textarea class=textarea cols=40 rows=3 name=noname readonly="readonly"><?=REC_TXT_VALUE?></textarea>
    </Td>
  </Tr>
	<?php }?>
	<?php if (VERIFY_ALLOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=VERIFICATION?> *<br>
		<img src=img.php border=0>
    </Td>
    <Td>
    <input class=input type=text name=verifyimage>
    </Td>
  </Tr>
	<?php }?>
  </table>
  </Td>
</Tr>
</Table>
<br><input class=input1 type=submit value="<?=SUBMIT_SITE?>" name="submit">
</form>
</TD>
</TR>
</TABLE>

