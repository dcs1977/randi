<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>">
<Tr>
  
  <Td width="<?=C_WIDTHL?>" bgcolor="<?=COLOR1?>" valign="top">
  <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTHL?>" class=mes>
  <Tr>
    <Td bgcolor="<?=C_TBCOLOR?>"></Td>
  </Tr>
  <Tr align="center" bgcolor="<?=COLORH?>">
    <Td>
    <?=FOR_MEMBERS?>
    </Td>
  </Tr>
  <Tr>
    <Td bgcolor="<?=C_TBCOLOR?>"></Td>
  </Tr>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>" class=desc>
    <Td>
    <form action="<?=C_URL?>/login.php" method="post">
    <input type="hidden" name="l" value="<?=LANGUAGE?>">
    <input type="hidden" name="p" value="s">
    <?=LOGIN_NAME?> <input class=minput type=text name=id><br>
    <?=PASSWORD?> <input class=minput type=password name=password><br>
    <?php if (VERIFY_ALLOW) {?><div align="right"><img src=img.php height=24 border=0></div>
<?=VERIFICATION?><INPUT  CLASS="minput" TYPE="text" NAME="verifyimage"><BR><?php }?>   
    <input class=minput type=submit value="<?=ENTER?>"><br>
		
    <a href="<?=C_URL?>/add.php?l=<?=$l?>" class=desc>[<?=REGISTER?>]</a> 
    <a href="<?=C_URL?>/remind.php?l=<?=$l?>" class=desc>[<?=FORGOT_PASSWORD?>]</a>
    </Td></form>
  </Tr>
<?php if(C_TOP_MANS){ /* If Top Man is allowed - Begin */?>
  <Tr>
    <Td bgcolor="<?=C_TBCOLOR?>"></Td>
  </Tr>
  <Tr align="center" bgcolor="<?=COLORH?>">
    <Td>
    <?=THE_BEST_MANS?>
    </Td>
  </Tr>
  <Tr>
    <Td bgcolor="<?=C_TBCOLOR?>"></Td>
  </Tr>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td class=desc align=center>
    <?=NOT_AVAILABLE_MANS?>
    <?php while(isset($tmids[$p])) { /* Top Mans show - Begin */ ?>
    <table width="100%" class=tr>
    <tr class=desc bgcolor="<?=$tmcolors[$p]?>" align=center>
      <td>
      <a href="<?=GenLink('UserProfile',array(LANGUAGE,$tmids[$p]))?>" class=desc><img src="<?=$tmpics[$p]?>" width="120" border="0" vspace="2" hspace="2"></a>
      </td>
    </tr>
    <tr class=mes bgcolor="<?=COLORH?>" align=center>
      <td>
      <?=$tmnames[$p]?>, <?=$tmages[$p]?>, <?=$tmcountries[$p]?> : <?=$tmhits[$p]?>
      </td>
    </tr>
    </table>
    <?php $p++;}$p=0; /* Top Mans show - End */ ?>	 
	<?php if(SHOW_MORE_RESULTS){?>
<a href="<?=GenLink('Top',array(LANGUAGE,0,1,1))?>" class="link3"><?=MORE_RESULTS?></a>
<?php }?>
    </Td>
  </Tr>
<?php } /* If Top Man is allowed - End */?>

  </Table>
  </Td>
  
  
  
  <Td width="<?=C_WIDTHR?>" valign="top" bgcolor="<?=COLOR1?>">
  <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTHC?>" class=mes>
  <Tr>
    <Td bgcolor="<?=C_TBCOLOR?>"></Td>
  </Tr>
  <Tr align="center" bgcolor="<?=COLORH?>">
    <Td>
    <?=WELCOME_TITLE?> <br> <?=ShowWelcomeBackMessage()?>
    </Td>
  </Tr>
  <Tr>
    <Td bgcolor="<?=C_TBCOLOR?>"></Td>
  </Tr>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <span class=mes><?=WELCOME_MESSAGE?></span>
    </Td>
  </Tr>
<?php if(C_NEWS_ALLOW) { /* If News is allowed - Begin */ ?> 
  <Tr>
    <Td bgcolor="<?=C_TBCOLOR?>"></Td>
  </Tr>
  <Tr align="center" bgcolor="<?=COLORH?>">
    <Td>
    <?=NEWS?>
    </Td>
  </Tr>
  <Tr>
    <Td bgcolor="<?=C_TBCOLOR?>"></Td>
  </Tr>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td align=center>
<?php if(!NEWS_NUMBER) smes(NO_NEWS) /* Message, if no news */ ?>
<?php else { /* Show News - Begin */ ?>
<?php while(isset($ndates[$p])) { /* Show news messages - Begin */ ?>
    <table width="100%" class=tr>
    <tr class=mes bgcolor="<?=COLORH?>" align=center>
      <td width="75%">
      <?=$nsubjects[$p]?>
      </td>
      <td>
      <?=$ndates[$p]?>
      </td>
    </tr>
    <tr>
      <td colspan="2" class=mes>
      <?=$nnews[$p]?>
      </td>
    </tr>
    </table><br>
<?php $p++;}$p=0; /* Show news messages - End */ ?>
<?php if(SHOW_ARCHIVE) { /* If show archive  - Begin */ ?>
<a href="javascript:open_win('<?=C_URL?>/news.php?l=<?=LANGUAGE?>','news');" class=desc>[<?=NEWS_ARCHIVE?>]</a>
<?php }} /* If show archive  - End */ ?>
    </Td>
  </Tr>
<?php } /* If News is allowed - End */ ?>
  

  </Table>
  </Td>
      
      
      
  <Td width="<?=C_WIDTHR?>" bgcolor="<?=COLOR1?>" valign="top">
  <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTHR?>" class=mes>
  <Tr>
    <Td bgcolor="<?=C_TBCOLOR?>"></Td>
  </Tr>
  <Tr align="center" bgcolor="<?=COLORH?>">
    <Td>
    <?=QUICK_SEARCH?>
    </Td>
  </Tr>
  <Tr>
    <Td bgcolor="<?=C_TBCOLOR?>"></Td>
  </Tr>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <form action="<?=C_URL?>/qsearch.php" method="get">
    <input type="hidden" name="l" value="<?=LANGUAGE?>">
    <input type="hidden" name="a" value="s">
    <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTHR?>" class=mes>
<?php if(I_AM_A_SHOW) { /* If is allow to show this section */ ?>
       <TR> 
        <TD VALIGN="TOP" CLASS="tab" ALIGN="RIGHT" COLSPAN="2"><br>
				<FONT CLASS="link3"><?=I_AM_A?></FONT>
				<select name="sgender" class="minput">
        <?=I_AM_A_VALUE?>
        </select>
  			</TD>
  		</TR>
<?php }?>	
<?php if(SEEKING_FOR_SHOW) { /* If is allow to show this section */ ?>
       <TR> 
        <TD VALIGN="TOP" CLASS="tab" ALIGN="RIGHT" COLSPAN="2">
        <FONT CLASS="link3"><?=SEEKING_FOR?></FONT> 
				<select name="gender[]" class="minput" multiple>
        <?=SEEKING_FOR_VALUE?>
      	</select>
  			</TD>
  		</TR>
 <Tr>
<?php }?>	
<?php if(AGE_SHOW) { /* If is allow to show this section */ ?>
    <TD VALIGN="TOP" CLASS="tab" ALIGN="RIGHT" COLSPAN="2">
    <FONT CLASS="link3"><?=AGE?>
    <input name="agef" class="sinput" style=width:44> -  <input name="aget" class="sinput" style=width:44>
    </td>
  </tr>
<?php }?>	
<?php if(COUNTRY_SHOW) { /* If is allow to show this section */ ?>
  <TR> 
        <TD VALIGN="TOP" CLASS="tab" ALIGN="RIGHT" COLSPAN="2">
        <FONT CLASS="link3"><?=COUNTRY?></FONT>
				<select name="country[]" class="minput" multiple>
      	<?=COUNTRY_VALUE?>
    		</select>
				</TD>
 			</TR>
<?php }?>	
       <TR> 
        <TD VALIGN="TOP" CLASS="tab" ALIGN="RIGHT" COLSPAN="2">
<?php if(PHOTO_SHOW) { /* If is allow to show this section */ ?>
    <?=SEARCH_USERS_WITH?> <?=PHOTO?> <input type="checkbox" name="photo"><br>
<?php }?>	
<?php if(ONLINE_USERS_ONLY_SHOW) { /* If is allow to show this section */ ?>
    <?=ONLINE_USERS_ONLY?> <input type="checkbox" name="online"> 
<?php }?>	
				</TD>
 			 </TR>
</table>
<?php if(ALLOW_BSEARCH) { /* If Birthday search is require - Begin */ ?>
    <?=BIRTHDAY?> 
    <select name="month" class="minput">
      <option selected value=0>-- 
      <?=MONTH_VALUE?>
    </select>
    <select name="day" class="sinput">
      <option selected value=0>--
      <?=DAY_VALUE?>
    </select>
    <select name="year" class="sinput">
      <option selected value=0>-- 
      <?=YEAR_VALUE?>
    </select>
    <br>	 
<?php } else { /* If Birthday search is NOT require - Begin */ ?>
    <input type="hidden" name="month" value="0">
    <input type="hidden" name="day" value="0">
    <input type="hidden" name="year" value="0">
<?php } /* If Birthday search is NOT require - End */ ?>
    <input class=minput type=submit value="<?=QUICK_SEARCH?>">
    <br>
    <a href="<?=C_URL?>/search.php?l=<?=LANGUAGE?>" class=desc>[<?=SEARCH?>]</a> 
    <a href="<?=C_URL?>/advsearch.php?l=<?=LANGUAGE?>" class=desc>[<?=ADV_SEARCH?>]</a>
    </Td></form>
  </Tr>
<?php if(C_TOP_WOMANS){ /* If Top Woman is allowed - Begin */?>
  <Tr>
    <Td bgcolor="<?=C_TBCOLOR?>"></Td>
  </Tr>
  <Tr align="center" bgcolor="<?=COLORH?>">
    <Td>
    <?=THE_BEST_WOMANS?>
    </Td>
  </Tr>
  <Tr>
    <Td bgcolor="<?=C_TBCOLOR?>"></Td>
  </Tr>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td class=desc align=center>
    <?=NOT_AVAILABLE_WOMANS?>
    <?php while(isset($twids[$p])) { /* Top Womans show - Begin */ ?>
    <table width="100%" class=tr>
    <tr class=desc bgcolor="<?=$twcolors[$p]?>" align=center>
      <td>
      <a href="<?=GenLink('UserProfile',array(LANGUAGE,$twids[$p]))?>" class=desc><img src="<?=$twpics[$p]?>" width="120" border="0" vspace="2" hspace="2"></a>
      </td>
    </tr>
    <tr class=mes bgcolor="<?=COLORH?>" align=center>
      <td>
      <?=$twnames[$p]?>, <?=$twages[$p]?>, <?=$twcountries[$p]?> : <?=$twhits[$p]?>
      </td>
    </tr>
    </table>
    <?php $p++;}$p=0; /* Top Womans show - End */ ?>	
	<?php if(SHOW_MORE_RESULTS){?>
<a href="<?=GenLink('Top',array(LANGUAGE,0,2,1))?>" class="link3"><?=MORE_RESULTS?></a>
<?php }?>
    </Td>
  </Tr>
<?php } /* If Top Woman is allowed - End */?>

  </Table>
	<?php ShowVote()?>
  </Td>
</Tr>
</Table>
