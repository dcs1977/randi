<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
<script language="JavaScript">
<!--
function formCheck(form) {

    if (form.subject.value == ""){
        alert("<?=ENTER_YOUR_SUBJECT?>");
        return false;
    }

    if (form.message.value == ""){
        alert("<?=ENTER_YOUR_MESSAGE?>");
        return false;
    }

    if (form.remail.value == ""){
        alert("<?=ENTER_YOUR_EMAIL?>");
        return false;
    }

    if (form.approve.checked == 0){
        alert("<?=ENTER_APPROVE_EMAIL?>");
        return false;
    }

    document.form.submit.disabled=1;

    if (captchaContainer !== null){
        return true;
    }

    var form = $('#contactForm')[0];
    var formData = new FormData(form);

    $.ajax({
        method: 'POST',
        url: "feedback.php",
        processData: false,
        contentType: false,
        dataType: "json",
        data: formData
    })
    .done(function(msg){
        //console.log(msg);
        if (msg.status == 200){
            location = msg.href;
        } else if (msg.status == 500){
            const nodeBadge = document.querySelector('.grecaptcha-badge');
            if (nodeBadge) {
                document.body.removeChild(nodeBadge.parentNode);
            }

            const scriptSelector = 'script[src=\'https://www.google.com/recaptcha/api.js?render=<?=RECAPTCHA_PUBLIC_KEY?>\']';
            const script = document.querySelector(scriptSelector);
            if (script) {
                script.remove();
            }

            captchaContainer = grecaptcha.render('captcha_container', {
                'sitekey' : '<?=OLD_RECAPTCHA_PUBLIC_KEY?>',
                'callback' : function(response) {
                    document.form.submit.disabled = false;
                }
            });

            $('#recaptcha_token').remove();
            return false;
        }
    });
    return false;
}
// -->
</script>

<script>
    var captchaContainer = null;

    grecaptcha.ready(function () {
        grecaptcha.execute('<?=RECAPTCHA_PUBLIC_KEY?>', {action: 'feedback'}).then(function(token) {
            $('#contactForm').prepend('<input id="recaptcha_token" type="hidden" name="token" value="' + token + '">');
            $('#contactForm').prepend('<input type="hidden" name="action" value="feedback">');
            document.form.submit.disabled = false;
        });
        setInterval(function () {
            grecaptcha.execute('<?=RECAPTCHA_PUBLIC_KEY?>', { action: 'feedback' }).then(function (token) {
                if ($("#recaptcha_token").length) {
                    $("#recaptcha_token").val(token);
                }
            });
        }, 60000);
    });
</script>

<form action="feedback.php" method="post" name=form  id="contactForm" OnSubmit="return formCheck(this)" enctype="multipart/form-data" >
<input class=input type=hidden name="l" value="<?=LANGUAGE?>">
<input class=input type=hidden name="a" value="s">
<center><span class=head><?=FEEDBACK?></span>
<br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_SWIDTH?>" bgcolor="<?=C_TBCOLOR?>">
<Tr>
  <Td>
  <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_SWIDTH?>" class=mes>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=NAME?>
    </td>
    <td>
    <input class=input type=text name=rname <?=NAME_VALUE_DISABLED?>>
    </td>
  </tr>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=EMAIL?>
    </td>
    <td>
    <input class=input type=text name=remail <?=EMAIL_VALUE_DISABLED?>>
    </td>
  </tr>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SUBJECT?>
    </td>
    <td>
    <input class=input type=text name=subject maxlength="64">
    </td>
  </tr>

      <Tr class="email_again" align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
          <Td>
              Ha látod ezt a mezőt, akkor kérlek hagyd üresen!
          </td>
          <td>
              <input class=input type=text name="website-honeypot" autocomplete="off">
          </td>
      </tr>

  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=MESSAGE?>
    </td>
    <td>
    <textarea class=textarea name=message cols=40 rows=15></textarea>
    </td>
  </tr>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td colspan="2">
          <label for="checkbox_approve">
              <input type="checkbox" name="approve" id="checkbox_approve" value="igen">
              <?=APPROVE_EMAIL?>
          </label>
      </td>
  </tr>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td colspan="2">
    <input class=input type=submit value="<?=SEND_MESSAGE?>" name="submit" disabled>
    </td>
  </tr>
      <Tr bgcolor="<?=COLOR1?>">
          <Td colspan="2">
              <div style="text-align: center" id="captcha_container"></div>
              <style>
                  #captcha_container div{
                      margin: auto;
                  }
              </style>
          </Td>
      </Tr>
  </table>
  </td>
</tr>
</table>
</form>