<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 TRANSITIONAL//EN">
<html dir=<?=C_HTML_DIR?>>
<head>
<title><?=C_TITLE?></title>
<META name="robots" content="noimageindex, noarchive">
<META HTTP-EQUIV="Expires" Content="0">
<meta name="rating" content="mature">
<meta http-equiv=Content-Type content="text/html; charset=<?=C_CHARSET?>">
<meta name=Copyright content="<?=C_SNAME?>">
<meta name=Author content="AzDG.com">
<META name=description content="<?=C_DESCRIPTION?>">
<META name=keywords content="<?=C_KEYWORDS?>">
<link rel="stylesheet" type="text/css" href="<?=C_URL?>/templates/<?=C_TEMP?>/style.css">

<!-- To support old sizes -->
<link rel="apple-touch-icon" sizes="57x57" href="<?=C_URL?>/templates/<?=C_TEMP?>/img/hotbulllogo_57.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?=C_URL?>/templates/<?=C_TEMP?>/img/hotbulllogo_72.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?=C_URL?>/templates/<?=C_TEMP?>/img/hotbulllogo_114.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?=C_URL?>/templates/<?=C_TEMP?>/img/hotbulllogo_144.png">

<!– To support new sizes –>
<link rel=”apple-touch-icon” sizes=”60×60″ href=”<?=C_URL?>/templates/<?=C_TEMP?>/img/hotbulllogo_60.png”>
<link rel=”apple-touch-icon” sizes=”76×76″ href=”<?=C_URL?>/templates/<?=C_TEMP?>/img/hotbulllogo_76.png”>
<link rel=”apple-touch-icon” sizes=”120×120″ href=”<?=C_URL?>/templates/<?=C_TEMP?>/img/hotbulllogo_120.png”>
<link rel=”apple-touch-icon” sizes=”152×152″ href=”<?=C_URL?>/templates/<?=C_TEMP?>/img/hotbulllogo_152.png”>
<link rel=”apple-touch-icon” sizes=”180×180″ href=”<?=C_URL?>/templates/<?=C_TEMP?>/img/hotbulllogo_180.png”>

<!– To support Android –>
<link rel=”icon” sizes=”192×192″ href=”<?=C_URL?>/templates/<?=C_TEMP?>/img/hotbulllogo_192.png”>
<link rel=”icon” sizes=”128×128″ href=”<?=C_URL?>/templates/<?=C_TEMP?>/img/hotbulllogo_128.png”>

<!-- Global site tag (gtag.js) - Google AdWords: 1029183088 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-762404243"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'AW-762404243');
</script>

<!--script type="text/javascript" src="<?=C_URL?>/templates/<?=C_TEMP?>/copy-disabler.js"></script-->
<script type="text/javascript" src="<?=C_URL?>/templates/<?=C_TEMP?>/jquery-3.4.1.min.js"></script>

<!--link rel="stylesheet" href="<?=C_URL?>/templates/<?=C_TEMP?>/iframe-modal.css"-->
<script type="text/javascript" src="<?=C_URL?>/templates/<?=C_TEMP?>/modalWindow.js"></script>
<script type="text/javascript" src="<?=C_URL?>/templates/<?=C_TEMP?>/initialize.js"></script>

<script src="https://www.google.com/recaptcha/api.js?render=<?=RECAPTCHA_PUBLIC_KEY?>"></script>
<script src="https://www.google.com/recaptcha/api.js?render=explicit" async defer></script>
<?php 
	$c = get_defined_constants();
    unset($c['C_ADMINL']);
    unset($c['C_ADMINP']);
    unset($c['C_ADMINM']);
    unset($c['C_ONESIGNAL_REST_API_KEY']);
    unset($c['RECAPTCHA_SECRET_KEY']);
    unset($c['OLD_RECAPTCHA_SECRET_KEY']);
    unset($c['C_HOST']);
    unset($c['C_USER']);
    unset($c['C_PASS']);
    unset($c['C_BASE']);
	$c = json_encode(unserialize(str_replace(array('NAN;','INF;'),'0;',serialize($c))));
?>
<script>
window.W = function(key){
	var w = <?=json_encode($w)?>;
	return w[key];
}

window.CONST = function(key){
    var c = <?=$c?>;
    return c[key];
}

window.MyID = '<?=MyID()?>';
window.C_URL = '<?=C_URL?>';
</script>

<script language="JavaScript" src="<?=C_URL?>/classes/Ajax.js"></script>
<link rel="manifest" href="/manifest.json" />
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
<script src="<?=C_URL?>/templates/<?=C_TEMP?>/sjcl.js"></script>
<script src="<?=C_URL?>/templates/<?=C_TEMP?>/fingerprint2.min.js"></script>
<?php if(MyID()) {?>
<script type="text/javascript" src="<?=C_URL?>/templates/<?=C_TEMP?>/user_menu.js"></script>
<style type="text/css" media="screen, projection">
@import "<?=C_URL?>/templates/<?=C_TEMP?>/user_menu.css";
</style>
<?php } ?>
<script src="<?=C_URL?>/templates/<?=C_TEMP?>/swfobject-2.2.min.js"></script>
<script src="<?=C_URL?>/templates/<?=C_TEMP?>/evercookie.js"></script>
<script src="<?=C_URL?>/templates/<?=C_TEMP?>/script.js"></script>
</head>
<body bgcolor=#BDD3F4 leftmargin=0 topmargin=0>
<noscript>
<style>
	.attention{
		color: black;
		width: 100%;
		text-align: center;
		padding: 5px 0;
		font-size: 20px;
	}
	center{
		display: none;
	}
</style>
<div style="text-align: center">
	<img src="https://hotbull.hu/bannerrandi.png">
</div>
<div class="attention">Úgy látjuk, javascript szűrőt használsz. <br/>
Az oldal használatához szükséged lesz rá! <br/>
Engedélyezd a böngésződben! <br/>
Köszönjük.</div>
</noscript>
<div class="window-modal js-modal-window js-window-overlay">
    <div class="window-modal__in js-modal-window-in"></div>
</div>
<style>
    .window-modal {
        display: none;
        align-items: center;
        justify-content: center;
        flex-wrap: wrap;
        overflow-y: auto;
        position: fixed;
        padding: 15px;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        z-index: 1000;
        background-color: rgba(0,0,0,0.7);
    }

    .window-modal__in {
        position: relative;
        max-width: 100%;
        padding: 1px;
        z-index: 1001;
        background-color: #fff;
        border-radius: 3px;
        box-shadow: 0 0 6px 2px rgba(50,50,50,0.3);
    }

    .window-activated {
        overflow: hidden;
        margin-right: 17px;
    }
    
    .grecaptcha-badge { 
        visibility: hidden;
    }
</style>
<!--div id="cookie_attention" class="hide">
	<p><b>Tudjuk, már unod, de kérlek, olvasd el figyelmesen!</b></p>
	<p>Figyelmeztetünk, hogy az oldal működéséhez cookie-kat (más néven sütiket) használunk.<br/>
	A legtöbb süti ahhoz kell, hogy az oldal működjön, némelyik pedig a felhasználói igények érvényesítése miatt szükséges, 
	némelyik pedig azon célból, hogy fejlesszük az oldalt.<br/>
	Mi csak arra kérünk téged, hogy az oldal működéséhez fogadd el, hogy sütiket alkalmazunk. 
	Bővebb információt az <a href="<?=C_URL?>/privacy.php?l=hu" target="_blank">Adatvédelmi Tájékoztatónkban</a> olvashatsz.</p>
	<p><button class="approve_button">Elfogadom</button></p>
</div-->
<center>
<Table CellSpacing="0" CellPadding="1" width="<?=C_BWIDTH?>" bgcolor="<?=C_TBCOLOR?>" align=center>
<tr>
  <td width="740" height="94" align=left>
  <Table Border=0 CellSpacing="0" CellPadding="2" width="<?=C_BWIDTH?>" class=mes>
	<Tr bgcolor="#73AED2" class=desc>
    <Td colspan=6 width=90%>
<center>	
<a href="https://hotbull.hu/randi"><img alt="Hotbull Randi" src="https://hotbull.hu/bannerrandi.png" border="0" width="240"></a>
<a href="https://hotbull.hu/portal" target="_blank"><img alt="Hotbull Portal" src="https://hotbull.hu/bannerportal.png" border="0" width="240"></a>
<!--a href="https://hotbull.hu/chat" target="_blank"><img alt="Hotbull Chat" src="https://hotbull.hu/bannerchat.png" border="0" width="240"></a-->
</center>
	</Td>
  </Tr>
 </Table>
  </td>
</tr>

<?php if(is_numeric(get_info('m'))) {/* If User is Registered - Begin */?>
  
<Tr bgcolor="#73AED2" class=desc>
<Tr>
  <Td>
  <Table Border=0 CellSpacing="0" CellPadding="2" width="<?=C_BWIDTH?>" class=mes>  
  <?php if (IsAccess(C_AJAX_MSGINF) && C_HAVE_MESSAGE && MyID() > 0) {?>
<script>
	var newmsg = <?=NEWMSG?>;
	function SRA() {
		var req = new Ajax();
		req.onreadystatechange = function() {
			if (req.readyState == 4) {
				if (req.resp) {
					if (req.resp.logout == '1')
					{
					    window.location = 'logout.php';
					}
					
					document.getElementById('newmsg').innerHTML = req.resp.newmsg;
					document.getElementById('newicon').innerHTML = req.resp.icon;
					//document.getElementById('num').innerHTML = req.resp.num;
					if(req.resp.num > newmsg) {
						<?php if(get_info('imsound')){?>
						document.getElementById('num').innerHTML = '<embed type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" width="1" height="1" name="chat" src="<?=C_URL?>/templates/<?=C_TEMP?>/images/chat.swf" bgcolor="#660000" quality="high" swLiveConnect="false" allowScriptAccess="samedomain" loop=false></embed>';
						<?php }?>

					} else document.getElementById('num').innerHTML = '';
					newmsg = req.resp.num;
				}
			}
		}
		//req.caching = false;
		req.open(null, '<?=C_URL?>/ajax/dl.php', true);
		req.send({ check:'2'});
		setTimeout("SRA()",<?=C_MSGINF_SEC*1000?>)	;
	}
	setTimeout("SRA()",<?=C_MSGINF_SEC*1000?>)	;
</script>
<?php }?>
 <Tr bgcolor="#73AED2" class=desc>
    <Td width=100%>
    <ul id="Menu">
<li id="AB">
	<a href="<?=MAIN_PAGE_LINK?>" style="color:#000"><?=MAIN_PAGE?></a>
</li>
<li id="AB"><a href="#" style="color:#000"><IMG SRC="<?=C_URL?>/templates/<?=C_TEMP?>/images/messages.gif" WIDTH="16" HEIGHT="16" style="border-style: none;vertical-align: middle;padding-bottom: 2px;	padding-top: 1px;	margin-top: -2px;"> <?=VIEW_MESSAGES?></a>
				<ul>
					<li><a class=link3 href="<?=VIEW_INBOX_LINK?>"><?=VIEW_INBOX?></a></li>
					<li><a class=link3 href="<?=VIEW_OUTBOX_LINK?>"><?=VIEW_OUTBOX?></a></li>
					<li><a class=link3 href="<?=ADDRESS_BOOK_LINK?>"><?=ADDRESS_BOOK?></a></li>
				</ul>
			</li>
			<li id="AB"><a href="#" style="color:#000"><IMG SRC="<?=C_URL?>/templates/<?=C_TEMP?>/images/mydata.gif" WIDTH="16" HEIGHT="16" style="border-style: none;vertical-align: middle;padding-bottom: 2px;	padding-top: 1px;	margin-top: -2px;"> <?=MY_DATA?></a>
				<ul>
					<li><a class=link3 href="<?=MY_PROFILE_LINK?>"><?=MY_PROFILE?></a></li>
					<?php if(IsAccess(C_WHO_SEEN_OWN)){?><li><a class=link3 href="<?=WHO_SEEN_MY_PROFILE_LINK?>"><?=WHO_SEEN_MY_PROFILE?> [<?=VisitFromLastLogin($_SESSION['lastlogin'])?>]</a></li><?php }?>					
					<li><a class=link3 href="<?=CHANGE_PROFILE_LINK?>"><?=CHANGE_PROFILE?></a></li>

					<?php if (C_REMOVE_ALLOW) {?><li><a class=link3 href="<?=REMOVE_PROFILE_LINK?>"><?=REMOVE_PROFILE?></a></li><?php }?>
				</ul>
			</li>
			<li id="AB"><a href="#" style="color:#000"><IMG SRC="<?=C_URL?>/templates/<?=C_TEMP?>/images/search.gif" WIDTH="16" HEIGHT="16" style="border-style: none;vertical-align: middle;padding-bottom: 2px;	padding-top: 1px;	margin-top: -2px;"> <?=SEARCH?></a>
				<ul>
					<li><a class=link3 href="<?=ONLINE_LINK?>"><?=USERS_ONLINE?></a></li>
					<li><a class=link3 href="<?=VISITED_PROFILES_LINK?>"><?=VISITED_PROFILES?></a></li>
					<li><a class=link3 href="<?=ALL_USERS_LINK?>"><?=ALL_USERS?></a></li>
					<li><a class=link3 href="<?=SEARCH_LINK?>" class=menu><?=SEARCH?></a></li>
					<li><a class=link3 href="<?=STATISTICS_LINK?>" class=menu><?=STATISTICS?></a></li>
					<!--li><a class=link3 href="<?=MATCHMAKER_LINK?>"><?=MATCHMAKER?></a></li-->
				</ul>
			</li>
			<li id="AB"><a href="#" style="color:#000"><IMG SRC="<?=C_URL?>/templates/<?=C_TEMP?>/images/options.gif" WIDTH="16" HEIGHT="16" style="border-style: none;vertical-align: middle;padding-bottom: 2px;	padding-top: 1px;	margin-top: -2px;"> <?=OPTIONS?></a>
				<ul>
					<?php if(IsAccess(C_OPTIONS)) {?><li><a class=link3 href="<?=OPTIONS_LINK?>"><?=OPTIONS?></a></li><?php }?>
					<li><a class=link3 href="<?=CHANGE_PASSWORD_LINK?>"><?=CHANGE_PASSWORD?></a></li>
					<li><a class=link3 href="<?=BANLIST_LINK?>"><?=BANLIST?></a></li>
					<li><a class=link3 href="<?=MY_FAVORITES_LINK?>"><?=MY_FAVORITES?></a></li>
				</ul>
			</li>		
	<li id="AB">
		<a href="<?=EXIT_LINK?>" style="color:#000"><img style="border-style: none;vertical-align: middle;padding-bottom: 2px;	padding-top: 1px;	margin-top: -2px;" src="<?=C_URL?>/templates/<?=C_TEMP?>/images/exit.png" alt="<?=EXIT_FROM?>" width="16" height="16" border="0"> <?=EXIT_FROM?></a>
	</li>
	</ul>

</Tr>
<?php /*if(NOTICE) {?>
<!-- <tr align="center" bgcolor="<?=COLOR1?>"><td colspan=7>
<?=NOTICE?>
</td></tr> -->                      
<?php }*/?>

  </table>
  </Td>
</Tr>

<?php } else {/* If User is Registered - End */?>

<Tr>
  <Td>
  <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="1" width="<?=C_BWIDTH?>" class=mes>
  <!--<?php show_languages()?>-->
  <Tr bgcolor="<?=COLOR2?>">
    <Td align="center">
    <a href="<?=MAIN_PAGE_LINK?>" class=menu><?=MAIN_PAGE?><!--<?php if(CountUsersPerPage("_OnlineMainIDs")){?> (<?=_OnlineMainIDs?>)<?php }?>--></a>	|
    <a href="<?=REGISTER_LINK?>" class=menu><?=REGISTER?></a>
    <!--<a href="<?=SEARCH_LINK?>" class=menu><?=SEARCH?><!--<?php if(CountUsersPerPage("_OnlineSearchIDs")){?> (<?=_OnlineSearchIDs?>)<?php }?></a>-->
	<!--<a href="<?=FAQ_LINK?>" class=menu><?=FAQ?><?php if(CountUsersPerPage("_OnlineFAQIDs")){?> (<?=_OnlineFAQIDs?>)<?php }?></a>		   |-->
  	<!-- <A CLASS="menu" HREF="<?=LINKS_LINK?>"><?=LINKS?><?php if(CountUsersPerPage("_OnlineLinksIDs")){?> (<?=_OnlineLinksIDs?>)<?php }?></A> |-->

    <!--<a href="<?=STATISTICS_LINK?>" class=menu><?=STATISTICS?><!--<?php if(CountUsersPerPage("_OnlineStatIDs")){?> (<?=_OnlineStatIDs?>)<?php }?></a>-->
    </td>
  </tr>
  </table>
  </Td>
</Tr>
<?php }?>

<Tr>
  <Td>
      <div style="background-color: #BDD3F4; padding: 10px;"><a class="desc" style="font-size: 20px !important;" href="<?=C_URL?>/m">PRÓBÁLD KI A HOTBULL ÚJ, MOBILON IS HASZNÁLHATÓ FELÜLETÉT!</a></div>
  <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="1" width="<?=C_BWIDTH?>" class=mes>
<?php /*Random Quotes*/?>
	<tr align="center" bgcolor="<?=COLOR1?>"><td>
	<span style="font-size: 14px"><?=$rqu[mt_rand(1,count($rqu))-1]?></span>
	</td></tr>   
	</Table>
	<?php if(MyID()) {?>
	<?php 
		$show_term = false;
		if(!isset($_SESSION['approved_term'])){
			$show_term = true;
		} else if($_SESSION['approved_term'] < C_TERM_RULES){
			$show_term = true;
		}
	?>
	<?php }?>
  </Td>
</Tr>   
</Table>

<?php if(is_numeric(get_info('m'))) {/* If User is Registered - Begin */?>
<Center style="margin: 10px 0;">
<sup><span id=newicon><?=NEW_ICON?></span><span id=num></span><a href="<?=VIEW_INBOX_LINK?>" class=menu><span id=newmsg style="font-size: 0.8em;"><?=YOU_HAVE_NEW_MESSAGE?></span></a></sup>
</Center>
<?php } ?>

<div id="banner_top" style="text-align:center; padding: 10px 0;">
<script type='text/javascript'><!--//<![CDATA[
   var m3_u = (location.protocol=='https:'?'https://ads.gaypoint.hu/www/delivery/ajs.php':'http://ads.gaypoint.hu/www/delivery/ajs.php');
   var m3_r = Math.floor(Math.random()*99999999999);
   if (!document.MAX_used) document.MAX_used = ',';
   document.write ("<scr"+"ipt type='text/javascript' src='"+m3_u);
   document.write ("?zoneid=20");
   document.write ('&amp;coupon=<?php echo Get_MY_Coupon(); ?>');
   document.write ('&amp;cb=' + m3_r);
   if (document.MAX_used != ',') document.write ("&amp;exclude=" + document.MAX_used);
   document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
   document.write ("&amp;loc=" + escape(window.location));
   if (document.referrer) document.write ("&amp;referer=" + escape(document.referrer));
   if (document.context) document.write ("&context=" + escape(document.context));
   if (document.mmm_fo) document.write ("&amp;mmm_fo=1");
   document.write ("'><\/scr"+"ipt>");
//]]>--></script><noscript><a href='http://ads.gaypoint.hu/www/delivery/ck.php?n=a6fd956e&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://ads.gaypoint.hu/www/delivery/avw.php?zoneid=20&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=a6fd956e' border='0' alt='' /></a></noscript>
</div>
<div align=center>
<?php if(defined("PROMPT_MESSAGES")):?>
    <div class='onesignal-customlink-container'></div>
<?php endif;?>
