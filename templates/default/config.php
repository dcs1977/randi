<?php if(!defined("C_URL")) die('Can`t access file directly!'); 
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# Template Name           Blue                             #
# Author                  AzDG <support@azdg.com>          #
############################################################
# File name               config.php                       #
# File purpose            Config for Blue template         #
# File created by         AzDG <support@azdg.com>          #
############################################################
###### Templates
### Colors and Borders
define('COLOR1','#A0C0F0'); // Color for add form, search, email and other
define('COLOR2','#73AED2'); // Background
define('COLOR3','#A0C0F0'); // Background
define('COLOR4','#84B8D7'); // Background
define('COLORH','#73AED2'); // Background
define('C_BORDER','1'); // Table border (external)
define('C_IBORDER','0'); // Table border (internal)
define('C_BWIDTH','728'); // Table width (Big width) 
define('C_WIDTHL','160'); // Table width (Big forms) 
define('C_WIDTHC','390'); // Table width (Big forms) 
define('C_WIDTHR','160'); // Table width (Big forms) 
define('C_WIDTH','500'); // Table width (Big forms) 
define('C_SWIDTH','350'); // Table width (Small forms) 
define('C_ROWWIDTH_A','410'); // Table row width right side 
define('C_ROWWIDTH_B','80'); // Table row width right side 
define('C_ALIGN','right'); // Align td
define('C_CELLP','4'); // Table CellPadding
define('C_TBCOLOR','black'); // Table border color
define('C_GRAPHIC','200'); // Graphic max width
define('C_WIN_WIDTH','600'); // Window width (bad profile, add to bedroom)
define('C_WIN_HEIGHT','400'); // Window height 
define('C_SS_WIDTH','728'); // Slideshow width (bad profile, add to bedroom)
define('C_SS_HEIGHT','600'); // Slideshow height 
define('C_IMG_BRDR','2'); // Image Border 

### Error and succesfull messages templates
define('C_ETOP','<center><br><br><br><br><Table Border=1 CellSpacing=0 CellPadding=4 bordercolor=black width=600><Tr><Td Width=600 align=center class=head bgcolor='.COLOR1.'><br><br>');

define('C_EBOT','<br><br><input class=button type=button OnClick=\'history.back()\' value=<<<<<><br></Td></Tr></table><br><br><br><br></center>');

define('C_SBOT','<br><br><br></Td></Tr></table><br><br><br><br></center>');



?>