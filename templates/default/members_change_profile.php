<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>

   <TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="6">
    <TR> 
     <TD ALIGN="CENTER" VALIGN="TOP">
<br><br><br>

<script language="JavaScript">
<!--
// Select Multiple js-function: hld()
function hld(nid){ln=nid.parentNode;if(nid.checked){ln.style.backgroundColor='#0a246a';ln.style.color='#fff';}else{ln.style.backgroundColor='#fff';ln.style.color='#000';}}

function formCheck(form) {
<?php if (USERNAME_ALLOW) {?>
if (form.username.value == "") {
    alert("<?=USERNAME_ERROR?>");
    return false;
}
<?php }?>
<?php if (ORIENTATION_ALLOW) {?>
if (form.orientation.selectedIndex=="") {
    alert("<?=ORIENTATION_ERROR?>");
    return false;
}
<?php }?>
<?php if (FIRSTNAME_ALLOW) {?>
if (form.fname.value == "") {
    alert("<?=FIRSTNAME_ERROR?>");
    return false;
}
<?php }?>

<?php if (LASTNAME_ALLOW) {?>
if (form.lname.value == "") {
    alert("<?=LASTNAME_ERROR?>");
    return false;
}
<?php }?>

<?php if (C_CHANGE_EMAIL) { ?>
if (form.email.value == "") {
    alert("<?=EMAIL_ERROR?>");
    return false;
}
<?php }?>

<?php if (BODYTYPE_ALLOW) {?>
if (form.bodytype.selectedIndex=="") {
    alert("<?=BODYTYPE_ERROR?>");
    return false;
}
<?php }?>

<?php if (INTERESTS_ALLOW) {?>
if (form.interests.selectedIndex=="") {
    alert("<?=INTERESTS_ERROR?>");
    return false;
}
<?php }?>

<?php if (ZIP_ALLOW) {?>
if (form.zip.value=="") {
    alert("<?=ZIP_ERROR?>");
    return false;
}
<?php }?>

<?php if (PHONE_ALLOW) {?>
if (form.phone.value == "") {
    alert("<?=PHONE_ERROR?>");
    return false;
}
<?php }?>

<?php if (CITY_ALLOW) {?>
if (form.city.value == "") {
    alert("<?=CITY_ERROR?>");
    return false;
}
<?php }?>

<?php if (STATE_ALLOW) {?>
if (form.state.value == "") {
    alert("<?=STATE_ERROR?>");
    return false;
}
<?php }?>

<?php if (ADDRESS_ALLOW) {?>
if (form.address.value == "") {
    alert("<?=ADDRESS_ERROR?>");
    return false;
}
<?php }?>

<?php if (JOB_ALLOW) {?>
if (form.job.value=="") {
    alert("<?=JOB_ERROR?>");
    return false;
}
<?php }?>

<?php if (HOBBY_ALLOW) {?>
if (form.hobby.value=="") {
    alert("<?=HOBBY_ERROR?>");
    return false;
}
<?php }?>

if (form.descr.value=="") {
    alert("<?=DESCRIPTION_ERROR?>");
    return false;
}

if (document.form.submit.action != "") {
document.form.submit.disabled=1;}
}

function ins(prop,type)
{
	var input = document.form.descr;var mysel;var text;
	if (window.getSelection) mysel = window.getSelection();
	else if (document.selection) mysel = document.selection.createRange().text;
	else if (document.getSelection)	mysel = document.getSelection();
	b = (type == 0) ? '['+prop+']' : prop;
	e = (type == 0) ? '[/'+prop+']' : '';
	if (input.createTextRange)
	{
		var text;
		input.focus(input.caretPos);
		input.caretPos = document.selection.createRange().duplicate();
		input.caretPos.text = (input.caretPos.text.length>0) ? b + input.caretPos.text + e : b + mysel + e;
	}
	else input.value += b + mysel + e;
}
// -->
</script>

<form action="index.php" method="post" enctype="multipart/form-data" name=form OnSubmit="return formCheck(this)">
<input type="hidden" name="l" value="<?=LANGUAGE?>">
<input type="hidden" name="a" value="u">
<span class=head><?=CHANGE_PROFILE?></span><br>
<br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>">
<Tr>
  <Td>
  <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
  <Tr style="height: 0">
	<Td style="padding: 0;"></Td>
	<Td style="padding: 0;" width="<?=C_SWIDTH?>"></Td>
  </Tr>
  <?php if(USERNAME_ALLOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=USERNAME?>
    </Td>
    <Td width="<?=C_WIDTH?>">
    <input class=input type=text name=username maxlength="16" value="<?=USERNAME_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(FIRSTNAME_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=FIRSTNAME?>
    </Td>
    <Td>
    <input class=input type=text name=fname maxlength="<?=C_FIRSTNB?>" value="<?=FIRSTNAME_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(LASTNAME_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=LASTNAME?>
    </Td>
    <Td>
    <input class=input type=text name=lname maxlength="<?=C_LASTNB?>" value="<?=LASTNAME_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(BIRTHDAY_SHOW) {?>
  <!--Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=BIRTHDAY?>
    </Td>
    <Td>
	<select name="year" class="sinput">
      <option value=0> ----- 
      <?=YEAR_VALUE?>
    </select>
    <select name="month" class="minput">
      <option value=0> --------- 
      <?=MONTH_VALUE?>
    </select>
    <select name="day" class="sinput">
      <option value=0> ----- 
      <?=DAY_VALUE?>
    </select>
    </Td>
  </Tr-->	
  
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=BIRTHDAY?>
    </Td>
    <Td>
	<?=BD_YEAR?><input type="hidden" name="year" value="<?=BD_YEAR?>">. 
	<?=BD_MONTH?><input type="hidden" name="month" value="<?=BD_MONTH?>">. 
	<?=BD_DAY?><input type="hidden" name="day" value="<?=BD_DAY?>">
    </Td>
  </Tr>	
  
  <?php } ?>
<?php if (C_CHANGE_GENDER) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=GENDER?>
    </Td>
    <Td>
    <select name="gender" class="input">
      <option value=0> --------- 
      <?=GENDER_VALUE?>
    </select>
    </Td>
  </Tr>
<?php }?>  
<?php if(ORIENTATION_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=ORIENTATION?>
    </Td>
    <Td>
    <select name="orientation" class="input">
      <option value=0> --------- 
      <?=ORIENTATION_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(BODYTYPE_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=BODYTYPE?>
    </Td>
    <Td>
    <select name="bodytype" class="input">
      <option value=0> --------- 
      <?=BODYTYPE_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(PURPOSE_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=PURPOSE?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=PURPOSE_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
  <?php }?>
  <?php if(COUNTRY_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=COUNTRY?>
    </Td>
    <Td>
    <select name="country" class="input">
      <?=COUNTRY_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(STATE_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=STATE?>
    </Td>
    <Td>
    <input class=input type=text name=state value="<?=STATE_VALUE?>">
    </Td>
  </Tr>	 
  <?php }?>
<?php if (C_CHANGE_EMAIL) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=EMAIL?>
    </Td>
    <Td>
    <input class=input type=text name=email value="<?=EMAIL_VALUE?>">
    </Td>
  </Tr>
<?php }?>
  <?php if(URL_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=URL?>
    </Td>
    <Td>
    <input class=input type=text name=url value="<?=URL_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(ICQ_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=ICQ?>
    </Td>
    <Td>
    <input class=input type=text name=icq value="<?=ICQ_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(AIM_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=AIM?>
    </Td>
    <Td>
    <input class=input type=text name=aim value="<?=AIM_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(PHONE_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=PHONE?>
    </Td>
    <Td>
    <input class=input type=text name=phone value="<?=PHONE_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(ZIP_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=ZIP?>
    </Td>
    <Td>
    <input class=input type=text name=zip value="<?=ZIP_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(CITY_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=CITY?>
    </Td>
    <Td>
    <input class=input type=text name=city value="<?=CITY_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(ADDRESS_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=ADDRESS?>
    </Td>
    <Td>
    <input class=input type=text name=address value="<?=ADDRESS_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(MARSTAT_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=MARITAL_STATUS?>
    </Td>
    <Td>
    <select name="marstat" class="input">
      <option value=0> --------- 
      <?=MARITAL_STATUS_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(CHILD_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=CHILD?>
    </Td>
    <Td>
    <select name="child" class="input">
      <option value=0> --------- 
      <?=CHILD_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(HEIGHT_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=HEIGHT?>
    </Td>
    <Td>
    <select name="height" class="input">
      <option value=0> --------- 
      <?=HEIGHT_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(WEIGHT_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=WEIGHT?>
    </Td>
    <Td>
    <select name="weight" class="input">
      <option value=0> --------- 
      <?=WEIGHT_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(HAIR_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=HAIR?>
    </Td>
    <Td>
    <select name="hcolor" class="input">
      <option value=0> --------- 
      <?=HAIR_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(EYE_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=EYE?>
    </Td>
    <Td>
    <select name="ecolor" class="input">
      <option value=0> --------- 
      <?=EYE_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(ETHNICITY_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=ETHNICITY?>
    </Td>
    <Td>
    <select name="etnicity" class="input">
      <option value=0> --------- 
      <?=ETHNICITY_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(RELIGION_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=RELIGION?>
    </Td>
    <Td>
        <div class=multi1>
            <div class=multis>
                <?=RELIGION_VALUE?>
            </div>
        </div>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SMOKE_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SMOKE?>
    </Td>
    <Td>
    <select name="smoke" class="input">
      <option value=0> --------- 
      <?=SMOKE_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(DRINK_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=DRINK?>
    </Td>
    <Td>
    <select name="drink" class="input">
      <option value=0> --------- 
      <?=DRINK_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(EDUCATION_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=EDUCATION?>
    </Td>
    <Td>
    <select name="education" class="input">
      <option value=0> --------- 
      <?=EDUCATION_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(LANG_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=LANG?>
    </Td>
    <Td>
		<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0">
	<?php $k = 1;while (isset($wlng[$k])) {?>
			<TR> 
  		<TD align="<?=C_ALIGN?>" VALIGN="TOP" width=50%>
	<?=$wlng[$k]?> <?=$checklang[$k]?> <?php $k++;?> <br>
			</Td>
	
  		<TD align="<?=C_ALIGN?>" VALIGN="TOP" width=50%>
	<?php if(isset($wlng[$k])){?><?=$wlng[$k]?> <?=$checklang[$k]?> <?php $k++;}?> <br>
			</Td></Tr>
	
	<?php }?>
  		</Table>
     </Td>
  </Tr>
  <?php }?>
  <?php if(JOB_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=JOB?>
    </Td>
    <Td>
    <input class=input type=text name=job value="<?=JOB_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(HOBBY_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=HOBBY?>
    </Td>
    <Td>
    <input class=input type=text name=hobby value="<?=HOBBY_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(INTERESTS_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=INTERESTS?>
    </Td>
    <Td align=left>    
    <div class=multi1>
    <div class=multi2>
      <?=INTERESTS_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
  <?php }?>
  <?php if(DESCRIPTION_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=DESCRIPTION?>
    </Td>
    <Td>
    <?php if(USE_SMILES || USE_BB){?>
    <Table Border=0 width="350" CellSpacing="2" CellPadding="6"><Tr align="left" bgcolor="<?=COLORH?>"><Td>
<?php if(USE_SMILES){?><?php $p=0;foreach ($smiles as $k=>$v){$p++;?><a href="javascript:ins(' <?=$k?> ',1)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/forum/smiles/<?=$v?>" border=0 /></a>
<?php if(C_SMILES_NUMBER <= $p){?><br><a href="javascript:open_win('<?=C_URL?>/forum/smiles_show.php','_blank')"><?=$w[720]?></a><br><?php break;}?><?php }?><?php }?> 
<?php if(USE_BB && USE_SMILES){?><br><br><?php }?>
<?php if(USE_BOLD){?><a href="javascript:ins('B',0)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Bold.gif" border=0 /></a><?php }?>
<?php if(USE_ITALIC){?><a href="javascript:ins('I',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Italic.gif" border=0 /></a><?php }?>
<?php if(USE_UNDERLINE){?><a href="javascript:ins('U',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Underline.gif" border=0 /></a><?php }?>
<?php if(USE_STRIKE){?><a href="javascript:ins('S',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Strike.gif" border=0 /></a><?php }?>
<?php if(USE_QUOTE){?><a href="javascript:ins('QUOTE',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Quote.gif" border=0 /></a><?php }?>  
<?php if(USE_URL){?><a href="javascript:ins('URL',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/URL.gif" border=0 /></a><?php }?>  
<?php if(USE_IMG){?><a href="javascript:ins('IMG',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Image.gif" border=0 /></a><?php }?>  
<?php if(USE_VIDEO){?><a href="javascript:ins('VIDEO',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Video.gif" border=0 /></a><?php }?>  
</td></tr></table><?php }?>
    <textarea class=forum_input cols=20 rows=12 name=descr><?=DESCRIPTION_VALUE?></textarea>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SGENDER_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_GENDER?>
    </Td>
    <Td>     
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_GENDER_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SCOUNTRY_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_COUNTRY?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multi2>
      <?=SEEKING_COUNTRY_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SMARSTAT_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_MARITAL_STATUS?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_MARITAL_STATUS_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SCHILD_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_CHILD?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_CHILD_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SHAIR_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_HAIR?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_HAIR_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SEYE_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_EYE?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_EYE_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SSMOKE_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_SMOKE?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_SMOKE_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SDRINK_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_DRINK?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_DRINK_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SEDUCATION_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_EDUCATION?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_EDUCATION_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SETHNICITY_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_ETHNICITY?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_ETHNICITY_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SRELIGION_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_RELIGION?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_RELIGION_VALUE?>
    </div>
		</div>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SAGE_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_AGE?>
    </Td>
    <Td>
    <select name="agef" class="minput"> 
      <?=SEEKING_AGE_FROM_VALUE?>
    </select>
    <select name="aget" class="minput">
      <?=SEEKING_AGE_TO_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SHEIGHT_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_HEIGHT?>
    </Td>
    <Td>
    <select name="heightf" class="minput"> 
      <?=SEEKING_HEIGHT_FROM_VALUE?>
    </select>
    <select name="heightt" class="minput">
      <?=SEEKING_HEIGHT_TO_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SWEIGHT_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_WEIGHT?>
    </Td>
    <Td>
    <select name="weightf" class="minput">
      <?=SEEKING_WEIGHT_FROM_VALUE?>
    </select>
    <select name="weightt" class="minput">
      <?=SEEKING_WEIGHT_TO_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(HDYFU_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=HOW_DID_YOU_FIND_US?>
    </Td>
    <Td>
    <select name="hdyfu" class="input">
      <option selected value=0> --------- 
      <?=HOW_DID_YOU_FIND_US_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  
  <?php if(PHOTO_SHOW && ALLOW_PHOTO) {?>
      <Tr align="center" bgcolor="<?=COLOR2?>">
          <Td colspan="2">
              <div style="font-size: 12px"><?=$w['875']?></div>
          </Td>
      </Tr>
<?php $p=0;while(isset($PHOTO_NUM[$p])) {?>
<?php if($REMOVE_PHOTO[$p] > 0) { ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td><?=$PHOTO_NUM[$p]?></Td>
    <Td><a href="<?=$REMOVE_VALUE[$p]?>&bigger=1"><img src="<?=$REMOVE_VALUE[$p]?>" border="<?=C_IMG_BRDR?>" width=100></a><br><input type="checkbox" name="delpic<?=$p?>"><?=DELETE_PHOTO?></Td>
  </Tr>
<?php } elseif($REMOVE_PHOTO[$p] < 0) { ?>
<?php } else { ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td><?=$PHOTO_NUM[$p]?></Td>
    <Td><input class=input type=file name="file<?=$p?>"></Td>
  </Tr>
<?php }?>
<?php $p++;}$p=0;?>
<?php }?>  
  <?php if(AUDIO_SHOW && ALLOW_AUDIO && C_AUDIO_ALLOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=AUDIO?>
    </Td>
    <Td>
<?php if(AUDIO_EXIST) {?>   
<?php if(IsAV(AUDIO_VALUE)) {?>
    <script language="JavaScript">
    <!--
    var soundFile = "<?=C_URL?>/members/uploads/<?=AUDIO_VALUE?>";
    if(navigator.plugins.length>0){
    document.write('<embed src="'+soundFile+'" autostart="<?=$autostart?>" loop="<?=C_AUDIO_LOOP?>" <?=audio_player(C_AUDIO_PLAYER,'0');?>></embed>');}
    else {
    document.write('<embed src="'+soundFile+'" autostart="<?=$autostart?>" loop="<?=C_AUDIO_LOOP?>" <?=audio_player(C_AUDIO_PLAYER,'1');?>></embed>');}
    //-->
    </script>
    <noscript><embed src="<?=C_URL?>/members/uploads/<?=AUDIO_VALUE?>" autostart="<?=$autostart?>" loop="<?=C_AUDIO_LOOP?>"></embed></noscript>
    <noembed><bgsound src="<?=C_URL?>/members/uploads/<?=AUDIO_VALUE?>" loop="<?=C_AUDIO_LOOP?>"></noembed>
	<?php  } else { ?>
      <OBJECT id='mediaPlayer' width="320" height="285" 
      classid='CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95' 
      codebase='http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701'
      standby='Loading Microsoft Windows Media Player components...' type='application/x-oleobject'>
      <param name='fileName' value="<?=C_URL?>/members/uploads/<?=AUDIO_VALUE?>">
      <param name='animationatStart' value='true'>
      <param name='transparentatStart' value='true'>
      <param name='autoStart' value="<?=AUTOSTART?>">
      <param name='showControls' value="true">
      <param name='loop' value="<?=C_AUDIO_LOOP?>">
      <EMBED type='application/x-mplayer2'
        pluginspage='http://microsoft.com/windows/mediaplayer/en/download/'
        id='mediaPlayer' name='mediaPlayer' displaysize='4' autosize='-1' 
        bgcolor='darkblue' showcontrols="true" showtracker='-1' 
        showdisplay='0' showstatusbar='-1' videoborder3d='-1' width="320" height="285"
        src="<?=C_URL?>/members/uploads/<?=AUDIO_VALUE?>" autostart="<?=AUTOSTART?>" designtimesp='5311' loop="<?=C_AUDIO_LOOP?>">
      </EMBED>
      </OBJECT>
			<?php }?>
    <br>
    <input type="checkbox" name="delaudio"><?=DELETE?>
<?php } else {?>
    <input class=input type=file name="filea">
<?php }?> 
    </Td>
  </Tr>
<?php }?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <td colspan=2 align=right>
    <input class=input1 type=submit value="<?=CHANGE_PROFILE?>" name="submit">
    </td>
  </tr>
  </table>
  </Td>
</Tr>
</table>
</form>
<br>
<br><br><br><br>

     </TD>
    </TR>
   </TABLE>

