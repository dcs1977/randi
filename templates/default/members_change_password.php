<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
   <TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="6">
    <TR> 
     <TD ALIGN="CENTER" VALIGN="TOP">

<br><br><br>

<script language="JavaScript">
<!--
function formCheck(form) {
  <?php if (NOT_FORGOTTEN_PASSWORD):?>
  if (form.oldpass.value == "") {
    alert("<?=ENTER_OLD_PASSWORD?>");
    return false;
  }
  <?php endif;?>
  if (form.newpass.value == "") {
    alert("<?=ENTER_NEW_PASSWORD?>");
    return false;
  }
  if (form.repass.value == "") {
    alert("<?=RETYPE_NEW_PASSWORD?>");
    return false;
  }
  if (form.newpass.value != form.repass.value) {
    alert("<?=PASSWORDS_NOT_IDENTICAL?>");
    return false;
  }

  if (document.form.submit.action != "") {
    document.form.submit.disabled=1;
  }
}
// -->
</script>
<form action="index.php" method="post" name=form OnSubmit="return formCheck(this)">
<input class=input type=hidden name="l" value="<?=LANGUAGE?>">
<input class=input type=hidden name="a" value="x">
<span class=head><?=CHANGE_PASSWORD?></span>
<br><br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>">
<Tr>
  <Td>
  <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
  <?php if (NOT_FORGOTTEN_PASSWORD):?>
  <Tr bgcolor="<?=COLOR1?>">
    <Td>
    <?=OLD_PASSWORD?>
    </Td>
    <td>
    <input type="password" name="oldpass" class="input">
    </td>
  </Tr>
  <?php else:?>
  <input class=input type=hidden name="fpw" value="1">
  <?php endif;?>
  <Tr bgcolor="<?=COLOR1?>">
    <Td>
    <?=NEW_PASSWORD?>
    </Td>
    <td>
    <input type="password" name="newpass" maxlength="<?=C_PASSB?>" class="input">
    </td>
  </Tr>
  <Tr bgcolor="<?=COLOR1?>">
    <Td>
    <?=RETYPE_NEW_PASSWORD?>
    </Td>
    <td>
    <input type="password" name="repass" maxlength="<?=C_PASSB?>" class="input">
    </td>
  </Tr>
  <Tr bgcolor="<?=COLOR1?>" align=center>
    <Td colspan="2">
    <input class=input1 type=submit value="<?=CHANGE_PASSWORD?>" name="submit">
    </td>
  </tr>
  </Table>
  </td>
</Tr>
</Table>
</form>


<br><br><br><br>

     </TD>
    </TR>
   </TABLE>
