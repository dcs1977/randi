<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>


   <TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="6">
    <TR> 
     <TD ALIGN="CENTER" VALIGN="TOP">

<script language="JavaScript">
<!--
function ins(prop,type)
{
	var input = document.form.message;
	var mysel;var text;
	if (window.getSelection) mysel = window.getSelection();
	else if (document.selection) mysel = document.selection.createRange().text;
	else if (document.getSelection)	mysel = document.getSelection();
	b = (type == 0) ? '['+prop+']' : prop;
	e = (type == 0) ? '[/'+prop+']' : '';
  if (input.createTextRange)
  {
	  var text;
    input.focus(input.caretPos);
    input.caretPos = document.selection.createRange().duplicate();
    input.caretPos.text = (input.caretPos.text.length>0) ? b + input.caretPos.text + e : b + mysel + e;
   }
   else input.value += b + mysel + e;
}

function formCheck(form) {
    if (form.message.value == "") {
        alert("<?=ENTER_YOUR_MESSAGE?>");
        return false;
    }

    document.form.submit.disabled=1;

    if (captchaContainer !== null){
        return true;
    }

    var form = $('#messageForm')[0];
    var formData = new FormData(form);

    $.ajax({
        method: 'POST',
        url: "send.php",
        processData: false,
        contentType: false,
        dataType: "json",
        data: formData
    })
    .done(function(msg){
        //console.log(msg);
        if (msg.status == 200){
            location = msg.href;
        } else if (msg.status == 500){
            const nodeBadge = document.querySelector('.grecaptcha-badge');
            if (nodeBadge) {
                document.body.removeChild(nodeBadge.parentNode);
            }

            const scriptSelector = 'script[src=\'https://www.google.com/recaptcha/api.js?render=<?=RECAPTCHA_PUBLIC_KEY?>\']';
            const script = document.querySelector(scriptSelector);
            if (script) {
                script.remove();
            }

            captchaContainer = grecaptcha.render('captcha_container', {
                'sitekey' : '<?=OLD_RECAPTCHA_PUBLIC_KEY?>',
                'callback' : function(response) {
                    document.form.submit.disabled = false;
                }
            });

            $('#recaptcha_token').remove();
            return false;
        }
    });
    return false;
}
// -->
</script>

<script>
    var captchaContainer = null;

    grecaptcha.ready(function () {
        grecaptcha.execute('<?=RECAPTCHA_PUBLIC_KEY?>', {action: 'message'}).then(function(token) {
            $('#messageForm').prepend('<input id="recaptcha_token" type="hidden" name="token" value="' + token + '">');
            $('#messageForm').prepend('<input type="hidden" name="action" value="message">');
            document.form.submit.disabled = false;
        });
        setInterval(function () {
            grecaptcha.execute('<?=RECAPTCHA_PUBLIC_KEY?>', { action: 'message' }).then(function (token) {
                if ($("#recaptcha_token").length) {
                    $("#recaptcha_token").val(token);
                }
            });
        }, 60000);
    });
</script>

<form action="?" method="post" name=form id="messageForm" OnSubmit="return formCheck(this)" enctype="multipart/form-data" >
<input class=input type=hidden name="l" value="<?=LANGUAGE?>">
<input class=input type=hidden name="id" value="<?=ID_VALUE?>">
<input class=input type=hidden name="a" value="s">
<p>
<center><span class=head><?=SEND_MESSAGE_TO_USER?></span>
</p>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" align=center width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>">
<Tr>
  <Td width="<?=C_WIDTH?>" bgcolor="<?=COLOR1?>" valign="top">
  <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
  <Tr bgcolor="<?=COLOR1?>">
	<Td align="<?=C_ALIGN?>" width="30%"><b><?=$w[228]?></b></Td>
	<Td width="70%"><?=USER_NAME?></Td>
  </Tr>

      <Tr class="email_again" align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
          <Td width="30%">
              Ha látod ezt a mezőt, akkor kérlek hagyd üresen!
          </td>
          <Td width="70%" align="left">
              <input class=input type="text" name="subject-honeypot" autocomplete="off">
          </td>
      </tr>

  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td width="30%"><b><?=MESSAGE?></b></Td>
    <Td width="70%">
<?php if(USE_SMILES || USE_BB){?>
    <Table Border=0 width="350" CellSpacing="2" CellPadding="6"><Tr align="left" bgcolor="<?=COLORH?>"><Td>
<?php if(USE_SMILES){?><?php $p=0;foreach ($smiles as $k=>$v){$p++;?><a href="javascript:ins(' <?=$k?> ',1)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/forum/smiles/<?=$v?>" border=0 /></a>
<?php if(C_SMILES_NUMBER <= $p){?><br><a href="javascript:open_win('<?=C_URL?>/forum/smiles_show.php','_blank')"><?=$w[720]?></a><br><?php break;}?><?php }?><?php }?> 
<?php if(USE_BB && USE_SMILES){?><br><br><?php }?>
<?php if(USE_BOLD){?><a href="javascript:ins('B',0)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Bold.gif" border=0 /></a><?php }?>
<?php if(USE_ITALIC){?><a href="javascript:ins('I',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Italic.gif" border=0 /></a><?php }?>
<?php if(USE_UNDERLINE){?><a href="javascript:ins('U',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Underline.gif" border=0 /></a><?php }?>
<?php if(USE_STRIKE){?><a href="javascript:ins('S',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Strike.gif" border=0 /></a><?php }?>
<?php if(USE_QUOTE){?><a href="javascript:ins('QUOTE',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Quote.gif" border=0 /></a><?php }?>  
<?php if(USE_URL){?><a href="javascript:ins('URL',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/URL.gif" border=0 /></a><?php }?>  
<?php if(USE_IMG){?><a href="javascript:ins('IMG',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Image.gif" border=0 /></a><?php }?>  
<?php if(USE_VIDEO){?><a href="javascript:ins('VIDEO',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Video.gif" border=0 /></a><?php }?>  
</td></tr></table><?php }?>
<textarea class=forum_input name=message cols=40 rows=12></textarea></Td></Tr>

<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td width="30%">
    <b><?=$w[87]?></b>
    </td>
    <Td width="70%" align="left">
    <input alt="" name="image" type="file">
    </td>
  </tr>

  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td colspan=2>
    <input type="checkbox" name="confirm"<?=CONFIRM?>> <?=NOTIFY_ME?>
    </td>
  </tr>
  <?php if (VERIFY_ALLOW) {?>
  	<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td><?=VERIFICATION?><br><img src="<?=C_URL?>/img.php" border=0></Td>
    <Td><input class=minput type=text name=verifyimage></Td>
  </Tr>
	<?php }?>  
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <td colspan=2 align=right>
        <input class=input1 type=submit value="<?=SEND_MESSAGE?>" name="submit" disabled>
    </Td>
</Tr>
      <Tr bgcolor="<?=COLOR1?>">
          <Td colspan="2">
              <div style="text-align: center" id="captcha_container"></div>
              <style>
                  #captcha_container div{
                      margin: auto;
                  }
              </style>
          </Td>
      </Tr>
  </table></td></tr></table></form><br><br></TD></TR></TABLE>
