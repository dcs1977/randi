<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>

<script language="JavaScript">
function sel() {
	with(document.form) {
		for(i=0;i<elements.length;i++) {
			thiselm = elements[i];
			if(thiselm.name.substring(0,3) == 'act')
			thiselm.checked = !thiselm.checked
		}
	}
}

function CheckAct() {
	with(document.form) {
		if(myactions.value == "" || myactions.value == "0") {
			alert("<?=$w[432]?>");
		} else {
			cnt=0;
			for(i=0;i<elements.length;i++) {thiselm = elements[i];
			if(thiselm.name.substring(0,3) == 'act') if(thiselm.checked) {
				cnt=cnt+1;
				v = thiselm.value.split("|");
				if(myactions.value >= 1 && myactions.value <= 8) {
					if(myactions.value == "1") {
						window.open("<?=C_URL?>/view.php?l=<?=LANGUAGE?>&id="+v[0], "_blank");
					} else if(myactions.value == "2") {
						window.open("<?=C_URL?>/sendk.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					} else if(myactions.value == "3") {
						window.open("<?=C_URL?>/send.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					} else if(myactions.value == "5") {
						window.open("<?=C_URL?>/mail.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					} else if(myactions.value == "6") {
						window.open("<?=C_URL?>/bedroom.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					} else if(myactions.value == "7") {
						window.open("<?=C_URL?>/ban.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					} else {
						window.open("<?=C_URL?>/bad.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					}
				}
			}
			}
			if(cnt == 0) alert("<?=$w[555]?>");
		}
	}
}
</script>
<form action="index.php" method="post" name=form>
<input class=input type=hidden name="l" value="<?=LANGUAGE?>">
<input class=input type=hidden name="a" value="d">
<TABLE align=center WIDTH="98%" BORDER="0" CELLSPACING="0" CELLPADDING="0"><TR><TD>

<br><div align="center"><span class=head>
<?php if(defined("VIEW_CREATED") && VIEW_CREATED){?><?=VIEW_CREATED?><br><br><?php }?>
<?=SEARCH_RESULTS?></span><br><br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_BWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td><Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_BWIDTH?>" class=mes>
  <Tr align="center" bgcolor="<?=COLORH?>">
	<td colspan="2"></td>
    <td>
    <input type="checkbox" onClick="sel()">
    </td>
  </Tr>
<?php  while(isset($ids[$p])) { /* Search results - begin */?>  
  <?php if(C_SR_BANNER) show_banners('search_results',C_SR_BANNER,$p);?>
  <Tr align="center" bgcolor="<?=$colors[$p]?>">
    <Td width=250>
		<p><a class=link2 href="<?=GenLink('UserProfile',array(LANGUAGE,$ids[$p]))?>"><?=$names[$p]?></a></p>
		<?php if(SHOW_ONLINE_STATUS){?><?php if(IsOnline($ids[$p])){?> <span style="color:green"><B><?=ONLINE?></B></span> 
		<?php }else{?> <?=OFFLINE?> <?php }?>
		
		<?php if(SHOW_PHOTO){?><p style="text-align: center; position: relative; margin: 0 60px;"><?=$pictures[$p]?><span style="position: absolute;bottom: 2px;left: 50%; margin-left: -50px; width: 100px;background-color: transparent;color: #fff; font-weight: bold;text-align: center;"><?=$photoinfo[$p]?> kép</span></p><?php }?>
		
		<?php if(SHOW_AGE){?><p><?=$ages[$p]?> éves</p><?php }?>
		<?php if(SHOW_ORIENTATION){?><p><?=$orientations[$p]?></p><?php }?>
		
	</td>
    
	<Td align=left valign=top>
        
    <table Border=0 CellSpacing="1" CellPadding="1" width="100%" class=mes>
	<?php if(WHENSEEN === true){?><tr><td width=200><b><font color="#FFFF00"><?=DATE?>:</font></b></td><td width=50%><font color="#FFFF00"><?=$visdate[$p]?></font></td></tr><?php }?>
		<?php if(SHOW_COUNTRY){?><tr><td width=200><b><?=COUNTRY?>:</b></td><td><?=$countries[$p]?></td></tr><?php }?>
		<?php if(SHOW_CITY){?><tr><td><b><?=CITY?>:</b></td><td><?=$cities[$p]?></td></tr><?php }?>
		<?php if(SHOW_PURPOSE){?><tr><td><b><?=PURPOSES?>:</b></td><td><?=$purposes[$p]?></td></tr><?php }?>
		<?php if(SHOW_BODYTYPE){?><tr><td><b><?=BODYTYPE?>:</b></td><td><?=$bodytypes[$p]?></td></tr><?php }?>
		<?php if(SHOW_HEIGHT){?><tr><td><b><?=HEIGHT?>:</b></td><td><?=$heights[$p]?></td></tr><?php }?>
		<?php if(SHOW_WEIGHT){?><tr><td><b><?=WEIGHT?>:</b></td><td><?=$weights[$p]?></td></tr><?php }?>
		<?php if(CHILD){?><tr><td><b><?=CHILD?>:</b></td><td><?=$childs[$p]?></td></tr><?php }?>
		<?php if(SHOW_LAST_ACCESS){?><tr><td><b><?=LAST_ACCESS?>:</b></td><td><?=$editdates[$p]?></td></tr><?php }?>
    <?php if(SHOW_RATING && TOPS){?><tr><td><b><?=RATING?>:</b></td><td><?=$rates[$p]?></td></tr><?php }?>
		</td></tr><?php }?>
    </table></td>
    <td><input type="checkbox" name="act[]" value="<?=$ids[$p].'|'.$names[$p]?>"></td>
  </Tr>
<?php  $p++;} /* Search results - end */?>
  </table>
  </td>
</tr>
</table>
<BR>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_BWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td><Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_BWIDTH?>" class=mes><?=PAGES?></table></td></tr></table>
<BR>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_BWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td><Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_BWIDTH?>" class=mes><tr><td bgcolor="<?=COLORH?>" align=right>
<?=WITH_SELECTED?> 
<select name=myactions class="select">
<option value=0> --------
<?=ShowActions()?>
</select> 
<input type="button" value="<?=$w[263]?>" class=button OnClick="javascript:CheckAct()"> 
</td></tr>
</table></td></tr></table>
  </TD>
 </TR>
</TABLE>
</form><br><br><br>
</div>