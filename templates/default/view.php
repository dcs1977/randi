<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
   <TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="6">
    <TR> 
     <TD ALIGN="CENTER" VALIGN="TOP"><p>
<h3><span class=yellow><?=USERNAME_VALUE?></span></h3>
</p>

<table CellSpacing="0" CellPadding="0" width="<?=C_BWIDTH?>" border=0>
<Tr>
  <Td valign="top">
<?php if (SHOW_PHOTOS) { /* If allow to show photos - Begin */ ?>
<?php while(isset($pictures[$p])) { /* Show Photos - Begin */ ?>
  <a href="<?=$photos[$p]?>"><img src="<?=$pictures[$p]?>" border="<?=C_IMG_BRDR?>" width="140"></a><br><br>
<?php $p++;if(C_VIEW_PHOTOS_SHOW == $p) break;} /* Show Photos - End */ ?>
<?php } else { /* If allow to show photos - End */ ?>
   &nbsp; 
<?php }?> 
<?=TOTAL_PHOTOS?>: <strong><?=PICTURES_NUMBER?></strong>
<?php if(IS_NOT_CHECKED) {?>
<BR>
  <BR>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="140" bgcolor="<?=C_TBCOLOR?>">
  <Tr>
    <Td>
    <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="140" class=mes>
    <Tr align="center" bgcolor="<?=COLOR1?>">
      <Td HEIGHT="24" colspan="2">
<?=IS_NOT_CHECKED?>
    </Td>
  </Tr>
</table>
  </td>
</tr>
</table>
<?php }?>
<?php if(PICTURES_NUMBER > C_VIEW_PHOTOS_SHOW) {?>
<br><a href="<?=$photos[$p]?>"><?=SEE_ANOTHER?></a>
<?php }$p=0;?>
  &nbsp;
  <?php if(PRIVATE_PHOTOS_NUM){?><br><br><a href="<?=C_URL?>/private_photos.php?l=<?=LANGUAGE?>&id=<?=ID?>"><?=PRIVATE_PHOTOS_TOTAL?></a></strong><?php }?>

<?php if(SLIDESHOW_SHOW) {?><br><br><a href="javascript:slide_win('<?=C_URL?>/slideshow.php?l=<?=LANGUAGE?>&id=<?=ID?>','_blank');" class=link2><?=SLIDESHOW?></a><?php }?>
  </td>
  <td width="<?=C_WIDTH?>" valign="top" align="right">
  <Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>">
  <Tr>
    <Td>
    <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class="mes prof">
    <Tr align="center" style="background-color:<?=COLOR2?>">
      <Td HEIGHT="24" colspan="2">
<?php if(SEND_KISS && !BANNED) {?>
      <a class=link2 href="javascript:open_win('<?=C_URL?>/sendk.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>','wink');"><?=SEND_KISS_MESSAGE?></a> &nbsp; 
<?php }?>
<?php if(SEND_SHOW && !BANNED) {?>
      <a class=link2 href="<?=C_URL?>/send.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>"><?=SEND_MESSAGE?></a> &nbsp; 
<?php }?>
<?php if(MAIL_SHOW && !BANNED) {?>
      <a class=link2 href="<?=C_URL?>/mail.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>"><?=MAIL_MESSAGE?></a> &nbsp; 
<?php }?>
<?php if(BED_SHOW && !BANNED) {?>
      <a class=link2 href="javascript:open_win('<?=C_URL?>/bedroom.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>','bed');"><?=BED_MESSAGE?></a> &nbsp; 
<?php }?>
<?php if(BAN_SHOW) {?>
	<?php if(ON_MYBANLIST){?>
      <a class=link2 href="javascript:open_win('<?=C_URL?>/unban.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>','unban');"><?=UNBAN_MESSAGE?></a> &nbsp; 
	<?php } else {?>
	  <a class=link2 href="javascript:open_win('<?=C_URL?>/ban.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>','ban');"><?=BAN_MESSAGE?></a> &nbsp; 
	<?php }?>
<?php }?>
<?php if(BAD_SHOW) {?>
      <a class=link2 href="javascript:open_win('<?=C_URL?>/bad.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>','bad');"><?=BAD_MESSAGE?></a>
<?php }?>
<?php if(MyAccess() < 0) echo $w[152];?>
      </td>
    </tr>

<?php if(BANNED){?>
	<?php if(ON_MYBANLIST){?>
		<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
		  <td colspan="2">
		  <center style="color: yellow; font-weight: bold; background-color: blue"><?=ON_BANLIST?></center>
		  </td>
		</tr>
	<?php } else{?>
		<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
		  <td colspan="2">
		  <center style="color: yellow; font-weight: bold; background-color: blue">Ez a felhasználó bannolt téged</center>
		  </td>
		</tr>
	<?php }?>
<?php }?>

<?php if(HIDDEN){?>
	<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <td colspan="2">
      <center style="color: yellow; font-weight: bold; background-color: blue">Ez a felhasználó rejtetten van jelen</center>
      </td>
    </tr>
<?php }?>

<?php if(DESCR_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=DESCR?></b>
      </td>
      <td>
      <?=DESCR_VALUE?>
      </td>
    </tr>
<?php }?>
	
<?php if(AGE_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=AGE?></b>
      </td>
      <td>
      <?=AGE_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(HOROSCOPE_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=HOROSCOPE?></b>
      </td>
      <td>
      <?=HOROSCOPE_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(GENDER_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=GENDER?></b>
      </td>
      <td>
      <?=GENDER_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(ORIENTATION_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=ORIENTATION?></b>
      </td>
      <td>
      <?=ORIENTATION_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(BODYTYPE_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=BODYTYPE?></b>
      </td>
      <td>
      <?=BODYTYPE_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(PURPOSES_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=PURPOSES?></b>
      </td>
      <td>
      <?=PURPOSES_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(COUNTRY_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=COUNTRY?></b>
      </td>
      <td>
      <?=COUNTRY_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(EMAIL_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=EMAIL?></b>
      </td>
      <td>
      <?=EMAIL_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(URL_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=URL?></b>
      </td>
      <td>
			<?php if(IsAccess(C_URLSH)) {?>
      <a href="<?=URL_VALUE?>"><?=URL_VALUE?></a>
			<?php }else{?>
			<?=URL_VALUE?>
			<?php }?>
      </td>
    </tr>
<?php }?>
<?php if(ICQ_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=ICQ?></b>
      </td>
      <td>
			<?php if(IsAccess(C_ICQSH)) {?>
      <?=ICQ_VALUE?> <img src="http://web.icq.com/whitepages/online?icq=<?=ICQ_VALUE?>&img=5">
			<?php }else{?>
			<?=ICQ_VALUE?>
			<?php }?>
      </td>
    </tr>
<?php }?>
<?php if(AIM_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=AIM?></b>
      </td>
      <td>
      <?=AIM_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(PHONE_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=PHONE?></b>
      </td>
      <td>
      <?=PHONE_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(STATE_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=STATE?></b>
      </td>
      <td>
      <?=STATE_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(ZIP_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=ZIP?></b>
      </td>
      <td>
      <?=ZIP_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(CITY_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=CITY?></b>
      </td>
      <td>
      <?=CITY_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(ADDRESS_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=ADDRESS?></b>
      </td>
      <td>
      <?=ADDRESS_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(MARSTAT_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=MARSTAT?></b>
      </td>
      <td>
      <?=MARSTAT_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(LINK_SHOW && !BANNED && !HIDDEN) {?>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
   <Td>
  <b><?=LINKED_PROFILE?></b>
   </td>
   <td><a class=link3 href="<?=GenLink('UserProfile',array(LANGUAGE,LINK_VALUE))?>"><?=LINKED_PROFILE_TXT?></a>
   </td>
</tr>
<?php }?>
<?php if(CHILD_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=CHILD?></b>
      </td>
      <td>
      <?=CHILD_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(HEIGHT_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=HEIGHT?></b>
      </td>
      <td>
      <?=HEIGHT_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(WEIGHT_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=WEIGHT?></b>
      </td>
      <td>
      <?=WEIGHT_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(HCOLOR_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=HCOLOR?></b>
      </td>
      <td>
      <?=HCOLOR_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(ECOLOR_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=ECOLOR?></b>
      </td>
      <td>
      <?=ECOLOR_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(ETHNICITY_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=ETHNICITY?></b>
      </td>
      <td>
      <?=ETHNICITY_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(RELIGION_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=RELIGION?></b>
      </td>
      <td>
      <?=RELIGION_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(SMOKE_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=SMOKE?></b>
      </td>
      <td>
      <?=SMOKE_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(DRINK_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=DRINK?></b>
      </td>
      <td>
      <?=DRINK_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(EDUCATION_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=EDUCATION?></b>
      </td>
      <td>
      <?=EDUCATION_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(LANG_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=KNOWN_LANGUAGES?></b>
      </td>
      <td>
      <?=LANG_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(JOB_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=JOB?></b>
      </td>
      <td>
      <?=JOB_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(HOBBY_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=HOBBY?></b>
      </td>
      <td>
      <?=HOBBY_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(INTERESTS_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=INTERESTS?></b>
      </td>
      <td>
      <?=INTERESTS_VALUE?>
      </td>
    </tr>
<?php }?>

<?php if(SGENDER_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=SGENDER?></b>
      </td>
      <td>
      <?=SGENDER_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(SCOUNTRY_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=SCOUNTRY?></b>
      </td>
      <td>
      <?=SCOUNTRY_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(SMARSTAT_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=SMARSTAT?></b>
      </td>
      <td>
      <?=SMARSTAT_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(SCHILD_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=SCHILD?></b>
      </td>
      <td>
      <?=SCHILD_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(SHCOLOR_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=SHCOLOR?></b>
      </td>
      <td>
      <?=SHCOLOR_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(SECOLOR_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=SECOLOR?></b>
      </td>
      <td>
      <?=SECOLOR_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(SSMOKE_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=SSMOKE?></b>
      </td>
      <td>
      <?=SSMOKE_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(SDRINK_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=SDRINK?></b>
      </td>
      <td>
      <?=SDRINK_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(SEDUCATION_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=SEDUCATION?></b>
      </td>
      <td>
      <?=SEDUCATION_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(SETHNICITY_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=SETHNICITY?></b>
      </td>
      <td>
      <?=SETHNICITY_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(SRELIGION_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=SRELIGION?></b>
      </td>
      <td>
      <?=SRELIGION_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(SAGE_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=SAGE?></b>
      </td>
      <td>
      <?=SAGEF_VALUE?> - <?=SAGET_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(SHEIGHT_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=SHEIGHT?></b>
      </td>
      <td>
      <?=SHEIGHTF_VALUE?> - <?=SHEIGHTT_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(SWEIGHT_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=SWEIGHT?></b>
      </td>
      <td>
      <?=SWEIGHTF_VALUE?> - <?=SWEIGHTT_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(REGDATE_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=REGDATE?></b>
      </td>
      <td>
      <?=REGDATE_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(ACCDATE_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=ACCDATE?></b>
      </td>
      <td>
      <?=ACCDATE_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(USERTYPE_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=USERTYPE?></b>
      </td>
      <td>
      <?=USERTYPE_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(ONLINESTAT_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=ONLINESTAT?></b>
      </td>
      <td>
      <?=ONLINESTAT_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(FAVORITES_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=USER_FAVORITES?></b>
      </td>
      <td>
      <?=FAVORITES_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(VIEWS_SHOW && VIEWS_VALUE && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=VIEWS?></b>
      </td>
      <td>
      <?=VIEWS_VALUE?>
      </td>
    </tr>
<?php }?>

<?php if(POPULARITY_SHOW && POPULARITY_VALUE && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=POPULARITY?></b>
      </td>
      <td>
      <?=POPULARITY_VALUE?>
      </td>
    </tr>
<?php }?>
<?php if(AUDIO_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
      <b><?=AUDIO?></b>
      </td>
      <Td>
      <?php if(IsAV(AUDIO_VALUE)) {?>
      <script language="JavaScript">
      <!--
      var soundFile = "<?=C_URL?>/members/uploads/<?=AUDIO_VALUE?>";
      if(navigator.plugins.length>0){
        document.write('<embed src="'+soundFile+'" autostart="<?=AUTOSTART?>" loop="<?=C_AUDIO_LOOP?>" <?=audio_player(C_AUDIO_PLAYER,'0');?>></embed>');}
      else {
        document.write('<embed src="'+soundFile+'" autostart="<?=AUTOSTART?>" loop="<?=C_AUDIO_LOOP?>" <?=audio_player(C_AUDIO_PLAYER,'1');?>></embed>');}
      //-->
      </script>
      <noscript><embed src="<?=C_URL?>/members/uploads/<?=AUDIO_VALUE?>" autostart="<?=AUTOSTART?>" loop="<?=C_AUDIO_LOOP?>"></embed></noscript>
      <noembed><bgsound src="<?=C_URL?>/members/uploads/<?=AUDIO_VALUE?>" loop="<?=C_AUDIO_LOOP?>"></noembed>
      <?php  } else { ?>
      <OBJECT id='mediaPlayer' width="320" height="285" 
      classid='CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95' 
      codebase='http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701'
      standby='Loading Microsoft Windows Media Player components...' type='application/x-oleobject'>
      <param name='fileName' value="<?=C_URL?>/members/uploads/<?=AUDIO_VALUE?>">
      <param name='animationatStart' value='true'>
      <param name='transparentatStart' value='true'>
      <param name='autoStart' value="<?=AUTOSTART?>">
      <param name='showControls' value="true">
      <param name='loop' value="<?=C_AUDIO_LOOP?>">
      <EMBED type='application/x-mplayer2'
        pluginspage='http://microsoft.com/windows/mediaplayer/en/download/'
        id='mediaPlayer' name='mediaPlayer' displaysize='4' autosize='-1' 
        bgcolor='darkblue' showcontrols="true" showtracker='-1' 
        showdisplay='0' showstatusbar='-1' videoborder3d='-1' width="320" height="285"
        src="<?=C_URL?>/members/uploads/<?=AUDIO_VALUE?>" autostart="<?=AUTOSTART?>" designtimesp='5311' loop="<?=C_AUDIO_LOOP?>">
      </EMBED>
      </OBJECT>
			<?php }?>
      </td>
    </tr>
<?php }?><!--1-->
<?php if(RATE_SHOW && !BANNED && !HIDDEN) {?>
    <Tr align="<?=C_ALIGN?>" style="background-color:<?=COLOR4?>">
      <Td align=center colspan=2>
      <?=RATE?>
      </td>
    </tr>
    <Tr align="<?=C_ALIGN?>" style="background-color:<?=COLOR4?>">
      <Td align=center colspan=2> 
			<?php  for($i=1;$i<=10;$i++) { ?>
      <a href="javascript:open_win('<?=C_URL?>/rate.php?l=<?=LANGUAGE?>&id=<?=ID?>&rate=<?=$i?>','rate');"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/rate_<?=$i?>.gif" width=30 height=30 border=0></a>
			<?php }?>
      </td>
    </tr>
<?php }?>

    <Tr align="center" style="background-color:<?=COLOR2?>">
      <Td colspan="2">
<?php if(SEND_KISS && !BANNED) {?>
      <a class="link2" href="javascript:open_win('<?=C_URL?>/sendk.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>','wink');"><?=SEND_KISS_MESSAGE?></a> &nbsp; 
<?php }?>
<?php if(SEND_SHOW && !BANNED) {?>
      <a class="link2" href="<?=C_URL?>/send.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>"><?=SEND_MESSAGE?></a> &nbsp; 
<?php }?>

<?php if(MAIL_SHOW && !BANNED) {?>
      <a class="link2" href="<?=C_URL?>/mail.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>"><?=MAIL_MESSAGE?></a> &nbsp; 
<?php }?>
<?php if(BED_SHOW && !BANNED) {?>
      <a class="link2" href="javascript:open_win('<?=C_URL?>/bedroom.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>','bed');"><?=BED_MESSAGE?></a> &nbsp; 
<?php }?>
<?php if(BAN_SHOW) {?>
	<?php if(ON_MYBANLIST){?>
      <a class=link2 href="javascript:open_win('<?=C_URL?>/unban.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>','unban');"><?=UNBAN_MESSAGE?></a> &nbsp; 
	<?php } else {?>
	  <a class=link2 href="javascript:open_win('<?=C_URL?>/ban.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>','ban');"><?=BAN_MESSAGE?></a> &nbsp; 
	<?php }?>
<?php }?>
<?php if(BAD_SHOW) {?>
      <a class="link2" href="javascript:open_win('<?=C_URL?>/bad.php?l=<?=LANGUAGE?>&id=<?=ID?>&uname=<?=NAME_VALUE?>','bad');"><?=BAD_MESSAGE?></a>
<?php }?>
      </td>
    </tr>
	<?php if (COUPON !== FALSE) {?>
  	<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>Kuponkód:</Td>
    <Td>
	<form method="post">
		<input type="text" name="kupon" value="<?=COUPON?>">
		<input type="submit" value="OK">
	</form>
	</Td>
  </Tr>
	<?php }?>
        <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
            <Td>#ID:</Td>
            <Td>
                <?=ID?>
            </Td>
        </Tr>
    </table>
    </td>
  </tr>
  </table>
	
<?php if(SHOW_VIEWERS) {?>
	<br><br>
	  <Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>">
  <Tr>
    <Td>
    <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td align=center>
			<strong><?=WHO_SEEN?></strong>
      </td>
    </tr>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
<?php if(defined("NOBODY_SEEN_PROFILE")) {?>
			<div align="center"><?=NOBODY_SEEN_PROFILE?></div>
<?php } else {?>

            <TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3">
  <Tr align="center" bgcolor="<?=COLORH?>" class=mes>
              <TD ><?=USERNAME?></TD>
              <TD ><?=GENDER?></TD>
              <TD ><?=AGE?></TD>
              <TD ><?=DATE?></TD>
             </TR>
	<?php while(isset($visid[$p])) {?>     
  <Tr align="center" bgcolor="<?=$colors[$p]?>" class=mes>
              <TD><A href="<?=GenLink('UserProfile',array(LANGUAGE,$visid[$p]))?>"><?=$names[$p]?></A></TD>
              <TD><?=$genders[$p]?></TD>
              <TD><?=$ages[$p]?></TD>
              <TD><?=$visdate[$p]?></TD>
             </TR>
<?php $p++;}$p=0; /* If popular users exist - Begin */ ?>  
			<?=PAGES?>      
      </TABLE>
			<?php }?>			
      </td>
    </tr>
		</table>
  	</td>
	</tr>
	</table>
<?php }?>

                 <?php //if(MyID() == ID){?>
                  <script language="JavaScript">
                  <!--
                  function ConfirmDeleteComment(CommentId)
                  {
                  	if(confirm('<?=WARNING?>'))
                  	{
                  		open_win('<?=C_URL?>/comments.php?l=<?=LANGUAGE?>&cid='+CommentId+'&remove','remove');
                  	}
                  }
                  // -->
									</script>
									<?php //}?>             

<?php if(SHOW_COMMENTS) {?>
<?php if(defined("NO_COMMENTS")) {?>
<br><br>
<div align="center"><strong><?=NO_COMMENTS?></strong></div>
<?php }else{?>


	<br><br>
<div align="center">			<strong><?=COMMENTS?></strong></div>
	<?php $p=0;while(isset($cfromid[$p])) {?>     
	  <Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>">
  <Tr>
    <Td>
    <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
    <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
      <Td>
            <TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3">
  <Tr align="center" bgcolor="<?=COLOR3?>" class=mes>
              <TD colspan="2"><?=$cdate[$p]?></TD>
             </TR>
  <Tr align="center" bgcolor="<?=COLOR3?>" class=mes>
              <TD><?=$ccomment[$p]?></TD>
              <TD>
							<?=$cphotos[$cfromid[$p]]?><br>
							<?php if($cfromid[$p]){?><a class="font2" href="<?=GenLink('UserProfile',array(LANGUAGE,$cfromid[$p]))?>"><?=$cnames[$cfromid[$p]]?></a><?php } else {?><?=$cnames[$cfromid[$p]]?><?php }?>
							</TD>
             </TR>
<?php if(!empty($creply[$p])){?>
  <Tr align="center" bgcolor="<?=COLOR3?>" class=mes>
              <TD style='color:red'><?=$creply[$p]?></TD>
              <TD valign=top><?=$rdate[$p]?></TD>
  </TR>
<?php }?>	
<?php if(MyID() == ID OR MyID() == $cfromid[$p]){?>
  <Tr align="center" bgcolor="<?=COLOR3?>" class=mes>
              <TD colspan=2>
							<?php if(SHOW_REPLY_LINK AND MyID() == ID){?><a class=link2 href="javascript:open_win('<?=C_URL?>/comments.php?l=<?=LANGUAGE?>&cid=<?=$cid[$p]?>&reply','reply');"><?=REPLY?></a><?php }?> &nbsp; 
							<?php if(SHOW_REMOVE_LINK){?><a class=link2 href="javascript:ConfirmDeleteComment('<?=$cid[$p]?>');"><?=DELETE?></a><?php }?> &nbsp;
							</TD>
  </TR>
<?php }?>	
      </TABLE>
      </td>
    </tr>
		</table>
  	</td>
	</tr>
	</table><br>
<?php $p++;}$p=0; /* If popular users exist - Begin */ ?>  

<?php if(CPAGES){?>
<br>
	  <Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>">
  <Tr>
    <Td>
    <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
<?=CPAGES?>  
		</table>
  	</td>
	</tr>
	</table>
	<?php }?> 
<?php }?> 
<?php }?> 

<?php if(SHOW_COMMENTS_FORM){?>
<script language="JavaScript">
<!--
function formCheck(form) {
if (form.message.value=="") {
    alert("<?=MESSAGE_ERROR?>");
    return false;
}
if (document.form.submit.action != "") {
document.form.submit.disabled=1;}
}
// -->
</script>
<script language="JavaScript">
<!--
function ins(prop,type)
	{
	var input = document.form.message;var mysel;var text;
	if (window.getSelection) mysel = window.getSelection();
	else if (document.selection) mysel = document.selection.createRange().text;
	else if (document.getSelection)	mysel = document.getSelection();
	b = (type == 0) ? '['+prop+']' : prop;
	e = (type == 0) ? '[/'+prop+']' : '';
	if (input.createTextRange)
		{
		var text;
		input.focus(input.caretPos);
		input.caretPos = document.selection.createRange().duplicate();
		input.caretPos.text = (input.caretPos.text.length>0) ? b + input.caretPos.text + e : b + mysel + e;
		}
	else input.value += b + mysel + e;
	}
// -->
</script>

<form action="<?=C_URL?>/comments.php" method="post" enctype="multipart/form-data" name=form OnSubmit="return formCheck(this)">
<input type="hidden" name="l" value="<?=LANGUAGE?>">
<input type="hidden" name="a" value="a">
<input type="hidden" name="id" value="<?=ID?>">
                <div align="center"><span><b>
                  <?=ADD_COMMENTS?>
                </b></span></div>
                <br>
                <br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" align=center width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>">
<Tr>
  <Td width="<?=C_WIDTH?>" bgcolor="<?=COLOR1?>" valign="top">
  <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td width="30%"><?=COMMENTS?> *</Td>
    <Td width="70%">
    <?php if(USE_SMILES || USE_BB){?>
    <Table Border=0 width="350" CellSpacing="1" CellPadding="4"><Tr align="left" bgcolor="<?=COLORH?>"><Td>
<?php if(USE_SMILES){?><?php $p=0;foreach ($smiles as $k=>$v){$p++;?><a href="javascript:ins(' <?=$k?> ',1)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/forum/smiles/<?=$v?>" border=0 hspace=1/></a><?php if(C_SMILES_NUMBER <= $p){?><br><a href="javascript:open_win('<?=C_URL?>/forum/smiles_show.php','_blank')"><?=$w[720]?></a><br><?php break;}?><?php }?><?php }?> 
<?php if(USE_BB && USE_SMILES){?><br><br><?php }?>
<?php if(USE_BOLD){?><a href="javascript:ins('B',0)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Bold.gif" border=0 /></a><?php }?>
<?php if(USE_ITALIC){?><a href="javascript:ins('I',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Italic.gif" border=0 /></a><?php }?>
<?php if(USE_UNDERLINE){?><a href="javascript:ins('U',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Underline.gif" border=0 /></a><?php }?>
<?php if(USE_STRIKE){?><a href="javascript:ins('S',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Strike.gif" border=0 /></a><?php }?>
<?php if(USE_QUOTE){?><a href="javascript:ins('QUOTE',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Quote.gif" border=0 /></a><?php }?>  
<?php if(USE_URL){?><a href="javascript:ins('URL',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/URL.gif" border=0 /></a><?php }?>  
<?php if(USE_IMG){?><a href="javascript:ins('IMG',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Image.gif" border=0 /></a><?php }?>  
<?php if(USE_VIDEO){?><a href="javascript:ins('VIDEO',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Video.gif" border=0 /></a><?php }?>  
</td></tr></table><?php }?>
<textarea class=forum_input type=text name=message rows=12></textarea></Td></Tr>
<?php if (VERIFY_ALLOW) {?>
  	<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td><?=VERIFICATION?><br><img src="<?=C_URL?>/img.php" border=0></Td>
    <Td><input class=minput type=text name=verifyimage></Td>
  </Tr>
	<?php }?>  
	  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
                          <td colspan=2 align=right>
    <input class=input1 type=submit value="OK" name="submit">
    </Td>
  </Tr>
                    </table></td>
</tr>
</table>
              </form>
<?php }?>

  </td>
</tr>

</table>
<br><br><br><br><br>

     </TD>
    </TR>
   </TABLE>
