<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>


   <TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="6">
    <TR> 
     <TD ALIGN="CENTER" VALIGN="TOP">
<br><br><br>



<script language="JavaScript">
<!--
function ins(prop,type)
{
	var input = document.form.message;
	var mysel;var text;
	if (window.getSelection) mysel = window.getSelection();
	else if (document.selection) mysel = document.selection.createRange().text;
	else if (document.getSelection)	mysel = document.getSelection();
	b = (type == 0) ? '['+prop+']' : prop;
	e = (type == 0) ? '[/'+prop+']' : '';
  if (input.createTextRange)
  {
	  var text;
    input.focus(input.caretPos);
    input.caretPos = document.selection.createRange().duplicate();
    input.caretPos.text = (input.caretPos.text.length>0) ? b + input.caretPos.text + e : b + mysel + e;
   }
   else input.value += b + mysel + e;
}
function formCheck(form) {
  if (form.message.value == "") {
    alert("<?=ENTER_MESSAGE?>");
    return false;
  }

  document.form.submit.disabled=1;

    if (captchaContainer !== null){
        return true;
    }

    var form = $('#replyForm')[0];
    var formData = new FormData(form);

    $.ajax({
        method: 'POST',
        url: "index.php",
        processData: false,
        contentType: false,
        dataType: "json",
        data: formData
    })
        .done(function(msg){
            //console.log(msg);
            if (msg.status == 200){
                location = msg.href;
            } else if (msg.status == 500){
                const nodeBadge = document.querySelector('.grecaptcha-badge');
                if (nodeBadge) {
                    document.body.removeChild(nodeBadge.parentNode);
                }

                const scriptSelector = 'script[src=\'https://www.google.com/recaptcha/api.js?render=<?=RECAPTCHA_PUBLIC_KEY?>\']';
                const script = document.querySelector(scriptSelector);
                if (script) {
                    script.remove();
                }

                captchaContainer = grecaptcha.render('captcha_container', {
                    'sitekey' : '<?=OLD_RECAPTCHA_PUBLIC_KEY?>',
                    'callback' : function(response) {
                        document.form.submit.disabled = false;
                    }
                });

                $('#recaptcha_token').remove();
                return false;
            }
        });
    return false;
}
// -->
</script>

<script>
    var captchaContainer = null;

 grecaptcha.ready(function () {
     grecaptcha.execute('<?=RECAPTCHA_PUBLIC_KEY?>', {action: 'reply'}).then(function(token) {
         $('#replyForm').prepend('<input id="recaptcha_token" type="hidden" name="token" value="' + token + '">');
         $('#replyForm').prepend('<input type="hidden" name="action" value="reply">');
         document.form.submit.disabled = false;
     });
     setInterval(function () {
         grecaptcha.execute('<?=RECAPTCHA_PUBLIC_KEY?>', { action: 'reply' }).then(function (token) {
             if ($("#recaptcha_token").length) {
                 $("#recaptcha_token").val(token);
             }
         });
     }, 60000);
 });
</script>

<form action="index.php" method="post" name=form id="replyForm" OnSubmit="return formCheck(this)" enctype="multipart/form-data">
<input class=input type=hidden name="l" value="<?=LANGUAGE?>">
<input class=input type=hidden name="id" value="<?=(SENDER=='HE'?FROMID:TOID)?>">
<input class=input type=hidden name="a" value="s">
<span class=head><?=MESSAGE_FROM?>
   <?php if(!FROMID) { /* If message from administrator */?>
    <B><?=ADMINISTRATOR?></B>
		<?php } else { /* Message from user*/ ?>
<a href="<?=GenLink('UserProfile',array(LANGUAGE,(SENDER=='HE'?FROMID:TOID)))?>"><?=NAME?></a></span>
		<?php }?>
<br><br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_BWIDTH?>" bgcolor="<?=C_TBCOLOR?>">
<Tr>
  <Td>
  <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_BWIDTH?>" class=mes>
  <Tr bgcolor="<?=COLOR1?>">
    <Td>
    <?=DATE?>
    </Td>
    <td>
    <?=SENDTIME?>
    </td>
  </Tr>
  <Tr bgcolor="<?=COLOR1?>">
    <Td>
    <?=MESSAGE?>
    </Td>
    <td>
    <?=MESSAGE_VALUE?>
    </td>
  </Tr>
  <?php  if(ATTACHMENT){?>
  <Tr bgcolor="<?=COLOR1?>">
    <Td>
    <?=$w[87]?>
    </Td>
    <td align=center>
    <img src="<?=ATTACHMENT?>">
    </td>
  </Tr>
  <?php  } ?>
  <?php  if(SENDER=='HE'){?>
  <Tr bgcolor="<?=COLOR1?>" align="left">
    <Td colspan=2>
    <input class=input1 type=button value="<?=DELETE_MESSAGE?>" OnClick="location.href='<?=C_URL?>/members/index.php?l=<?=LANGUAGE?>&a=d&del[]=<?=MESSAGE_ID?>'"> <!--input class=input1 type=button value="<?=READ_NEXT_UNREAD?>" OnClick="location.href='<?=C_URL?>/members/index.php?l=<?=LANGUAGE?>&a=mnext'"></input-->
    </Td>
  </Tr>
  <?php }?>
  <Tr bgcolor="<?=COLORH?>" align="center">
    <Td colspan=2>
    <?=REPLY?>
    </Td>
  </Tr>

      <Tr class="email_again" align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
          <Td>
              Ha látod ezt a mezőt, akkor kérlek hagyd üresen!
          </td>
          <Td align="left">
          <input class=input type="text" name="subject-honeypot" autocomplete="off">
          </td>
      </tr>
  
  <Tr bgcolor="<?=COLOR1?>">
    <Td>
    <?=MESSAGER?>
        </td>
    <td>
<?php if(USE_SMILES || USE_BB){?>
    <Table Border=0 width="<?=C_WIDTH?>" CellSpacing="2" CellPadding="6">
    <Tr align="left" bgcolor="<?=COLORH?>"><Td>
<?php if(USE_SMILES){?><?php $p=0;foreach ($smiles as $k=>$v){$p++;?><a href="javascript:ins(' <?=$k?> ',1)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/forum/smiles/<?=$v?>" border=0 /></a>
<?php if(C_SMILES_NUMBER <= $p){?><br><a href="javascript:open_win('<?=C_URL?>/forum/smiles_show.php','_blank')"><?=$w[720]?></a><br><?php break;}?><?php }?><?php }?> 
<?php if(USE_BB && USE_SMILES){?><br><br><?php }?>
<?php if(USE_BOLD){?><a href="javascript:ins('B',0)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Bold.gif" border=0 /></a><?php }?>
<?php if(USE_ITALIC){?><a href="javascript:ins('I',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Italic.gif" border=0 /></a><?php }?>
<?php if(USE_UNDERLINE){?><a href="javascript:ins('U',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Underline.gif" border=0 /></a><?php }?>
<?php if(USE_STRIKE){?><a href="javascript:ins('S',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Strike.gif" border=0 /></a><?php }?>
<?php if(USE_QUOTE){?><a href="javascript:ins('QUOTE',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Quote.gif" border=0 /></a><?php }?>  
<?php if(USE_URL){?><a href="javascript:ins('URL',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/URL.gif" border=0 /></a><?php }?>  
<?php if(USE_IMG){?><a href="javascript:ins('IMG',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Image.gif" border=0 /></a><?php }?>  
<?php if(USE_VIDEO){?><a href="javascript:ins('VIDEO',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Video.gif" border=0 /></a><?php }?>  
</td></tr></table><?php }?>

    <textarea class=forum_input name=message id=message cols=40 rows=9 style="width:<?=C_WIDTH?>px"><?=((SENDER=='HE')?MESSAGE_ANSWER:'')?></textarea>
    </td>
  </tr>
  
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td width="30%">
    <?=$w[87]?>
    </td>
    <Td width="70%" align="left">
    <input alt="" name="image" type="file">
    </td>
  </tr>

  <Tr bgcolor="<?=COLOR1?>">
    <Td colspan=2>
    <input type="checkbox" name="confirm"<?=CONFIRM?>> <?=NOTIFY_ME?>
    </td>
  </tr>
  <Tr bgcolor="<?=COLOR1?>">
    <Td colspan="2">
    <input class=input1 type=submit value="<?=SEND_MESSAGE?>" name="submit" disabled>
    </td>
  </tr>
      <Tr bgcolor="<?=COLOR1?>">
          <Td colspan="2">
              <div style="text-align: center" id="captcha_container"></div>
              <style>
                  #captcha_container div{
                      margin: auto;
                  }
              </style>
          </Td>
      </Tr>
  </Table>
  </td>
</Tr>

</Table>
</form>

<h3><?=$w['96']?></h3>
	
<?php foreach ($meslog as $mes) {?>
	<p>
	<Table CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_BWIDTH?>" class="mes"><tbody>
	<Tr bgcolor="<?=COLOR2?>">
    <Td width="100">
    <?=(($mes['fromid']==get_info('m'))?(MyName()):NAME)?>
    </td>
    <Td>
    <?=$mes['sendtime']?>
    </td>
  </Tr>
  <Tr>
    <td colspan="2" bgcolor="<?=(($mes['fromid']==get_info('m'))?COLOR1:COLOR4)?>" >
    <?=$mes['message']?>
    </td>
  </Tr>
  <?php if ($mes['attachment'] != ''){?>
  <Tr>
    <td colspan="2" bgcolor="<?=(($mes['fromid']==get_info('m'))?COLOR1:COLOR4)?>" >
    <center><img src="<?=$mes['attachment']?>"></center>
    </td>
  </Tr>
  <?php }?>
  </tbody>
  </Table>
  </p>
	<?php }?>

<br><br><br><br>

     </TD>
    </TR>
   </TABLE>
