<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
<script language="JavaScript">
function sel() {
	with(document.form) {
		for(i=0;i<elements.length;i++) {
			thiselm = elements[i];
			if(thiselm.name.substring(0,3) == 'act')
			thiselm.checked = !thiselm.checked
		}
	}
}

function CheckAct() {
	with(document.form) {
		if(myactions.value == "" || myactions.value == "0") {
			alert("<?=$w[432]?>");
		} else {
			cnt=0;
			for(i=0;i<elements.length;i++) {thiselm = elements[i];
			if(thiselm.name.substring(0,3) == 'act') if(thiselm.checked) {
				cnt=cnt+1;
				v = thiselm.value.split("|");
				if(myactions.value >= 1 && myactions.value <= 8) {
					if(myactions.value == "1") {
						window.open("<?=C_URL?>/view.php?l=<?=LANGUAGE?>&id="+v[0], "_blank");
					} else if(myactions.value == "2") {
						window.open("<?=C_URL?>/sendk.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					} else if(myactions.value == "3") {
						window.open("<?=C_URL?>/send.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					} else if(myactions.value == "5") {
						window.open("<?=C_URL?>/mail.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					} else if(myactions.value == "6") {
						window.open("<?=C_URL?>/bedroom.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					} else if(myactions.value == "7") {
						document.form.submit();
					} else {
						window.open("<?=C_URL?>/bad.php?l=<?=LANGUAGE?>&id="+v[0]+"&uname="+v[1], "_blank");
					}
				}
			}
			}
			if(cnt == 0) alert("<?=$w[555]?>");
		}
	}
}
</script>
<form action="index.php" method="post" name=form>
<input class=input type=hidden name="l" value="<?=LANGUAGE?>">
<input class=input type=hidden name="a" value="l">
<TABLE align=center WIDTH="98%" BORDER="0" CELLSPACING="0" CELLPADDING="0"><TR><TD><br><div align="center"><span class=head><?=SEARCH_RESULTS?></span></div><br><br><center>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_BWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td><Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_BWIDTH?>" class=mes>
  <Tr align="center" bgcolor="<?=COLORH?>">
    <Td><?=PHOTO?></td>
    <?php if(SHOW_PURPOSE){?><Td><?=PURPOSES?></td><?php }?>
    <?php if(SHOW_COUNTRY){?><Td><?=COUNTRY?></td><?php }?>
    <?php if(SHOW_AGE){?><Td><?=AGE?></td><?php }?>
    <Td><?=ADDITIONAL_INFO?></td>
    <td>
    <input type="checkbox" onClick="sel()">
    </td>
  </Tr>
<?php  while(isset($ids[$p])) { /* Search results - begin */?>  
  <Tr align="center" bgcolor="<?=$colors[$p]?>">
    <Td width=100>
    <?php if(SHOW_PHOTO){?><?=$pictures[$p]?><?php }?><BR><?=$freasons[$p]?></td>
    <?php if(SHOW_PURPOSE){?><Td><?=$purposes[$p]?></td><?php }?>
    <?php if(SHOW_COUNTRY){?><Td><?=$countries[$p]?></td><?php }?>
    <?php if(SHOW_AGE){?><Td><?=$ages[$p]?></td><?php }?>
    <Td align=left valign=top>
        
    <table Border=0 CellSpacing="1" CellPadding="1" width="100%" class=mes>
		<tr><td width=50%><a class=link2 href="<?=GenLink('UserProfile',array(LANGUAGE,$ids[$p]))?>"><?=$names[$p]?></a></td></tr>
    <?php if(SHOW_FNAME){?><tr><td width=50%><?=FIRSTNAME?>:</td><td width=50%><?=$fnames[$p]?></td></tr><?php }?>
    <?php if(SHOW_LNAME){?><tr><td><?=LASTNAME?>:</td><td><?=$lnames[$p]?></td></tr><?php }?>
		<?php if(SHOW_BODYTYPE){?><tr><td><?=BODYTYPE?>:</td><td><?=$bodytypes[$p]?></td></tr><?php }?>
		<?php if(SHOW_ORIENTATION){?><tr><td><?=ORIENTATION?>:</td><td><?=$orientations[$p]?></td></tr><?php }?>
		<?php if(SHOW_CITY){?><tr><td><?=CITY?>:</td><td><?=$cities[$p]?></td></tr><?php }?>
		<?php if(SHOW_HEIGHT){?><tr><td><?=HEIGHT?>:</td><td><?=$heights[$p]?></td></tr><?php }?>
		<?php if(SHOW_WEIGHT){?><tr><td><?=WEIGHT?>:</td><td><?=$weights[$p]?></td></tr><?php }?>
		<?php if(SHOW_SEEKING_GENDER){?><tr><td><?=SEEKING_GENDER?>:</td><td><?=$sgenders[$p]?></td></tr><?php }?>
		<?php if(SHOW_PHOTO){?><tr><td><?=PHOTO?>:</td><td><?=$photoinfo[$p]?></td></tr><?php }?>
		<?php if(SHOW_HORO){?><tr><td><?=HOROSCOPE?>:</td><td><?=$horoscopes[$p]?></td></tr><?php }?>
		<?php if(SHOW_REG_DATE){?><tr><td><?=REGISTER_DATE?>:</td><td><?=$regdates[$p]?></td></tr><?php }?>
		<?php if(SHOW_LAST_ACCESS){?><tr><td><?=LAST_ACCESS?>:</td><td><?=$editdates[$p]?></td></tr><?php }?>
    <?php if(SHOW_RATING){?><tr><td><?=RATING?>:</td><td><?=$rates[$p]?></td></tr><?php }?>
		<?php if(SHOW_USERTYPE){?><tr><td><?=USERTYPE?>:</td><td><?=$usertypes[$p]?></td></tr><?php }?>
		<?php if(SHOW_ONLINE_STATUS){?><tr><td><?=ONLINE_STATUS?>:</td><td>
		<?php if(IsOnline($ids[$p])){?> <span style="color:green"><B><?=ONLINE?></B></span> <?php }else{?> <?=OFFLINE?> <?php }?>
		</td></tr><?php }?>
    </table></td>
    <td><input type="checkbox" name="act[]" value="<?=$ids[$p].'|'.$names[$p]?>"></td>
  </Tr>
<?php  $p++;} /* Search results - end */?>
  </table>
  </td>
</tr>
</table>
<BR>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_BWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td><Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_BWIDTH?>" class=mes><?=PAGES?></table></td></tr></table>
<BR>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_BWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td><Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_BWIDTH?>" class=mes><tr><td bgcolor="<?=COLORH?>" align=right>
<?=WITH_SELECTED?> 
<select name=myactions class="select">
<option value="0"> --------
<option value="7"> <?=DELETE_USERS?>
<?=ShowActions(7)?>
</select> 
 <input type="button" value="<?=$w[263]?>" class=button OnClick="javascript:CheckAct()"> 
</td></tr>
</table></td></tr></table></center>
  </TD>
 </TR>
</TABLE>
</form><br><br><br>
