<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>

<form action="?" method="post" name=form>
<div align="center"><span class=head><?=OPTIONS?></span></div>
<input type="hidden" name="l" value="<?=LANGUAGE?>">
<input type="hidden" name="act" value="change">
   <TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="6">
    <TR> 
     <TD ALIGN="CENTER" VALIGN="TOP">
			<Table CellSpacing="<?=C_BORDER?>" CellPadding="4" width="<?=C_BWIDTH?>" bgcolor="<?=C_TBCOLOR?>">
		 <?php if(IsAccess(C_OPTIONS_TEMPLATE)) {?>
			<Tr bgcolor="<?=COLOR1?>">
  			<Td align=left>
				<strong><?=CHANGE_DESIGN?></strong> 
				</td>
				<td align=center>
				<select name="ChangeTemplate" class=minput> 
					<?=AVAILABLE_OPTIONS?> 
				</select> 
  			</td>
			</tr>
			<?php }?>
		 <?php if(IsAccess(C_MY_TEMP)) {?>
			<Tr bgcolor="<?=COLOR1?>">
  			<Td align=left>
				<strong><?=MY_DESIGN?></strong> 
				</td>
				<td align=center>
				<select name="MyTemplate" class=minput> 
					<?=MY_AVAILABLE_OPTIONS?> 
				</select> 
  			</td>
			</tr>
			<?php }?>
		 <?php if(IsAccess(C_CAN_NO_RATE)) {?>
			<Tr bgcolor="<?=COLOR1?>">
  			<Td align=left>
				<strong><?=PARTICIPATE_IN_RATE?></strong> 
				</td>
				<td align=center>
				<input type="checkbox" name="rate" <?=PARTICIPATE_IN_RATE_VALUE?>> 
  			</td>
			</tr>
			<?php }?>
		 <?php if(IsAccess(C_HIDE_PROFILE)) {?>
			<Tr bgcolor="<?=COLOR1?>">
  			<Td align=left>
				<strong><?=HIDE_PROFILE?></strong> 
				</td>
				<td align=center>
				<input type="checkbox" name="hide" <?=HIDE_PROFILE_VALUE?>> 
  			</td>
			</tr>
			<?php }?>
		 <?php if(IsAccess(C_OPT_NOMAIL)) {?>
			<Tr bgcolor="<?=COLOR1?>">
  			<Td align=left>
				<strong><?=NOMAIL?></strong> 
				</td>
				<td align=center>
				<input type="checkbox" name="nomail" <?=NOMAIL_VALUE?>> 
  			</td>
			</tr>
			<?php }?>
		 <?php if(IsAccess(C_NO_KISS)) {?>
			<Tr bgcolor="<?=COLOR1?>">
  			<Td align=left>
				<strong><?=NOKISS?></strong> 
				</td>
				<td align=center>
				<input type="checkbox" name="nokiss" <?=NOKISS_VALUE?>> 
  			</td>
			</tr>
			<?php }?>
		 <?php if(IsAccess(C_IMSOUND)) {?>
			<Tr bgcolor="<?=COLOR1?>">
  			<Td align=left>
				<strong><?=IMSOUND?></strong> 
				</td>
				<td align=center>
				<input type="checkbox" name="imsound" <?=IMSOUND_VALUE?>> 
  			</td>
			</tr>
			<?php }?>
			<Tr bgcolor="<?=COLOR1?>">
  			<Td align=left>
				<?=TIME_DIFFERENCE?> <BR>
				<?=CURRENT_DATE?> <BR>
				<?=MY_CURRENT_DATE?> 
				</td>
				<td align=center>
				<input type="input" class=sinput name="timediff" value=<?=CURRENT_DATE_VALUE?>> 
  			</td>
			</tr>
			</table>
			<br><input class=input1 type=submit value="<?=CHANGE_OPTIONS?>" name="submit"><br><br><br>
     </TD>
    </TR>
   </TABLE>
</form>
<?php if(IsAccess(C_PROF_LNK)){?>
<form action="?" method="post" name=link>
<div align="center"><span class=head><?=LINKING_PROFILES?></span></div>
<input type="hidden" name="l" value="<?=LANGUAGE?>">
<input type="hidden" name="act" value="change_link">
   <TABLE WIDTH="500" BORDER="0" CELLSPACING="0" CELLPADDING="6">
    <TR> 
     <TD ALIGN="CENTER" VALIGN="TOP">
     <?=LINKING_NOTE?>
			<Table CellSpacing="<?=C_BORDER?>" CellPadding="4" width="<?=C_SWIDTH?>" bgcolor="<?=C_TBCOLOR?>">
			<Tr bgcolor="<?=COLOR1?>">
  			<Td align=left width=50%>
				<?=USER_ID?>
				</td>
				<td align=left width=50%>
				<input type="input" class=sinput name="link" value="<?=USER_ID_VALUE?>">

				<input class=input1 type=button value="Check" name="submit" OnCLick="javascript:window.open('<?=C_URL?>/view.php?l=<?=LANGUAGE?>&id='+document.link.link.value,'vp')">
  			</td>
			</tr>
			<Tr bgcolor="<?=COLOR1?>">
  			<Td align=left>
				<?=LINKING_DESC?>
				</td>
				<td align=center>
				<input type="input" class=minput name="link_txt" value="<?=LINKING_DESCR?>"> 
  			</td>
			</tr>
			</table>
			<br><input class=input1 type=submit value="<?=CHANGE_OPTIONS?>" name="submit"><br><br><br>
     </TD>
    </TR>
   </TABLE>
</form>
<?php }?>