<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0">
<TR> 
  <TD align=center VALIGN="TOP"><br><br>
<script language="JavaScript">
<!--
// Select Multiple js-function: hld()
function hld(nid){ln=nid.parentNode;if(nid.checked){ln.style.backgroundColor='#0a246a';ln.style.color='#fff';}else{ln.style.backgroundColor='#fff';ln.style.color='#000';}}

function formCheck(form) {
	var cnt = form.elements.length;
	var flds = form.elements;
<?php if (STEP == '1') {?>
<?php if (USERNAME_ALLOW) {?>
if (form.username.value == "") {
    alert("<?=USERNAME_ERROR?>");
    return false;
}
<?php }?>
if (form.pass.value == "") {
    alert("<?=PASSWORD_ERROR_1?>");
    return false;
}
if (form.rpass.value == "") {
    alert("<?=PASSWORD_ERROR_2?>");
    return false;
}
if (form.pass.value != form.rpass.value) {
    alert("<?=PASSWORD_ERROR_3?>");
    return false;
}
<?php if (VERIFY_ALLOW) {?>
if (form.verifyimage.value == "") {
    alert("<?=VERIFY_ERROR?>");
    return false;
}
<?php }?>
if (form.email.value == "") {
    alert("<?=EMAIL_ERROR?>");
    return false;
}
<?php if (REG_TERMS_ALLOW) {?>
if (form.reg_terms.checked == 0) {
    alert("<?=REG_TERMS_ERROR?>");
    return false;
}
<?php }?>
<?php }?>
<?php if (STEP == '2') {?>
<?php if (FIRSTNAME_ALLOW) {?>
if (form.fname.value == "") {
    alert("<?=FIRSTNAME_ERROR?>");
    return false;
}
<?php }?>
<?php if (LASTNAME_ALLOW) {?>
if (form.lname.value == "") {
    alert("<?=LASTNAME_ERROR?>");
    return false;
}
<?php }?>
<?php if (BIRTHDAY_ALLOW) {?>
if ( (form.month.selectedIndex == "") || (form.day.selectedIndex == "") || (form.year.selectedIndex == "")) {
    alert("<?=BIRTHDAY_ERROR?>");
    return false;
}
<?php }?>
<?php if (GENDER_ALLOW) {?>
if (form.gender.selectedIndex=="") {
    alert("<?=GENDER_ERROR?>");
    return false;
}
<?php }?>

	<?php if (PURPOSE_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,7) == "purpose" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=PURPOSE_ERROR?>");return false;}
	<?php }?>
	
<?php if (ZIP_ALLOW) {?>
if (form.zip.value=="") {
    alert("<?=ZIP_ERROR?>");
    return false;
}
<?php }?>
<?php if (COUNTRY_ALLOW) {?>
if (form.country.selectedIndex=="") {
    alert("<?=COUNTRY_ERROR?>");
    return false;
}
<?php }?>
<?php if (PHONE_ALLOW) {?>
if (form.phone.value == "") {
    alert("<?=PHONE_ERROR?>");
    return false;
}
<?php }?>
<?php if (CITY_ALLOW) {?>
if (form.city.value == "") {
    alert("<?=CITY_ERROR?>");
    return false;
}
<?php }?>
<?php if (ADDRESS_ALLOW) {?>
if (form.address.value == "") {
    alert("<?=ADDRESS_ERROR?>");
    return false;
}
<?php }?>
<?php if (MARSTAT_ALLOW) {?>
if (form.marstat.selectedIndex=="") {
    alert("<?=MARSTAT_ERROR?>");
    return false;
}
<?php }?>
<?php if (CHILD_ALLOW) {?>
if (form.child.selectedIndex=="") {
    alert("<?=CHILD_ERROR?>");
    return false;
}
<?php }?>
<?php if (HEIGHT_ALLOW) {?>
if (form.height.selectedIndex=="") {
    alert("<?=HEIGHT_ERROR?>");
    return false;
}
<?php }?>
<?php if (WEIGHT_ALLOW) {?>
if (form.weight.selectedIndex=="") {
    alert("<?=WEIGHT_ERROR?>");
    return false;
}
<?php }?>
<?php if (HAIR_ALLOW) {?>
if (form.hcolor.selectedIndex=="") {
    alert("<?=HAIR_ERROR?>");
    return false;
}
<?php }?>
<?php if (EYE_ALLOW) {?>
if (form.ecolor.selectedIndex=="") {
    alert("<?=EYE_ERROR?>");
    return false;
}
<?php }?>
<?php if (ETHNICITY_ALLOW) {?>
if (form.etnicity.selectedIndex=="") {
    alert("<?=ETHNICITY_ERROR?>");
    return false;
}
<?php }?>
<?php if (RELIGION_ALLOW) {?>
if (form.religion.selectedIndex=="") {
    alert("<?=RELIGION_ERROR?>");
    return false;
}
<?php }?>
<?php if (DRINK_ALLOW) {?>
if (form.drink.selectedIndex=="") {
    alert("<?=DRINK_ERROR?>");
    return false;
}
<?php }?>
<?php if (EDUCATION_ALLOW) {?>
if (form.education.selectedIndex=="") {
    alert("<?=EDUCATION_ERROR?>");
    return false;
}
<?php }?>
<?php if (JOB_ALLOW) {?>
if (form.job.value=="") {
    alert("<?=JOB_ERROR?>");
    return false;
}
<?php }?>
	<?php if (LANG_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,4) == "lang" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=LANG_ERROR?>");return false;}
	<?php }?>
<?php if (HOBBY_ALLOW) {?>
if (form.hobby.value=="") {
    alert("<?=HOBBY_ERROR?>");
    return false;
}
<?php }?>

	<?php if (INTERESTS_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,9) == "interests" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=INTERESTS_ERROR?>");return false;}
	<?php }?>

	<?php if (DESCRIPTION_SHOW) {?>
function trim(s)
{
  return s.replace(/^\s+|\s+$/, '');
}    
if (trim(form.descr.value)=="") {
		alert("<?=DESCRIPTION_ERROR?>");
		return false;
	}
<?php }?>
<?php if (HDYFU_ALLOW) {?>
if (form.hdyfu.selectedIndex=="") {
    alert("<?=HDYFU_ERROR?>");
    return false;
}
<?php }?>
<?php }?>
<?php if (STEP == '3') {?>
	<?php if (SGENDER_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,7) == "sgender" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=SGENDER_ERROR?>");return false;}
	<?php }?>
	
	<?php if (SCOUNTRY_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,8) == "scountry" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=SCOUNTRY_ERROR?>");return false;}
	<?php }?>
	<?php if (SMARSTAT_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,8) == "smarstat" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=SMARSTAT_ERROR?>");return false;}
	<?php }?>
	<?php if (SCHILD_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,6) == "schild" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=SCHILD_ERROR?>");return false;}
	<?php }?>
	<?php if (SHAIR_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,7) == "shcolor" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=SHAIR_ERROR?>");return false;}
	<?php }?>
	<?php if (SEYE_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,7) == "secolor" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=SEYE_ERROR?>");return false;}
	<?php }?>
	<?php if (SSMOKE_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,6) == "ssmoke" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=SSMOKE_ERROR?>");return false;}
	<?php }?>
	<?php if (SDRINK_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,6) == "sdrink" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=SDRINK_ERROR?>");return false;}
	<?php }?>
	<?php if (SEDUCATION_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,10) == "seducation" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=SEDUCATION_ERROR?>");return false;}
	<?php }?>
	
	<?php if (SETHNICITY_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,9) == "setnicity" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=SETHNICITY_ERROR?>");return false;}
	<?php }?>

	<?php if (SRELIGION_ALLOW) {?>
	var err = true;
	for (i=0; i < cnt; i++)	{	if(flds[i].name.substring(0,9) == "sreligion" && flds[i].checked == 1)	{err = false;break;}}	
	if (err) {alert("<?=SRELIGION_ERROR?>");return false;}
	<?php }?>
	
<?php }?>
if (document.form.submit.action != "") {
document.form.submit.disabled=1;}
}

function CheckUncheckAll(itemname) {
	with(document.form) {
		for(i=0;i<elements.length;i++) {
			thiselm = elements[i];
			if(thiselm.name == itemname) thiselm.checked = !thiselm.checked
		}
	}
}

function ins(prop,type)
{
	var input = document.form.descr;var mysel;var text;
	if (window.getSelection) mysel = window.getSelection();
	else if (document.selection) mysel = document.selection.createRange().text;
	else if (document.getSelection)	mysel = document.getSelection();
	b = (type == 0) ? '['+prop+']' : prop;
	e = (type == 0) ? '[/'+prop+']' : '';
	if (input.createTextRange)
	{
		var text;
		input.focus(input.caretPos);
		input.caretPos = document.selection.createRange().duplicate();
		input.caretPos.text = (input.caretPos.text.length>0) ? b + input.caretPos.text + e : b + mysel + e;
	}
	else input.value += b + mysel + e;
}	
// -->
</script>

<script language="JavaScript" src="classes/Ajax.js"></script>
<script>
	function SR(force) {
		var username = '' + document.getElementById('username').value;
		var req = new Ajax();
		req.onreadystatechange = function() {
			if (req.readyState == 4) {
				if (req.resp) {
					document.getElementById('UsernameValue').innerHTML = req.resp.username;
				}
			}
		}
		req.caching = true;
		req.open('POST', 'ajax/dl.php', true);
		req.send({ check:'1',username: username});
	}
</script>
<form action="<?=filename()?>" method="post" enctype="multipart/form-data" name=form OnSubmit="return formCheck(this)">
<input type="hidden" name="l" value="<?=LANGUAGE?>">
<input type="hidden" name="a" value="a">
<input type="hidden" name="have_error" value="<?=HAVE_ERROR?>">
<input type="hidden" name="step" value="<?=STEP?>">
<input type="hidden" name="passw" value="<?=MD5PASS?>">
<input type="hidden" name="id" value="<?=UID?>">
<span class=head><?=REGISTER_USER?> - <?=WSTEP?>: <?=STEP?> / 4</span>
<br><br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>">
<Tr>
  <Td>
  <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
<?php if (STEP == '1') {?>
<!-- STEP 1: BEGIN -->
  <?php if(USERNAME_ALLOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=USERNAME?>
    </Td>
    <Td>
    <input class=input type=text id=username name=username maxlength="16" value="<?=USERNAME_VALUE?>" OnChange="SR(true)"><br><span id="UsernameValue" style="color:red"></span>
    </Td>
  </Tr>
  <?php }?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td width=40%>
    <?=EMAIL?>
    </Td>
    <Td>
    <input class=input type=text name=email value="<?=EMAIL_VALUE?>">
    </Td>
  </Tr>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=PASSWORD?>
    </Td>
    <Td>
    <input class=input type=password name=pass maxlength="<?=C_PASSB?>" value="<?=PASSWORD_VALUE?>">
    </Td>
  </Tr>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=RETYPE_PASSWORD?>
    </Td>
    <Td>
    <input class=input type=password name=rpass maxlength="<?=C_PASSB?>" value="<?=RETYPE_PASSWORD_VALUE?>">
    </Td>
  </Tr>
	<?php if (VERIFY_ALLOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=VERIFICATION?><br>
		<img src=img.php border=0>
    </Td>
    <Td>
    <input class=input type=text name=verifyimage>
    </Td>
  </Tr>
	<?php }?>
<?php if(AGREEMENT){?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td colspan=2 align=right>
    <textarea cols="60" rows="10" class=binput disabled="disabled"><?php include_once C_PATH.'/languages/'.LANGUAGE.'/agr.php';?></textarea>
    </Td>
  </Tr>
<?php }?>
  <?php if(REG_TERMS_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=REG_TERMS?>
    </Td>
    <Td>
    <input type=checkbox name=reg_terms <?=REG_TERMS_VALUE?>>
    </Td>
  </Tr>
  <?php }?>
  <?php if(WANTPRIV_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=WANTPRIV?>
    </Td>
    <Td>
    <input type=checkbox name=wantpriv <?=WANTPRIV_VALUE?>>
    </Td>
  </Tr>
  <?php }?>
<!-- STEP 1: END -->
<?php }?>
<?php if (STEP == '2') {?>
<!-- STEP 2: BEGIN -->
  <?php if(FIRSTNAME_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=FIRSTNAME?>
    </Td>
    <Td>
    <input class=input type=text name=fname maxlength="<?=C_FIRSTNB?>" value="<?=FIRSTNAME_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(LASTNAME_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=LASTNAME?>
    </Td>
    <Td>
    <input class=input type=text name=lname maxlength="<?=C_LASTNB?>" value="<?=LASTNAME_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(BIRTHDAY_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=BIRTHDAY?>
    </Td>
    <Td>
    <select name="month" class="minput">
      <option value=0> --------- 
      <?=MONTH_VALUE?>
    </select>
    <select name="day" class="sinput">
      <option value=0> ----- 
      <?=DAY_VALUE?>
    </select>
    <select name="year" class="sinput">
      <option value=0> ----- 
      <?=YEAR_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=GENDER?>
    </Td>
    <Td>
    <select name="gender" class="input">
      <option value=0> --------- 
      <?=GENDER_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php if(ORIENTATION_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=ORIENTATION?>
    </Td>
    <Td>
    <select name="orientation" class="input">
      <option value=0> --------- 
      <?=ORIENTATION_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(BODYTYPE_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=BODYTYPE?>
    </Td>
    <Td>
    <select name="bodytype" class="input">
      <option value=0> --------- 
      <?=BODYTYPE_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(PURPOSE_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=PURPOSE?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=PURPOSE_VALUE?>
    </div>
		</div>
		<?php if(C_CHECK_ALL){?><input type="checkbox" OnClick="javascript:CheckUncheckAll('purpose[]')"> <?=CHECK_UNCHECK_ALL?><?php }?>
		</Td>
  </Tr>
  <?php }?>
  <?php if(COUNTRY_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=COUNTRY?>
    </Td>
    <Td>
    <select name="country" class="input"<?php if(STATE_SHOW) {?> onChange="ShowRegion(this.options[this.selectedIndex].value)"<?php }?>>
      <?=COUNTRY_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(STATE_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=STATE?>
    </Td>
    <Td>
<div id='state'>
    <input class=input type=text name=state value="<?=STATE_VALUE?>">
</div>  
<div id='statelist' style="display:none;">
    <select name="statelist" class="input">
		</select>
</div>  
    </Td>
  </Tr>
<script language="JavaScript">
var ids=new Array('state','statelist');
function switchid(id){hideallids();	showdiv(id);}
function hideallids(){for (var i=0;i<ids.length;i++){hidediv(ids[i]);}}
function hidediv(id) {if (document.getElementById) {document.getElementById(id).style.display = 'none';}
	else {if (document.layers) {document.id.display = 'none';}
		else {document.all.id.style.display = 'none';}}}
function showdiv(id) {	  
	if (document.getElementById) {document.getElementById(id).style.display = 'block';}
	else {if (document.layers) {document.id.display = 'block';}
		else {document.all.id.style.display = 'block';}}}
var rc= new Array();
<?=STATELIST_VALUE?>
function GetIndex(index){var sc = rc[index];return sc.split(","); }
function ShowRegion(index){
if(<?=STATELIST_CHECK?>) {
switchid('statelist');
document.forms["form"].elements["state"].value = '';
var ind = GetIndex(index);var cur = '<?=STATELIST_CUR?>';var cnt = ind.length;var lst = document.forms["form"].elements["statelist"];var lstOptionsCnt = lst.options.length;lst.length = 0; 
		lst.options[0] = new Option('--------', '', false, false);
   for (i = 1; i < cnt; i++){
        lst.options[i] = (cur == ind[i]) ? new Option(ind[i], ind[i], true, false) : new Option(ind[i], ind[i], false, false);
    }
} else {
switchid('state');
for (var i = 0; i < document.forms["form"].elements["statelist"].length; i++) {document.forms["form"].elements["statelist"].selected = false;}
document.forms["form"].elements["statelist"].value = '';
}
}
ShowRegion(document.forms["form"].elements["country"].options[document.forms["form"].elements["country"].options.selectedIndex].value);
// -->
</script>    
  <?php }?>
  <?php if(ZIP_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=ZIP?>
    </Td>
    <Td>
    <input class=input type=text name=zip value="<?=ZIP_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(CITY_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=CITY?>
    </Td>
    <Td>
    <input class=input type=text name=city value="<?=CITY_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(ADDRESS_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=ADDRESS?>
    </Td>
    <Td>
    <input class=input type=text name=address value="<?=ADDRESS_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(URL_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=URL?>
    </Td>
    <Td>
    <input class=input type=text name=url value="<?=URL_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(ICQ_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=ICQ?>
    </Td>
    <Td>
    <input class=input type=text name=icq value="<?=ICQ_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(AIM_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=AIM?>
    </Td>
    <Td>
    <input class=input type=text name=aim value="<?=AIM_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(PHONE_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=PHONE?>
    </Td>
    <Td>
    <input class=input type=text name=phone value="<?=PHONE_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(MARSTAT_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=MARITAL_STATUS?>
    </Td>
    <Td>
    <select name="marstat" class="input">
      <option value=0> --------- 
      <?=MARITAL_STATUS_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(CHILD_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=CHILD?>
    </Td>
    <Td>
    <select name="child" class="input">
      <option value=0> --------- 
      <?=CHILD_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(HEIGHT_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=HEIGHT?>
    </Td>
    <Td>
    <select name="height" class="input">
      <option value=0> --------- 
      <?=HEIGHT_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(WEIGHT_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=WEIGHT?>
    </Td>
    <Td>
    <select name="weight" class="input">
      <option value=0> --------- 
      <?=WEIGHT_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(HAIR_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=HAIR?>
    </Td>
    <Td>
    <select name="hcolor" class="input">
      <option value=0> --------- 
      <?=HAIR_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(EYE_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=EYE?>
    </Td>
    <Td>
    <select name="ecolor" class="input">
      <option value=0> --------- 
      <?=EYE_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(ETHNICITY_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=ETHNICITY?>
    </Td>
    <Td>
    <select name="etnicity" class="input">
      <option value=0> --------- 
      <?=ETHNICITY_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(RELIGION_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=RELIGION?>
    </Td>
    <Td>
    <select name="religion" class="input">
      <option value=0> --------- 
      <?=RELIGION_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SMOKE_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SMOKE?>
    </Td>
    <Td>
    <select name="smoke" class="input">
      <option value=0> --------- 
      <?=SMOKE_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(DRINK_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=DRINK?>
    </Td>
    <Td>
    <select name="drink" class="input">
      <option value=0> --------- 
      <?=DRINK_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(EDUCATION_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=EDUCATION?>
    </Td>
    <Td>
    <select name="education" class="input">
      <option value=0> --------- 
      <?=EDUCATION_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(LANG_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=LANG?>
    </Td>
    <Td>
		<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0">
	<?php $k = 1;while (isset($wlng[$k])) {?>
			<TR> 
  		<TD align="<?=C_ALIGN?>" VALIGN="TOP" width=50%>
	<?=$wlng[$k]?> <?=$checklang[$k]?> <?php $k++;?> <br>
			</Td>
	
  		<TD align="<?=C_ALIGN?>" VALIGN="TOP" width=50%>
	<?php if(isset($wlng[$k])){?><?=$wlng[$k]?> <?=$checklang[$k]?> <?php $k++;}?> <br>
			</Td></Tr>
	
	<?php }?>
  		</Table>
     </Td>
  </Tr>
  <?php }?>
  <?php if(JOB_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=JOB?>
    </Td>
    <Td>
    <input class=input type=text name=job value="<?=JOB_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(HOBBY_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=HOBBY?>
    </Td>
    <Td>
    <input class=input type=text name=hobby value="<?=HOBBY_VALUE?>">
    </Td>
  </Tr>
  <?php }?>
  <?php if(INTERESTS_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=INTERESTS?>
    </Td>
    <Td align=left>    
    <div class=multi1>
    <div class=multi2>
      <?=INTERESTS_VALUE?>
    </div>
		</div>
		<?php if(C_CHECK_ALL){?><input type="checkbox" OnClick="javascript:CheckUncheckAll('interests[]')"> <?=CHECK_UNCHECK_ALL?><?php }?>
    </Td>
  </Tr>
  <?php }?>
  <?php if(DESCRIPTION_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=DESCRIPTION?>
    </Td>
    <Td>
    <?php if(USE_SMILES || USE_BB){?>
    <Table Border=0 width="350" CellSpacing="2" CellPadding="6"><Tr align="left" bgcolor="<?=COLORH?>"><Td>
<?php if(USE_SMILES){?><?php $p=0;foreach ($smiles as $k=>$v){$p++;?><a href="javascript:ins(' <?=$k?> ',1)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/forum/smiles/<?=$v?>" border=0 /></a>
<?php if(C_SMILES_NUMBER <= $p){?><br><a href="javascript:open_win('<?=C_URL?>/forum/smiles_show.php','_blank')"><?=$w[720]?></a><br><?php break;}?><?php }?><?php }?> 
<?php if(USE_BB && USE_SMILES){?><br><br><?php }?>
<?php if(USE_BOLD){?><a href="javascript:ins('B',0)"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Bold.gif" border=0 /></a><?php }?>
<?php if(USE_ITALIC){?><a href="javascript:ins('I',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Italic.gif" border=0 /></a><?php }?>
<?php if(USE_UNDERLINE){?><a href="javascript:ins('U',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Underline.gif" border=0 /></a><?php }?>
<?php if(USE_STRIKE){?><a href="javascript:ins('S',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Strike.gif" border=0 /></a><?php }?>
<?php if(USE_QUOTE){?><a href="javascript:ins('QUOTE',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Quote.gif" border=0 /></a><?php }?>  
<?php if(USE_URL){?><a href="javascript:ins('URL',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/URL.gif" border=0 /></a><?php }?>  
<?php if(USE_IMG){?><a href="javascript:ins('IMG',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Image.gif" border=0 /></a><?php }?>  
<?php if(USE_VIDEO){?><a href="javascript:ins('VIDEO',0);"><img src="<?=C_URL?>/templates/<?=C_TEMP?>/images/Video.gif" border=0 /></a><?php }?>  
</td></tr></table><?php }?>
    <textarea class=forum_input cols=20 rows=12 name=descr><?=DESCRIPTION_VALUE?></textarea>
    </Td>
  </Tr>
  <?php }?>
  <?php if(HDYFU_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=HOW_DID_YOU_FIND_US?>
    </Td>
    <Td>
    <select name="hdyfu" class="input">
      <option selected value=0> --------- 
      <?=HOW_DID_YOU_FIND_US_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
<!-- STEP 2: END -->
<?php }?>
<?php if (STEP == '3') {?>
<!-- STEP 3: BEGIN -->
  <?php if(SGENDER_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_GENDER?>
    </Td>
    <Td>     
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_GENDER_VALUE?>
    </div>
		</div>
 		<?php if(C_CHECK_ALL){?><input type="checkbox" OnClick="javascript:CheckUncheckAll('sgender[]')"> <?=CHECK_UNCHECK_ALL?><?php }?>
   </Td>
  </Tr>
  <?php }?>
  <?php if(SCOUNTRY_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_COUNTRY?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multi2>
      <?=SEEKING_COUNTRY_VALUE?>
    </div>
		</div>
		<?php if(C_CHECK_ALL){?><input type="checkbox" OnClick="javascript:CheckUncheckAll('scountry[]')"> <?=CHECK_UNCHECK_ALL?><?php }?>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SMARSTAT_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_MARITAL_STATUS?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_MARITAL_STATUS_VALUE?>
    </div>
		</div>
		<?php if(C_CHECK_ALL){?><input type="checkbox" OnClick="javascript:CheckUncheckAll('smarstat[]')"> <?=CHECK_UNCHECK_ALL?><?php }?>
		</Td>
  </Tr>
  <?php }?>
  <?php if(SCHILD_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_CHILD?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_CHILD_VALUE?>
    </div>
		</div>
		<?php if(C_CHECK_ALL){?><input type="checkbox" OnClick="javascript:CheckUncheckAll('schild[]')"> <?=CHECK_UNCHECK_ALL?><?php }?>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SHAIR_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_HAIR?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_HAIR_VALUE?>
    </div>
		</div>
		<?php if(C_CHECK_ALL){?><input type="checkbox" OnClick="javascript:CheckUncheckAll('shcolor[]')"> <?=CHECK_UNCHECK_ALL?><?php }?>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SEYE_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_EYE?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_EYE_VALUE?>
    </div>
		</div>
		<?php if(C_CHECK_ALL){?><input type="checkbox" OnClick="javascript:CheckUncheckAll('secolor[]')"> <?=CHECK_UNCHECK_ALL?><?php }?>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SSMOKE_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_SMOKE?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_SMOKE_VALUE?>
    </div>
		</div>
		<?php if(C_CHECK_ALL){?><input type="checkbox" OnClick="javascript:CheckUncheckAll('ssmoke[]')"> <?=CHECK_UNCHECK_ALL?><?php }?>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SDRINK_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_DRINK?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_DRINK_VALUE?>
    </div>
		</div>
		<?php if(C_CHECK_ALL){?><input type="checkbox" OnClick="javascript:CheckUncheckAll('sdrink[]')"> <?=CHECK_UNCHECK_ALL?><?php }?>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SEDUCATION_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_EDUCATION?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_EDUCATION_VALUE?>
    </div>
		</div>
		<?php if(C_CHECK_ALL){?><input type="checkbox" OnClick="javascript:CheckUncheckAll('seducation[]')"> <?=CHECK_UNCHECK_ALL?><?php }?>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SETHNICITY_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_ETHNICITY?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_ETHNICITY_VALUE?>
    </div>
		</div>
		<?php if(C_CHECK_ALL){?><input type="checkbox" OnClick="javascript:CheckUncheckAll('setnicity[]')"> <?=CHECK_UNCHECK_ALL?><?php }?>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SRELIGION_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_RELIGION?>
    </Td>
    <Td>
    <div class=multi1>
    <div class=multis>
      <?=SEEKING_RELIGION_VALUE?>
    </div>
		</div>
		<?php if(C_CHECK_ALL){?><input type="checkbox" OnClick="javascript:CheckUncheckAll('sreligion[]')"> <?=CHECK_UNCHECK_ALL?><?php }?>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SAGE_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_AGE?>
    </Td>
    <Td>
    <select name="agef" class="minput"> 
      <?=SEEKING_AGE_FROM_VALUE?>
    </select>
    <select name="aget" class="minput">
      <?=SEEKING_AGE_TO_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SHEIGHT_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_HEIGHT?>
    </Td>
    <Td>
    <select name="heightf" class="minput"> 
      <?=SEEKING_HEIGHT_FROM_VALUE?>
    </select>
    <select name="heightt" class="minput">
      <?=SEEKING_HEIGHT_TO_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
  <?php if(SWEIGHT_SHOW) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_WEIGHT?>
    </Td>
    <Td>
    <select name="weightf" class="minput">
      <?=SEEKING_WEIGHT_FROM_VALUE?>
    </select>
    <select name="weightt" class="minput">
      <?=SEEKING_WEIGHT_TO_VALUE?>
    </select>
    </Td>
  </Tr>
  <?php }?>
<!-- STEP 3: END -->
<?php }?>
<?php if (STEP == '4') {?>
<!-- STEP 4: BEGIN -->
  <?php if(PHOTO_SHOW) {?>
	<?php while(isset($PHOTO_NUM[$p])) {?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=$PHOTO_NUM[$p]?>
    </Td>
    <Td>
    <input class=input type=file name="file<?=$p?>">
    </Td>
  </Tr>
	<?php $p++;}$p=0;?>
  <?php }?>
  <?php if(AUDIO_SHOW) {?>
<?php if(C_AUDIO_ALLOW) { ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=AUDIO?>
    </Td>
    <Td>
    <input class=input type=file name="filea">
    </Td>
  </Tr>
<?php }?>
  <?php }?>
<!-- STEP 4: END -->
<?php }?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <td colspan=2 align=right>
    <input class=input1 type=submit value="<?=REGISTER?>" name="submit">
    </Td>
  </Tr>
  </table>
  </Td>
</Tr></form>
</Table>
<br>
</TD>
</TR>
</TABLE>

