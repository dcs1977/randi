/*function open_win(win_file, win_title) {
    window.open(win_file, win_title, 'width=' + window.CONST('C_WIN_WIDTH') + ',height=' + window.CONST('C_WIN_HEIGHT') + ',toolbar=no,scrollbars=yes,location=no,menubar=no,status=no,resizable=yes');
}*/

function open_win(win_file, win_title){
    MT.modalWindow.show({
        target: win_title,
        iframeUrl: win_file,
        animated: true
    });
}

function slide_win(win_file, win_title) {
    window.open(win_file, win_title, 'width=' + window.CONST('C_WIN_WIDTH') + ',height=' + window.CONST('C_WIN_HEIGHT') + ',toolbar=no,scrollbars=yes,location=no,menubar=no,status=no,resizable=yes');
}

function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function delCookie(name) { 
    setCookie(name, '', -1); 
}

// matches polyfill
this.Element && function(ElementPrototype) {
    ElementPrototype.matches = ElementPrototype.matches ||
    ElementPrototype.matchesSelector ||
    ElementPrototype.webkitMatchesSelector ||
    ElementPrototype.msMatchesSelector ||
    function(selector) {
        var node = this, nodes = (node.parentNode || node.document).querySelectorAll(selector), i = -1;
        while (nodes[++i] && nodes[i] != node);
        return !!nodes[i];
    }
}(Element.prototype);

// helper for enabling IE 8 event bindings
function addEvent(el, type, handler) {
    if (el.attachEvent) el.attachEvent('on'+type, handler); else el.addEventListener(type, handler);
}

// live binding helper using matchesSelector
function live(selector, event, callback, context) {
    addEvent(context || document, event, function(e) {
        var found, el = e.target || e.srcElement;
        while (el && el.matches && el !== context && !(found = el.matches(selector))) el = el.parentElement;
        if (found) callback.call(el, e);
    });
}

var os = {};
var ec = null;

var sendResult = function(){
    var req = new Ajax();
    if(os.player_id === null){
    os.player_id = '';
    }
    if(typeof os.fingerprint === 'undefined' || os.fingerprint === null){
    os.fingerprint = getCookie('fingerprint');
    if (os.fingerprint === null){
      var date_now = new Date();
      os.fingerprint = date_now.toISOString();
      setCookie('fingerprint', os.fingerprint, 90);
    }
    }
    var input = document.getElementById('fingerprint');
    if (typeof input !== 'undefined' && input != null)
    {
    input.value = os.fingerprint;
    }
    req.open(null, window.C_URL + '/ajax/os.php', true);
    req.send({ post: os});
}

/*Evercookie */
var approveCookie = function(){

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      
    ga('create', 'UA-51360727-3', 'hotbull.hu');
    ga('send', 'pageview');

    $('#banner_top a img').ready(function() {
        var banner = $('#banner_top a img').attr('alt');
        ga('send', 'event', 'banner_top', 'view', banner);
        //console.log(banner);
        
        $('#banner_top a').click( function() {
            var banner = $('#banner_top a img').attr('alt');
            ga('send', 'event', 'banner_top', 'click', banner);
            //console.log(banner);
        });
    });
    
    
    $('#right_b1 a img').ready( function() {
        var banner = $('#right_b1 a img').attr('alt');
        ga('send', 'event', 'right_b1', 'view', banner);
        //console.log(banner);
        
        $('#right_b1 a').click( function() {
            var banner = $('#right_b1 a img').attr('alt');
            ga('send', 'event', 'right_b1', 'click', banner);
            //console.log(banner);
        });
    });
    
    $('a').click( function() {
        var link = $(this).attr('href');
        if (link.substr(0,4)=='http')
        {
                ga('send', 'event', 'link', 'click', link);
        } 
    //console.log(link);
    });

    if (typeof evercookie !== 'undefined'){
        var ec = new evercookie({
        history: false, // CSS history knocking or not .. can be network intensive
        java: false, // Java applet on/off... may prompt users for permission to run.
        tests: 10,  // 1000 what is it, actually?
        silverlight: false, // you might want to turn it off https://github.com/samyk/evercookie/issues/45,
        lso: true, // local storage
        domain: window.C_URL, // Get current domain
        baseurl: window.C_URL, // base url for php, flash and silverlight assets
        asseturi: '/templates/default/assets', // assets = .fla, .jar, etc
        phpuri: '/templates/default/php', // php file path or route
        authPath: false, //'/evercookie_auth.php', // set to false to disable Basic Authentication cache
        swfFileName: '/evercookie.swf',
        xapFileName: '/evercookie.xap',
        pngCookieName: 'evercookie_png',
        pngPath: '/evercookie_png.php',
        etagCookieName: 'evercookie_etag',
        etagPath: '/evercookie_etag.php',
        cacheCookieName: 'evercookie_cache',
        cachePath: '/evercookie_cache.php',
        hsts: false,
        hsts_domains: [],
        db: true, // Database
        idb: true // Indexed DB
        });
        
        ec.get("fingerprint", function(value) {
            //console.log(value);
            var regex = /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/;
            var m;

            if (typeof value === 'undefined' || (m = regex.exec(value)) === null) {
                var date_now = new Date();
                os.fingerprint = date_now.toISOString();
                ec.set("fingerprint", os.fingerprint);
            } else {
                os.fingerprint = value;
            }
            setCookie('fingerprint', os.fingerprint, 90);
            sendResult();
        });
    } else {
        os.fingerprint = getCookie('fingerprint');
        if (os.fingerprint === null){
            var date_now = new Date();
            os.fingerprint = date_now.toISOString();
            setCookie('fingerprint', os.fingerprint, 90);
        }
        sendResult();
    }

    var OneSignal = window.OneSignal || [];
    OneSignal.push(function() {

    if (window.MyID > 0 && typeof window.CONST('PROMPT_MESSAGES') !== 'undefined'){
        OneSignal.init({
            appId: window.CONST('C_ONESIGNAL_APP_ID'),
            autoRegister: false,
            notifyButton: {
            enable: false,
            },
            welcomeNotification: {
            "title": "Hotbull Randi",
            "message": "Köszönjük, hogy feliratkoztál. Célunk, hogy jobb legyen a randizás!",
            },
            promptOptions: {
                customlink: {
                    enabled: true, /* Required to use the Custom Link */
                    style: "button", /* Has value of 'button' or 'link' */
                    size: "medium", /* One of 'small', 'medium', or 'large' */
                    color: {
                        button: '#FFFFFF', /* Color of the button background if style = "button" */
                        text: '#212529', /* Color of the prompt's text */
                    },
                    text: {
                        subscribe: "Értesítések bekapcsolása", /* Prompt's text when not subscribed */
                        unsubscribe: "Értesítések kikapcsolása", /* Prompt's text when subscribed */
                    },
                    unsubscribeEnabled: true, /* Controls whether the prompt is visible after subscription */
                }
            }
        });

    } else {
        OneSignal.init({
            appId: window.CONST('C_ONESIGNAL_APP_ID'),
            autoRegister: false,
            notifyButton: {
            enable: false,
            },
        });

    }


    os.is_enabled = false;
    OneSignal.getUserId(function(userId) {
        os.player_id = userId;
        OneSignal.isPushNotificationsEnabled(function(isEnabled) {
            os.is_enabled = isEnabled;
            os.is_supported = OneSignal.isPushNotificationsSupported();
            os.is_logged_in = ((window.MyID)?1:0);
            sendResult();
        });
    });

    OneSignal.on('subscriptionChange', function (isSubscribed) {
        os.is_supported = isSubscribed;
        OneSignal.isPushNotificationsEnabled(function(isEnabled) {
            os.is_enabled = isEnabled;
            os.is_logged_in = ((window.MyID)?1:0);
            sendResult();
        });
    });

    OneSignal.on('notificationPermissionChange', function(permissionChange) {
        var currentPermission = permissionChange.to;
        os.is_enabled = (currentPermission == 'granted');
        os.is_supported = OneSignal.isPushNotificationsSupported();
        os.is_logged_in = ((window.MyID)?1:0);
        sendResult();
    });
    });
};

$(document).ready(function() {
    //delCookie('approve_cookie');
    var approve_cookie = getCookie('approve_cookie');
    var date_now = new Date();
    //date_now.setMonth(date_now.getMonth() + 6);
    var timestamp = date_now.toISOString();
    approve_cookie = timestamp;

    if(approve_cookie !== null && approve_cookie >= timestamp){
        approveCookie();
    } else {
        document.getElementById('cookie_attention').removeAttribute('class');
        var button = document.getElementById('cookie_button');
        live('#cookie_button', 'click', function(){
            var el = document.getElementById('cookie_attention');
            el.parentNode.removeChild(el);
            var date_now = new Date();
            date_now.setMonth(date_now.getMonth() + 6);
            var timestamp = date_now.toISOString();
            setCookie('approve_cookie', timestamp, 180);
            approveCookie();
        })
    }            
});