<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>
<?php 
#OK#########################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.3.1                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# Template Name           Love                             #
# Author                  AzDG <support@azdg.com>          #
############################################################
# File name               banner.php                       #
# File purpose            Banner rotator                   #
# File created by         AzDG <support@azdg.com>          #
############################################################

$banner=array();
/*
Don`t use ' character in banner code or slash it, for ex: \'
*/

$banner[0]='<Tr align=center><Td colspan=3><h3><a href="http://www.azdg.com">Banner 1</a></h3></td></Tr>';
$banner[1]='<Tr align=center><Td colspan=3><h3><a href="http://www.azdg.com">Banner 2</a></h3></td></Tr>';
$banner[2]='<Tr align=center><Td colspan=3><h3><a href="http://www.azdg.com">Banner 3</a></h3></td></Tr>';


###### Don`t change anything below
$fe=rand(0,sizeof($banner)-1);
echo $banner[$fe];
?>

