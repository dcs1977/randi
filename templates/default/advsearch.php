<?php if(!defined("C_URL")) die('Can`t access file directly!'); ?>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0"><TR><TD align=center VALIGN="TOP"><br><br>
<form action="<?=C_URL?>/advsearch.php" method="post" name=form>
<input type="hidden" name="l" value="<?=LANGUAGE?>">
<input type="hidden" name="a" value="s">
<span class=head><?=ADVANCED_SEARCH?></span>
<br><br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_WIDTH?>" bgcolor="<?=C_TBCOLOR?>">
<Tr>
  <Td>
  <Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_WIDTH?>" class=mes>
<?php if(I_AM_A_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=I_AM_A?>
    </td>
    <td>
    <select name="sgender" class="input">
      <?=I_AM_A_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(SEEKING_FOR_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEEKING_FOR?>
    </td>
    <td>
    <select name="gender[]" class="inputm" multiple>
      <?=SEEKING_FOR_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(AGE_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=AGE?>
    </td>
    <td>
    <input name="agef" class="sinput"> -  <input name="aget" class="sinput">
    </td>
  </tr>
<?php }?>	
<?php if(COUNTRY_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=COUNTRY?>
    </td>
    <td>
    <select name="country[]" class="inputm" multiple>
      <?=COUNTRY_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(TYPE_OF_RELATION_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=TYPE_OF_RELATION?>
    </td>
    <td>
    <select name="purpose[]" class="inputm" multiple>
      <?=TYPE_OF_RELATION_VALUE?>
    </select>
    <BR>
		<span class=mes><input type=radio name=purpose_oper value=0 checked><?=WOR?> <input type=radio name=purpose_oper value=1><?=WAND?></span>
    </td>
  </tr>
<?php }?>	
<?php if(ORIENTATION_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=ORIENTATION?>
    </td>
    <td>
    <select name="orientation[]" class="inputm" multiple>
      <?=ORIENTATION_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(BODYTYPE_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=BODYTYPE?>
    </td>
    <td>
    <select name="bodytype[]" class="inputm" multiple>
      <?=BODYTYPE_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(INTERESTS_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=INTERESTS?>
    </td>
    <td>
    <select name="interests[]" class="inputm" multiple>
      <?=INTERESTS_VALUE?>
    </select>
    <BR>
		<span class=mes><input type=radio name=interests_oper value=0 checked><?=WOR?> <input type=radio name=interests_oper value=1><?=WAND?></span>
    </td>
  </tr>
<?php }?>	
<?php if(HOROSCOPE_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=HOROSCOPE?>
    </td>
    <td>
    <select name="horo[]" class="inputm" multiple>
      <?=HOROSCOPE_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(USERID_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=USERID?>
    </td>
    <td>
    <input type="text" name="id" class="minput">
    </td>
  </tr>
<?php }?>	
<?php if(USERNAME_ALLOW && USERNAME_SHOW) {?>	
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=USERNAME?>
    </td>
    <td>
    <input type="text" name="usr" class="minput">
    </td>
  </tr>
<?php }?>	
<?php if(FIRSTNAME_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=FIRSTNAME?>
    </td>
    <td>
    <input class=input type=text name=fname maxlength="<?=C_FIRSTNB?>">
    </td>
  </tr>
<?php }?>	
<?php if(LASTNAME_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=LASTNAME?>
    </td>
    <td>
    <input class=input type=text name=lname maxlength="<?=C_LASTNB?>">
    </td>
  </tr>
<?php }?>	
<?php if(KEYWORDS_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=KEYWORDS?>
    </td>
    <td>
    <input class=input type=text name=keywords>
    </td>
  </tr>
<?php }?>	
<?php if(STATE_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=STATE?>
    </td>
    <td>
    <input class=minput type=text name=state>
    </td>
  </tr>	
<?php }?>	
<?php if(CITY_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=CITY?>
    </td>
    <td>
    <input class=minput type=text name=city maxlength="32">
    </td>
  </tr>	
<?php }?>	
<?php if(ZIP_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=ZIP?>
    </td>
    <td>
    <input class=minput type=text name=zip maxlength="7">
    </td>
  </tr>
<?php }?>	
<?php if(DISTANCE_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=DISTANCE?>
    </td>
    <td><input class=sinput type=text name=distance maxlength="16"> <?=$w[722]?></td>
  </tr>	
<?php }?>	
<?php if(HEIGHT_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=HEIGHT?>
    </td>
    <td>
    <select name="heightf" class="minput"> 
      <?=HEIGHT_FROM?>
    </select>-<select name="heightt" class="minput">
      <?=HEIGHT_TO?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(WEIGHT_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=WEIGHT?>
    </td>
    <td>
    <select name="weightf" class="minput"> 
      <?=WEIGHT_FROM?>
    </select>-<select name="weightt" class="minput">
      <?=WEIGHT_TO?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(MARITAL_STATUS_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=MARITAL_STATUS?>
    </td>
    <td>
    <select name="marstat[]" class="inputm" multiple>
      <?=MARITAL_STATUS_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(CHILD_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=CHILD?>
    </td>
    <td>
    <select name="child[]" class="inputm" multiple> 
      <?=CHILD_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(HAIR_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=HAIR?>
    </td>
    <td>
    <select name="hcolor[]" class="inputm" multiple>
      <?=HAIR_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(EYE_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=EYE?>
    </td>
    <td>
    <select name="ecolor[]" class="inputm" multiple>
      <?=EYE_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(ETHNICITY_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=ETHNICITY?>
    </td>
    <td>
    <select name="etnicity[]" class="inputm" multiple>
      <?=ETHNICITY_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(RELIGION_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=RELIGION?>
    </td>
    <td>
    <select name="religion[]" class="inputm" multiple>
      <?=RELIGION_VALUE?>
    </select>
        <BR>
        <span class=mes><input type=radio name=religion_oper value=0 checked><?=WOR?> <input type=radio name=religion_oper value=1><?=WAND?></span>
    </td>
  </tr>
<?php }?>	
<?php if(SMOKE_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SMOKE?>
    </td>
    <td>
    <select name="smoke[]" class="inputm" multiple> 
      <?=SMOKE_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(DRINK_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=DRINK?>
    </td>
    <td>
    <select name="drink[]" class="inputm" multiple>
      <?=DRINK_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(EDUCATION_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=EDUCATION?>
    </td>
    <td>
    <select name="education[]" class="inputm" multiple>
      <?=EDUCATION_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(LANG_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=LANG?>
    </td>
    <td>
			<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0">
	<?php $k = 1;while (isset($wlng[$k])) {?>
			<TR> 
  		<TD align="<?=C_ALIGN?>" VALIGN="TOP" width=50%>
	<?=$wlng[$k]?> <?=$checklang[$k]?> <?php $k++;?> <br>
			</Td>
	
  		<TD align="<?=C_ALIGN?>" VALIGN="TOP" width=50%>
	<?php if(isset($wlng[$k])){?><?=$wlng[$k]?> <?=$checklang[$k]?> <?php $k++;}?> <br>
			</Td></Tr>
	
	<?php }?>
  		</Table>
    <BR>
		<span class=mes><input type=radio name=lang_oper value=0 checked><?=WOR?> <input type=radio name=lang_oper value=1><?=WAND?></span>
    </td>
  </tr>
<?php }?>	
<?php if(PHOTO_SHOW || URL_SHOW || ICQ_SHOW || AIM_SHOW || PHONE_SHOW || ONLINE_USERS_ONLY_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SEARCH_USERS_WITH?>
    </td>
    <td>
<?php if(PHOTO_SHOW) { /* If is allow to show this section */ ?>
    <?=PHOTO?> <input type="checkbox" name="photo"><br>
<?php }?>	
<?php if(URL_SHOW) { /* If is allow to show this section */ ?>
    <?=URL?> <input type="checkbox" name="url"><br>
<?php }?>	
<?php if(ICQ_SHOW) { /* If is allow to show this section */ ?>
    <?=ICQ?> <input type="checkbox" name="icq"><br>
<?php }?>	
<?php if(AIM_SHOW) { /* If is allow to show this section */ ?>
    <?=AIM?> <input type="checkbox" name="aim"><br>
<?php }?>	
<?php if(PHONE_SHOW) { /* If is allow to show this section */ ?>
    <?=PHONE?> <input type="checkbox" name="phone"><BR>
<?php }?>	
<?php if(ONLINE_USERS_ONLY_SHOW) { /* If is allow to show this section */ ?>
    <?=ONLINE_USERS_ONLY?> <input type="checkbox" name="online"> 
<?php }?>	
    </td>
  </tr>
<?php }?>	
<?php if(REGISTERED_IN_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=REGISTERED_IN?> 
    </td>
    <td>
    <select name="regin" class="input">
      <?=REGISTERED_IN_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(USERTYPE_SHOW) {?>	
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=USERTYPE?> 
    </td>
    <td>
    <select name="usertype" class="input">
      <?=USERTYPE_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(SORT_BY_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=SORT_BY?> 
    </td>
    <td>
    <select name="sortby" class="input">
      <?=SORT_BY_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
<?php if(RESULTS_ON_PAGE_SHOW) { /* If is allow to show this section */ ?>
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td>
    <?=RESULTS_ON_PAGE?> 
    </td>
    <td>
    <select name="step" class="sinput">
      <?=RESULTS_ON_PAGE_VALUE?>
    </select>
    </td>
  </tr>
<?php }?>	
  <Tr align="<?=C_ALIGN?>" bgcolor="<?=COLOR1?>">
    <Td colspan=2>
    <table width=100%>
    <tr>
      <td align=left>
      <!--a href="search.php?l=<?=LANGUAGE?>" class=link2>[<?=SIMPLE_SEARCH?>]</a-->
      </td>
      <td align=right>
      <input type="submit" value="<?=SEARCH?>" class=button>
      </td>
    </tr>
    </table>
    </td>
  </tr>
  </table>
  </td>
</tr></table></form></TD></TR></TABLE>