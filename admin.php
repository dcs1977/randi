<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               admin.php                        #
# File purpose            Form for enter in Admin Area     #
# File created by         AzDG <support@azdg.com>          #
############################################################

include_once 'include/config.inc.php';
include_once 'include/options.inc.php';$l=C_ADMINLANG;
include_once 'languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'.php';
include_once 'languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'_.php';
include_once 'languages/'.C_ADMINLANG.'/'.C_ADMINLANG.'a.php';
include_once 'include/functions.inc.php';

if(!isset($p)) $p='';
if ($p == "login") {
    $x = false;
    foreach ($c_adminl as $key => $login_name){
        $login_password = $c_adminp[$key];
        if(($login == $login_name)&&(hash('sha256', $password . '997E5w8dghepVJpa5K7DgBhsbzMxyFaEbEV7fXG5J9TGCrfTB7hZ2EUiisex') == $login_password)) {
            $x = $key;
        }
    } 
	if($x === false) {
                include_once 'templates/'.C_TEMP.'/config.php';
		include_once C_PATH.'/sheader.php';
		sprintm($x[1]);
	}
	else {
		unset($adminlogin);
		unset($adminpass);
		unset($adminip);
		if(C_AUTH) { // If working on Sessions
			if(C_LOGIN_SECURE)
			{
				@session_destroy();
			}
            session_set_cookie_params(0, '/', C_DOMAIN, true, true);
			@session_start();
			$_SESSION['adminlogin']=md5($c_adminl[$x]);
			$_SESSION['adminpass']=$c_adminp[$x];
			$_SESSION['adminip']=md5(ip());
                        $_SESSION['adminID']=$x;
		} else {
			// If working on Cookies
			$cr64 = new AzDGCrypt(C_CRYPT_KEY);

			set_login('adminlogin',$cr64->crypt(md5($c_adminl[$x])));
			set_login('adminpass',$cr64->crypt($c_adminp[$x]));
			set_login('adminip',$cr64->crypt(md5(ip())));
                        set_login('adminID',$x);
		}
	}

	header("Refresh:0;URL=".C_URL."/".C_ADMINDIR."/index.php?a=v&frame=index&".s());
}
else
{
	include_once 'templates/'.C_TEMP.'/config.php';
	include_once C_PATH.'/sheader.php';
?>
<center>
<form action="<?=filename()?>" method="post">
<input type="hidden" name="l" value="<?=$l?>">
<input type="hidden" name="p" value="login">
<center><span class=head><?=$x[2]?></span></center>
<br>
<Table CellSpacing="<?=C_BORDER?>" CellPadding="0" width="<?=C_SWIDTH?>" bgcolor="<?=C_TBCOLOR?>"><Tr><Td>
<Table Border=0 CellSpacing="<?=C_IBORDER?>" CellPadding="<?=C_CELLP?>" width="<?=C_SWIDTH?>" class=mes>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLORH?>"><Td>
<?=$x[3]?></td><td><input class=minput type=text name=login></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLORH?>"><Td><?=$x[4]?></td><td><input class=minput type=password name=password></td></tr>
<Tr align="<?=C_ALIGN?>" bgcolor="<?=COLORH?>"><Td colspan=2 align=right><input class=minput type=submit value="<?=$x[5]?>"></td></tr>
</table></td></tr></table></form>
<?php 
include_once C_PATH.'/sfooter.php';
}
?>