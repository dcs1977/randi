<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               stat.php                         #
# File purpose            Show statistic                   #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
//include_once 'classes/cachequery.class.php';
include_once 'templates/'.C_TEMP.'/config.php';
include_once C_PATH.'/header.php';
IsSecure(C_STATISTICS);
// Data
$stat_need=array(C_STAT_COUNTRY,C_STAT_GENDER,C_STAT_MARITAL,C_STAT_CHILDREN,C_STAT_HEIGHT,C_STAT_WEIGHT,C_STAT_HAIR,C_STAT_EYES,C_STAT_ETNICITY,C_STAT_RELIGION,C_STAT_SMOKE,C_STAT_DRINK,C_STAT_EDUCATION,C_STAT_HDYFU,C_STAT_HORO);
$w_num=array('224','57','66','67','68','69','70','71','72','73','74','75','76','86','129');
$mysql_name=array('t1.country','t1.gender','t2.marstat','t2.child','t1.height','t1.weight','t2.hcolor','t2.ecolor','t2.etnicity','t2.religion','t2.smoke','t2.drink','t2.education','t2.hdyfu','t1.horo');
$w_name=array('wcr','wg','wm','wc','wh','ww','whc','we','wet','wr','ws','wd','wed','wu','whr');
$sr=explode(',',C_SR_COLUMNS);
/////////////////////

if(!isset($a)) $a='';
if ($a == "s") {
	IsAccessToSeeThisPage();



	if(!isset($src)) $src='0';
	if(!isset($val)) $val='0';
	if($stat_need[$src] != '2') printm($w[1].'7',1);
	if(C_HACK1) {
		if(!is_numeric($src)||!is_numeric($step)||!is_numeric($from)||!is_numeric($val)) printm($w[1].'1',1);}

		if(C_HACK2) {
                    $k = $w_name[$src];
			if(($src < 0)||($src >= sizeof($mysql_name))||($val < 0)||($val >= sizeof($$k))) printm($w[1].'2',1);}

			///////// Checking and creating for search
			$msortby = " order by t1.req DESC, t1.editdate DESC";

			// Important /////////////
			$mid = " t1.status > '5' AND t1.hide = '0' AND t1.id = t2.id";

			$msrc=" AND ".$mysql_name[$src]." = '".$val."'";
                        
                        if (isset($_SESSION['search_date']) && $_SESSION['search_date'] < date("Y-m-d H:i:s")){
                            unset($_SESSION['search_type']);    
                            unset($_SESSION['search_date']);
                            unset($_SESSION['search_data']);
                        }

                        if (!isset($_SESSION['search_data']) || (isset($_SESSION['search_type']) && $_SESSION['search_type'] != 'stat')){
                            $_SESSION['search_type'] = 'stat';
                            $_SESSION['search_data'] = array();
                            $tmp0=db_query("SELECT t1.id FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2 WHERE ".$mid.$msrc.$msortby) or die(db_error());
                            while($i=db_fetch_array($tmp0)) {
                                $_SESSION['search_data'][] = $i['id'];
                            }
                            $_SESSION['search_date'] = date("Y-m-d H:i:s", ( time() + 300 ) );
                        }
                        $search = $_SESSION['search_data'];

                        //$tmp0=db_query("SELECT count(*) as total FROM ".C_MYSQL_ONLINE_USERS." as t1, ".C_MYSQL_MEMBERS_MAIN." as t2 WHERE t1.user = t2.id AND t2.status > '5' AND t2.hide='0'".$sql_dump) or die(db_error());
                        //$cnt=db_fetch_assoc($tmp0);
                        //$count=$cnt['total'];#die($count);
                        $count = count($search);
                        $in = array_slice($search, $from, $step);
                        if (count($in)){
                            $where_in = ' AND t1.id IN ('.implode(', ', $in).') ';
                        } else {
                            $where_in = ' AND t1.id IS NULL ';
                        }
                        //$tmp=db_query("SELECT DISTINCT t1.user, t1.upage, t2.*, t3.* FROM ".C_MYSQL_ONLINE_USERS." as t1, ".C_MYSQL_MEMBERS_MAIN." as t2, ".C_MYSQL_MEMBERS_EXT." as t3 WHERE t1.user = t2.id AND t2.id = t3.id AND t2.status > '5' AND t2.hide='0'".$sql_dump.$where_in) or die(db_error());

			//$tmp = New CacheQuery("SELECT count(*) as total FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2 WHERE ".$mid.$msrc,IsCache(1,16384));
			//$count = $tmp->data[0]['total'];
			if($count == "0") printm($w[110]);

			### What to show
			$qage = (IsAccess($sr[4])) ? ',t1.birthday' : '';
			$qgender = (IsAccess($sr[5])) ? ',t1.gender' : '';
			$qbodytype = (IsAccess($sr[6])) ? ',t1.bodytype' : '';
			$qorientation = (IsAccess($sr[7])) ? ',t1.orientation' : '';
			$qpurpose = (IsAccess($sr[8])) ? ',t1.purposes' : '';
			$qcountry = (IsAccess($sr[9])) ? ',t1.country' : '';
			$qcity = (IsAccess($sr[10])) ? ',t1.city' : '';
			$qheight = (IsAccess($sr[11])) ? ',t1.height' : '';
			$qweight = (IsAccess($sr[12])) ? ',t1.weight' : '';
			$qsgender = (IsAccess($sr[13])) ? ',t1.sgender' : '';
			$qhoro = (IsAccess($sr[15])) ? ',t1.horo' : '';
			$qregdate = (IsAccess($sr[16])) ? ',t1.regdate' : '';
			$qeditdate = (IsAccess($sr[17])) ? ',t1.editdate' : '';
			$qusertype = (IsAccess($sr[19])) ? ',t1.req' : '';
			$qzip = (IsAccess($sr[21])) ? ',t1.zip' : '';
			if(IsAccess($sr[18])) {
				if(C_POPULARITY == 2) {
					$qrate =',ROUND(hits/hits_count,'.C_RATE_DIGITS.') as hits';
				} else {
					$qrate = ',ROUND(hits/(TO_DAYS(Now()) - TO_DAYS(IF(regdate > \''.C_LAST_CLEAR_DATE.'\', regdate, \''.C_LAST_CLEAR_DATE.'\')) + 1),'.C_RATE_DIGITS.') as hits';
				}
			}
			else $qrate='';
			$qchild = ',t2.child';

			//$tmp = New CacheQuery("SELECT t1.id,t1.username,t1.fname,t1.lname,t1.files".$qage.$qgender.$qchild.$qbodytype.$qorientation.$qpurpose.$qcountry.$qcity.$qheight.$qweight.$qsgender.$qhoro.$qregdate.$qeditdate.$qusertype.$qzip.$qrate." FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2 WHERE ".$mid.$msrc.$where_in,IsCache(1,16384));
                        $query = "SELECT t1.id,t1.username,t1.fname,t1.lname,t1.files".$qage.$qgender.$qchild.$qbodytype.$qorientation.$qpurpose.$qcountry.$qcity.$qheight.$qweight.$qsgender.$qhoro.$qregdate.$qeditdate.$qusertype.$qzip.$qrate." FROM ".C_MYSQL_MEMBERS_MAIN." as t1, ".C_MYSQL_MEMBERS_EXT." as t2 WHERE ".$mid.$msrc.$where_in.$msortby;
                        $result = db_query($query);
                        $tmp = array();
                        while ($i = db_fetch_array($result)){
                            $tmp[] = $i;
                        }
        
			$str='';$color='';
			$ids=$names=$photoinfo=$genders=$childs=$sgenders=$fnames=$lnames=$bodytypes=$orientations=$purposes=$ages=$countries=$cities=$heights=$weights=$horoscopes=$rates=$usertypes=$editdates=$regdates=$pictures=$colors=$zips=array();
			### What to show
			function ShowConst($var,$acs)
			{
				Global $sr;
				if(IsAccess($sr[$acs])) define($var,'1');
				else define($var,'0');
			}

			/*ShowConst('SHOW_FNAME',2);
			ShowConst('SHOW_LNAME',3);*/
			define('SHOW_FNAME',false);
			define('SHOW_LNAME', false);
			ShowConst('SHOW_AGE',4);
			ShowConst('SHOW_GENDER',5);
			define('CHILD', $w[135]);
			ShowConst('SHOW_BODYTYPE',6);
			ShowConst('SHOW_ORIENTATION',7);
			ShowConst('SHOW_PURPOSE',8);
			ShowConst('SHOW_COUNTRY',9);
			ShowConst('SHOW_CITY',10);
			ShowConst('SHOW_HEIGHT',11);
			ShowConst('SHOW_WEIGHT',12);
			/*ShowConst('SHOW_SEEKING_GENDER',13);*/
			define('SHOW_SEEKING_GENDER', false);
			ShowConst('SHOW_PHOTO',14);
			ShowConst('SHOW_HORO',15);
			ShowConst('SHOW_REG_DATE',16);
			/*ShowConst('SHOW_LAST_ACCESS',17);*/
			define('SHOW_LAST_ACCESS',true);
			ShowConst('SHOW_RATING',18);
			ShowConst('SHOW_USERTYPE',19);
			ShowConst('SHOW_ONLINE_STATUS',20);
			/*ShowConst('SHOW_ZIP',21);*/
			define('SHOW_ZIP', false);
			define('ONLINE_STATUS',$w[352]);
			define('ONLINE', $w[350]);
			define('OFFLINE', $w[351]);
			define('TOPS', false);
			define('WHENSEEN', false);

			while(list($k,$i)=@each($tmp)) {
				$pictures[] = SearchPhotoResult(IsAccess($sr[14]),$i['files'],$i['id'],$i['gender']);
				$color = ($color == COLOR4) ? COLOR3 : COLOR4;
				$colors[] = $color;
				$ids[]=$i['id'];
				$photoinfo[]=($i['files'] == '') ? $w[532] : template($w[533],array(PhotosNum($i['files'])));
				$names[] = username($i['id'],$i['username'],$i['fname'],$i['lname']);
				$fnames[] = (IsAccess($sr[2])) ? $i['fname'] : '';
				$lnames[] = (IsAccess($sr[3])) ? $i['lname'] : '';
				$genders[]=(IsAccess($sr[5])) ? $wg[$i['gender']] : '';
				$sgenders[]=(IsAccess($sr[13])) ? arr2val($wg, $i['sgender']) : '';
				$purposes[]=(IsAccess($sr[8])) ? arr2val($wp, $i['purposes']) : '';
				$orientations[]=(IsAccess($sr[7])) ? $wor[$i['orientation']] : '';
				$heights[]=(IsAccess($sr[11])) ? $wh[$i['height']] : '';
				$weights[]=(IsAccess($sr[12])) ? $ww[$i['weight']] : '';
				$horoscopes[]=(IsAccess($sr[15])) ? $whr[$i['horo']] : '';
				$bodytypes[]=(IsAccess($sr[6])) ? $wbt[$i['bodytype']] : '';
				$childs[]= $wc[$i['child']];
				$ages[]=(IsAccess($sr[4])) ? abs(mysql2data($i['birthday'],1)) : '';
				$countries[]=(IsAccess($sr[9])) ? $wcr[$i['country']] : '';
				$cities[]=(IsAccess($sr[10])) ? $i['city'] : '';
				$usertypes[]=(IsAccess($sr[19])) ? $wpu[$i['req']] : '';
				/*$editdates[]=(IsAccess($sr[17])) ? mysql2data($i['editdate']) : '';*/
				if (isset($i['editdate'])){
					$editdates[]= mysql2data($i['editdate']);
				} else {
					$editdates[] = '';
				}
				$regdates[]=(IsAccess($sr[16])) ? mysql2data($i['regdate']) : '';
				$rates[]=(IsAccess($sr[18])) ? $i['hits'] : '';
				$zips[]=(IsAccess($sr[21])) ? $i['zip'] : '';
			}


			$param='l='.$l.'&a=s&src='.$src.'&val='.$val;
			$colspan=1;
			define('PAGES',pages($from,$step,$count,$param,$colspan));
			define('SEARCH_RESULTS',search_results($from,$step,$count,IsAccess(C_SHOW_SEARCH_COUNT),STATISTICS));
			define('USERNAME',$w[118]);
			define('PURPOSES',$w[119]);
			define('AGE',$w[120]);
			define('COUNTRY',$w[121]);
			define('CITY',$w[122]);
			define('LAST_ACCESS',$w[123]);
			define('REGISTER_DATE',$w[124]);
			define('PHOTO',$w[87]);
			define('RATING',$w[216]);
			define('FIRSTNAME',$w[127]);
			define('LASTNAME',$w[128]);
			define('GENDER',$w[57]);
			define('BODYTYPE',$w[525]);
			define('ORIENTATION',$w[524]);
			define('HEIGHT',$w[130]);
			define('WEIGHT',$w[131]);
			define('SEEKING_GENDER',$w[80]);
			define('HOROSCOPE',$w[129]);
			define('USERTYPE',$w[270]);
			define('ZIP',$w[285]);
			define('ADDITIONAL_INFO',$w[534]);
			$p=0;

			include_once C_PATH.'/templates/'.C_TEMP.'/search_results.php';

} else {

        unset($_SESSION['search_type']);    
        unset($_SESSION['search_date']);
        unset($_SESSION['search_data']);
	$tmp = New CacheQuery("SELECT count(*) as total FROM ".C_MYSQL_MEMBERS_MAIN." WHERE status > '5'",IsCache(1,8192));
	if ($tmp->data[0]['total'] == 0) printm($w[204]);

	function azdg_stat($stat_need=array(),$w_num=array(),$mysql_name=array(),$w_name=array()) {
		global $w;
		for($i=0;$i<sizeof($stat_need);$i++) {
			if($stat_need[$i]) {
				/*$_STATNAME=$w[$w_num[$i]];

				$color='';
				$colors=$counts=$values=array();
				$DB_NAME = (substr($mysql_name[$i],1,1) == '1') ? C_MYSQL_MEMBERS_MAIN.' as t1' : C_MYSQL_MEMBERS_EXT.' as t2';
				$tmp = New CacheQuery("SELECT ".$mysql_name[$i].", count(*) as c FROM ".$DB_NAME." WHERE status >= '7' group by ".$mysql_name[$i]." order by c desc",IsCache(1,8192));
				while(list($k,$j)=@each($tmp->data)) {
					$color = ($color == COLOR4) ? COLOR3 : COLOR4;
					$colors[]=$color;
					$larr = $w_name[$i];

					global ${$larr};$mas = ${$larr};

					$counts[]=$j['c'];

					$sttmp = $j[substr(strstr($mysql_name[$i], '.'),1)];
					if($stat_need[$i] == '2') {
						global $l;
						$param='l='.$l.'&a=s&src='.$i.'&val='.$sttmp;
						if(isset($mas[$sttmp])){
							$values[] = '<a href="'.C_URL.'/stat.php?'.$param.'">'.$mas[$sttmp].'</a>';
						}
					} else {
						$values[] = $mas[$sttmp];
					}

				}
				$p=0;
				include(C_PATH.'/templates/'.C_TEMP.'/stat.php');*/
				$larr = $w_name[$i];
				global ${$larr};$mas = ${$larr};
				global $l;
				$_STATNAME=$w[$w_num[$i]];
				$color='';
				$colors=$counts=$values=array();
				$DB_NAME = (substr($mysql_name[$i],1,1) == '1') ? C_MYSQL_MEMBERS_MAIN.' as t1' : C_MYSQL_MEMBERS_EXT.' as t2';
				$tmp = New CacheQuery("SELECT ".$mysql_name[$i].", count(*) as c FROM ".$DB_NAME." WHERE status > '5' group by ".$mysql_name[$i]." order by c desc",IsCache(1,8192));
				while(list($k,$j)=@each($tmp->data)) {
					$sttmp = $j[substr(strstr($mysql_name[$i], '.'),1)];
					if(isset($mas[$sttmp])){
						$color = ($color == COLOR4) ? COLOR3 : COLOR4;
						$colors[$sttmp]=$color;
						$counts[$sttmp] = $j['c'];
						if($stat_need[$i] == '2') {
							$param='l='.$l.'&a=s&src='.$i.'&val='.$sttmp;
							$values[$sttmp] = '<a href="'.C_URL.'/stat.php?'.$param.'">'.$mas[$sttmp].'</a>';
						} else {
							$values[$sttmp] = $mas[$sttmp];
						}
					} else {
						$more_sttmp = explode(',', $sttmp);
						foreach ($more_sttmp as $more){
							if (isset($mas[$more])){
								if (isset($counts[$more])){
									$counts[$more] = $counts[$more] + $j['c'];
								} else {
									$color = ($color == COLOR4) ? COLOR3 : COLOR4;
									$colors[$more]=$color;
									$counts[$more] = $j['c'];
									if($stat_need[$i] == '2') {
										$param='l='.$l.'&a=s&src='.$i.'&val='.$more;
										$values[$more] = '<a href="'.C_URL.'/stat.php?'.$param.'">'.$mas[$more].'</a>';
									} else {
										$values[$more] = $mas[$more];
									}
								}
							}
						}
					}
				}
				if (count($values) > 1){
					include(C_PATH.'/templates/'.C_TEMP.'/stat.php');
				}
			}
		}
	}
	azdg_stat($stat_need,$w_num,$mysql_name,$w_name);
}
include_once C_PATH.'/footer.php';
?>
