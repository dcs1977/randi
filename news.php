<?php
############################################################
# \-\-\-\-\-\-\     AzDG  - S C R I P T S    /-/-/-/-/-/-/ #
############################################################
# AzDGDatingMedium        Version 1.9.3                    #
# Written by              AzDG (support@azdg.com)          #
# Created 24/10/02        Last Modified 22/03/06           #
# Scripts Home:           http://www.azdg.com              #
############################################################
# File name               news.php                         #
# File purpose            Website news                     #
# File created by         AzDG <support@azdg.com>          #
############################################################
include_once 'include/config.inc.php';
include_once 'include/options.inc.php';
include_once 'include/security.inc.php';
include_once 'include/functions.inc.php';
include_once 'templates/'.C_TEMP.'/config.php';
include_once C_PATH.'/sheader.php';
if(!C_NEWS_ALLOW) sprintm($w[1]);

if(!isset($from) || !is_numeric($from)) $from = C_NEWS_NUM;
$step = C_NEWS_NUM;
if(C_NEWS_NUM <= 0 || !is_numeric(C_NEWS_NUM)) sprintm($w[345]);
if (get_info('adminip') == md5(ip())) $news_type='0';
else $news_type='7';
$mlang = (C_NEWS_MLTLNG) ? " AND lang = '".cb($l)."' AND lang != ''" : "";
$tmp=db_query("SELECT * FROM ".C_MYSQL_NEWS." WHERE type >= '".$news_type."'".$mlang." order by priority DESC, date DESC limit ".$from.",".$step);
$tmp2=db_query("SELECT count(id) as total FROM ".C_MYSQL_NEWS." WHERE type >= '".$news_type."'".$mlang);
$trows = db_fetch_array($tmp2);

$count = $trows['total'];
$param='l='.$l;
$colspan=1;
define('PAGES',pages($from,$step,$count,$param,$colspan));

define('NEWS',$w[344]);
define('NEWS_COUNT',db_num_rows($tmp));

if(NEWS_COUNT == '0') {
  define('NO_NEWS',$w[345]);
  } else {
  define('NO_NEWS','');
}

$subjects=array();$dates=array();$messages=array();
while($i=db_fetch_array($tmp)) {
  $subjects[]=quot_add($i['subject']);
  $dates[]=mysql2data($i['date']);
  $messages[]=quot_add($i['news']);
}
$p=0;
include_once C_PATH.'/templates/'.C_TEMP.'/news.php';
include_once C_PATH.'/sfooter.php';
?>